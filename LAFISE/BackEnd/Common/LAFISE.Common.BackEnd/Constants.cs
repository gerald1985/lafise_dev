﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LAFISE.Common.Backend
{
    /// <summary>
    /// Clase para el manejo de los valores constantes del Backend
    /// </summary>
    public class Constants
    {
        //Importante: Documentar cada constante
        /// <summary>
        /// Este es la ruta de archivos de tarjeta
        /// </summary>
        public const string RUTA_ARCHIVOS_TARJETA = "RUTA_ARCHIVOS_TARJETA";

        /// <summary>
        /// Este es la ruta de archivos de marca auto
        /// </summary>
        public const string RUTA_ARCHIVOS_MODELOS = "RUTA_ARCHIVOS_MODELOS";

        /// <summary>
        /// Este es la ruta de archivos thumb de marca auto
        /// </summary>
        public const string RUTA_ARCHIVOS_MODELOS_THUMB = "RUTA_ARCHIVOS_THUMB_MODELOS";

        /// <summary>
        /// Nombre del país no encontrado
        /// </summary>
        public const string PAIS_NO_ENCONTRADO = "GENERICO";

        /// <summary>
        /// Mensaje cuando no se envia correo
        /// </summary>
        public const string MENSAJE_EMAIL = "Ha ocurrido un error con el envío de correo";
        /// <summary>
        /// Información de la garantia
        /// </summary>
        public const string INFORMACION_GARANTIA = "Fiduciaria";
        /// <summary>
        /// Contiene la marca de auto (Otro)
        /// </summary>
        public const string MARCA_AUTO_OTRO = "Otro";

        /// <summary>
        /// Identificador del estado rechazado
        /// </summary>
        public const string ESTADO_RECHAZADO = "2";

        /// <summary>
        /// Identificador del estado desembolsado
        /// </summary>
        public const string ESTADO_DESEMBOLSADO = "4";

        /// <summary>
        /// Identificador del estado terminación proceso
        /// </summary>
        public const string ESTADO_TERMINACIONPROCESO = "5";

        /// <summary>
        /// Contiene la url para realizar la consulta de datos en yahoo finance
        /// </summary>
        public const string URL_YAHOO = "http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quotes where symbol in ({0})&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys";

        /// <summary>
        /// Contiene la url para realizar la consulta de históricos de 
        /// </summary>
        public const string URL_YAHOO_HISTORICO = "https://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.historicaldata where symbol in ({0}) and startDate={1} and endDate={2}&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys";


        /// <summary>
        /// Este es el email para el envio de correo contactenos
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_CONTACTENOS = "EMAIL_ENVIO_CORREO_CONTACTENOS";

        /// <summary>
        /// Este es el email para el envio de correo marca auto
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_MARCAAUTO = "EMAIL_ENVIO_CORREO_MARCAAUTO";

        /// <summary>
        /// Este es el email para el envio de correo prestamo personal
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_PRESTAMOPERSONAL = "EMAIL_ENVIO_CORREO_PRESTAMOPERSONAL";

        /// <summary>
        /// Este es el email para el envio de correo prestamo hipotecario
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_PRESTAMOHIPOTECARIO = "EMAIL_ENVIO_CORREO_PRESTAMOHIPOTECARIO";

        /// <summary>
        /// Este es el email para el envio de correo tarjeta credito
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_TARJETACREDITO = "EMAIL_ENVIO_CORREO_TARJETACREDITO";

        /// <summary>
        /// Este es el email para el envio de correo tarjeta debito
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_TARJETADEBITO = "EMAIL_ENVIO_CORREO_TARJETADEBITO";

        /// <summary>
        /// Este es el email para el envio de correo prestamo educativo
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_PRESTAMOEDUCATIVO = "EMAIL_ENVIO_CORREO_PRESTAMOEDUCATIVO";

        /// <summary>
        /// Este es el email para el envio de correo apertura cuenta
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_APERTURACUENTA = "EMAIL_ENVIO_CORREO_APERTURACUENTA";

        /// <summary>
        /// Este es el email para el envio de correo seguro de vida de accidente
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_SEGUROVIDAACCIDENTE = "EMAIL_ENVIO_CORREO_SEGUROVIDAACCIDENTE";

        /// <summary>
        /// Este es el email para el envio de correo seguro vehicular
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_SEGUROVEHICULAR = "EMAIL_ENVIO_CORREO_SEGUROVEHICULAR";

        /// <summary>
        /// Este es el email para el envio de correo reclamos
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_RECLAMOS = "EMAIL_ENVIO_CORREO_RECLAMOS";

        /// <summary>
        /// Este es el identificador del portal
        /// </summary>
        public const string IDENTIFICADOR_PORTAL = "IDENTIFICADOR_PORTAL";

        /// <summary>
        /// Contiene el contenido del mensaje para el usuario en una solicitud 
        /// </summary>
        public const string SOLICITUD_CORREO_CONTENIDO_USUARIO = "Su {0} # {1} ha sido ingresada exitosamente. Uno de nuestros asesores se contactará con usted.";

        /// <summary>
        /// Contiene el contenido del mensaje para el administrador en una solicitud 
        /// </summary>
        public const string SOLICITUD_CORREO_CONTENIDO_ADMIN = "La solicitud número {0} ";

        /// <summary>
        /// Contiene la procedencia de donde fue llamado algún formulario - ejm facebook 
        /// </summary>
        public const string PROCEDENCIA = "procedencia";

        /// <summary>
        /// Este es el email para el envio de contáctenos admin NICARAGUA
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_CONTACTENOS_NICARAGUA = "EMAIL_ENVIO_CORREO_CONTACTENOS_NICARAGUA";

        /// <summary>
        /// Este es el email para el envio de contáctenos admin COSTA RICA
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_CONTACTENOS_COSTA_RICA = "EMAIL_ENVIO_CORREO_CONTACTENOS_COSTA_RICA";

        /// <summary>
        /// Este es el email para el envio de contáctenos admin HONDURAS
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_CONTACTENOS_HONDURAS = "EMAIL_ENVIO_CORREO_CONTACTENOS_HONDURAS";

        /// <summary>
        /// Este es el email para el envio de contáctenos admin PANAMA
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_CONTACTENOS_PANAMA = "EMAIL_ENVIO_CORREO_CONTACTENOS_PANAMA";

        /// <summary>
        /// Este es el email para el envio de contáctenos admin REPUBLICA DOMINICANA
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_CONTACTENOS_REP_DOMINICANA = "EMAIL_ENVIO_CORREO_CONTACTENOS_REP_DOMINICANA";

        /// <summary>
        /// Este es el email para el envio de contáctenos admin GENERICO
        /// </summary>
        public const string EMAIL_ENVIO_CORREO_CONTACTENOS_GENERICO = "EMAIL_ENVIO_CORREO_CONTACTENOS_GENERICO";

    }
}
