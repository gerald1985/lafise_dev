﻿/*==========================================================================
Archivo:            Enumeradores
Descripción:        Enumeradores que necesita el sitio                   
Autor:              steven.echavarria                          
Fecha de creación:  30/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias


#endregion

namespace LAFISE.Common.Backend
{
    /// <summary>
    /// Enumerador para manejar las acciones del log de auditoria
    /// </summary>
    public enum Accion
    {
        Creacion,
        Modificacion,
        Inactivacion,
        Eliminacion
    }

    public enum EstadoTransaccion
    {
        /// <summary>
        /// Si el elemento se va a adicionar
        /// </summary>
        Add,
        /// <summary>
        /// Si el elemento se va a actualizar
        /// </summary>
        Edit,
        /// <summary>
        /// Si el elemento se va a eliminar
        /// </summary>
        Delete
    }
}
