﻿/*==========================================================================
Archivo:            LogHelper
Descripción:        (Descripción de LogHelper..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  30/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using NLog;
#endregion


namespace LAFISE.Common.Backend.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class LogHelper
    {
        private static Logger logger = LogManager.GetLogger("audit");

        private const string Usuario = "Usuario";
        private const string Modulo = "Modulo";
        private const string Operacion = "Operacion";
        private const string OperacionExitosa = "OperacionExitosa";
                

        public static void Audit(string modulo, string operacion,  string usuario = "Lafise",bool operacionExitosa = true)
        {
            var info = new LogEventInfo(LogLevel.Info, "audit", null, string.Empty, null);
            info.Properties[Usuario] = usuario;
            info.Properties[Modulo] = modulo;
            info.Properties[Operacion] = operacion;
            info.Properties[OperacionExitosa] = operacionExitosa.ToString();
            logger.Log(info);
        }
    }
}
