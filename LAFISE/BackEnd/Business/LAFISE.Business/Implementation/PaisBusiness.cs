﻿/*==========================================================================
Archivo:            PaisBusiness
Descripción:        Logica de negocio de Pais                   
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para país
    /// </summary>
    public class PaisBusiness : IPaisBusiness
    {
        /// <summary>
        /// Instancia de la interfaz IPaisData;
        /// </summary>
        IPaisData _PaisData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public PaisBusiness()
        {
            _PaisData = new PaisData();
        }

        /// <summary>
        /// Método usado para cargar todos los paises
        /// </summary>
        /// <returns>Lista de paises</returns>
        public async Task<BusinessResult<ICollection<PaisDto>>> VerTodos()
        {
            try
            {
                var result = await _PaisData.VerTodos();
                return BusinessResult<ICollection<PaisDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<PaisDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar un pais por su id
        /// </summary>
        /// <param name="id">Identificador del pais</param>
        /// <returns>Pais</returns>
        public async Task<BusinessResult<PaisDto>> VerPaisPorId(int id)
        {
            try
            {
                var result = await _PaisData.VerPaisPorId(id);
                return BusinessResult<PaisDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<PaisDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo que se encarga de consultar el pais por el nombre
        /// </summary>
        /// <param name="nombre">Nombre pais</param>
        /// <returns>Identificador de país</returns>
        public async Task<BusinessResult<Int64>> VerPaisPorNombre(string nombre)
        {
            try
            {
                var result = await _PaisData.VerPaisPorNombre(nombre);
                return BusinessResult<Int64>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<Int64>.Issue(0, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// Metodo que se encarga de consultar el pais por el nombre no asincrono
        /// </summary>
        /// <param name="nombre">Nombre pais</param>
        /// <returns>Identificador de país</returns>
        public BusinessResult<Int64> VerPaisPorNombreNoAsincrono(string nombre)
        {
            try
            {
                var result =  _PaisData.VerPaisPorNombreNoAsincrono(nombre);
                return BusinessResult<Int64>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<Int64>.Issue(0, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todas los paises de la lista no asincrono
        /// </summary>
        /// <param name="listaPais">lista con el identificador de pais</param>
        /// <returns>Lista de pais</returns>
        public BusinessResult<ICollection<PaisDto>> VerPaisPorListaIdNoAsincrono(List<Int64> listaPais)
        {
            try
            {
                var result =  _PaisData.VerPaisPorListaIdNoAsincrono(listaPais);
                return BusinessResult<ICollection<PaisDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<PaisDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
