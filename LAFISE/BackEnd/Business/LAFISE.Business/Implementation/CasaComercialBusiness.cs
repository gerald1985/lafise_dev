/*==========================================================================
Archivo:            CasaComercialBusiness
Descripción:        Logica de negocio de CasaComercial                      
Autor:              Juan.Hincapie                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Business
{


    /// <summary>
    /// Clase encargada del manejo de logica de negocio para las casas comerciales.
    /// </summary>
    public class CasaComercialBusiness : ICasaComercialBusiness
    {
        /// <summary>
        /// Constantes
        /// </summary>
        #region Constantes
        public const string ModuloCasaComercial = "Casa Comercial";
        public const string ClaveUnicaCasaComercialNombre = "UQ_CasaComercial_Nombre";
        public const string ClaveForaneaPlanFinanciacionCasaComercial = "FK_PlanFinanciacion_CasaComercial";
        #endregion

        /// <summary>
        /// Instancia de la clase CasaComercialData;
        /// </summary>
        ICasaComercialData _CasaComercialData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public CasaComercialBusiness()
        {
            _CasaComercialData = new CasaComercialData();

        }


        /// <summary>
        /// Método encargado de consultar todas las casas comerciales.
        /// </summary>
        /// <returns>BusinessResult con la lista de todas las casas comerciales.</returns>
        public async Task<BusinessResult<ICollection<CasaComercialDto>>> ConsultarTodosGrid()
        {
            try
            {
                var result = await _CasaComercialData.ConsultarTodosGrid();
                return BusinessResult<ICollection<CasaComercialDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CasaComercialDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="Modelo">Identificador del modelo.</param>
        /// <returns>Entidad CasaComercial.</returns>
        public async Task<BusinessResult<ICollection<CasaComercialDto>>> VerCasaComercialPorModelo(long Modelo)
        {
            try
            {
                var result = await _CasaComercialData.VerCasaComercialPorModelo(Modelo);
                return BusinessResult<ICollection<CasaComercialDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CasaComercialDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de consultar el detalle de la casa comercial.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>BusinessResult con la lista del detalle de las casas comerciales. </returns>
        public async Task<BusinessResult<CasaComercialDto>> ConsultarDetalleCasaComercial(long id)
        {
            try
            {
                var result = await _CasaComercialData.ConsultarDetalleCasaComercial(id);
                return BusinessResult<CasaComercialDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial.</returns>
        public async Task<BusinessResult<CasaComercialDto>> VerCasaComercialPorId(long id)
        {
            try
            {
                var result = await _CasaComercialData.VerCasaComercialPorId(id);
                return BusinessResult<CasaComercialDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial.</returns>
        public BusinessResult<CasaComercialDto> VerCasaComercialPorIdNoAsincrono(long id)
        {
            try
            {
                var result = _CasaComercialData.VerCasaComercialPorIdNoAsincrono(id);
                return BusinessResult<CasaComercialDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo que valida que el nombre de la casa comercial sea unico.
        /// </summary>
        /// <param name="casacomercial">entidad casa comercial</param>
        /// <returns>casacomercial</returns>
        public async Task<BusinessResult<CasaComercialDto>> ValidarCasaComercialPorNombre(CasaComercialDto casacomercial)
        {
            try
            {
                var result = await _CasaComercialData.ValidarCasaComercialPorNombre(casacomercial);
                return BusinessResult<CasaComercialDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de eliminar las casas comerciales según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>BusinessResult con true: Elimino Correctamente; false: No se logro eliminar</returns>
        public async Task<BusinessResult<CasaComercialDto>> EliminarCasaComercialPorId(long id, string usuario)
        {
            try
            {
                var result = await _CasaComercialData.EliminarCasaComercialPorId(id);
                CasaComercialDto _mensajeDto = new CasaComercialDto { mensajeError = Messages.Messages.OperacionCorrecta };
                LogHelper.Audit(ModuloCasaComercial, Accion.Eliminacion.ToString(), usuario);
                return BusinessResult<CasaComercialDto>.Success(_mensajeDto, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Eliminacion.ToString(),usuario, false);
                if (ex.InnerException.InnerException.Message.Contains(ClaveForaneaPlanFinanciacionCasaComercial))
                {
                    CasaComercialDto _mensajeDto = new CasaComercialDto { mensajeError = Messages.Messages.OperacionErroneaFK };
                    return BusinessResult<CasaComercialDto>.Issue(_mensajeDto, Messages.Messages.OperacionErroneaFK, ex);
                }
                else
                {
                    CasaComercialDto _mensajeDto = new CasaComercialDto { mensajeError = Messages.Messages.OperacionErronea };
                    return BusinessResult<CasaComercialDto>.Issue(_mensajeDto, Messages.Messages.OperacionErronea, ex);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Eliminacion.ToString(),usuario, false);
                CasaComercialDto _mensajeDto = new CasaComercialDto { mensajeError = Messages.Messages.OperacionErronea };
                return BusinessResult<CasaComercialDto>.Issue(_mensajeDto, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de crear las casas comerciales y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial que se insertó.</returns>
        public async Task<BusinessResult<CasaComercialDto>> CrearCasaComercial(CasaComercialDto casacomercial)
        {
            try
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Creacion.ToString(), casacomercial.Usuario);
                var result = await _CasaComercialData.CrearCasaComercial(casacomercial);
                return BusinessResult<CasaComercialDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Creacion.ToString(),casacomercial.Usuario, false);
                if (ex.InnerException.InnerException.Message.Contains(ClaveUnicaCasaComercialNombre))
                {
                    return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                else { return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex); }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Creacion.ToString(), casacomercial.Usuario, false);
                return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial que se editó.</returns>
        public async Task<BusinessResult<CasaComercialDto>> EditarCasaComercial(CasaComercialDto casacomercial)
        {
            try
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Modificacion.ToString(), casacomercial.Usuario);
                var result = await _CasaComercialData.EditarCasaComercial(casacomercial);
                return BusinessResult<CasaComercialDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Modificacion.ToString(), casacomercial.Usuario, false);
                if (ex.InnerException.InnerException.Message.Contains(ClaveUnicaCasaComercialNombre))
                {
                    return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                else
                {
                    return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Modificacion.ToString(), casacomercial.Usuario, false);
                return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de consultar las casas comerciales según los filtros de búsqueda
        /// </summary>
        /// <param name="casaComercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult con la lista de todas las casas comerciales.</returns>
        public async Task<BusinessResult<ICollection<CasaComercialFiltroDto>>> ConsultarCasaComercialFiltros(CasaComercialDto casaComercial)
        {
            try
            {
                var result = await _CasaComercialData.ConsultarCasaComercialFiltros(casaComercial);
                return BusinessResult<ICollection<CasaComercialFiltroDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CasaComercialFiltroDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de consultar los modelos según los filtros de las marcas
        /// </summary>
        /// <param name="marcas">id de las marcas separadas por comas</param>
        /// <returns>lista de modelos agrupados por marca</returns>
        public async Task<BusinessResult<ICollection<CasaComercialModeloMarcaFiltroDto>>> ConsultarCasaComercialModeloMarcaFiltros(string marcas)
        {
            try
            {
                var result = await _CasaComercialData.ConsultarCasaComercialModeloMarcaFiltros(marcas);

                if (result != null)
                {
                    IParametrosData parametros = new ParametrosData();
                    var ruta = parametros.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_MODELOS);
                    var rutaThumb = parametros.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_MODELOS_THUMB);
                    string path = ruta != null ? ruta.Result : string.Empty;
                    string pathThumb = rutaThumb != null ? rutaThumb.Result : string.Empty;
                    var query = (from ma in result
                                 select new CasaComercialModeloMarcaFiltroDto
                                 {
                                     IdMarcaAuto = ma.IdMarcaAuto,
                                     Marca = ma.Marca,
                                     listaModelos = (from mod in result
                                                     where mod.IdMarcaAuto == ma.IdMarcaAuto
                                                     orderby mod.Modelo
                                                     select new ModeloAutoDto
                                                     {
                                                         Id = mod.IdModelo,
                                                         Modelo = mod.Modelo,
                                                         IdMarcaAuto = ma.IdMarcaAuto,
                                                         Imagen = mod.Imagen,
                                                         RutaImagen = path + mod.Imagen,
                                                         RutaThumb = pathThumb + mod.Imagen,
                                                         Activo = mod.Activo
                                                     }).ToList()
                                 }).ToList();
                    var listaFiltrada = query.DistinctBy(s => new { s.IdMarcaAuto, s.Marca }).ToList();
                    return BusinessResult<ICollection<CasaComercialModeloMarcaFiltroDto>>.Success(listaFiltrada, Messages.Messages.OperacionCorrecta);
                }

                return BusinessResult<ICollection<CasaComercialModeloMarcaFiltroDto>>.Success(result, Messages.Messages.OperacionCorrecta);

            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CasaComercialModeloMarcaFiltroDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de inactivar las casas comerciales.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        public async Task<BusinessResult<CasaComercialDto>> InactivarCasaComercial(CasaComercialDto casacomercial)
        {
            try
            {
                var existePlanFinaUrba = await _CasaComercialData.ExistePlanFinanciacionPorCasaComercial(casacomercial.Id);
                if (!existePlanFinaUrba)
                {
                    LogHelper.Audit(ModuloCasaComercial, Accion.Inactivacion.ToString(), casacomercial.Usuario);
                    var result = await _CasaComercialData.InactivarCasaComercial(casacomercial);
                    return BusinessResult<CasaComercialDto>.Success(result, Messages.Messages.OperacionCorrecta);
                }
                else
                {
                    LogHelper.Audit(ModuloCasaComercial, Accion.Inactivacion.ToString(), casacomercial.Usuario, false);
                    CasaComercialDto _mensajeDto = new CasaComercialDto { mensajeError = Messages.Messages.OperacionErroneaFK };
                    return BusinessResult<CasaComercialDto>.Success(_mensajeDto, Messages.Messages.OperacionErroneaDuplicada);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloCasaComercial, Accion.Inactivacion.ToString(), casacomercial.Usuario,  false);
                return BusinessResult<CasaComercialDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de consultar las casas comerciales según el pais.
        /// </summary>
        /// <param name="idPais">Identificador del pais asociado a la casa comercial.</param>
        /// <returns>BusinessResult conm lista de las casas comerciales.</returns>
        public async Task<BusinessResult<ICollection<CasaComercialDto>>> ConsultarCasaComercialPorPais(long idPais)
        {
            try
            {
                var result = await _CasaComercialData.ConsultarCasaComercialPorPais(idPais);
                return BusinessResult<ICollection<CasaComercialDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CasaComercialDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="entidad">entidad CasaComercialModeloPaisDto</param>
        /// <returns></returns>
        public async Task<BusinessResult<ICollection<CasaComercialModeloPaisDto>>> ConsultarCasaComercialModeloPais(CasaComercialModeloPaisDto entidad)
        {
            try
            {
                string[] sigla = entidad.pathIdPais.Split('/');
                IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                if (portalPais.SuccessfulOperation)
                {
                    if (portalPais.Result != null)
                    {
                        entidad.IdPais = portalPais.Result.IdPais.Value;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                        if (pais.SuccessfulOperation)
                        {
                            entidad.IdPais = pais.Result;
                        }
                    }
                }

                var result = await _CasaComercialData.ConsultarCasaComercialModeloPais(entidad);
                return BusinessResult<ICollection<CasaComercialModeloPaisDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CasaComercialModeloPaisDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}