﻿using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Threading.Tasks;

namespace LAFISE.Business
{ 
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Cotizador
    /// </summary>
    public class CotizadorBusiness : ICotizadorBusiness
    {
        /// <summary>
        /// Constante para logger
        /// </summary>
        public const string ModuloCotizador = "Cotizador";

        /// <summary>
        /// Instancia de la interfaz ICotizadorData;
        /// </summary>
        ICotizadorData _CotizadorData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public CotizadorBusiness()
        {
            _CotizadorData = new CotizadorData();
        }
        /// <summary>
        /// Método usado para cargar los estados por el tipo de estado
        /// </summary>
        /// <param name="cotizador">instancia Cotizador</param>
        /// <returns>Lista de Estados</returns>
        public async Task<BusinessResult<CotizadorDto>> Crear(CotizadorDto cotizador)
        {
            try
            {
                LogHelper.Audit(ModuloCotizador, Accion.Creacion.ToString());
                var result = await _CotizadorData.Crear(cotizador);
                LogHelper.Audit(ModuloCotizador, Accion.Creacion.ToString());
                return BusinessResult<CotizadorDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloCotizador, Accion.Creacion.ToString(), cotizador.Email, false);
                return BusinessResult<CotizadorDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}