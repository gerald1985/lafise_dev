﻿/*==========================================================================
Archivo:            EstadoBusiness
Descripción:        Logica de negocio de Estado               
Autor:              paola.munoz                         
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para estado
    /// </summary>
    public class EstadoBusiness : IEstadoBusiness
    {

        /// <summary>
        /// Instancia de la interfaz IEstadoData;
        /// </summary>
        IEstadoData _EstadoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public EstadoBusiness()
        {
            _EstadoData = new EstadoData();
        }

        /// <summary>
        /// Método usado para cargar los estados por el tipo de estado
        /// </summary>
        /// <param name="idTipoEstado">Identificador del tipo de estado</param>
        /// <returns>Lista de Estados</returns>
        public async Task<BusinessResult<ICollection<EstadoDto>>> VerEstadoIdTipo(Int64 idTipoEstado)
        {
            try
            {
                var result = await _EstadoData.VerEstadoIdTipo(idTipoEstado);
                return BusinessResult<ICollection<EstadoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<EstadoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo para consultar el estado por el identificador
        /// </summary>
        /// <param name="id">identificador del estado</param>
        /// <returns>Estado
        /// </returns>
        public BusinessResult<EstadoDto> VerEstadoById(Int64 id)
        {
            try
            {
                var result =  _EstadoData.VerEstadoPorId(id);
                return BusinessResult<EstadoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<EstadoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
