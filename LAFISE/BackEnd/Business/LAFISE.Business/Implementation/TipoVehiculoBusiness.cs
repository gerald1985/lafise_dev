﻿/*==========================================================================
Archivo:            TipoVehiculoBusiness
Descripción:        Logica de negocio de TipoVehículo                     
Autor:              juan.hincapie                          
Fecha de creación:  06/10/2015 10:15:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using ARKIX.Business;

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Tipo Vehículo
    /// </summary>
    public class TipoVehiculoBusiness : ITipoVehiculoBusiness
    {
        /// <summary>
        /// Instancia de la clase TipoVehiculoData;
        /// </summary>
        ITipoVehiculoData _TipoVehiculoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TipoVehiculoBusiness()
        {
            _TipoVehiculoData = new TipoVehiculoData();
        }

        /// <summary>
        /// Ver todos los tipos de sucursal
        /// </summary>
        /// <returns>Lista de tipo de sucursal</returns>
        public async Task<BusinessResult<ICollection<TipoVehiculoDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _TipoVehiculoData.VerTodosGrid();
                return BusinessResult<ICollection<TipoVehiculoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoVehiculoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar el tipo de vehiculo
        /// </summary>
        /// <returns>TipoVehiculoDto</returns>
        public BusinessResult<TipoVehiculoDto> VerPorIdNoAsincrono(Int64 id)
        {
            try
            {
                var result = _TipoVehiculoData.VerPorIdNoAsincrono(id);
                return BusinessResult<TipoVehiculoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoVehiculoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
