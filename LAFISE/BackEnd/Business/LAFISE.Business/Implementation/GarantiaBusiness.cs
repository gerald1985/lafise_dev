﻿/*==========================================================================
Archivo:            GarantiaBusiness
Descripción:        Logica de negocio de garantia               
Autor:              paola.munoz                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para garantia
    /// </summary>
    public class GarantiaBusiness : IGarantiaBusiness
    {

        /// <summary>
        /// Instancia de la interfaz IGarantiaData;
        /// </summary>
        IGarantiaData _GarantiaData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public GarantiaBusiness()
        {
            _GarantiaData = new GarantiaData();
        }

        /// <summary>
        /// Método usado para cargar todas las garantias
        /// </summary>
        /// <returns>Lista de Garantias</returns>
        public async Task<BusinessResult<ICollection<GarantiaDto>>> VerTodos()
        {
            try
            {
                var result = await _GarantiaData.VerTodos();
                return BusinessResult<ICollection<GarantiaDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<GarantiaDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo encargado de ver la garantia por identificador
        /// </summary>
        /// <param name="id">Identificador de la garantia</param>
        /// <returns>Garantia</returns>
        public async Task<BusinessResult<GarantiaDto>> VerGarantiaPorId(Int64 id)
        {
            try
            {
                var result = await _GarantiaData.VerGarantiaPorId(id);
                return BusinessResult<GarantiaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<GarantiaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo encargado de ver la garantia por identificador no asincrono
        /// </summary>
        /// <param name="id">Identificador de la garantia</param>
        /// <returns>Garantia</returns>
        public BusinessResult<GarantiaDto> VerGarantiaPorIdNoAsincrono(Int64 id)
        {
            try
            {
                var result = _GarantiaData.VerGarantiaPorIdNoAsincrono(id);
                return BusinessResult<GarantiaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<GarantiaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
