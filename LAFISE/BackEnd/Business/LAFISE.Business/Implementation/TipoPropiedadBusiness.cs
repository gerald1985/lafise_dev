﻿/*==========================================================================
Archivo:            TipoPropiedadBusiness
Descripción:        logica de negocio TipoPropiedad                      
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 10:15:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Tipo propiedad
    /// </summary>
    public class TipoPropiedadBusiness : ITipoPropiedadBusiness
    {
        /// <summary>
        /// Instancia de la clase TipoPropiedadData;
        /// </summary>
        ITipoPropiedadData _TipoPropiedadData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TipoPropiedadBusiness()
        {
            _TipoPropiedadData = new TipoPropiedadData();

        }
        /// <summary>
        /// Ver todos los tipos de propiedad
        /// </summary>
        /// <returns>Lista de tipo propiedad</returns>
        public async Task<BusinessResult<ICollection<TipoPropiedadDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _TipoPropiedadData.VerTodosGrid();
                return BusinessResult<ICollection<TipoPropiedadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoPropiedadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Método usado para cargar los tipos de propiedades que están activas
        /// </summary>
        /// <returns>Lista de tipo propiedad</returns>
        public async Task<BusinessResult<ICollection<TipoPropiedadDto>>> VerTodosActivos()
        {
            try
            {
                var result = await _TipoPropiedadData.VerTodosActivos();
                return BusinessResult<ICollection<TipoPropiedadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoPropiedadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Ver Tipo propiedad por identificador
        /// </summary>
        /// <param name="Id">Identificador de tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        public async Task<BusinessResult<TipoPropiedadDto>> VerPorId(long Id)
        {
            try
            {
                var result = await _TipoPropiedadData.VerPorId(Id);
                return BusinessResult<TipoPropiedadDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Ver Tipo propiedad por identificador no asincrono
        /// </summary>
        /// <param name="Id">Identificador de tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        public BusinessResult<TipoPropiedadDto> VerPorIdNoAsincrono(long Id)
        {
            try
            {
                var result =  _TipoPropiedadData.VerPorIdNoAsincrono(Id);
                return BusinessResult<TipoPropiedadDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Ver Tipo propiedad por identificador
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de la urbanizadora</param>
        /// <returns>Tipo propiedad</returns>
        public async Task<BusinessResult<ICollection<TipoPropiedadDto>>> VerTiposPropiedadPorUrbanizadora(long idUrbanizadora)
        {
            try
            {
                var result = await _TipoPropiedadData.VerTiposPropiedadPorUrbanizadora(idUrbanizadora);
                return BusinessResult<ICollection<TipoPropiedadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoPropiedadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Validar que el nombre de la propiedad sea unico.
        /// </summary>
        /// <param name="Nombre">Nombre del tipo de propiedad.</param>
        /// <returns>Entidad con el tipo propiedad encontrada.</returns>
        public async Task<BusinessResult<TipoPropiedadDto>> ValidarTipoPropiedadUnica(long Id, string Nombre)
        {
            try
            {
                var result = await _TipoPropiedadData.ValidarTipoPropiedadUnica(Id, Nombre);
                return BusinessResult<TipoPropiedadDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Eliminar tipo propiedad
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Eliminar(long id, string usuario)
        {

            try
            {
                await _TipoPropiedadData.Eliminar(id);
                LogHelper.Audit("Tipo Propiedad", Accion.Eliminacion.ToString(), usuario);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateException)
                {
                    if (ex.InnerException.InnerException.Message.Contains("FK_PlanFinanciacion_TipoPropiedad"))
                    {
                        return BusinessResult<bool>.Issue(true, Messages.Messages.OperacionErroneaFK, ex);
                    }
                }
                LogHelper.Audit("Tipo Propiedad", Accion.Eliminacion.ToString(),usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }


        /// <summary>
        /// Consulta la validacion del tipo de propiedad
        /// </summary>
        /// <param name="Id">Identificador de tipo de propiedad</param>
        /// <returns>Validar urbanizadora</returns>
        public async Task<BusinessResult<TipoPropiedadDto>> ValidarRelaciones(long Id)
        {
            try
            {
                TipoPropiedadDto dto = new TipoPropiedadDto();
                var validarUrbanizadora = await _TipoPropiedadData.ValidarTipoPropiedad(Id);
                if (validarUrbanizadora != null)
                {
                    if (validarUrbanizadora.Contador != 0)
                    {
                        dto.UrbanizadorasRelacionadas = validarUrbanizadora.Nombre;
                    }
                }
                var validarSolicitud = await _TipoPropiedadData.validarRelacionPlanFinanciacion(Id);
                if (validarSolicitud != null)
                {
                    dto.RelacionSolicitud = Messages.Messages.OperacionErroneaFK;
                }
                return BusinessResult<TipoPropiedadDto>.Success(dto, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }

        }

        /// <summary>
        /// Crear Tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        public async Task<BusinessResult<TipoPropiedadDto>> Crear(TipoPropiedadDto tipoPropiedad)
        {
            try
            {
                var result = await _TipoPropiedadData.Crear(tipoPropiedad);
                LogHelper.Audit("Tipo Propiedad", Accion.Creacion.ToString(), tipoPropiedad.Usuario);
                return BusinessResult<TipoPropiedadDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException.InnerException.Message.Contains("UQ_TipoPropiedad_Tipo"))
                {
                    LogHelper.Audit("Tipo Propiedad", Accion.Creacion.ToString(),tipoPropiedad.Usuario, false);
                    TipoPropiedadDto dto = new TipoPropiedadDto()
                    {
                        mensajeError = Messages.Messages.OperacionErroneaDuplicada
                    };
                    LogHelper.Audit("Tipo Propiedad", Accion.Creacion.ToString(),tipoPropiedad.Usuario, false);
                    return BusinessResult<TipoPropiedadDto>.Issue(dto, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                LogHelper.Audit("Tipo Propiedad", Accion.Creacion.ToString(),tipoPropiedad.Usuario, false);
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tipo Propiedad", Accion.Creacion.ToString(),tipoPropiedad.Usuario, false);
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Editar tipo propieda
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        public async Task<BusinessResult<TipoPropiedadDto>> Editar(TipoPropiedadDto tipoPropiedad)
        {
            try
            {
                var result = await _TipoPropiedadData.Editar(tipoPropiedad);
                LogHelper.Audit("Tipo Propiedad", Accion.Modificacion.ToString(), tipoPropiedad.Usuario);
                return BusinessResult<TipoPropiedadDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException.InnerException.Message.Contains("UQ_TipoPropiedad_Tipo"))
                {
                    TipoPropiedadDto dto = new TipoPropiedadDto()
                    {
                        mensajeError = Messages.Messages.OperacionErroneaDuplicada
                    };
                    LogHelper.Audit("Tipo Propiedad", Accion.Modificacion.ToString(),tipoPropiedad.Usuario, false);
                    return BusinessResult<TipoPropiedadDto>.Issue(dto, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                LogHelper.Audit("Tipo Propiedad", Accion.Modificacion.ToString(),tipoPropiedad.Usuario, false);
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tipo Propiedad", Accion.Modificacion.ToString(),tipoPropiedad.Usuario, false);
                return BusinessResult<TipoPropiedadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Ver consulta por filtro de tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">nombre tipo propiedad</param>
        /// <returns>Lista de tipo propiedad</returns>
        public async Task<BusinessResult<ICollection<TipoPropiedadDto>>> TipoPropiedadFiltro(string tipoPropiedad)
        {
            try
            {
                var result = await _TipoPropiedadData.TipoPropiedadFiltro(tipoPropiedad);
                return BusinessResult<ICollection<TipoPropiedadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoPropiedadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// validar el tipo de propiedad existe en urbanizadora
        /// </summary>
        /// <param name="tipoPropiedad">Identificador de tipo propiedad</param>
        /// <returns>UrbanizadoraValidarDto</returns>
        public async Task<BusinessResult<UrbanizadoraValidarDto>> ValidarTipoPropiedad(long tipoPropiedad)
        {
            try
            {
                var result = await _TipoPropiedadData.ValidarTipoPropiedad(tipoPropiedad);
                return BusinessResult<UrbanizadoraValidarDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<UrbanizadoraValidarDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método que permite inactivar o activar el tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>string</returns>
        public async Task<BusinessResult<string>> Inactivar(TipoPropiedadDto tipoPropiedad)
        {
            string datos = string.Empty;
            try
            {
                if (!tipoPropiedad.Activo)
                {
                    var validar = await _TipoPropiedadData.ValidarTipoPropiedad(tipoPropiedad.Id);
                    if (validar != null)
                    {
                        if (validar.Contador == 0)
                        {
                            var result = await _TipoPropiedadData.Inactivar(tipoPropiedad);
                        }
                        else
                        {
                            datos = validar.Nombre;
                        }
                    }
                }
                else
                {
                    var result = await _TipoPropiedadData.Inactivar(tipoPropiedad);
                }
                LogHelper.Audit("Tipo Propiedad", Accion.Inactivacion.ToString(), tipoPropiedad.Usuario);
                return BusinessResult<string>.Success(datos, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tipo Propiedad", Accion.Inactivacion.ToString(),tipoPropiedad.Usuario, false);
                return BusinessResult<string>.Issue(datos, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
