﻿/*==========================================================================
Archivo:            SucursalBusiness
Descripción:        Logica de negocio de Sucursal                    
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 5:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion


namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Sucursal
    /// </summary>
    public class SucursalBusiness : ISucursalBusiness
    {
        /// <summary>
        /// Instancia de la clase SucursalData;
        /// </summary>
        ISucursalData _SucursalData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public SucursalBusiness()
        {
            _SucursalData = new SucursalData();

        }
        /// <summary>
        /// ver todas las sucursales
        /// </summary>
        /// <returns>Lista de sucursales</returns>
        public async Task<BusinessResult<ICollection<SucursalDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _SucursalData.VerTodosGrid();
                return BusinessResult<ICollection<SucursalDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<SucursalDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// ver todas las sucursales por pais
        /// </summary>
        /// <param name="idPais"></param>
        /// <returns></returns>
        public async Task<BusinessResult<ICollection<SucursalDto>>> VerTodosPorPais(string url)
        {
            try
            {

                string[] sigla = url.Split('/');
                long idPais = 244;
                IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                if (portalPais.SuccessfulOperation)
                {
                    if (portalPais.Result != null)
                    {
                        idPais = portalPais.Result.IdPais.Value;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                        if (pais.SuccessfulOperation)
                        {
                            idPais = pais.Result;
                        }
                    }
                }


                var result = await _SucursalData.VerTodosPorPais(idPais);
                return BusinessResult<ICollection<SucursalDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<SucursalDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// ver sucursales por identificador
        /// </summary>
        /// <param name="id">Identificador de sucursales</param>
        /// <returns>Sucursal</returns>
        public async Task<BusinessResult<SucursalDto>> VerPorId(Int64 id)
        {
            try
            {
                var result = await _SucursalData.VerPorId(id);
                return BusinessResult<SucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// ver sucursales por identificador no asncrono
        /// </summary>
        /// <param name="id">Identificador de sucursales</param>
        /// <returns>Sucursal</returns>
        public BusinessResult<SucursalDto> VerPorIdNoAsincrono(Int64 id)
        {
            try
            {
                var result = _SucursalData.VerPorIdNoAsincrono(id);
                return BusinessResult<SucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// eliminar Sucursal
        /// </summary>
        /// <param name="id">Identificador de sucursal</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario)
        {
            try
            {
                await _SucursalData.Eliminar(id);
                LogHelper.Audit("Sucursal", Accion.Eliminacion.ToString(), usuario);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Sucursal", Accion.Eliminacion.ToString(), usuario, true);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Crear Sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        public async Task<BusinessResult<SucursalDto>> Crear(SucursalDto sucursal)
        {
            try
            {
                var result = await _SucursalData.Crear(sucursal);
                LogHelper.Audit("Sucursal", Accion.Creacion.ToString(), sucursal.Usuario);
                return BusinessResult<SucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Sucursal", Accion.Creacion.ToString(), sucursal.Usuario, false);
                return BusinessResult<SucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Editar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        public async Task<BusinessResult<SucursalDto>> Editar(SucursalDto sucursal)
        {
            try
            {
                var result = await _SucursalData.Editar(sucursal);
                LogHelper.Audit("Sucursal", Accion.Modificacion.ToString(), sucursal.Usuario);
                return BusinessResult<SucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Sucursal", Accion.Modificacion.ToString(), sucursal.Usuario, false);
                return BusinessResult<SucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Inactivar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Inactivar(SucursalDto sucursal)
        {
            try
            {
                var result = await _SucursalData.Inactivar(sucursal);
                LogHelper.Audit("Sucursal", Accion.Inactivacion.ToString(), sucursal.Usuario);
                return BusinessResult<bool>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Sucursal", Accion.Inactivacion.ToString(), sucursal.Usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Consultar por filtro
        /// </summary>
        /// <param name="sucursal">Sucursal filtro</param>
        /// <returns>Sucursal filtro</returns>
        public async Task<BusinessResult<ICollection<SucursalFiltroDto>>> SucursalFiltro(SucursalFiltroDto sucursal)
        {
            try
            {
                var result = await _SucursalData.SucursalFiltro(sucursal);
                return BusinessResult<ICollection<SucursalFiltroDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<SucursalFiltroDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Validar que el nombre sucursal sea unico.
        /// </summary>
        /// <param name="sucursal">Entidad sucursal.</param>
        /// <returns>Entidad sucursal encontrada.</returns>
        public async Task<BusinessResult<SucursalDto>> ValidarSucursalUnica(SucursalDto sucursal)
        {
            try
            {
                var result = await _SucursalData.ValidarSucursalUnica(sucursal);
                return BusinessResult<SucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo que se encarga de consultar todas las sucursales activas
        /// </summary>
        /// <returns>Lista de tipo sucursal</returns>
        public async Task<BusinessResult<ICollection<SucursalDto>>> VerTodosActivos()
        {
            try
            {
                var result = await _SucursalData.VerTodosActivas();
                return BusinessResult<ICollection<SucursalDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<SucursalDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}