﻿/*==========================================================================
Archivo:            IndicadorBusiness
Descripción:        Logica de negocio de Indicador         
Autor:              carlos.arboleda                          
Fecha de creación:  12/02/2016 05:12:10 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para los indicadores
    /// </summary>
    public class IndicadorBusiness : IIndicadorBusiness
    {    
        /// <summary>
        /// Instancia de la clase IndicadorData;
        /// </summary>
        IIndicadorData _IndicadorData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public IndicadorBusiness()
        {
            _IndicadorData = new IndicadorData();

        }
        /// <summary>
        /// Método encargado de consultar todas los indicadores.
        /// </summary>
        /// <returns>BusinessResult con la lista de todas los indicadores.</returns>
        public BusinessResult<ICollection<IndicadorDto>> VerTodosActivos()
        {
            try
            {
                var result =  _IndicadorData.VerTodosActivos();
                return BusinessResult<ICollection<IndicadorDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<IndicadorDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }


        /// <summary>
        /// Método encargado de consultar todas los indicadores.
        /// </summary>
        /// <returns>BusinessResult con la lista de todas los indicadores.</returns>
        public async Task<BusinessResult<ICollection<IndicadorDto>>> VerTodos()
        {
            try
            {
                var result = await _IndicadorData.VerTodos();
                return BusinessResult<ICollection<IndicadorDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<IndicadorDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Crear Indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        public async Task<BusinessResult<IndicadorDto>> Crear(IndicadorDto indicador)
        {
            try
            {
                var result = await _IndicadorData.Crear(indicador);
                LogHelper.Audit("Indicador", Accion.Creacion.ToString(), indicador.Usuario);
                return BusinessResult<IndicadorDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Indicador", Accion.Creacion.ToString(),indicador.Usuario, false);
                return BusinessResult<IndicadorDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Editar indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        public async Task<BusinessResult<IndicadorDto>> Editar(IndicadorDto indicador)
        {
            try
            {
                var result = await _IndicadorData.Editar(indicador);
                LogHelper.Audit("Indicador", Accion.Modificacion.ToString(), indicador.Usuario);
                return BusinessResult<IndicadorDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Indicador", Accion.Modificacion.ToString(), indicador.Usuario, false);
                return BusinessResult<IndicadorDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Inactivar indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Inactivar(IndicadorDto indicador)
        {
            try
            {
                var result = await _IndicadorData.Inactivar(indicador);
                LogHelper.Audit("Indicador", Accion.Inactivacion.ToString(), indicador.Usuario);
                return BusinessResult<bool>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Indicador", Accion.Inactivacion.ToString(), indicador.Usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Eliminar Indicador
        /// </summary>
        /// <param name="id">Identificador del indicador</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario)
        {
            try
            {
                await _IndicadorData.Eliminar(id);
                LogHelper.Audit("Indicador", Accion.Eliminacion.ToString(), usuario);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Indicador", Accion.Eliminacion.ToString(), usuario, true);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Validar que el nombre del indicador sea unico.
        /// </summary>
        /// <param name="indicador">Nombre del indicador.</param>
        /// <returns>Entidad con el indicador encontrado.</returns>
        public async Task<BusinessResult<IndicadorDto>> ValidarIndicadorUnico(IndicadorDto indicador)
        {
            try
            {
                var result = await _IndicadorData.ValidarIndicadorUnico(indicador);
                return BusinessResult<IndicadorDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<IndicadorDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
