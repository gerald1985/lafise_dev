﻿/*==========================================================================
Archivo:            TarjetaBusiness
Descripción:        logica de negocio de Tarjeta                      
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 12:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Business
{

    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Tarjeta
    /// </summary>
    public class TarjetaBusiness : ITarjetaBusiness
    {
        /// <summary>
        /// Instancia de la clase TarjetaData;
        /// </summary>
        ITarjetaData _TarjetaData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TarjetaBusiness()
        {
            _TarjetaData = new TarjetaData();

        }
        /// <summary>
        /// ver todas las tarjetas
        /// </summary>
        /// <returns>Lista de tarjeta</returns>
        public async Task<BusinessResult<ICollection<TarjetaDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _TarjetaData.VerTodosGrid();
                return BusinessResult<ICollection<TarjetaDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TarjetaDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// ver tarjeta por identificador
        /// </summary>
        /// <param name="id">Identificador de tarjeta</param>
        /// <returns>Tarjeta</returns>
        public async Task<BusinessResult<TarjetaDto>> VerPorId(Int64 id)
        {
            try
            {
                var result = await _TarjetaData.VerPorId(id);
                return BusinessResult<TarjetaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Consulta de tarjeta con filtro
        /// </summary>
        /// <param name="tarjetaCredito">Nombre tarjeta</param>
        /// <returns>Lista tarjeta</returns>
        public async Task<BusinessResult<ICollection<TarjetaDto>>> TarjetaFiltro(string tarjetaCredito)
        {
            try
            {
                var result = await _TarjetaData.TarjetaFiltro(tarjetaCredito);
                return BusinessResult<ICollection<TarjetaDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TarjetaDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Eliminar tarjeta
        /// </summary>
        /// <param name="id">Identificador de la tarjeta</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario)
        {
            try
            {
                await _TarjetaData.Eliminar(id);
                LogHelper.Audit("Tarjeta", Accion.Eliminacion.ToString(), usuario);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tarjeta", Accion.Eliminacion.ToString(),usuario,false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Crear tarjeta
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>Tarjeta</returns>
        public async Task<BusinessResult<TarjetaDto>> Crear(TarjetaDto tarjeta)
        {
            try
            {
                var result = await _TarjetaData.Crear(tarjeta);
                LogHelper.Audit("Tarjeta", Accion.Creacion.ToString(),tarjeta.Usuario);
                return BusinessResult<TarjetaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException.InnerException.Message.Contains("UQ_Tarjeta_TarjetaCredito"))
                {
                    LogHelper.Audit("Tarjeta", Accion.Creacion.ToString(),tarjeta.Usuario,false);
                    return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                LogHelper.Audit("Tarjeta", Accion.Creacion.ToString(),tarjeta.Usuario, false);
                return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tarjeta", Accion.Creacion.ToString(),tarjeta.Usuario, false);
                return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Editar tarjeta
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>Tarjeta</returns>
        public async Task<BusinessResult<TarjetaDto>> Editar(TarjetaDto tarjeta)
        {
            try
            {
                var result = await _TarjetaData.Editar(tarjeta);
                LogHelper.Audit("Tarjeta", Accion.Modificacion.ToString(), tarjeta.Usuario);
                return BusinessResult<TarjetaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException.InnerException.Message.Contains("UQ_Tarjeta_TarjetaCredito"))
                {
                    LogHelper.Audit("Tarjeta", Accion.Modificacion.ToString(), tarjeta.Usuario, false);
                    return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                LogHelper.Audit("Tarjeta", Accion.Modificacion.ToString(), tarjeta.Usuario, false);
                return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tarjeta", Accion.Modificacion.ToString(), tarjeta.Usuario, false);
                return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Inactivar tarjeta
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Inactivar(TarjetaDto tarjeta)
        {
            try
            {
                var result = await _TarjetaData.Inactivar(tarjeta);
                LogHelper.Audit("Tarjeta", Accion.Inactivacion.ToString(), tarjeta.Usuario);
                return BusinessResult<bool>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tarjeta", Accion.Inactivacion.ToString(), tarjeta.Usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Validar que el nombre de la tarjeta sea unico.
        /// </summary>
        /// <param name="tarjeta">Entidad tarjeta.</param>
        /// <returns>Entidad tarjeta encontrada.</returns>
        public async Task<BusinessResult<TarjetaDto>> ValidarTarjetaUnica(TarjetaDto tarjeta)
        {
            try
            {
                var result = await _TarjetaData.ValidarTarjetaUnica(tarjeta);
                return BusinessResult<TarjetaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TarjetaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
