﻿/*==========================================================================
Archivo:            ModeloAutoBusiness
Descripción:        Logica de negocio de modelo auto               
Autor:              paola.munoz                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para modelo auto
    /// </summary>
    public class ModeloAutoBusiness : IModeloAutoBusiness
    {

        /// <summary>
        /// Instancia de la interfaz IModeloAutoData;
        /// </summary>
        IModeloAutoData _ModeloAutoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ModeloAutoBusiness()
        {
            _ModeloAutoData = new ModeloAutoData();
        }

        /// <summary>
        /// Método usado para cargar todos los modelos auto
        /// </summary>
        /// <returns>Lista de modelo auto</returns>
        public async Task<BusinessResult<ICollection<ModeloAutoDto>>> VerTodos()
        {
            try
            {
                var result = await _ModeloAutoData.VerTodos();
                return BusinessResult<ICollection<ModeloAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ModeloAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo para ver todas los modelos autos por marca
        /// </summary>
        /// <param name="Marca">Id marca</param>
        /// <returns>Lista de modelos</returns>
        public async Task<BusinessResult<ICollection<ModeloAutoDto>>> VerModelosPorMarca(long Marca)
        {
            try
            {
                var result = await _ModeloAutoData.VerModelosPorMarca(Marca);
                return BusinessResult<ICollection<ModeloAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ModeloAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo encargado de ver los modelos auto por identificador
        /// </summary>
        /// <param name="id">Identificador del modelo auto</param>
        /// <returns>ModeloAutoDto</returns>
        public async Task<BusinessResult<ModeloAutoDto>> VerModeloAutoPorId(Int64 id)
        {
            try
            {
                var result = await _ModeloAutoData.VerModeloAutoPorId(id);
                return BusinessResult<ModeloAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ModeloAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo encargado de ver los modelos auto por identificador no asincrono
        /// </summary>
        /// <param name="id">Identificador del modelo auto</param>
        /// <returns>ModeloAutoDto</returns>
        public BusinessResult<ModeloAutoDto> VerModeloAutoPorIdNoAsincrono(Int64 id)
        {
            try
            {
                var result = _ModeloAutoData.VerModeloAutoPorIdNoAsincrono(id);
                return BusinessResult<ModeloAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ModeloAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
