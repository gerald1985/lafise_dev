﻿/*==========================================================================
Archivo:            TipoActividadBusiness
Descripción:        Logica de negocio de Tipo de actividad                     
Autor:              juan.hincapie                          
Fecha de creación:  06/10/2015 10:15:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;

namespace LAFISE.Business
{
    /// <summary>
    /// Logica de negocio de Tipo de actividad 
    /// </summary>
    public class TipoActividadBusiness : ITipoActividadBusiness
    {
        /// <summary>
        /// Instancia de la clase TipoActividadData;
        /// </summary>
        ITipoActividadData _TipoActividadData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TipoActividadBusiness()
        {
            _TipoActividadData = new TipoActividadData();
        }

        /// <summary>
        /// Ver todos los tipos de actividad
        /// </summary>
        /// <returns>Lista de tipo de actividad</returns>
        public async Task<BusinessResult<ICollection<TipoActividadDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _TipoActividadData.VerTodosGrid();
                return BusinessResult<ICollection<TipoActividadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoActividadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
