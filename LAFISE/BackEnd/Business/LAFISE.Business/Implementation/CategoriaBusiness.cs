﻿/*==========================================================================
Archivo:            ICategoriaBusiness
Descripción:        Logica de negocio de Categoria               
Autor:              steven.echavarria                          
Fecha de creación:  13/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para categoria
    /// </summary>
    public class CategoriaBusiness : ICategoriaBusiness
    {

        /// <summary>
        /// Instancia de la interfaz ICategoriaData;
        /// </summary>
        ICategoriaData _CategoriaData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public CategoriaBusiness()
        {
            _CategoriaData = new CategoriaData();
        }

        /// <summary>
        /// Método usado para cargar todas las categorias
        /// </summary>
        /// <param name="idTipoSolicitud">Identificador del tipo de la solicitud</param>
        /// <returns>Lista de Categoria</returns>
        public async Task<BusinessResult<ICollection<CategoriaDto>>> VerTodosIdSolicitud(Int64 idTipoSolicitud)
        {
            try
            {
                var result = await _CategoriaData.VerTodosIdSolicitud(idTipoSolicitud);
                return BusinessResult<ICollection<CategoriaDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CategoriaDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo encargado de ver la categoria por identificador
        /// </summary>
        /// <param name="id">Identificador de la categoria</param>
        /// <returns>Categoria</returns>
        public async Task<BusinessResult<CategoriaDto>> VerCategoriaPorId(Int64 id)
        {
            try
            {
                var result = await _CategoriaData.VerCategoriaPorId(id);
                return BusinessResult<CategoriaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CategoriaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo encargado de ver la categoria por identificador no asincrono
        /// </summary>
        /// <param name="id">Identificador de la categoria</param>
        /// <returns>Categoria</returns>
        public BusinessResult<CategoriaDto> VerCategoriaPorIdNoAsincrono(Int64 id)
        {
            try
            {
                var result =  _CategoriaData.VerCategoriaPorIdNoAsincrona(id);
                return BusinessResult<CategoriaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CategoriaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
