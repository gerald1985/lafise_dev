﻿/*==========================================================================
Archivo:            DepartamentoBusiness
Descripción:        Logica de negocio de Departamento                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                     
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Departamento
    /// </summary>
    public class DepartamentoBusiness : IDepartamentoBusiness
    {
        /// <summary>
        /// Instancia de la interfaz IDepartamentoData;
        /// </summary>
        IDepartamentoData _DepartamentoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public DepartamentoBusiness()
        {
            _DepartamentoData = new DepartamentoData();
        }

        /// <summary>
        /// Método usado para cargar todos los departamentos
        /// </summary>
        /// <returns>Lista de departamentos</returns>
        public async Task<BusinessResult<ICollection<DepartamentoDto>>> VerTodos()
        {
            try
            {
                var result = await _DepartamentoData.VerTodos();
                return BusinessResult<ICollection<DepartamentoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<DepartamentoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar un departamento por su id
        /// </summary>
        /// <param name="id">Identificador del departamento</param>
        /// <returns>Departamento</returns>
        public async Task<BusinessResult<DepartamentoDto>> VerDepartamentoPorId(int id)
        {
            try
            {
                var result = await _DepartamentoData.VerDepartamentoPorId(id);
                return BusinessResult<DepartamentoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<DepartamentoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todos los departamentos aociados a un pais
        /// </summary>
        /// <param name="idPais">Identificador del pais</param>
        /// <returns>Lista de departamentos</returns>
        public async Task<BusinessResult<ICollection<DepartamentoDto>>> VerDepartamentosPorPais(int idPais)
        {
            try
            {
                var result = await _DepartamentoData.VerDepartamentosPorPais(idPais);
                return BusinessResult<ICollection<DepartamentoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<DepartamentoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo para retorna una lista de departamento por una lista de paises
        /// </summary>
        /// <param name="listaPais">lista de paises</param>
        /// <returns>lista de departamento</returns>
        public async Task<BusinessResult<ICollection<DepartamentoDto>>> VerListaDepartamentoPorPais(List<Int64> listaPais)
        {
            try
            {
                var result = await _DepartamentoData.VerListaDepartamentoPorPais(listaPais);
                return BusinessResult<ICollection<DepartamentoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<DepartamentoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
