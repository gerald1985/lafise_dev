﻿/*==========================================================================
Archivo:            SolicitudBusiness
Descripción:        Logica de negocio de Solicitud                      
Autor:              paola.munoz                          
Fecha de creación:  14/10/2015 12:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Business
{

    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Solicitud
    /// </summary>
    public class SolicitudBusiness : ISolicitudBusiness
    {
        /// <summary>
        /// Instancia de la clase SolicitudData;
        /// </summary>
        ISolicitudData _SolicitudData;

        /// <summary>
        /// Instancia de la clase PrimaCategoriaPaisData;
        /// </summary>
        IPrimaCategoriaPaisData _IPrimaCategoriaPaisData;

        /// <summary>
        /// Instancia de la clase TipoPropiedadPlazoData;
        /// </summary>
        ITipoPropiedadPlazoData _ITipoPropiedadPlazoData;

        /// <summary>
        /// Instancia de la clase MarcaAutoPlazoData;
        /// </summary>
        IMarcaAutoPlazoData _IMarcaAutoPlazoData;


        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public SolicitudBusiness()
        {
            _SolicitudData = new SolicitudData();
            _IPrimaCategoriaPaisData = new PrimaCategoriaPaisData();
            _ITipoPropiedadPlazoData = new TipoPropiedadPlazoData();
            _IMarcaAutoPlazoData = new MarcaAutoPlazoData();
        }
        /// <summary>
        /// ver todas las solicitudes
        /// </summary>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<ICollection<SolicitudDto>>> VerTodos()
        {
            try
            {
                var result = await _SolicitudData.VerTodos();
                return BusinessResult<ICollection<SolicitudDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<SolicitudDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// ver por id de formulario general
        /// </summary>
        /// <param name="id">Identificador por solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<SolicitudDto>> VerFormularioGeneralPorIdSolicitud(Int64 id)
        {
            try
            {
                var result = await _SolicitudData.VerFormularioGeneralPorIdSolicitud(id);
                return BusinessResult<SolicitudDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// ver por id de formulario de seguro de vida teniendo en cuenta el id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<SolicitudDto>> VerFormularioSeguroVidaPorIdSolicitud(Int64 id)
        {
            try
            {
                var result = await _SolicitudData.VerFormularioSeguroVidaPorIdSolicitud(id);
                return BusinessResult<SolicitudDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// ver por id de formulario de reclamos teniendo en cuenta el id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<SolicitudDto>> VerFormularioReclamosPorIdSolicitud(Int64 id)
        {
            try
            {
                var result = await _SolicitudData.VerFormularioReclamosPorIdSolicitud(id);
                return BusinessResult<SolicitudDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Ver el formulario de las reclamaciones
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<SolicitudDto>> VerFormularioConReferenciasPorIdSolicitud(Int64 id)
        {
            try
            {
                var result = await _SolicitudData.VerFormularioConReferenciasPorIdSolicitud(id);
                return BusinessResult<SolicitudDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Ver información de los datos personales para el envio del correo
        /// </summary>
        /// <param name="idSolicitud">Identificador de la solicitud</param>
        /// <returns>DatosPersonalesDto</returns>
        public BusinessResult<DatosPersonalesDto> VerDatosPersonales(Int64 idSolicitud)
        {
            try
            {
                var result =  _SolicitudData.VerDatosPersonales(idSolicitud);
                return BusinessResult<DatosPersonalesDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<DatosPersonalesDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Crear solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<SolicitudDto>> CrearSolicitud(SolicitudDto solicitud)
        {
            try
            {
                solicitud.Procedencia = null;
                if (!string.IsNullOrEmpty(solicitud.PathUrl))
                {
                    if (solicitud.PathUrl.Contains(Constants.PROCEDENCIA))
                    {
                        var procedencia = solicitud.PathUrl.Split('=');
                        solicitud.Procedencia = procedencia[1];
                    }

                    string[] sigla = solicitud.PathUrl.Split('/');
                    if (sigla.Length > 3)
                    {
                        IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                        var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                        if (portalPais.SuccessfulOperation && portalPais.Result != null)
                        {
                            solicitud.IdPais = portalPais.Result.IdPais.Value;
                        }
                        else
                        {
                            IPaisBusiness _PaisBusiness = new PaisBusiness();
                            var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                            if (pais.SuccessfulOperation)
                            {
                                solicitud.IdPais = pais.Result;
                            }
                        }
                        var fecha = _PortalPaisBusiness.ObtenerFechaPortal(sigla[3]);
                        solicitud.FechaCreacion = fecha.Result;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                        if (pais.SuccessfulOperation)
                        {
                            solicitud.IdPais = pais.Result;
                        }
                    }
                }
                else
                {
                    IPaisBusiness _PaisBusiness = new PaisBusiness();
                    var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                    if (pais.SuccessfulOperation)
                    {
                        solicitud.IdPais = pais.Result;
                    }
                }

                MonedaBusiness moneda = new MonedaBusiness();
                var resultMoneda = moneda.VerMonedaPais(solicitud.IdPais);
                if (resultMoneda.SuccessfulOperation && resultMoneda.Result != null)
                {
                    solicitud.Moneda = resultMoneda.Result.Nombre;
                }

                var datosPersonales = solicitud.DatosPersonales.FirstOrDefault();
                if (solicitud.IdTipoSolicitud == 3 || solicitud.IdTipoSolicitud == 1)
                {
                    return await ValidarSolicitud(solicitud);
                }

                
                if (IsValidDate(datosPersonales.FechaNacimiento))
                {
                    LogHelper.Audit("Solicitud", Accion.Creacion.ToString(), solicitud.Usuario);
                    return await CreacionSolicitud(solicitud);
                }
                else
                {
                    SolicitudDto _mensajeDto = new SolicitudDto { MensajeValidacion = "Seleccione una Fecha de Nacimiento Válida en la seccion Datos Personales"};
                    return BusinessResult<SolicitudDto>.Success(_mensajeDto, Messages.Messages.OperacionValidacionIncorrecta);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Solicitud", Accion.Creacion.ToString(), solicitud.Usuario, false);
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }


        private static bool IsValidDate(DateTime date)
        {
            DateTime newDate = DateTime.Now;
            try
            {
                if ((newDate.Year - date.Year) > 90 || (newDate.Year - date.Year) < 12)
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Metodo encargado de validar la solicitud para el prestamo hipotecario
        /// </summary>
        /// <param name="solicitud">Entidad SolicitudDto</param>
        /// <returns>Entidad SolicitudDto</returns>
        private async Task<BusinessResult<SolicitudDto>> ValidarSolicitud(SolicitudDto solicitud)
        {
            var planFinanciacion = solicitud.PlanFinanciacion.FirstOrDefault();
            if (planFinanciacion.ConstruccionPropia != true)
            {
                var primaIngresada = planFinanciacion.PrimaAnticipo;
                PrimaCategoriaPaisDto _primaCategoriaPais = new PrimaCategoriaPaisDto { IdCategoria = Convert.ToInt64(planFinanciacion.IdCategoria), IdPais = solicitud.IdPais };
                //Obtiene el porcentaje de la prima según la categoría y el país
                var porcentajePrima = _IPrimaCategoriaPaisData.VerPorcentajePrimaPorCategoriaPais(_primaCategoriaPais);
                decimal resPorcPrima = porcentajePrima == null ? 0 : porcentajePrima.PorcentajePrima;
                var valMinPrima = planFinanciacion.ValorBien * (resPorcPrima / 100);
                if (primaIngresada >= valMinPrima)
                {
                    if (solicitud.IdTipoSolicitud == 3)
                    {
                        //Obtiene el plazo de pago según el tipo de propiedad
                        var plazoTipoPropiedad = _ITipoPropiedadPlazoData.VerPlazoPorTipoPropiedad(Convert.ToInt64(planFinanciacion.IdTipoPropiedad));
                        if (plazoTipoPropiedad != null)
                        {
                            if (planFinanciacion.Plazo > 0 && planFinanciacion.Plazo <= plazoTipoPropiedad.Plazo)
                            {
                                return await CreacionSolicitud(solicitud);
                            }
                            else
                            {
                                SolicitudDto _mensajeDto = new SolicitudDto { MensajeValidacion = "Señor usuario el valor del plazo debe ser mayor a cero (0) y menor o igual a " + plazoTipoPropiedad.Plazo + "" };
                                return BusinessResult<SolicitudDto>.Success(_mensajeDto, Messages.Messages.OperacionValidacionIncorrecta);
                            }
                        }
                        else
                        {
                            SolicitudDto _mensajeDto = new SolicitudDto { MensajeValidacion = "No existe un parámetro de plazo creado para el tipo de propiedad seleccionado" };
                            return BusinessResult<SolicitudDto>.Success(_mensajeDto, Messages.Messages.OperacionValidacionIncorrecta);
                        }
                    }
                    else
                    {
                        //Obtiene el plazo de pago según la marca de auto
                        var PlazoMarcaAuto = _IMarcaAutoPlazoData.VerPlazoPorMarcaAuto(Convert.ToInt64(planFinanciacion.IdMarcaAuto));
                        if (PlazoMarcaAuto != null)
                        {
                            if (planFinanciacion.Plazo > 0 && planFinanciacion.Plazo <= PlazoMarcaAuto.Plazo)
                            {
                                return await CreacionSolicitud(solicitud);
                            }
                            else
                            {
                                SolicitudDto _mensajeDto = new SolicitudDto { MensajeValidacion = "Señor usuario el valor del plazo debe ser mayor a cero (0) y menor o igual a " + PlazoMarcaAuto.Plazo + "" };
                                return BusinessResult<SolicitudDto>.Success(_mensajeDto, Messages.Messages.OperacionValidacionIncorrecta);
                            }
                        }
                        else
                        {
                            SolicitudDto _mensajeDto = new SolicitudDto { MensajeValidacion = "No existe un parámetro de plazo creado para la marca seleccionada" };
                            return BusinessResult<SolicitudDto>.Success(_mensajeDto, Messages.Messages.OperacionValidacionIncorrecta);
                        }
                    }
                }
                else
                {
                    SolicitudDto _mensajeDto = new SolicitudDto { MensajeValidacion = "El valor de la prima deberá ser mayor o igual a" + valMinPrima, MinimoPrima = valMinPrima.Value, TipoValidacion = Messages.Messages.OperacionErroneaPrima };
                    return BusinessResult<SolicitudDto>.Success(_mensajeDto, Messages.Messages.OperacionValidacionIncorrecta);
                }
            }
            else { return await CreacionSolicitud(solicitud); }
        }

        /// <summary>
        /// Metodo extraido del metodo CrearSolicitud para no repetir este mismo fragmento en varias partes, se encarga de crear la solicitud
        /// </summary>
        /// <param name="solicitud"></param>
        /// <returns></returns>
        private async Task<BusinessResult<SolicitudDto>> CreacionSolicitud(SolicitudDto solicitud)
        {
            try
            {
                var result = await _SolicitudData.CrearSolicitud(solicitud);
                LogHelper.Audit("Solicitud", Accion.Creacion.ToString(), solicitud.Usuario);
                return BusinessResult<SolicitudDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Solicitud", Accion.Modificacion.ToString(),solicitud.Usuario, false);
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }

        }
        /// <summary>
        /// Editar solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<SolicitudDto>> EditarSolicitud(SolicitudDto solicitud, String backOfficeUser)
        {
            try
            {

                if (!string.IsNullOrEmpty(solicitud.PathUrl))
                {
                    string[] sigla = solicitud.PathUrl.Split('/');
                    if (sigla.Length > 3)
                    {
                        IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                      
                        var fecha = _PortalPaisBusiness.ObtenerFechaPortal(sigla[3]);
                        solicitud.FechaActualizacion = fecha.Result;
                    }                    
                }
                
                var result = await _SolicitudData.EditarSolicitud(solicitud, backOfficeUser);
                LogHelper.Audit("Solicitud", Accion.Modificacion.ToString(), solicitud.Usuario);
                return BusinessResult<SolicitudDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Solicitud", Accion.Modificacion.ToString(),solicitud.Usuario, false);
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Ver la gestion de las solicitudes
        /// </summary>
        /// <param name="solicitudCondicion">Identificador de los estados concatenados por coma</param>
        /// <returns>Solicitud Filtro</returns>
        public async Task<BusinessResult<ICollection<SolicitudFiltroDto>>> VerGestorSolicitud(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.VerGestorSolicitud(solicitudCondicion);
                return BusinessResult<ICollection<SolicitudFiltroDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<SolicitudFiltroDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// ver por id de formulario de seguro de vehicular teniendo en cuenta el id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<BusinessResult<SolicitudDto>> VerFormularioSeguroVehicularPorIdSolicitud(Int64 id)
        {
            try
            {
                var result = await _SolicitudData.VerFormularioSeguroVehicularPorIdSolicitud(id);
                return BusinessResult<SolicitudDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SolicitudDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar marca auto
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarMarcaAutoDto</returns>
        public async Task<BusinessResult<ICollection<ExportarMarcaAutoDto>>> CargarExportarMarcaAuto(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarMarcaAuto(solicitudCondicion);
                return BusinessResult<ICollection<ExportarMarcaAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarMarcaAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar prestamo personal
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarPrestamoPersonalDto</returns>
        public async Task<BusinessResult<ICollection<ExportarPrestamoPersonalDto>>> CargarExportarPrestamoPersonal(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarPrestamoPersonal(solicitudCondicion);
                return BusinessResult<ICollection<ExportarPrestamoPersonalDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarPrestamoPersonalDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar prestamo hipotecario
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarPrestamoHipotecarioDto</returns>
        public async Task<BusinessResult<ICollection<ExportarPrestamoHipotecarioDto>>> CargarExportarPrestamoHipotecario(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarPrestamoHipotecario(solicitudCondicion);
                return BusinessResult<ICollection<ExportarPrestamoHipotecarioDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarPrestamoHipotecarioDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar tarjeta credito o debito
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarTarjetaCreditoDebitoDto</returns>
        public async Task<BusinessResult<ICollection<ExportarTarjetaCreditoDebitoDto>>> CargarExportarTarjetaCreditoDebito(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarTarjetaCreditoDebito(solicitudCondicion);
                return BusinessResult<ICollection<ExportarTarjetaCreditoDebitoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarTarjetaCreditoDebitoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar prestamo educativo
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarPrestamoEducativoDto</returns>
        public async Task<BusinessResult<ICollection<ExportarPrestamoEducativoDto>>> CargarExportarPrestamoEducativo(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarPrestamoEducativo(solicitudCondicion);
                return BusinessResult<ICollection<ExportarPrestamoEducativoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarPrestamoEducativoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar apertura de cuenta
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarAperturaCuentaDto</returns>
        public async Task<BusinessResult<ICollection<ExportarAperturaCuentaDto>>> CargarExportarAperturaCuenta(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarAperturaCuenta(solicitudCondicion);
                return BusinessResult<ICollection<ExportarAperturaCuentaDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarAperturaCuentaDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar seguro de vida y accidente
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarSeguroVidaAccidenteDto</returns>
        public async Task<BusinessResult<ICollection<ExportarSeguroVidaAccidenteDto>>> CargarExportarSeguroVidaAccidente(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarSeguroVidaAccidente(solicitudCondicion);
                return BusinessResult<ICollection<ExportarSeguroVidaAccidenteDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarSeguroVidaAccidenteDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar seguro vehicular
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarSeguroVehicularDto</returns>
        public async Task<BusinessResult<ICollection<ExportarSeguroVehicularDto>>> CargarExportarSeguroVehicular(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarSeguroVehicular(solicitudCondicion);
                return BusinessResult<ICollection<ExportarSeguroVehicularDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarSeguroVehicularDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Exportar reclamos
        /// </summary>
        /// <param name="solicitudCondicion">Filtros para exportar</param>
        /// <returns>ExportarReclamosDto</returns>
        public async Task<BusinessResult<ICollection<ExportarReclamosDto>>> CargarExportarReclamos(SolicitudCondicionesDto solicitudCondicion)
        {
            try
            {
                var result = await _SolicitudData.CargarExportarReclamos(solicitudCondicion);
                return BusinessResult<ICollection<ExportarReclamosDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ExportarReclamosDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

    }
}
