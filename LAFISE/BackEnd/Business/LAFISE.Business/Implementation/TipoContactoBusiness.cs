﻿/*==========================================================================
Archivo:            TipoContactoBusiness
Descripción:        Logica de negocio de TipoContacto               
Autor:              paola.munoz                         
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para TipoContacto
    /// </summary>
    public class TipoContactoBusiness : ITipoContactoBusiness
    {

        /// <summary>
        /// Instancia de la interfaz ITipoContactoData;
        /// </summary>
        ITipoContactoData _TipoContactoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public TipoContactoBusiness()
        {
            _TipoContactoData = new TipoContactoData();
        }

        /// <summary>
        /// Método usado para cargar los TipoContacto 
        /// </summary>
        /// <returns>Lista de TipoContacto</returns>
        public async Task<BusinessResult<ICollection<TipoContactoDto>>> VerTodos()
        {
            try
            {
                var result = await _TipoContactoData.VerTodos();
                return BusinessResult<ICollection<TipoContactoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoContactoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
