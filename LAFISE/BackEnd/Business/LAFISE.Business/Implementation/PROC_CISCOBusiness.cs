﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using LAFISE.Data;

namespace LAFISE.Business
{
    public class PROC_CISCOBusiness : IPROC_CISCOBusiness
    {
        IPROC_CISCO_CiudadesData _PROC_CISCO_CiudadesData;
        IPROC_CISCO_PaisesData _PROC_CISCO_PaisesData;
        IPROC_CISCO_OcupacionesData _PROC_CISCO_OcupacionesData;
        IPROC_CISCO_Centro_De_CostoData _PROC_CISCO_Centro_De_CostoData;

        public PROC_CISCOBusiness()
        {
            _PROC_CISCO_CiudadesData = new PROC_CISCO_CiudadesData();
            _PROC_CISCO_PaisesData = new PROC_CISCO_PaisesData();
            _PROC_CISCO_Centro_De_CostoData = new PROC_CISCO_Centro_De_CostoData();
            _PROC_CISCO_OcupacionesData = new PROC_CISCO_OcupacionesData();
        }

        public async Task<BusinessResult<ICollection<PROC_CISCO_PaisesDto>>> VerPaisesPROCTodos()
        {
            try
            {
                var result = await _PROC_CISCO_PaisesData.VerTodos();
                return BusinessResult<ICollection<PROC_CISCO_PaisesDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<PROC_CISCO_PaisesDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        public async Task<BusinessResult<ICollection<PROC_CISCO_CiudadesDto>>> VerCiudadesPROCTodos()
        {
            try
            {
                var result = await _PROC_CISCO_CiudadesData.VerTodos();
                return BusinessResult<ICollection<PROC_CISCO_CiudadesDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<PROC_CISCO_CiudadesDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        public async Task<BusinessResult<ICollection<PROC_CISCO_OcupacionesDto>>> VerOcupacionesPROCTodos()
        {
            try
            {
                var result = await _PROC_CISCO_OcupacionesData.VerTodos();
                return BusinessResult<ICollection<PROC_CISCO_OcupacionesDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<PROC_CISCO_OcupacionesDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        public async Task<BusinessResult<ICollection<PROC_CISCO_Centro_De_CostoDto>>> VerSucursalesPROCTodos()
        {
            try
            {
                var result = await _PROC_CISCO_Centro_De_CostoData.VerTodos();
                return BusinessResult<ICollection<PROC_CISCO_Centro_De_CostoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<PROC_CISCO_Centro_De_CostoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
