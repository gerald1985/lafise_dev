﻿/*==========================================================================
Archivo:            SuscripcionBoletinBusiness
Descripción:        Logica de negocio de SuscripcionBoletin                  
Autor:              steven.echavarria                          
Fecha de creación:  02/23/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                       
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para suscripción a boletines
    /// </summary>
    public class SuscripcionBoletinBusiness : ISuscripcionBoletinBusiness
    {

        /// <summary>
        /// Nombre del módulo que se usará en el log
        /// </summary>
        public const string ModuloSuscripcionBoletin = "Suscripción Boletin";

        /// <summary>
        /// Instancia de la interfaz ISuscripcionBoletinData;
        /// </summary>
        ISuscripcionBoletinData _SuscripcionBoletinData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public SuscripcionBoletinBusiness()
        {
            _SuscripcionBoletinData = new SuscripcionBoletinData();
        }

        /// <summary>
        /// Crear suscripción a boletin
        /// </summary>
        /// <param name="suscripcion">SuscripcionBoletin</param>
        /// <returns>SuscripcionBoletin</returns>
        public async Task<BusinessResult<SuscripcionBoletinDto>> Crear(SuscripcionBoletinDto suscripcion)
        {
            try
            {
                VerPais(suscripcion);
                var result = await _SuscripcionBoletinData.Crear(suscripcion);
                LogHelper.Audit(ModuloSuscripcionBoletin, Accion.Creacion.ToString());
                return BusinessResult<SuscripcionBoletinDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloSuscripcionBoletin, Accion.Creacion.ToString(), suscripcion.Usuario, false);
                return BusinessResult<SuscripcionBoletinDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }        

        /// <summary>
        /// Metodo que valida que el email ya esté registrado en las suscripciones
        /// </summary>
        /// <param name="suscripcion">entidad suscripcion</param>
        /// <returns>suscripcion</returns>
        public async Task<BusinessResult<SuscripcionBoletinDto>> ValidarSuscripcionPorEmail(SuscripcionBoletinDto suscripcion)
        {
            try
            {
                VerPais(suscripcion);
                var result = await _SuscripcionBoletinData.ValidarSuscripcionPorEmail(suscripcion);
                return BusinessResult<SuscripcionBoletinDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SuscripcionBoletinDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo que consulta el país del usuario
        /// </summary>
        /// <param name="suscripcion"></param>
        private static void VerPais(SuscripcionBoletinDto suscripcion)
        {
            if (!string.IsNullOrEmpty(suscripcion.PathUrl))
            {
                string[] sigla = suscripcion.PathUrl.Split('/');
                if (sigla.Length > 3)
                {
                    IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                    var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                    if (portalPais.SuccessfulOperation && portalPais.Result != null)
                    {
                        suscripcion.IdPais = portalPais.Result.IdPais.Value;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                        if (pais.SuccessfulOperation)
                        {
                            suscripcion.IdPais = pais.Result;
                        }
                    }
                    var fecha = _PortalPaisBusiness.ObtenerFechaPortal(sigla[3]);
                    suscripcion.FechaCreacion = fecha.Result;
                }
                else
                {
                    IPaisBusiness _PaisBusiness = new PaisBusiness();
                    var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                    if (pais.SuccessfulOperation)
                    {
                        suscripcion.IdPais = pais.Result;
                    }
                }
            }
            else
            {
                IPaisBusiness _PaisBusiness = new PaisBusiness();
                var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                if (pais.SuccessfulOperation)
                {
                    suscripcion.IdPais = pais.Result;
                }
            }
        }
    }
}
