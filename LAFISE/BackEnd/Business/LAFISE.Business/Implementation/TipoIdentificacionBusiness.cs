﻿/*==========================================================================
Archivo:            TipoIdentificacionBusiness
Descripción:        Logica de negocio de TipoIdentificacion                  
Autor:              paola.munoz                          
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    public class TipoIdentificacionBusiness: ITipoIdentificacionBusiness
    {
        /// <summary>
        /// Instancia de la interfaz ITipoIdentificacionData;
        /// </summary>
        ITipoIdentificacionData _TipoIdentificacionData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TipoIdentificacionBusiness()
        {
            _TipoIdentificacionData = new TipoIdentificacionData();
        }

        /// <summary>
        /// Método usado para cargar todos los tipo de identificacion
        /// </summary>
        /// <returns>Lista de tipoIdentificacion</returns>
        public async Task<BusinessResult<ICollection<TipoIdentificacionDto>>> VerTodos()
        {
            try
            {
                var result = await _TipoIdentificacionData.VerTodos();
                return BusinessResult<ICollection<TipoIdentificacionDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoIdentificacionDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar el tipo de identificacion
        /// </summary>
        /// <returns>tipoIdentificacion</returns>
        public BusinessResult<TipoIdentificacionDto> VerPorIdNoAsincrono(Int64 id)
        {
            try
            {
                var result = _TipoIdentificacionData.VerPorIdNoAsincrono(id);
                return BusinessResult<TipoIdentificacionDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoIdentificacionDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar los tipos de identificación teniendo en cuenta el identificador de país
        /// </summary>
        /// <param name="path">url del sitio</param>
        /// <returns>Lista de tipoIdentificacion</returns>
        public async Task<BusinessResult<ICollection<TipoIdentificacionDto>>> VerTipoIdentificacionPorIdPais(string path)
        {
            try
            {
                Int64 idPais = 0;
                string[] sigla = path.Split('/');
                if (sigla.Length > 3)
                {
                    IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                    var portalPais =  _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                    if (portalPais.SuccessfulOperation && portalPais.Result != null)
                    {
                       idPais = portalPais.Result.IdPais.Value;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        idPais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO).Result;
                    }
                }
                else
                {
                    IPaisBusiness _PaisBusiness = new PaisBusiness();
                    idPais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO).Result;
                }


                var result = await _TipoIdentificacionData.VerTipoIdentificacionPorIdPais(idPais);
                return BusinessResult<ICollection<TipoIdentificacionDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoIdentificacionDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo utilizado para optener mascara de entrada por identificacion
        /// </summary>
        /// <param name="id">id identificacion</param>
        /// <returns>mascara de entrada</returns>
        public async Task<BusinessResult<String>> obtenerMascaraPorIdentificacion(Int64 id)
        {
            try
            {
                var result = await _TipoIdentificacionData.obtenerMascaraPorIdentificacion(id);
                return BusinessResult<String>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<String>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
