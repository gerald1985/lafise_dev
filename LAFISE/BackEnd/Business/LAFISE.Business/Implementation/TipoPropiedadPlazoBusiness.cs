﻿/*==========================================================================
Archivo:            TipoPropiedadPlazoBusiness
Descripción:        Logica de negocio TipoPropiedadPlazo                    
Autor:              steven.echavarria                          
Fecha de creación:  18/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para la consulta de plazo por tipo de propiedad
    /// </summary>
    public class TipoPropiedadPlazoBusiness : ITipoPropiedadPlazoBusiness
    {

        /// <summary>
        /// Instancia de la clase TipoPropiedadPlazoData;
        /// </summary>
        ITipoPropiedadPlazoData _TipoPropiedadPlazoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TipoPropiedadPlazoBusiness()
        {
            _TipoPropiedadPlazoData = new TipoPropiedadPlazoData();
        }

        /// <summary>
        /// Método encargado de consultar el plazo de un tipo de propiedad
        /// </summary>
        /// <param name="idTipoPropiedad">identificador del tipo de propiedad</param>
        /// <returns>Entidad TipoPropiedadPlazoDto</returns>
        public BusinessResult<TipoPropiedadPlazoDto> VerPlazoPorTipoPropiedad(long idTipoPropiedad)
        {
            try
            {
                var result = _TipoPropiedadPlazoData.VerPlazoPorTipoPropiedad(idTipoPropiedad);
                return BusinessResult<TipoPropiedadPlazoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoPropiedadPlazoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
