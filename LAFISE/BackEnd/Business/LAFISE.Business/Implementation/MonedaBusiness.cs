﻿/*==========================================================================
Archivo:            MonedaBusiness
Descripción:        Logica de negocio de Moneda                   
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para moneda
    /// </summary>
    public class MonedaBusiness : IMonedaBusiness
    {
        /// <summary>
        /// Instancia de la interfaz IMonedaData;
        /// </summary>
        IMonedaData _MonedaData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public MonedaBusiness()
        {
            _MonedaData = new MonedaData();
        }

        /// <summary>
        /// Método usado para cargar todas las monedas
        /// </summary>
        /// <returns>Lista de monedas</returns>
        public async Task<BusinessResult<ICollection<MonedaDto>>> VerTodos()
        {
            try
            {
                var result = await _MonedaData.VerTodos();
                return BusinessResult<ICollection<MonedaDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<MonedaDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }


        /// <summary>
        /// Método usado para cargar todas las monedas
        /// </summary>
        /// <returns>Lista de monedas</returns>
        public BusinessResult<MonedaDto> VerMonedaPais(long idPais)
        {
            try
            {
                var result =  _MonedaData.VerMonedaPais(idPais);
                return BusinessResult<MonedaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<MonedaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }


        /// <summary>
        /// Método usado para cargar una moneda por su id
        /// </summary>
        /// <param name="id">Identificador de la moneda</param>
        /// <returns>Moneda</returns>
        public async Task<BusinessResult<MonedaDto>> VerMonedaPorId(int id)
        {
            try
            {
                var result = await _MonedaData.VerMonedaPorId(id);
                return BusinessResult<MonedaDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<MonedaDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Obtener el simbolo de la moneda
        /// </summary>
        /// <param name="siglaPortal">Sigla relacionada con el portal</param>
        /// <returns>Texto</returns>
        public BusinessResult<string> ObtenerSimboloMoneda(string siglaPortal)
        {
            try
            {
                var result = _MonedaData.ObtenerSimboloMoneda(siglaPortal);
                return BusinessResult<string>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<string>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
