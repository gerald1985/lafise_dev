﻿/*==========================================================================
Archivo:            CiudadBusiness
Descripción:        Logica de negocio de Ciudad                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para las Ciudad
    /// </summary>
    public class CiudadBusiness: ICiudadBusiness
    {

        /// <summary>
        /// Instancia de la interfaz ICiudadData;
        /// </summary>
        ICiudadData _CiudadData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public CiudadBusiness()
        {
            _CiudadData = new CiudadData();
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades
        /// </summary>
        /// <returns>Lista de ciudades</returns>
        public async Task<BusinessResult<ICollection<CiudadDto>>> VerTodos()
        {
            try
            {
                var result = await _CiudadData.VerTodos();
                return BusinessResult<ICollection<CiudadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CiudadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar una ciudad por su id
        /// </summary>
        /// <param name="id">Identificador de la ciudad</param>
        /// <returns>Ciudad</returns>
        public async Task<BusinessResult<CiudadDto>> VerCiudadPorId(int id)
        {
            try
            {
                var result = await _CiudadData.VerCiudadPorId(id);
                return BusinessResult<CiudadDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CiudadDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades asociadas un departamento
        /// </summary>
        /// <param name="idDepartamento">Identificador del departamento</param>
        /// <returns>Lista de ciudad</returns>
        public async Task<BusinessResult<ICollection<CiudadDto>>> VerCiudadesPorDepartamento(int idDepartamento)
        {
            try
            {
                var result = await _CiudadData.VerCiudadesPorDepartamento(idDepartamento);
                return BusinessResult<ICollection<CiudadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CiudadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades asociadas un país
        /// </summary>
        /// <param name="listaPais">lista con el identificador de pais</param>
        /// <returns>Lista de ciudad</returns>
        public async Task<BusinessResult<ICollection<CiudadDto>>> VerCiudadesPorPais(List<Int64> listaPais)
        {
            try
            {
                var result = await _CiudadData.VerCiudadesPorPais(listaPais);
                return BusinessResult<ICollection<CiudadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CiudadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades asociadas un departamento
        /// </summary>
        /// <param name="listaDepartamento">lista con el identificador de departamento</param>
        /// <returns>Lista de ciudad</returns>
        public async Task<BusinessResult<ICollection<CiudadDto>>> VerListaCiudadesPorDepartamento(List<Int64> listaDepartamento)
        {
            try
            {
                var result = await _CiudadData.VerListaCiudadesPorDepartamento(listaDepartamento);
                return BusinessResult<ICollection<CiudadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CiudadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades asociadas una lista no asincrono
        /// </summary>
        /// <param name="listaCiudad">lista con el identificador de ciudad</param>
        /// <returns>Lista de ciudad</returns>
        public BusinessResult<ICollection<CiudadDto>> VerCiudadesPorCiudadNoAsincrono(List<Int64> listaCiudad)
        {
            try
            {
                var result =  _CiudadData.VerCiudadesPorCiudadNoAsincrono(listaCiudad);
                return BusinessResult<ICollection<CiudadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CiudadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
