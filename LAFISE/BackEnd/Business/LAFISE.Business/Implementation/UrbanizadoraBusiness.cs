﻿/*==========================================================================
Archivo:            UrbanizadoraBusiness
Descripción:        Logica de negocio de Urbanizadora                  
Autor:              steven.echavarria                          
Fecha de creación:  30/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para urbanizadora
    /// </summary>
    public class UrbanizadoraBusiness : IUrbanizadoraBusiness
    {
        /// <summary>
        /// Nombre del módulo que se usará en el log
        /// </summary>
        public const string ModuloUrbanizadora = "Urbanizadora";

        /// <summary>
        /// Instancia de la interfaz IUrbanizadoraData;
        /// </summary>
        IUrbanizadoraData _UrbanizadoraData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public UrbanizadoraBusiness()
        {
            _UrbanizadoraData = new UrbanizadoraData();
        }

        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activas
        /// </summary>
        /// <returns>Lista urbanizadora</returns>
        public async Task<BusinessResult<ICollection<UrbanizadoraDto>>> VerTodosActivos()
        {
            try
            {
                var result = await _UrbanizadoraData.VerTodosActivos();
                return BusinessResult<ICollection<UrbanizadoraDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<UrbanizadoraDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activas y por país
        /// </summary>
        /// <param name="pathIdPais">identificador del país</param>
        /// <returns></returns>
        public async Task<BusinessResult<ICollection<UrbanizadoraDto>>> VerTodosActivosPorPais(string pathIdPais)
        {
            try
            {
                Int64 idPais = 0;
                string[] sigla = pathIdPais.Split('/');
                IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                if (portalPais.SuccessfulOperation)
                {
                    if (portalPais.Result != null)
                    {
                        idPais = portalPais.Result.IdPais.Value; 
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                        if (pais.SuccessfulOperation)
                        {
                            idPais = pais.Result;
                        }
                    }
                }
                var result = await _UrbanizadoraData.VerTodosActivosPorPais(idPais);
                return BusinessResult<ICollection<UrbanizadoraDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<UrbanizadoraDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Consultar urbanizadora por tipo propiedad
        /// </summary>
        /// <param name="urbanizadoraTipoPropiedad">UrbanizadoraTipoPropiedadDto</param>
        /// <returns>Lista UrbanizadoraTipoPropiedadDto</returns>
        public async Task<BusinessResult<ICollection<UrbanizadoraTipoPropiedadDto>>> VerUrbanizadorasTiposPropiedades(UrbanizadoraTipoPropiedadDto urbanizadoraTipoPropiedad)
        {
            try
            {
                var result = await _UrbanizadoraData.VerUrbanizadorasTiposPropiedades(urbanizadoraTipoPropiedad);
                return BusinessResult<ICollection<UrbanizadoraTipoPropiedadDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<UrbanizadoraTipoPropiedadDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo para consultar urbanizadora por identificador
        /// </summary>
        /// <param name="id">Identificador de la urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<BusinessResult<UrbanizadoraDto>> VerUrbanizadoraPorId(Int64 id)
        {
            try
            {
                var result = await _UrbanizadoraData.VerUrbanizadoraPorId(id);
                return BusinessResult<UrbanizadoraDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo para consultar urbanizadora por identificador no asincrona
        /// </summary>
        /// <param name="id">Identificador de la urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public BusinessResult<UrbanizadoraDto> VerUrbanizadoraPorIdNoAsincrono(Int64 id)
        {
            try
            {
                var result = _UrbanizadoraData.VerUrbanizadoraPorIdNoAsincrono(id);
                return BusinessResult<UrbanizadoraDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo que valida que el nombre de la urbanizadora sea unico.
        /// </summary>
        /// <param name="urbanizadora">entidad urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<BusinessResult<UrbanizadoraDto>> ValidarUrbanizadoraPorNombre(UrbanizadoraDto urbanizadora)
        {
            try
            {
                var result = await _UrbanizadoraData.ValidarUrbanizadoraPorNombre(urbanizadora);
                return BusinessResult<UrbanizadoraDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Eliminar urbanizadora
        /// </summary>
        /// <param name="id">Identificador de urbanizadora</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<UrbanizadoraDto>> EliminarUrbanizadoraPorId(int id, string usuario)
        {
            try
            {
                var result = await _UrbanizadoraData.EliminarUrbanizadoraPorId(id);
                UrbanizadoraDto _mensajeDto = new UrbanizadoraDto { mensajeError = Messages.Messages.OperacionCorrecta };
                LogHelper.Audit(ModuloUrbanizadora, Accion.Eliminacion.ToString(), usuario);
                return BusinessResult<UrbanizadoraDto>.Success(_mensajeDto, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Eliminacion.ToString(), usuario, false);
                if (ex.InnerException.InnerException.Message.Contains("FK_PlanFinanciacion_Urbanizadora"))
                {
                    UrbanizadoraDto _mensajeDto = new UrbanizadoraDto { mensajeError = Messages.Messages.OperacionErroneaFK };
                    return BusinessResult<UrbanizadoraDto>.Issue(_mensajeDto, Messages.Messages.OperacionErroneaFK, ex);
                }
                else
                {
                    UrbanizadoraDto _mensajeDto = new UrbanizadoraDto { mensajeError = Messages.Messages.OperacionErronea };
                    return BusinessResult<UrbanizadoraDto>.Issue(_mensajeDto, Messages.Messages.OperacionErronea, ex);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Eliminacion.ToString(), usuario, false);
                UrbanizadoraDto _mensajeDto = new UrbanizadoraDto { mensajeError = Messages.Messages.OperacionErronea };
                return BusinessResult<UrbanizadoraDto>.Issue(_mensajeDto, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Crear urbanizadora
        /// </summary>
        /// <param name="urbanizadora">urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<BusinessResult<UrbanizadoraDto>> CrearUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            try
            {
                var result = await _UrbanizadoraData.CrearUrbanizadora(urbanizadora);
                LogHelper.Audit(ModuloUrbanizadora, Accion.Creacion.ToString(), urbanizadora.Usuario);
                return BusinessResult<UrbanizadoraDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Creacion.ToString(), urbanizadora.Usuario, false);
                if (ex.InnerException.InnerException.Message.Contains("UQ_Urbanizadora_Nombre"))
                {
                    UrbanizadoraDto _mensajeDto = new UrbanizadoraDto { mensajeError = Messages.Messages.OperacionErroneaDuplicada };
                    return BusinessResult<UrbanizadoraDto>.Issue(_mensajeDto, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                else { return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex); }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Creacion.ToString(), urbanizadora.Usuario, false);
                return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Editar urbanizadora
        /// </summary>
        /// <param name="urbanizadora">urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<BusinessResult<UrbanizadoraDto>> EditarUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            try
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Modificacion.ToString(), urbanizadora.Usuario);
                var result = await _UrbanizadoraData.EditarUrbanizadora(urbanizadora);
                return BusinessResult<UrbanizadoraDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Modificacion.ToString(), urbanizadora.Usuario, false);
                if (ex.InnerException.InnerException.Message.Contains("UQ_Urbanizadora_Nombre"))
                {
                    UrbanizadoraDto _mensajeDto = new UrbanizadoraDto { mensajeError = Messages.Messages.OperacionErroneaDuplicada };
                    return BusinessResult<UrbanizadoraDto>.Issue(_mensajeDto, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                else { return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex); }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Modificacion.ToString(), urbanizadora.Usuario, false);
                return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método que permite inactivar una urbanizadora
        /// </summary>
        /// <param name="urbanizadora">urbanizadora</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<UrbanizadoraDto>> InactivarUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            try
            {

                var existePlanFinaUrba = await _UrbanizadoraData.ExistePlanFinanciacionPorUrbanizadora(urbanizadora.Id);
                if (!existePlanFinaUrba)
                {
                    LogHelper.Audit(ModuloUrbanizadora, Accion.Inactivacion.ToString(), urbanizadora.Usuario);
                    var result = await _UrbanizadoraData.InactivarUrbanizadora(urbanizadora);
                    return BusinessResult<UrbanizadoraDto>.Success(result, Messages.Messages.OperacionCorrecta);
                }
                else
                {
                    LogHelper.Audit(ModuloUrbanizadora, Accion.Inactivacion.ToString(), urbanizadora.Usuario, false);
                    UrbanizadoraDto _mensajeDto = new UrbanizadoraDto { mensajeError = Messages.Messages.OperacionErroneaFK };
                    return BusinessResult<UrbanizadoraDto>.Success(_mensajeDto, Messages.Messages.OperacionErroneaDuplicada);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Inactivacion.ToString(), urbanizadora.Usuario, false);
                return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Eliminar urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de la urbanizadora</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> EliminarDetalleUrbanizadora(long idUrbanizadora, string usuario)
        {
            try
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Eliminacion.ToString(), usuario);
                var result = await _UrbanizadoraData.EliminarDetalleUrbanizadora(idUrbanizadora);
                return BusinessResult<bool>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloUrbanizadora, Accion.Eliminacion.ToString(), usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Consultar el detalle de la urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificar urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<BusinessResult<UrbanizadoraDto>> ConsultarDetalleUrbanizadora(long idUrbanizadora)
        {
            try
            {
                var result = await _UrbanizadoraData.ConsultarDetalleUrbanizadora(idUrbanizadora);
                return BusinessResult<UrbanizadoraDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<UrbanizadoraDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
