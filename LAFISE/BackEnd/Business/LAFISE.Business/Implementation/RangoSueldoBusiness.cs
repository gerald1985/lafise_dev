/*==========================================================================
Archivo:            RangoSueldoBusiness
Descripción:        Logica de negocio RangoSueldo                 
Autor:              paola.munoz                          
Fecha de creación:  29/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para rango sueldo
    /// </summary>
    public class RangoSueldoBusiness : IRangoSueldoBusiness
    {
        /// <summary>
        /// Instancia de la clase RangoSueldoData;
        /// </summary>
        IRangoSueldoData _RangoSueldoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public RangoSueldoBusiness()
        {
            _RangoSueldoData = new RangoSueldoData();
        }

        /// <summary>
        /// Consultar todos los rango sueldo
        /// </summary>
        /// <returns>Lista de rango sueldo</returns>
        public async Task<BusinessResult<ICollection<RangoSueldoDto>>> VerTodos()
        {
            try
            {
                var result = await _RangoSueldoData.VerTodos();
                return BusinessResult<ICollection<RangoSueldoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<RangoSueldoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Consultar el rango sueldo por identificador 
        /// </summary>
        /// <param name="id">Identificador de rango sueldo</param>
        /// <returns>rango sueldo</returns>
        public async Task<BusinessResult<RangoSueldoDto>> VerPorId(Int64 id)
        {
            try
            {
                var result = await _RangoSueldoData.VerPorId(id);

                return BusinessResult<RangoSueldoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<RangoSueldoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Consultar el detalle por el identificador del rango
        /// </summary>
        /// <param name="id">identificador de rango sueldo</param>
        /// <returns>rango sueldo detalle</returns>
        public async Task<BusinessResult<ICollection<RangoSueldoDto>>> ConsultarDetalleRangoSueldo(long id)
        {
            try
            {
                var result = await _RangoSueldoData.ConsultarDetalleRangoSueldo(id);

                return BusinessResult<ICollection<RangoSueldoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<RangoSueldoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Editar rango sueldo
        /// </summary>
        /// <param name="rangoSueldo">rango sueldo y rango sueldo detalle</param>
        /// <returns>rango sueldo</returns>
        public async Task<BusinessResult<RangoSueldoDto>> Editar(RangoSueldoDto rangoSueldo)
        {
            try
            {
                var result = await _RangoSueldoData.Editar(rangoSueldo);
                if (result == null)
                {
                    LogHelper.Audit("Rango Sueldo", Accion.Modificacion.ToString(),rangoSueldo.Usuario, false);
                    return BusinessResult<RangoSueldoDto>.Success(result, Messages.Messages.OperacionErronea);
                }
                LogHelper.Audit("Rango Sueldo", Accion.Modificacion.ToString(),rangoSueldo.Usuario);
                return BusinessResult<RangoSueldoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Rango Sueldo", Accion.Modificacion.ToString(),rangoSueldo.Usuario, false);
                return BusinessResult<RangoSueldoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Crear rango sueldo
        /// </summary>
        /// <param name="rangoSueldo">Rango sueldo</param>
        /// <returns>rango sueldo</returns>
        public async Task<BusinessResult<RangoSueldoDto>> Crear(RangoSueldoDto rangoSueldo)
        {
            try
            {
                var result = await _RangoSueldoData.Crear(rangoSueldo);
                LogHelper.Audit("Rango Sueldo", Accion.Creacion.ToString(),rangoSueldo.Usuario);
                return BusinessResult<RangoSueldoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Rango Sueldo", Accion.Creacion.ToString(),rangoSueldo.Usuario, false);
                return BusinessResult<RangoSueldoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }


        /// <summary>
        /// Ver consulta por filtro
        /// </summary>
        /// <param name="pais">Identificadores de país concatenado por coma</param>
        /// <returns>Lista rango sueldo filtro</returns>
        public async Task<BusinessResult<ICollection<RangoSueldoFiltroDto>>> VerRangoSueldoFiltro(string pais)
        {
            try
            {
                var result = await _RangoSueldoData.VerRangoSueldoFiltro(pais);
                return BusinessResult<ICollection<RangoSueldoFiltroDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<RangoSueldoFiltroDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Ver consulta por pais
        /// </summary>
        /// <param name="PathUrl">Url</param>
        /// <returns>Lista rango sueldo por pais</returns>
        public async Task<BusinessResult<ICollection<RangoSueldoPaisDto>>> VerRangoSueldoPais(string PathUrl)
        {
            try
            {
                long IdPais = 0;
                if (!string.IsNullOrEmpty(PathUrl))
                {
                    string[] sigla = PathUrl.Split('/');
                    if (sigla.Length > 3)
                    {
                        IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                        var portalPais = await _PortalPaisBusiness.ObtenerPaisPorPortal(sigla[3]);
                        if (portalPais.SuccessfulOperation)
                        {
                            IdPais = portalPais.Result.IdPais.Value;
                        }
                        else
                        {
                            IPaisBusiness _PaisBusiness = new PaisBusiness();
                            var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                            if (pais.SuccessfulOperation)
                            {
                                IdPais = pais.Result;
                            }
                        }
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                        if (pais.SuccessfulOperation)
                        {
                            IdPais = pais.Result;
                        }
                    }
                }
                else
                {
                    IPaisBusiness _PaisBusiness = new PaisBusiness();
                    var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                    if (pais.SuccessfulOperation)
                    {
                        IdPais = pais.Result;
                    }
                }

                var result = await _RangoSueldoData.VerRangoSueldoPais(IdPais);
                return BusinessResult<ICollection<RangoSueldoPaisDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<RangoSueldoPaisDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

    }
}