﻿/*==========================================================================
Archivo:            PortalPaisBusiness
Descripción:        Logica de negocio de PortalPais              
Autor:              paola.munoz                         
Fecha de creación:  10/11/2015 08:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada de realizar la logica de negocio para el portalPais
    /// </summary>
    public class PortalPaisBusiness : IPortalPaisBusiness
    {
        /// <summary>
        /// Instancia de la clase PortalPaisData;
        /// </summary>
        IPortalPaisData _PortalPaisData;


        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public PortalPaisBusiness()
        {
            _PortalPaisData = new PortalPaisData();
        }

        /// <summary>
        /// Metodo para obtener el país teniendo en cuenta el portal
        /// </summary>
        /// <param name="siglaPortal">Sigla del portal</param>
        /// <returns>Identificador de país</returns>
        public async Task<BusinessResult<PortalPaisDto>> ObtenerPaisPorPortal(string siglaPortal)
        {
            try
            {
                var result = await _PortalPaisData.ObtenerPaisPorPortal(siglaPortal);
                return BusinessResult<PortalPaisDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<PortalPaisDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Obtener el pais del portal no asincrona
        /// </summary>
        /// <param name="siglaPortal"></param>
        /// <returns></returns>
        public BusinessResult<PortalPaisDto> ObtenerPaisPorPortalNoAsincrono(string siglaPortal)
        {
            try
            {
                var result = _PortalPaisData.ObtenerPaisPorPortalNoAsincrono(siglaPortal);
                return BusinessResult<PortalPaisDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<PortalPaisDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Obtener la fecha del portal
        /// </summary>
        /// <param name="siglaPortal"></param>
        /// <returns></returns>
        public BusinessResult<DateTime> ObtenerFechaPortal(string siglaPortal)
        {
            try
            {
                string zona = "Central America Standard Time";
                var result = _PortalPaisData.ObtenerPaisPorPortalNoAsincrono(siglaPortal);
                if (result != null)
                {
                    zona = result.ZonaHoraria;
                }
                var info = TimeZoneInfo.FindSystemTimeZoneById(zona);
                DateTimeOffset localServerTime = DateTimeOffset.Now;
                DateTimeOffset istambulTime = TimeZoneInfo.ConvertTime(localServerTime, info);
                return BusinessResult<DateTime>.Success(istambulTime.DateTime, "Exito!");
            }
            catch (Exception ex)
            {
                return BusinessResult<DateTime>.Issue(DateTime.UtcNow, "Error!", ex);
            }
        }
    }
}
