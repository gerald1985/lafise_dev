﻿/*==========================================================================
Archivo:            ContactenosBusiness
Descripción:        Logica de negocio de Contactenos                     
Autor:              paola.munoz                          
Fecha de creación:  13/10/2015 12:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Business
{

    /// <summary>
    /// Clase encargada del manejo de logica de negocio para  Contactenos
    /// </summary>
    public class ContactenosBusiness : IContactenosBusiness
    {
        /// <summary>
        /// Instancia de la clase ContactenosData;
        /// </summary>
        IContactenosData _ContactenosData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public ContactenosBusiness()
        {
            _ContactenosData = new ContactenosData();

        }
        /// <summary>
        /// ver todos los contactenos
        /// </summary>
        /// <returns>Lista de contactenos</returns>
        public async Task<BusinessResult<ICollection<ContactenosDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _ContactenosData.VerTodosGrid();
                return BusinessResult<ICollection<ContactenosDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ContactenosDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// ver los contactenos por identificador
        /// </summary>
        /// <param name="id">Identificador de los contactenos</param>
        /// <returns>Contactenos</returns>
        public async Task<BusinessResult<ContactenosDto>> VerPorId(Int64 id)
        {
            try
            {
                var result = await _ContactenosData.VerPorId(id);
                return BusinessResult<ContactenosDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ContactenosDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
      
        /// <summary>
        /// Eliminar contactenos
        /// </summary>
        /// <param name="id">identificador de contactenos</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>bool</returns>
        public async Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario)
        {
            try
            {
                await _ContactenosData.Eliminar(id);
                LogHelper.Audit("Contactenos", Accion.Eliminacion.ToString(),usuario);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Contactenos", Accion.Eliminacion.ToString(),usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// CrearContactenos
        /// </summary>
        /// <param name="contactenos">Contactenos</param>
        /// <returns>Contactenos</returns>
        public async Task<BusinessResult<ContactenosDto>> Crear(ContactenosDto contactenos)
        {
            try
            {
                if (!string.IsNullOrEmpty(contactenos.PathUrl))
                {
                    string[] sigla = contactenos.PathUrl.Split('/');
                    if (sigla.Length > 3)
                    {
                        IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                        var portalPais = await _PortalPaisBusiness.ObtenerPaisPorPortal(sigla[3]);
                        if (portalPais.SuccessfulOperation)
                        {
                            if (portalPais.Result != null)
                            {
                                contactenos.IdPais = portalPais.Result.IdPais.Value;
                            }
                            else
                            {
                                IPaisBusiness _PaisBusiness = new PaisBusiness();
                                var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                                if (pais.SuccessfulOperation)
                                {
                                    contactenos.IdPais = pais.Result;
                                }
                            }
                        }
                        else
                        {
                            IPaisBusiness _PaisBusiness = new PaisBusiness();
                            var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                            if (pais.SuccessfulOperation)
                            {
                                contactenos.IdPais = pais.Result;
                            }
                        }
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                        if (pais.SuccessfulOperation)
                        {
                            contactenos.IdPais = pais.Result;
                        }
                    }
                }
                else
                {
                    IPaisBusiness _PaisBusiness = new PaisBusiness();
                    var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                    if (pais.SuccessfulOperation)
                    {
                        contactenos.IdPais = pais.Result;
                    }
                }

                var result = await _ContactenosData.Crear(contactenos);
                LogHelper.Audit("Contactenos", Accion.Creacion.ToString(), contactenos.Usuario);
                return BusinessResult<ContactenosDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Contactenos", Accion.Creacion.ToString(), contactenos.Usuario, false);
                return BusinessResult<ContactenosDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Editar Contactenos
        /// </summary>
        /// <param name="contactenos">Contactenos</param>
        /// <returns>Contactenos</returns>
        public async Task<BusinessResult<ContactenosDto>> Editar(ContactenosDto contactenos)
        {
            try
            {
                var result = await _ContactenosData.Editar(contactenos);
                LogHelper.Audit("Contactenos", Accion.Modificacion.ToString(), contactenos.Usuario);
                return BusinessResult<ContactenosDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Contactenos", Accion.Modificacion.ToString(), contactenos.Usuario, false);
                return BusinessResult<ContactenosDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
