﻿/*==========================================================================
Archivo:            TasacambioBusiness
Descripción:        logica de negocio de Tasa de cambio                      
Autor:              paola.munoz                          
Fecha de creación:  25/02/2016 8:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase para consultar las tasas de cambio 
    /// </summary>
    public class TasaCambioBusiness : ITasaCambioBusiness
    {
         /// <summary>
        /// Instancia de la clase TasaCambioData;
        /// </summary>
        ITasaCambioData _TasaCambioData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TasaCambioBusiness()
        {
            _TasaCambioData = new TasaCambioData();

        }
        /// <summary>
        /// Metodo para consultar las tasas de cambio por pais
        /// </summary>
        /// <param name="idPais">Identificador del país</param>
        /// <returns>Lista de tasa de cambio</returns>
        public async Task<BusinessResult<ICollection<TasaCambioDto>>> VerPorPaisActivo(TasaCambioDto tasaCambio)
        {
            try
            {
                Int64 idPais = VerPais(tasaCambio);
                var result = await _TasaCambioData.VerPorPaisActivo(idPais);
                return BusinessResult<ICollection<TasaCambioDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TasaCambioDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo que consulta el país del usuario
        /// </summary>
        /// <param name="suscripcion"></param>
        private static Int64 VerPais(TasaCambioDto tasaCambio)
        {
            Int64 idPais = 0;
            if (!string.IsNullOrEmpty(tasaCambio.PathUrl))
            {
                string[] sigla = tasaCambio.PathUrl.Split('/');
                if (sigla.Length > 3)
                {
                    IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                    var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                    if (portalPais.SuccessfulOperation && portalPais.Result != null)
                    {
                        idPais = portalPais.Result.IdPais.Value;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                        if (pais.SuccessfulOperation)
                        {
                            idPais = pais.Result;
                        }
                    }
                    
                }
                else
                {
                    IPaisBusiness _PaisBusiness = new PaisBusiness();
                    var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                    if (pais.SuccessfulOperation)
                    {
                        idPais = pais.Result;
                    }
                }
            }
            else
            {
                IPaisBusiness _PaisBusiness = new PaisBusiness();
                var pais = _PaisBusiness.VerPaisPorNombreNoAsincrono(Constants.PAIS_NO_ENCONTRADO);
                if (pais.SuccessfulOperation)
                {
                    idPais = pais.Result;
                }
            }
            return idPais;
        }
    }
}
