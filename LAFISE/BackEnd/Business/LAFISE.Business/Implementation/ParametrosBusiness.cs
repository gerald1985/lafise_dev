﻿/*==========================================================================
Archivo:            ParametrosBusiness
Descripción:        Logica de negocio de Parametros              
Autor:              Juan.Hincapie                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada de realizar la logica de negocio para los parametros
    /// </summary>
    public class ParametrosBusiness :IParametrosBusiness
    {
         /// <summary>
        /// Instancia de la clase MarcaAutoData;
        /// </summary>
        IParametrosData _ParametrosData;


        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public ParametrosBusiness()
        {
            _ParametrosData = new ParametrosData();
        }

        /// <summary>
        /// Método encargado de obtener la ruta de la imagen.
        /// </summary>
        /// <param name="rutaArchivo">Parametro inicial</param>
        /// <returns>Ruta solicitada</returns>
        public async Task<BusinessResult<string>> ObtenerParametroRuta(string rutaArchivo)
        {
            try
            {
                var result = await _ParametrosData.ObtenerParametroRuta(rutaArchivo);
                return BusinessResult<string>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<string>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de obtener la informacion no asincrona.
        /// </summary>
        /// <param name="informa">Parametro inicial</param>
        /// <returns>Información solicitada</returns>
        public BusinessResult<string> ObtenerParametroRutaNoAsincrono(string informa)
        {
            try
            {
                var result = _ParametrosData.ObtenerParametroRutaNoAsincrono(informa);
                return BusinessResult<string>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<string>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
