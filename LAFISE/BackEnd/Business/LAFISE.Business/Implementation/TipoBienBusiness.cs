﻿/*==========================================================================
Archivo:            TipoBienBusiness
Descripción:        Logica de negocio de Tipo Bien                     
Autor:              juan.hincapie                          
Fecha de creación:  06/01/2016 10:15:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para Tipo bien
    /// </summary>
    public class TipoBienBusiness : ITipoBienBusiness
    {
        /// <summary>
        /// Instancia de la clase TipoBienData;
        /// </summary>
        ITipoBienData _TipoBienData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TipoBienBusiness()
        {
            _TipoBienData = new TipoBienData();
        }
        /// <summary>
        /// Ver todos los tipos de bien
        /// </summary>
        /// <returns>Lista de tipo de bien</returns>
        public async Task<BusinessResult<ICollection<TipoBienDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _TipoBienData.VerTodosGrid();
                return BusinessResult<ICollection<TipoBienDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoBienDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
