﻿/*==========================================================================
Archivo:            PrimaCategoriaPaisBusiness
Descripción:        Logica de negocio PrimaCategoriaPais                    
Autor:              steven.echavarria                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Business.Interfaces;
using LAFISE.Common.Backend;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para obtener el porcentaje de prima por categoría y país
    /// </summary>
    public class PrimaCategoriaPaisBusiness : IPrimaCategoriaPaisBusiness
    {
        /// <summary>
        /// Instancia de la clase PrimaCategoriaPaisData;
        /// </summary>
        IPrimaCategoriaPaisData _PrimaCategoriaPaisData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public PrimaCategoriaPaisBusiness()
        {
            _PrimaCategoriaPaisData = new PrimaCategoriaPaisData();

        }

        /// <summary>
        /// Método encargado de consultar el porcentaje de prima por categoria y país
        /// </summary>
        /// <param name="primaCategoriaPais">entidad prima categoria pais</param>
        /// <returns>Entidad PrimaCategoriaPaisDto</returns>
        public BusinessResult<PrimaCategoriaPaisDto> VerPorcentajePrimaPorCategoriaPais(PrimaCategoriaPaisDto primaCategoriaPais)
        {
            try
            {
                Int64 idPais = 0;
                string[] sigla = primaCategoriaPais.Path.Split('/');
                if (sigla.Length > 3)
                {
                    IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                    var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                    if (portalPais.SuccessfulOperation && portalPais.Result != null)
                    {
                        idPais = portalPais.Result.IdPais.Value;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        idPais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result.Result;
                    }
                }
                else
                {
                    IPaisBusiness _PaisBusiness = new PaisBusiness();
                    idPais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result.Result;
                }

                primaCategoriaPais.IdPais = idPais;
                var result = _PrimaCategoriaPaisData.VerPorcentajePrimaPorCategoriaPais(primaCategoriaPais);
                return BusinessResult<PrimaCategoriaPaisDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<PrimaCategoriaPaisDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
