﻿/*==========================================================================
Archivo:            TipoSucursalBusiness
Descripción:        Logica de negocio de TipoSucursal                     
Autor:              paola.munoz                          
Fecha de creación:  06/10/2015 10:15:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using ARKIX.Business;
using System.Data.Entity.Infrastructure;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Common.Backend;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada del manejo de logica de negocio para tipo sucursal
    /// </summary>
    public class TipoSucursalBusiness : ITipoSucursalBusiness
    {
        /// <summary>
        /// Instancia de la clase TipoSucursalData;
        /// </summary>
        ITipoSucursalData _TipoSucursalData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public TipoSucursalBusiness()
        {
            _TipoSucursalData = new TipoSucursalData();

        }
        /// <summary>
        /// Ver todos los tipos de sucursal
        /// </summary>
        /// <returns>Lista de tipo de sucursal</returns>
        public async Task<BusinessResult<ICollection<TipoSucursalDto>>> VerTodosGrid()
        {
            try
            {
                var result = await _TipoSucursalData.VerTodosGrid();
                return BusinessResult<ICollection<TipoSucursalDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoSucursalDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Ver Tipo sucursal por identificador
        /// </summary>
        /// <param name="id">Identificador de tipo de sucursal</param>
        /// <returns>Tipo sucursal</returns>
        public async Task<BusinessResult<TipoSucursalDto>> VerPorId(Int64 id)
        {
            try
            {
                var result = await _TipoSucursalData.VerPorId(id);
                return BusinessResult<TipoSucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoSucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Eliminar tipo sucursal
        /// </summary>
        /// <param name="id">Identificador de tipo de sucursal</param>
        /// <returns>string</returns>
        public async Task<BusinessResult<string>> Eliminar(Int64 id, string usuario)
        {
            string datos = "";
            try
            {
                var validar = await _TipoSucursalData.ValidarTipoSucursal(id);
                if (validar != null)
                {
                    if (validar.Contador == 0)
                    {
                        await _TipoSucursalData.Eliminar(id);
                    }
                    else
                    {
                        datos = validar.Nombre;
                    }
                }
                LogHelper.Audit("Tipo Sucursal", Accion.Eliminacion.ToString(),usuario);
                return BusinessResult<string>.Success(datos, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tipo Sucursal", Accion.Eliminacion.ToString(), usuario, false);
                return BusinessResult<string>.Issue(datos, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Crear tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        public async Task<BusinessResult<TipoSucursalDto>> Crear(TipoSucursalDto tipoSucursal)
        {
            try
            {
                var result = await _TipoSucursalData.Crear(tipoSucursal);
                LogHelper.Audit("Tipo Sucursal", Accion.Creacion.ToString(),tipoSucursal.Usuario);
                return BusinessResult<TipoSucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
               if (ex.InnerException.InnerException.Message.Contains("UQ_TipoSucursal_TipoSucursal"))
                {
                    LogHelper.Audit("Tipo Sucursal", Accion.Creacion.ToString(), tipoSucursal.Usuario, false);
                    TipoSucursalDto dto = new TipoSucursalDto()
                    {
                        MensajeError = Messages.Messages.OperacionErroneaDuplicada
                    };
                    return BusinessResult<TipoSucursalDto>.Issue(dto, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
               LogHelper.Audit("Tipo Sucursal", Accion.Creacion.ToString(), tipoSucursal.Usuario, false);
               return BusinessResult<TipoSucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }  
            catch (Exception ex)
            {
                LogHelper.Audit("Tipo Sucursal", Accion.Creacion.ToString(), tipoSucursal.Usuario, false);
                return BusinessResult<TipoSucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Editar tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        public async Task<BusinessResult<TipoSucursalDto>> Editar(TipoSucursalDto tipoSucursal)
        {
            try
            {
                var result = await _TipoSucursalData.Editar(tipoSucursal);
                LogHelper.Audit("Tipo Sucursal", Accion.Modificacion.ToString(),tipoSucursal.Usuario);
                return BusinessResult<TipoSucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
               if (ex.InnerException.InnerException.Message.Contains("UQ_TipoSucursal_TipoSucursal"))
                {
                    LogHelper.Audit("Tipo Sucursal", Accion.Creacion.ToString(), tipoSucursal.Usuario, false);
                    TipoSucursalDto dto = new TipoSucursalDto()
                    {
                        MensajeError = Messages.Messages.OperacionErroneaDuplicada
                    };
                    return BusinessResult<TipoSucursalDto>.Issue(dto, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
               LogHelper.Audit("Tipo Sucursal", Accion.Creacion.ToString(), tipoSucursal.Usuario, false);
               return BusinessResult<TipoSucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tipo Sucursal", Accion.Modificacion.ToString(), tipoSucursal.Usuario, false);
                return BusinessResult<TipoSucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Ver consulta por filtro
        /// </summary>
        /// <param name="tipoSucursal">nombre Tipo sucursal</param>
        /// <returns>lista de tipo de sucursal</returns>
        public async Task<BusinessResult<ICollection<TipoSucursalDto>>> TipoSucursalFiltro(string tipoSucursal)
        {
            try
            {
                var result = await _TipoSucursalData.TipoSucursalFiltro(tipoSucursal);
                return BusinessResult<ICollection<TipoSucursalDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoSucursalDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo para validar si existen sucursales
        /// </summary>
        /// <param name="tipoSucursal">Identificador del tipo de sucursal</param>
        /// <returns>SucursalValidarDto</returns>
        public async Task<BusinessResult<SucursalValidarDto>> ValidarTipoSucursal(Int64 tipoSucursal)
        {
            try
            {
                var result = await _TipoSucursalData.ValidarTipoSucursal(tipoSucursal);
                return BusinessResult<SucursalValidarDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<SucursalValidarDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método que permite inactivar o activar
        /// </summary>
        /// <param name="tipoSucursal">Tipo de sucursal</param>
        /// <returns>string</returns>
        public async Task<BusinessResult<string>> Inactivar(TipoSucursalDto tipoSucursal)
        {
            string datos = "";
            try
            {
                if (!tipoSucursal.Activo)
                {
                    var validar = await _TipoSucursalData.ValidarTipoSucursal(tipoSucursal.Id);
                    if (validar != null)
                    {
                        if (validar.Contador == 0)
                        {
                            var result = await _TipoSucursalData.Inactivar(tipoSucursal);
                        }
                        else
                        {
                            datos = validar.Nombre;
                        }
                    }
                }
                else
                {
                    var result = await _TipoSucursalData.Inactivar(tipoSucursal);
                }
                LogHelper.Audit("Tipo Sucursal", Accion.Inactivacion.ToString(), tipoSucursal.Usuario);
                return BusinessResult<string>.Success(datos, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Tipo Sucursal", Accion.Inactivacion.ToString(), tipoSucursal.Usuario, false);
                return BusinessResult<string>.Issue(datos, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Validar que el nombre del tipo sucursal sea unico.
        /// </summary>
        /// <param name="tipoSucursal">Dto tipo sucursal.</param>
        /// <returns>Entidad con del tipo sucursal encontrada.</returns>
        public async Task<BusinessResult<TipoSucursalDto>> ValidarTipoSucursalUnica(TipoSucursalDto tipoSucursal)
        {
            try
            {
                var result = await _TipoSucursalData.ValidarTipoSucursalUnica(tipoSucursal);
                return BusinessResult<TipoSucursalDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<TipoSucursalDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo que se encarga de consultar todas las sucursales activas
        /// </summary>
        /// <returns>Lista de tipo sucursal</returns>
        public async Task<BusinessResult<ICollection<TipoSucursalDto>>> VerTodosActivos()
        {
            try
            {
                var result = await _TipoSucursalData.VerTodosActivas();
                return BusinessResult<ICollection<TipoSucursalDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<TipoSucursalDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

    }
}
