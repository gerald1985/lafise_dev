﻿/*==========================================================================
Archivo:            MarcaAutoBusiness
Descripción:        Logica de negocio de MarcaAuto                      
Autor:              Juan.Hincapie                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada de realizar la logica de negocio para las Marca de auto.
    /// </summary>
    public class MarcaAutoBusiness : IMarcaAutoBusiness
    {
        /// <summary>
        /// Constantes
        /// </summary>
        #region Constantes
        public const string ModuloMarcas = "Marca Auto";
        public const string ClaveUnicaMarcasNombre = "UQ_MarcaAuto_Marca";
        #endregion

        /// <summary>
        /// Instancia de la clase MarcaAutoData;
        /// </summary>
        IMarcaAutoData _MarcaAutoData;


        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public MarcaAutoBusiness()
        {
            _MarcaAutoData = new MarcaAutoData();
        }

        /// <summary>
        /// Método encargado de consultar las marca de auto según los filtros de búsqueda
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca de auto</param>
        /// <returns>BusinessResult con la lista de todas las marcas de auto</returns>
        public async Task<BusinessResult<ICollection<MarcaAutoFiltroDto>>> VerMarcasModelos(MarcaAutoDto marcaauto)
        {
            try
            {
                var result = await _MarcaAutoData.VerMarcasModelos(marcaauto);
                return BusinessResult<ICollection<MarcaAutoFiltroDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<MarcaAutoFiltroDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de consultar todas las marcas de auto.
        /// </summary>
        /// <returns>BusinessResult con la lista de todas las marcas de auto.</returns>
        public async Task<BusinessResult<ICollection<MarcaAutoDto>>> VerTodos()
        {
            try
            {
                var result = await _MarcaAutoData.VerTodos();
                return BusinessResult<ICollection<MarcaAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<MarcaAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de retornar una marca auto según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>BusinessResult con  el dto de la marca de Auto.</returns>
        public async Task<BusinessResult<MarcaAutoDto>> VerMarcaAutoPorId(long id)
        {
            try
            {
                var result = await _MarcaAutoData.VerMarcaAutoPorId(id);
                return BusinessResult<MarcaAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de retornar una marca auto según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>BusinessResult con  el dto de la marca de Auto.</returns>
        public BusinessResult<MarcaAutoDto> VerMarcaAutoPorIdNoAsincrono(long id)
        {
            try
            {
                var result = _MarcaAutoData.VerMarcaAutoPorIdNoAsincrono(id);
                return BusinessResult<MarcaAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de crear las Marcas de Auto y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto</param>
        /// <returns>BusinessResult con el dto de marca auto que se insertó.</returns>
        public async Task<BusinessResult<MarcaAutoDto>> CrearMarcaAuto(MarcaAutoDto marcaauto)
        {
            try
            {
                LogHelper.Audit(ModuloMarcas, Accion.Creacion.ToString(), marcaauto.Usuario);
                var result = await _MarcaAutoData.CrearMarcaAuto(marcaauto);
                return BusinessResult<MarcaAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloMarcas, Accion.Creacion.ToString(), marcaauto.Usuario, false);
                if (ex.InnerException.InnerException.Message.Contains(ClaveUnicaMarcasNombre))
                {
                    return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                else { return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex); }
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloMarcas, Accion.Creacion.ToString(), marcaauto.Usuario, false);
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de actualizar las Marcas de Auto y sus relaciones.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>BusinessResult el dto de marca auto que se editó.</returns>
        public async Task<BusinessResult<MarcaAutoDto>> EditarMarcaAuto(MarcaAutoDto marcaauto)
        {
            try
            {
                LogHelper.Audit(ModuloMarcas, Accion.Modificacion.ToString(), marcaauto.Usuario);
                var result = await _MarcaAutoData.EditarMarcaAuto(marcaauto);
                return BusinessResult<MarcaAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (DbUpdateException ex)
            {
                LogHelper.Audit(ModuloMarcas, Accion.Modificacion.ToString(), marcaauto.Usuario, false);
                if (ex.InnerException.InnerException.Message.Contains(ClaveUnicaMarcasNombre))
                {
                    MarcaAutoDto dto = new MarcaAutoDto()
                    {
                        mensajeError = Messages.Messages.OperacionErroneaDuplicada
                    };
                    LogHelper.Audit("Marca Auto", Accion.Modificacion.ToString(), marcaauto.Usuario, false);
                    return BusinessResult<MarcaAutoDto>.Issue(dto, Messages.Messages.OperacionErroneaDuplicada, ex);
                }
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloMarcas, Accion.Modificacion.ToString(), marcaauto.Usuario, false);
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo usado para eliminar MarcaAuto y sus modelos asociados
        /// </summary>
        /// <param name="id">Identificador de la marca auto.</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>BusinessResult con true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<BusinessResult<bool>> EliminarMarcaAutoPorId(long id, string usuario)
        {
            try
            {
                LogHelper.Audit(ModuloMarcas, Accion.Eliminacion.ToString(), usuario);
                var result = await _MarcaAutoData.EliminarMarcaAutoPorId(id);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloMarcas, Accion.Eliminacion.ToString(), usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Metodo usado para eliminar Modelo de un auto.
        /// </summary>
        /// <param name="id">Identificador de el modelo auto.</param>
        /// <returns>BusinessResult con true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<BusinessResult<bool>> EliminarModeloAutoPorId(long id, string usuario)
        {
            try
            {
                LogHelper.Audit(ModuloMarcas, Accion.Eliminacion.ToString(), usuario);
                var result = await _MarcaAutoData.EliminarModeloAutoPorId(id);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloMarcas, Accion.Eliminacion.ToString(), usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de inactivar las Marcas de Auto.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>BusinessResult con true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        public async Task<BusinessResult<bool>> InactivarMarcaAuto(MarcaAutoDto marcaauto)
        {
            try
            {
                LogHelper.Audit(ModuloMarcas, Accion.Inactivacion.ToString(), marcaauto.Usuario);
                var result = await _MarcaAutoData.InactivarMarcaAuto(marcaauto);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit(ModuloMarcas, Accion.Inactivacion.ToString(), marcaauto.Usuario, false);
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        ///  Método que permite inactivar o activar la marca de auto.
        /// </summary>
        /// <param name="marcaAuto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<string>> Inactivar(MarcaAutoDto marcaAuto)
        {
            string datos = string.Empty;
            try
            {
                if (!marcaAuto.Activo)
                {
                    var validar = await _MarcaAutoData.ValidarCasaComercialMarca(marcaAuto.Id);
                    if (validar != null)
                    {
                        datos = validar.Nombre;
                    }
                    else
                    {
                        LogHelper.Audit("Marca Auto", Accion.Inactivacion.ToString(), marcaAuto.Usuario);
                        var result = await _MarcaAutoData.InactivarMarcaAuto(marcaAuto);
                    }
                }
                else
                {
                    LogHelper.Audit("Marca Auto", Accion.Inactivacion.ToString(), marcaAuto.Usuario);
                    var result = await _MarcaAutoData.InactivarMarcaAuto(marcaAuto);
                }
                return BusinessResult<string>.Success(datos, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                LogHelper.Audit("Marca Auto", Accion.Inactivacion.ToString(), marcaAuto.Usuario, false);
                return BusinessResult<string>.Issue(datos, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        ///  Método que permite inactivar o activar el modelo de auto.
        /// </summary>
        /// <param name="modeloAuto"></param>
        /// <returns></returns>
        public async Task<BusinessResult<string>> InactivarModelo(ModeloAutoDto modeloAuto)
        {
            string datos = string.Empty;
            try
            {
                if (!modeloAuto.Activo)
                {
                    var validar = await _MarcaAutoData.ValidarCasaComercialModelo(modeloAuto.Id);
                    if (validar != null)
                    {
                        datos = validar.Nombre;
                    }
                }
                return BusinessResult<string>.Success(datos, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<string>.Issue(datos, Messages.Messages.OperacionErronea, ex);
            }
        }


        /// <summary>
        /// Método encargado de consultar el detalle de la marca auto.
        /// </summary>
        /// <param name="idMarca">Identificador de la marca auto.</param>
        /// <returns>BusinessResult con la lista del detalle de las Marcas de Auto.</returns>
        public async Task<BusinessResult<ICollection<ModeloAutoDto>>> ConsultarDetalleModelo(long idMarca)
        {
            try
            {
                var result = await _MarcaAutoData.ConsultarDetalleModelo(idMarca);
                return BusinessResult<ICollection<ModeloAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<ModeloAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }


        /// <summary>
        /// Obtener marca de autos activas
        /// </summary>
        /// <returns>lista MarcaAutoDto</returns>
        public async Task<BusinessResult<ICollection<MarcaAutoDto>>> ObtenerMarcasActivas()
        {
            try
            {
                var result = await _MarcaAutoData.ObtenerMarcasActivas();
                return BusinessResult<ICollection<MarcaAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<MarcaAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Obtener marca de autos activas
        /// </summary>
        /// <returns>lista MarcaAutoDto</returns>
        public async Task<BusinessResult<ICollection<MarcaAutoDto>>> ObtenerMarcasActivasPorPais(string pathIdPais)
        {
            try
            {
                Int64 idPais = 0;
                string[] sigla = pathIdPais.Split('/');
                IPortalPaisBusiness _PortalPaisBusiness = new PortalPaisBusiness();
                var portalPais = _PortalPaisBusiness.ObtenerPaisPorPortalNoAsincrono(sigla[3]);
                if (portalPais.SuccessfulOperation)
                {
                    if (portalPais.Result != null)
                    {
                        idPais = portalPais.Result.IdPais.Value;
                    }
                    else
                    {
                        IPaisBusiness _PaisBusiness = new PaisBusiness();
                        var pais = _PaisBusiness.VerPaisPorNombre(Constants.PAIS_NO_ENCONTRADO).Result;
                        if (pais.SuccessfulOperation)
                        {
                            idPais = pais.Result;
                        }
                    }
                }

                var result = await _MarcaAutoData.ObtenerMarcasActivasPorPais(idPais);
                return BusinessResult<ICollection<MarcaAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<MarcaAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de validar las relaciones de marca auto.
        /// </summary>
        /// <param name="Id">Identificador de marca de auto</param>
        /// <returns></returns>
        public async Task<BusinessResult<MarcaAutoDto>> ValidarRelaciones(long Id)
        {
            try
            {
                MarcaAutoDto dto = new MarcaAutoDto();
                var validarCasaComercial = await _MarcaAutoData.ValidarCasaComercialMarca(Id);
                if (validarCasaComercial != null)
                {
                    if (validarCasaComercial.Contador != 0)
                    {
                        dto.CasasComercialesRelacionadas = validarCasaComercial.Nombre;
                    }
                }
                var validarSolicitud = await _MarcaAutoData.validarRelacionPlanFinanciacion(Id);
                if (validarSolicitud != null)
                {
                    dto.RelacionSolicitud = Messages.Messages.OperacionErroneaFK;
                }
                return BusinessResult<MarcaAutoDto>.Success(dto, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
        /// <summary>
        /// Metodo para validar las relaciones con el modelo
        /// </summary>
        /// <param name="Id">Identificador de marca auto</param>
        /// <returns></returns>
        public async Task<BusinessResult<MarcaAutoDto>> ValidarRelacionesModelos(long Id)
        {
            try
            {
                MarcaAutoDto dto = new MarcaAutoDto();
                var validarCasaComercial = await _MarcaAutoData.ValidarCasaComercialModelo(Id);
                if (validarCasaComercial != null)
                {
                    if (validarCasaComercial.Contador != 0)
                    {
                        dto.CasasComercialesRelacionadas = validarCasaComercial.Nombre;
                    }
                }
                var validarSolicitud = await _MarcaAutoData.validarRelacionPlanFinanciacionModelo(Id);
                if (validarSolicitud != null)
                {
                    dto.RelacionSolicitud = Messages.Messages.OperacionErroneaFK;
                }
                return BusinessResult<MarcaAutoDto>.Success(dto, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Validar que el nombre de la propiedad sea unico.
        /// </summary>
        /// <param name="Nombre">Nombre del tipo de propiedad.</param>
        /// <returns>Entidad con el tipo propiedad encontrada.</returns>
        public async Task<BusinessResult<MarcaAutoDto>> ValidarMarcaUnica(long Id, string Nombre)
        {
            try
            {
                var result = await _MarcaAutoData.ValidarMarcaUnica(Id, Nombre);
                return BusinessResult<MarcaAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<MarcaAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método usado para cargar todas las marcas según sus ids
        /// </summary>
        /// <param name="listaMarcas">lista con el identificador de la marcas</param>
        /// <returns>Lista de marcas</returns>
        public async Task<BusinessResult<ICollection<MarcaAutoDto>>> VerMarcasPorId(List<long> listaMarcas)
        {
            try
            {
                var result = await _MarcaAutoData.VerMarcasPorId(listaMarcas);
                return BusinessResult<ICollection<MarcaAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<MarcaAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
