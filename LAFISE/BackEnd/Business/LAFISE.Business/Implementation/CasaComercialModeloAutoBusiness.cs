﻿/*==========================================================================
Archivo:            CasaComercialModeloAutoBusiness
Descripción:        Logica de negocio de CasaComercialModeloAuto                      
Autor:             Juan Hincapie                        
Fecha de creación:  30/09/2015 03:51:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Data;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// Clase encargada de realizar la logica de negocio para el detalle de las casas comerciales. 
    /// </summary>
    public class CasaComercialModeloAutoBusiness : ICasaComercialModeloAutoBusiness
    {
        /// <summary>
        /// Instancia de la clase CasaComercialModeloAutoData;
        /// </summary>
        ICasaComercialModeloAutoData _CasaComercialModeloAutoData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public CasaComercialModeloAutoBusiness()
        {
            _CasaComercialModeloAutoData = new CasaComercialModeloAutoData();
        }

        /// <summary>
        /// Método encargado de consultar todas las casas comerciales.
        /// </summary>
        /// <returns>BusinessResult con la lista de los detalle de las casas comerciales</returns>
        public async Task<BusinessResult<ICollection<CasaComercialModeloAutoDto>>> VerTodos()
        {
            try
            {
                var result = await _CasaComercialModeloAutoData.VerTodos();
                return BusinessResult<ICollection<CasaComercialModeloAutoDto>>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<ICollection<CasaComercialModeloAutoDto>>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de retornar el detalle de una casa comercial según su identificador.
        /// </summary>
        /// <param name="id">Identificador correspondiente a la entidad CasaComercialModeloAuto</param>
        /// <returns>BusinessResult con la lista de todas los detalles de las casas comerciales. </returns>
        public async Task<BusinessResult<CasaComercialModeloAutoDto>> VerPorId(int id)
        {
            try
            {
                var result = await _CasaComercialModeloAutoData.VerPorId(id);
                return BusinessResult<CasaComercialModeloAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CasaComercialModeloAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial.
        /// </summary>
        /// <param name="id">Identificador de la entidad CasaComercialModeloAutoDto</param>
        /// <returns>BusinessResult true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<BusinessResult<bool>> Eliminar(int id)
        {
            try
            {
                await _CasaComercialModeloAutoData.Eliminar(id);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial según su casa comercial
        /// </summary>
        /// <param name="id">Identificador de la casa comercial</param>
        /// <returns>BusinessResult true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<BusinessResult<bool>> EliminarPorCasaComercial(int id)
        {
            try
            {
                await _CasaComercialModeloAutoData.EliminarPorCasaComercial(id);
                return BusinessResult<bool>.Success(true, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<bool>.Issue(false, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de crear el detalle de las casas comerciales.
        /// </summary>
        /// <param name="casaComercialModeloAutoDto">Dto con las propiedades de CasaComercialModeloAutoDto.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial Modelo Auto que se insertó.</returns>
        public async Task<BusinessResult<CasaComercialModeloAutoDto>> Crear(CasaComercialModeloAutoDto casaComercialModeloAutoDto)
        {
            try
            {
                var result = await _CasaComercialModeloAutoData.Crear(casaComercialModeloAutoDto);
                return BusinessResult<CasaComercialModeloAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CasaComercialModeloAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casaComercialModeloAutoDto">Dto con las propiedades de CasaComercialModeloAutoDto.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial Modelo Auto que se editó</returns>
        public async Task<BusinessResult<CasaComercialModeloAutoDto>> Editar(CasaComercialModeloAutoDto casaComercialModeloAutoDto)
        {
            try
            {
                var result = await _CasaComercialModeloAutoData.Editar(casaComercialModeloAutoDto);
                return BusinessResult<CasaComercialModeloAutoDto>.Success(result, Messages.Messages.OperacionCorrecta);
            }
            catch (Exception ex)
            {
                return BusinessResult<CasaComercialModeloAutoDto>.Issue(null, Messages.Messages.OperacionErronea, ex);
            }
        }
    }
}
