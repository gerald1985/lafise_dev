/*==========================================================================
Archivo:            ICasaComercialBusiness
Descripción:        Interfaz de CasaComercial                      
Autor:              Juan.Hincapie                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using ARKIX.Business;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{

    /// <summary>
    ///Interface encargada del manejo de logica de negocio para las casas comerciales.
    /// </summary>
    public interface ICasaComercialBusiness
    {
        /// <summary>
        /// Método encargado de consultar todas las casas comerciales.
        /// </summary>
        /// <returns>BusinessResult con la lista de todas las casas comerciales.</returns>
        Task<BusinessResult<ICollection<CasaComercialDto>>> ConsultarTodosGrid();

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="Modelo">Identificador del modelo.</param>
        /// <returns>Entidad CasaComercial.</returns>
        Task<BusinessResult<ICollection<CasaComercialDto>>> VerCasaComercialPorModelo(long Modelo);

        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial.</returns>
        Task<BusinessResult<CasaComercialDto>> VerCasaComercialPorId(long id);
        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial.</returns>
        BusinessResult<CasaComercialDto> VerCasaComercialPorIdNoAsincrono(long id);

        /// <summary>
        /// Metodo que valida que el nombre de la casa comercial sea unico.
        /// </summary>
        /// <param name="casacomercial">entidad casa comercial</param>
        /// <returns>casacomercial</returns>
        Task<BusinessResult<CasaComercialDto>> ValidarCasaComercialPorNombre(CasaComercialDto casacomercial);

        /// <summary>
        /// Método encargado de eliminar las casas comerciales según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>BusinessResult con true: Elimino Correctamente; false: No se logro eliminar</returns>
        Task<BusinessResult<CasaComercialDto>> EliminarCasaComercialPorId(long id, string usuario);

        /// <summary>
        /// Método encargado de crear las casas comerciales y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial que se insertó.</returns>
        Task<BusinessResult<CasaComercialDto>> CrearCasaComercial(CasaComercialDto casacomercial);

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult con el dto de la Casa Comercial que se editó.</returns>
        Task<BusinessResult<CasaComercialDto>> EditarCasaComercial(CasaComercialDto casacomercial);

        /// <summary>
        /// Método encargado de consultar las casas comerciales según los filtros de búsqueda
        /// </summary>
        /// <param name="casaComercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult con la lista de todas las casas comerciales.</returns>
        Task<BusinessResult<ICollection<CasaComercialFiltroDto>>> ConsultarCasaComercialFiltros(CasaComercialDto casaComercial);

        /// <summary>
        /// Método encargado de consultar los modelos según los filtros de las marcas
        /// </summary>
        /// <param name="marcas">id de las marcas separadas por comas</param>
        /// <returns>lista de modelos agrupados por marca</returns>
        Task<BusinessResult<ICollection<CasaComercialModeloMarcaFiltroDto>>> ConsultarCasaComercialModeloMarcaFiltros(string marcas);

        /// <summary>
        /// Método encargado de inactivar las casas comerciales.
        /// </summary>
        /// <param name="casaComercialDto">Dto con las propiedades de la casa comercial.</param>
        /// <returns>BusinessResult true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        Task<BusinessResult<CasaComercialDto>> InactivarCasaComercial(CasaComercialDto casaComercialDto);

        /// <summary>
        /// Método encargado de consultar el detalle de la casa comercial.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>detalle de las casas comerciales. </returns>
        Task<BusinessResult<CasaComercialDto>> ConsultarDetalleCasaComercial(long id);

        /// <summary>
        /// Método encargado de consultar las casas comerciales según el pais.
        /// </summary>
        /// <param name="idPais">Identificador del pais asociado a la casa comercial.</param>
        /// <returns>BusinessResult conm lista de las casas comerciales.</returns>
        Task<BusinessResult<ICollection<CasaComercialDto>>> ConsultarCasaComercialPorPais(long idPais);

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="entidad">entidad CasaComercialModeloPaisDto</param>
        /// <returns></returns>
        Task<BusinessResult<ICollection<CasaComercialModeloPaisDto>>> ConsultarCasaComercialModeloPais(CasaComercialModeloPaisDto entidad);

    }

}