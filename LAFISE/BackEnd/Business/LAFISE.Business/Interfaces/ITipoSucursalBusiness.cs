﻿/*==========================================================================
Archivo:            ITipoSucursalBusiness
Descripción:        Interface de TipoSucursalBusiness                    
Autor:              paola.munoz                          
Fecha de creación:  06/10/2015 5:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias

using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
  
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para  TipoSucursalBusiness
    /// </summary>
    public interface ITipoSucursalBusiness
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de tipo sucursal</returns>
        Task<BusinessResult<ICollection<TipoSucursalDto>>> VerTodosGrid();

        /// <summary>
        /// Ver tipo sucursal por identificador
        /// </summary>
        /// <param name="id">Identificador de tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        Task<BusinessResult<TipoSucursalDto>> VerPorId(Int64 id);

        /// <summary>
        /// Eliminar tipo sucursal
        /// </summary>
        /// <param name="id">Identificador de tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        Task<BusinessResult<string>> Eliminar(Int64 id, string usuario);

        /// <summary>
        /// Crear Tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        Task<BusinessResult<TipoSucursalDto>> Crear(TipoSucursalDto tipoSucursal);

        /// <summary>
        /// Editar Tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        Task<BusinessResult<TipoSucursalDto>> Editar(TipoSucursalDto tipoSucursal);
        /// <summary>
        /// Tipo sucursal filtro
        /// </summary>
        /// <param name="tipoSucursal">nombre Tipo sucursal</param>
        /// <returns>Lista Tipo sucursal</returns>
        Task<BusinessResult<ICollection<TipoSucursalDto>>> TipoSucursalFiltro(string tipoSucursal);
        /// <summary>
        /// Inactivar Tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>string</returns>
        Task<BusinessResult<string>> Inactivar(TipoSucursalDto tipoSucursal);
        /// <summary>
        /// Validar tipo Sucursal
        /// </summary>
        /// <param name="tipoSucursal">Identificador de tipo sucursal</param>
        /// <returns>SucursalValidarDto</returns>
        Task<BusinessResult<SucursalValidarDto>> ValidarTipoSucursal(Int64 tipoSucursal);
        /// <summary>
        /// Validar que el tipo sucursal sea unico
        /// </summary>
        /// <param name="tipoSucursal">Entidad tipo sucursal</param>
        /// <returns>Entidad del tipo de sucursal</returns>
        Task<BusinessResult<TipoSucursalDto>> ValidarTipoSucursalUnica(TipoSucursalDto tipoSucursal);

        /// <summary>
        /// Interfaz para consultar todas las sucursales activas
        /// </summary>
        /// <returns>Lista de tipo sucursal</returns>
        Task<BusinessResult<ICollection<TipoSucursalDto>>> VerTodosActivos();

    }
}
