﻿/*==========================================================================
Archivo:            ITipoBienBusiness
Descripción:        Interface de TipoBienBusiness                    
Autor:              juan.hincapie                          
Fecha de creación:  06/10/2015 5:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para  TipoBienBusiness
    /// </summary>
    public interface ITipoBienBusiness
    {
        /// <summary>
        /// Ver todos los tipos de bien
        /// </summary>
        /// <returns>Lista de tipo de bien</returns>
        Task<BusinessResult<ICollection<TipoBienDto>>> VerTodosGrid();
    }
}
