﻿/*==========================================================================
Archivo:            IMarcaAutoBusiness
Descripción:        Interfaz de MarcaAutoBusiness                      
Autor:              Juan.Hincapie                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// IMarcaAutoBusiness encargada de realizar la logica de negocio para las Marca de auto.
    /// </summary>
    public interface IMarcaAutoBusiness
    {
        /// <summary>
        /// Método encargado de consultar las marca de auto según los filtros de búsqueda
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca de auto</param>
        /// <returns>BusinessResult con la lista de todas las marcas de auto</returns>
        Task<BusinessResult<ICollection<MarcaAutoFiltroDto>>> VerMarcasModelos(MarcaAutoDto marcaauto);

        /// <summary>
        /// Método encargado de consultar todas las marcas de auto.
        /// </summary>
        /// <returns>BusinessResult con la lista de todas las marcas de auto.</returns>
        Task<BusinessResult<ICollection<MarcaAutoDto>>> VerTodos();

        /// <summary>
        /// Método encargado de retornar una marca auto según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>BusinessResult con  el dto de la marca de Auto.</returns>
        Task<BusinessResult<MarcaAutoDto>> VerMarcaAutoPorId(long id);
        /// <summary>
        /// Método encargado de retornar una marca auto según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>BusinessResult con  el dto de la marca de Auto.</returns>
        BusinessResult<MarcaAutoDto> VerMarcaAutoPorIdNoAsincrono(long id);

        /// <summary>
        /// Método encargado de crear las Marcas de Auto y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto</param>
        /// <returns>BusinessResult con el dto de marca auto que se insertó.</returns>
        Task<BusinessResult<MarcaAutoDto>> CrearMarcaAuto(MarcaAutoDto marcaauto);

        /// <summary>
        /// Método encargado de actualizar las Marcas de Auto y sus relaciones.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>BusinessResult el dto de marca auto que se editó.</returns>
        Task<BusinessResult<MarcaAutoDto>> EditarMarcaAuto(MarcaAutoDto marcaauto);

        /// <summary>
        /// Metodo usado para eliminar MarcaAuto y sus modelos asociados
        /// </summary>
        /// <param name="id">Identificador de la marca auto.</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>BusinessResult con true: Elimino Correctamente; false: No se logro eliminar.</returns>
        Task<BusinessResult<bool>> EliminarMarcaAutoPorId(long id, string usuario);

        /// <summary>
        /// Método encargado de inactivar las Marcas de Auto.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>BusinessResult con true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        Task<BusinessResult<bool>> InactivarMarcaAuto(MarcaAutoDto marcaauto);

        /// <summary>
        /// Método encargado de consultar el detalle de la marca auto.
        /// </summary>
        /// <param name="idMarca">Identificador de la marca auto.</param>
        /// <returns>BusinessResult con la lista del detalle de las Marcas de Auto.</returns>
        Task<BusinessResult<ICollection<ModeloAutoDto>>> ConsultarDetalleModelo(long idMarca);

        /// <summary>
        /// Método que retorna las marcas activas
        /// </summary>
        /// <returns></returns>
        Task<BusinessResult<ICollection<MarcaAutoDto>>> ObtenerMarcasActivas();


        /// <summary>
        /// Método que retorna las marcas activas
        /// </summary>
        /// <param name="pathIdPais">identificador del país</param>
        /// <returns></returns>
        Task<BusinessResult<ICollection<MarcaAutoDto>>> ObtenerMarcasActivasPorPais(string pathIdPais);

        /// <summary>
        /// Método encargado de inactivar y validar las marcas.
        /// </summary>
        /// <param name="marcaAuto"></param>
        /// <returns></returns>
        Task<BusinessResult<string>> Inactivar(MarcaAutoDto marcaAuto);

        /// <summary>
        /// Método encargado de validar las relaciones de marca auto.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<BusinessResult<MarcaAutoDto>> ValidarRelaciones(long Id);

        /// <summary>
        /// Método encargado de validar las relaciones de modelo.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<BusinessResult<MarcaAutoDto>> ValidarRelacionesModelos(long Id);

        /// <summary>
        /// Validar que el nombre de la propiedad sea unico.
        /// </summary>
        /// <param name="Nombre">Nombre del tipo de propiedad.</param>
        /// <returns>Entidad con el tipo propiedad encontrada.</returns>
        Task<BusinessResult<MarcaAutoDto>> ValidarMarcaUnica(long Id, string Nombre);

        /// <summary>
        ///  Método que permite inactivar o activar el modelo de auto.
        /// </summary>
        /// <param name="modeloAuto"></param>
        /// <returns></returns>
        Task<BusinessResult<string>> InactivarModelo(ModeloAutoDto modeloAuto);

        /// <summary>
        /// Metodo usado para eliminar Modelo de un auto.
        /// </summary>
        /// <param name="id">Identificador de el modelo auto.</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>BusinessResult con true: Elimino Correctamente; false: No se logro eliminar.</returns>
        Task<BusinessResult<bool>> EliminarModeloAutoPorId(long id, string usuario);

        /// <summary>
        /// Método usado para cargar todas las marcas según sus ids
        /// </summary>
        /// <param name="listaMarcas">lista con el identificador de la marcas</param>
        /// <returns>Lista de marcas</returns>
        Task<BusinessResult<ICollection<MarcaAutoDto>>> VerMarcasPorId(List<long> listaMarcas);
    }
}
