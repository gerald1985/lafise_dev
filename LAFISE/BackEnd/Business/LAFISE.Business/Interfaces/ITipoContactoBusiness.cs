﻿/*==========================================================================
Archivo:            ITipoContactoBusiness
Descripción:        Interface de ITipoContactoBusiness               
Autor:              paola.munoz                 
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para TipoContacto
    /// </summary>
    public interface ITipoContactoBusiness
    {
        /// <summary>
        /// metodo que se encarga de consultar los TipoContacto
        /// </summary>
        /// <returns>Lista TipoContacto</returns>
        Task<BusinessResult<ICollection<TipoContactoDto>>> VerTodos();

        
    }
}
