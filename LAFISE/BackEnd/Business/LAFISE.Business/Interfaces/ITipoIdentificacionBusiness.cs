﻿/*==========================================================================
Archivo:            ITipoIdentificacionBusiness
Descripción:        Interface de TipoIdentificacionBusiness                 
Autor:              paola.munoz                       
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para TipoIdentificacion
    /// </summary>
    public interface ITipoIdentificacionBusiness
    {
        /// <summary>
        /// metodo que se encarga de consultar todos los tipos de identificacion
        /// </summary>
        /// <returns>Lista TipoIdentificacion</returns>
        Task<BusinessResult<ICollection<TipoIdentificacionDto>>> VerTodos();

        /// <summary>
        /// Metodo que se encarga de consultar los tipos de identificacion teniendo en cuenta el identificador de páís
        /// </summary>
        /// <param name="path">url del sitio</param>
        /// <returns>Lista TipoIdentificacion</returns>
        Task<BusinessResult<ICollection<TipoIdentificacionDto>>> VerTipoIdentificacionPorIdPais(string path);

        /// <summary>
        /// Metodo que se encarga de consultar las mascaras de entrada por tipo de identificacion
        /// </summary>
        /// <param name="id">id de identificacion</param>
        /// <returns>Mascara de entrada</returns>
        Task<BusinessResult<String>> obtenerMascaraPorIdentificacion(Int64 id);

        /// <summary>
        /// Método encargado de consultar el tipo de identificación teniendo en cuenta el id
        /// </summary>
        /// <param name="id">Identificador del tipo de identificación</param>
        /// <returns>TipoIdentificacionDto</returns>
        BusinessResult<TipoIdentificacionDto> VerPorIdNoAsincrono(Int64 id);
    }
}
