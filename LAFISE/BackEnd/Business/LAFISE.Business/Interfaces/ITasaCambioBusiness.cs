﻿using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Business
{
    public interface ITasaCambioBusiness
    {
        Task<BusinessResult<ICollection<TasaCambioDto>>> VerPorPaisActivo(TasaCambioDto tasaCambio);
    }
}
