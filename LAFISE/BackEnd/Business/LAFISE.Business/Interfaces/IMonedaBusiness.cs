﻿/*==========================================================================
Archivo:            IMonedaBusiness
Descripción:        Interface de MonedaBusiness                 
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Moneda
    /// </summary>
    public interface IMonedaBusiness
    {
        /// <summary>
        /// Método usado para cargar todas las monedas
        /// </summary>
        /// <returns>Lista de monedas</returns>
        Task<BusinessResult<ICollection<MonedaDto>>> VerTodos();

        /// <summary>
        /// Método usado para cargar una moneda por su id
        /// </summary>
        /// <param name="id">Identificador de la moneda</param>
        /// <returns>Moneda</returns>
        Task<BusinessResult<MonedaDto>> VerMonedaPorId(int id);

        /// <summary>
        /// Método que retorna el simbolo de la moneda según el portal.
        /// </summary>
        /// <param name="siglaPortal"></param>
        /// <returns></returns>
        BusinessResult<string> ObtenerSimboloMoneda(string siglaPortal);

        /// <summary>
        /// Metodo que se encarga de consultar la moneda teniendo en cuenta el país
        /// </summary>
        /// <param name="idPais">identificador del país</param>
        /// <returns>MonedaDto</returns>
        BusinessResult<MonedaDto> VerMonedaPais(long idPais);
    }
}
