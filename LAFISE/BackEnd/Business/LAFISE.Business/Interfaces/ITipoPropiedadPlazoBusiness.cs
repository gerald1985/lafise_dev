﻿/*==========================================================================
Archivo:            ITipoPropiedadPlazoBusiness
Descripción:        Interfaz de TipoPropiedadPlazoBusiness                    
Autor:              steven.echavarria                          
Fecha de creación:  18/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para la consulta de plazo por tipo de propiedad
    /// </summary>
    public interface ITipoPropiedadPlazoBusiness
    {
        /// <summary>
        /// Método encargado de consultar el plazo de un tipo de propiedad
        /// </summary>
        /// <param name="idTipoPropiedad">identificador del tipo de propiedad</param>
        /// <returns>Entidad TipoPropiedadPlazoDto</returns>
        BusinessResult<TipoPropiedadPlazoDto> VerPlazoPorTipoPropiedad(long idTipoPropiedad);
    }
}
