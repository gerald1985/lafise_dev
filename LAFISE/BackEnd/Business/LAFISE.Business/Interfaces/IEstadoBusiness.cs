﻿/*==========================================================================
Archivo:            IEstadoBusiness
Descripción:        Interface de IEstadoBusiness               
Autor:              paola.munoz                 
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Estado
    /// </summary>
    public interface IEstadoBusiness
    {
        /// <summary>
        /// metodo que se encarga de consultar los estados por tipo de estado
        /// </summary>
        /// <param name="idTipoEstado">Identificador del tipo de la estado</param>
        /// <returns>Lista Estado</returns>
        Task<BusinessResult<ICollection<EstadoDto>>> VerEstadoIdTipo(Int64 idTipoEstado);

        /// <summary>
        /// Metodo que se encarga de consultar el estado por Id
        /// </summary>
        /// <param name="id">Identificador del estado</param>
        /// <returns>Estado</returns>
        BusinessResult<EstadoDto> VerEstadoById(Int64 id);
        
    }
}
