/*==========================================================================
Archivo:            IRangoSueldoBusiness
Descripción:        Interface RangoSueldoBusiness                      
Autor:              paola.munoz                          
Fecha de creación:  29/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using ARKIX.Business;
using System.Threading.Tasks;
using System;
#endregion

namespace LAFISE.Business
{

    /// <summary>
    ///nterface encargada del manejo de logica de negocio para Rango Sueldo
    /// </summary>
    public interface IRangoSueldoBusiness
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de rango sueldo</returns>
        Task<BusinessResult<ICollection<RangoSueldoDto>>> VerTodos();
        /// <summary>
        /// Consultar rango sueldo por identificador
        /// </summary>
        /// <param name="id">Identificador de rango sueldo</param>
        /// <returns>rango sueldo</returns>
        Task<BusinessResult<RangoSueldoDto>> VerPorId(long id);
        /// <summary>
        /// Metodo que se encarga de consultar el detalle del rango sueldo
        /// </summary>
        /// <param name="id">Identificador de rango sueldo</param>
        /// <returns>rango sueldo detalle</returns>
        Task<BusinessResult<ICollection<RangoSueldoDto>>> ConsultarDetalleRangoSueldo(long id);
        /// <summary>
        /// Editar rango sueldo
        /// </summary>
        /// <param name="rangoSueldo">rango sueldo</param>
        /// <returns>rango sueldo</returns>
        Task<BusinessResult<RangoSueldoDto>> Editar(RangoSueldoDto rangoSueldo);
        /// <summary>
        /// Crear rango sueldo
        /// </summary>
        /// <param name="rangoSueldo">rango sueldo</param>
        /// <returns>Rango sueldo</returns>
        Task<BusinessResult<RangoSueldoDto>> Crear(RangoSueldoDto rangoSueldo);
        /// <summary>
        /// Consultar por el filtro
        /// </summary>
        /// <param name="pais">identificadores de paises concatenados por coma</param>
        /// <returns>rango sueldo filtro</returns>
        Task<BusinessResult<ICollection<RangoSueldoFiltroDto>>> VerRangoSueldoFiltro(string pais);

        /// <summary>
        /// Ver consulta por pais
        /// </summary>
        /// <param name="PathUrl">Url</param>
        /// <returns>Lista rango sueldo por pais</returns>
        Task<BusinessResult<ICollection<RangoSueldoPaisDto>>> VerRangoSueldoPais(string PathUrl);
    }

}