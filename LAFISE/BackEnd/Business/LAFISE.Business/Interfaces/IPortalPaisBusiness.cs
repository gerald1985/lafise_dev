﻿/*==========================================================================
Archivo:            IPortalPaisBusiness
Descripción:        Interfaz de PortalPaisBusiness                      
Autor:              paola.munoz                          
Fecha de creación:  10/11/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// IPortalPaisBusiness encargada de realizar la logica de negocio para el maestro de portalPais.
    /// </summary>
    public interface IPortalPaisBusiness
    {
        /// <summary>
        /// Metodo para obtener el país teniendo en cuenta el portal
        /// </summary>
        /// <param name="siglaPortal">Sigla del portal</param>
        /// <returns>Identificador de país</returns>
        Task<BusinessResult<PortalPaisDto>> ObtenerPaisPorPortal(string siglaPortal);

        /// <summary>
        /// Metodo para obtener el país teniendo en cuenta el portal
        /// </summary>
        /// <param name="siglaPortal">Sigla del portal</param>
        /// <returns>Identificador de país</returns>
        BusinessResult<PortalPaisDto> ObtenerPaisPorPortalNoAsincrono(string siglaPortal);

        /// <summary>
        /// Método que retorna la fecha del portal en el cual se encuentra el usuario
        /// </summary>
        /// <param name="siglaPortal"></param>
        /// <returns>retorna nulo en caso de error.</returns>
        BusinessResult<DateTime> ObtenerFechaPortal(string siglaPortal);
    }
}
