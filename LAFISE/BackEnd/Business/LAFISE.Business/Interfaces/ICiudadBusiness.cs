﻿/*==========================================================================
Archivo:            ICiudadBusiness
Descripción:        Interfaz de CiudadBusiness                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface ICiudadBusiness
    /// </summary>
    public interface ICiudadBusiness
    {
        /// <summary>
        /// Método usado para cargar todas las ciudades
        /// </summary>
        /// <returns>Lista de ciudades</returns>
        Task<BusinessResult<ICollection<CiudadDto>>> VerTodos();

        /// <summary>
        /// Método usado para cargar una ciudad por su id
        /// </summary>
        /// <param name="id">Identificador de la ciudad</param>
        /// <returns>Ciudad</returns>
        Task<BusinessResult<CiudadDto>> VerCiudadPorId(int id);

        /// <summary>
        /// Método usado para cargar todas las ciudades asociadas un departamento
        /// </summary>
        /// <param name="idDepartamento">Identificador del departamento</param>
        /// <returns>Ciudad</returns>
        Task<BusinessResult<ICollection<CiudadDto>>> VerCiudadesPorDepartamento(int idDepartamento);

        /// <summary>
        /// Metodo usado para cargar las ciudades asociadas al país seleccionado
        /// </summary>
        /// <param name="listaPais">lista del identificador de país</param>
        /// <returns>Lista de ciudades</returns>
        Task<BusinessResult<ICollection<CiudadDto>>> VerCiudadesPorPais(List<Int64> listaPais);
        /// <summary>
        /// Metodo para consultar las ciudades teniendo en cuenta la lista no asincrono
        /// </summary>
        /// <param name="listaCiudad">Lista de ciudad</param>
        /// <returns>Lista de ciudad</returns>
       BusinessResult<ICollection<CiudadDto>> VerCiudadesPorCiudadNoAsincrono(List<Int64> listaCiudad);

        /// <summary>
        /// Metodo para consultar las ciudades teniendo en cuenta la lista de departamento
        /// </summary>
        /// <param name="listaDepartamento">lista de departamento</param>
        /// <returns>lista de ciudad</returns>
       Task<BusinessResult<ICollection<CiudadDto>>> VerListaCiudadesPorDepartamento(List<Int64> listaDepartamento);
    }
}
