﻿#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Cotizador
    /// </summary>
    public interface ICotizadorBusiness
    {
        /// <summary>
        /// metodo que se encarga de crear un nuevo registro de cotizacion
        /// </summary>
        /// <param name="cotizador">instancia para Cotizador</param>
        /// <returns>cotizador</returns>
        Task<BusinessResult<CotizadorDto>> Crear(CotizadorDto cotizador);
    }
}
