﻿/*==========================================================================
Archivo:            ISolicitudBusiness
Descripción:        Interface de SolicitudBusiness                     
Autor:              paola.munoz                          
Fecha de creación:  14/10/2015 12:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using ARKIX.Business;
using System.Threading.Tasks;
using System;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// nterface encargada del manejo de logica de negocio para solicitud
    /// </summary>
    public interface ISolicitudBusiness
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de solicitud</returns>
        Task<BusinessResult<ICollection<SolicitudDto>>> VerTodos();

        /// <summary>
        /// Consultar el formulario general por id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        Task<BusinessResult<SolicitudDto>> VerFormularioGeneralPorIdSolicitud(Int64 id);

        /// <summary>
        /// Consultar el formulario de seguro de vida teniendo en cuenta el identificador de la solicitud
        /// </summary>
        /// <param name="id">identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        Task<BusinessResult<SolicitudDto>> VerFormularioSeguroVidaPorIdSolicitud(Int64 id);

        /// <summary>
        /// Consultar el formulario de los reclamos teniendo en cuenta el id de la solicitud
        /// </summary>
        /// <param name="id">identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        Task<BusinessResult<SolicitudDto>> VerFormularioReclamosPorIdSolicitud(Int64 id);

        /// <summary>
        /// Consultar los formularios con referencias teniendo en cuenta el identificador de la solicitud
        /// </summary>
        /// <param name="id">identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        Task<BusinessResult<SolicitudDto>> VerFormularioConReferenciasPorIdSolicitud(Int64 id);

        /// <summary>
        /// Crear solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        Task<BusinessResult<SolicitudDto>> CrearSolicitud(SolicitudDto solicitud);
        /// <summary>
        /// Editar solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        Task<BusinessResult<SolicitudDto>> EditarSolicitud(SolicitudDto solicitud, String backOfficeUser);
        /// <summary>
        /// Ver la gestion de la solicitud
        /// </summary>
        /// <param name="solicitudCondicion">identificador de los estados separados por coma</param>
        /// <returns>SolicitudFiltro</returns>
        Task<BusinessResult<ICollection<SolicitudFiltroDto>>> VerGestorSolicitud(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Consultar el formulario de seguro vehicular teniendo en cuenta el id de la solicitud
        /// </summary>
        /// <param name="id">identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        Task<BusinessResult<SolicitudDto>> VerFormularioSeguroVehicularPorIdSolicitud(Int64 id);
        /// <summary>
        /// Exportar marca auto
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>Exportar marca auto</returns>
        Task<BusinessResult<ICollection<ExportarMarcaAutoDto>>> CargarExportarMarcaAuto(SolicitudCondicionesDto solicitudCondicion);
        /// <summary>
        /// Exportar prestamo personal
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>ExportarPrestamoPersonalDto</returns>
        Task<BusinessResult<ICollection<ExportarPrestamoPersonalDto>>> CargarExportarPrestamoPersonal(SolicitudCondicionesDto solicitudCondicion);
        /// <summary>
        /// Exportar prestamo hipotecario
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>ExportarPrestamoHipotecarioDto</returns>
        Task<BusinessResult<ICollection<ExportarPrestamoHipotecarioDto>>> CargarExportarPrestamoHipotecario(SolicitudCondicionesDto solicitudCondicion);
        /// <summary>
        /// Exportar tarjeta de credito o debito
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>ExportarTarjetaCreditoDebitoDto</returns>
        Task<BusinessResult<ICollection<ExportarTarjetaCreditoDebitoDto>>> CargarExportarTarjetaCreditoDebito(SolicitudCondicionesDto solicitudCondicion);
        /// <summary>
        /// Exportar prestamo educativo
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>ExportarPrestamoEducativoDto</returns>
        Task<BusinessResult<ICollection<ExportarPrestamoEducativoDto>>> CargarExportarPrestamoEducativo(SolicitudCondicionesDto solicitudCondicion);
        /// <summary>
        /// Exportar apertura de cuenta
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>ExportarAperturaCuentaDto</returns>
        Task<BusinessResult<ICollection<ExportarAperturaCuentaDto>>> CargarExportarAperturaCuenta(SolicitudCondicionesDto solicitudCondicion);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>ExportarSeguroVidaAccidenteDto</returns>
        Task<BusinessResult<ICollection<ExportarSeguroVidaAccidenteDto>>> CargarExportarSeguroVidaAccidente(SolicitudCondicionesDto solicitudCondicion);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro</param>
        /// <returns>ExportarSeguroVehicularDto</returns>
        Task<BusinessResult<ICollection<ExportarSeguroVehicularDto>>> CargarExportarSeguroVehicular(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Ver información de los datos personales para el envio del correo
        /// </summary>
        /// <param name="idSolicitud">Identificador de la solicitud</param>
        /// <returns>DatosPersonalesDto</returns>
        BusinessResult<DatosPersonalesDto> VerDatosPersonales(Int64 idSolicitud);

        /// <summary>
        /// Exportar reclamos
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones filtro </param>
        /// <returns>ExportarReclamosDto</returns>
        Task<BusinessResult<ICollection<ExportarReclamosDto>>> CargarExportarReclamos(SolicitudCondicionesDto solicitudCondicion);
    }
}
