﻿/*==========================================================================
Archivo:            ICategoriaBusiness
Descripción:        Interface de ICategoriaBusiness               
Autor:              steven.echavarria                          
Fecha de creación:  13/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Categoria
    /// </summary>
    public interface ICategoriaBusiness
    {
        /// <summary>
        /// metodo que se encarga de consultar todos las categorias
        /// </summary>
        /// <param name="idTipoSolicitud">Identificador del tipo de la solicitud</param>
        /// <returns>Lista Categoria</returns>
        Task<BusinessResult<ICollection<CategoriaDto>>> VerTodosIdSolicitud(Int64 idTipoSolicitud);

        /// <summary>
        /// Metodo para ver la categoria po identificador
        /// </summary>
        /// <param name="id">Identificador de categoria</param>
        /// <returns>Categoria</returns>
        Task<BusinessResult<CategoriaDto>> VerCategoriaPorId(Int64 id);

        /// <summary>
        /// Metodo para ver las categorias por identificador de forma no asincrona
        /// </summary>
        /// <param name="id">identificador de la categoria</param>
        /// <returns>CategoriaDto</returns>
        BusinessResult<CategoriaDto> VerCategoriaPorIdNoAsincrono(Int64 id);
    }
}
