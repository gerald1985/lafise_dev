﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using ARKIX.Business;

namespace LAFISE.Business
{
    public interface IPROC_CISCOBusiness
    {
        Task<BusinessResult<ICollection<PROC_CISCO_CiudadesDto>>> VerCiudadesPROCTodos();
        Task<BusinessResult<ICollection<PROC_CISCO_PaisesDto>>> VerPaisesPROCTodos();
        Task<BusinessResult<ICollection<PROC_CISCO_OcupacionesDto>>> VerOcupacionesPROCTodos();
        Task<BusinessResult<ICollection<PROC_CISCO_Centro_De_CostoDto>>> VerSucursalesPROCTodos();

    }
}
