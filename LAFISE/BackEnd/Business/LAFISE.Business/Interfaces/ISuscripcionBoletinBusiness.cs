﻿/*==========================================================================
Archivo:            ISuscripcionBoletinBusiness
Descripción:        Interface de SuscripcionBoletinBusiness                      
Autor:              steven.echavarria                          
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                       
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para SuscripcionBoletin
    /// </summary>
    public interface ISuscripcionBoletinBusiness
    {
        /// <summary>
        /// Método usado para crear una Suscripción al boletín
        /// </summary>
        /// <param name="suscripcion">SuscripcionBoletin</param>
        /// <returns>SuscripcionBoletin</returns>
        Task<BusinessResult<SuscripcionBoletinDto>> Crear(SuscripcionBoletinDto suscripcion);

        /// <summary>
        /// Metodo que valida que el email ya esté registrado en las suscripciones
        /// </summary>
        /// <param name="suscripcion">entidad suscripcion</param>
        /// <returns>suscripcion</returns>
        Task<BusinessResult<SuscripcionBoletinDto>> ValidarSuscripcionPorEmail(SuscripcionBoletinDto suscripcion);
    }
}
