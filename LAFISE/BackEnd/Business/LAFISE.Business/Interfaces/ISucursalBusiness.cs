﻿/*==========================================================================
Archivo:            ISucursalBusiness
Descripción:        interface de SucursalBusiness                
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 5:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using ARKIX.Business;
using System.Threading.Tasks;
using System;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Sucursal
    /// </summary>
    public interface ISucursalBusiness
    {
        /// <summary>
        /// Ver sucursales
        /// </summary>
        /// <returns>Lista Sucursal</returns>
        Task<BusinessResult<ICollection<SucursalDto>>> VerTodosGrid();
        /// <summary>
        /// Ver sucursales por pais
        /// </summary>
        /// <param name="url">ruta con la cual se consulta el identificador del pais</param>
        /// <returns></returns>
        Task<BusinessResult<ICollection<SucursalDto>>> VerTodosPorPais(string url);
        /// <summary>
        /// Ver sucursal por identificador
        /// </summary>
        /// <param name="id">Identificador de la sucursal</param>
        /// <returns>Sucursal</returns>
        Task<BusinessResult<SucursalDto>> VerPorId(Int64 id);
        /// <summary>
        /// Ver sucursal por identificador no asincrono
        /// </summary>
        /// <param name="id">identificador de la sucursal</param>
        /// <returns>SucursalDto</returns>
        BusinessResult<SucursalDto> VerPorIdNoAsincrono(Int64 id);
        /// <summary>
        /// Eliminar sucursal por identificador 
        /// </summary>
        /// <param name="id">Identificador de la sucursal</param>
        ///  <param name="usuario">usuario logueado</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario);
        /// <summary>
        /// Crear sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        Task<BusinessResult<SucursalDto>> Crear(SucursalDto sucursal);
        /// <summary>
        /// Editar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        Task<BusinessResult<SucursalDto>> Editar(SucursalDto sucursal);
        /// <summary>
        /// Inactivar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Inactivar(SucursalDto sucursal);
        /// <summary>
        /// Consultar por filtro las sucursales
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>SucursalFiltro</returns>
        Task<BusinessResult<ICollection<SucursalFiltroDto>>> SucursalFiltro(SucursalFiltroDto sucursal);

        /// <summary>
        /// Validar que lasucursal sea unico
        /// </summary>
        /// <param name="sucursal">Entidad  sucursal</param>
        /// <returns>Entidad  sucursal</returns>
        Task<BusinessResult<SucursalDto>> ValidarSucursalUnica(SucursalDto sucursal);

        /// <summary>
        /// Interfaz que se encarga de consultar todas las sucursales activas
        /// </summary>
        /// <returns>Sucursales activas</returns>
        Task<BusinessResult<ICollection<SucursalDto>>> VerTodosActivos();
    }
}