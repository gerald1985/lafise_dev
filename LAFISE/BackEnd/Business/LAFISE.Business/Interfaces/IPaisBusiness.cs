﻿/*==========================================================================
Archivo:            IPaisBusiness
Descripción:        Interface de PaisBusiness                 
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Pais
    /// </summary>
    public interface IPaisBusiness
    {
        /// <summary>
        /// Método usado para cargar todos los paises
        /// </summary>
        /// <returns>Lista de paises</returns>
        Task<BusinessResult<ICollection<PaisDto>>> VerTodos();

        /// <summary>
        /// Método usado para cargar un pais por su id
        /// </summary>
        /// <param name="id">Identificador del pais</param>
        /// <returns>Pais</returns>
        Task<BusinessResult<PaisDto>> VerPaisPorId(int id);

        /// <summary>
        /// Metodo que se encargado de consultar el país por nombre
        /// </summary>
        /// <param name="nombre">Nombre país</param>
        /// <returns>Identificador del país</returns>
        Task<BusinessResult<Int64>> VerPaisPorNombre(string nombre);

        /// <summary>
        /// Metodo que se encargado de consultar el país por nombre no asincrono
        /// </summary>
        /// <param name="nombre">Nombre país</param>
        /// <returns>Identificador del país</returns>
        BusinessResult<Int64> VerPaisPorNombreNoAsincrono(string nombre);

        /// <summary>
        /// Metodo encargado de consultar los paises teniendo en cuenta la lista
        /// </summary>
        /// <param name="listaPais">Lista de paises</param>
        /// <returns>Lista de paises</returns>
        BusinessResult<ICollection<PaisDto>> VerPaisPorListaIdNoAsincrono(List<Int64> listaPais);
    }
}
