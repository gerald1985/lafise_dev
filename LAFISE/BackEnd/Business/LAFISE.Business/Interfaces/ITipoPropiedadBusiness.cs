﻿/*==========================================================================
Archivo:            ITipoPropiedadBusiness
Descripción:        Interface de TipoPropiedadBusiness                 
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 10:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias

using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{

    /// <summary>
    /// Interface encargada del manejo de logica de negocio para TipoPropiedadBusiness
    /// </summary>
    public interface ITipoPropiedadBusiness
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de tipo propiedad</returns>
        Task<BusinessResult<ICollection<TipoPropiedadDto>>> VerTodosGrid();
        /// <summary>
        /// Método usado para cargar los tipos de propiedades que están activas
        /// </summary>
        /// <returns>Lista de tipo propiedad</returns>
        Task<BusinessResult<ICollection<TipoPropiedadDto>>> VerTodosActivos();
        /// <summary>
        /// Consultar tipo propiedad por identificador
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        /// <returns>tipo propiedad</returns>
        Task<BusinessResult<TipoPropiedadDto>> VerPorId(long id);
        /// <summary>
        /// Consultar tipo propiedad por identificador no asincrono
        /// </summary>
        /// <param name="Id">Identificador de tipo propiedad</param>
        /// <returns>tipo propiedad</returns>
        BusinessResult<TipoPropiedadDto> VerPorIdNoAsincrono(long Id);
        /// <summary>
        /// Método usado para cargar los tipos de propiedad asociados a una urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de urbanizadora</param>
        /// <returns>Lista de tipo propiedad</returns>
        Task<BusinessResult<ICollection<TipoPropiedadDto>>> VerTiposPropiedadPorUrbanizadora(long idUrbanizadora);
        /// <summary>
        /// Eliminar tipo propiedad
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        ///  <param name="usuario">usuario logueado</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Eliminar(long id, string usuario);
        /// <summary>
        /// Crear tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        Task<BusinessResult<TipoPropiedadDto>> Crear(TipoPropiedadDto tipoPropiedad);
        /// <summary>
        /// Editar tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        Task<BusinessResult<TipoPropiedadDto>> Editar(TipoPropiedadDto tipoPropiedad);
        /// <summary>
        /// Consultar por filtro
        /// </summary>
        /// <param name="tipoPropiedad">nombre de Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        Task<BusinessResult<ICollection<TipoPropiedadDto>>> TipoPropiedadFiltro(string tipoPropiedad);
        /// <summary>
        /// Inactivar tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>string </returns>
        Task<BusinessResult<string>> Inactivar(TipoPropiedadDto tipoPropiedad);
        /// <summary>
        /// Validar que el nombre de la propiedad sea unico.
        /// </summary>
        ///  <param name="Id">Identificador tipo propiedad</param>
        /// <param name="Nombre">Nombre del tipo de propiedad.</param>
        /// <returns>Entidad con el tipo propiedad encontrada.</returns>
        Task<BusinessResult<TipoPropiedadDto>> ValidarTipoPropiedadUnica(long Id, string Nombre);

        /// <summary>
        /// Consulta la validacion del tipo de propiedad
        /// </summary>
        /// <param name="Id">Identificador de tipo de propiedad</param>
        /// <returns>Validar urbanizadora</returns>
        Task<BusinessResult<TipoPropiedadDto>> ValidarRelaciones(long Id);

    }
}
