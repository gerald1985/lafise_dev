﻿/*==========================================================================
Archivo:            IDepartamentoBusiness
Descripción:        interfaz de DepartamentoBusiness                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                     
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Departamento
    /// </summary>
    public interface IDepartamentoBusiness
    {
        /// <summary>
        /// Método usado para cargar todos los departamentos
        /// </summary>
        /// <returns>Lista de departamentos</returns>
        Task<BusinessResult<ICollection<DepartamentoDto>>> VerTodos();

        /// <summary>
        /// Método usado para cargar un departamento por su id
        /// </summary>
        /// <param name="id">Identificador del departamento</param>
        /// <returns>Departamento</returns>
        Task<BusinessResult<DepartamentoDto>> VerDepartamentoPorId(int id);

        /// <summary>
        /// Método usado para cargar todos los departamentos aociados a un pais
        /// </summary>
        /// <param name="idPais">Identificador del pais</param>
        /// <returns>Lista de departamentos</returns>
        Task<BusinessResult<ICollection<DepartamentoDto>>> VerDepartamentosPorPais(int idPais);

        /// <summary>
        /// Metod para ver una lista de departamentos teniendo en cuenta una lista de paises
        /// </summary>
        /// <param name="listaPais">lista de paises</param>
        /// <returns>lista de departamento</returns>
        Task<BusinessResult<ICollection<DepartamentoDto>>> VerListaDepartamentoPorPais(List<Int64> listaPais);
    }
}
