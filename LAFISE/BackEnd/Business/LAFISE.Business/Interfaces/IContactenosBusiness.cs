﻿/*==========================================================================
Archivo:            IContactenosBusiness
Descripción:        Interfaz de ContactenosBusiness                   
Autor:              paola.munoz                          
Fecha de creación:  13/10/2015 12:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using ARKIX.Business;
using System.Threading.Tasks;
using System;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para contactenos
    /// </summary>
    public interface IContactenosBusiness
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista contactenos</returns>
        Task<BusinessResult<ICollection<ContactenosDto>>> VerTodosGrid();
        /// <summary>
        /// Consultar contactenos por identificador
        /// </summary>
        /// <param name="id">Identificador de contactenos</param>
        /// <returns>Contactenos</returns>
        Task<BusinessResult<ContactenosDto>> VerPorId(Int64 id);
        /// <summary>
        /// Eliminar contactenos
        /// </summary>
        /// <param name="id">Identificador de contactenos</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario);
        /// <summary>
        /// Crear contactenos
        /// </summary>
        /// <param name="contactenos">Contactenos</param>
        /// <returns>Contactenos</returns>
        Task<BusinessResult<ContactenosDto>> Crear(ContactenosDto contactenos);
        /// <summary>
        /// Editar contactenos
        /// </summary>
        /// <param name="contactenos">Contactenos</param>
        /// <returns>Contactenos</returns>
        Task<BusinessResult<ContactenosDto>> Editar(ContactenosDto contactenos);
 
    }
}
