﻿/*==========================================================================
Archivo:            IParametrosBusiness
Descripción:        Interfaz de ParametrosBusiness                      
Autor:              paola.munoz                          
Fecha de creación:  28/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Business
{
     /// <summary>
    /// IParametrosBusiness encargada de realizar la logica de negocio para los parametros.
    /// </summary>
    public interface IParametrosBusiness
    {
       /// <summary>
       /// Método encargado de obtener la ruta de la imagen.
       /// </summary>
       /// <param name="rutaArchivo">parametro</param>
       /// <returns>Ruta solicitada</returns>
        Task<BusinessResult<string>> ObtenerParametroRuta(string rutaArchivo);

        /// <summary>
        /// Método encargado de obtener la informacion no asincrona.
        /// </summary>
        /// <param name="informa">Parametro inicial</param>
        /// <returns>Información solicitada</returns>
        BusinessResult<string> ObtenerParametroRutaNoAsincrono(string informa);
    }
}
