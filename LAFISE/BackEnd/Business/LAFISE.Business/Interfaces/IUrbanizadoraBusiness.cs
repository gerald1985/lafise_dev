﻿/*==========================================================================
Archivo:            IUrbanizadoraBusiness
Descripción:        Interface de UrbanizadoraBusiness                      
Autor:              steven.echavarria                          
Fecha de creación:  30/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Urbanizadora
    /// </summary>
    public interface IUrbanizadoraBusiness
    {
        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activas
        /// </summary>
        /// <returns>Lista de urbanizadora</returns>
        Task<BusinessResult<ICollection<UrbanizadoraDto>>> VerTodosActivos();

        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activa y por país
        /// </summary>
        /// <param name="pathIdPais">identificador del país</param>
        /// <returns></returns>
        Task<BusinessResult<ICollection<UrbanizadoraDto>>> VerTodosActivosPorPais(string pathIdPais);

        /// <summary>
        /// Consultar urbanizadora por tipo propiedad
        /// </summary>
        /// <param name="urbanizadoraTipoPropiedad">UrbanizadoraTipoPropiedadDto</param>
        /// <returns>Lista UrbanizadoraTipoPropiedadDto</returns>
        Task<BusinessResult<ICollection<UrbanizadoraTipoPropiedadDto>>> VerUrbanizadorasTiposPropiedades(UrbanizadoraTipoPropiedadDto urbanizadoraTipoPropiedad);

        /// <summary>
        /// Método usado para cargar una Urbanizadora por id
        /// </summary>
        /// <param name="id">Identificador de urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<BusinessResult<UrbanizadoraDto>> VerUrbanizadoraPorId(Int64 id);

        /// <summary>
        /// Método usado para cargar una Urbanizadora por identificador no asincrona
        /// </summary>
        /// <param name="id">Identificador de urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        BusinessResult<UrbanizadoraDto> VerUrbanizadoraPorIdNoAsincrono(Int64 id);

        /// <summary>
        /// Metodo que valida que el nombre de la urbanizadora sea unico.
        /// </summary>
        /// <param name="urbanizadora">entidad urbanizadora</param>
        /// <returns>urbanizadora</returns>
        Task<BusinessResult<UrbanizadoraDto>> ValidarUrbanizadoraPorNombre(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Método usado para eliminar una Urbanizadora por id
        /// </summary>
        /// <param name="id">Identificador de urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<BusinessResult<UrbanizadoraDto>> EliminarUrbanizadoraPorId(int id, string usuario);

        /// <summary>
        /// Crear urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<BusinessResult<UrbanizadoraDto>> CrearUrbanizadora(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Editar urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<BusinessResult<UrbanizadoraDto>> EditarUrbanizadora(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Método que permite inactivar una urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>urbanizadora</returns>
        Task<BusinessResult<UrbanizadoraDto>> InactivarUrbanizadora(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Consultar el detalle de la urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<BusinessResult<UrbanizadoraDto>> ConsultarDetalleUrbanizadora(long idUrbanizadora);

        /// <summary>
        /// Eliminar el detalle de la urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de urbanizadora</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> EliminarDetalleUrbanizadora(long idUrbanizadora, string usuario);
    }
}
