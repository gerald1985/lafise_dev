﻿/*==========================================================================
Archivo:            ITipoVehiculoBusiness
Descripción:        Interface de TipoVehiculoBusiness                    
Autor:              juan.hincapie                          
Fecha de creación:  06/10/2015 5:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARKIX.Business;
using LAFISE.Entities.Dtos;

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para  TipoVehiculoBusiness
    /// </summary>
    public interface ITipoVehiculoBusiness
    {
        /// <summary>
        /// Ver todos los tipos de sucursal
        /// </summary>
        /// <returns>Lista de tipo de sucursal</returns>
        Task<BusinessResult<ICollection<TipoVehiculoDto>>> VerTodosGrid();

        /// <summary>
        /// Metodo que se encarga de consultar el tipo vehiculo teniendo en cuenta el identificador
        /// </summary>
        /// <param name="id">identificador de tipo vehiculo</param>
        /// <returns>TipoVehiculoDto</returns>
        BusinessResult<TipoVehiculoDto> VerPorIdNoAsincrono(Int64 id);
    }
}
