﻿/*==========================================================================
Archivo:            IPrimaCategoriaPaisBusiness
Descripción:        Interfaz de PrimaCategoriaPaisBusiness                    
Autor:              steven.echavarria                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business.Interfaces
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para obtener el porcentaje de prima por categoría y país
    /// </summary>
    public interface IPrimaCategoriaPaisBusiness
    {
        /// <summary>
        /// Método encargado de consultar el porcentaje de prima por categoria y país
        /// </summary>
        /// <param name="primaCategoriaPais">entidad prima categoria pais</param>
        /// <returns>Entidad PrimaCategoriaPaisDto</returns>
        BusinessResult<PrimaCategoriaPaisDto> VerPorcentajePrimaPorCategoriaPais(PrimaCategoriaPaisDto primaCategoriaPais);
    }
}
