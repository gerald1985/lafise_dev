﻿/*==========================================================================
Archivo:            ITarjetaBusiness
Descripción:       interface TarjetaBusiness              
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 12:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using ARKIX.Business;
using System.Threading.Tasks;
using System;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para tarjeta
    /// </summary>
    public interface ITarjetaBusiness
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de tarjeta</returns>
        Task<BusinessResult<ICollection<TarjetaDto>>> VerTodosGrid();
        /// <summary>
        /// Consultar tarjeta por identificador
        /// </summary>
        /// <param name="id">Identificador de tarjeta</param>
        /// <returns>Tarjeta</returns>
        Task<BusinessResult<TarjetaDto>> VerPorId(Int64 id);
        /// <summary>
        /// Eliminar tarjeta
        /// </summary>
        /// <param name="id">Identificador de tarjeta</param>
        ///  <param name="usuario">usuario logueado</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario);
        /// <summary>
        /// Crear tarjeta
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>Tarjeta</returns>
        Task<BusinessResult<TarjetaDto>> Crear(TarjetaDto tarjeta);
        /// <summary>
        /// Editar
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>Tarjeta</returns>
        Task<BusinessResult<TarjetaDto>> Editar(TarjetaDto tarjeta);
        /// <summary>
        /// Consultar por filtro
        /// </summary>
        /// <param name="tarjeta">nombre tarjeta</param>
        /// <returns>Tarjeta</returns>
        Task<BusinessResult<ICollection<TarjetaDto>>> TarjetaFiltro(string tarjeta);
        /// <summary>
        /// Inactivar
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Inactivar(TarjetaDto tarjeta);

        /// <summary>
        /// Validar que la tarjeta sea unica
        /// </summary>
        /// <param name="tarjeta">Entidad tarjeta</param>
        /// <returns>Entidad tarjeta</returns>
        Task<BusinessResult<TarjetaDto>> ValidarTarjetaUnica(TarjetaDto tarjeta);
    }
}
