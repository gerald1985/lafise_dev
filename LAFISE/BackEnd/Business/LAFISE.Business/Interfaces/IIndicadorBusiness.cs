﻿/*==========================================================================
Archivo:            IIndicadorBusiness
Descripción:        Interfaz de Indicador           
Autor:              carlos.arboleda                         
Fecha de creación:  12/02/2016 05:46:10 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    ///Interface encargada del manejo de logica de negocio para los indicadores
    /// </summary>
    public interface IIndicadorBusiness
    {
        /// <summary>
        /// Método encargado de consultar todos los indicadores activos
        /// </summary>
        BusinessResult<ICollection<IndicadorDto>> VerTodosActivos();

        /// <summary>
        /// Método encargado de consultar todos los indicadores activos y no activos
        /// </summary>
        Task<BusinessResult<ICollection<IndicadorDto>>> VerTodos();

        /// <summary>
        /// Crear indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        Task<BusinessResult<IndicadorDto>> Crear(IndicadorDto indicador);

        /// <summary>
        /// Editar indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        Task<BusinessResult<IndicadorDto>> Editar(IndicadorDto indicador);

        /// <summary>
        /// Inactivar indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Inactivar(IndicadorDto indicador);

        /// <summary>
        /// Eliminar indicador por identificador 
        /// </summary>
        /// <param name="id">Identificador del indicador</param>
        /// <param name="usuario">usuario logueado</param>
        /// <returns>bool</returns>
        Task<BusinessResult<bool>> Eliminar(Int64 id, string usuario);

        /// <summary>
        /// Validar que el tipo de indicador sea unico
        /// </summary>
        /// <param name="indicador">Entidad indicador</param>
        /// <returns>Entidad indicador</returns>
        Task<BusinessResult<IndicadorDto>> ValidarIndicadorUnico(IndicadorDto indicador);
    }
}
