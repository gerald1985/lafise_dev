﻿/*==========================================================================
Archivo:            ITipoActividadBusiness
Descripción:        Interface de TipoActividadBusiness                    
Autor:              juan.hincapie                          
Fecha de creación:  06/10/2015 5:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARKIX.Business;
using LAFISE.Entities.Dtos;

namespace LAFISE.Business
{
    /// <summary>
    /// Interface de TipoActividadBusiness     
    /// </summary>
    public interface ITipoActividadBusiness
    {
        /// <summary>
        /// Ver todos los tipos de actividad
        /// </summary>
        /// <returns>Lista de tipo de actividad</returns>
        Task<BusinessResult<ICollection<TipoActividadDto>>> VerTodosGrid();
    }
}
