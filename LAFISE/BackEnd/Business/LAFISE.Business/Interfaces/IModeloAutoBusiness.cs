﻿/*==========================================================================
Archivo:            IModeloAutoBusiness
Descripción:        Interface de ModeloAutoBusiness               
Autor:              paola.munoz                         
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Garantia
    /// </summary>
    public interface IModeloAutoBusiness
    {
        /// <summary>
        /// metodo que se encarga de consultar todos las modelos auto
        /// </summary>
        /// <returns>Lista modelo auto</returns>
        Task<BusinessResult<ICollection<ModeloAutoDto>>> VerTodos();
        /// <summary>
        /// Metodo para consultar el modelo auto por identificador
        /// </summary>
        /// <param name="id">Identificador del modelo auto</param>
        /// <returns>ModeloAutoDto</returns>
        Task<BusinessResult<ModeloAutoDto>> VerModeloAutoPorId(Int64 id);
        /// <summary>
        /// Metodo para consultar el modelo auto por identificador no asincrono
        /// </summary>
        /// <param name="id">Identificador del modelo auto</param>
        /// <returns>ModeloAutoDto</returns>
        BusinessResult<ModeloAutoDto> VerModeloAutoPorIdNoAsincrono(Int64 id);

        /// <summary>
        /// Metodo para ver todas los modelos autos por marca
        /// </summary>
        /// <param name="Marca">Id marca</param>
        /// <returns>Lista de modelos</returns>
        Task<BusinessResult<ICollection<ModeloAutoDto>>> VerModelosPorMarca(long Marca);
    }
}
