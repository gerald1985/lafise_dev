﻿/*==========================================================================
Archivo:            IGarantiaBusiness
Descripción:        Interface de IGarantiaBusiness               
Autor:              paola.munoz                         
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Business
{
    /// <summary>
    /// Interface encargada del manejo de logica de negocio para Garantia
    /// </summary>
    public interface IGarantiaBusiness
    {
        /// <summary>
        /// metodo que se encarga de consultar todos las garantias
        /// </summary>
        /// <returns>Lista Garantia</returns>
        Task<BusinessResult<ICollection<GarantiaDto>>> VerTodos();
        /// <summary>
        /// Metodo para consultar la garantia por identificador
        /// </summary>
        /// <param name="id">Identificador de la garantia</param>
        /// <returns>Garantia</returns>
        Task<BusinessResult<GarantiaDto>> VerGarantiaPorId(Int64 id);

        /// <summary>
        /// Metodo no asincrono para ver la garantia por el identificadr
        /// </summary>
        /// <param name="id">identificador de la garantia</param>
        /// <returns>GarantiaDto</returns>
        BusinessResult<GarantiaDto> VerGarantiaPorIdNoAsincrono(Int64 id);
    }
}
