﻿/*==========================================================================
Archivo:            IModeloAutoData
Descripción:        interfaz de la clase ModeloAutoData              
Autor:              paola.munoz                          
Fecha de creación:  17/11/2015 
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion


namespace LAFISE.Data
{
    public interface IModeloAutoData
    {
        /// <summary>
        /// Metodo para consultar todos los modelos auto
        /// </summary>
        /// <returns>Lista de modelo auto</returns>
        Task<ICollection<ModeloAutoDto>> VerTodos();

        /// <summary>
        /// Metodo para ver el modelo auto por identificador
        /// </summary>
        /// <param name="id">Identificador del modelo auto</param>
        /// <returns>ModeloAutoDto</returns>
        Task<ModeloAutoDto> VerModeloAutoPorId(Int64 id);
        /// <summary>
        /// Metodo para ver el modelo auto por identificador no asincrono
        /// </summary>
        /// <param name="id">Identificador del modelo auto</param>
        /// <returns>ModeloAutoDto</returns>
        ModeloAutoDto VerModeloAutoPorIdNoAsincrono(Int64 id);

        /// <summary>
        /// Metodo para ver todas los modelos autos por marca
        /// </summary>
        /// <param name="Marca">Id marca</param>
        /// <returns>Lista de modelos</returns>
        Task<ICollection<ModeloAutoDto>> VerModelosPorMarca(long Marca);
    }
}
