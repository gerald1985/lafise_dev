/*==========================================================================
Archivo:            ICasaComercialData
Descripción:        interfaz de la clase CasaComercialData              
Autor:              Juan.Hincapie                           
Fecha de creación:  9/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using ARKIX.Data.EntityFramework.Interfaces;
using System.Threading.Tasks;
using System;

#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para las casas comerciales.
    /// </summary>	
    public interface ICasaComercialData
    {
        /// <summary>
        /// Método encargado de consultar todas las casas comerciales.
        /// </summary>
        /// <returns>Lista con todas las casas comerciales.</returns>
        Task<ICollection<CasaComercialDto>> ConsultarTodosGrid();

        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>Entidad CasaComercial.</returns>
        Task<CasaComercialDto> VerCasaComercialPorId(long id);

        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>Entidad CasaComercial.</returns>
        CasaComercialDto VerCasaComercialPorIdNoAsincrono(long id);

        /// <summary>
        /// Metodo que valida que el nombre de la casa comercial sea unico.
        /// </summary>
        /// <param name="casacomercial">entidad casa comercial</param>
        /// <returns>casacomercial</returns>
        Task<CasaComercialDto> ValidarCasaComercialPorNombre(CasaComercialDto casacomercial);

        /// <summary>
        /// Método encargado de eliminar las casas comerciales según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar</returns>
        Task<bool> EliminarCasaComercialPorId(long id);

        /// <summary>
        /// Método encargado de crear las casas comerciales y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Entidad CasaComercial que se insertó.</returns>
        Task<CasaComercialDto> CrearCasaComercial(CasaComercialDto casacomercial);

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Entidad CasaComercial que se editó.</returns>
        Task<CasaComercialDto> EditarCasaComercial(CasaComercialDto casacomercial);

        /// <summary>
        /// Método encargado de consultar las casas comerciales según los filtros de búsqueda
        /// </summary>
        /// <param name="casaComercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Lista con todas las casas comerciales.</returns>
        Task<ICollection<CasaComercialFiltroDto>> ConsultarCasaComercialFiltros(CasaComercialDto casaComercial);

        /// <summary>
        /// Método encargado de consultar los modelos según los filtros de las marcas
        /// </summary>
        /// <param name="marcas">id de las marcas separadas por comas</param>
        /// <returns>lista de modelos agrupados por marca</returns>
        Task<ICollection<CasaComercialModeloMarcaFiltroDto>> ConsultarCasaComercialModeloMarcaFiltros(string marcas);

        /// <summary>
        /// Método encargado de inactivar las casas comerciales.
        /// </summary>
        /// <param name="casaComercialDto">Dto con las propiedades de la casa comercial.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        Task<CasaComercialDto> InactivarCasaComercial(CasaComercialDto casaComercialDto);

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial.
        /// </summary>
        /// <param name="idCasaComercial">Identificador de la casa comercial.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        Task<bool> EliminarDetalleCasaComercial(long idCasaComercial);

        /// <summary>
        /// Método encargado de consultar el detalle de la casa comercial.
        /// </summary>
        /// <param name="idCasaComercial">Identificador de la casa comercial.</param>
        /// <returns>detalle de la casa comercial</returns>
        Task<CasaComercialDto> ConsultarDetalleCasaComercial(long idCasaComercial);

        /// <summary>
        /// Método encargado de consultar las casas comerciales según el pais.
        /// </summary>
        /// <param name="idPais">Identificador del pais asociado a la casa comercial.</param>
        /// <returns>Lista de las casas comerciales.</returns>
        Task<ICollection<CasaComercialDto>> ConsultarCasaComercialPorPais(long idPais);

        /// <summary>
        /// Método que identifica si hay una casa comercial asociada a un plan financiero
        /// </summary>
        /// <param name="idCasaComercial">Identificador de la casa comercial</param>
        /// <returns>bool</returns>
        Task<bool> ExistePlanFinanciacionPorCasaComercial(long idCasaComercial);

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="Modelo">Identificador del modelo.</param>
        /// <returns>Entidad CasaComercial.</returns>
        Task<ICollection<CasaComercialDto>> VerCasaComercialPorModelo(long Modelo);

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="entidad">entidad CasaComercialModeloPaisDto</param>
        /// <returns></returns>
        Task<ICollection<CasaComercialModeloPaisDto>> ConsultarCasaComercialModeloPais(CasaComercialModeloPaisDto entidad);

    }
}