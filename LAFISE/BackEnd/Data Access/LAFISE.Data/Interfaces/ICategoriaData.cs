﻿/*==========================================================================
Archivo:            ICategoriaData
Descripción:        interfaz de la clase CategoriaData              
Autor:              steven.echavarria                           
Fecha de creación:  13/11/2015 
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    public interface ICategoriaData
    {
        /// <summary>
        /// Metodo encargado para ver todos las categorias
        /// </summary>
        /// <param name="idTipoSolicitud">Identificador del tipo de la solicitud</param>
        /// <returns>Lista Categoria</returns>
        Task<ICollection<CategoriaDto>> VerTodosIdSolicitud(Int64 idTipoSolicitud);
        /// <summary>
        /// Metodo encargado de ver una categoria por Id
        /// </summary>
        /// <param name="id">Identificador de la categoria</param>
        /// <returns>Categoria</returns>
        Task<CategoriaDto> VerCategoriaPorId(Int64 id);

        /// <summary>
        /// Metodo para ver la categoria por el identificador no asincrona
        /// </summary>
        /// <param name="id">Identificador de la categoria</param>
        /// <returns>CategoriaDto</returns>
        CategoriaDto VerCategoriaPorIdNoAsincrona(Int64 id);
    }
}
