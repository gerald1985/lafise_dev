﻿/*==========================================================================
Archivo:            ITasaCambioData
Descripción:      Interfaz de la tasa de cambio            
Autor:              paola.munoz                         
Fecha de creación:  25/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

#endregion
namespace LAFISE.Data
{
    /// <summary>
    /// Interfaz de la tasa de cambio
    /// </summary>
    public interface ITasaCambioData
    {
        /// <summary>
        /// Llamado de la tasa de cambio
        /// </summary>
        /// <param name="idPais">Identificador del pais</param>
        /// <returns></returns>
        Task<ICollection<TasaCambioDto>> VerPorPaisActivo(Int64 idPais);
    }
}
