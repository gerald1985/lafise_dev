﻿/*==========================================================================
Archivo:            ITipoSucursalData
Descripción:        Interfaz TipoSucursalData                    
Autor:              paola.munoz                          
Fecha de creación:  06/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Interface encargada de realizar el acceso a datos para  TipoSucursalData
    /// </summary>
    public interface ITipoSucursalData
    {
        /// <summary>
        /// Metodo para ver todos
        /// </summary>
        /// <returns>Lista tipo sucursal</returns>
        Task<ICollection<TipoSucursalDto>> VerTodosGrid();

        /// <summary>
        /// Metodo para consultar por id
        /// </summary>
        /// <param name="id">Identificador para tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        Task<TipoSucursalDto> VerPorId(Int64 id);

        /// <summary>
        /// Eliminar tipo sucursal
        /// </summary>
        /// <param name="id">Identificador para tipo sucursal</param>
        Task Eliminar(Int64 id);
        /// <summary>
        /// Crear tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        Task<TipoSucursalDto> Crear(TipoSucursalDto tipoSucursal);

        /// <summary>
        /// Editar tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        Task<TipoSucursalDto> Editar(TipoSucursalDto tipoSucursal);

        /// <summary>
        /// Consultar por filtro de tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">tipo sucursal</param>
        /// <returns>Lista de tipo sucursal</returns>
        Task<ICollection<TipoSucursalDto>> TipoSucursalFiltro(string tipoSucursal);

        /// <summary>
        /// Inactivar tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>bool</returns>
        Task<bool> Inactivar(TipoSucursalDto tipoSucursal);

        /// <summary>
        /// Validar el tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>SucursalValidarDto</returns>
        Task<SucursalValidarDto> ValidarTipoSucursal(Int64 tipoSucursal);

        /// <summary>
        /// Validar que el nombre del tipo sucursal sea unico
        /// </summary>
        /// <param name="tipoSucursal">Entidad tipo sucursal</param>
        /// <returns>Entidad de tipo sucursal</returns>
        Task<TipoSucursalDto> ValidarTipoSucursalUnica(TipoSucursalDto tipoSucursal);
        /// <summary>
        /// Metodo para consultar todos los tipos sucursales activos
        /// </summary>
        /// <returns></returns>
        Task<ICollection<TipoSucursalDto>> VerTodosActivas();
    }
}
