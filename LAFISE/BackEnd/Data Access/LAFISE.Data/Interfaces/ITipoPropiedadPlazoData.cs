﻿/*==========================================================================
Archivo:            ITipoPropiedadPlazoData
Descripción:        Interfaz de la clase PrimaCategoriaPaisData                          
Autor:              steven.echavarria                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para la consulta de plazo por tipo de propiedad
    /// </summary>
    public interface ITipoPropiedadPlazoData
    {
        /// <summary>
        /// Método encargado de consultar el plazo de un tipo de propiedad
        /// </summary>
        /// <param name="idTipoPropiedad">identificador del tipo de propiedad</param>
        /// <returns>Entidad TipoPropiedadPlazoDto</returns>
        TipoPropiedadPlazoDto VerPlazoPorTipoPropiedad(long idTipoPropiedad);
    }
}
