﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;

namespace LAFISE.Data
{
    public interface IPROC_CISCO_CiudadesData
    {
        Task<ICollection<PROC_CISCO_CiudadesDto>> VerTodos();
    }
}
