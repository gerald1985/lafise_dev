﻿/*==========================================================================
Archivo:            IPaisData
Descripción:        Interfaz para PaisData                     
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para PaisData
    /// </summary>
    public interface IPaisData
    {
        /// <summary>
        /// Método usado para cargar todos los paises
        /// </summary>
        /// <returns>Lista de paises</returns>
        Task<ICollection<PaisDto>> VerTodos();

        /// <summary>
        /// Método usado para cargar un pais por su id
        /// </summary>
        /// <param name="id">Identificador del pais</param>
        /// <returns>Pais</returns>
        Task<PaisDto> VerPaisPorId(int id);

        /// <summary>
        /// Metodo que retorna el identificador del pais
        /// </summary>
        /// <param name="nombre">Nombre pais</param>
        /// <returns>Identificador del pais</returns>
        Task<Int64> VerPaisPorNombre(string nombre);

        /// <summary>
        /// Metodo que retorna el identificador del pais no asincrono
        /// </summary>
        /// <param name="nombre">Nombre pais</param>
        /// <returns>Identificador del pais</returns>
        Int64 VerPaisPorNombreNoAsincrono(string nombre);

        /// <summary>
        /// Metodo que se encarga de consultar los paises teniendo en cuenta una lista
        /// </summary>
        /// <param name="listaPais">Lista de paises</param>
        /// <returns>Lista PaisDto</returns>
        ICollection<PaisDto> VerPaisPorListaIdNoAsincrono(List<Int64> listaPais);
    }
}
