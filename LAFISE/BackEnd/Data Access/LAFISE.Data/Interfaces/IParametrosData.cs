﻿/*==========================================================================
Archivo:            IParametrosData
Descripción:        Interfaz de ParametrosData                     
Autor:              paola.munoz                  
Fecha de creación:  28/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion


namespace LAFISE.Data
{
    /// <summary>
    ///  Interface encargada de realizar el acceso a datos para ParametrosData
    /// </summary>
    public interface IParametrosData
    {
       /// <summary>
        /// Método encargado de obtener la ruta de la imagen.
       /// </summary>
       /// <param name="rutaArchivos">Palabra clave de la ruta</param>
       /// <returns>Ruta solicitada</returns>
        Task<string> ObtenerParametroRuta(string rutaArchivos);

        /// <summary>
        /// Metodo quese encarga de consultar la información de parametros no asincrono
        /// </summary>
        /// <param name="info">Información de la configuracion</param>
        /// <returns>Dato solicitado</returns>
        string ObtenerParametroRutaNoAsincrono(string info);
    }
}
