﻿/*==========================================================================
Archivo:            ITipoContactoData
Descripción:        interfaz de la clase TipoContactoData              
Autor:              paola.munoz                           
Fecha de creación:  17/12/2015 
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

#endregion

using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace LAFISE.Data
{
    public interface ITipoContactoData
    {
        /// <summary>
        ///  Metodo para ver los TipoContacto 
        /// </summary>
        /// <returns>Lista de TipoContacto</returns>
        Task<ICollection<TipoContactoDto>> VerTodos();
    }
}
