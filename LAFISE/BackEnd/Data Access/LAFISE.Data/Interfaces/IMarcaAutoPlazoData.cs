﻿/*==========================================================================
Archivo:            IMarcaAutoPlazoData
Descripción:        Interfaz de la clase MarcaAutoPlazoData                          
Autor:              juan.hincapie                          
Fecha de creación:  23/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para la consulta de plazo por marca de auto
    /// </summary>
    public interface IMarcaAutoPlazoData
    {
        /// <summary>
        /// Método encargado de consultar el plazo de una marca de auto
        /// </summary>
        /// <param name="idMarcaAuto">identificador de la marca</param>
        /// <returns>Entidad MarcaAutoPlazoDto</returns>
        MarcaAutoPlazoDto VerPlazoPorMarcaAuto(long idMarcaAuto);
    }
}
