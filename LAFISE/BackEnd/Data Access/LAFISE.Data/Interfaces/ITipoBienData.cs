﻿/*==========================================================================
Archivo:            ITipoBienData
Descripción:        Administrador de la data de tipo de bien                    
Autor:              juan.hincapie                       
Fecha de creación:  06/01/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Data
{
    public interface ITipoBienData
    {
        /// <summary>
        /// Ver todas los tipos de bien
        /// </summary>
        /// <returns>Lista de tipos de bien</returns>
        Task<ICollection<TipoBienDto>> VerTodosGrid();
    }
}
