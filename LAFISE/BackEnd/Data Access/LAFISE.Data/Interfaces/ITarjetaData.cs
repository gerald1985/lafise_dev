﻿/*==========================================================================
Archivo:            ITarjetaData
Descripción:        Interfaz de TarjetaData                     
Autor:              paola.munoz                  
Fecha de creación:  30/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion


namespace LAFISE.Data
{
    /// <summary>
    ///  Interface encargada de realizar el acceso a datos para TarjetaData
    /// </summary>
    public interface ITarjetaData
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de tarjeta</returns>
        Task<ICollection<TarjetaDto>> VerTodosGrid();

        /// <summary>
        /// Método usado para cargar un Tarjeta por id
        /// </summary>
        /// <param name="id">Identificador de tarjeta</param>
        /// <returns>tarjeta</returns>
        Task<TarjetaDto> VerPorId(Int64 id);

        /// <summary>
        /// Método usado para eliminar un Tarjeta por id
        /// </summary>
        /// <param name="id">Identificador de tarjeta</param>
        Task Eliminar(Int64 id);

        /// <summary>
        /// Método usado para crear un Tarjeta
        /// </summary>
        /// <param name="tarjeta">tarjeta</param>
        /// <returns>Tarjeta</returns>
        Task<TarjetaDto> Crear(TarjetaDto tarjeta);

        /// <summary>
        /// Método usado para editar un Tarjeta
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>tarjeta</returns>
        Task<TarjetaDto> Editar(TarjetaDto tarjeta);

        /// <summary>
        /// Consulta por filtros
        /// </summary>
        /// <param name="tarjeta">Nombre tarjeta</param>
        /// <returns>Tarjeta</returns>
        Task<ICollection<TarjetaDto>> TarjetaFiltro(string tarjeta);

        /// <summary>
        /// Consulta para inactivar
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>bool</returns>
        Task<bool> Inactivar(TarjetaDto tarjeta);

        /// <summary>
        /// Validar que las tarjetas sean unicas
        /// </summary>
        /// <param name="tarjeta">Entidad de la tarjeta</param>
        /// <returns>Entidad de la tarjeta retornada</returns>
        Task<TarjetaDto> ValidarTarjetaUnica(TarjetaDto tarjeta);

    }
}
