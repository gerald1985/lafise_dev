﻿/*==========================================================================
Archivo:            IIndicadorData
Descripción:        Interfaz de IndicadorData                   
Autor:              arley.lopez                  
Fecha de creación:  12/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/


#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion


namespace LAFISE.Data
{
    /// <summary>
    ///  Interface encargada de realizar el acceso a datos para IndicadorData
    /// </summary>
    public interface IIndicadorData
    {
        /// <summary>
        /// Método usado para cargar todos los indicadores activos
        /// </summary>
        /// <returns>Lista de indicadores</returns>
        ICollection<IndicadorDto> VerTodosActivos();

        /// <summary>
        /// Método usado para cargar todos los indicadores activos y no activos
        /// </summary>
        /// <returns>Lista de indicadores</returns>
        Task<ICollection<IndicadorDto>> VerTodos(); 

        /// <summary>
        /// Método usado para crear un indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        Task<IndicadorDto> Crear(IndicadorDto indicador);

        /// <summary>
        /// Método usado para editar un indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        Task<IndicadorDto> Editar(IndicadorDto indicador);

        /// <summary>
        /// Metodo que se encarga de inactivar un indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>bool</returns>
        Task<bool> Inactivar(IndicadorDto indicador);

        /// <summary>
        /// Método usado para eliminar un Indicador por id
        /// </summary>
        /// <param name="id">Identificador del indicador</param>
        Task Eliminar(Int64 id);

        /// <summary>
        /// Validar que el nombre del indicador sea unico
        /// </summary>
        /// <param name="indicador">Entidad indicador</param>
        /// <returns>Entidad indicador</returns>
        Task<IndicadorDto> ValidarIndicadorUnico(IndicadorDto indicador);
    }
}
