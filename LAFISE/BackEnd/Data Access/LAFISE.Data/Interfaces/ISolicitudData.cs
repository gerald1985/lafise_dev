﻿/*==========================================================================
Archivo:            ISolicitudData
Descripción:        Interfaz de SolicitudData                      
Autor:              paola.munoz                  
Fecha de creación:  14/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Interface encargada de realizar el acceso a datos para SolicitudData
    /// </summary>
    public interface ISolicitudData
    {
        /// <summary>
        /// Metodo que se encarga de ver todos
        /// </summary>
        /// <returns>Lista de solicitudes</returns>
        Task<ICollection<SolicitudDto>> VerTodos();

        /// <summary>
        /// Metodo para ver la solicitud teniendo en cuenta el identificador de la solicitud
        /// </summary>
        /// <param name="id">Identificador de solicitud</param>
        /// <returns>Solicitud</returns>
        Task<SolicitudDto> VerFormularioGeneralPorIdSolicitud(Int64 id);

        /// <summary>
        /// Metodo que se encarga de consultar el formulario por id cuando tiene referencia
        /// </summary>
        /// <param name="id">Identificador de solicitud</param>
        /// <returns>Solicitud</returns>
        Task<SolicitudDto> VerFormularioConReferenciasPorIdSolicitud(Int64 id);

        /// <summary>
        /// Metodo que se encarga de consultar los formularios de seguro de vida teniendo en cuenta el id solicitud
        /// </summary>
        /// <param name="id">Identificador de solicitud</param>
        /// <returns>Solicitud</returns>
        Task<SolicitudDto> VerFormularioSeguroVidaPorIdSolicitud(Int64 id);

        /// <summary>
        /// Metodo que se encarga de consultar el formulario por id teniendo en cuenta las reclamaciones
        /// </summary>
        /// <param name="id">Identificador de solicitud</param>
        /// <returns>Solicitud</returns>
        Task<SolicitudDto> VerFormularioReclamosPorIdSolicitud(Int64 id);

        /// <summary>
        /// Metodo que se encarga de crear la solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        Task<SolicitudDto> CrearSolicitud(SolicitudDto solicitud);

        /// <summary>
        /// Metodo que se encarga de ver la solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        Task<SolicitudDto> EditarSolicitud(SolicitudDto solicitud, String backOfficeUser);

        /// <summary>
        /// Metodo que se encarga de consultar los gestores de la solicitud
        /// </summary>
        /// <param name="solicitudCondicion">filtros en solicitudCondicion</param>
        /// <returns>Lista de solicitud filtro</returns>
        Task<ICollection<SolicitudFiltroDto>> VerGestorSolicitud(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        ///  Metodo que se encarga de consultar el formulario por id teniendo en cuenta los seguros vehiculares
        /// </summary>
        /// <param name="id">Identificador de solicitud</param>
        /// <returns>Solicitud</returns>
        Task<SolicitudDto> VerFormularioSeguroVehicularPorIdSolicitud(Int64 id);

        /// <summary>
        /// Metodo para consultar los datos para exportar marca auto
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones del filtro</param>
        /// <returns>Lista de exportacion</returns>
        Task<ICollection<ExportarMarcaAutoDto>> CargarExportarMarcaAuto(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Metodo para consultar el exportar de prestamo personal
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de exportacion de prestamo personal</returns>
        Task<ICollection<ExportarPrestamoPersonalDto>> CargarExportarPrestamoPersonal(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Metodo para consultar el exportar de prestamos hipotecarios
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de exportacion de prestamo hipotecario</returns>
        Task<ICollection<ExportarPrestamoHipotecarioDto>> CargarExportarPrestamoHipotecario(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Metodo para consultar la exportacion de tarjetas de credito y debito
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de exportacion de  tarjeta debito o credito</returns>
        Task<ICollection<ExportarTarjetaCreditoDebitoDto>> CargarExportarTarjetaCreditoDebito(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Metodo para consultar la exportacion de prestamo educativo
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de exportación para prestamo educativo</returns>
        Task<ICollection<ExportarPrestamoEducativoDto>> CargarExportarPrestamoEducativo(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Metodo para consultar la exportación de apertura de cuenta
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de exportación de apertura de cuenta</returns>
        Task<ICollection<ExportarAperturaCuentaDto>> CargarExportarAperturaCuenta(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Metodo para consultar la exportación de seguro de vida y accidente
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de exportación de seguro de vida y accidente</returns>
        Task<ICollection<ExportarSeguroVidaAccidenteDto>> CargarExportarSeguroVidaAccidente(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Metodo para consultar la exportación de seguro vehicular
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de seguro vehicular</returns>
        Task<ICollection<ExportarSeguroVehicularDto>> CargarExportarSeguroVehicular(SolicitudCondicionesDto solicitudCondicion);

        /// <summary>
        /// Ver información de los datos personales para el envio del correo
        /// </summary>
        /// <param name="idSolicitud">Identificador de la solicitud</param>
        /// <returns>DatosPersonalesDto</returns>
        DatosPersonalesDto VerDatosPersonales(Int64 idSolicitud);

        /// <summary>
        /// Metodo para exportar reclamos
        /// </summary>
        /// <param name="solicitudCondicion">Condiciones de filtro</param>
        /// <returns>Lista de reclamos</returns>
        Task<ICollection<ExportarReclamosDto>> CargarExportarReclamos(SolicitudCondicionesDto solicitudCondicion);
        
    }
}
