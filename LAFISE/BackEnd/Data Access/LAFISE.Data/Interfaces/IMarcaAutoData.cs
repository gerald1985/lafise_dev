﻿/*==========================================================================
Archivo:            IMarcaAutoData
Descripción:        Interfaz de la clase MarcaAutoData                      
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para las Marca de auto.
    /// </summary>
    public interface IMarcaAutoData
    {
        /// <summary>
        /// Método encargado de consultar las marca de auto según los filtros de búsqueda
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca de auto</param>
        /// <returns>Lista con todas las marcas de auto</returns>
        Task<ICollection<MarcaAutoFiltroDto>> VerMarcasModelos(MarcaAutoDto marcaauto);

        /// <summary>
        /// Método encargado de consultar todas las marcas de auto.
        /// </summary>
        /// <returns>Lista con todas las marcas de auto.</returns>
        Task<ICollection<MarcaAutoDto>> VerTodos();

        /// <summary>
        /// Método encargado de retornar una marca auto según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>Entidad MarcaAuto.</returns>
        Task<MarcaAutoDto> VerMarcaAutoPorId(long id);

         /// <summary>
        /// Método encargado de retornar una marca auto según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>Entidad MarcaAuto.</returns>
        MarcaAutoDto VerMarcaAutoPorIdNoAsincrono(long id);

        /// <summary>
        /// Método encargado de crear las Marcas de Auto y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto</param>
        /// <returns>Entidad MarcaAuto que se insertó.</returns>
        Task<MarcaAutoDto> CrearMarcaAuto(MarcaAutoDto marcaauto);

        /// <summary>
        /// Método encargado de actualizar las Marcas de Auto y sus relaciones.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>Entidad MarcaAuto que se editó.</returns>
        Task<MarcaAutoDto> EditarMarcaAuto(MarcaAutoDto marcaauto);

        /// <summary>
        /// Metodo usado para eliminar MarcaAuto y sus modelos asociados
        /// </summary>
        /// <param name="id">Identificador de la marca auto.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        Task<bool> EliminarMarcaAutoPorId(long id);

        /// <summary>
        /// Método encargado de inactivar las Marcas de Auto.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        Task<bool> InactivarMarcaAuto(MarcaAutoDto marcaauto);


        /// <summary>
        /// Método encargado de consultar el detalle de la marca auto.
        /// </summary>
        /// <param name="idMarca">Identificador de la marca auto.</param>
        /// <returns>Lista con el detalle de las Marcas de Auto.</returns>
        Task<ICollection<ModeloAutoDto>> ConsultarDetalleModelo(long idMarca);


        /// <summary>
        /// Consulta la validacion de MarcaAuto
        /// </summary>
        /// <param name="MarcaAuto">Identificador de MarcaAuto</param>
        /// <returns>Validar CasaComercial</returns>
        Task<CasaComercialValidarDto> ValidarCasaComercialMarca(long MarcaAuto);


        /// <summary>
        /// Método que retorna las marcas activas
        /// </summary>
        /// <returns></returns>
        Task<ICollection<MarcaAutoDto>> ObtenerMarcasActivas();

        /// <summary>
        /// Método que retorna las marcas activas
        /// </summary>
        /// <param name="idPais">identificador del pais</param>
        /// <returns></returns>
        Task<ICollection<MarcaAutoDto>> ObtenerMarcasActivasPorPais(Int64 idPais);

        /// <summary>
        /// Validación con la solicitud.
        /// </summary>
        /// <param name="Id">Identificador</param>
        /// <returns>Entidad de plan de financiación asociada.</returns>
        Task<PlanFinanciacionDto> validarRelacionPlanFinanciacion(long Id);

        /// <summary>
        /// Validar que el nombre de la marca sea unico.
        /// </summary>
        /// <param name="Id">Identificador marca</param>
        /// <param name="Nombre">Nombre marca</param>
        /// <returns>Entidad con la marca encontrada.</returns>
        Task<MarcaAutoDto> ValidarMarcaUnica(long Id, string Nombre);

        /// <summary>
        /// Validar que el nombre del modelo sea unico.
        /// </summary>
        ///  <param name="Id">Identificador marca</param>
        /// <param name="Nombre">Nombre modelo</param>
        /// <returns>Entidad con la modelo encontrado.</returns>
        Task<ModeloAutoDto> ValidarModeloUnica(long Id, string Nombre);

        /// <summary>
        /// Consulta la validacion de MarcaAuto
        /// </summary>
        /// <param name="ModeloAuto">Identificador de modelo</param>
        /// <returns>Validar CasaComercial</returns>
        Task<CasaComercialValidarDto> ValidarCasaComercialModelo(long ModeloAuto);

        /// <summary>
        /// Método encargado de inactivar los Modelos de Auto.
        /// </summary>
        /// <param name="modeloauto">Dto con las propiedades de el modelo auto.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        Task<bool> InactivarModeloAuto(ModeloAutoDto modeloauto);

        /// <summary>
        /// Validación con la solicitud
        /// </summary>
        /// <param name="id">Identificador modelo</param>
        /// <returns>Entidad de plan de financiación asociada.</returns>
        Task<PlanFinanciacionDto> validarRelacionPlanFinanciacionModelo(long Id);

        Task<bool> EliminarModeloAutoPorId(long id);

        /// <summary>
        /// Método usado para cargar todas las marcas según sus ids
        /// </summary>
        /// <param name="listaMarcas">lista con el identificador de la marcas</param>
        /// <returns>Lista de marcas</returns>
        Task<ICollection<MarcaAutoDto>> VerMarcasPorId(List<long> listaMarcas);
    }
}
