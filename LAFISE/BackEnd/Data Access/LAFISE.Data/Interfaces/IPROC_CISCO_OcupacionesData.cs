﻿using System.Collections.Generic;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using System;

namespace LAFISE.Data
{
    public interface IPROC_CISCO_OcupacionesData
    {
        Task<ICollection<PROC_CISCO_OcupacionesDto>> VerTodos();
    }
}
