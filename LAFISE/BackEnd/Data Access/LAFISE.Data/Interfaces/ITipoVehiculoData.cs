﻿/*==========================================================================
Archivo:            ITipoVehiculoData
Descripción:        Administrador de la data de tipo de vehículo                    
Autor:              juan.hincapie                       
Fecha de creación:  26/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;

namespace LAFISE.Data
{
    /// <summary>
    /// Administrador de la data de tipo de vehículo  
    /// </summary>
    public interface ITipoVehiculoData
    {
        /// <summary>
        /// Consultar todos los tipos vehiculos
        /// </summary>
        /// <returns>lista TipoVehiculoDto</returns>
        Task<ICollection<TipoVehiculoDto>> VerTodosGrid();

        /// <summary>
        /// Consultar el tipo de vehiculo teniendo en cuenta el dentificador
        /// </summary>
        /// <param name="id">identificador del tipo de vehiculo</param>
        /// <returns>TipoVehiculoDto</returns>
        TipoVehiculoDto VerPorIdNoAsincrono(Int64 id);
    }
}
