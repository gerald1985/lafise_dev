﻿/*==========================================================================
Archivo:            ICasaComercialModeloAutoData
Descripción:        Interfaz de la clase CasaComercialModeloAutoData                     
Autor:              Juan Hincapie                         
Fecha de creación:  30/09/2015 11:07:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para el detalle de las casas comerciales. 
    /// </summary>
    public interface ICasaComercialModeloAutoData
    {
        /// <summary>
        /// Método encargado de consultar todas las casas comerciales.
        /// </summary>
        /// <returns>Lista con los detalle de las casas comerciales</returns>
        Task<ICollection<CasaComercialModeloAutoDto>> VerTodos();

        /// <summary>
        /// Método encargado de retornar el detalle de una casa comercial según su identificador.
        /// </summary>
        /// <param name="id">Identificador correspondiente a la entidad CasaComercialModeloAuto</param>
        /// <returns>Lista con todas los detalles de las casas comerciales. </returns>
        Task<CasaComercialModeloAutoDto> VerPorId(int id);

        /// <summary>
        /// Método encargado de retornar el detalle de una casa comercial según el identificador de la casa comercial.
        /// </summary>
        /// <param name="id">Identificador correspondiente a la entidad CasaComercial</param>
        /// <returns>Lista con todas los detalles de las casas comerciales.</returns>
        Task<CasaComercialModeloAutoDto> VerPorCasaComercial(int id);

        /// <summary>
        /// Método encargado de crear el detalle de las casas comerciales.
        /// </summary>
        /// <param name="casaComercialModeloAuto">Dto con las propiedades de CasaComercialModeloAutoDto.</param>
        /// <returns>Entidad CasaComercialModeloAutoDto que se insertó.</returns>
        Task<CasaComercialModeloAutoDto> Crear(CasaComercialModeloAutoDto casaComercialModeloAuto);

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casaComercialModeloAuto">Dto con las propiedades de CasaComercialModeloAutoDto.</param>
        /// <returns>Entidad CasaComercial que se editó</returns>
        Task<CasaComercialModeloAutoDto> Editar(CasaComercialModeloAutoDto casaComercialModeloAuto);

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial.
        /// </summary>
        /// <param name="id">Identificador de la entidad CasaComercialModeloAutoDto</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        Task<bool> Eliminar(int id);

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial según su casa comercial
        /// </summary>
        /// <param name="id">Identificador de la casa comercial</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        Task<bool> EliminarPorCasaComercial(int id);

    }
}
