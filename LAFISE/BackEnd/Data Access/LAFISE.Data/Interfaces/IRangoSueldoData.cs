﻿/*==========================================================================
Archivo:            IRangoSueldoData
Descripción:        Interfaz para RangoSueldoData          
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para RangoSueldoData
    /// </summary>
    public interface IRangoSueldoData
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de rango sueldo</returns>
        Task<ICollection<RangoSueldoDto>> VerTodos();

        /// <summary>
        /// Método usado para cargar un RangoSueldo por id
        /// </summary>
        /// <param name="id">Identificador de rango sueldo</param>
        /// <returns>Rango sueldo</returns>
        Task<RangoSueldoDto> VerPorId(Int64 id);

        /// <summary>
        /// Metodo que se encarga de consultar el detalle del rango sueldo
        /// </summary>
        /// <param name="id">Identificador de rango sueldo</param>
        /// <returns>rango sueldo detalle</returns>
        Task<ICollection<RangoSueldoDto>> ConsultarDetalleRangoSueldo(long id);

        /// <summary>
        /// Método usado para editar un RangoSueldo
        /// </summary>
        /// <param name="rangosueldo">rango sueldo</param>
        /// <returns>rango sueldo</returns>
        Task<RangoSueldoDto> Editar(RangoSueldoDto rangosueldo);

        /// <summary>
        /// Método usado para crear un RangoSueldo
        /// </summary>
        /// <param name="rangosueldo">Rango sueldo</param>
        /// <returns>Rango sueldo</returns>
        Task<RangoSueldoDto> Crear(RangoSueldoDto rangosueldo);



        /// <summary>
        /// Consultar por filtro
        /// </summary>
        /// <param name="pais">identificador de país concatenados por coma</param>
        /// <returns>Lista rango sueldo filtro</returns>
        Task<ICollection<RangoSueldoFiltroDto>> VerRangoSueldoFiltro(string pais);

        /// <summary>
        /// Metodo para retornar la información teniendo en cuenta la lista del identificador de paises
        /// </summary>
        /// <param name="pais">identificador de país </param>
        /// <returns>lista de RangoSueldoPaisDto</returns>
        Task<ICollection<RangoSueldoPaisDto>> VerRangoSueldoPais(long pais);
    }
}
