﻿/*==========================================================================
Archivo:            IPrimaCategoriaPaisData
Descripción:        Interfaz de la clase PrimaCategoriaPaisData                          
Autor:              steven.echavarria                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para la consulta de porcentaje de prima por categoría y país
    /// </summary>
    public interface IPrimaCategoriaPaisData
    {
        /// <summary>
        /// Método encargado de consultar el porcentaje de prima por categoria y país
        /// </summary>
        /// <param name="primaCategoriaPais">objeto primaCategoriaPais</param>
        /// <returns>Entidad PrimaCategoriaPaisDto</returns>
        PrimaCategoriaPaisDto VerPorcentajePrimaPorCategoriaPais(PrimaCategoriaPaisDto primaCategoriaPais);
    }
}
