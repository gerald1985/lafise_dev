﻿/*==========================================================================
Archivo:            ITipoPropiedadData
Descripción:       Interfaz de TipoPropiedadData                 
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Interface encargada de realizar el acceso a datos para TipoPropiedadData
    /// </summary>
    public interface ITipoPropiedadData
    {
        /// <summary>
        /// Método usado para cargar los grid con un objectDataSource
        /// </summary>
        /// <returns>Lista de tipo propiedad</returns>
        Task<ICollection<TipoPropiedadDto>> VerTodosGrid();

        /// <summary>
        /// Método usado para cargar los tipos de propiedades que están activas
        /// </summary>
        /// <returns>Lista de tipo propiedad</returns>
        Task<ICollection<TipoPropiedadDto>> VerTodosActivos();

        /// <summary>
        /// Método usado para cargar un TipoPropiedad por id
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        Task<TipoPropiedadDto> VerPorId(long id);

        /// <summary>
        /// Método usado para cargar un TipoPropiedad por identificador no asincrona
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        TipoPropiedadDto VerPorIdNoAsincrono(long id);

        /// <summary>
        /// Método usado para cargar los tipos de propiedad asociados a una urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de urbanizadora</param>
        /// <returns>Lista de tipo propiedad</returns>
        Task<ICollection<TipoPropiedadDto>> VerTiposPropiedadPorUrbanizadora(long idUrbanizadora);

        /// <summary>
        /// Método usado para eliminar un TipoPropiedad por id
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        Task Eliminar(long id);

        /// <summary>
        /// Método usado para crear un TipoPropiedad
        /// </summary>
        /// <param name="tipopropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        Task<TipoPropiedadDto> Crear(TipoPropiedadDto tipopropiedad);

        /// <summary>
        /// Método usado para editar un TipoPropiedad
        /// </summary>
        /// <param name="tipopropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        Task<TipoPropiedadDto> Editar(TipoPropiedadDto tipopropiedad);

        /// <summary>
        /// Metodo para realizar la consulta teniendo en cuenta los filtros
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        Task<ICollection<TipoPropiedadDto>> TipoPropiedadFiltro(string tipoPropiedad);

        /// <summary>
        /// Metodo para activar e inactivar
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>bool</returns>
        Task<bool> Inactivar(TipoPropiedadDto tipoPropiedad);

        /// <summary>
        /// validar el tipo de propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>UrbanizadoraValidarDto</returns>
        Task<UrbanizadoraValidarDto> ValidarTipoPropiedad(long tipoPropiedad);

        /// <summary>
        /// Validación con la solicitud.
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns>Entidad de plan de financiación asociada.</returns>
        Task<PlanFinanciacionDto> validarRelacionPlanFinanciacion(long id);


        /// <summary>
        /// Validar que el nombre de la propiedad sea unico.
        /// </summary>
        /// <param name="Nombre">Nombre del tipo de propiedad.</param>
        /// <returns>Entidad con el tipo propiedad encontrada.</returns>
        Task<TipoPropiedadDto> ValidarTipoPropiedadUnica(long Id, string Nombre);
    }
}
