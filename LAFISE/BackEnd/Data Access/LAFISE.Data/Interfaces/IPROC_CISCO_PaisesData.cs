﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;

namespace LAFISE.Data
{
    public interface IPROC_CISCO_PaisesData
    {
        Task<ICollection<PROC_CISCO_PaisesDto>> VerTodos();
    }
}
