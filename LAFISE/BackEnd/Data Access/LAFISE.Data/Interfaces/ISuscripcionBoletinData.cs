﻿/*==========================================================================
Archivo:            ISuscripcionBoletinData
Descripción:        Interfaz SuscripcionBoletinData                      
Autor:              steven.echavarria                          
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                              
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para SuscripcionBoletinData
    /// </summary>
    public interface ISuscripcionBoletinData
    {
        /// <summary>
        /// Método usado para crear una Suscripción al boletín
        /// </summary>
        /// <param name="suscripcion">SuscripcionBoletin</param>
        /// <returns>SuscripcionBoletin</returns>
        Task<SuscripcionBoletinDto> Crear(SuscripcionBoletinDto suscripcion);

        /// <summary>
        /// Metodo que valida que el email ya esté registrado en las suscripciones
        /// </summary>
        /// <param name="suscripcion">entidad suscripcion</param>
        /// <returns>suscripcion</returns>
        Task<SuscripcionBoletinDto> ValidarSuscripcionPorEmail(SuscripcionBoletinDto suscripcion);
    }
}
