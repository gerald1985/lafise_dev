﻿/*==========================================================================
Archivo:            ITipoActividadData
Descripción:        Administrador de la data de tipo de actividad                    
Autor:              juan.hincapie                       
Fecha de creación:  26/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Data
{
    /// <summary>
    /// Administrador de la data de tipo de actividad  
    /// </summary>
    public interface ITipoActividadData
    {
        /// <summary>
        /// Ver todas los tipos de Actividad
        /// </summary>
        /// <returns>Lista de tipos de actividad</returns>
        Task<ICollection<TipoActividadDto>> VerTodosGrid();
    }
}
