﻿/*==========================================================================
Archivo:            IContactenosData
Descripción:       Interfaz de la clase ContactenosData                
Autor:              paola.munoz                  
Fecha de creación:  13/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion


namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para ContactenosData
    /// </summary>
    public interface IContactenosData
    {
        /// <summary>
        /// Ver todos los contactenos
        /// </summary>
        /// <returns>Lista de contactenos</returns>
        Task<ICollection<ContactenosDto>> VerTodosGrid();

        /// <summary>
        /// Ver por identificador los contactenos
        /// </summary>
        /// <param name="id">Identificador de los contactenos</param>
        /// <returns>Contactenos</returns>
        Task<ContactenosDto> VerPorId(Int64 id);

        /// <summary>
        /// Eliminar contactenos
        /// </summary>
        /// <param name="id">Identificador de los contactenos</param>
        Task Eliminar(Int64 id);

        /// <summary>
        /// Crear contactenos
        /// </summary>
        /// <param name="contactanos">Contactenos</param>
        /// <returns>Contactenos</returns>
        Task<ContactenosDto> Crear(ContactenosDto contactanos);

        /// <summary>
        /// Editar contactenos
        /// </summary>
        /// <param name="contactenos">Contactenos</param>
        /// <returns>Contactenos</returns>
        Task<ContactenosDto> Editar(ContactenosDto contactenos);
    }
}
