﻿/*==========================================================================
Archivo:            ICiudadData
Descripción:        Interfaz de la clase CiudadData                    
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para  CiudadData
    /// </summary>
    public interface ICiudadData
    {
        /// <summary>
        /// Método usado para cargar todas las ciudades
        /// </summary>
        /// <returns>Lista de ciudades</returns>
        Task<ICollection<CiudadDto>> VerTodos();

        /// <summary>
        /// Método usado para cargar una ciudad por su id
        /// </summary>
        /// <param name="id">Identificador de la ciudad</param>
        /// <returns>Ciudad</returns>
        Task<CiudadDto> VerCiudadPorId(int id);

        /// <summary>
        /// Método usado para cargar todas las ciudades asociadas un departamento
        /// </summary>
        /// <param name="idDepartamento">Identificador del departamento</param>
        /// <returns>Ciudad</returns>
        Task<ICollection<CiudadDto>> VerCiudadesPorDepartamento(int idDepartamento);

        /// <summary>
        /// Metodo que se encarga de retornar la lista de ciudades teniendo en cuenta el país
        /// </summary>
        /// <param name="listaPais">lista con el identificador de pais</param>
        /// <returns>Ciudad</returns>
        Task<ICollection<CiudadDto>> VerCiudadesPorPais(List<Int64> listaPais);
        /// <summary>
        /// Metodo para consultar las ciudades teniendo en cuenta la lista
        /// </summary>
        /// <param name="listaCiudad">Identificador de las ciudades en lista</param>
        /// <returns>Lista de ciudad</returns>
        ICollection<CiudadDto> VerCiudadesPorCiudadNoAsincrono(List<Int64> listaCiudad);

        /// <summary>
        /// Metodo para consultar la ciudad teniendo en cuenta la lista de departamento
        /// </summary>
        /// <param name="listaDepartamento">lista de identificador de departamentos</param>
        /// <returns>lista de ciudad</returns>
        Task<ICollection<CiudadDto>> VerListaCiudadesPorDepartamento(List<Int64> listaDepartamento);
    }
}
