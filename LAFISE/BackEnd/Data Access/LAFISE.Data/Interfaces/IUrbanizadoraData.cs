﻿/*==========================================================================
Archivo:            IUrbanizadoraData
Descripción:       Interfaz UrbanizadoraData                      
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                              
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para UrbanizadoraData
    /// </summary>
    public interface IUrbanizadoraData
    {
        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activas
        /// </summary>
        /// <returns>Lista de urbanizadora</returns>
        Task<ICollection<UrbanizadoraDto>> VerTodosActivos();


        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activas por país
        /// </summary>
        /// <returns>Lista de urbanizadora</returns>
        Task<ICollection<UrbanizadoraDto>> VerTodosActivosPorPais(Int64 idPais);

        /// <summary>
        /// Ver urbanizadora por tipo propiedad 
        /// </summary>
        /// <param name="urbanizadoraTipoPropiedad">Urbanizadora Tipo Propiedad</param>
        /// <returns>Lista de UrbanizadoraTipoPropiedadDto</returns>
        Task<ICollection<UrbanizadoraTipoPropiedadDto>> VerUrbanizadorasTiposPropiedades(UrbanizadoraTipoPropiedadDto urbanizadoraTipoPropiedad);

        /// <summary>
        /// Método usado para cargar una Urbanizadora por id
        /// </summary>
        /// <param name="id">Identificador de urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<UrbanizadoraDto> VerUrbanizadoraPorId(Int64 id);

        /// <summary>
        /// Método usado para cargar una Urbanizadora por id no asincrono
        /// </summary>
        /// <param name="id">Identificador de urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        UrbanizadoraDto VerUrbanizadoraPorIdNoAsincrono(Int64 id);

        /// <summary>
        /// Metodo que valida que el nombre de la urbanizadora sea unico.
        /// </summary>
        /// <param name="urbanizadora">entidad urbanizadora</param>
        /// <returns>urbanizadora</returns>
        Task<UrbanizadoraDto> ValidarUrbanizadoraPorNombre(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Método usado para eliminar una Urbanizadora por id
        /// </summary>
        /// <param name="id">Identificador de urbanizadora</param>
        /// <returns>bool</returns>
        Task<bool> EliminarUrbanizadoraPorId(int id);

        /// <summary>
        /// Método usado para crear una Urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<UrbanizadoraDto> CrearUrbanizadora(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Método usado para editar una Urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        Task<UrbanizadoraDto> EditarUrbanizadora(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Método que permite inactivar una urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>urbanizadora</returns>
        Task<UrbanizadoraDto> InactivarUrbanizadora(UrbanizadoraDto urbanizadora);

        /// <summary>
        /// Consultar el detalle  de la urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de la urbanizdora</param>
        /// <returns>Urbanizadora</returns>
        Task<UrbanizadoraDto> ConsultarDetalleUrbanizadora(long idUrbanizadora);

        /// <summary>
        /// Eliminar el detalle de la urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de urbanizadora</param>
        /// <returns>bool</returns>
        Task<bool> EliminarDetalleUrbanizadora(long idUrbanizadora);


        /// <summary>
        /// Método que identifica si hay una urbanizadora asociada a un plan financiero
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de la urbanizadora</param>
        /// <returns>bool</returns>
        Task<bool> ExistePlanFinanciacionPorUrbanizadora(long idUrbanizadora);
    }
}
