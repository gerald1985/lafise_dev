﻿#region Referencias
using LAFISE.Entities.Dtos;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos de Cotizacion
    /// </summary>
    public interface ICotizadorData
    {
        /// <summary>
        /// Método usado para crear un registro de cotizacion
        /// </summary>
        /// <param name="cotizador">Cotizador</param>
        /// <returns>Cotizador</returns>
        Task<CotizadorDto> Crear(CotizadorDto cotizador);
    }
}
