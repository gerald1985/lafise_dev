﻿/*==========================================================================
Archivo:            IGarantiaData
Descripción:        interfaz de la clase GarantiaData              
Autor:              paola.munoz                           
Fecha de creación:  17/11/2015 
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    public interface IGarantiaData
    {
        /// <summary>
        /// Metodo encargado para ver todos las garantias
        /// </summary>
        /// <returns>Lista Garantias</returns>
        Task<ICollection<GarantiaDto>> VerTodos();

        /// <summary>
        /// Metodo que se encarga de ver la garantia por el id
        /// </summary>
        /// <param name="id">Identificador de la garantia</param>
        /// <returns>Garantia</returns>
        Task<GarantiaDto> VerGarantiaPorId(Int64 id);

        /// <summary>
        /// Metodo para consultar las garantias por identificador no asincrono
        /// </summary>
        /// <param name="id">Identificador garantia</param>
        /// <returns>GarantiaDto</returns>
        GarantiaDto VerGarantiaPorIdNoAsincrono(Int64 id);
    }
}
