﻿/*==========================================================================
Archivo:            IDepartamentoData
Descripción:        Interfaz de la clase DepartamentoData                 
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                     
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para DepartamentoData
    /// </summary>
    public interface IDepartamentoData
    {
        /// <summary>
        /// Método usado para cargar todos los departamentos
        /// </summary>
        /// <returns>Lista de departamentos</returns>
        Task<ICollection<DepartamentoDto>> VerTodos();

        /// <summary>
        /// Método usado para cargar un departamento por su id
        /// </summary>
        /// <param name="id">Identificador del departamento</param>
        /// <returns>Departamento</returns>
        Task<DepartamentoDto> VerDepartamentoPorId(int id);

        /// <summary>
        /// Método usado para cargar todos los departamentos aociados a un pais
        /// </summary>
        /// <param name="idPais">Identificador del pais</param>
        /// <returns>Lista de departamentos</returns>
        Task<ICollection<DepartamentoDto>> VerDepartamentosPorPais(int idPais);

        /// <summary>
        /// Metodo para mostrar los departamentos teniendo en cuenta el país
        /// </summary>
        /// <param name="listaPais"> lista de paises</param>
        /// <returns>Lista de departamento</returns>
        Task<ICollection<DepartamentoDto>> VerListaDepartamentoPorPais(List<Int64> listaPais);
    }
}
