﻿/*==========================================================================
Archivo:            IEstadoData
Descripción:        interfaz de la clase EstadoData              
Autor:              paola.munoz                           
Fecha de creación:  16/12/2015 
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

#endregion

using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace LAFISE.Data
{
    public interface IEstadoData
    {
        /// <summary>
        ///  Metodo para ver los estados teniendo en cuenta el tipo de estado
        /// </summary>
        /// <param name="idTipoEstado">Identificador del tipo de estado</param>
        /// <returns>Lista de estado</returns>
        Task<ICollection<EstadoDto>> VerEstadoIdTipo(Int64 idTipoEstado);
        /// <summary>
        /// Metodo para capturar el nombre del estado
        /// </summary>
        /// <param name="id">Identificador del estado</param>
        /// <returns></returns>
        EstadoDto VerEstadoPorId(Int64 id);
    }
}
