﻿/*==========================================================================
Archivo:            ISucursalData
Descripción:        Interfaz de SucursalData                   
Autor:              paola.munoz                  
Fecha de creación:  30/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion


namespace LAFISE.Data
{
    /// <summary>
    ///  Interface encargada de realizar el acceso a datos para SucursalData
    /// </summary>
    public interface ISucursalData
    {
        /// <summary>
        /// Método usado para cargar las sucursales
        /// </summary>
        /// <returns>Lista de sucursales</returns>
        Task<ICollection<SucursalDto>> VerTodosGrid();

        /// <summary>
        /// Método usado para cargar las sucursales de acuerdo a un pais
        /// </summary>
        /// <returns>Lista de sucursales</returns>
        Task<ICollection<SucursalDto>> VerTodosPorPais(long idPais);

        /// <summary>
        /// Método usado para cargar un Sucursal por id
        /// </summary>
        /// <param name="id">identificador de sucursales</param>
        /// <returns>Sucursal</returns>
        Task<SucursalDto> VerPorId(Int64 id);

        /// <summary>
        /// Metodo para consultar la sucursal teniendo el identificador
        /// </summary>
        /// <param name="id">Identificador de la sucursal</param>
        /// <returns>SucursalDto</returns>
        SucursalDto VerPorIdNoAsincrono(Int64 id);

        /// <summary>
        /// Método usado para eliminar un Sucursal por id
        /// </summary>
        /// <param name="id">Identificador de sucursal</param>
        Task Eliminar(Int64 id);

        /// <summary>
        /// Método usado para crear un sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        Task<SucursalDto> Crear(SucursalDto sucursal);

        /// <summary>
        /// Método usado para editar un sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        Task<SucursalDto> Editar(SucursalDto sucursal);

        /// <summary>
        /// metodo para realizar la consulta por los filtros
        /// </summary>
        /// <param name="sucursal">Sucursal filtro</param>
        /// <returns>Sucursal filtro</returns>
        Task<ICollection<SucursalFiltroDto>> SucursalFiltro(SucursalFiltroDto sucursal);

        /// <summary>
        /// Metodo que se encarga de inactivar
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>bool</returns>
        Task<bool> Inactivar(SucursalDto sucursal);

        /// <summary>
        /// Metodo que se encarga de validar si la sucursal es unica
        /// </summary>
        /// <param name="sucursal">Entidad sucursal</param>
        /// <returns>Entidad de sucursal resultante</returns>
        Task<SucursalDto> ValidarSucursalUnica(SucursalDto sucursal);

        /// <summary>
        /// Método usado para cargar todos las sucursales activos
        /// </summary>
        /// <returns>Lista de sucursal</returns>
        Task<ICollection<SucursalDto>> VerTodosActivas();
    }
}