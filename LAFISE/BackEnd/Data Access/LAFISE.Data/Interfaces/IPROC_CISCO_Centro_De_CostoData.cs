﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;

namespace LAFISE.Data
{
    public interface IPROC_CISCO_Centro_De_CostoData
    {
        Task<ICollection<PROC_CISCO_Centro_De_CostoDto>> VerTodos();
    }
}
