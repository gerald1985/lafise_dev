﻿/*==========================================================================
Archivo:            ITipoIdentificacion
Descripción:        Interfaz para ITipoIdentificacion                     
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para TipoIdentificacionData
    /// </summary>
    public interface ITipoIdentificacionData
    {
        /// <summary>
        /// Metodo encargado para ver todos los tipos de identificación
        /// </summary>
        /// <returns>Lista TipoIdentificacion</returns>
        Task<ICollection<TipoIdentificacionDto>> VerTodos();

        /// <summary>
        /// Metodo encargado de ver todos los tipos de identificacion relacionados con un país
        /// </summary>
        /// <param name="idPais">Identificador del país</param>
        /// <returns>Lista TipoIdentificacion</returns>
        Task<ICollection<TipoIdentificacionDto>> VerTipoIdentificacionPorIdPais(Int64 idPais);


        /// <summary>
        /// Metodo retorna mascara de entrada por identificacion
        /// </summary>
        /// <param name="id">id identificacion</param>
        /// <returns>mascara de entrada</returns>
        Task<String> obtenerMascaraPorIdentificacion(Int64 id);

        /// <summary>
        /// Metodo para consultar el tipo de identificacion teniendo en cuenta el identificador de forma no asincrona
        /// </summary>
        /// <param name="id">Identificador de tipo de identificación</param>
        /// <returns>Tipo Identificación</returns>
        TipoIdentificacionDto VerPorIdNoAsincrono(Int64 id);
    }
}
