﻿/*==========================================================================
Archivo:            IPaisData
Descripción:        Interfaz para MonedaData                     
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Interface encargada de realizar el acceso a datos para MonedaData
    /// </summary>
    public interface IMonedaData
    {
        /// <summary>
        /// Método usado para cargar todas las monedas
        /// </summary>
        /// <returns>Lista de monedas</returns>
        Task<ICollection<MonedaDto>> VerTodos();

        /// <summary>
        /// Método usado para cargar una moneda por su id
        /// </summary>
        /// <param name="id">Identificador de la moneda</param>
        /// <returns>Moneda</returns>
        Task<MonedaDto> VerMonedaPorId(int id);

        /// <summary>
        /// Método que retorna el simbolo de la moneda según el portal
        /// </summary>
        /// <param name="siglaPortal"></param>
        /// <returns></returns>
        string ObtenerSimboloMoneda(string siglaPortal);

        /// <summary>
        /// Metodo que se encarga de consultar la moneda teniendo en cuenta el país
        /// </summary>
        /// <param name="idPais">Identificador del país</param>
        /// <returns>MonedaDto</returns>
        MonedaDto VerMonedaPais(long idPais);
    }
}
