﻿/*==========================================================================
Archivo:            IPortalPaisData
Descripción:        Interfaz de PortalPaisData                     
Autor:              paola.munoz                  
Fecha de creación:  10/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
    public interface IPortalPaisData
    {
        /// <summary>
        /// Interfaz que devuelve el país teniendo en cuenta el portal
        /// </summary>
        /// <param name="siglaPortal">Sigla del portal</param>
        /// <returns>Identificador del país</returns>
        Task<PortalPaisDto> ObtenerPaisPorPortal(string siglaPortal);

        /// <summary>
        /// Interfaz que devuelve el país teniendo en cuenta el portal
        /// </summary>
        /// <param name="siglaPortal">Sigla del portal</param>
        /// <returns>Identificador del país</returns>
        PortalPaisDto ObtenerPaisPorPortalNoAsincrono(string siglaPortal);
    }
}
