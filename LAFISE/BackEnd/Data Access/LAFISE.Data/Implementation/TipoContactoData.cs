﻿/*==========================================================================
Archivo:            TipoContactoData
Descripción:        Administador de la data de TipoContacto                     
Autor:              paola.munoz                         
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de TipoContactoData
    /// </summary>
    public class TipoContactoData : AsyncRepositoryBase<TipoContacto, LafiseContext>, ITipoContactoData
    {
        /// <summary>
        /// Metodo para ver los TipoContacto
        /// </summary>
        /// <returns>Lista TipoContacto</returns>
        public async Task<ICollection<TipoContactoDto>> VerTodos()
        {
            var query = (from tc in DataContext.TipoContacto
                         select new TipoContactoDto
                         {
                             Id = tc.Id,
                             Nombre = tc.Nombre
                         }).ToListAsync();

            return await query;
        }
    }
}
