﻿/*==========================================================================
Archivo:            UrbanizadoraData
Descripción:        Administrador de la data de urbanizadora                     
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Collections;
using System.Data.Entity;
using System;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de UrbanizadoraData
    /// </summary>
    public class UrbanizadoraData : AsyncRepositoryBase<Urbanizadora, LafiseContext>, IUrbanizadoraData
    {
        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activas
        /// </summary>
        /// <returns>Lista de urbanizadora</returns>
        public async Task<ICollection<UrbanizadoraDto>> VerTodosActivos()
        {
            var query = (from u in DataContext.Urbanizadora
                         where u.Activo
                         select new UrbanizadoraDto
                         {
                             Id = u.Id,
                             Nombre = u.Nombre
                         }).ToListAsync();

            return await query;
        }


        /// <summary>
        /// Método usado para cargar todas las urbanizadoras activas y por país
        /// </summary>
        /// <param name="idPais"></param>
        /// <returns></returns>
        public async Task<ICollection<UrbanizadoraDto>> VerTodosActivosPorPais(long idPais)
        {
            var query = (from u in DataContext.Urbanizadora
                         where u.Activo && u.IdPais == idPais
                         select new UrbanizadoraDto
                         {
                             Id = u.Id,
                             Nombre = u.Nombre
                         }).ToListAsync();

            return await query;
        }


        /// <summary>
        /// Metodo para ver las urbanizadora por los filtros
        /// </summary>
        /// <returns>Lista de UrbanizadoraTipoPropiedadDto</returns>
        public async Task<ICollection<UrbanizadoraTipoPropiedadDto>> VerUrbanizadorasTiposPropiedades(UrbanizadoraTipoPropiedadDto urbanizadoraTipoPropiedad)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idPais", urbanizadoraTipoPropiedad.Pais);
            _parametros.Add("@urbanizadora", urbanizadoraTipoPropiedad.Urbanizadora);
            _parametros.Add("@tipoPropiedad", urbanizadoraTipoPropiedad.TipoPropiedad);

            return await RunStoredProcedureAsync<ICollection<UrbanizadoraTipoPropiedadDto>, UrbanizadoraTipoPropiedadDto>("CargarUrbanizadoraPorFiltros", _parametros);
        }

        /// <summary>
        /// metodo para ver una urbanizadora por identificador
        /// </summary>
        /// <param name="id">Identificador de la urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<UrbanizadoraDto> VerUrbanizadoraPorId(Int64 id)
        {
            return await GetByAsync<UrbanizadoraDto>(c => c.Id == id);
        }


        /// <summary>
        /// metodo para ver una urbanizadora por identificador no asincrono
        /// </summary>
        /// <param name="id">Identificador de la urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public UrbanizadoraDto VerUrbanizadoraPorIdNoAsincrono(Int64 id)
        {
            var query = (from u in DataContext.Urbanizadora
                         where u.Id == id
                         select new UrbanizadoraDto
                         {
                             Id = u.Id,
                             Nombre = u.Nombre
                         }).FirstOrDefault();
            return query;
        }


        /// <summary>
        /// Metodo que valida que el nombre de la urbanizadora sea unico.
        /// </summary>
        /// <param name="urbanizadora">entidad urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<UrbanizadoraDto> ValidarUrbanizadoraPorNombre(UrbanizadoraDto urbanizadora)
        {
            var entidad = await GetByAsync(c => c.Nombre == urbanizadora.Nombre && c.Id != urbanizadora.Id);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Crear urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        public async Task<UrbanizadoraDto> CrearUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            var entidad = urbanizadora.Convert();

            foreach (var item in urbanizadora.ListaPropiedades)
            {
                UrbanizadoraTipo _urbanizadoraTipo = new UrbanizadoraTipo();
                _urbanizadoraTipo.IdTipoPropiedad = item.Id;
                entidad.UrbanizadoraTipo.Add(_urbanizadoraTipo);
            }

            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Metodo para editar la urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        public async Task<UrbanizadoraDto> EditarUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            var eliminarDetalle = EliminarDetalleUrbanizadora(urbanizadora.Id);

            var entidad = await GetByAsync(c => c.Id == urbanizadora.Id);
            if (entidad != null)
            {
                entidad.Id = urbanizadora.Id;
                entidad.Nombre = urbanizadora.Nombre;
                entidad.IdPais = urbanizadora.IdPais;
                entidad.Activo = urbanizadora.Activo;
            }

            foreach (var item in urbanizadora.ListaPropiedades)
            {
                UrbanizadoraTipo _urbanizadoraTipo = new UrbanizadoraTipo();
                _urbanizadoraTipo.IdTipoPropiedad = item.Id;
                _urbanizadoraTipo.IdUrbanizadora = entidad.Id;
                entidad.UrbanizadoraTipo.Add(_urbanizadoraTipo);
            }

            await SaveAsync();
            return entidad.ConvertToDto();
        }


        /// <summary>
        /// Metodo para eliminar la urbanizadora por identificador
        /// </summary>
        /// <param name="id">Identificador de la urbanizadora</param>
        /// <returns>bool</returns>
        public async Task<bool> EliminarUrbanizadoraPorId(int id)
        {
            var queryDetalle = DataContext.UrbanizadoraTipo.Where(s => s.IdUrbanizadora == id).ToList();
            var eliminarDetalle = DataContext.UrbanizadoraTipo.RemoveRange(queryDetalle);
            var queryUrbanizadora = DataContext.Urbanizadora.First(s => s.Id == id);
            var eliminarMarcaAuto = DataContext.Urbanizadora.Remove(queryUrbanizadora);
            await SaveAsync();
            return true;
        }


        /// <summary>
        /// Método que permite inactivar una urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>urbanizadora</returns>
        public async Task<UrbanizadoraDto> InactivarUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            var entidad = await GetByAsync(c => c.Id == urbanizadora.Id);
            if (entidad != null)
            {
                entidad.Activo = urbanizadora.Activo;
            }
            await SaveAsync();
            return entidad.ConvertToDto();
        }


        /// <summary>
        /// Metodo para consultar el detalle de la urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">identificador de la urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        public async Task<UrbanizadoraDto> ConsultarDetalleUrbanizadora(long idUrbanizadora)
        {
            var query = (from u in DataContext.Urbanizadora
                         where u.Id == idUrbanizadora
                         select new UrbanizadoraDto
                         {
                             Id = u.Id,
                             Nombre = u.Nombre,
                             IdPais = u.IdPais,
                             Activo = u.Activo,
                             ListaPropiedades = (from uts in DataContext.UrbanizadoraTipo
                                                 join tp in DataContext.TipoPropiedad
                                                 on uts.IdTipoPropiedad equals tp.Id
                                                 where uts.IdUrbanizadora == u.Id && tp.Activo
                                                 select new TipoPropiedadDto
                                                 {
                                                     Id = tp.Id,
                                                     Tipo = uts.TipoPropiedad.Tipo
                                                 }).ToList()
                         }).FirstAsync();
            return await query;
        }


        /// <summary>
        /// Metodo para eliminar el detalle de la urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de la urbanizadora</param>
        /// <returns>bool</returns>
        public async Task<bool> EliminarDetalleUrbanizadora(long idUrbanizadora)
        {
            LafiseContext contexto = new LafiseContext();
            var entidad = await contexto.UrbanizadoraTipo.Where(s => s.IdUrbanizadora == idUrbanizadora).ToListAsync();
            contexto.UrbanizadoraTipo.RemoveRange(entidad);
            await contexto.SaveChangesAsync();
            return true;
        }


        /// <summary>
        /// Método que identifica si hay una urbanizadora asociada a un plan financiero
        /// </summary>
        /// <param name="idUrbanizadora">Identificador de la urbanizadora</param>
        /// <returns>bool</returns>
        public async Task<bool> ExistePlanFinanciacionPorUrbanizadora(long idUrbanizadora)
        {
            LafiseContext context = new LafiseContext();
            var query = await context.PlanFinanciacion.Where(s => s.IdUrbanizadora == idUrbanizadora).FirstOrDefaultAsync();
            return query != null ? true : false;
        }

    }
}
