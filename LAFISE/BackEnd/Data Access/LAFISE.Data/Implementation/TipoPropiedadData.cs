﻿/*==========================================================================
Archivo:            TipoPropiedadData
Descripción:        Administrador de la data de tipo de propiedad                    
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using AutoMapper;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Clase encargada de realizar el acceso a datos para  TipoPropiedad
    /// </summary>
    public class TipoPropiedadData : AsyncRepositoryBase<TipoPropiedad, LafiseContext>, ITipoPropiedadData
    {
        /// <summary>
        /// Ver todas los tipos de propiedad
        /// </summary>
        /// <returns>Lista de tipo de propiedad</returns>
        public async Task<ICollection<TipoPropiedadDto>> VerTodosGrid()
        {
            return await GetAllAsync<TipoPropiedadDto>();
        }
        /// <summary>
        ///  Ver todos los tipos de propiedades que están activas
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<TipoPropiedadDto>> VerTodosActivos()
        {
            var query = (from tp in DataContext.TipoPropiedad
                         where tp.Activo
                         select new TipoPropiedadDto
                         {
                             Id = tp.Id,
                             Tipo = tp.Tipo,
                             Activo = tp.Activo
                         }).ToListAsync();
            return await query;
        }
        /// <summary>
        /// Ver por identificador de tipo de propiedad
        /// </summary>
        /// <param name="id">Identificador de tipo de propiedad</param>
        /// <returns>Tipo de propiedad</returns>
        public async Task<TipoPropiedadDto> VerPorId(long id)
        {
            return await GetByAsync<TipoPropiedadDto>(c => c.Id == id);
        }

        /// <summary>
        /// Ver por identificador de tipo de propiedad no asincrono
        /// </summary>
        /// <param name="id">Identificador de tipo de propiedad</param>
        /// <returns>Tipo de propiedad</returns>
        public TipoPropiedadDto VerPorIdNoAsincrono(long id)
        {
            var query = (from tp in DataContext.TipoPropiedad
                         where tp.Id == id
                         select new TipoPropiedadDto
                         {
                             Id = tp.Id,
                             Tipo = tp.Tipo
                         }).FirstOrDefault();
            return query;
        }

        /// <summary>
        /// Ver tipo de propiedad por urbanizadora
        /// </summary>
        /// <param name="idUrbanizadora">identificador de la urbanizadora</param>
        /// <returns></returns>
        public async Task<ICollection<TipoPropiedadDto>> VerTiposPropiedadPorUrbanizadora(long idUrbanizadora)
        {
            var query = (from tp in DataContext.TipoPropiedad
                         join ut in DataContext.UrbanizadoraTipo on tp.Id equals ut.IdTipoPropiedad
                         where ut.IdUrbanizadora == idUrbanizadora
                         select new TipoPropiedadDto
                         {
                             Id = tp.Id,
                             Tipo = tp.Tipo
                         }).ToListAsync();
            return await query;
        }

        /// <summary>
        /// Eliminar tipo de propiedad
        /// </summary>
        /// <param name="id">Identificador del tipo de propiedad</param>
        public async Task Eliminar(long id)
        {
            await DeleteAsync(c => c.Id == id);
        }
        /// <summary>
        /// Crear tipo de propiedad
        /// </summary>
        /// <param name="tipopropiedad">Tipo de propiedad</param>
        /// <returns>Tipo de propiedad</returns>
        public async Task<TipoPropiedadDto> Crear(TipoPropiedadDto tipopropiedad)
        {
            var entidad = tipopropiedad.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }
        /// <summary>
        /// Editar tipo de propiedad
        /// </summary>
        /// <param name="tipopropiedad">Tipo de propiedad</param>
        /// <returns>Tipo de propiedad</returns>
        public async Task<TipoPropiedadDto> Editar(TipoPropiedadDto tipopropiedad)
        {
            var entidadOriginal = await GetByAsync(c => c.Id == tipopropiedad.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.Id = tipopropiedad.Id;
                entidadOriginal.Tipo = tipopropiedad.Tipo;
                entidadOriginal.Activo = tipopropiedad.Activo;
                await SaveAsync();
            }
            return tipopropiedad;
        }

        /// <summary>
        /// Validar que el nombre de la propiedad sea unico.
        /// </summary>
        /// <param name="Nombre">Nombre del tipo de propiedad.</param>
        /// <returns>Entidad con el tipo propiedad encontrada.</returns>
        public async Task<TipoPropiedadDto> ValidarTipoPropiedadUnica(long Id, string Nombre)
        {
            var entidad = await GetByAsync(c => c.Tipo == Nombre && c.Id != Id);
            return entidad.ConvertToDto();
        }


        /// <summary>
        /// Consulta de tipo de propiedad teniendo en cuenta los filtros
        /// </summary>
        /// <param name="tipoPropiedad">Nombre tipo de propiedad</param>
        /// <returns>Lista de tipo de propiedad</returns>
        public async Task<ICollection<TipoPropiedadDto>> TipoPropiedadFiltro(string tipoPropiedad)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@TipoPropiedad", tipoPropiedad);

            return await RunStoredProcedureAsync<ICollection<TipoPropiedadDto>, TipoPropiedadDto>("CargarTipoPropiedadFiltro", _parametros);

        }


        /// <summary>
        /// Consulta la validacion del tipo de propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Identificador de tipo de propiedad</param>
        /// <returns>Validar urbanizadora</returns>
        public async Task<UrbanizadoraValidarDto> ValidarTipoPropiedad(long tipoPropiedad)
        {
            try
            {
                Hashtable _parametros = new Hashtable();
                _parametros.Add("@TipoPropiedad", tipoPropiedad);

                return await RunStoredProcedureAsync<UrbanizadoraValidarDto, UrbanizadoraValidarDto>("CargarUrbanizadora", _parametros);
            }
            catch (Exception ex)
            {
                return new UrbanizadoraValidarDto { Nombre = ex.Message };
            }
        }
        /// <summary>
        /// Validación con la solicitud.
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns>Entidad de plan de financiación asociada.</returns>
        public async Task<PlanFinanciacionDto> validarRelacionPlanFinanciacion(long id)
        {
            var entidad = (from c in DataContext.PlanFinanciacion where c.IdTipoPropiedad == id select c).FirstOrDefault();
            Mapper.CreateMap<PlanFinanciacion, PlanFinanciacionDto>();
            var planfinanciaciondto = Mapper.Map<PlanFinanciacion, PlanFinanciacionDto>(entidad);
            return planfinanciaciondto;
        }

        /// <summary>
        /// Método que permite inactivar o activar un tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo de propiedad</param>
        /// <returns>bool</returns>
        public async Task<bool> Inactivar(TipoPropiedadDto tipoPropiedad)
        {
            var entidad = await GetByAsync(c => c.Id == tipoPropiedad.Id);
            if (entidad != null)
            {
                entidad.Activo = tipoPropiedad.Activo;
            }
            await SaveAsync();
            return entidad.Activo;
        }
    }
}
