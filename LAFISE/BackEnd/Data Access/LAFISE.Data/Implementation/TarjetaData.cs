﻿/*==========================================================================
Archivo:            TarjetaData
Descripción:       Administrador de la data de tarjeta                 
Autor:              paola.munoz                         
Fecha de creación:  29/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using LAFISE.Common.Backend;
using System.Linq;
using System.Data.Entity;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Clase encargada de realizar el acceso a datos para  Tarjeta
    /// </summary>
    public class TarjetaData : AsyncRepositoryBase<Tarjeta, LafiseContext>, ITarjetaData
    {
        /// <summary>
        /// Ver todas las tarjetas
        /// </summary>
        /// <returns>Lista de tarjetas</returns>
        public async Task<ICollection<TarjetaDto>> VerTodosGrid()
        {
            IParametrosData parametros = new ParametrosData();
            var ruta = parametros.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_TARJETA);
            LafiseContext context = new LafiseContext();
            var query = (from t in context.Tarjeta
                         select new TarjetaDto
                         {
                             Id = t.Id,
                             TarjetaCredito = t.TarjetaCredito,
                             Imagen = t.Imagen,
                             Activo = t.Activo,
                             RutaImagen = ruta.Result + t.Imagen
                         }).ToListAsync();
            return await query;
        }
        /// <summary>
        /// Ver por identificador las tarjetas
        /// </summary>
        /// <param name="id">Identificador de las tarjetas</param>
        /// <returns>Tarjetas</returns>
        public async Task<TarjetaDto> VerPorId(Int64 id)
        {
            return await GetByAsync<TarjetaDto>(c => c.Id == id);
        }
        /// <summary>
        /// Eliminar las tarjetas
        /// </summary>
        /// <param name="id">Identificador de las tarjetas</param>
        public async Task Eliminar(Int64 id)
        {
            await DeleteAsync(c => c.Id == id);
        }
        /// <summary>
        /// Crear tarjetas
        /// </summary>
        /// <param name="tarjeta">Tarjetas</param>
        /// <returns>tarjeta</returns>
        public async Task<TarjetaDto> Crear(TarjetaDto tarjeta)
        {
            var entidad = tarjeta.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }
        /// <summary>
        /// Editar tarjeta
        /// </summary>
        /// <param name="tarjeta">tarjeta</param>
        /// <returns>Tarjeta</returns>
        public async Task<TarjetaDto> Editar(TarjetaDto tarjeta)
        {
            var entidadOriginal = await GetByAsync(c => c.Id == tarjeta.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.Id = tarjeta.Id;
                entidadOriginal.TarjetaCredito = tarjeta.TarjetaCredito;
                entidadOriginal.Imagen = tarjeta.Imagen;
                entidadOriginal.Activo = tarjeta.Activo;
                await SaveAsync();
            }
            return tarjeta;
        }


        /// <summary>
        /// Consulta de tipo de propiedad teniendo en cuenta los filtros
        /// </summary>
        /// <param name="tarjeta">Nombre de la tarjeta</param>
        /// <returns>Lista de tarjeta</returns>
        public async Task<ICollection<TarjetaDto>> TarjetaFiltro(string tarjeta)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@TarjetaCredito", tarjeta);

            return await RunStoredProcedureAsync<ICollection<TarjetaDto>, TarjetaDto>("CargarTarjetaFiltro", _parametros);

        }

        /// <summary>
        /// Método que permite inactivar o activar un tipo propiedad
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>bool</returns>
        public async Task<bool> Inactivar(TarjetaDto tarjeta)
        {
            var entidad = await GetByAsync(c => c.Id == tarjeta.Id);
            if (entidad != null)
            {
                entidad.Activo = tarjeta.Activo;
            }
            await SaveAsync();
            return entidad.Activo;
        }

        /// <summary>
        /// Validar que el nombre de tarjetas sea unico.
        /// </summary>
        /// <param name="tarjeta">Entidad tarjeta</param>
        /// <returns>Entidad tarjeta encontrada.</returns>
        public async Task<TarjetaDto> ValidarTarjetaUnica(TarjetaDto tarjeta)
        {
            var entidad = await GetByAsync(c => c.TarjetaCredito == tarjeta.TarjetaCredito && c.Id != tarjeta.Id);
            return entidad.ConvertToDto();
        }
    }
}
