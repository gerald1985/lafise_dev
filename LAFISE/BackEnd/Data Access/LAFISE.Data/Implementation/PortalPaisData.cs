﻿/*==========================================================================
Archivo:            PortalPaisData
Descripción:       Administrador el portal por pais               
Autor:              paola.munoz                         
Fecha de creación:  10/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using LAFISE.Common.Backend;
using System.Linq;
using System.Data.Entity;
#endregion

namespace LAFISE.Data
{
    public class PortalPaisData : AsyncRepositoryBase<PortalPais, LafiseContext>, IPortalPaisData
    {

        /// <summary>
        /// Metodo para obtener el país teniendo en cuenta la sigla del portal
        /// </summary>
        /// <param name="siglaPortal">Sigla del portal</param>
        /// <returns>Identificador del país</returns>
        public async Task<PortalPaisDto> ObtenerPaisPorPortal(string siglaPortal)
        {
            return await GetByAsync<PortalPaisDto>(c => c.Sigla == siglaPortal);
        }

        /// <summary>
        /// Metodo para obtener el país teniendo en cuenta la sigla del portal
        /// </summary>
        /// <param name="siglaPortal">Sigla del portal</param>
        /// <returns>Identificador del país</returns>
        public PortalPaisDto ObtenerPaisPorPortalNoAsincrono(string siglaPortal)
        {
            var portal= (from s in DataContext.PortalPais
                         where s.Sigla== siglaPortal
                        select new PortalPaisDto
                        {
                            Id = s.Id,
                            IdPais = s.IdPais.HasValue? s.IdPais.Value:0,
                            Nombre = s.Nombre,
                            Sigla = s.Sigla
                        }).FirstOrDefault();

            return portal;
        }

    }
}
