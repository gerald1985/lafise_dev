﻿/*==========================================================================
Archivo:            SolicitudData
Descripción:        Administrador de la data de solicitud                     
Autor:              paola.munoz                          
Fecha de creación:  13/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Collections;
using System.Data.Entity;
using System;
using LAFISE.Common.Backend;
using System.Data.Entity.Infrastructure;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Clase encargada de realizar el acceso a datos para  Solicitud
    /// </summary>
    public class SolicitudData : AsyncRepositoryBase<Solicitud, LafiseContext>, ISolicitudData
    {
#if DEBUG
        /// <summary>
        /// Constructor
        /// </summary>
        public SolicitudData()
        {
            DataContext.Database.Log = Log;
        }
        /// <summary>
        /// Metodo log
        /// </summary>
        /// <param name="message">Mensaje</param>
        void Log(string message)
        {
            System.Diagnostics.Debug.Write(message);
        }
#endif

        /// <summary>
        /// Método usado para ver todas las solicitudes
        /// </summary>
        /// <returns>Lista de solicitud</returns>
        public async Task<ICollection<SolicitudDto>> VerTodos()
        {
            return await GetAllAsync<SolicitudDto>();
        }

        /// <summary>
        /// Ver solicitud general por id
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<SolicitudDto> VerFormularioGeneralPorIdSolicitud(Int64 id)
        {
            var moneda = (from s in DataContext.Solicitud
                          join d in DataContext.RangoSueldo on s.IdPais equals d.IdPais
                          join e in DataContext.Moneda on d.IdMoneda equals e.Id
                          where s.Id == id
                          select e.Simbolo).FirstOrDefault();

            LafiseContext context = new LafiseContext();
            var query = (from s in context.Solicitud
                         where s.Id == id
                         select new SolicitudDto
                         {
                             Id = s.Id,
                             IdEstado = s.IdEstado,
                             IdPais = s.IdPais,
                             IdTipoContacto = s.IdTipoContacto,
                             TipoContacto = s.TipoContacto.Nombre,
                             IdTipoSolicitud = s.IdTipoSolicitud,
                             DatosPersonales = (from dp in context.DatosPersonales
                                                where dp.IdSolicitud == id
                                                select new DatosPersonalesDto
                                                {
                                                    Id = dp.Id,
                                                    Nombre = dp.Nombre,
                                                    Apellidos = dp.Apellidos,
                                                    Telefono = dp.Telefono,
                                                    Email = dp.Email,
                                                    Masculino = dp.Masculino,
                                                    Genero = dp.Masculino ? "Masculino" : "Femenino",
                                                    Nacionalidad = dp.Nacionalidad,
                                                    CiudadResidencia = dp.CiudadResidencia,
                                                    IdTipoIdentificacion = dp.IdTipoIdentificacion,
                                                    TipoIdentificacion = dp.TipoIdentificacion.Nombre,
                                                    NumeroIdentificacion = dp.NumeroIdentificacion,
                                                    TipoDocumentoSolicitud = dp.TipoDocumentoSolicitud,
                                                    Profesion = dp.Profesion,
                                                    IdSolicitud = dp.IdSolicitud,
                                                    EsCodeudor = dp.EsCodeudor,
                                                    EstadoCivil = dp.EstadoCivil,
                                                    Dependientes = dp.Dependientes,
                                                    DireccionResidencia = dp.DireccionResidencia,
                                                    TipoResidencia = dp.TipoResidencia,
                                                    EstadoResidencia = dp.EstadoResidencia,
                                                    AniosResidencia = dp.AniosResidencia,
                                                    Educacion = dp.Educacion,

                                                    /* INICIA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
                                                    SegundoNombre = dp.SegundoNombre,
                                                    SegundoApellido = dp.SegundoApellido,
                                                    PrimerNombreBeneficiario = dp.PrimerNombreBeneficiario,
                                                    SegundoNombreBeneficiario = dp.SegundoNombreBeneficiario,
                                                    PrimerApellidoBeneficiario = dp.PrimerApellidoBeneficiario,
                                                    SegundoApellidoBeneficiario = dp.SegundoApellidoBeneficiario,
                                                    TipoIdBeneficiario = dp.TipoIdBeneficiario,
                                                    NumeroIdBeneficiario = dp.NumeroIdBeneficiario,
                                                    DireccionBeneficiario = dp.DireccionBeneficiario,
                                                    ParentescoBeneficiario = dp.ParentescoBeneficiario,
                                                    PrimerNombreTutor = dp.PrimerNombreTutor,
                                                    SegundoNombreTutor = dp.SegundoNombreTutor,
                                                    PrimerApellidoTutor = dp.PrimerApellidoTutor,
                                                    SegundoApellidoTutor = dp.SegundoApellidoTutor,
                                                    TipoIdTutor = dp.TipoIdTutor,
                                                    NumeroIdTutor = dp.NumeroIdTutor,
                                                    ParentescoTutor = dp.ParentescoTutor,
                                                    TelefonoTutor = dp.TelefonoTutor,
                                                    EmailTutor = dp.EmailTutor
                                                    /* FINALIZA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */



                                                }).ToList(),
                             DatosFinancieros = (from df in context.DatosFinancieros
                                                 where df.IdSolicitud == id
                                                 select new DatosFinancieroDto
                                                 {
                                                     Id = df.Id,
                                                     IdSolicitud = df.IdSolicitud,
                                                     EsCliente = df.EsCliente,
                                                     PoseeNomina = df.PoseeNomina,
                                                     NegocioPropio = df.NegocioPropio,
                                                     Asalariado = df.Asalariado,
                                                     PromedioIngresoMensualNombre = df.PromedioIngresoMensualNombre,
                                                     EsZurdo = df.EsZurdo,
                                                     EsZurdoNombre = df.EsZurdo.HasValue ? "Zurdo" : "Derecho",
                                                     Estatura = df.Estatura,
                                                     Peso = df.Peso,
                                                     EstadoCuenta = df.EstadoCuenta,
                                                     EstadoCuentaNombre = df.EstadoCuenta.HasValue ? "Físico" : "Electrónico",
                                                     Direccion = df.Direccion,
                                                     Sucursal = df.Sucursal.Nombre,
                                                     IndustriaTrabajo = df.IndustriaTrabajo,
                                                     Antiguedad = df.Antiguedad,
                                                     DireccionOficina = df.DireccionOficina,
                                                     TelefonoOficina = df.TelefonoOficina,
                                                     /* INICIA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
                                                     SucursalEnvio = df.SucursalEnvio,
                                                     /* FINALIZA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
                                                 }).ToList(),
                             PlanFinanciacion = (from pf in context.PlanFinanciacion
                                                 where pf.IdSolicitud == id
                                                 select new PlanFinanciacionDto
                                                 {
                                                     Id = pf.Id,
                                                     IdSolicitud = pf.IdSolicitud,
                                                     ValorBien = pf.ValorBien,
                                                     MontoFinanciar = pf.MontoFinanciar,
                                                     PrimaAnticipo = pf.PrimaAnticipo,
                                                     Plazo = pf.Plazo,
                                                     AutoUsado = pf.AutoUsado.Value,
                                                     IdCategoria = pf.IdCategoria,
                                                     Categoria = pf.IdCategoria.HasValue ? pf.Categoria.Nombre : "",
                                                     IdMarcaAuto = pf.IdMarcaAuto,
                                                     MarcaAuto = pf.IdMarcaAuto.HasValue ? pf.MarcaAuto.Marca : "",
                                                     IdModelo = pf.IdModelo,
                                                     Modelo = pf.IdModelo.HasValue ? pf.ModeloAuto.Modelo : "",
                                                     IdCasaComercial = pf.IdCasaComercial,
                                                     CasaComercial = pf.IdCasaComercial.HasValue ? pf.CasaComercial.Nombre : "",
                                                     OtroMarcaAuto = pf.OtroMarcaAuto,
                                                     OtroModelo = pf.OtroModelo,
                                                     OtroCasaComercial = pf.OtroCasaComercial,
                                                     IdGarantia = pf.IdGarantia,
                                                     Garantia = pf.IdGarantia.HasValue ? pf.Garantia.Nombre : "",
                                                     ConstruccionPropia = pf.ConstruccionPropia,
                                                     IdTipoPropiedad = pf.IdTipoPropiedad,
                                                     TipoPropiedad = pf.IdTipoPropiedad.HasValue ? pf.TipoPropiedad.Tipo : "",
                                                     IdUrbanizadora = pf.IdUrbanizadora,
                                                     Urbanizadora = pf.IdUrbanizadora.HasValue ? pf.Urbanizadora.Nombre : "",
                                                     CostoPrograma = pf.CostoPrograma,
                                                     NombreEmpresaInmobiliaria = pf.NombreEmpresaInmobiliaria,
                                                     NombreProyectoResidencial = pf.NombreProyectoResidencial,
                                                     DireccionVivienda = pf.DireccionVivienda,
                                                     PropositoCompra = pf.PropositoCompra,
                                                     Simbolo = moneda.ToString(),
                                                 }).ToList(),
                             Soportes = (from sop in context.Soportes
                                         where sop.IdSolicitud == id
                                         select new SoporteDto
                                         {
                                             Id = s.Id,
                                             IdSolicitud = sop.IdSolicitud,
                                             IdTipoSoporte = sop.IdTipoSoporte,
                                             Nombre = sop.Nombre,
                                             Ruta = sop.Ruta,
                                             EsCodeudor=sop.EsCodeudor
                                         }).ToList(),
                             HistoricoComentario = (from hc in context.HistoricoComentario
                                                    where hc.IdSolicitud == id
                                                    select new HistoricoComentarioDto
                                                    {
                                                        Id = hc.Id,
                                                        IdSolicitud = hc.IdSolicitud,
                                                        IdEstado = hc.IdEstado,
                                                        Estado = hc.Estado.Nombre,
                                                        Comentario = hc.Comentario,
                                                        FechaCreacion = hc.FechaCreacion
                                                    }).ToList()
                         }).FirstOrDefault();

            return query;
        }

        /// <summary>
        /// Ver información de los datos personales para el envio del correo
        /// </summary>
        /// <param name="idSolicitud">Identificador de la solicitud</param>
        /// <returns>DatosPersonalesDto</returns>
        public DatosPersonalesDto VerDatosPersonales(Int64 idSolicitud)
        {
            LafiseContext context = new LafiseContext();
            var query = (from dp in context.DatosPersonales
                         where dp.IdSolicitud == idSolicitud
                         select new DatosPersonalesDto
                         {
                             Id = dp.Id,
                             Nombre = dp.Nombre,
                             Apellidos = dp.Apellidos,
                             Telefono = dp.Telefono,
                             Email = dp.Email,
                             Masculino = dp.Masculino,
                             Genero = dp.Masculino ? "Masculino" : "Femenino",
                             Nacionalidad = dp.Nacionalidad,
                             CiudadResidencia = dp.CiudadResidencia,
                             IdTipoIdentificacion = dp.IdTipoIdentificacion,
                             TipoIdentificacion = dp.TipoIdentificacion.Nombre,
                             NumeroIdentificacion = dp.NumeroIdentificacion,
                             Profesion = dp.Profesion,
                             IdSolicitud = dp.IdSolicitud,
                             EsCodeudor = dp.EsCodeudor
                         }).FirstOrDefault();

            return query;
        }

        /// <summary>
        /// Ver formulario cuando tienen referencias personales por id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<SolicitudDto> VerFormularioConReferenciasPorIdSolicitud(Int64 id)
        {

            LafiseContext context = new LafiseContext();
            var query = (from s in context.Solicitud
                         where s.Id == id
                         select new SolicitudDto
                         {
                             Id = s.Id,
                             IdEstado = s.IdEstado,
                             IdPais = s.IdPais,
                             IdTipoContacto = s.IdTipoContacto,
                             TipoContacto = s.TipoContacto.Nombre,
                             IdTipoSolicitud = s.IdTipoSolicitud,
                             DatosPersonales = (from dp in context.DatosPersonales
                                                where dp.IdSolicitud == id
                                                select new DatosPersonalesDto
                                                {
                                                    Id = dp.Id,
                                                    Nombre = dp.Nombre,
                                                    Apellidos = dp.Apellidos,
                                                    Telefono = dp.Telefono,
                                                    Email = dp.Email,
                                                    Masculino = dp.Masculino,
                                                    Genero = dp.Masculino ? "Masculino" : "Femenino",
                                                    Nacionalidad = dp.Nacionalidad,
                                                    CiudadResidencia = dp.CiudadResidencia,
                                                    IdTipoIdentificacion = dp.IdTipoIdentificacion,
                                                    TipoIdentificacion = dp.TipoIdentificacion.Nombre,
                                                    NumeroIdentificacion = dp.NumeroIdentificacion,
                                                    Profesion = dp.Profesion,
                                                    IdSolicitud = dp.IdSolicitud,
                                                    EsCodeudor = dp.EsCodeudor
                                                }).ToList(),
                             DatosFinancieros = (from df in context.DatosFinancieros
                                                 where df.IdSolicitud == id
                                                 select new DatosFinancieroDto
                                                 {
                                                     Id = df.Id,
                                                     IdSolicitud = df.IdSolicitud,
                                                     EsCliente = df.EsCliente,
                                                     PoseeNomina = df.PoseeNomina,
                                                     NegocioPropio = df.NegocioPropio,
                                                     Asalariado = df.Asalariado,
                                                     PromedioIngresoMensualNombre = df.PromedioIngresoMensualNombre,
                                                     EsZurdo = df.EsZurdo,
                                                     Estatura = df.Estatura,
                                                     Peso = df.Peso,
                                                     EstadoCuenta = df.EstadoCuenta,
                                                     Direccion = df.Direccion,
                                                     IdSucursal = df.IdSucursal,
                                                     Sucursal = df.IdSucursal.HasValue ? df.Sucursal.Nombre : ""
                                                 }).ToList(),
                             Soportes = (from sop in context.Soportes
                                         where sop.IdSolicitud == id
                                         select new SoporteDto
                                         {
                                             Id = s.Id,
                                             IdSolicitud = sop.IdSolicitud,
                                             IdTipoSoporte = sop.IdTipoSoporte,
                                             Nombre = sop.Nombre,
                                             Ruta = sop.Ruta,
                                             EsCodeudor= sop.EsCodeudor
                                         }).ToList(),
                             Referencias = (from r in context.Referencias
                                            where r.IdSolicitud == id
                                            select new ReferenciaDto
                                            {
                                                Nombre = r.Nombre,
                                                Apellidos = r.Apellidos,
                                                Telefono = r.Telefono
                                            }).ToList(),
                             HistoricoComentario = (from hc in context.HistoricoComentario
                                                    where hc.IdSolicitud == id
                                                    select new HistoricoComentarioDto
                                                    {
                                                        Id = hc.Id,
                                                        IdSolicitud = hc.IdSolicitud,
                                                        IdEstado = hc.IdEstado,
                                                        Estado = hc.Estado.Nombre,
                                                        Comentario = hc.Comentario,
                                                        FechaCreacion = hc.FechaCreacion
                                                    }).ToList()
                         }).FirstOrDefault();

            return query;
        }


        /// <summary>
        /// Ver formulario para la solicitud de seguro de vida por id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<SolicitudDto> VerFormularioSeguroVidaPorIdSolicitud(Int64 id)
        {
            LafiseContext context = new LafiseContext();
            var query = (from s in context.Solicitud
                         where s.Id == id
                         select new SolicitudDto
                         {
                             Id = s.Id,
                             IdEstado = s.IdEstado,
                             IdPais = s.IdPais,
                             IdTipoContacto = s.IdTipoContacto,
                             TipoContacto = s.TipoContacto.Nombre,
                             IdTipoSolicitud = s.IdTipoSolicitud,
                             DatosPersonales = (from dp in context.DatosPersonales
                                                where dp.IdSolicitud == id
                                                select new DatosPersonalesDto
                                                {
                                                    Id = dp.Id,
                                                    Nombre = dp.Nombre,
                                                    Apellidos = dp.Apellidos,
                                                    Telefono = dp.Telefono,
                                                    Email = dp.Email,
                                                    Masculino = dp.Masculino,
                                                    Genero = dp.Masculino ? "Masculino" : "Femenino",
                                                    Nacionalidad = dp.Nacionalidad,
                                                    CiudadResidencia = dp.CiudadResidencia,
                                                    IdTipoIdentificacion = dp.IdTipoIdentificacion,
                                                    TipoIdentificacion = dp.TipoIdentificacion.Nombre,
                                                    NumeroIdentificacion = dp.NumeroIdentificacion,
                                                    Profesion = dp.Profesion,
                                                    IdSolicitud = dp.IdSolicitud,
                                                    EsCodeudor = dp.EsCodeudor
                                                }).ToList(),
                             DatosFinancieros = (from df in context.DatosFinancieros
                                                 where df.IdSolicitud == id
                                                 select new DatosFinancieroDto
                                                 {
                                                     Id = df.Id,
                                                     IdSolicitud = df.IdSolicitud,
                                                     EsCliente = df.EsCliente,
                                                     PoseeNomina = df.PoseeNomina,
                                                     NegocioPropio = df.NegocioPropio,
                                                     Asalariado = df.Asalariado,
                                                     PromedioIngresoMensualNombre = df.PromedioIngresoMensualNombre,
                                                     EsZurdo = df.EsZurdo,
                                                     Estatura = df.Estatura,
                                                     Peso = df.Peso,
                                                     EstadoCuenta = df.EstadoCuenta,
                                                     Direccion = df.Direccion,
                                                     ActividadEconomica = (from ae in context.ActividadEconomica
                                                                           where ae.IdDatosFinancieros == df.Id
                                                                           select new ActividadEconomicaDto
                                                                           {
                                                                               Id = ae.Id,
                                                                               IdDatosFinancieros = ae.IdDatosFinancieros,
                                                                               IdTipoActividad = ae.IdTipoActividad,
                                                                               TipoActividad = ae.TipoActividad.Nombre,
                                                                               OtraActividad = ae.OtraActividad
                                                                           }).ToList()
                                                 }).ToList(),
                             HistoricoComentario = (from hc in context.HistoricoComentario
                                                    where hc.IdSolicitud == id
                                                    select new HistoricoComentarioDto
                                                    {
                                                        Id = hc.Id,
                                                        IdSolicitud = hc.IdSolicitud,
                                                        IdEstado = hc.IdEstado,
                                                        Estado = hc.Estado.Nombre,
                                                        Comentario = hc.Comentario,
                                                        FechaCreacion = hc.FechaCreacion
                                                    }).ToList()
                         }).FirstOrDefault();

            return query;
        }

        /// <summary>
        /// Ver formulario para la solicitud de seguro vehicular por id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<SolicitudDto> VerFormularioSeguroVehicularPorIdSolicitud(Int64 id)
        {
            var moneda = (from s in DataContext.Solicitud
                          join d in DataContext.RangoSueldo on s.IdPais equals d.IdPais
                          join e in DataContext.Moneda on d.IdMoneda equals e.Id
                          where s.Id == id
                          select e.Simbolo).FirstOrDefault();

            LafiseContext context = new LafiseContext();
            var query = (from s in context.Solicitud
                         where s.Id == id
                         select new SolicitudDto
                         {
                             Id = s.Id,
                             IdEstado = s.IdEstado,
                             IdPais = s.IdPais,
                             IdTipoContacto = s.IdTipoContacto,
                             TipoContacto = s.TipoContacto.Nombre,
                             IdTipoSolicitud = s.IdTipoSolicitud,
                             DatosPersonales = (from dp in context.DatosPersonales
                                                where dp.IdSolicitud == id
                                                select new DatosPersonalesDto
                                                {
                                                    Id = dp.Id,
                                                    Nombre = dp.Nombre,
                                                    Apellidos = dp.Apellidos,
                                                    Telefono = dp.Telefono,
                                                    Email = dp.Email,
                                                    Masculino = dp.Masculino,
                                                    Genero = dp.Masculino ? "Masculino" : "Femenino",
                                                    Nacionalidad = dp.Nacionalidad,
                                                    CiudadResidencia = dp.CiudadResidencia,
                                                    IdTipoIdentificacion = dp.IdTipoIdentificacion,
                                                    TipoIdentificacion = dp.TipoIdentificacion.Nombre,
                                                    NumeroIdentificacion = dp.NumeroIdentificacion,
                                                    Profesion = dp.Profesion,
                                                    IdSolicitud = dp.IdSolicitud,
                                                    EsCodeudor = dp.EsCodeudor
                                                }).ToList(),
                             DatosFinancieros = (from df in context.DatosFinancieros
                                                 where df.IdSolicitud == id
                                                 select new DatosFinancieroDto
                                                 {
                                                     Id = df.Id,
                                                     IdSolicitud = df.IdSolicitud,
                                                     EsCliente = df.EsCliente,
                                                     PoseeNomina = df.PoseeNomina,
                                                     NegocioPropio = df.NegocioPropio,
                                                     Asalariado = df.Asalariado,
                                                     PromedioIngresoMensualNombre = df.PromedioIngresoMensualNombre,
                                                 }).ToList(),
                             DatosBien = (from da in context.DatosBien
                                          where da.IdSolicitud == id
                                          select new DatosBienDto
                                          {
                                              Id = da.Id,
                                              IdMarcaAuto = da.IdMarcaAuto,
                                              IdModelo = da.IdModelo,
                                              IdTipoVehiculo = da.IdTipoVehiculo,
                                              MarcaAuto = da.MarcaAuto.Marca,
                                              ModeloAuto = da.ModeloAuto.Modelo,
                                              TipoVehiculo = da.TipoVehiculo.Nombre,
                                              IdSolicitud = da.IdSolicitud,
                                              ValorBien = da.ValorBien,
                                              Simbolo = moneda.ToString(),
                                              IdTipoBien = da.IdTipoBien,
                                              TipoBien = da.TipoBien.Nombre
                                          }).ToList(),
                             HistoricoComentario = (from hc in context.HistoricoComentario
                                                    where hc.IdSolicitud == id
                                                    select new HistoricoComentarioDto
                                                    {
                                                        Id = hc.Id,
                                                        IdSolicitud = hc.IdSolicitud,
                                                        IdEstado = hc.IdEstado,
                                                        Estado = hc.Estado.Nombre,
                                                        Comentario = hc.Comentario,
                                                        FechaCreacion = hc.FechaCreacion
                                                    }).ToList()
                         }).FirstOrDefault();



            return query;
        }



        /// <summary>
        /// Ver formulario para reclamos por id de la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<SolicitudDto> VerFormularioReclamosPorIdSolicitud(Int64 id)
        {
            LafiseContext context = new LafiseContext();
            var query = (from s in context.Solicitud
                         where s.Id == id
                         select new SolicitudDto
                         {
                             Id = s.Id,
                             IdEstado = s.IdEstado,
                             IdPais = s.IdPais,
                             IdTipoContacto = s.IdTipoContacto,
                             TipoContacto = s.TipoContacto.Nombre,
                             IdTipoSolicitud = s.IdTipoSolicitud,
                             DatosPersonales = (from dp in context.DatosPersonales
                                                where dp.IdSolicitud == id
                                                select new DatosPersonalesDto
                                                {
                                                    Id = dp.Id,
                                                    Nombre = dp.Nombre,
                                                    Apellidos = dp.Apellidos,
                                                    Telefono = dp.Telefono,
                                                    Email = dp.Email,
                                                    Masculino = dp.Masculino,
                                                    Genero = dp.Masculino ? "Masculino" : "Femenino",
                                                    Nacionalidad = dp.Nacionalidad,
                                                    CiudadResidencia = dp.CiudadResidencia,
                                                    IdTipoIdentificacion = dp.IdTipoIdentificacion,
                                                    TipoIdentificacion = dp.TipoIdentificacion.Nombre,
                                                    NumeroIdentificacion = dp.NumeroIdentificacion,
                                                    Profesion = dp.Profesion,
                                                    IdSolicitud = dp.IdSolicitud,
                                                    EsCodeudor = dp.EsCodeudor
                                                }).ToList(),
                             Siniestro = (from sin in context.Siniestro
                                          where sin.IdSolicitud == id
                                          select new SiniestroDto
                                          {
                                              Id = sin.Id,
                                              IdSolicitud = sin.IdSolicitud,
                                              NumeroPoliza = sin.NumeroPoliza,
                                              Detalle = sin.Detalle,
                                              Fecha = sin.Fecha,
                                              SiniestroCiudad = (from sc in context.SiniestroCiudad
                                                                 where sc.IdSiniestro == sin.Id
                                                                 select new SiniestroCiudadDto
                                                                 {
                                                                     Id = sc.Id,
                                                                     IdCiudad = sc.IdCiudad,
                                                                     Ciudad = sc.Ciudad.Nombre,
                                                                     IdSiniestro = sc.IdSiniestro
                                                                 }).ToList(),
                                              SiniestroPais = (from sp in context.SiniestroPais
                                                               where sp.IdSiniestro == sin.Id
                                                               select new SiniestroPaisDto
                                                               {
                                                                   Id = sp.Id,
                                                                   IdPais = sp.IdPais,
                                                                   Pais = sp.Pais.Nombre,
                                                                   IdSiniestro = sp.IdSiniestro
                                                               }).ToList()
                                          }).ToList(),
                             HistoricoComentario = (from hc in context.HistoricoComentario
                                                    where hc.IdSolicitud == id
                                                    select new HistoricoComentarioDto
                                                    {
                                                        Id = hc.Id,
                                                        IdSolicitud = hc.IdSolicitud,
                                                        IdEstado = hc.IdEstado,
                                                        Estado = hc.Estado.Nombre,
                                                        Comentario = hc.Comentario,
                                                        FechaCreacion = hc.FechaCreacion
                                                    }).ToList()
                         }).FirstOrDefault();

            return query;
        }


        /// <summary>
        /// Metodo que se encarga de crear toda la solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<SolicitudDto> CrearSolicitud(SolicitudDto solicitud)
        {

            var IdSolProd = (from s in DataContext.Solicitud
                             join d in DataContext.Pais on s.IdPais equals d.Id
                             where s.IdTipoSolicitud == solicitud.IdTipoSolicitud
                             where s.IdPais == solicitud.IdPais
                             select s.IdSolicitudProducto).DefaultIfEmpty(0).Max();
            if (IdSolProd <= 0)
            {
                solicitud.IdSolicitudProducto = 1;
            } else
            {
                solicitud.IdSolicitudProducto = IdSolProd + 1;
            }

            var entidad = solicitud.Convert();
            await AddAsync(entidad);

            var solicitudResult = entidad.ConvertToDto();
             LafiseContext context = new LafiseContext();
             var paisCod = (from p in context.Pais
                            where p.Id == solicitud.IdPais
                            select p.Codigo).FirstOrDefault();
             solicitudResult.CodigoIdSolicitud = paisCod + "-" + entidad.Id;
            return solicitudResult;
        }

        /// <summary>
        /// Metodo que se encarga de editar la solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>Solicitud</returns>
        public async Task<SolicitudDto> EditarSolicitud(SolicitudDto solicitud, string backOfficeUser)
        {

            var entidad = await GetByAsync(c => c.Id == solicitud.Id);
            if (entidad != null)
            {
                entidad.Id = solicitud.Id;
                entidad.IdEstado = solicitud.IdEstado;
                entidad.IdTipoContacto = solicitud.IdTipoContacto;
                entidad.FechaActualizacion = solicitud.FechaActualizacion;
                entidad.BackOfficeUser = backOfficeUser;
            }
            //Guardar el historico de los comentarios
            foreach (var item in solicitud.HistoricoComentario)
            {
                HistoricoComentario _historicoComentario = new HistoricoComentario();
                _historicoComentario.IdSolicitud = item.IdSolicitud;
                _historicoComentario.IdEstado = item.IdEstado;
                _historicoComentario.Comentario = item.Comentario;
                _historicoComentario.FechaCreacion = solicitud.FechaActualizacion.Value;
                entidad.HistoricoComentario.Add(_historicoComentario);
            }

            await SaveAsync();
            var solicitudResult = entidad.ConvertToDto();

            LafiseContext context = new LafiseContext();
            var paisCod = (from p in context.Pais
                           where p.Id == solicitudResult.IdPais
                           select p.Codigo).FirstOrDefault();

            solicitudResult.CodigoIdSolicitud = paisCod + "-" + entidad.Id;

            return solicitudResult;
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista Solicitud filtro</returns>
        public async Task<ICollection<SolicitudFiltroDto>> VerGestorSolicitud(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);

            return await RunStoredProcedureAsync<ICollection<SolicitudFiltroDto>, SolicitudFiltroDto>("CargarSolicitud", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion marca auto</returns>
        public async Task<ICollection<ExportarMarcaAutoDto>> CargarExportarMarcaAuto(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarMarcaAutoDto>, ExportarMarcaAutoDto>("CargarExportarMarcaAuto", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion prestamos personales</returns>
        public async Task<ICollection<ExportarPrestamoPersonalDto>> CargarExportarPrestamoPersonal(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarPrestamoPersonalDto>, ExportarPrestamoPersonalDto>("CargarExportarPrestamoPersonal", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion prestamos hipotecarios</returns>
        public async Task<ICollection<ExportarPrestamoHipotecarioDto>> CargarExportarPrestamoHipotecario(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarPrestamoHipotecarioDto>, ExportarPrestamoHipotecarioDto>("CargarExportarPrestamoHipotecario", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion tarjeta de credito o debito</returns>
        public async Task<ICollection<ExportarTarjetaCreditoDebitoDto>> CargarExportarTarjetaCreditoDebito(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarTarjetaCreditoDebitoDto>, ExportarTarjetaCreditoDebitoDto>("CargarExportarTarjetaCreditoDebito", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion prestamo educativo</returns>
        public async Task<ICollection<ExportarPrestamoEducativoDto>> CargarExportarPrestamoEducativo(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarPrestamoEducativoDto>, ExportarPrestamoEducativoDto>("CargarExportarPrestamoEducativo", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion prestamo educativo</returns>
        public async Task<ICollection<ExportarAperturaCuentaDto>> CargarExportarAperturaCuenta(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarAperturaCuentaDto>, ExportarAperturaCuentaDto>("CargarExportarAperturaCuenta", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion seguro de vidad y accidente</returns>
        public async Task<ICollection<ExportarSeguroVidaAccidenteDto>> CargarExportarSeguroVidaAccidente(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarSeguroVidaAccidenteDto>, ExportarSeguroVidaAccidenteDto>("CargarExportarSeguroVidaAccidente", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion seguro vehicular</returns>
        public async Task<ICollection<ExportarSeguroVehicularDto>> CargarExportarSeguroVehicular(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarSeguroVehicularDto>, ExportarSeguroVehicularDto>("CargarExportarSeguroVehicular", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información en los gestores para la exportacion
        /// </summary>
        /// <param name="solicitudCondicion">filtro en solicitudCondicion</param>
        /// <returns>Lista exportacion reclamos</returns>
        public async Task<ICollection<ExportarReclamosDto>> CargarExportarReclamos(SolicitudCondicionesDto solicitudCondicion)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idTipoSolicitud", solicitudCondicion.IdTipoSolicitud);
            _parametros.Add("@idEstado", solicitudCondicion.Estado);
            _parametros.Add("@idPais", solicitudCondicion.IdPais);

            return await RunStoredProcedureAsync<ICollection<ExportarReclamosDto>, ExportarReclamosDto>("CargarExportarReclamos", _parametros);
        }


    }
}
