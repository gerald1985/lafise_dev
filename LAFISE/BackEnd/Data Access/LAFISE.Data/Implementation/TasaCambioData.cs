﻿/*==========================================================================
Archivo:            TasaCambioData
Descripción:       Administrador de la data de tasa de cambio                 
Autor:              paola.munoz                         
Fecha de creación:  25/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using LAFISE.Common.Backend;
using System.Linq;
using System.Data.Entity;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Clase encargada de realizar el acceso a datos para tasa de cambio
    /// </summary>
    public class TasaCambioData : AsyncRepositoryBase<TasaCambio, LafiseContext>, ITasaCambioData
    {
       /// <summary>
       /// Metodo para ver las tasas de cambios por pais que se encuentren activas
       /// </summary>
       /// <param name="idPais">Identificador del pais</param>
       /// <returns>Lista de tasa de cambio</returns>
        public async Task<ICollection<TasaCambioDto>> VerPorPaisActivo(Int64 idPais)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idPais", idPais);

            return await RunStoredProcedureAsync<ICollection<TasaCambioDto>, TasaCambioDto>("CargarTasaCambio", _parametros);
        }
    }
}
