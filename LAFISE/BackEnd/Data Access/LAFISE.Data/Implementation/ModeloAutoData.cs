﻿/*==========================================================================
Archivo:            ModeloAutoData
Descripción:        Administador de la data de modelo auto                     
Autor:              paola.munoz                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
#endregion


namespace LAFISE.Data
{
    public class ModeloAutoData : AsyncRepositoryBase<ModeloAuto, LafiseContext>, IModeloAutoData
    {
        /// <summary>
        /// Metodo para ver todas los modelos autos
        /// </summary>
        /// <returns>Lista modelos auto</returns>
        public async Task<ICollection<ModeloAutoDto>> VerTodos()
        {
            return await GetAllAsync<ModeloAutoDto>();
        }

        /// <summary>
        /// Metodo para ver todas los modelos autos por marca
        /// </summary>
        /// <param name="Marca">Id marca</param>
        /// <returns>Lista de modelos</returns>
        public async Task<ICollection<ModeloAutoDto>> VerModelosPorMarca(long Marca)
        {
            return await GetAllByAsync<ModeloAutoDto>(c => c.IdMarcaAuto == Marca);
        }

        /// <summary>
        /// Metodo que se encarga de ver  el modelo auto por id
        /// </summary>
        /// <returns>ModeloAutoDto</returns>
        public async Task<ModeloAutoDto> VerModeloAutoPorId(Int64 id)
        {
            return await GetByAsync<ModeloAutoDto>(c => c.Id == id);
        }

        /// <summary>
        /// Metodo que se encarga de ver  el modelo auto por id no asincrono
        /// </summary>
        /// <param name="id">Identificador del modelo auto</param>
        /// <returns>ModeloAutoDto</returns>
        public ModeloAutoDto VerModeloAutoPorIdNoAsincrono(Int64 id)
        {
            var query = (from ma in DataContext.ModeloAuto
                         where ma.Id == id
                         select new ModeloAutoDto
                         {
                             Id = ma.Id,
                             Modelo = ma.Modelo
                         }).FirstOrDefault();
            return query;
        }
    }
}
