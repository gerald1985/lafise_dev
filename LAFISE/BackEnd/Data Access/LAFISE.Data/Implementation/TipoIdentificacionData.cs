﻿/*==========================================================================
Archivo:            TipoIdentificacionData
Descripción:        Administrador de la data de la entidad tipo identificación                
Autor:              paola.munoz                          
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                          
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System;
#endregion


namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de TipoIdentificacionData
    /// </summary>
    public class TipoIdentificacionData : AsyncRepositoryBase<TipoIdentificacion, LafiseContext>, ITipoIdentificacionData
    {

        /// <summary>
        /// Método usado para cargar todos los tipos de identificacion
        /// </summary>
        /// <returns>Lista de tipoIdentificacion</returns>
        public async Task<ICollection<TipoIdentificacionDto>> VerTodos()
        {
            var query = (from ti in DataContext.TipoIdentificacion
                         orderby ti.Nombre
                         select new TipoIdentificacionDto
                         {
                             Id = ti.Id,
                             Nombre = ti.Nombre,
                             IdPais=ti.IdPais
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Método usado para cargar los tipos de identificacion teniendo en cuenta el identificador de país
        /// </summary>
        /// <param name="id">Identificador de país</param>
        /// <returns>Objeto tipoIdentificacion</returns>
        public async Task<ICollection<TipoIdentificacionDto>> VerTipoIdentificacionPorIdPais(Int64 idPais)
        {
            var query = (from ti in DataContext.TipoIdentificacion
                         orderby ti.Nombre
                         where ti.IdPais==idPais
                         select new TipoIdentificacionDto
                         {
                             Id = ti.Id,
                             Nombre = ti.Nombre,
                             IdPais = ti.IdPais
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Ver por identificador el tipo de identificación
        /// </summary>
        /// <param name="id">Identificador del tipo de identificación</param>
        /// <returns>TipoIdentificacion</returns>
        public TipoIdentificacionDto VerPorIdNoAsincrono(Int64 id)
        {
            var query = (from c in DataContext.TipoIdentificacion
                         where c.Id == id
                         select new TipoIdentificacionDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre
                         }).FirstOrDefault();
            return query;
        }


        /// <summary>
        /// Metodo que se encarga de consultar las mascaras de entrada por tipo de identificacion
        /// </summary>
        /// <param name="id">id de identificacion</param>
        /// <returns>Mascara de entrada</returns>
        public async Task<String> obtenerMascaraPorIdentificacion(Int64 id)
        {
            var query = (from c in DataContext.TipoIdentificacion
                         where c.Id == id
                         select c.Mascara).FirstOrDefaultAsync();
            return await query;
        }
    }
}
