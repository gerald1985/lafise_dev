/*==========================================================================
Archivo:            CasaComercialData
Descripción:        Administrador de la data de casa comercial         
Autor:              Juan.Hincapie                           
Fecha de creación:  09/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System;
using System.Collections.Generic;
using System.Linq;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using ARKIX.Data.EntityFramework.Core;
using System.Threading.Tasks;
using System.Collections;
using System.Data.Entity;
using LAFISE.Common.Backend;
using LAFISE.Common.Backend.Helpers;

#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Clase encargada de realizar el acceso a datos para las casas comerciales.
    /// </summary>
    public class CasaComercialData : AsyncRepositoryBase<CasaComercial, LafiseContext>, ICasaComercialData
    {

        /// <summary>
        /// Método encargado de crear las casas comerciales y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Entidad CasaComercial que se insertó.</returns>
        public async Task<CasaComercialDto> CrearCasaComercial(CasaComercialDto casacomercial)
        {
            casacomercial.FechaCreacion = DateTime.Now;
            casacomercial.Activo = true;
            var entidad = casacomercial.Convert();
            foreach (var item in casacomercial.listaModelos)
            {
                CasaComercialModeloAuto _CasaComercialModeloAuto = new CasaComercialModeloAuto();
                _CasaComercialModeloAuto.IdModeloAuto = item.IdModeloAuto;
                _CasaComercialModeloAuto.FechaCreacion = DateTime.Now;
                entidad.CasaComercialModeloAuto.Add(_CasaComercialModeloAuto);
            }

            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Entidad CasaComercial que se editó.</returns>
        public async Task<CasaComercialDto> EditarCasaComercial(CasaComercialDto casacomercial)
        {
            var eliminarDetalle = EliminarDetalleCasaComercial(casacomercial.Id);

            var entidadOriginal = await GetByAsync(c => c.Id == casacomercial.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.Nombre = casacomercial.Nombre;
                entidadOriginal.IdPais = casacomercial.IdPais;
                entidadOriginal.Nuevo = casacomercial.Nuevo;
                entidadOriginal.FechaCreacion = DateTime.Now;
                entidadOriginal.Usado = casacomercial.Usado;
                await SaveAsync();
            }

            foreach (var item in casacomercial.listaModelos)
            {
                CasaComercialModeloAuto _CasaComercialModeloAuto = new CasaComercialModeloAuto();
                _CasaComercialModeloAuto.Id = item.Id;
                _CasaComercialModeloAuto.IdCasaComercial = item.IdCasaComercial;
                _CasaComercialModeloAuto.IdModeloAuto = item.IdModeloAuto;
                _CasaComercialModeloAuto.FechaCreacion = DateTime.Now;
                entidadOriginal.CasaComercialModeloAuto.Add(_CasaComercialModeloAuto);
            }
            await SaveAsync();
            return casacomercial;
        }

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial.
        /// </summary>
        /// <param name="idCasaComercial">Identificador de la casa comercial.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<bool> EliminarDetalleCasaComercial(long idCasaComercial)
        {
            LafiseContext contexto = new LafiseContext();
            var entidad = await contexto.CasaComercialModeloAuto.Where(s => s.IdCasaComercial == idCasaComercial).ToListAsync();
            contexto.CasaComercialModeloAuto.RemoveRange(entidad);
            await contexto.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Método encargado de consultar todas las casas comerciales.
        /// </summary>
        /// <returns>Lista con todas las casas comerciales.</returns>
        public async Task<ICollection<CasaComercialDto>> ConsultarTodosGrid()
        {
            return await GetAllAsync<CasaComercialDto>();
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>Entidad CasaComercial.</returns>
        public async Task<CasaComercialDto> VerCasaComercialPorId(long id)
        {
            return await GetByAsync<CasaComercialDto>(c => c.Id == id);
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>Entidad CasaComercial.</returns>
        public CasaComercialDto VerCasaComercialPorIdNoAsincrono(long id)
        {
            var query = (from cc in DataContext.CasaComercial
                         where cc.Id == id
                         select new CasaComercialDto
                         {
                             Id = cc.Id,
                             Nombre = cc.Nombre
                         }).FirstOrDefault();
            return query;
        }

        /// <summary>
        /// Metodo que valida que el nombre de la casa comercial sea unico.
        /// </summary>
        /// <param name="casacomercial">entidad casa comercial</param>
        /// <returns>casacomercial</returns>
        public async Task<CasaComercialDto> ValidarCasaComercialPorNombre(CasaComercialDto casacomercial)
        {
            var entidad = await GetByAsync(c => c.Nombre == casacomercial.Nombre && c.Id != casacomercial.Id);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Método encargado de eliminar las casas comerciales según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la casa comercial.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar</returns>
        public async Task<bool> EliminarCasaComercialPorId(long id)
        {
            var queryDetalle = DataContext.CasaComercialModeloAuto.Where(s => s.IdCasaComercial == id).ToList();
            var eliminarDetalle = DataContext.CasaComercialModeloAuto.RemoveRange(queryDetalle);
            var queryCasaComercial = DataContext.CasaComercial.First(s => s.Id == id);
            var eliminarCasaComercial = DataContext.CasaComercial.Remove(queryCasaComercial);
            await SaveAsync();
            return true;
        }

        /// <summary>
        /// Método encargado de consultar las casas comerciales según los filtros de búsqueda
        /// </summary>
        /// <param name="casaComercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Lista con todas las casas comerciales.</returns>
        public async Task<ICollection<CasaComercialFiltroDto>> ConsultarCasaComercialFiltros(CasaComercialDto casaComercial)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@Paises", casaComercial.Paises);
            _parametros.Add("@nombreCasaComercial", casaComercial.Nombre);
            _parametros.Add("@marca", casaComercial.Marca);

            return await RunStoredProcedureAsync<ICollection<CasaComercialFiltroDto>, CasaComercialFiltroDto>("dbo.CargarCasaComercialPorFiltros", _parametros);
        }

        /// <summary>
        /// Método encargado de consultar los modelos según los filtros de las marcas
        /// </summary>
        /// <param name="marcas">id de las marcas separadas por comas</param>
        /// <returns>lista de modelos agrupados por marca</returns>
        public async Task<ICollection<CasaComercialModeloMarcaFiltroDto>> ConsultarCasaComercialModeloMarcaFiltros(string marcas)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@marcas", marcas);
            return await RunStoredProcedureAsync<ICollection<CasaComercialModeloMarcaFiltroDto>, CasaComercialModeloMarcaFiltroDto>("dbo.CargarModelosPorMarcas", _parametros);
        }

        /// <summary>
        /// Método encargado de inactivar las casas comerciales.
        /// </summary>
        /// <param name="casaComercialDto">Dto con las propiedades de la casa comercial.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        public async Task<CasaComercialDto> InactivarCasaComercial(CasaComercialDto casaComercialDto)
        {
            var entidad = await GetByAsync(c => c.Id == casaComercialDto.Id);
            if (entidad != null)
            {
                entidad.Activo = casaComercialDto.Activo;
            }
            await SaveAsync();
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Método encargado de consultar el detalle de la casa comercial.
        /// </summary>
        /// <param name="idCasaComercial">Identificador de la casa comercial.</param>
        /// <returns>detalle de las casas comerciales</returns>
        public async Task<CasaComercialDto> ConsultarDetalleCasaComercial(long idCasaComercial)
        {
            IParametrosData parametros = new ParametrosData();
            var ruta = parametros.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_MODELOS);
            var rutaThumb = parametros.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_MODELOS_THUMB);
            string path = ruta != null ? ruta.Result : string.Empty;
            string pathThumb = rutaThumb != null ? rutaThumb.Result : string.Empty;
            var query = (from cc in DataContext.CasaComercial
                         where cc.Id == idCasaComercial
                         select new CasaComercialDto
                         {
                             Id = cc.Id,
                             Nombre = cc.Nombre,
                             IdPais = cc.IdPais,
                             Nuevo = cc.Nuevo,
                             FechaCreacion = cc.FechaCreacion,
                             Usado = cc.Usado,
                             Activo = cc.Activo,
                             listaMarcas = (from ccma in DataContext.CasaComercialModeloAuto
                                            join mod in DataContext.ModeloAuto
                                            on ccma.IdModeloAuto equals mod.Id
                                            join marca in DataContext.MarcaAuto
                                            on mod.IdMarcaAuto equals marca.Id
                                            where ccma.IdCasaComercial == cc.Id
                                            orderby marca.Marca
                                            select new MarcaAutoDto
                                            {
                                                Id = marca.Id,
                                                Marca = marca.Marca,
                                                listaModelos = (from modelo in DataContext.ModeloAuto
                                                                where modelo.IdMarcaAuto == marca.Id && modelo.Activo
                                                                orderby modelo.Modelo
                                                                select new ModeloAutoDto
                                                                {
                                                                    Id = modelo.Id,
                                                                    IdMarcaAuto = modelo.IdMarcaAuto,
                                                                    NombreMarca = marca.Marca,
                                                                    Modelo = modelo.Modelo,
                                                                    Imagen = modelo.Imagen,
                                                                    RutaImagen = path + modelo.Imagen,
                                                                    RutaThumb = pathThumb + modelo.Imagen,
                                                                    RelacionCasaComercial = (from relMod in DataContext.CasaComercialModeloAuto
                                                                                             where modelo.Id == relMod.IdModeloAuto && relMod.IdCasaComercial == cc.Id
                                                                                             select relMod).Any() ? true : false
                                                                }).ToList()
                                            }).ToList()

                         }).FirstOrDefaultAsync();

            var listaFiltrada = query.Result.listaMarcas.DistinctBy(s => new { s.Id, s.Marca }).ToList();
            query.Result.listaMarcas = listaFiltrada;
            return await query;
        }

        /// <summary>
        /// Método encargado de consultar las casas comerciales según el pais.
        /// </summary>
        /// <param name="idPais">Identificador del pais asociado a la casa comercial.</param>
        /// <returns>Lista de las casas comerciales.</returns>
        public async Task<ICollection<CasaComercialDto>> ConsultarCasaComercialPorPais(long idPais)
        {
            return await GetAllByAsync<CasaComercialDto>(c => c.IdPais == idPais); ;
        }

        /// <summary>
        /// Método que identifica si hay una casa comercial asociada a un plan financiero
        /// </summary>
        /// <param name="idCasaComercial">Identificador de la casa comercial</param>
        /// <returns>bool</returns>
        public async Task<bool> ExistePlanFinanciacionPorCasaComercial(long idCasaComercial)
        {
            LafiseContext context = new LafiseContext();
            var query = await context.PlanFinanciacion.Where(s => s.IdCasaComercial == idCasaComercial).FirstOrDefaultAsync();
            return query != null ? true : false;
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="Modelo">Identificador del modelo.</param>
        /// <returns>Entidad CasaComercial.</returns>
        public async Task<ICollection<CasaComercialDto>> VerCasaComercialPorModelo(long Modelo)
        {
            var query = from c in DataContext.CasaComercial
                        join cd in DataContext.CasaComercialModeloAuto
                        on c.Id equals cd.IdCasaComercial
                        where cd.IdModeloAuto == Modelo
                        select new CasaComercialDto
                        {
                            Id = c.Id,
                            Nombre = c.Nombre
                        };
            return await query.ToListAsync();
        }


        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="entidad">entidad CasaComercialModeloPaisDto</param>
        /// <returns></returns>
        public async Task<ICollection<CasaComercialModeloPaisDto>> ConsultarCasaComercialModeloPais(CasaComercialModeloPaisDto entidad)
        {
            var query = from c in DataContext.CasaComercial
                        join cd in DataContext.CasaComercialModeloAuto
                        on c.Id equals cd.IdCasaComercial
                        where cd.IdModeloAuto == entidad.IdModelo && c.IdPais == entidad.IdPais
                        select new CasaComercialModeloPaisDto
                        {
                            Id = c.Id,
                            Nombre = c.Nombre
                        };
            return await query.ToListAsync();
        }

    }
}