﻿/*==========================================================================
Archivo:            CiudadData
Descripción:        Administrador de la data de ciudad                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                          
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Clase encargada de realizar el acceso a datos para ciudad
    /// </summary>
    public class CiudadData : AsyncRepositoryBase<Ciudad, LafiseContext>, ICiudadData
    {

        /// <summary>
        /// Método usado para cargar todas las ciudades
        /// </summary>
        /// <returns>Lista de ciudades</returns>
        public async Task<ICollection<CiudadDto>> VerTodos()
        {
            var query = (from c in DataContext.Ciudad
                         orderby c.Nombre
                         select new CiudadDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre,
                             IdDepartamento=c.IdDepartamento
                         }).ToListAsync();

            return await query;

        }

        /// <summary>
        /// Método usado para cargar una ciudad por su id
        /// </summary>
        /// <param name="id">Identificador de la ciudad</param>
        /// <returns>Ciudad</returns>
        public async Task<CiudadDto> VerCiudadPorId(int id)
        {
            return await GetByAsync<CiudadDto>(c => c.Id == id);
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades asociados a un departamento
        /// </summary>
        /// <param name="idDepartamento">Identificador del departamento</param>
        /// <returns>Lista de ciudades</returns>
        public async Task<ICollection<CiudadDto>> VerCiudadesPorDepartamento(int idDepartamento)
        {
            var query = (from c in DataContext.Ciudad
                         orderby c.Nombre
                         where c.IdDepartamento == idDepartamento
                         select new CiudadDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre,
                             IdDepartamento = c.IdDepartamento
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades asociados a un país
        /// </summary>
        /// <param name="listaPais">lista con el identificador del pais</param>
        /// <returns>Lista de ciudades</returns>
        public async Task<ICollection<CiudadDto>> VerCiudadesPorPais(List<Int64> listaPais)
        {
            var query = (from c in DataContext.Ciudad
                         join d  in DataContext.Departamento on c.IdDepartamento equals d.Id
                         orderby c.Nombre
                         where listaPais.Contains(d.IdPais)
                         select new CiudadDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre,
                             IdDepartamento = c.IdDepartamento
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades asociados a un departamento
        /// </summary>
        /// <param name="listaDepartamento">lista con el identificador del departamento</param>
        /// <returns>Lista de ciudades</returns>
        public async Task<ICollection<CiudadDto>> VerListaCiudadesPorDepartamento(List<Int64> listaDepartamento)
        {
            var query = (from c in DataContext.Ciudad
                         orderby c.Nombre
                         where listaDepartamento.Contains(c.IdDepartamento)
                         select new CiudadDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre,
                             IdDepartamento = c.IdDepartamento
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Método usado para cargar todas las ciudades relacionadas con las listas no asincrono
        /// </summary>
        /// <param name="listaCiudad">lista con el identificador de las ciudades</param>
        /// <returns>Lista de ciudades</returns>
        public ICollection<CiudadDto> VerCiudadesPorCiudadNoAsincrono(List<Int64> listaCiudad)
        {
            var query = (from c in DataContext.Ciudad
                         orderby c.Nombre
                         where listaCiudad.Contains(c.Id)
                         select new CiudadDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre
                         }).ToList();

            return  query;
        }
    }
}
