﻿/*==========================================================================
Archivo:            TipoSucursalData
Descripción:        Administrador de la data de tipo de sucursal                    
Autor:              paola.munoz                       
Fecha de creación:  06/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Clase encargada de realizar el acceso a datos para  TipoSucursal
    /// </summary>
    public class TipoSucursalData : AsyncRepositoryBase<TipoSucursal, LafiseContext>, ITipoSucursalData
    {
        /// <summary>
        /// Ver todas los tipos de sucursal
        /// </summary>
        /// <returns>Lista de tipo de sucursal</returns>
        public async Task<ICollection<TipoSucursalDto>> VerTodosGrid()
        {
            return await GetAllAsync<TipoSucursalDto>();
        }
        /// <summary>
        /// Ver tipos de sucursal por identificador
        /// </summary>
        /// <param name="id">Identificador de tipo de sucursal</param>
        /// <returns>Tipo de sucursal</returns>
        public async Task<TipoSucursalDto> VerPorId(Int64 id)
        {
            return await GetByAsync<TipoSucursalDto>(c => c.Id == id);
        }
        /// <summary>
        /// Eliminar tipo de sucursal
        /// </summary>
        /// <param name="id">Identificador de tipo de sucursal</param>
        public async Task Eliminar(Int64 id)
        {
            await DeleteAsync(c => c.Id == id);
        }
        /// <summary>
        /// Crear tipo de sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo de sucursal</param>
        /// <returns>Tipo de sucursal</returns>
        public async Task<TipoSucursalDto> Crear(TipoSucursalDto tipoSucursal)
        {
            var entidad = tipoSucursal.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }
        /// <summary>
        /// Editar tipo de sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo de sucursal</param>
        /// <returns>Tipo de sucursal</returns>
        public async Task<TipoSucursalDto> Editar(TipoSucursalDto tipoSucursal)
        {
            var entidadOriginal = await GetByAsync(c => c.Id == tipoSucursal.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.Id = tipoSucursal.Id;
                entidadOriginal.TipoSucursal1 = tipoSucursal.TipoSucursal1;
                entidadOriginal.Activo = tipoSucursal.Activo;
                await SaveAsync();
            }
            return tipoSucursal;
        }


        /// <summary>
        /// Consulta de tipo de sucursales teniendo en cuenta los filtros
        /// </summary>
        /// <param name="tipoSucursal">Tipo de sucursal</param>
        /// <returns>Lista de tipo de sucursal</returns>
        public async Task<ICollection<TipoSucursalDto>> TipoSucursalFiltro(string tipoSucursal)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@TipoSucursal", tipoSucursal);

            return await RunStoredProcedureAsync<ICollection<TipoSucursalDto>, TipoSucursalDto>("CargarTipoSucursalFiltro", _parametros);

        }

        /// <summary>
        /// Consulta sucursales para realizar las validaciones
        /// </summary>
        /// <param name="tipoSucursal">Identificador del tipo de sucursal</param>
        /// <returns>Objeto de sucursalValidarDto</returns>
        public async Task<SucursalValidarDto> ValidarTipoSucursal(Int64 tipoSucursal)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@TipoSucursal", tipoSucursal);

            return await RunStoredProcedureAsync<SucursalValidarDto, SucursalValidarDto>("CargarSucursal", _parametros);

        }

        /// <summary>
        /// Método que permite inactivar o activar un tipo propiedad
        /// </summary>
        /// <param name="tipoSucursal">Tipo de sucursal</param>
        /// <returns>bool</returns>
        public async Task<bool> Inactivar(TipoSucursalDto tipoSucursal)
        {
            var entidad = await GetByAsync(c => c.Id == tipoSucursal.Id);
            if (entidad != null)
            {
                entidad.Activo = tipoSucursal.Activo;
            }
            await SaveAsync();
            return entidad.Activo;
        }

        /// <summary>
        /// Validar que el nombre del tipo sucursal sea unico.
        /// </summary>
        /// <param name="tipoSucursal">Entidad tipo sucursal.</param>
        /// <returns>Entidad con el tipo sucursal encontrada.</returns>
        public async Task<TipoSucursalDto> ValidarTipoSucursalUnica(TipoSucursalDto tipoSucursal)
        {
            var entidad = await GetByAsync(c => c.TipoSucursal1 == tipoSucursal.TipoSucursal1 && c.Id != tipoSucursal.Id);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Método usado para cargar todos los tipos sucursales activos
        /// </summary>
        /// <returns>Lista de tipo sucursal</returns>
        public async Task<ICollection<TipoSucursalDto>> VerTodosActivas()
        {
            var query = (from ts in DataContext.TipoSucursal
                         orderby ts.TipoSucursal1
                         where ts.Activo
                         select new TipoSucursalDto
                         {
                             Id = ts.Id,
                             TipoSucursal1= ts.TipoSucursal1,
                             Activo= ts.Activo

                         }).ToListAsync();

            return await query;

        }

    }
}
