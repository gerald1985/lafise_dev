﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using ARKIX.Data.EntityFramework.Core;
using System.Data.Entity;

namespace LAFISE.Data
{
    public class PROC_CISCO_PaisesData : AsyncRepositoryBase<PROC_CISCO_Paises, LafiseContext>, IPROC_CISCO_PaisesData
    {
        public async Task<ICollection<PROC_CISCO_PaisesDto>> VerTodos()
        {
            var query = (from p in DataContext.PROC_CISCO_Paises
                         orderby p.NombrePais
                         select new PROC_CISCO_PaisesDto
                         {
                             ABV = p.ABV,
                             NombrePais = p.NombrePais
                         }).ToListAsync();

            return await query;
        }
    }
}
