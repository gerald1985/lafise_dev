﻿/*==========================================================================
Archivo:            EstadoData
Descripción:        Administador de la data de Estado                     
Autor:              paola.munoz                         
Fecha de creación:  16/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de EstadoData
    /// </summary>
    public class EstadoData : AsyncRepositoryBase<Estado, LafiseContext>, IEstadoData
    {
        /// <summary>
        /// Metodo para ver los estados teniendo en cuenta el tipo de estado
        /// </summary>
        /// <param name="idTipoEstado">Identificador del tipo de estado</param>
        /// <returns>Lista estado</returns>
        public async Task<ICollection<EstadoDto>> VerEstadoIdTipo(Int64 idTipoEstado)
        {
            var query = (from e in DataContext.Estado
                         where e.IdTipoEstado==idTipoEstado
                         select new EstadoDto
                         {
                             Id = e.Id,
                             Nombre = e.Nombre,
                             IdTipoEstado= e.IdTipoEstado.Value,
                             TipoEstado=e.TipoEstado.Nombre
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Metodo para ver los estados teniendo en cuenta el tipo de estado
        /// </summary>
        /// <param name="id">Identificador del estado</param>
        /// <returns>Estado</returns>
        public EstadoDto VerEstadoPorId(Int64 id)
        {
            var query = (from e in DataContext.Estado
                         where e.Id == id
                         select new EstadoDto
                         {
                             Id = e.Id,
                             Nombre = e.Nombre,
                             IdTipoEstado = e.IdTipoEstado.Value,
                             TipoEstado = e.TipoEstado.Nombre
                         }).FirstOrDefault();

            return query;
        }
    }
}
