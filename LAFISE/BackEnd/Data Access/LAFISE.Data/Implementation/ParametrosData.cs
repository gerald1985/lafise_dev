﻿/*==========================================================================
Archivo:            ParametrosData
Descripción:        Administrador de la data de parametros                   
Autor:              paola.munoz                      
Fecha de creación:  28/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using AutoMapper;
#endregion

namespace LAFISE.Data
{
     /// <summary>
    ///  Clase encargada de realizar el acceso a datos para  Parametros
    /// </summary>
    public class ParametrosData : AsyncRepositoryBase<Parametro, LafiseContext>, IParametrosData
    {
        /// <summary>
        /// Método encargado de obtener la ruta de la imagen.
        /// </summary>
        /// <param name="rutaArchivos">Nombre de la ruta</param>
        /// <returns>La ruta solicitada</returns>
        public async Task<string> ObtenerParametroRuta(string rutaArchivos)
        {
            var ruta = DataContext.Parametro.FirstOrDefaultAsync(x => x.Nombre == rutaArchivos).Result.Valor;
            return ruta;
        }

        /// <summary>
        /// Método encargado de obtener la ruta no asincrono.
        /// </summary>
        /// <param name="info">Nombre de la información</param>
        /// <returns>Información solicitada</returns>
        public string ObtenerParametroRutaNoAsincrono(string info)
        {
            var informacion = DataContext.Parametro.FirstOrDefault(x => x.Nombre == info).Valor;
            return informacion;
        }

    }
}
