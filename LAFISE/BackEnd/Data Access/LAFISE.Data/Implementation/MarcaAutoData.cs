﻿/*==========================================================================
Archivo:            MarcaAutoData
Descripción:        Administrador de la data de la entidad MarcaAuto                      
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Collections;
using System.Data.Entity;
using System;
using AutoMapper;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Data
{


    /// <summary>
    /// Clase encargada de realizar el acceso a datos para las Marca de auto.
    /// </summary>
    public class MarcaAutoData : AsyncRepositoryBase<MarcaAuto, LafiseContext>, IMarcaAutoData
    {

        /// <summary>
        /// Instancia de la clase TarjetaData;
        /// </summary>
        IParametrosData _ParametrosData;

        /// <summary>
        /// Constructor de la clase
        /// </summary>	
        public MarcaAutoData()
        {
            _ParametrosData = new ParametrosData();

        }

        /// <summary>
        /// Método encargado de consultar las marca de auto según los filtros de búsqueda
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca de auto</param>
        /// <returns>Lista con todas las marcas de auto</returns>
        public async Task<ICollection<MarcaAutoFiltroDto>> VerMarcasModelos(MarcaAutoDto marcaauto)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@Marca", marcaauto.Marca);
            _parametros.Add("@Modelo", marcaauto.Modelo);
            var marcas = await RunStoredProcedureAsync<ICollection<MarcaAutoFiltroDto>, MarcaAutoFiltroDto>("dbo.CargarMarcaAutoFiltro", _parametros);
            return marcas;
        }




        /// <summary>
        /// Método encargado de consultar todas las marcas de auto.
        /// </summary>
        /// <returns>Lista con todas las marcas de auto.</returns>
        public async Task<ICollection<MarcaAutoDto>> VerTodos()
        {
            return await GetAllAsync<MarcaAutoDto>();
        }

        /// <summary>
        /// Método encargado de retornar una marca auto según su identificador.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>Entidad MarcaAuto.</returns>
        public async Task<MarcaAutoDto> VerMarcaAutoPorId(long id)
        {
            return await GetByAsync<MarcaAutoDto>(s => s.Id == id);
        }

        /// <summary>
        /// Método encargado de retornar una marca auto según su identificador no asincrono.
        /// </summary>
        /// <param name="id">Identificador de la marca auto</param>
        /// <returns>Entidad MarcaAuto.</returns>
        public MarcaAutoDto VerMarcaAutoPorIdNoAsincrono(long id)
        {
            var query = (from ma in DataContext.MarcaAuto
                         where ma.Id == id
                         select new MarcaAutoDto
                         {
                             Id = ma.Id,
                             Marca = ma.Marca
                         }).FirstOrDefault();
            return query;
        }

        /// <summary>
        /// Método encargado de crear las Marcas de Auto y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto</param>
        /// <returns>Entidad MarcaAuto que se insertó.</returns>
        public async Task<MarcaAutoDto> CrearMarcaAuto(MarcaAutoDto marcaauto)
        {
            marcaauto.FechaCreacion = DateTime.Now;
            var entidad = marcaauto.Convert();

            foreach (var item in marcaauto.listaModelos)
            {
                ModeloAuto _modeloAuto = new ModeloAuto();
                _modeloAuto.Id = item.Id;
                _modeloAuto.Modelo = item.Modelo;
                _modeloAuto.Imagen = item.Imagen;
                _modeloAuto.FechaCreacion = DateTime.Now;
                _modeloAuto.Activo = item.Activo;
                entidad.ModeloAuto.Add(_modeloAuto);
            }
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Método encargado de actualizar las Marcas de Auto y sus relaciones.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>Entidad MarcaAuto que se editó.</returns>
        public async Task<MarcaAutoDto> EditarMarcaAuto(MarcaAutoDto marcaauto)
        {
            var adicionados = marcaauto.listaModelos.Where(c => c.EstadoTransaccion == EstadoTransaccion.Add.ToString()).ToList();
            var editados = marcaauto.listaModelos.Where(c => c.EstadoTransaccion == EstadoTransaccion.Edit.ToString()).ToList();
            var eliminados = marcaauto.listaModelos.Where(c => c.EstadoTransaccion == EstadoTransaccion.Delete.ToString()).ToList();

            var entidad = await GetByAsync(c => c.Id == marcaauto.Id);
            if (entidad != null)
            {
                entidad.Id = marcaauto.Id;
                entidad.Marca = marcaauto.Marca;
                entidad.FechaCreacion = DateTime.Now;
                entidad.Activo = marcaauto.Activo;
            }

            foreach (var add in adicionados)
            {
                ModeloAuto _modeloAuto = new ModeloAuto();
                _modeloAuto.Id = add.Id;
                _modeloAuto.Modelo = add.Modelo;
                _modeloAuto.Imagen = add.Imagen;
                _modeloAuto.IdMarcaAuto = entidad.Id;
                _modeloAuto.FechaCreacion = DateTime.Now;
                _modeloAuto.Activo = add.Activo;
                entidad.ModeloAuto.Add(_modeloAuto);
            }

            foreach (var edit in editados)
            {
                var modelo = DataContext.ModeloAuto.FirstOrDefault(x => x.Id == edit.Id);
                if (modelo != null)
                {
                    modelo.Id = edit.Id;
                    modelo.Modelo = edit.Modelo;
                    modelo.Imagen = edit.Imagen;
                    modelo.IdMarcaAuto = entidad.Id;
                    modelo.FechaCreacion = DateTime.Now;
                    modelo.Activo = edit.Activo;
                }
            }

            foreach (var delete in eliminados)
            {
                await EliminarModeloAutoPorId(delete.Id);
            }

            await SaveAsync();
            return entidad.ConvertToDto();
        }



        /// <summary>
        /// Metodo usado para eliminar MarcaAuto y sus modelos asociados
        /// </summary>
        /// <param name="id">Identificador de la marca auto.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<bool> EliminarMarcaAutoPorId(long id)
        {
            var queryDetalle = DataContext.ModeloAuto.Where(s => s.IdMarcaAuto == id).ToList();
            var eliminarDetalle = DataContext.ModeloAuto.RemoveRange(queryDetalle);
            var queryMarcaAuto = DataContext.MarcaAuto.First(s => s.Id == id);
            var eliminarMarcaAuto = DataContext.MarcaAuto.Remove(queryMarcaAuto);
            await SaveAsync();
            return true;
        }



        /// <summary>
        /// Metodo usado para eliminar MarcaAuto y sus modelos asociados
        /// </summary>
        /// <param name="id">Identificador del modelo.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<bool> EliminarModeloAutoPorId(long id)
        {
            var queryModeloAuto = DataContext.ModeloAuto.Where(s => s.Id == id).FirstOrDefault();
            var eliminarModeloAuto = DataContext.ModeloAuto.Remove(queryModeloAuto);
            await SaveAsync();
            return true;
        }

        /// <summary>
        /// Método encargado de inactivar las Marcas de Auto.
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca auto.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        public async Task<bool> InactivarMarcaAuto(MarcaAutoDto marcaauto)
        {
            var entidad = await GetByAsync(s => s.Id == marcaauto.Id);
            if (entidad != null)
            {
                entidad.Activo = marcaauto.Activo;
            }
            await SaveAsync();
            return entidad.Activo;

        }

        /// <summary>
        /// Método encargado de inactivar los Modelos de Auto.
        /// </summary>
        /// <param name="modeloauto">Dto con las propiedades de el modelo auto.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        public async Task<bool> InactivarModeloAuto(ModeloAutoDto modeloauto)
        {
            var entidad = await GetByAsync<ModeloAuto>(s => s.Id == modeloauto.Id);
            if (entidad != null)
            {
                entidad.Activo = modeloauto.Activo;
            }
            await SaveAsync();
            return entidad.Activo;

        }

        /// <summary>
        /// Método encargado de consultar el detalle de la marca auto.
        /// </summary>
        /// <param name="idMarca">Identificador de la marca auto.</param>
        /// <returns>Lista con el detalle de las Marcas de Auto.</returns>
        public async Task<ICollection<ModeloAutoDto>> ConsultarDetalleModelo(long idMarca)
        {
            var ruta = _ParametrosData.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_MODELOS);
            var rutaThumb = _ParametrosData.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_MODELOS_THUMB);
            LafiseContext context = new LafiseContext();
            var query = (from m in context.ModeloAuto
                         where m.IdMarcaAuto == idMarca
                         select new ModeloAutoDto
                         {
                             Id = m.Id,
                             Modelo = m.Modelo,
                             Imagen = m.Imagen,
                             IdMarcaAuto = m.IdMarcaAuto,
                             FechaCreacion = m.FechaCreacion,
                             Activo = m.Activo,
                             RutaThumb = rutaThumb.Result + m.Imagen,
                             RutaImagen = ruta.Result + m.Imagen
                         }).ToListAsync();
            return await query;

        }

        /// <summary>
        /// Método encargado de eliminar los detalles de la marca auto.
        /// </summary>
        /// <param name="idMarcaAuto">Identificador de la marca auto.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public bool EliminarDetalleModelo(long idMarcaAuto)
        {
            LafiseContext contexto = new LafiseContext();
            var entidad = contexto.ModeloAuto.Where(s => s.IdMarcaAuto == idMarcaAuto).ToList();
            contexto.ModeloAuto.RemoveRange(entidad);
            contexto.SaveChanges();
            return true;
        }

        /// <summary>
        /// Método que retorna las marcas activas
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<MarcaAutoDto>> ObtenerMarcasActivas()
        {
            LafiseContext contexto = new LafiseContext();
            var ruta = contexto.Parametro.FirstOrDefault(h => h.Nombre == Constants.RUTA_ARCHIVOS_MODELOS);
            string path = ruta != null ? ruta.Valor : string.Empty;
            var marcas = (from c in contexto.MarcaAuto
                          where c.Activo && c.Marca != Constants.MARCA_AUTO_OTRO
                          orderby c.Marca
                          select new MarcaAutoDto
                          {
                              Activo = c.Activo,
                              FechaCreacion = c.FechaCreacion,
                              Id = c.Id,
                              listaModelos = (from d in contexto.ModeloAuto
                                              where d.IdMarcaAuto == c.Id
                                              select new ModeloAutoDto
                                              {
                                                  Activo = c.Activo,
                                                  FechaCreacion = d.FechaCreacion,
                                                  Id = d.Id,
                                                  IdMarcaAuto = d.IdMarcaAuto,
                                                  Imagen = d.Imagen,
                                                  Modelo = d.Modelo,
                                                  NombreMarca = c.Marca,
                                                  RutaImagen = path
                                              }).ToList(),
                              Marca = c.Marca
                          }).ToListAsync();
            return await marcas;
        }


        /// <summary>
        /// Método que retorna las marcas activas
        /// </summary>
        /// <param name="idPais">identificador del pais</param>
        /// <returns></returns>
        public async Task<ICollection<MarcaAutoDto>> ObtenerMarcasActivasPorPais(Int64 idPais)
        {
            LafiseContext contexto = new LafiseContext();
            var ruta = contexto.Parametro.FirstOrDefault(h => h.Nombre == Constants.RUTA_ARCHIVOS_MODELOS);
            string path = ruta != null ? ruta.Valor : string.Empty;
            var marcas = (from c in contexto.MarcaAuto
                          join mod in contexto.ModeloAuto
                          on c.Id equals mod.IdMarcaAuto
                          join casMod in contexto.CasaComercialModeloAuto
                          on mod.Id equals casMod.IdModeloAuto
                          join casa in contexto.CasaComercial
                          on casMod.IdCasaComercial equals casa.Id
                          where c.Activo && c.Marca != Constants.MARCA_AUTO_OTRO && casa.IdPais == idPais
                          orderby c.Marca
                          select new MarcaAutoDto
                          {
                              Id = c.Id,
                              Marca = c.Marca
                          }).GroupBy(s => new { s.Marca })
                          .Select(s => s.FirstOrDefault())
                          .ToListAsync();

            return await marcas;
        }

        /// <summary>
        /// Consulta la validacion de MarcaAuto
        /// </summary>
        /// <param name="MarcaAuto">Identificador de MarcaAuto</param>
        /// <returns>Validar CasaComercial</returns>
        public async Task<CasaComercialValidarDto> ValidarCasaComercialMarca(long MarcaAuto)
        {
            try
            {
                Hashtable _parametros = new Hashtable();
                _parametros.Add("@Marca", MarcaAuto);

                return await RunStoredProcedureAsync<CasaComercialValidarDto, CasaComercialValidarDto>("CargarCasaComercialPorMarca", _parametros);
            }
            catch (Exception ex)
            {
                return new CasaComercialValidarDto { Nombre = ex.Message };
            }
        }

        /// <summary>
        /// Consulta la validacion de MarcaAuto
        /// </summary>
        /// <param name="ModeloAuto">Identificador de modelo</param>
        /// <returns>Validar CasaComercial</returns>
        public async Task<CasaComercialValidarDto> ValidarCasaComercialModelo(long ModeloAuto)
        {
            try
            {
                Hashtable _parametros = new Hashtable();
                _parametros.Add("@Modelo", ModeloAuto);

                return await RunStoredProcedureAsync<CasaComercialValidarDto, CasaComercialValidarDto>("CargarCasaComercialPorModelo", _parametros);
            }
            catch (Exception ex)
            {
                return new CasaComercialValidarDto { Nombre = ex.Message };
            }
        }


        /// <summary>
        /// Validación con la solicitud.
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns>Entidad de plan de financiación asociada.</returns>
        public async Task<PlanFinanciacionDto> validarRelacionPlanFinanciacion(long Id)
        {
            var entidad = (from c in DataContext.PlanFinanciacion where c.IdMarcaAuto == Id select c).FirstOrDefault();
            Mapper.CreateMap<PlanFinanciacion, PlanFinanciacionDto>();
            var planfinanciaciondto = Mapper.Map<PlanFinanciacion, PlanFinanciacionDto>(entidad);
            return planfinanciaciondto;
        }


        /// <summary>
        /// Validación con la solicitud
        /// </summary>
        /// <param name="id">Identificador modelo</param>
        /// <returns>Entidad de plan de financiación asociada.</returns>
        public async Task<PlanFinanciacionDto> validarRelacionPlanFinanciacionModelo(long Id)
        {
            var entidad = (from c in DataContext.PlanFinanciacion where c.IdModelo == Id select c).FirstOrDefault();
            Mapper.CreateMap<PlanFinanciacion, PlanFinanciacionDto>();
            var planfinanciaciondto = Mapper.Map<PlanFinanciacion, PlanFinanciacionDto>(entidad);
            return planfinanciaciondto;
        }




        /// <summary>
        /// Validar que el nombre de la marca sea unico.
        /// </summary>
        /// <param name="Nombre">Nombre marca</param>
        /// <returns>Entidad con la marca encontrada.</returns>
        public async Task<MarcaAutoDto> ValidarMarcaUnica(long Id, string Nombre)
        {
            var entidad = await GetByAsync(c => c.Marca == Nombre && c.Id != Id);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Validar que el nombre del modelo sea unico.
        /// </summary>
        /// <param name="Nombre">Nombre modelo</param>
        /// <returns>Entidad con la modelo encontrado.</returns>
        public async Task<ModeloAutoDto> ValidarModeloUnica(long Id, string Nombre)
        {
            var query = (DataContext.ModeloAuto.FirstOrDefaultAsync(x => x.Id == Id && x.Modelo == Nombre)).Result;
            return query.ConvertToDto();
        }

        /// <summary>
        /// Método usado para cargar todas las marcas según sus ids
        /// </summary>
        /// <param name="listaMarcas">lista con el identificador de la marcas</param>
        /// <returns>Lista de marcas</returns>
        public async Task<ICollection<MarcaAutoDto>> VerMarcasPorId(List<long> listaMarcas)
        {
            var query = (from mar in DataContext.MarcaAuto
                         orderby mar.Marca
                         where listaMarcas.Contains(mar.Id)
                         select new MarcaAutoDto
                         {
                             Id = mar.Id,
                             Marca = mar.Marca
                         }).ToListAsync();

            return await query;
        }
    }
}
