﻿/*==========================================================================
Archivo:            SucursalData
Descripción:        Administrador de la data de sucursal
Autor:              paola.munoz                         
Fecha de creación:  30/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Data.EntityFramework.Core;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
#endregion
namespace LAFISE.Data
{
    /// <summary>
    ///  Clase encargada de realizar el acceso a datos para  Sucursal
    /// </summary>
    public class SucursalData : AsyncRepositoryBase<Sucursal, LafiseContext>, ISucursalData
    {
        /// <summary>
        /// Ver todas las sucursales
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<SucursalDto>> VerTodosGrid()
        {
            return await GetAllAsync<SucursalDto>();
        }

        /// <summary>
        /// Ver todas las sucursales por país
        /// </summary>
        /// <param name="idPais"></param>
        /// <returns></returns>
        public async Task<ICollection<SucursalDto>> VerTodosPorPais(long idPais)
        {
            return await GetAllByAsync<SucursalDto>(c => c.IdPais == idPais);
        }

        /// <summary>
        /// Ver por identificador las sucursales
        /// </summary>
        /// <param name="id">Identificador de las sucursales</param>
        /// <returns>Sucursal</returns>
        public async Task<SucursalDto> VerPorId(Int64 id)
        {
            return await GetByAsync<SucursalDto>(c => c.Id == id);
        }

        /// <summary>
        /// Ver por identificador las sucursales no asincrono
        /// </summary>
        /// <param name="id">Identificador de las sucursales</param>
        /// <returns>Sucursal</returns>
        public SucursalDto VerPorIdNoAsincrono(Int64 id)
        {
            var query = (from c in DataContext.Sucursal
                         where c.Id == id
                         select new SucursalDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre
                         }).FirstOrDefault();
            return query;
        }

        /// <summary>
        /// Eliminar sucursal
        /// </summary>
        /// <param name="id">Identificador de la sucursal</param>
        public async Task Eliminar(Int64 id)
        {
            await DeleteAsync(c => c.Id == id);
        }
        /// <summary>
        /// Crear sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        public async Task<SucursalDto> Crear(SucursalDto sucursal)
        {
            var entidad = sucursal.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }
        /// <summary>
        /// Editar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        public async Task<SucursalDto> Editar(SucursalDto sucursal)
        {
            var entidadOriginal = await GetByAsync(c => c.Id == sucursal.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.Id = sucursal.Id;
                entidadOriginal.IdPais = sucursal.IdPais;
                entidadOriginal.IdDepartamento = sucursal.IdDepartamento;
                entidadOriginal.IdCiudad = sucursal.IdCiudad;
                entidadOriginal.IdTipoSucursal = sucursal.IdTipoSucursal;
                entidadOriginal.Nombre = sucursal.Nombre;
                entidadOriginal.Activo = sucursal.Activo;
                await SaveAsync();
            }
            return sucursal;
        }


        /// <summary>
        /// Método que permite inactivar o activar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>bool</returns>
        public async Task<bool> Inactivar(SucursalDto sucursal)
        {
            var entidad = await GetByAsync(c => c.Id == sucursal.Id);
            if (entidad != null)
            {
                entidad.Activo = sucursal.Activo;
            }
            await SaveAsync();
            return entidad.Activo;
        }

        /// <summary>
        /// Consulta de sucursales teniendo en cuenta los filtros
        /// </summary>
        /// <param name="sucursal">sucursal filtros</param>
        /// <returns>lista de sucursal filtro</returns>
        public async Task<ICollection<SucursalFiltroDto>> SucursalFiltro(SucursalFiltroDto sucursal)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@Pais", sucursal.NombrePais);
            _parametros.Add("@Departamento", sucursal.NombreDepartamento);
            _parametros.Add("@Ciudad", sucursal.NombreCiudad);
            _parametros.Add("@TipoSucursal", sucursal.TipoSucursal);
            _parametros.Add("@NombreSucursal", sucursal.NombreSucursal);

            return await RunStoredProcedureAsync<ICollection<SucursalFiltroDto>, SucursalFiltroDto>("CargarSucursalFiltro", _parametros);

        }

        /// <summary>
        /// Validar que el nombre de la sucursal sea único.
        /// </summary>
        /// <param name="sucursal">Entidad  sucursal.</param>
        /// <returns>Entidad con la sucursal encontrada.</returns>
        public async Task<SucursalDto> ValidarSucursalUnica(SucursalDto sucursal)
        {
            var entidad = await GetByAsync(c => c.Nombre == sucursal.Nombre && c.Id != sucursal.Id);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Método usado para cargar todos las sucursales activos
        /// </summary>
        /// <returns>Lista de sucursal</returns>
        public async Task<ICollection<SucursalDto>> VerTodosActivas()
        {
            var query = (from s in DataContext.Sucursal
                         orderby s.Nombre
                         where s.Activo
                         select new SucursalDto
                         {
                             Id = s.Id,
                             IdPais = s.IdPais,
                             IdDepartamento = s.IdDepartamento,
                             IdCiudad = s.IdCiudad,
                             IdTipoSucursal = s.IdTipoSucursal,
                             Nombre = s.Nombre,
                             Activo = s.Activo
                         }).ToListAsync();

            return await query;
        }

    }
}