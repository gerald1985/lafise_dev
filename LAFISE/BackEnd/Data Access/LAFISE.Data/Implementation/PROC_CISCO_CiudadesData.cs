﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using ARKIX.Data.EntityFramework.Core;
using System.Data.Entity;

namespace LAFISE.Data
{
    public class PROC_CISCO_CiudadesData : AsyncRepositoryBase<PROC_CISCO_Ciudades, LafiseContext>, IPROC_CISCO_CiudadesData
    {
        public async Task<ICollection<PROC_CISCO_CiudadesDto>> VerTodos()
        {
            var query = (from p in DataContext.PROC_CISCO_Ciudades
                         orderby p.Nombre_Ciudad
                         select new PROC_CISCO_CiudadesDto
                         {
                             Cod_Ciudad  = p.Cod_Ciudad,
                             Nombre_Ciudad = p.Nombre_Ciudad
                         }).ToListAsync();

            return await query;
        }
    }
}
