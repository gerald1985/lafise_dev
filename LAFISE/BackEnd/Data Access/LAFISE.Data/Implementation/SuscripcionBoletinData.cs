﻿/*==========================================================================
Archivo:            SuscripcionBoletinData
Descripción:        Administrador suscripción a boletines                      
Autor:              steven.echavarria                          
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de SuscripcionBoletinData
    /// </summary>
    public class SuscripcionBoletinData : AsyncRepositoryBase<SuscripcionBoletin, LafiseContext>, ISuscripcionBoletinData
    {
        /// <summary>
        /// Crear Suscripción a boletin
        /// </summary>
        /// <param name="suscripcion">SuscripcionBoletin</param>
        /// <returns>SuscripcionBoletin</returns>
        public async Task<SuscripcionBoletinDto> Crear(SuscripcionBoletinDto suscripcion)
        {
            var entidad = suscripcion.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Metodo que valida que el email ya esté registrado en las suscripciones
        /// </summary>
        /// <param name="suscripcion">entidad suscripcion</param>
        /// <returns>suscripcion</returns>
        public async Task<SuscripcionBoletinDto> ValidarSuscripcionPorEmail(SuscripcionBoletinDto suscripcion)
        {
            var entidad = await GetByAsync(c => c.Email == suscripcion.Email && c.Pais.Id == suscripcion.IdPais && c.Id != suscripcion.Id);
            return entidad.ConvertToDto();
        }
    }
}
