﻿/*==========================================================================
Archivo:            DepartamentoData
Descripción:        Administrar la data de departamento                      
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                          
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    ///  Clase encargada de realizar el acceso a datos para  DepartamentoData
    /// </summary>
    public class DepartamentoData : AsyncRepositoryBase<Departamento, LafiseContext>, IDepartamentoData
    {
        /// <summary>
        /// Método usado para cargar todos los departamentos
        /// </summary>
        /// <returns>Lista de departamentos</returns>
        public async Task<ICollection<DepartamentoDto>> VerTodos()
        {
            var query = (from d in DataContext.Departamento
                         orderby d.Nombre
                         select new DepartamentoDto
                         {
                             Id = d.Id,
                             Nombre = d.Nombre,
                             IdPais= d.IdPais
                         }).ToListAsync();

            return await query;

        }

        /// <summary>
        /// Método usado para cargar un departamento por su id
        /// </summary>
        /// <param name="id">Identificador del departamento</param>
        /// <returns>Departamento</returns>
        public async Task<DepartamentoDto> VerDepartamentoPorId(int id)
        {
            return await GetByAsync<DepartamentoDto>(c => c.Id == id);
        }

        /// <summary>
        /// Método usado para cargar todos los departamentos aociados a un pais
        /// </summary>
        /// <param name="idPais">Identificador del pais</param>
        /// <returns>Lista de departamentos</returns>
        public async Task<ICollection<DepartamentoDto>> VerDepartamentosPorPais(int idPais)
        {
            var query = (from d in DataContext.Departamento
                         orderby d.Nombre
                         where d.IdPais==idPais
                         select new DepartamentoDto
                         {
                             Id = d.Id,
                             Nombre = d.Nombre,
                             IdPais = d.IdPais
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Método usado para cargar todos los departamentos asociados a un pais
        /// </summary>
        /// <param name="listaPais">lista con el identificador del pais</param>
        /// <returns>Lista de departamento</returns>
        public async Task<ICollection<DepartamentoDto>> VerListaDepartamentoPorPais(List<Int64> listaPais)
        {
            var query = (from d in DataContext.Departamento
                         orderby d.Nombre
                         where listaPais.Contains(d.IdPais)
                         select new DepartamentoDto
                         {
                             Id = d.Id,
                             Nombre = d.Nombre,
                             IdPais= d.IdPais
                         }).ToListAsync();

            return await query;
        }
    }
}
