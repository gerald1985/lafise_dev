﻿/*==========================================================================
Archivo:            TipoBienData
Descripción:        Administrador de la data de tipo de bien                    
Autor:              juan.hincapie                       
Fecha de creación:  06/01/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Data
{
    public class TipoBienData : AsyncRepositoryBase<TipoBien, LafiseContext>, ITipoBienData
    {
        /// <summary>
        /// Ver todas los tipos de vehículo
        /// </summary>
        /// <returns>Lista de tipos de vehículo</returns>
        public async Task<ICollection<TipoBienDto>> VerTodosGrid()
        {
            return await GetAllAsync<TipoBienDto>();
        }
    }
}
