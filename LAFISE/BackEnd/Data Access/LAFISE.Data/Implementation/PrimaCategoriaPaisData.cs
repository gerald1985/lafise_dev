﻿/*==========================================================================
Archivo:            PrimaCategoriaPaisData
Descripción:        Administrador de la data de prima categoria pais                          
Autor:              steven.echavarria                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Clase encargada de realizar el acceso a datos para la consulta de porcentaje de prima por categoría y país.
    /// </summary>
    public class PrimaCategoriaPaisData : AsyncRepositoryBase<PrimaCategoriaPais, LafiseContext>, IPrimaCategoriaPaisData
    {
        /// <summary>
        /// Método encargado de consultar el porcentaje de prima por categoria y país
        /// </summary>
        /// <param name="primaCategoriaPais">entidad prima categoria pais</param>
        /// <returns>Entidad PrimaCategoriaPaisDto</returns>
        public PrimaCategoriaPaisDto VerPorcentajePrimaPorCategoriaPais(PrimaCategoriaPaisDto primaCategoriaPais)
        {
            var query = (from pp in DataContext.PrimaCategoriaPais
                         join c in DataContext.Categorias
                         on pp.IdCategoria equals c.Id
                         join p in DataContext.Pais
                         on pp.IdPais equals p.Id
                         where pp.IdCategoria == primaCategoriaPais.IdCategoria && pp.IdPais == primaCategoriaPais.IdPais
                         select new PrimaCategoriaPaisDto
                         {
                             Id = pp.Id,
                             IdCategoria = pp.IdCategoria,
                             IdPais = pp.IdPais,
                             PorcentajePrima = pp.PorcentajePrima
                         }).FirstOrDefault();
            return query;
        }
    }
}
