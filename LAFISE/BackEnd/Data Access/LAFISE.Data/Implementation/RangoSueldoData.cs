﻿/*==========================================================================
Archivo:            RangoSueldoData
Descripción:        Administrador de la data de rango sueldo                     
Autor:              steven.echavarria                          
Fecha de creación:  17/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Collections;
using System.Data.Entity;
using System;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de RangoSueldoData
    /// </summary>
    public class RangoSueldoData : AsyncRepositoryBase<RangoSueldo, LafiseContext>, IRangoSueldoData
    {
        /// <summary>
        /// Ver todas los rango sueldo 
        /// </summary>
        /// <returns>Objeto rango sueldo</returns>
        public async Task<ICollection<RangoSueldoDto>> VerTodos()
        {
            return await GetAllAsync<RangoSueldoDto>();
        }

        /// <summary>
        /// Ver rango sueldo por identificador
        /// </summary>
        /// <param name="id">identificador de rango sueldo</param>
        /// <returns>objeto rango sueldo</returns>
        public async Task<RangoSueldoDto> VerPorId(Int64 id)
        {
            return await GetByAsync<RangoSueldoDto>(c => c.Id == id);
        }

        /// <summary>
        /// Ver por el id el detalle del rango sueldo
        /// </summary>
        /// <param name="id">identificador del rango sueldo</param>
        /// <returns>Lista de detalle de rango sueldo</returns>
        public async Task<ICollection<RangoSueldoDto>> ConsultarDetalleRangoSueldo(long id)
        {
            var query = (from rs in DataContext.RangoSueldo
                         where rs.Id == id
                         select new RangoSueldoDto
                         {
                             Id = rs.Id,
                             IdPais = rs.IdPais,
                             IdMoneda = rs.IdMoneda,
                             RangoSueldoDetalle = (from rd in DataContext.RangoSueldoDetalle
                                                   where rd.IdRangoSueldo == id
                                                   select new RangoSueldoDetalleDto
                                                   {
                                                       Id = rd.Id,
                                                       IdRangoSueldo = rd.IdRangoSueldo,
                                                       MinSueldo = rd.MinSueldo,
                                                       MaxSueldo = rd.MaxSueldo
                                                   }).ToList(),

                         }).ToListAsync();
            return await query;

        }

        /// <summary>
        /// Editar rango sueldo
        /// </summary>
        /// <param name="rangoSueldo">Rango sueldo</param>
        /// <returns>Objeto de rango sueldo</returns>
        public async Task<RangoSueldoDto> Editar(RangoSueldoDto rangoSueldo)
        {

            var entidadDetalle = DataContext.RangoSueldoDetalle.Where(s => s.IdRangoSueldo == rangoSueldo.Id).ToList();
            DataContext.RangoSueldoDetalle.RemoveRange(entidadDetalle);

            var entidad = DataContext.RangoSueldo.Where(s => s.Id == rangoSueldo.Id).FirstOrDefault();
            if (entidad != null)
            {
                entidad.Id = rangoSueldo.Id;
                entidad.IdMoneda = rangoSueldo.IdMoneda;
                entidad.IdPais = rangoSueldo.IdPais;
            }

            foreach (var item in rangoSueldo.RangoSueldoDetalle)
            {
                RangoSueldoDetalle _rangoDetalle = new RangoSueldoDetalle();
                _rangoDetalle.MinSueldo = item.MinSueldo;
                _rangoDetalle.MaxSueldo = item.MaxSueldo;
                _rangoDetalle.IdRangoSueldo = entidad.Id;
                entidad.RangoSueldoDetalle.Add(_rangoDetalle);
            }

            await DataContext.SaveChangesAsync();
            return entidad.ConvertToDto();

        }


        /// <summary>
        /// Crear rango sueldo
        /// </summary>
        /// <param name="rangosueldo">identificador de rango sueldo</param>
        /// <returns>objeto de rango sueldo</returns>
        public async Task<RangoSueldoDto> Crear(RangoSueldoDto rangosueldo)
        {
            var entidad = rangosueldo.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }


        /// <summary>
        /// Metodo para retornar la información teniendo en cuenta la lista del identificador de paises
        /// </summary>
        /// <param name="pais">identificadores de país concatenados por coma</param>
        /// <returns>lista de rangosueldofiltro</returns>
        public async Task<ICollection<RangoSueldoFiltroDto>> VerRangoSueldoFiltro(string pais)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idPais", pais);

            return await RunStoredProcedureAsync<ICollection<RangoSueldoFiltroDto>, RangoSueldoFiltroDto>("CargarRangoSueldoFiltro", _parametros);
        }

        /// <summary>
        /// Metodo para retornar la información teniendo en cuenta la lista del identificador de paises
        /// </summary>
        /// <param name="pais">identificador de país </param>
        /// <returns>lista de RangoSueldoPaisDto</returns>
        public async Task<ICollection<RangoSueldoPaisDto>> VerRangoSueldoPais(long pais)
        {
            Hashtable _parametros = new Hashtable();
            _parametros.Add("@idPais", pais);

            return await RunStoredProcedureAsync<ICollection<RangoSueldoPaisDto>, RangoSueldoPaisDto>("CargarRangoSueldoPais", _parametros);
        }
    }
}
