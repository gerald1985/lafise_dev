﻿/*==========================================================================
Archivo:            ContactenosData
Descripción:        Administrador de data del formulario de contactenos                      
Autor:              paola.munoz                         
Fecha de creación:  13/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace LAFISE.Data
{
  
      /// <summary>
    ///  Clase encargada de realizar el acceso a datos para  de Tarjeta
    /// </summary>
    public class ContactenosData : AsyncRepositoryBase<Contacteno, LafiseContext>, IContactenosData
    {
        /// <summary>
        /// Ver todas las tarjetas
        /// </summary>
        /// <returns>Lista de tarjetas</returns>
        public async Task<ICollection<ContactenosDto>> VerTodosGrid()
        {
            return await GetAllAsync<ContactenosDto>();
        }
        /// <summary>
        /// Ver las tarjetas por el identificador
        /// </summary>
        /// <param name="id">Identificador de las tarjetas</param>
        /// <returns>Objeto tarjeta</returns>
        public async Task<ContactenosDto> VerPorId(Int64 id)
        {
            return await GetByAsync<ContactenosDto>(c => c.Id == id);
        }
        /// <summary>
        /// Borrar las tarjetas
        /// </summary>
        /// <param name="id">Identificador de la tarjeta</param>
        public async Task Eliminar(Int64 id)
        {
            await DeleteAsync(c => c.Id == id);
        }
        /// <summary>
        /// Crear las tarjetas
        /// </summary>
        /// <param name="contactanos">Objeto de contactenos</param>
        /// <returns>Objeto de contactenos</returns>
        public async Task<ContactenosDto> Crear(ContactenosDto contactanos)
        {
            var entidad = contactanos.Convert();
            entidad.FechaCreacion = DateTime.Now;
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }
        /// <summary>
        /// Editar contactenos
        /// </summary>
        /// <param name="contactenos">Objeto de contactenos</param>
        /// <returns>objeto de contactenos</returns>
        public async Task<ContactenosDto> Editar(ContactenosDto contactenos)
        {
            var entidadOriginal = await GetByAsync(c => c.Id == contactenos.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.Id = contactenos.Id;
                entidadOriginal.NombreCompleto = contactenos.NombreCompleto;
                entidadOriginal.EsCliente = contactenos.EsCliente;
                entidadOriginal.Identificacion = contactenos.Identificacion;
                entidadOriginal.Telefono = contactenos.Telefono;
                entidadOriginal.Email = contactenos.Email;
                entidadOriginal.Asunto = contactenos.Asunto;
                entidadOriginal.Mensaje = contactenos.Mensaje;
                entidadOriginal.IdPais = contactenos.IdPais;
                entidadOriginal.FechaCreacion = DateTime.Now;
                await SaveAsync();
            }
            return contactenos;
        }
    }

}
