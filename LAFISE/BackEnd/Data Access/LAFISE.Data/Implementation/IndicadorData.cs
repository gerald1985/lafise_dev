﻿/*==========================================================================
Archivo:            IIndicadorData
Descripción:        Interfaz de IndicadorData                   
Autor:              arley.lopez                  
Fecha de creación:  12/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Validation;
#endregion


namespace LAFISE.Data
{
    /// <summary>
    /// Clase encargada de realizar el acceso a datos para los indicadores.
    /// </summary>
    public class IndicadorData : AsyncRepositoryBase<Indicador, LafiseContext>, IIndicadorData
    {

        /// <summary>
        /// Método usado para cargar todos los indicadores activos
        /// </summary>
        /// <returns>Lista de Indicadores</returns>
        public ICollection<IndicadorDto> VerTodosActivos()
        {
            var query = (from s in DataContext.Indicadors
                         where s.Activo
                         select new IndicadorDto
                         {
                             Id = s.Id,
                             NombreIndicador = s.NombreIndicador,
                             SimboloActual = s.SimboloActual,
                             SimboloHistorico = s.SimboloHistorico,
                             Activo = s.Activo
                         }).ToList();
            return query;
        }

        /// <summary>
        /// Método usado para cargar todos los indicadores activos y no activos
        /// </summary>
        /// <returns>Lista de Indicadores</returns>
        public async Task<ICollection<IndicadorDto>> VerTodos()
        {
            return await GetAllAsync<IndicadorDto>();
        }

        /// <summary>
        /// Crear indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        public async Task<IndicadorDto> Crear(IndicadorDto indicador)
        {
                var entidad = indicador.Convert();
                await AddAsync(entidad);
                return entidad.ConvertToDto();
        }

        /// <summary>
        /// Editar indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        public async Task<IndicadorDto> Editar(IndicadorDto indicador)
        {
            var entidadOriginal = await GetByAsync(c => c.Id == indicador.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.Id = indicador.Id;
                entidadOriginal.NombreIndicador = indicador.NombreIndicador;
                entidadOriginal.SimboloActual = indicador.SimboloActual;
                entidadOriginal.SimboloHistorico = indicador.SimboloHistorico;
                entidadOriginal.Activo = indicador.Activo;
                await SaveAsync();
            }
            return indicador;
        }

        /// <summary>
        /// Método que permite inactivar o activar un indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>bool</returns>
        public async Task<bool> Inactivar(IndicadorDto indicador)
        {
            var entidad = await GetByAsync(c => c.Id == indicador.Id);
            if (entidad != null)
            {
                entidad.Activo = indicador.Activo;
            }
            await SaveAsync();
            return entidad.Activo;
        }

        /// <summary>
        /// Eliminar indicador
        /// </summary>
        /// <param name="id">Identificador del indicador</param>
        public async Task Eliminar(Int64 id)
        {
            await DeleteAsync(c => c.Id == id);
        }

        /// <summary>
        /// Validar que el nombre del indicador sea unico.
        /// </summary>
        /// <param name="indicador">Entidad indicador.</param>
        /// <returns>Entidad con el indicador encontrado.</returns>
        public async Task<IndicadorDto> ValidarIndicadorUnico(IndicadorDto indicador)
        {
            var entidad = await GetByAsync(c => c.NombreIndicador == indicador.NombreIndicador && c.Id != indicador.Id);
            return entidad.ConvertToDto();
        }
    }
}
