﻿/*==========================================================================
Archivo:            TipoPropiedadPlazoData
Descripción:        Administrador de la data de tipo propiedad plazo                  
Autor:              steven.echavarria                          
Fecha de creación:  18/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Clase encargada de realizar el acceso a datos para la consulta de plazo por tipo de propiedad
    /// </summary>
    public class TipoPropiedadPlazoData : AsyncRepositoryBase<TipoPropiedadPlazo, LafiseContext>, ITipoPropiedadPlazoData
    {
        /// <summary>
        /// Método encargado de consultar el plazo de un tipo de propiedad
        /// </summary>
        /// <param name="idTipoPropiedad">identificador del tipo de propiedad</param>
        /// <returns>Entidad TipoPropiedadPlazoDto</returns>
        public TipoPropiedadPlazoDto VerPlazoPorTipoPropiedad(long idTipoPropiedad)
        {
            var query = (from tpp in DataContext.TipoPropiedadPlazo
                         join tp in DataContext.TipoPropiedad
                         on tpp.IdTipoPropiedad equals tp.Id
                         where tpp.IdTipoPropiedad == idTipoPropiedad
                         select new TipoPropiedadPlazoDto
                         {
                             Id = tpp.Id,
                             IdTipoPropiedad = tpp.IdTipoPropiedad,
                             Plazo = tpp.Plazo
                         }).FirstOrDefault();
            return query;
        }
    }
}
