﻿/*==========================================================================
Archivo:            MarcaAutoPlazoData
Descripción:        Administrador de la data de Marca Auto plazo                  
Autor:              juan.hincapie
Fecha de creación:  23/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Data
{
    public class MarcaAutoPlazoData : AsyncRepositoryBase<MarcaAutoPlazo, LafiseContext>, IMarcaAutoPlazoData
    {
        /// <summary>
        /// Método encargado de consultar el plazo de una marca de auto
        /// </summary>
        /// <param name="idMarcaAuto">identificador de la marca</param>
        /// <returns>Entidad MarcaAutoPlazoDto</returns>
        public MarcaAutoPlazoDto VerPlazoPorMarcaAuto(long idMarcaAuto)
        {
            var query = (from map in DataContext.MarcaAutoPlazo
                         join ma in DataContext.MarcaAuto
                         on map.IdMarcaAuto equals ma.Id
                         where map.IdMarcaAuto == idMarcaAuto
                         select new MarcaAutoPlazoDto
                         {
                             Id = map.Id,
                             IdMarcaAuto = map.IdMarcaAuto,
                             Plazo = map.Plazo
                         }).FirstOrDefault();
            return query;
        }
    }
}
