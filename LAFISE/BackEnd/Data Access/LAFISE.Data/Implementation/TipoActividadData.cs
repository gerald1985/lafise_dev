﻿/*==========================================================================
Archivo:            TipoActividadData
Descripción:        Administrador de la data de tipo de Actividad                    
Autor:              juan.hincapie                       
Fecha de creación:  26/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

namespace LAFISE.Data
{
    /// <summary>
    ///  Administrador de la data de tipo de Actividad   
    /// </summary>
    public class TipoActividadData : AsyncRepositoryBase<TipoActividad, LafiseContext>, ITipoActividadData
    {
        /// <summary>
        /// Ver todas los tipos de Actividad
        /// </summary>
        /// <returns>Lista de tipos de actividad</returns>
        public async Task<ICollection<TipoActividadDto>> VerTodosGrid()
        {
            return await GetAllAsync<TipoActividadDto>();
        }
    }
}
