﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using ARKIX.Data.EntityFramework.Core;
using System.Data.Entity;

namespace LAFISE.Data
{
    public class PROC_CISCO_Centro_De_CostoData : AsyncRepositoryBase<PROC_CISCO_Centro_De_Costo, LafiseContext>, IPROC_CISCO_Centro_De_CostoData
    {
        public async Task<ICollection<PROC_CISCO_Centro_De_CostoDto>> VerTodos()
        {
            var query = (from p in DataContext.PROC_CISCO_Centro_De_Costo
                         orderby p.Nombre_Centro
                         select new PROC_CISCO_Centro_De_CostoDto
                         {
                             id_centro = p.id_centro,
                             Nombre_Centro = p.Nombre_Centro
                         }).ToListAsync();

            return await query;
        }
    }
}
