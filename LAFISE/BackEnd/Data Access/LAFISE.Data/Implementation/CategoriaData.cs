﻿/*==========================================================================
Archivo:            CategoriaData
Descripción:        Administador de la data de categoria                     
Autor:              steven.echavarria                          
Fecha de creación:  13/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks; 
using System.Linq;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de CategoriaData
    /// </summary>
    public class CategoriaData : AsyncRepositoryBase<Categoria, LafiseContext>, ICategoriaData
    {
        /// <summary>
        /// Metodo para ver todas las categorias
        /// </summary>
        /// <param name="idTipoSolicitud">Identificador del tipo de la solicitud</param>
        /// <returns>Lista categoria</returns>
        public async Task<ICollection<CategoriaDto>> VerTodosIdSolicitud(Int64 idTipoSolicitud)
        {
            return await GetAllByAsync<CategoriaDto>(c => c.IdTipoSolicitud==idTipoSolicitud);
        }

        /// <summary>
        /// Metodo que se encarga de ver  la categoria por id
        /// </summary>
        /// <returns>CategoriaDto</returns>
        public async Task<CategoriaDto> VerCategoriaPorId(Int64 id)
        {
            return await GetByAsync<CategoriaDto>(c => c.Id == id);
        }

        /// <summary>
        /// Metodo que se encarga de ver  la categoria por id
        /// </summary>
        /// <returns>CategoriaDto</returns>
        public CategoriaDto VerCategoriaPorIdNoAsincrona(Int64 id)
        {
            var query = (from c in DataContext.Categorias
                         where c.Id == id
                         select new CategoriaDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre
                         }).FirstOrDefault();
            return query;
        }
    }
}
