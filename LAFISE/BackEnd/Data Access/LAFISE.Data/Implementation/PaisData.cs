﻿/*==========================================================================
Archivo:            PaisData
Descripción:        Administrador de la data de la entidad pais                
Autor:              steven.echavarria                          
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                          
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de PaisData
    /// </summary>
    public class PaisData : AsyncRepositoryBase<Pais, LafiseContext>, IPaisData
    {

        /// <summary>
        /// Método usado para cargar todos los paises
        /// </summary>
        /// <returns>Lista de paises</returns>
        public async Task<ICollection<PaisDto>> VerTodos()
        {
            var query = (from p in DataContext.Pais
                         orderby p.Nombre
                         select new PaisDto
                         {
                             Id = p.Id,
                             Nombre = p.Nombre
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Método usado para cargar un pais por su id
        /// </summary>
        /// <param name="id">Identificador de país</param>
        /// <returns>Objeto país</returns>
        public async Task<PaisDto> VerPaisPorId(int id)
        {
            return await GetByAsync<PaisDto>(c => c.Id == id);
        }

        /// <summary>
        /// Metodo para cargar el identificador de un pais
        /// </summary>
        /// <param name="nombre">Nombre pais</param>
        /// <returns>Identificador de país</returns>
        public async Task<Int64> VerPaisPorNombre(string nombre)
        {
            var idPais = DataContext.Pais.FirstOrDefaultAsync(x => x.Nombre == nombre).Result.Id;
            return idPais;
        }


        /// <summary>
        /// Metodo para cargar el identificador de un pais no asincrono
        /// </summary>
        /// <param name="nombre">Nombre pais</param>
        /// <returns>Identificador de país</returns>
        public Int64 VerPaisPorNombreNoAsincrono(string nombre)
        {
            var idPais = DataContext.Pais.FirstOrDefault(x => x.Nombre == nombre).Id;
            return idPais;
        }

        /// <summary>
        /// Método usado para cargar todas los paises teniendo en cuenta la lista de paises no asincrono
        /// </summary>
        /// <param name="listaPais">lista con el identificador del pais</param>
        /// <returns>Lista de pais</returns>
        public ICollection<PaisDto> VerPaisPorListaIdNoAsincrono(List<Int64> listaPais)
        {
            var query = (from p in DataContext.Pais
                         orderby p.Nombre
                         where listaPais.Contains(p.Id)
                         select new PaisDto
                         {
                             Id = p.Id,
                             Nombre = p.Nombre
                         }).ToList();

            return  query;
        }
    }
}
