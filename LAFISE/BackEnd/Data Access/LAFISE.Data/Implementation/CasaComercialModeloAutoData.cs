﻿/*==========================================================================
Archivo:            CasaComercialModeloAutoData
Descripción:        Administrador de la data de casa comercial de modelo auto                   
Autor:              Juan Hincapie                         
Fecha de creación:  30/09/2015 11:07:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Data
{
    /// <summary>
    ///  Clase encargada de realizar el acceso a datos para el detalle de las casas comerciales. 
    /// </summary>
    public class CasaComercialModeloAutoData : AsyncRepositoryBase<CasaComercialModeloAuto, LafiseContext>, ICasaComercialModeloAutoData
    {
        /// <summary>
        /// Método encargado de consultar todas las casas comerciales.
        /// </summary>
        /// <returns>Lista con los detalle de las casas comerciales</returns>
        public async Task<ICollection<CasaComercialModeloAutoDto>> VerTodos()
        {
            return await GetAllAsync<CasaComercialModeloAutoDto>();
        }

        /// <summary>
        /// Método encargado de retornar el detalle de una casa comercial según su identificador.
        /// </summary>
        /// <param name="id">Identificador correspondiente a la entidad CasaComercialModeloAuto</param>
        /// <returns>Lista con todas los detalles de las casas comerciales. </returns>
        public async Task<CasaComercialModeloAutoDto> VerPorId(int id)
        {
            return await GetByAsync<CasaComercialModeloAutoDto>(c => c.Id == id);
        }

        /// <summary>
        /// Método encargado de retornar el detalle de una casa comercial según el identificador de la casa comercial.
        /// </summary>
        /// <param name="id">Identificador correspondiente a la entidad CasaComercial</param>
        /// <returns>Lista con todas los detalles de las casas comerciales.</returns>
        public async Task<CasaComercialModeloAutoDto> VerPorCasaComercial(int id)
        {
            return await GetByAsync<CasaComercialModeloAutoDto>(c => c.IdCasaComercial == id);
        }

        /// <summary>
        /// Método encargado de crear el detalle de las casas comerciales.
        /// </summary>
        /// <param name="casaComercialModeloAuto">Dto con las propiedades de CasaComercialModeloAutoDto.</param>
        /// <returns>Entidad CasaComercialModeloAutoDto que se insertó.</returns>
        public async Task<CasaComercialModeloAutoDto> Crear(CasaComercialModeloAutoDto casaComercialModeloAuto)
        {
            var entidad = casaComercialModeloAuto.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casaComercialModeloAuto">Dto con las propiedades de CasaComercialModeloAutoDto.</param>
        /// <returns>Entidad CasaComercial que se editó</returns>
        public async Task<CasaComercialModeloAutoDto> Editar(CasaComercialModeloAutoDto casaComercialModeloAuto)
        {
            var entidadOriginal = await GetByAsync(c => c.Id == casaComercialModeloAuto.Id);
            if (entidadOriginal != null)
            {
                entidadOriginal.IdCasaComercial = casaComercialModeloAuto.IdCasaComercial;
                entidadOriginal.IdModeloAuto = casaComercialModeloAuto.IdModeloAuto;
                entidadOriginal.FechaCreacion = casaComercialModeloAuto.FechaCreacion;
                await SaveAsync();
            }
            return casaComercialModeloAuto;
        }

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial.
        /// </summary>
        /// <param name="id">Identificador de la entidad CasaComercialModeloAutoDto</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<bool> Eliminar(int id)
        {
            await DeleteAsync(c => c.Id == id);
            return true;
        }

        /// <summary>
        /// Método encargado de eliminar los detalles de la casa comercial según su casa comercial
        /// </summary>
        /// <param name="id">Identificador de la casa comercial</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        public async Task<bool> EliminarPorCasaComercial(int id)
        {
            await DeleteAsync(c => c.IdCasaComercial == id);
            return true;
        }




    }
}
