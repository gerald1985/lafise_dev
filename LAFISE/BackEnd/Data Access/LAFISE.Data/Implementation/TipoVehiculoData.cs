﻿/*==========================================================================
Archivo:            TipoVehiculoData
Descripción:        Administrador de la data de tipo de vehículo                    
Autor:              juan.hincapie                       
Fecha de creación:  26/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Models;
using LAFISE.Entities.Dtos;

namespace LAFISE.Data
{
    public class TipoVehiculoData : AsyncRepositoryBase<TipoVehiculo, LafiseContext>, ITipoVehiculoData
    {
        /// <summary>
        /// Ver todas los tipos de vehículo
        /// </summary>
        /// <returns>Lista de tipos de vehículo</returns>
        public async Task<ICollection<TipoVehiculoDto>> VerTodosGrid()
        {
            return await GetAllAsync<TipoVehiculoDto>();
        }

        /// <summary>
        /// Ver por identificador el tipo de vehiculo
        /// </summary>
        /// <param name="id">Identificador del tipo de vehiculo</param>
        /// <returns>TipoVehiculoDto</returns>
        public TipoVehiculoDto VerPorIdNoAsincrono(Int64 id)
        {
            var query = (from c in DataContext.TipoVehiculo
                         where c.Id == id
                         select new TipoVehiculoDto
                         {
                             Id = c.Id,
                             Nombre = c.Nombre
                         }).FirstOrDefault();
            return query;
        }
    }
}
