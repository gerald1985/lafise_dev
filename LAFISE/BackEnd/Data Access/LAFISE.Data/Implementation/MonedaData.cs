﻿/*==========================================================================
Archivo:            MonedaData
Descripción:        Administrador de la data de la entidad moneda                
Autor:              steven.echavarria                          
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                          
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using LAFISE.Common.Backend;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de MonedaData
    /// </summary>
    public class MonedaData : AsyncRepositoryBase<Moneda, LafiseContext>, IMonedaData
    {
        /// <summary>
        /// Método usado para cargar todos las monedas
        /// </summary>
        /// <returns>Lista de monedas</returns>
        public async Task<ICollection<MonedaDto>> VerTodos()
        {
            var query = (from p in DataContext.Moneda
                         orderby p.Nombre
                         select new MonedaDto
                         {
                             Id = p.Id,
                             Nombre = p.Nombre,
                             Simbolo = p.Simbolo
                         }).ToListAsync();

            return await query;
        }

        /// <summary>
        /// Método usado para consultar la moneda teniendo en cuenta el país
        /// </summary>
        /// <param name="idPais">Identificador del país</param>
        /// <returns>Monedas</returns>
        public MonedaDto VerMonedaPais(long idPais)
        {
            var query = (from m in DataContext.Moneda
                         join rs in DataContext.RangoSueldo on m.Id equals rs.IdMoneda
                         where rs.IdPais==idPais
                         select new MonedaDto
                         {
                             Id = m.Id,
                             Nombre = m.Nombre,
                             Simbolo = m.Simbolo
                         }).FirstOrDefault();

            return query;
        }

        /// <summary>
        /// Método usado para cargar una moneda por su id
        /// </summary>
        /// <param name="id">Identificador de moneda</param>
        /// <returns>Objeto moneda</returns>
        public async Task<MonedaDto> VerMonedaPorId(int id)
        {
            return await GetByAsync<MonedaDto>(c => c.Id == id);
        }

        /// <summary>
        /// Obtener el simbolo de la moneda
        /// </summary>
        /// <param name="siglaPortal">sigla del portal</param>
        /// <returns></returns>
        public string ObtenerSimboloMoneda(string siglaPortal)
        {
            var moneda = from c in DataContext.PortalPais
                         join d in DataContext.RangoSueldo on c.IdPais equals d.IdPais
                         join e in DataContext.Moneda on d.IdMoneda equals e.Id
                         where c.Sigla == siglaPortal
                         select e.Simbolo;
            if (!moneda.Any())
            {
                return (from d in DataContext.RangoSueldo 
                        join e in DataContext.Moneda on d.IdMoneda equals e.Id
                        join p in DataContext.Pais on d.IdPais equals p.Id
                        where p.Nombre == Constants.PAIS_NO_ENCONTRADO
                        select e.Simbolo).FirstOrDefault();
            }
            return moneda.FirstOrDefault();
        }
    }
}
