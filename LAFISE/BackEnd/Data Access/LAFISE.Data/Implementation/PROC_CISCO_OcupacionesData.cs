﻿using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using ARKIX.Data.EntityFramework.Core;
using System.Data.Entity;

namespace LAFISE.Data
{
    public class PROC_CISCO_OcupacionesData : AsyncRepositoryBase<Pais, LafiseContext>, IPROC_CISCO_OcupacionesData
    {
        public async Task<ICollection<PROC_CISCO_OcupacionesDto>> VerTodos()
        {
            var query = (from p in DataContext.PROC_CISCO_Ocupaciones
                         orderby p.Nombre_Ocupacion
                         select new PROC_CISCO_OcupacionesDto
                         {
                             Cod_Ocupacion = p.Cod_Ocupacion,
                             Nombre_Ocupacion = p.Nombre_Ocupacion
                         }).ToListAsync();

            return await query;
        }
    }
}
