﻿#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de CotizadorData
    /// </summary>
    public class CotizadorData : AsyncRepositoryBase<Cotizador, LafiseContext>, ICotizadorData
    {
        /// <summary>
        /// Crear registro de Cotizador
        /// </summary>
        /// <param name="cotizador">Cotizador</param>
        /// <returns>Cotizador</returns>
        public async Task<CotizadorDto> Crear(CotizadorDto cotizador)
        {   
            var entidad = cotizador.Convert();
            await AddAsync(entidad);
            return entidad.ConvertToDto();
        }
    }
}
