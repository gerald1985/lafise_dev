﻿/*==========================================================================
Archivo:            GarantiaData
Descripción:        Administador de la data de garantia                     
Autor:              paola.munoz                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using ARKIX.Data.EntityFramework.Core;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Documentación de GarantiaData
    /// </summary>
    public  class GarantiaData : AsyncRepositoryBase<Garantia, LafiseContext>, IGarantiaData
    {

        /// <summary>
        /// Metodo que se encarga de ver todas las garantias
        /// </summary>
        /// <returns>lista Garantias</returns>
        public async Task<ICollection<GarantiaDto>> VerTodos()
        {
            return await GetAllAsync<GarantiaDto>();
        }

        /// <summary>
        /// Metodo que se encarga de ver  las garantias por id
        /// </summary>
        /// <returns>Garantias</returns>
        public async Task<GarantiaDto> VerGarantiaPorId(Int64 id)
        {
            return await GetByAsync<GarantiaDto>(c => c.Id == id);
        }


         /// <summary>
        /// Metodo que se encarga de ver  las garantias por id
        /// </summary>
        /// <returns>Garantias</returns>
        public GarantiaDto VerGarantiaPorIdNoAsincrono(Int64 id)
        {
            var query = (from g in DataContext.Garantias
                         where g.Id== id
                         select new GarantiaDto
                         {
                             Id = g.Id,
                             Nombre = g.Nombre
                         }).FirstOrDefault();
            return query;
        }
        
    }
}
