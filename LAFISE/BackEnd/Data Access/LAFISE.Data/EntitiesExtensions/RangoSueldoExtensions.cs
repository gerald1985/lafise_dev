/*==========================================================================
Archivo:            RangoSueldoExtensions
Descripción:        Extensión de  RangoSueldoData                      
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Extensión de  RangoSueldoData 
    /// </summary>
	public static class RangoSueldoExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static RangoSueldoExtensions()
        {
            Mapper.CreateMap<RangoSueldoDto, RangoSueldo>();
            Mapper.CreateMap<RangoSueldo, RangoSueldoDto>();
            Mapper.CreateMap<RangoSueldoDetalleDto, RangoSueldoDetalle>();
            Mapper.CreateMap<RangoSueldoDetalle, RangoSueldoDetalleDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="RangoSueldoDto"/> en un <see cref="RangoSueldo"/>
        /// </summary>
        /// <param name="rangosueldo">RangoSueldoDto</param>
        /// <returns>RangoSueldo</returns>
        public static RangoSueldo Convert(this RangoSueldoDto rangosueldo)
        {
            return Mapper.Map<RangoSueldoDto, RangoSueldo>(rangosueldo);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="RangoSueldoDto"/> a una de <see cref="RangoSueldo"/>
        /// </summary>
        /// <param name="listaRangoSueldo">Lista RangoSueldoDto</param>
        /// <returns>Lista RangoSueldo</returns>
        public static IEnumerable<RangoSueldo> Convert(this IEnumerable<RangoSueldoDto> listaRangoSueldo)
        {
            return Mapper.Map<IEnumerable<RangoSueldoDto>, IEnumerable<RangoSueldo>>(listaRangoSueldo);
        }

		/// <summary>
        /// Convierte un <see cref="RangoSueldo"/> en un <see cref="RangoSueldoDto"/>
        /// </summary>
        /// <param name="rangosueldo">Lista RangoSueldo</param>
        /// <returns>RangoSueldoDto</returns>
        public static RangoSueldoDto ConvertToDto(this RangoSueldo rangosueldo)
        {
            return Mapper.Map<RangoSueldo,RangoSueldoDto>(rangosueldo);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="RangoSueldo"/> a una de <see cref="RangoSueldoDto"/>
        /// </summary>
        /// <param name="listaRangoSueldo">Lista RangoSueldo</param>
        /// <returns>Lista RangoSueldoDto</returns>
        public static IEnumerable<RangoSueldoDto> ConvertToDto(this IEnumerable<RangoSueldo> listaRangoSueldo)
        {
            return Mapper.Map<IEnumerable<RangoSueldo>, IEnumerable<RangoSueldoDto>>(listaRangoSueldo);
        }

		#endregion
	}
}