﻿/*==========================================================================
Archivo:            TarjetaExtensions
Descripción:        Extensión de TarjetaData                   
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 11:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion


namespace LAFISE.Data
{
    /// <summary>
    /// Extensión de TarjetaData 
    /// </summary>
    public static class TarjetaExtensions
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static TarjetaExtensions()
        {
            Mapper.CreateMap<TarjetaDto, Tarjeta>();
            Mapper.CreateMap<Tarjeta, TarjetaDto>();
        }

        /// <summary>
        /// Convierte un <see cref="TarjetaDto"/> en un <see cref="Tarjeta"/>
        /// </summary>
        /// <param name="tarjeta">TarjetaDto</param>
        /// <returns>Tarjeta</returns>
        public static Tarjeta Convert(this TarjetaDto tarjeta)
        {
            return Mapper.Map<TarjetaDto, Tarjeta>(tarjeta);
        }

        /// <summary>
        /// Convierte una lista de <see cref="TarjetaDto"/> a una de <see cref="Tarjeta"/>
        /// </summary>
        /// <param name="listaTarjeta">Lista TarjetaDto</param>
        /// <returns>Lista Tarjeta</returns>
        public static IEnumerable<Tarjeta> Convert(this IEnumerable<TarjetaDto> listaTarjeta)
        {
            return Mapper.Map<IEnumerable<TarjetaDto>, IEnumerable<Tarjeta>>(listaTarjeta);
        }

        /// <summary>
        /// Convierte un <see cref="Tarjeta"/> en un <see cref="TarjetaDto"/>
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>TarjetaDto</returns>
        public static TarjetaDto ConvertToDto(this Tarjeta tarjeta)
        {
            return Mapper.Map<Tarjeta, TarjetaDto>(tarjeta);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="Tarjeta"/> a una de <see cref="TarjetaDto"/>
        /// </summary>
        /// <param name="listaTarjeta">Lista Tarjeta</param>
        /// <returns>Lista TarjetaDto</returns>
        public static IEnumerable<TarjetaDto> ConvertToDto(this IEnumerable<Tarjeta> listaTarjeta)
        {
            return Mapper.Map<IEnumerable<Tarjeta>, IEnumerable<TarjetaDto>>(listaTarjeta);
        }
    }
}
