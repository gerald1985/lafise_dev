/*==========================================================================
Archivo:            CasaComercialExtensions
Descripción:        Extensión de  CasaComercial                
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Extensión de  la clase CasaComercialData
    /// </summary>
	public static class CasaComercialExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static CasaComercialExtensions()
        {
            Mapper.CreateMap<CasaComercialDto, CasaComercial>();
            Mapper.CreateMap<CasaComercial, CasaComercialDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="CasaComercialDto"/> en un <see cref="CasaComercial"/>
        /// </summary>
        /// <param name="casacomercial">CasaComercialDto</param>
        /// <returns>CasaComercial</returns>
        public static CasaComercial Convert(this CasaComercialDto casacomercial)
        {
            return Mapper.Map<CasaComercialDto, CasaComercial>(casacomercial);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="CasaComercialDto"/> a una de <see cref="CasaComercial"/>
        /// </summary>
        /// <param name="listaCasaComercial">Lista de CasaComercialDto</param>
        /// <returns>Lista de CasaComercial</returns>
        public static IEnumerable<CasaComercial> Convert(this IEnumerable<CasaComercialDto> listaCasaComercial)
        {
            return Mapper.Map<IEnumerable<CasaComercialDto>, IEnumerable<CasaComercial>>(listaCasaComercial);
        }

		/// <summary>
        /// Convierte un <see cref="CasaComercial"/> en un <see cref="CasaComercialDto"/>
        /// </summary>
        /// <param name="casacomercial">CasaComercial</param>
        /// <returns>CasaComercialDto</returns>
        public static CasaComercialDto ConvertToDto(this CasaComercial casacomercial)
        {
            return Mapper.Map<CasaComercial,CasaComercialDto>(casacomercial);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="CasaComercial"/> a una de <see cref="CasaComercialDto"/>
        /// </summary>
        /// <param name="listaCasaComercial">Lista CasaComercial</param>
        /// <returns>Lista CasaComercialDto</returns>
        public static IEnumerable<CasaComercialDto> ConvertToDto(this IEnumerable<CasaComercial> listaCasaComercial)
        {
            return Mapper.Map<IEnumerable<CasaComercial>, IEnumerable<CasaComercialDto>>(listaCasaComercial);
        }

		#endregion
	}
}