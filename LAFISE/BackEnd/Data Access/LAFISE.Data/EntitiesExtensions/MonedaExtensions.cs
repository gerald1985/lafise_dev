﻿/*==========================================================================
Archivo:            MonedaExtensions
Descripción:        Extensión de MonedaData                
Autor:              steven.echavarria                          
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                          
==========================================================================*/

#region Referencias
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.Data.EntitiesExtensions
{
    public static class MonedaExtensions
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static MonedaExtensions()
        {
            Mapper.CreateMap<MonedaDto, Moneda>();
            Mapper.CreateMap<Moneda, MonedaDto>();
        }

        /// <summary>
        /// Convierte un <see cref="MonedaDto"/> en un <see cref="Moneda"/>
        /// </summary>
        /// <param name="moneda">MonedaDto</param>
        /// <returns>Moneda</returns>
        public static Moneda Convert(this MonedaDto moneda)
        {
            return Mapper.Map<MonedaDto, Moneda>(moneda);
        }

        /// <summary>
        /// Convierte una lista de <see cref="MonedaDto"/> a una de <see cref="Moneda"/>
        /// </summary>
        /// <param name="listaMoneda">Lista MonedaDto</param>
        /// <returns>Lista Moneda</returns>
        public static IEnumerable<Moneda> Convert(this IEnumerable<MonedaDto> listaMoneda)
        {
            return Mapper.Map<IEnumerable<MonedaDto>, IEnumerable<Moneda>>(listaMoneda);
        }

        /// <summary>
        /// Convierte un <see cref="Moneda"/> en un <see cref="MonedaDto"/>
        /// </summary>
        /// <param name="moneda">Moneda</param>
        /// <returns>MonedaDto</returns>
        public static MonedaDto ConvertToDto(this Moneda moneda)
        {
            return Mapper.Map<Moneda, MonedaDto>(moneda);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="Moneda"/> a una de <see cref="MonedaDto"/>
        /// </summary>
        /// <param name="listaMoneda">Lista Moneda</param>
        /// <returns>Lista MonedaDto</returns>
        public static IEnumerable<MonedaDto> ConvertToDto(this IEnumerable<Moneda> listaMoneda)
        {
            return Mapper.Map<IEnumerable<Moneda>, IEnumerable<MonedaDto>>(listaMoneda);
        }
    }
}
