﻿/*==========================================================================
Archivo:            SuscripcionBoletinExtensions
Descripción:        Extensión de SuscripcionBoletin                      
Autor:              steven.echavarria                          
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

#region Referencias
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;

#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Extensión de la clase SuscripcionBoletinData
    /// </summary>
    public static class SuscripcionBoletinExtensions
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static SuscripcionBoletinExtensions()
        {
            Mapper.CreateMap<SuscripcionBoletinDto, SuscripcionBoletin>();
            Mapper.CreateMap<SuscripcionBoletin, SuscripcionBoletinDto>();
        }

        /// <summary>
        /// Convierte un <see cref="SuscripcionBoletinDto"/> en un <see cref="SuscripcionBoletin"/>
        /// </summary>
        /// <param name="suscripcion">SuscripcionBoletinDto</param>
        /// <returns>suscripcion</returns>
        public static SuscripcionBoletin Convert(this SuscripcionBoletinDto suscripcion)
        {
            return Mapper.Map<SuscripcionBoletinDto, SuscripcionBoletin>(suscripcion);
        }

        /// <summary>
        /// Convierte una lista de <see cref="SuscripcionBoletinDto"/> a una de <see cref="SuscripcionBoletin"/>
        /// </summary>
        /// <param name="listasuscripcion">Lista de SuscripcionBoletinDto</param>
        /// <returns>Lista de suscripcion</returns>
        public static IEnumerable<SuscripcionBoletin> Convert(this IEnumerable<SuscripcionBoletinDto> listasuscripcion)
        {
            return Mapper.Map<IEnumerable<SuscripcionBoletinDto>, IEnumerable<SuscripcionBoletin>>(listasuscripcion);
        }

        /// <summary>
        /// Convierte un <see cref="SuscripcionBoletin"/> en un <see cref="SuscripcionBoletinDto"/>
        /// </summary>
        /// <param name="suscripcion">suscripcion</param>
        /// <returns>SuscripcionBoletinDto</returns>
        public static SuscripcionBoletinDto ConvertToDto(this SuscripcionBoletin suscripcion)
        {
            return Mapper.Map<SuscripcionBoletin, SuscripcionBoletinDto>(suscripcion);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="SuscripcionBoletin"/> a una de <see cref="SuscripcionBoletinDto"/>
        /// </summary>
        /// <param name="listasuscripcion">Lista suscripcion</param>
        /// <returns>Lista SuscripcionBoletinDto</returns>
        public static IEnumerable<SuscripcionBoletinDto> ConvertToDto(this IEnumerable<SuscripcionBoletin> listasuscripcion)
        {
            return Mapper.Map<IEnumerable<SuscripcionBoletin>, IEnumerable<SuscripcionBoletinDto>>(listasuscripcion);
        }
    }
}
