﻿
#region Referencias
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;
using System.Collections.Generic;

#endregion

namespace LAFISE.Data
{
    /// <summary>
    /// Extensión de la clase CotizadorData
    /// </summary>
    public static class CotizadorExtensions
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static CotizadorExtensions()
        {
            Mapper.CreateMap<CotizadorDto, Cotizador>();
            Mapper.CreateMap<Cotizador, CotizadorDto>();
        }

        /// <summary>
        /// Convierte un <see cref="CotizadorDto"/> en un <see cref="Cotizador"/>
        /// </summary>
        /// <param name="cotizador">CotizadorDto</param>
        /// <returns>cotizador</returns>
        public static Cotizador Convert(this CotizadorDto cotizador)
        {
            return Mapper.Map<CotizadorDto, Cotizador>(cotizador);
        }

     
        /// <summary>
        /// Convierte un <see cref="Cotizador"/> en un <see cref="CotizadorDto"/>
        /// </summary>
        /// <param name="cotizador">cotizador</param>
        /// <returns>CotizadorDto</returns>
        public static CotizadorDto ConvertToDto(this Cotizador cotizador)
        {
            return Mapper.Map<Cotizador, CotizadorDto>(cotizador);
        }

    }
}
