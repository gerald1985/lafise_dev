/*==========================================================================
Archivo:            MarcaAutoExtensions
Descripción:        (Descripción de MarcaAutoExtensions..........)                      
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Documentación de MarcaAutoExtensions
    /// </summary>
	public static class MarcaAutoExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static MarcaAutoExtensions()
        {
            Mapper.CreateMap<MarcaAutoDto, MarcaAuto>();
            Mapper.CreateMap<MarcaAuto, MarcaAutoDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="MarcaAutoDto"/> en un <see cref="MarcaAuto"/>
        /// </summary>
        /// <param name="marcaauto">MarcaAutoDto</param>
        /// <returns>MarcaAuto</returns>
        public static MarcaAuto Convert(this MarcaAutoDto marcaauto)
        {
            return Mapper.Map<MarcaAutoDto, MarcaAuto>(marcaauto);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="MarcaAutoDto"/> a una de <see cref="MarcaAuto"/>
        /// </summary>
        /// <param name="listaMarcaAuto">Lista MarcaAutoDto</param>
        /// <returns>Lista MarcaAuto</returns>
        public static IEnumerable<MarcaAuto> Convert(this IEnumerable<MarcaAutoDto> listaMarcaAuto)
        {
            return Mapper.Map<IEnumerable<MarcaAutoDto>, IEnumerable<MarcaAuto>>(listaMarcaAuto);
        }

		/// <summary>
        /// Convierte un <see cref="MarcaAuto"/> en un <see cref="MarcaAutoDto"/>
        /// </summary>
        /// <param name="marcaauto">MarcaAuto</param>
        /// <returns>MarcaAutoDto</returns>
        public static MarcaAutoDto ConvertToDto(this MarcaAuto marcaauto)
        {
            return Mapper.Map<MarcaAuto,MarcaAutoDto>(marcaauto);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="MarcaAuto"/> a una de <see cref="MarcaAutoDto"/>
        /// </summary>
        /// <param name="listaMarcaAuto">Lista MarcaAuto</param>
        /// <returns>Lista MarcaAutoDto</returns>
        public static IEnumerable<MarcaAutoDto> ConvertToDto(this IEnumerable<MarcaAuto> listaMarcaAuto)
        {
            return Mapper.Map<IEnumerable<MarcaAuto>, IEnumerable<MarcaAutoDto>>(listaMarcaAuto);
        }

		#endregion
	}
}