﻿/*==========================================================================
Archivo:            SolicitudExtensions
Descripción:        Extensión de SolicitudData                      
Autor:              paola.munoz                          
Fecha de creación:  13/10/2015 11:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion


namespace LAFISE.Data
{
    /// <summary>
    /// Extensión de SolicitudData 
    /// </summary>
    public static class SolicitudExtensions
    {
        /// <summary>
        /// Constructores
        /// </summary>
        static SolicitudExtensions()
        {
            Mapper.CreateMap<SolicitudDto, Solicitud>();
            Mapper.CreateMap<Solicitud, SolicitudDto>();
            Mapper.CreateMap<DatosPersonalesDto, DatosPersonales>();
            Mapper.CreateMap<DatosPersonales, DatosPersonalesDto>();
            Mapper.CreateMap<DatosFinancieroDto, DatosFinancieros>();
            Mapper.CreateMap<DatosFinancieros, DatosFinancieroDto>();
            Mapper.CreateMap<PlanFinanciacionDto, PlanFinanciacion>();
            Mapper.CreateMap<PlanFinanciacion, PlanFinanciacionDto>();
            Mapper.CreateMap<SoporteDto, Soportes>();
            Mapper.CreateMap<Soportes, SoporteDto>();
            Mapper.CreateMap<DatosBienDto, DatosBien>();
            Mapper.CreateMap<DatosBien, DatosBienDto>();
            Mapper.CreateMap<ReferenciaDto, Referencias>();
            Mapper.CreateMap<Referencias, ReferenciaDto>();
            Mapper.CreateMap<SiniestroDto, Siniestro>();
            Mapper.CreateMap<Siniestro, SiniestroDto>();
            Mapper.CreateMap<HistoricoComentarioDto, HistoricoComentario>();
            Mapper.CreateMap<HistoricoComentario, HistoricoComentarioDto>();
            Mapper.CreateMap<ActividadEconomicaDto, ActividadEconomica>();
            Mapper.CreateMap<ActividadEconomica, ActividadEconomicaDto>();
            Mapper.CreateMap<SiniestroCiudadDto, SiniestroCiudad>();
            Mapper.CreateMap<SiniestroCiudad, SiniestroCiudadDto>();
            Mapper.CreateMap<SiniestroPaisDto, SiniestroPais>();
            Mapper.CreateMap<SiniestroPais, SiniestroPaisDto>();

        }

        /// <summary>
        /// Convierte un <see cref="SolicitudDto"/> en un <see cref="Solicitud"/>
        /// </summary>
        /// <param name="solicitud">SolicitudDto</param>
        /// <returns>Solicitud</returns>
        public static Solicitud Convert(this SolicitudDto solicitud)
        {
            return Mapper.Map<SolicitudDto, Solicitud>(solicitud);
        }

        /// <summary>
        /// Convierte una lista de <see cref="SolicitudDto"/> a una de <see cref="Solicitud"/>
        /// </summary>
        /// <param name="listaSolicitud">Lista SolicitudDto</param>
        /// <returns>Lista Solicitud</returns>
        public static IEnumerable<Solicitud> Convert(this IEnumerable<SolicitudDto> listaSolicitud)
        {
            return Mapper.Map<IEnumerable<SolicitudDto>, IEnumerable<Solicitud>>(listaSolicitud);
        }

        /// <summary>
        /// Convierte un <see cref="Solicitud"/> en un <see cref="SolicitudDto"/>
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        /// <returns>SolicitudDto</returns>
        public static SolicitudDto ConvertToDto(this Solicitud solicitud)
        {
            return Mapper.Map<Solicitud, SolicitudDto>(solicitud);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="Solicitud"/> a una de <see cref="SolicitudDto"/>
        /// </summary>
        /// <param name="listaSolicitud">Lista Solicitud</param>
        /// <returns>Lista SolicitudDto</returns>
        public static IEnumerable<SolicitudDto> ConvertToDto(this IEnumerable<Solicitud> listaSolicitud)
        {
            return Mapper.Map<IEnumerable<Solicitud>, IEnumerable<SolicitudDto>>(listaSolicitud);
        }
    }
}
