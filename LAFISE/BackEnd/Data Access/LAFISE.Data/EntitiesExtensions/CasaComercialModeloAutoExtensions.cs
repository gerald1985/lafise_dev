/*==========================================================================
Archivo:            CasaComercialModeloAutoExtensions
Descripción:        Extensión CasaComercialModeloAuto                 
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Extensión CasaComercialModeloAuto
    /// </summary>
	public static class CasaComercialModeloAutoExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static CasaComercialModeloAutoExtensions()
        {
            Mapper.CreateMap<CasaComercialModeloAutoDto, CasaComercialModeloAuto>();
            Mapper.CreateMap<CasaComercialModeloAuto, CasaComercialModeloAutoDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="CasaComercialModeloAutoDto"/> en un <see cref="CasaComercialModeloAuto"/>
        /// </summary>
        /// <param name="casacomercialmodeloauto">CasaComercialModeloAutoDto</param>
        /// <returns>CasaComercialModeloAuto</returns>
        public static CasaComercialModeloAuto Convert(this CasaComercialModeloAutoDto casacomercialmodeloauto)
        {
            return Mapper.Map<CasaComercialModeloAutoDto, CasaComercialModeloAuto>(casacomercialmodeloauto);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="CasaComercialModeloAutoDto"/> a una de <see cref="CasaComercialModeloAuto"/>
        /// </summary>
        /// <param name="listaCasaComercialModeloAuto">Lista CasaComercialModeloAutoDto</param>
        /// <returns>Lista CasaComercialModeloAuto</returns>
        public static IEnumerable<CasaComercialModeloAuto> Convert(this IEnumerable<CasaComercialModeloAutoDto> listaCasaComercialModeloAuto)
        {
            return Mapper.Map<IEnumerable<CasaComercialModeloAutoDto>, IEnumerable<CasaComercialModeloAuto>>(listaCasaComercialModeloAuto);
        }

		/// <summary>
        /// Convierte un <see cref="CasaComercialModeloAuto"/> en un <see cref="CasaComercialModeloAutoDto"/>
        /// </summary>
        /// <param name="casacomercialmodeloauto">CasaComercialModeloAuto</param>
        /// <returns>CasaComercialModeloAutoDto</returns>
        public static CasaComercialModeloAutoDto ConvertToDto(this CasaComercialModeloAuto casacomercialmodeloauto)
        {
            return Mapper.Map<CasaComercialModeloAuto,CasaComercialModeloAutoDto>(casacomercialmodeloauto);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="CasaComercialModeloAuto"/> a una de <see cref="CasaComercialModeloAutoDto"/>
        /// </summary>
        /// <param name="listaCasaComercialModeloAuto">Lista CasaComercialModeloAuto</param>
        /// <returns>Lista CasaComercialModeloAutoDto</returns>
        public static IEnumerable<CasaComercialModeloAutoDto> ConvertToDto(this IEnumerable<CasaComercialModeloAuto> listaCasaComercialModeloAuto)
        {
            return Mapper.Map<IEnumerable<CasaComercialModeloAuto>, IEnumerable<CasaComercialModeloAutoDto>>(listaCasaComercialModeloAuto);
        }

		#endregion
	}
}