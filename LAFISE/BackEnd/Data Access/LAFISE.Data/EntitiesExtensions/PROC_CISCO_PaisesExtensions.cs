﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAFISE.Entities.Models;
using LAFISE.Entities.Dtos;
using AutoMapper;

namespace LAFISE.Data
{
    public static class PROC_CISCO_PaisesExtensions
    {
        static PROC_CISCO_PaisesExtensions()
        {
            Mapper.CreateMap<PROC_CISCO_PaisesDto, PROC_CISCO_Paises>();
            Mapper.CreateMap<Pais, PROC_CISCO_PaisesDto>();
        }

        /// <summary>
        /// Convierte un <see cref="PROC_CISCO_PaisesDto"/> en un <see cref="Pais"/>
        /// </summary>
        /// <param name="pais">PROC_CISCO_PaisesDto</param>
        /// <returns>Pais</returns>
        public static PROC_CISCO_Paises Convert(this PROC_CISCO_PaisesDto pais)
        {
            return Mapper.Map<PROC_CISCO_PaisesDto, PROC_CISCO_Paises>(pais);
        }

        /// <summary>
        /// Convierte una lista de <see cref="PROC_CISCO_PaisesDto"/> a una de <see cref="Pais"/>
        /// </summary>
        /// <param name="listaPais">Lista PROC_CISCO_PaisesDto</param>
        /// <returns>Lista PROC_CISCO_Paises</returns>
        public static IEnumerable<PROC_CISCO_Paises> Convert(this IEnumerable<PROC_CISCO_PaisesDto> listaPais)
        {
            return Mapper.Map<IEnumerable<PROC_CISCO_PaisesDto>, IEnumerable<PROC_CISCO_Paises>>(listaPais);
        }

        /// <summary>
        /// Convierte un <see cref="Pais"/> en un <see cref="PROC_CISCO_PaisesDto"/>
        /// </summary>
        /// <param name="pais">Pais</param>
        /// <returns>PROC_CISCO_PaisesDto</returns>
        public static PROC_CISCO_PaisesDto ConvertToDto(this PROC_CISCO_Paises pais)
        {
            return Mapper.Map<PROC_CISCO_Paises, PROC_CISCO_PaisesDto>(pais);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="Pais"/> a una de <see cref="PROC_CISCO_PaisesDto"/>
        /// </summary>
        /// <param name="listaPais">Lista Pais</param>
        /// <returns>Lista PROC_CISCO_PaisesDto</returns>
        public static IEnumerable<PROC_CISCO_PaisesDto> ConvertToDto(this IEnumerable<PROC_CISCO_Paises> listaPais)
        {
            return Mapper.Map<IEnumerable<PROC_CISCO_Paises>, IEnumerable<PROC_CISCO_PaisesDto>>(listaPais);
        }
    }
}
