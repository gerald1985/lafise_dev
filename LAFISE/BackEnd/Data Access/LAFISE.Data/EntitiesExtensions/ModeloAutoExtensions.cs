/*==========================================================================
Archivo:            ModeloAutoExtensions
Descripción:        Extensión de  ModeloAutoData                   
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Documentación de ModeloAutoExtensions
    /// </summary>
	public static class ModeloAutoExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static ModeloAutoExtensions()
        {
            Mapper.CreateMap<ModeloAutoDto, ModeloAuto>();
            Mapper.CreateMap<ModeloAuto, ModeloAutoDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="ModeloAutoDto"/> en un <see cref="ModeloAuto"/>
        /// </summary>
        /// <param name="modeloauto">ModeloAutoDto</param>
        /// <returns>Lista ModeloAuto</returns>
        public static ModeloAuto Convert(this ModeloAutoDto modeloauto)
        {
            return Mapper.Map<ModeloAutoDto, ModeloAuto>(modeloauto);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="ModeloAutoDto"/> a una de <see cref="ModeloAuto"/>
        /// </summary>
        /// <param name="listaModeloAuto">Lista ModeloAutoDto</param>
        /// <returns>Lista ModeloAuto</returns>
        public static IEnumerable<ModeloAuto> Convert(this IEnumerable<ModeloAutoDto> listaModeloAuto)
        {
            return Mapper.Map<IEnumerable<ModeloAutoDto>, IEnumerable<ModeloAuto>>(listaModeloAuto);
        }

		/// <summary>
        /// Convierte un <see cref="ModeloAuto"/> en un <see cref="ModeloAutoDto"/>
        /// </summary>
        /// <param name="modeloauto">Lista ModeloAuto</param>
        /// <returns>ModeloAutoDto</returns>
        public static ModeloAutoDto ConvertToDto(this ModeloAuto modeloauto)
        {
            return Mapper.Map<ModeloAuto,ModeloAutoDto>(modeloauto);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="ModeloAuto"/> a una de <see cref="ModeloAutoDto"/>
        /// </summary>
        /// <param name="listaModeloAuto">Lista ModeloAuto</param>
        /// <returns>Lista ModeloAutoDto</returns>
        public static IEnumerable<ModeloAutoDto> ConvertToDto(this IEnumerable<ModeloAuto> listaModeloAuto)
        {
            return Mapper.Map<IEnumerable<ModeloAuto>, IEnumerable<ModeloAutoDto>>(listaModeloAuto);
        }

		#endregion
	}
}