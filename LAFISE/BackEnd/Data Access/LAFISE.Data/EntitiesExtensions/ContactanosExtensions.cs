﻿
/*==========================================================================
Archivo:            ContactanosExtensions
Descripción:        Extensión de  Contactanos              
Autor:              paola.munoz                          
Fecha de creación:  13/10/2015 9:35:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion


namespace LAFISE.Data
{
    /// <summary>
    ///Extensión de  ContactanosData
    /// </summary>
    public static class ContactanosExtensions
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static ContactanosExtensions()
        {
            Mapper.CreateMap<ContactenosDto, Contacteno>();
            Mapper.CreateMap<Contacteno, ContactenosDto>();
        }

        /// <summary>
        /// Convierte un <see cref="ContactenosDto"/> en un <see cref="Contacteno"/>
        /// </summary>
        /// <param name="contactenos">ContactenosDto</param>
        /// <returns>Contacteno</returns>
        public static Contacteno Convert(this ContactenosDto contactenos)
        {
            return Mapper.Map<ContactenosDto, Contacteno>(contactenos);
        }

        /// <summary>
        /// Convierte una lista de <see cref="ContactenosDto"/> a una de <see cref="Contacteno"/>
        /// </summary>
        /// <param name="listaContactenos">Lista ContactenosDto</param>
        /// <returns>Lista Contacteno</returns>
        public static IEnumerable<Contacteno> Convert(this IEnumerable<ContactenosDto> listaContactenos)
        {
            return Mapper.Map<IEnumerable<ContactenosDto>, IEnumerable<Contacteno>>(listaContactenos);
        }

        /// <summary>
        /// Convierte un <see cref="Contacteno"/> en un <see cref="ContactenosDto"/>
        /// </summary>
        /// <param name="contactenos">Contacteno</param>
        /// <returns>ContactenosDto</returns>
        public static ContactenosDto ConvertToDto(this Contacteno contactenos)
        {
            return Mapper.Map<Contacteno, ContactenosDto>(contactenos);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="Contacteno"/> a una de <see cref="ContactenosDto"/>
        /// </summary>
        /// <param name="listaContacteno">Lista Contacteno</param>
        /// <returns>Lista ContactenosDto</returns>
        public static IEnumerable<ContactenosDto> ConvertToDto(this IEnumerable<Contacteno> listaContactenos)
        {
            return Mapper.Map<IEnumerable<Contacteno>, IEnumerable<ContactenosDto>>(listaContactenos);
        }
    }
}
