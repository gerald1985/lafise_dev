/*==========================================================================
Archivo:            UrbanizadoraTipoExtensions
Descripción:        Extensión de UrbanizadoraTipoData                    
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Extensión de UrbanizadoraTipoData 
    /// </summary>
	public static class UrbanizadoraTipoExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static UrbanizadoraTipoExtensions()
        {
            Mapper.CreateMap<UrbanizadoraTipoDto, UrbanizadoraTipo>();
            Mapper.CreateMap<UrbanizadoraTipo, UrbanizadoraTipoDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="UrbanizadoraTipoDto"/> en un <see cref="UrbanizadoraTipo"/>
        /// </summary>
        /// <param name="urbanizadoratipo">UrbanizadoraTipoDto</param>
        /// <returns>UrbanizadoraTipo</returns>
        public static UrbanizadoraTipo Convert(this UrbanizadoraTipoDto urbanizadoratipo)
        {
            return Mapper.Map<UrbanizadoraTipoDto, UrbanizadoraTipo>(urbanizadoratipo);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="UrbanizadoraTipoDto"/> a una de <see cref="UrbanizadoraTipo"/>
        /// </summary>
        /// <param name="listaUrbanizadoraTipo">Lista UrbanizadoraTipoDto</param>
        /// <returns>Lista UrbanizadoraTipo</returns>
        public static IEnumerable<UrbanizadoraTipo> Convert(this IEnumerable<UrbanizadoraTipoDto> listaUrbanizadoraTipo)
        {
            return Mapper.Map<IEnumerable<UrbanizadoraTipoDto>, IEnumerable<UrbanizadoraTipo>>(listaUrbanizadoraTipo);
        }

		/// <summary>
        /// Convierte un <see cref="UrbanizadoraTipo"/> en un <see cref="UrbanizadoraTipoDto"/>
        /// </summary>
        /// <param name="urbanizadoratipo">UrbanizadoraTipo</param>
        /// <returns>UrbanizadoraTipoDto</returns>
        public static UrbanizadoraTipoDto ConvertToDto(this UrbanizadoraTipo urbanizadoratipo)
        {
            return Mapper.Map<UrbanizadoraTipo,UrbanizadoraTipoDto>(urbanizadoratipo);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="UrbanizadoraTipo"/> a una de <see cref="UrbanizadoraTipoDto"/>
        /// </summary>
        /// <param name="listaUrbanizadoraTipo">Lista UrbanizadoraTipo</param>
        /// <returns>Lista UrbanizadoraTipoDto</returns>
        public static IEnumerable<UrbanizadoraTipoDto> ConvertToDto(this IEnumerable<UrbanizadoraTipo> listaUrbanizadoraTipo)
        {
            return Mapper.Map<IEnumerable<UrbanizadoraTipo>, IEnumerable<UrbanizadoraTipoDto>>(listaUrbanizadoraTipo);
        }

		#endregion
	}
}