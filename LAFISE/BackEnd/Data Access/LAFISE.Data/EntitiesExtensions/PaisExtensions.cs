/*==========================================================================
Archivo:            PaisExtensions
Descripción:        Extensión de PaisData                      
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Extensión de PaisData 
    /// </summary>
	public static class PaisExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static PaisExtensions()
        {
            Mapper.CreateMap<PaisDto, Pais>();
            Mapper.CreateMap<Pais, PaisDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="PaisDto"/> en un <see cref="Pais"/>
        /// </summary>
        /// <param name="pais">PaisDto</param>
        /// <returns>Pais</returns>
        public static Pais Convert(this PaisDto pais)
        {
            return Mapper.Map<PaisDto, Pais>(pais);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="PaisDto"/> a una de <see cref="Pais"/>
        /// </summary>
        /// <param name="listaPais">Lista PaisDto</param>
        /// <returns>Lista Pais</returns>
        public static IEnumerable<Pais> Convert(this IEnumerable<PaisDto> listaPais)
        {
            return Mapper.Map<IEnumerable<PaisDto>, IEnumerable<Pais>>(listaPais);
        }

		/// <summary>
        /// Convierte un <see cref="Pais"/> en un <see cref="PaisDto"/>
        /// </summary>
        /// <param name="pais">Pais</param>
        /// <returns>PaisDto</returns>
        public static PaisDto ConvertToDto(this Pais pais)
        {
            return Mapper.Map<Pais,PaisDto>(pais);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="Pais"/> a una de <see cref="PaisDto"/>
        /// </summary>
        /// <param name="listaPais">Lista Pais</param>
        /// <returns>Lista PaisDto</returns>
        public static IEnumerable<PaisDto> ConvertToDto(this IEnumerable<Pais> listaPais)
        {
            return Mapper.Map<IEnumerable<Pais>, IEnumerable<PaisDto>>(listaPais);
        }

		#endregion
	}
}