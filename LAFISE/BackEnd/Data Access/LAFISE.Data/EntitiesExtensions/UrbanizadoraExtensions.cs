/*==========================================================================
Archivo:            UrbanizadoraExtensions
Descripción:        Extensión de UrbanizadoraData                      
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Extensión de UrbanizadoraData
    /// </summary>
	public static class UrbanizadoraExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static UrbanizadoraExtensions()
        {
            Mapper.CreateMap<UrbanizadoraDto, Urbanizadora>();
            Mapper.CreateMap<Urbanizadora, UrbanizadoraDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="UrbanizadoraDto"/> en un <see cref="Urbanizadora"/>
        /// </summary>
        /// <param name="urbanizadora">UrbanizadoraDto</param>
        /// <returns>Urbanizadora</returns>
        public static Urbanizadora Convert(this UrbanizadoraDto urbanizadora)
        {
            return Mapper.Map<UrbanizadoraDto, Urbanizadora>(urbanizadora);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="UrbanizadoraDto"/> a una de <see cref="Urbanizadora"/>
        /// </summary>
        /// <param name="listaUrbanizadora">Lista UrbanizadoraDto</param>
        /// <returns>Lista Urbanizadora</returns>
        public static IEnumerable<Urbanizadora> Convert(this IEnumerable<UrbanizadoraDto> listaUrbanizadora)
        {
            return Mapper.Map<IEnumerable<UrbanizadoraDto>, IEnumerable<Urbanizadora>>(listaUrbanizadora);
        }

		/// <summary>
        /// Convierte un <see cref="Urbanizadora"/> en un <see cref="UrbanizadoraDto"/>
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>UrbanizadoraDto</returns>
        public static UrbanizadoraDto ConvertToDto(this Urbanizadora urbanizadora)
        {
            return Mapper.Map<Urbanizadora,UrbanizadoraDto>(urbanizadora);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="Urbanizadora"/> a una de <see cref="UrbanizadoraDto"/>
        /// </summary>
        /// <param name="listaUrbanizadora">Lista Urbanizadora</param>
        /// <returns>Lista UrbanizadoraDto</returns>
        public static IEnumerable<UrbanizadoraDto> ConvertToDto(this IEnumerable<Urbanizadora> listaUrbanizadora)
        {
            return Mapper.Map<IEnumerable<Urbanizadora>, IEnumerable<UrbanizadoraDto>>(listaUrbanizadora);
        }

		#endregion
	}
}