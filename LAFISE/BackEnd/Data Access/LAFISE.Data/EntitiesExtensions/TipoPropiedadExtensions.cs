/*==========================================================================
Archivo:            TipoPropiedadExtensions
Descripción:        Extensión de TipoPropiedadData                     
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    /// Extensión de TipoPropiedadData 
    /// </summary>
	public static class TipoPropiedadExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Constructor
        /// </summary>
		static TipoPropiedadExtensions()
        {
            Mapper.CreateMap<TipoPropiedadDto, TipoPropiedad>();
            Mapper.CreateMap<TipoPropiedad, TipoPropiedadDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="TipoPropiedadDto"/> en un <see cref="TipoPropiedad"/>
        /// </summary>
        /// <param name="tipopropiedad">TipoPropiedadDto</param>
        /// <returns>TipoPropiedad</returns>
        public static TipoPropiedad Convert(this TipoPropiedadDto tipopropiedad)
        {
            return Mapper.Map<TipoPropiedadDto, TipoPropiedad>(tipopropiedad);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="TipoPropiedadDto"/> a una de <see cref="TipoPropiedad"/>
        /// </summary>
        /// <param name="listaTipoPropiedad">Lista TipoPropiedadDto</param>
        /// <returns>Lista TipoPropiedad</returns>
        public static IEnumerable<TipoPropiedad> Convert(this IEnumerable<TipoPropiedadDto> listaTipoPropiedad)
        {
            return Mapper.Map<IEnumerable<TipoPropiedadDto>, IEnumerable<TipoPropiedad>>(listaTipoPropiedad);
        }

		/// <summary>
        /// Convierte un <see cref="TipoPropiedad"/> en un <see cref="TipoPropiedadDto"/>
        /// </summary>
        /// <param name="tipopropiedad">TipoPropiedad</param>
        /// <returns>TipoPropiedadDto</returns>
        public static TipoPropiedadDto ConvertToDto(this TipoPropiedad tipopropiedad)
        {
            return Mapper.Map<TipoPropiedad,TipoPropiedadDto>(tipopropiedad);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="TipoPropiedad"/> a una de <see cref="TipoPropiedadDto"/>
        /// </summary>
        /// <param name="listaTipoPropiedad">Lista TipoPropiedad</param>
        /// <returns>Lista TipoPropiedadDto</returns>
        public static IEnumerable<TipoPropiedadDto> ConvertToDto(this IEnumerable<TipoPropiedad> listaTipoPropiedad)
        {
            return Mapper.Map<IEnumerable<TipoPropiedad>, IEnumerable<TipoPropiedadDto>>(listaTipoPropiedad);
        }

		#endregion
	}
}