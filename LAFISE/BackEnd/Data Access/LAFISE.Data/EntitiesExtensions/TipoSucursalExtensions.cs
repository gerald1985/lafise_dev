/*==========================================================================
Archivo:            TipoSucursalExtensions
Descripción:        Extensión  TipoSucursalData                      
Autor:              paola.munoz                           
Fecha de creación:  06/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
	/// <summary>
    ///Extensión  TipoSucursalData   
    /// </summary>
	public static class TipoSucursalExtensions
	{
		#region Código Autogenerado
        /// <summary>
        /// Costructor
        /// </summary>
        static TipoSucursalExtensions()
        {
            Mapper.CreateMap<TipoSucursalDto, TipoSucursal>();
            Mapper.CreateMap<TipoSucursal, TipoSucursalDto>();
        }

		 /// <summary>
        /// Convierte un <see cref="TipoSucursalDto"/> en un <see cref="TipoSucursal"/>
        /// </summary>
        /// <param name="tipoSucursal">TipoSucursalDto</param>
        /// <returns>TipoSucursal</returns>
        public static TipoSucursal Convert(this TipoSucursalDto tipoSucursal)
        {
            return Mapper.Map<TipoSucursalDto, TipoSucursal>(tipoSucursal);
        }
		
		/// <summary>
        /// Convierte una lista de <see cref="TipoSucursalDto"/> a una de <see cref="TipoSucursal"/>
        /// </summary>
        /// <param name="listaTipoSucursal">Lista TipoSucursalDto</param>
        /// <returns>Lista TipoSucursal</returns>
        public static IEnumerable<TipoSucursal> Convert(this IEnumerable<TipoSucursalDto> listaTipoSucursal)
        {
            return Mapper.Map<IEnumerable<TipoSucursalDto>, IEnumerable<TipoSucursal>>(listaTipoSucursal);
        }

		/// <summary>
        /// Convierte un <see cref="TipoSucursal"/> en un <see cref="TipoSucursalDto"/>
        /// </summary>
        /// <param name="tipoSucursal">TipoSucursal</param>
        /// <returns>TipoSucursalDto</returns>
        public static TipoSucursalDto ConvertToDto(this TipoSucursal tipoSucursal)
        {
            return Mapper.Map<TipoSucursal, TipoSucursalDto>(tipoSucursal);
        }

		/// <summary>
        ///  Convierte una lista de <see cref="TipoSucursal"/> a una de <see cref="TipoSucursalDto"/>
        /// </summary>
        /// <param name="listaTipoSucursal">Lista TipoSucursal</param>
        /// <returns>Lista TipoSucursalDto</returns>
        public static IEnumerable<TipoSucursalDto> ConvertToDto(this IEnumerable<TipoSucursal> listaTipoSucursal)
        {
            return Mapper.Map<IEnumerable<TipoSucursal>, IEnumerable<TipoSucursalDto>>(listaTipoSucursal);
        }

		#endregion
	}
}