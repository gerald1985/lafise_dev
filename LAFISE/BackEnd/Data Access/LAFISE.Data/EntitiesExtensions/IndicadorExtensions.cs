﻿/*==========================================================================
Archivo:            IndicadorExtensions
Descripción:        Extension de Indicador                  
Autor:              arley.lopez                  
Fecha de creación:  12/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion

namespace LAFISE.Data
{
    public static class IndicadorExtensions
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static IndicadorExtensions()
        {
            Mapper.CreateMap<IndicadorDto, Indicador>();
            Mapper.CreateMap<Indicador, IndicadorDto>();
        }

        /// <summary>
        /// Convierte un <see cref="IndicadorDto"/> en un <see cref="Indicador"/>
        /// </summary>
        /// <param name="indicador">IndicadorDto</param>
        /// <returns>Indicador</returns>
        public static Indicador Convert(this IndicadorDto indicador)
        {
            return Mapper.Map<IndicadorDto, Indicador>(indicador);
        }

        /// <summary>
        /// Convierte una lista de <see cref="IndicadorDto"/> a una de <see cref="Indicador"/>
        /// </summary>
        /// <param name="listaIndicador">Lista IndicadorDto</param>
        /// <returns>Indicador</returns>
        public static IEnumerable<Indicador> Convert(this IEnumerable<IndicadorDto> listaIndicador)
        {
            return Mapper.Map<IEnumerable<IndicadorDto>, IEnumerable<Indicador>>(listaIndicador);
        }

        /// <summary>
        /// Convierte un <see cref="Indicador"/> en un <see cref="IndicadorDto"/>
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Lista IndicadorDto</returns>
        public static IndicadorDto ConvertToDto(this Indicador indicador)
        {
            return Mapper.Map<Indicador, IndicadorDto>(indicador);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="Indicador"/> a una de <see cref="IndicadorDto"/>
        /// </summary>
        /// <param name="listaIndicador">Lista Indicador</param>
        /// <returns>Lista IndicadorDto</returns>
        public static IEnumerable<IndicadorDto> ConvertToDto(this IEnumerable<Indicador> listaIndicador)
        {
            return Mapper.Map<IEnumerable<Indicador>, IEnumerable<IndicadorDto>>(listaIndicador);
        }
    }
}
