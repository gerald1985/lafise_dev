﻿/*==========================================================================
Archivo:            SucursalExtensions
Descripción:        Extensión de SucursalData                      
Autor:              paola.munoz                          
Fecha de creación:  30/09/2015 5:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System.Collections.Generic;
using AutoMapper;
using LAFISE.Entities.Dtos;
using LAFISE.Entities.Models;

#endregion


namespace LAFISE.Data
{
    /// <summary>
    /// Extensión de SucursalData
    /// </summary>
    public static class SucursalExtensions
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static SucursalExtensions()
        {
            Mapper.CreateMap<SucursalDto, Sucursal>();
            Mapper.CreateMap<Sucursal, SucursalDto>();
        }

        /// <summary>
        /// Convierte un <see cref="SucursalDto"/> en un <see cref="Sucursal"/>
        /// </summary>
        /// <param name="sucursal">SucursalDto</param>
        /// <returns>Sucursal</returns>
        public static Sucursal Convert(this SucursalDto sucursal)
        {
            return Mapper.Map<SucursalDto, Sucursal>(sucursal);
        }

        /// <summary>
        /// Convierte una lista de <see cref="SucursalDto"/> a una de <see cref="Sucursal"/>
        /// </summary>
        /// <param name="listaSucursal">Lista SucursalDto</param>
        /// <returns>Sucursal</returns>
        public static IEnumerable<Sucursal> Convert(this IEnumerable<SucursalDto> listaSucursal)
        {
            return Mapper.Map<IEnumerable<SucursalDto>, IEnumerable<Sucursal>>(listaSucursal);
        }

        /// <summary>
        /// Convierte un <see cref="Sucursal"/> en un <see cref="SucursalDto"/>
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Lista SucursalDto</returns>
        public static SucursalDto ConvertToDto(this Sucursal sucursal)
        {
            return Mapper.Map<Sucursal, SucursalDto>(sucursal);
        }

        /// <summary>
        ///  Convierte una lista de <see cref="Sucursal"/> a una de <see cref="SucursalDto"/>
        /// </summary>
        /// <param name="listaSucursal">Lista Sucursal</param>
        /// <returns>Lista SucursalDto</returns>
        public static IEnumerable<SucursalDto> ConvertToDto(this IEnumerable<Sucursal> listaSucursal)
        {
            return Mapper.Map<IEnumerable<Sucursal>, IEnumerable<SucursalDto>>(listaSucursal);
        }
    }
}
