﻿/*==========================================================================
Archivo:            TipoPropiedadController
Descripción:        Servicio de tipo de propiedad                   
Autor:              paola.munoz                           
Fecha de creación:  6/10/2015                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
#region Referencia
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
#endregion


namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para tipo de propiedad
    /// </summary>
    [AllowAnonymous]
    public class TipoPropiedadController : DnnApiController
    {
        private ITipoPropiedadBusiness tipoPropiedadesBusiness
        {
            get
            {
                return new TipoPropiedadBusiness();
            }
        }

        /// <summary>
        /// Consultar todos
        /// </summary>
        /// <returns>Lista tipo propiedad</returns>
        [HttpGet]
        public async Task<ICollection<TipoPropiedadDto>> VerTodos()
        {
            var result = await tipoPropiedadesBusiness.VerTodosGrid();
            return result.Result;
        }
        /// <summary>
        /// Consultar todos activos
        /// </summary>
        /// <returns>Lista tipo propiedad</returns>
        [HttpGet]
        public async Task<ICollection<TipoPropiedadDto>> VerTodosActivos()
        {
            var result = await tipoPropiedadesBusiness.VerTodosActivos();
            return result.Result;
        }
        /// <summary>
        /// Editar tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>Tipo Propiedad</returns>
        [HttpPost]
        public async Task<TipoPropiedadDto> EditarTipoPropiedad(TipoPropiedadDto tipoPropiedad)
        {
            tipoPropiedad.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tipoPropiedadesBusiness.Editar(tipoPropiedad);
            return result.Result;
        }
        /// <summary>
        /// Inactivar tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo Propiedad</param>
        /// <returns>string</returns>
        [HttpPost]
        public async Task<string> InactivarTipoPropiedad(TipoPropiedadDto tipoPropiedad)
        {
            tipoPropiedad.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tipoPropiedadesBusiness.Inactivar(tipoPropiedad);
            return result.Result;
        }
        /// <summary>
        /// Crear tipo propiedad
        /// </summary>
        /// <param name="tipoPropiedad">Tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        [HttpPost]
        public async Task<TipoPropiedadDto> CrearTipoPropiedad(TipoPropiedadDto tipoPropiedad)
        {
            tipoPropiedad.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tipoPropiedadesBusiness.Crear(tipoPropiedad);
            return result.Result;
        }
        /// <summary>
        /// Eliminar tipo propiedad
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        /// <returns>bool</returns>
        [HttpPost]
        public async Task<bool> EliminarTipoPropiedad(long id)
        {
           string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tipoPropiedadesBusiness.Eliminar(id,usuario);
            return result.Result;
        }
        /// <summary>
        /// Consultar por filtro
        /// </summary>
        /// <param name="tipoPropiedad">nombre tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        [HttpGet]
        public async Task<ICollection<TipoPropiedadDto>> ConsultarTipoPropiedadFiltro(string tipoPropiedad)
        {
            var result = await tipoPropiedadesBusiness.TipoPropiedadFiltro(tipoPropiedad);
            return result.Result;
        }
        /// <summary>
        /// Consultar por identificador
        /// </summary>
        /// <param name="id">Identificador de tipo propiedad</param>
        /// <returns>Tipo propiedad</returns>
        [HttpGet]
        public async Task<TipoPropiedadDto> ConsultarPorId(Int64 id)
        {
            var result = await tipoPropiedadesBusiness.VerPorId(id);
            return result.Result;
        }

        [HttpPost]
        public async Task<TipoPropiedadDto> ConsultarPorNombre(TipoPropiedadDto dtoTipoPropiedad)
        {
            var result = await tipoPropiedadesBusiness.ValidarTipoPropiedadUnica(dtoTipoPropiedad.Id, dtoTipoPropiedad.Tipo);
            return result.Result;
        }

        [HttpGet]
        public async Task<TipoPropiedadDto> ConsultarRelaciones(long Id)
        {
            var result = await tipoPropiedadesBusiness.ValidarRelaciones(Id);
            return result.Result;
        }

    }
}
