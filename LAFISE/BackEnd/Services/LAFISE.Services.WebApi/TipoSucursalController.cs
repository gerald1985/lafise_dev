﻿/*==========================================================================
Archivo:            TipoSucursalController
Descripción:         Servicio Tipo sucursal                  
Autor:              paola.munoz                           
Fecha de creación:  7/10/2015                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
#region Referencia
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
#endregion


namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para tipo sucursal
    /// </summary>
    [AllowAnonymous]
    public class TipoSucursalController : DnnApiController
    {
        private ITipoSucursalBusiness tipoSucursalBusiness
        {
            get
            {
                return new TipoSucursalBusiness();
            }
        }

        /// <summary>
        /// Consultar todos los tipos sucursal
        /// </summary>
        /// <returns>lista tipo sucursal</returns>
        [HttpGet]
        public async Task<ICollection<TipoSucursalDto>> VerTodos()
        {
            var result = await tipoSucursalBusiness.VerTodosGrid();

            return result.Result;
        }
        /// <summary>
        /// Editar tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        [HttpPost]
        public async Task<TipoSucursalDto> EditarTipoSucursal(TipoSucursalDto tipoSucursal)
        {
            tipoSucursal.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tipoSucursalBusiness.Editar(tipoSucursal);

            return result.Result;
        }
        /// <summary>
        /// Inactivar tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>string datos relacionados</returns>
        [HttpPost]
        public async Task<string> InactivarTipoSucursal(TipoSucursalDto tipoSucursal)
        {
            tipoSucursal.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tipoSucursalBusiness.Inactivar(tipoSucursal);

            return result.Result;
        }
        /// <summary>
        /// Crear tipo sucursal
        /// </summary>
        /// <param name="tipoSucursal">Tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        [HttpPost]
        public async Task<TipoSucursalDto> CrearTipoSucursal(TipoSucursalDto tipoSucursal)
        {
            tipoSucursal.Usuario = UserController.Instance.GetCurrentUserInfo().Username;
            var result = await tipoSucursalBusiness.Crear(tipoSucursal);

            return result.Result;
        }
        /// <summary>
        /// Eliminar tipo sucursal
        /// </summary>
        /// <param name="id">Identificador de tipo sucursal</param>
        /// <returns>string datos relacionados</returns>
        [HttpPost]
        public async Task<string> EliminarTipoSucursal(Int64 id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tipoSucursalBusiness.Eliminar(id, usuario);

            return result.Result;
        }
        /// <summary>
        /// Consultar filtro
        /// </summary>
        /// <param name="tipoSucursal">nombre tipo sucursal</param>
        /// <returns>lista tipo sucursal</returns>
        [HttpGet]
        public async Task<ICollection<TipoSucursalDto>> ConsultarTipoSucursalFiltro(string tipoSucursal)
        {
            var result = await tipoSucursalBusiness.TipoSucursalFiltro(tipoSucursal);

            return result.Result;
        }
        /// <summary>
        /// Consultar por identificador
        /// </summary>
        /// <param name="id">Identificador de tipo sucursal</param>
        /// <returns>Tipo sucursal</returns>
        [HttpGet]
        public async Task<TipoSucursalDto> ConsultarPorId(Int64 id)
        {
            var result = await tipoSucursalBusiness.VerPorId(id);

            return result.Result;
        }
        /// <summary>
        /// Consultar para la validacion por nombre
        /// </summary>
        /// <param name="tipoSucursal">Entidad tipo sucursal</param>
        /// <returns>Entidad que retornan</returns>
        [HttpPost]
        public async Task<TipoSucursalDto> ConsultarPorNombre(TipoSucursalDto tipoSucursal)
        {
            var result = await tipoSucursalBusiness.ValidarTipoSucursalUnica(tipoSucursal);
            return result.Result;
        }

        /// <summary>
        /// Consultar para la validacion el tipo de sucursal
        /// </summary>
        /// <param name="id">Identificador del tipo de sucursal</param>
        /// <returns>String resultado</returns>
        [HttpGet]
        public async Task<string> ValidarTipoSucursal(Int64 id)
        {
            string nombre = "";
            var result = await tipoSucursalBusiness.ValidarTipoSucursal(id);
            if (result.SuccessfulOperation)
            {
                nombre = result.Result.Nombre != null?result.Result.Nombre:"";
            }
            return nombre;
        }

        /// <summary>
        /// Consultar todos los tipos sucursal activos
        /// </summary>
        /// <returns>lista tipo sucursal</returns>
        [HttpGet]
        public async Task<ICollection<TipoSucursalDto>> VerTodosActivos()
        {
            var result = await tipoSucursalBusiness.VerTodosActivos();

            return result.Result;
        }

    }
}
