﻿/*==========================================================================
Archivo:            UrbanizadoraController
Descripción:        Servicio de Urbanizadora                  
Autor:              steven.echavarria                          
Fecha de creación:  07/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para urbanizadora
    /// </summary>
    [AllowAnonymous]
    public class UrbanizadoraController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IUrbanizadoraBusiness
        /// </summary>
        public IUrbanizadoraBusiness _UrbanizadoraBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public UrbanizadoraController()
        {
            _UrbanizadoraBusiness = new UrbanizadoraBusiness();
        }
        /// <summary>
        /// Ver todas las urbanizadoras activas
        /// </summary>
        /// <returns>Lista urbanizadora</returns>
        [HttpGet]
        public async Task<ICollection<UrbanizadoraDto>> VerTodosActivos()
        {
            var result = await _UrbanizadoraBusiness.VerTodosActivos();
            return result.Result;
        }
        /// <summary>
        /// Ver todas las urbanizadoras activas y por país
        /// </summary>
        /// <param name="pathIdPais">url para sacar identificador del país</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ICollection<UrbanizadoraDto>> VerTodosActivosPorPais(string pathIdPais)
        {
            var result = await _UrbanizadoraBusiness.VerTodosActivosPorPais(pathIdPais);
            return result.Result;
        }
        /// <summary>
        /// Consultar urbanizadora por tipo de propiedad
        /// </summary>
        /// <param name="urbanizadoraTipoPropiedad">UrbanizadoraTipoPropiedadDto</param>
        /// <returns>UrbanizadoraTipoPropiedadDto</returns>
        [HttpPost]
        public async Task<ICollection<UrbanizadoraTipoPropiedadDto>> VerUrbanizadorasTiposPropiedades(UrbanizadoraTipoPropiedadDto urbanizadoraTipoPropiedad)
        {
            string paisUsuario = UserController.Instance.GetCurrentUserInfo().Profile.City;
            if (paisUsuario != null && !paisUsuario.Equals(""))
            {
                urbanizadoraTipoPropiedad.Pais = paisUsuario;
            }
            var result = await _UrbanizadoraBusiness.VerUrbanizadorasTiposPropiedades(urbanizadoraTipoPropiedad);
            return result.Result;
        }
        /// <summary>
        /// Ver urbanizadora por identificador
        /// </summary>
        /// <param name="id">Identificador de la urbanizadora</param>
        /// <returns>urbanizadora</returns>
        [HttpGet]
        public async Task<UrbanizadoraDto> VerUrbanizadoraPorId(Int64 id)
        {
            var result = await _UrbanizadoraBusiness.VerUrbanizadoraPorId(id);
            return result.Result;
        }
        /// <summary>
        /// Metodo que valida que el nombre de la urbanizadora sea unico.
        /// </summary>
        /// <param name="urbanizadora">entidad urbanizadora</param>
        /// <returns>urbanizadora</returns>
        [HttpPost]
        public async Task<UrbanizadoraDto> ValidarUrbanizadoraPorNombre(UrbanizadoraDto urbanizadora)
        {
            var result = await _UrbanizadoraBusiness.ValidarUrbanizadoraPorNombre(urbanizadora);
            return result.Result;
        }
        /// <summary>
        /// Consultar el detalle de la urbanizadora
        /// </summary>
        /// <param name="id">Identificador de la urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        [HttpGet]
        public async Task<UrbanizadoraDto> ConsultarDetalleUrbanizadora(long id)
        {
            var result = await _UrbanizadoraBusiness.ConsultarDetalleUrbanizadora(id);
            return result.Result;
        }
        /// <summary>
        /// Crear urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        [HttpPost]
        public async Task<UrbanizadoraDto> CrearUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            urbanizadora.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _UrbanizadoraBusiness.CrearUrbanizadora(urbanizadora);
            return result.Result;
        }
        /// <summary>
        /// Editar urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        [HttpPost]
        public async Task<UrbanizadoraDto> EditarUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            urbanizadora.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _UrbanizadoraBusiness.EditarUrbanizadora(urbanizadora);
            return result.Result;
        }
        /// <summary>
        /// Eliminar urbanizadora
        /// </summary>
        /// <param name="id">identificador de la urbanizadora</param>
        /// <returns>Urbanizadora</returns>
        [HttpPost]
        public async Task<UrbanizadoraDto> EliminarUrbanizadoraPorId(int id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _UrbanizadoraBusiness.EliminarUrbanizadoraPorId(id,usuario);
            return result.Result;
        }
        /// <summary>
        /// Inactivar urbanizadora
        /// </summary>
        /// <param name="urbanizadora">Urbanizadora</param>
        /// <returns>bool</returns>
        [HttpPost]
        public async Task<UrbanizadoraDto> InactivarUrbanizadora(UrbanizadoraDto urbanizadora)
        {
            urbanizadora.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _UrbanizadoraBusiness.InactivarUrbanizadora(urbanizadora);
            return result.Result;
        }
    }
}
