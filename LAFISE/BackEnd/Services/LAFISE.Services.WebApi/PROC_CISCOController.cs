﻿using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace LAFISE.Services.WebApi
{
    [AllowAnonymous]
    public class PROC_CISCOController : DnnApiController
    {
        public IPROC_CISCOBusiness _PROC_CISCOBusiness;

        public PROC_CISCOController()
        {
            _PROC_CISCOBusiness = new PROC_CISCOBusiness();
        }

        [HttpGet]
        public async Task<ICollection<PROC_CISCO_CiudadesDto>> VerCiudades()
        {
            var result = await _PROC_CISCOBusiness.VerCiudadesPROCTodos();
            return result.Result;

        }

        [HttpGet]
        public async Task<ICollection<PROC_CISCO_PaisesDto>> VerPaises()
        {
            var result = await _PROC_CISCOBusiness.VerPaisesPROCTodos();
            return result.Result;

        }

        [HttpGet]
        public async Task<ICollection<PROC_CISCO_Centro_De_CostoDto>> VerSurcursales()
        {
            var result = await _PROC_CISCOBusiness.VerSucursalesPROCTodos();
            return result.Result;

        }

        [HttpGet]
        public async Task<ICollection<PROC_CISCO_OcupacionesDto>> VerOcupaciones()
        {
            var result = await _PROC_CISCOBusiness.VerOcupacionesPROCTodos();
            return result.Result;

        }
    }
}

