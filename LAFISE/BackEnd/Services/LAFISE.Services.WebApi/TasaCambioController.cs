﻿/*==========================================================================
Archivo:            TasaCambioController
Descripción:        Servicio de tasa de cambio                  
Autor:              paola.munoz                          
Fecha de creación:  25/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para tasa de cambio
    /// </summary>
    [AllowAnonymous]
    public class TasaCambioController : DnnApiController
    {
         /// <summary>
        /// Instancia de la interfaz ISuscripcionBoletinBusiness
        /// </summary>
        public ITasaCambioBusiness _TasaCambioBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public TasaCambioController()
        {
            _TasaCambioBusiness = new TasaCambioBusiness();
        }

  
        [HttpPost]
        public async Task<ICollection<TasaCambioDto>> VerPorPaisActivo(TasaCambioDto tasaCambio)
        {

            var result = await _TasaCambioBusiness.VerPorPaisActivo(tasaCambio);
            return result.Result;
        }

    }
}
