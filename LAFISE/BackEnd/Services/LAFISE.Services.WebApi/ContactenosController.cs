﻿/*==========================================================================
Archivo:            ContactenosController
Descripción:        Servicio de Contactenos                     
Autor:              paola.munoz                          
Fecha de creación:  29/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Common.FrontEnd;
using LAFISE.Entities.Dtos;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.UI;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para contactenos.
    /// </summary>
    [AllowAnonymous]
    public class ContactenosController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IContactenosBusiness
        /// </summary>
        public IContactenosBusiness _ContactenosBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public ContactenosController()
        {
            _ContactenosBusiness = new ContactenosBusiness();
        }

        /// <summary>
        /// Crear contactenos
        /// </summary>
        /// <param name="contactenos">Contactenos</param>
        /// <returns>ContactenosDto</returns>
        [HttpPost]
        public async Task<ContactenosDto> CrearContactenos(ContactenosDto contactenos)
        {
            var result = await _ContactenosBusiness.Crear(contactenos);
            if (result.SuccessfulOperation)
            {
                Hashtable parametros = new Hashtable();
                parametros.Add("[NOMBRECOMPLETO]", contactenos.NombreCompleto);
                parametros.Add("[TELEFONO]", contactenos.Telefono);
                parametros.Add("[EMAIL]", contactenos.Email);
                parametros.Add("[ASUNTO]", contactenos.Asunto);
                parametros.Add("[MENSAJE]", contactenos.Mensaje);
                parametros.Add("[IDENTIFICACION]", contactenos.Identificacion);
                List<MailAddress> destinatario = new List<MailAddress>();
                destinatario.Add(new MailAddress(contactenos.Email));
                MailHelper.EnvioCorreo(TipoPlantilla.Contactenos, parametros, true, "Contáctenos", "", result.Result.IdPais, null, null);//Administrador
                MailHelper.EnvioCorreo(TipoPlantilla.Contactenos, parametros, false, "Contáctenos", "", result.Result.IdPais, destinatario, null);//Usuario
            }
            return result.Result;
        }

    }
}
