﻿/*==========================================================================
Archivo:            IndicadorController
Descripción:        Servicio de Indicador                  
Autor:              arley.lopez                         
Fecha de creación:  17/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencia
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Common.Backend;
using LAFISE.Entities.Dtos;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para indicador
    /// </summary>
    [AllowAnonymous]
    public class IndicadorController : DnnApiController
    {

        private IIndicadorBusiness indicadorBusiness
        {
            get
            {
                return new IndicadorBusiness();
            }
        }

        /// <summary>
        /// Consultar Todos los Indicadores activos
        /// </summary>
        /// <returns>Lista de indicadores</returns>
        [HttpGet]
        public List<IndicadorYahooDto> VerTodosActivos()
        {
            List<IndicadorYahooDto> listaIndicador = new List<IndicadorYahooDto>();
            var result = indicadorBusiness.VerTodosActivos();
            if (result.SuccessfulOperation)
            {
                string symbolList = "\u0022" + String.Join(", ", result.Result.Select(w => w.SimboloActual).ToArray()) + "\u0022";
                string url = string.Format(Constants.URL_YAHOO, symbolList);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json;charset=\"utf-8\"";
                request.Accept = "application/json";
                WebResponse resp = request.GetResponse();
                Stream respStream = resp.GetResponseStream();
                StreamReader reader = new StreamReader(respStream);
                var response = reader.ReadToEnd();
                dynamic resultado = JsonConvert.DeserializeObject(response);
                foreach (var item in resultado.query.results.quote)
                {
                    string dato = item.symbol;
                    var registro = result.Result.Where(c => c.SimboloActual == dato).FirstOrDefault();
                    IndicadorYahooDto indicadorYahoo = new IndicadorYahooDto();
                    indicadorYahoo.Id = registro.Id;
                    indicadorYahoo.Nombre = registro.NombreIndicador;
                    indicadorYahoo.Simbolo = registro.SimboloHistorico;
                    indicadorYahoo.ValorActual = item.LastTradePriceOnly;
                    indicadorYahoo.Variacion = item.PercentChange;
                    indicadorYahoo.Fecha = item.LastTradeDate;
                    indicadorYahoo.Hora = item.LastTradeTime;
                    listaIndicador.Add(indicadorYahoo);
                }
            }
            return listaIndicador;
        }

        /// <summary>
        /// Consultar Históricos de un Indicador
        /// </summary>
        /// <returns>Retorna la lista de histórico de un indicador</returns>
        [HttpPost]
        public List<IndicadoresHistoricosYahooDto> VerHistorico(FiltroIndicadorYahooDto filtro)
        {
            List<IndicadoresHistoricosYahooDto> listaIndicador = new List<IndicadoresHistoricosYahooDto>();

            string symbolList = "\u0022" + filtro.Simbolo + "\u0022";
            string FechaIni = "\u0022" +filtro.FechaInicial + "\u0022";
            string FechaFin = "\u0022" + filtro.FechaFinal + "\u0022";

            string url = string.Format(Constants.URL_YAHOO_HISTORICO, symbolList, FechaIni, FechaFin);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Accept = "application/json";
            WebResponse resp = request.GetResponse();
            Stream respStream = resp.GetResponseStream();
            StreamReader reader = new StreamReader(respStream);
            var response = reader.ReadToEnd();
            dynamic resultado = JsonConvert.DeserializeObject(response);
            foreach (var item in resultado.query.results.quote)
            {
                string dato = item.symbol;
                IndicadoresHistoricosYahooDto indicadorYahoo = new IndicadoresHistoricosYahooDto();

                indicadorYahoo.Fecha = item.Date;
                indicadorYahoo.Apertura = item.Open;
                indicadorYahoo.MasAlto = item.High;
                indicadorYahoo.MasBajo = item.Low;
                indicadorYahoo.Cierre = item.Close;
                indicadorYahoo.Volumen = item.Volume;
                indicadorYahoo.CierreAjustado = item.Adj_Close;
                 listaIndicador.Add(indicadorYahoo);
            }
            return listaIndicador;
        }

        /// <summary>
        /// Verificar que el indicador exista en Yahoo Finance
        /// </summary>
        /// <returns>Lista de indicadores</returns>
        [HttpGet]
        public bool VerificarIndicador(string simbolo)
        {
            //var simbolo = "GOLD";
            string symbolList = "\u0022" + simbolo + "\u0022";
            //string url = string.Format(Constants.URL_YAHOO, symbolList);
            string url = string.Format(Constants.URL_YAHOO, symbolList);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Accept = "application/json";
            WebResponse resp = request.GetResponse();
            Stream respStream = resp.GetResponseStream();
            StreamReader reader = new StreamReader(respStream);
            var response = reader.ReadToEnd();
            dynamic resultado = JsonConvert.DeserializeObject(response);

            var res2 = resultado.query.results;
            var res = resultado.query.results.quote.Ask;

            if(res == null){
                return false;
            }
            else
            {
                return true;
            }
        }

          /// <summary>
        /// Verificar que el código del indicador ingresado sea valido
        /// </summary>
        /// <returns>Retorna la lista de histórico de un indicador</returns>
        [HttpPost]
        public bool VerificarIndicadorHistorico(FiltroIndicadorYahooDto filtro)
        {
 
            string symbolList = "\u0022" + filtro.Simbolo + "\u0022";
            string FechaIni = "\u0022" +filtro.FechaInicial + "\u0022";
            string FechaFin = "\u0022" + filtro.FechaFinal + "\u0022";

            string url = string.Format(Constants.URL_YAHOO_HISTORICO, symbolList, FechaIni, FechaFin);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json;charset=\"utf-8\"";
            request.Accept = "application/json";
            WebResponse resp = request.GetResponse();
            Stream respStream = resp.GetResponseStream();
            StreamReader reader = new StreamReader(respStream);
            var response = reader.ReadToEnd();
            dynamic resultado = JsonConvert.DeserializeObject(response);

            var res = resultado.query.results;

            if (res == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
  

        /// <summary>
        /// Consultar Todos los Indicadores activos y no activos
        /// </summary>
        /// <returns>Lista de indicadores</returns>
        [HttpGet]
        public async Task<ICollection<IndicadorDto>> VerTodos()
        {
            var result = await indicadorBusiness.VerTodos();

            return result.Result;
        }

        /// <summary>
        /// Crear indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        [HttpPost]
        public async Task<IndicadorDto> CrearIndicador(IndicadorDto indicador)
        {
            indicador.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await indicadorBusiness.Crear(indicador);

            return result.Result;
        }

        /// <summary>
        /// Editar indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>Indicador</returns>
        [HttpPost]
        public async Task<IndicadorDto> EditarIndicador(IndicadorDto indicador)
        {
            indicador.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await indicadorBusiness.Editar(indicador);

            return result.Result;
        }

        /// <summary>
        /// Inactivar indicador
        /// </summary>
        /// <param name="indicador">Indicador</param>
        /// <returns>bool</returns>
        [HttpPost]
        public async Task<bool> InactivarIndicador(IndicadorDto indicador)
        {
            indicador.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await indicadorBusiness.Inactivar(indicador);

            return result.Result;
        }

        /// <summary>
        /// Eliminar indicador
        /// </summary>
        /// <param name="id">Identificador del indicador</param>
        /// <returns>bool</returns>
        [HttpGet]
        public async Task<bool> EliminarIndicador(Int64 id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await indicadorBusiness.Eliminar(id, usuario);

            return result.Result;
        }

        /// <summary>
        /// Consultar para la validacion por nombre
        /// </summary>
        /// <param name="indicador">Entidad indicador</param>
        /// <returns>Entidad indicador</returns>
        [HttpPost]
        public async Task<IndicadorDto> ValidarIndicadorUnico(IndicadorDto indicador)
        {
            var result = await indicadorBusiness.ValidarIndicadorUnico(indicador);
            return result.Result;
        }
    }
}
