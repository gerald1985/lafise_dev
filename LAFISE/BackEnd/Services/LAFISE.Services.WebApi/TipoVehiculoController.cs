﻿/*==========================================================================
Archivo:            TipoVehiculoController
Descripción:        Servicio Tipo vehículo                  
Autor:              juan.hincapie                           
Fecha de creación:  7/10/2015                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para Tipo Vehiculo
    /// </summary>

    [AllowAnonymous]
    public class TipoVehiculoController : DnnApiController
    {
        private ITipoVehiculoBusiness tipoSucursalBusiness
        {
            get
            {
                return new TipoVehiculoBusiness();
            }
        }

        /// <summary>
        /// Consultar todos los tipos vehiculo
        /// </summary>
        /// <returns>lista tipo vehiculo</returns>
        [HttpGet]
        public async Task<ICollection<TipoVehiculoDto>> VerTodos()
        {
            var result = await tipoSucursalBusiness.VerTodosGrid();

            return result.Result;
        }
    }
}
