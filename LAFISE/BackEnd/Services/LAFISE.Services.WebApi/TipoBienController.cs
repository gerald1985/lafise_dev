﻿using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
/*==========================================================================
Archivo:            TipoBienController
Descripción:        Servicio Tipo bien                  
Autor:              juan.hincapie                           
Fecha de creación:  7/01/2016                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para Tipo bien
    /// </summary>
    /// 
    [AllowAnonymous]
    public class TipoBienController : DnnApiController
    {
        private ITipoBienBusiness tipoBienBusiness
        {
            get
            {
                return new TipoBienBusiness();
            }
        }

        /// <summary>
        /// Consultar todos los tipos vehiculo
        /// </summary>
        /// <returns>lista tipo vehiculo</returns>
        [HttpGet]
        public async Task<ICollection<TipoBienDto>> VerTodos()
        {
            var result = await tipoBienBusiness.VerTodosGrid();

            return result.Result;
        }
    }
}
