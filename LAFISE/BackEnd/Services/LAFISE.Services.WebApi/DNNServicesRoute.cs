﻿/*==========================================================================
Archivo:            DNNServicesRoute
Descripción:        Administra las rutas de los servicios                   
Autor:              ximena.echeverri                           
Fecha de creación:  8/9/2015 10:49:17 PM                                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/

using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Routing;
using DotNetNuke.Web.Api;

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Administración de rutas servicios 
    /// </summary>
    public class DNNServicesRoute : DotNetNuke.Web.Api.IServiceRouteMapper
    {
        const string moduleFolderName = "Servicios";

        public void RegisterRoutes(IMapRoute routeManager)
        {
            routeManager.MapHttpRoute(moduleFolderName, "default", "{controller}/{action}/{id}",
                new { id = RouteParameter.Optional },
                new[] { "LAFISE.Services.WebApi" });
            GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());
        }
    }
}
