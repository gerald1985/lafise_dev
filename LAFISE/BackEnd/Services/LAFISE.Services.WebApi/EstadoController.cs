﻿/*==========================================================================
Archivo:            EstadoController
Descripción:        Controlador de EstadoController               
Autor:              paola.munoz                         
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http; 
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los métodos para los estados
    /// </summary>
    [AllowAnonymous]
    public class EstadoController : DnnApiController
    {
           /// <summary>
        /// Instancia de la interfaz ICategoriaBusiness
        /// </summary>
        public IEstadoBusiness _EstadoBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public EstadoController()
        {
            _EstadoBusiness = new EstadoBusiness();
        }

        /// <summary>
        /// Ver todos los estados teniendo en cuenta el identificador del tipo de estado
        /// </summary>
        /// <param name="idTipoSolicitud">Identificador del tipo de estado</param>
        /// <returns>Lista de estado</returns>
        [HttpGet]
        public async Task<ICollection<EstadoDto>> VerTodosIdSolicitud(Int64 idTipoEstado)
        {
            var result = await _EstadoBusiness.VerEstadoIdTipo(idTipoEstado);
            return result.Result;
        }
    }
}
