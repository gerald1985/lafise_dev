﻿/*==========================================================================
Archivo:            TarjetaController
Descripción:         Servicio de Tarjeta                  
Autor:              paola.munoz                           
Fecha de creación:  6/10/2015                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
#region Referencia
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
#endregion


namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para tarjeta
    /// </summary>

    [AllowAnonymous]
    public class TarjetaController : DnnApiController
    {
        private ITarjetaBusiness tarjetaBusiness
        {
            get
            {
                return new TarjetaBusiness();
            }
        }
        /// <summary>
        /// Consultar todos
        /// </summary>
        /// <returns>Lista tarjeta</returns>
        [HttpGet]
        public async Task<ICollection<TarjetaDto>> VerTodos()
        {
            var result = await tarjetaBusiness.VerTodosGrid();

            return result.Result;
        }
        /// <summary>
        /// Editar tarjeta
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>Tarjeta</returns>
        [HttpPost]
        public async Task<TarjetaDto> EditarTarjeta(TarjetaDto tarjeta)
        {
            tarjeta.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tarjetaBusiness.Editar(tarjeta);

            return result.Result;
        }
        /// <summary>
        /// Inactivar tarjeta
        /// </summary>
        /// <param name="tarjeta">tarjeta</param>
        /// <returns>bool</returns>
        [HttpPost]
        public async Task<bool> InactivarTarjeta(TarjetaDto tarjeta)
        {
            tarjeta.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tarjetaBusiness.Inactivar(tarjeta);

            return result.Result;
        }
        /// <summary>
        /// Crear tarjeta
        /// </summary>
        /// <param name="tarjeta">Tarjeta</param>
        /// <returns>tarjeta</returns>
        [HttpPost]
        public async Task<TarjetaDto> CrearTarjeta(TarjetaDto tarjeta)
        {
            tarjeta.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tarjetaBusiness.Crear(tarjeta);

            return result.Result;
        }
        /// <summary>
        /// Eliminar tarjeta
        /// </summary>
        /// <param name="id">Identificador de la tarjeta</param>
        /// <returns>bool</returns>
        [HttpPost]
        public async Task<bool> EliminarTarjeta(Int64 id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await tarjetaBusiness.Eliminar(id, usuario);

            return result.Result;
        }
        /// <summary>
        /// Consultar filtro
        /// </summary>
        /// <param name="tarjetaCredito">nombre de la tarjeta</param>
        /// <returns>Lista de tarjetas</returns>
        [HttpGet]
        public async Task<ICollection<TarjetaDto>> ConsultarTarjetaFiltro(string tarjetaCredito)
        {
            var result = await tarjetaBusiness.TarjetaFiltro(tarjetaCredito);

            return result.Result;
        }
        /// <summary>
        /// consultar por identificador
        /// </summary>
        /// <param name="id">Identificador de la tarjeta</param>
        /// <returns>Tarjeta</returns>
        [HttpGet]
        public async Task<TarjetaDto> ConsultarPorId(Int64 id)
        {
            var result = await tarjetaBusiness.VerPorId(id);

            return result.Result;
        }

        /// <summary>
        /// Consultar para la validacion por nombre
        /// </summary>
        /// <param name="tarjeta">Entidad tarjeta</param>
        /// <returns>Entidad que retornan</returns>
        [HttpPost]
        public async Task<TarjetaDto> ConsultarPorNombre(TarjetaDto tarjeta)
        {
            var result = await tarjetaBusiness.ValidarTarjetaUnica(tarjeta);
            return result.Result;
        }


    }
}
