﻿/*==========================================================================
Archivo:            ICategoriaBusiness
Descripción:        Logica de negocio de Categoria               
Autor:              steven.echavarria                          
Fecha de creación:  13/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http; 
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los métodos para las categorias.
    /// </summary>
    [AllowAnonymous]
    public class CategoriaController : DnnApiController
    {
           /// <summary>
        /// Instancia de la interfaz ICategoriaBusiness
        /// </summary>
        public ICategoriaBusiness _CategoriaBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public CategoriaController()
        {
            _CategoriaBusiness = new CategoriaBusiness();
        }

        /// <summary>
        /// Ver todos las categorias
        /// </summary>
        /// <param name="idTipoSolicitud">Identificador del tipo de la solicitud</param>
        /// <returns>Lista de categoria</returns>
        [HttpGet]
        public async Task<ICollection<CategoriaDto>> VerTodosIdSolicitud(Int64 idTipoSolicitud)
        {
            var result = await _CategoriaBusiness.VerTodosIdSolicitud(idTipoSolicitud);
            return result.Result;
        }
    }
}
