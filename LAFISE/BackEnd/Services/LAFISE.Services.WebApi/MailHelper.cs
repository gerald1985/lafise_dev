﻿/*==========================================================================
Source File:  MailHelper.cs                                             
Description:   Ayudas envio de correo, tomado de lo realizado por Mario                    
Author:        paola.munoz                          
Date:          domingo, 9 de noviembre de 2015                                               
Last Modified: domingo, 9 de novienbre de 2015                                               
Version:       1.1                                                       
Copyright (c), 2012 Aleriant SAS                                         
==========================================================================*/


#region Referencias

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Mail;
using System.Web;
using Aleriant.Mail;
using DotNetNuke.Entities.Host;
using LAFISE.Common.Backend;
using DotNetNuke.Services.Log.EventLog;
using DotNetNuke.Entities.Portals;
using LAFISE.Business;
using LAFISE.Common.FrontEnd;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// 
    /// </summary>
    public enum TipoPlantilla
    {
        [EnumUtils("Contactenos.html")]
        Contactenos,
        [EnumUtils("MarcaAuto.html")]
        MarcaAuto,
        [EnumUtils("PrestamoPersonal.html")]
        PrestamoPersonal,
        [EnumUtils("PrestamoHipotecario.html")]
        PrestamoHipotecario,
        [EnumUtils("TarjetaCredito.html")]
        TarjetaCredito,
        [EnumUtils("TarjetaDebito.html")]
        TarjetaDebito,
        [EnumUtils("PrestamoEducativo.html")]
        PrestamoEducativo,
        [EnumUtils("AperturaCuenta.html")]
        AperturaCuenta,
        [EnumUtils("SeguroVidaAccidente.html")]
        SeguroVidaAccidente,
        [EnumUtils("SeguroVehicular.html")]
        SeguroVehicular,
        [EnumUtils("Reclamos.html")]
        Reclamos,
        [EnumUtils("Default.html")]
        Default,
        [EnumUtils("TerminacionRechazado.html")]
        TerminacionRechazado,
        [EnumUtils("TerminacionPreaprobado.html")]
        TerminacionPreaprobado,
        [EnumUtils("TerminacionAprobado.html")]
        TerminacionAprobado
    }

    /// <summary>
    /// 
    /// </summary>
    public class MailHelper
    {
        private static string RutaPlantillas
        {
            get { return System.Web.Hosting.HostingEnvironment.MapPath("~/PlantillasCorreo/"); }
        }

        /// <summary>
        /// Envio de correo con plantilla para usuario o administrador
        /// </summary>
        /// <param name="remitente">Se envia el remitente para el administrador, sino se envia en blanco</param>
        /// <param name="destinatarios">Se envia la lista de correo cuando es usuario, sino se envia null</param>
        /// <param name="tipoPlantilla">Se envia el enumerador tipo de planilla</param>
        /// <param name="parametros">Se envia la lista de parametros para ser reemplazado en los correos</param>
        /// <param name="usarNvelocity">Nvelocity, valor por defecto false</param>
        public static void EnvioCorreo(TipoPlantilla tipoPlantilla, Hashtable parametros, bool esAdmin, string tipoFormulario, string numeroSolicitud, Int64? idPais, List<MailAddress> destinatarios, List<Attachment> attachment = null, bool usarNvelocity = false)
        {
            if (parametros.ContainsKey("[CONTENIDOUSUARIO]"))
            {
                parametros.Remove("[CONTENIDOUSUARIO]");
            }
            if (parametros == null)
            {
                parametros = new Hashtable();
                if (parametros.Count == 0)
                {
                    parametros.Add("dummy", 1);
                }
            }
            else if (!esAdmin)
            {
                parametros.Add("[CONTENIDOUSUARIO]", string.Format(LAFISE.Common.Backend.Constants.SOLICITUD_CORREO_CONTENIDO_USUARIO, tipoFormulario, numeroSolicitud));
            }
            else
            {
                parametros.Add("[CONTENIDOUSUARIO]", string.Format(LAFISE.Common.Backend.Constants.SOLICITUD_CORREO_CONTENIDO_ADMIN, numeroSolicitud));
            }

            var mailHelper = new Aleriant.Mail.MailHelper();

            if (usarNvelocity)
                mailHelper = new Aleriant.Mail.MailHelper(new ProcesadorNVelocity());

            string asunto = string.Empty;
            string nombrePlantilla = string.Empty;
            string correo = string.Empty;
            string destinoAdminContactenos = string.Empty;

            switch (tipoPlantilla)
            {
                case TipoPlantilla.Contactenos:
                    asunto = "Formulario Contáctenos";
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    if (esAdmin)
                    {
                        if (idPais != null)
                        {
                            switch (idPais)
                            {
                                case 161: destinoAdminContactenos = LAFISE.Common.Backend.Constants.EMAIL_ENVIO_CORREO_CONTACTENOS_NICARAGUA;
                                    break;
                                case 49: destinoAdminContactenos = LAFISE.Common.Backend.Constants.EMAIL_ENVIO_CORREO_CONTACTENOS_COSTA_RICA;
                                    break;
                                case 96: destinoAdminContactenos = LAFISE.Common.Backend.Constants.EMAIL_ENVIO_CORREO_CONTACTENOS_HONDURAS;
                                    break;
                                case 169: destinoAdminContactenos = LAFISE.Common.Backend.Constants.EMAIL_ENVIO_CORREO_CONTACTENOS_PANAMA;
                                    break;
                                case 60: destinoAdminContactenos = LAFISE.Common.Backend.Constants.EMAIL_ENVIO_CORREO_CONTACTENOS_REP_DOMINICANA;
                                    break;
                                case 244: destinoAdminContactenos = LAFISE.Common.Backend.Constants.EMAIL_ENVIO_CORREO_CONTACTENOS_GENERICO;
                                    break;
                            }
                        }
                    }
                    break;
                case TipoPlantilla.MarcaAuto:
                    asunto = "Solicitud de Préstamo de Auto # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.PrestamoPersonal:
                    asunto = "Solicitud de Préstamo personal # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.PrestamoHipotecario:
                    asunto = "Solicitud de Préstamo hipotecario # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.TarjetaCredito:
                    asunto = "Solicitud Tarjeta de crédito # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.TarjetaDebito:
                    asunto = "Solicitud Tarjeta débito # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.PrestamoEducativo:
                    asunto = "Solicitud de Préstamo educativo # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.AperturaCuenta:
                    asunto = "Solicitud de Apertura de cuenta # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.SeguroVidaAccidente:
                    asunto = "Solicitud Seguro de vida y accidente # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.SeguroVehicular:
                    asunto = "Solicitud Seguro vehicular # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.Reclamos:
                    asunto = "Solicitud de Reclamo # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.TerminacionRechazado:
                    asunto = "Lafise- Estado solicitud " + tipoFormulario + " # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.TerminacionAprobado:
                    asunto = "Lafise- Estado solicitud " + tipoFormulario + " # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
                case TipoPlantilla.TerminacionPreaprobado:
                    asunto = "Lafise- Estado solicitud " + tipoFormulario + " # " + numeroSolicitud;
                    nombrePlantilla = tipoPlantilla.GetStringValue();
                    break;
            }

            try
            {
                if (!string.IsNullOrEmpty(destinoAdminContactenos))
                {
                    destinatarios = new List<MailAddress>();
                    IParametrosBusiness parametrosBusiness = new ParametrosBusiness();
                    var result = parametrosBusiness.ObtenerParametroRutaNoAsincrono(destinoAdminContactenos);
                    if (result.SuccessfulOperation)
                    {
                        if (result.Result.Contains(","))
                        {

                            var correoAdmin = result.Result.Split(',');
                            foreach (var item in correoAdmin)
                            {
                                destinatarios.Add(new MailAddress(item));
                            }
                        }
                        else
                        {
                            destinatarios.Add(new MailAddress(result.Result));
                        }
                    }
                }

                if (attachment == null)
                {
                    attachment = new List<Attachment>();
                }
                if (nombrePlantilla != "Default" && destinatarios.Count > 0)
                    mailHelper.Enviar(Host.SMTPServer, null, Host.SMTPUsername, Host.SMTPPassword, Host.EnableSMTPSSL, Host.SMTPUsername, destinatarios, asunto, RutaPlantillas, nombrePlantilla, parametros, null, attachment, null, null, true);
            }
            catch (Exception ex)
            {

            }

        }
    }
}
