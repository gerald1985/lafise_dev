﻿/*==========================================================================
Archivo:            PaisController
Descripción:        Servicio de Pais                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para país.
    /// </summary>
    [AllowAnonymous]
    public class PaisController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IPaisBusiness
        /// </summary>
        public IPaisBusiness _PaisBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public PaisController()
        {
            _PaisBusiness = new PaisBusiness();
        }
        /// <summary>
        /// Ver todos los paises
        /// </summary>
        /// <returns>Lista país</returns>
        [HttpGet]
        public async Task<ICollection<PaisDto>> VerTodos()
        {
            string pais = UserController.Instance.GetCurrentUserInfo().Profile.City;

            if (pais != null && !pais.Equals(""))
            {
                var paisLista = new List<PaisDto>();
                var result = await _PaisBusiness.VerPaisPorId(Convert.ToInt32(pais));
                if (result.SuccessfulOperation)
                {
                    paisLista.Add(result.Result);
                }
                return paisLista;
            }
            else
            {
                var result = await _PaisBusiness.VerTodos();
                return result.Result;
            }
        }
        /// <summary>
        /// Ver país por identificador 
        /// </summary>
        /// <param name="id">Identificador de país</param>
        /// <returns>Pais</returns>
        [HttpGet]
        public async Task<PaisDto> VerPaisPorId(int id)
        {
            var result = await _PaisBusiness.VerPaisPorId(id);
            return result.Result;
        }
    }
}
