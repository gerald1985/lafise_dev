﻿/*==========================================================================
Archivo:            SuscripcionBoletinController
Descripción:        Servicio de SuscripcionBoletin                  
Autor:              steven.echavarria                          
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para suscripción a boletin
    /// </summary>
    [AllowAnonymous]
    public class SuscripcionBoletinController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz ISuscripcionBoletinBusiness
        /// </summary>
        public ISuscripcionBoletinBusiness _SuscripcionBoletinBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public SuscripcionBoletinController()
        {
            _SuscripcionBoletinBusiness = new SuscripcionBoletinBusiness();
        }

        /// <summary>
        /// Crear suscripción a boletin
        /// </summary>
        /// <param name="suscripcion">SuscripcionBoletin</param>
        /// <returns>SuscripcionBoletin</returns>
        [HttpPost]
        public async Task<SuscripcionBoletinDto> CrearSuscripcionBoletin(SuscripcionBoletinDto suscripcion)
        {
            var result = await _SuscripcionBoletinBusiness.Crear(suscripcion);
            return result.Result;
        }

        /// <summary>
        /// Metodo que valida que el email ya esté registrado en las suscripciones
        /// </summary>
        /// <param name="suscripcion">entidad suscripcion</param>
        /// <returns>suscripcion</returns>
        [HttpPost]
        public async Task<SuscripcionBoletinDto> ValidarSuscripcionPorEmail(SuscripcionBoletinDto suscripcion)
        {
            var result = await _SuscripcionBoletinBusiness.ValidarSuscripcionPorEmail(suscripcion);
            return result.Result;
        }
    }
}
