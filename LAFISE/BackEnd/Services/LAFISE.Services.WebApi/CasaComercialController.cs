﻿/*==========================================================================
Archivo:            CasaComercialController
Descripción:        Servicio casa comercial                   
Autor:              Juan.Hincapie                           
Fecha de creación:  9/8/2015 10:41:57 AM                                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/

#region Referencias
using ARKIX.Business;
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
#endregion
namespace LAFISE.Services.WebApi
{

    /// <summary>
    /// Clase encargada de exponer los métodos para las casas comerciales.
    /// </summary>

    [AllowAnonymous]
    public class CasaComercialController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz ICasaComercialBusiness
        /// </summary>
        public ICasaComercialBusiness _CasaComercialBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public CasaComercialController()
        {
            _CasaComercialBusiness = new CasaComercialBusiness();
        }


        /// <summary>
        /// Método encargado de consultar las casas comerciales según los filtros de búsqueda
        /// </summary>
        /// <param name="filtros">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Lista con todas las casas comerciales.</returns>
        [HttpPost]
        public async Task<ICollection<CasaComercialFiltroDto>> ConsultarCasaComercialFiltros(CasaComercialDto filtros)
        {
            string paisUsuario = UserController.Instance.GetCurrentUserInfo().Profile.City;
            if (paisUsuario != null && !paisUsuario.Equals(""))
            {
                filtros.Paises = paisUsuario;
            }
            var result = await _CasaComercialBusiness.ConsultarCasaComercialFiltros(filtros);
            return result.Result;
        }


        /// <summary>
        /// Método encargado de consultar los modelos según los filtros de las marcas
        /// </summary>
        /// <param name="marcas">id de las marcas separadas por comas</param>
        /// <returns>lista de modelos agrupados por marca</returns>
        [HttpGet]
        public async Task<ICollection<CasaComercialModeloMarcaFiltroDto>> ConsultarCasaComercialModeloMarcaFiltros(string marcas)
        {
            var result = await _CasaComercialBusiness.ConsultarCasaComercialModeloMarcaFiltros(marcas);
            return result.Result;
        }


        /// <summary>
        /// Metodo que valida que el nombre de la casa comercial sea unico.
        /// </summary>
        /// <param name="casacomercial">entidad casa comercial</param>
        /// <returns>Entidad CasaComercial</returns>
        [HttpPost]
        public async Task<CasaComercialDto> ValidarCasaComercialPorNombre(CasaComercialDto casacomercial)
        {
            var result = await _CasaComercialBusiness.ValidarCasaComercialPorNombre(casacomercial);
            return result.Result;
        }


        /// <summary>
        /// Método encargado de consultar el detalle de la casa comercial.
        /// </summary>
        /// <param name="Id">Identificador de la casa comercial.</param>
        /// <returns>Lista con el detalle de las casas comerciales. </returns>
        [HttpGet]
        public async Task<CasaComercialDto> ConsultarDetalleCasaComercial(long Id)
        {
            var result = await _CasaComercialBusiness.ConsultarDetalleCasaComercial(Id);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de crear las casas comerciales y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Entidad CasaComercial que se insertó.</returns>
        [HttpPost]
        public async Task<CasaComercialDto> CrearCasaComercial(CasaComercialDto casacomercial)
        {
            casacomercial.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _CasaComercialBusiness.CrearCasaComercial(casacomercial);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de actualizar las casas comerciales y sus relaciones.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>Entidad CasaComercial que se editó.</returns>
        [HttpPost]
        public async Task<CasaComercialDto> EditarCasaComercial(CasaComercialDto casacomercial)
        {
            casacomercial.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _CasaComercialBusiness.EditarCasaComercial(casacomercial);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de inactivar las casas comerciales.
        /// </summary>
        /// <param name="casacomercial">Dto con las propiedades de la casa comercial.</param>
        /// <returns>CasaComercialDto.</returns>
        [HttpPost]
        public async Task<CasaComercialDto> InactivarCasaComercial(CasaComercialDto casacomercial)
        {
            casacomercial.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _CasaComercialBusiness.InactivarCasaComercial(casacomercial);
            return result.Result;
        }


        /// <summary>
        /// Eliminar casa comercial
        /// </summary>
        /// <param name="id">identificador de la casa comercial</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<CasaComercialDto> EliminarCasaComercialPorId(long id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _CasaComercialBusiness.EliminarCasaComercialPorId(id, usuario);
            return result.Result;
        }


        /// <summary>
        /// Método encargado de retornar una casa comercial según su identificador.
        /// </summary>
        /// <param name="Id">Identificador de la casa comercial.</param>
        /// <returns>Entidad CasaComercial.</returns>
        [HttpGet]
        public async Task<CasaComercialDto> ConsultarCasasComercialPorId(long Id)
        {
            var result = await _CasaComercialBusiness.VerCasaComercialPorId(Id);
            return result.Result;
        }


        /// <summary>
        /// Método encargado de consultar las casas comerciales según el pais.
        /// </summary>
        /// <param name="idPais">Identificador del pais asociado a la casa comercial.</param>
        /// <returns>Lista de las casas comerciales.</returns>
        [HttpGet]
        public async Task<ICollection<CasaComercialDto>> ConsultarCasaComercialPorPais(long idPais)
        {
            var result = await _CasaComercialBusiness.ConsultarCasaComercialPorPais(idPais);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="Modelo">Identificador del modelo.</param>
        /// <returns>Entidad CasaComercial.</returns>
        [HttpGet]
        public async Task<ICollection<CasaComercialDto>> ConsultarCasaComercialPorModelo(long Modelo)
        {
            var result = await _CasaComercialBusiness.VerCasaComercialPorModelo(Modelo);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de retornar una casa comercial según el identificador del modelo.
        /// </summary>
        /// <param name="entidad">entidad CasaComercialModeloPaisDto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ICollection<CasaComercialModeloPaisDto>> ConsultarCasaComercialModeloPais(CasaComercialModeloPaisDto entidad)
        {
            var result = await _CasaComercialBusiness.ConsultarCasaComercialModeloPais(entidad);
            return result.Result;
        }
    }
}
