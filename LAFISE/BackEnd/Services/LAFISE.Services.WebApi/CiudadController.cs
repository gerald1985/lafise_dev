﻿/*==========================================================================
Archivo:            CiudadController
Descripción:        Servicio de Ciudad                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para ciudad.
    /// </summary>
    [AllowAnonymous]
    public class CiudadController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz ICiudadBusiness
        /// </summary>
        public ICiudadBusiness _CiudadBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public CiudadController()
        {
            _CiudadBusiness = new CiudadBusiness();
        }
        /// <summary>
        /// Ver todas las ciudades
        /// </summary>
        /// <returns>Lista de ciudades</returns>
        [HttpGet]
        public async Task<ICollection<CiudadDto>> VerTodos()
        {
            var result = await _CiudadBusiness.VerTodos();
            return result.Result;
        }
        /// <summary>
        /// Ver las ciudades por identificador
        /// </summary>
        /// <param name="id">identificador de la ciudad</param>
        /// <returns>Ciudad</returns>
        [HttpGet]
        public async Task<CiudadDto> VerCiudadPorId(int id)
        {
            var result = await _CiudadBusiness.VerCiudadPorId(id);
            return result.Result;
        }
        /// <summary>
        /// Ver las ciudades por departamento
        /// </summary>
        /// <param name="id">Identificador del departamento</param>
        /// <returns>Lista Ciudad</returns>
        [HttpGet]
        public async Task<ICollection<CiudadDto>> VerCiudadesPorDepartamento(int id)
        {
            var result = await _CiudadBusiness.VerCiudadesPorDepartamento(id);
            return result.Result;
        }

        /// <summary>
        /// Ver las ciudades por país
        /// </summary>
        /// <param name="listaPais">lista de los identificadores del país</param>
        /// <returns>Lista Ciudad</returns>
        [HttpPost]
        public async Task<ICollection<CiudadDto>> VerCiudadesPorPais(List<Int64> listaPais)
        {
            var result = await _CiudadBusiness.VerCiudadesPorPais(listaPais);
            return result.Result;
        }

        /// <summary>
        /// Ver las ciudades por departamento
        /// </summary>
        /// <param name="listaDepartamento">lista de los identificadores de los departamentos</param>
        /// <returns>Lista Ciudad</returns>
        [HttpPost]
        public async Task<ICollection<CiudadDto>> VerListaCiudadesPorDepartamento(List<Int64> listaDepartamento)
        {
            var result = await _CiudadBusiness.VerListaCiudadesPorDepartamento(listaDepartamento);
            return result.Result;
        }
    }
}
