﻿/*==========================================================================
Archivo:            GarantiaController
Descripción:        Servicio de Garantia                  
Autor:              steven.echavarria                          
Fecha de creación:  20/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http; 
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para garantia
    /// </summary>
    [AllowAnonymous]
    public class GarantiaController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IGarantiaBusiness
        /// </summary>
        public IGarantiaBusiness _GarantiaBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public GarantiaController()
        {
            _GarantiaBusiness = new GarantiaBusiness();
        }
        /// <summary>
        /// Ver todas las urbanizadoras activas
        /// </summary>
        /// <returns>Lista urbanizadora</returns>
        [HttpGet]
        public async Task<ICollection<GarantiaDto>> VerTodos()
        {
            var result = await _GarantiaBusiness.VerTodos();
            return result.Result;
        }
    }
}
