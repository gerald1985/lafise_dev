﻿/*==========================================================================
Archivo:            SolicitudController
Descripción:        Servicio para Solicitud                      
Autor:              paola.munoz                          
Fecha de creación:  14/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using LAFISE.Business.Interfaces;
using LAFISE.Common.Backend;
using System.Net.Mail;
using System.Web;
using System.IO;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Roles;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para solicitud
    /// </summary>

    [AllowAnonymous]
    public class SolicitudController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz ISolicitudBusiness
        /// </summary>
        public ISolicitudBusiness _SolicitudBusiness;

        /// <summary>
        /// Instancia de la interfaz IPrimaCategoriaPaisBusiness
        /// </summary>
        public IPrimaCategoriaPaisBusiness _PrimaCategoriaBusiness;

        /// <summary>
        /// Instancia de la interfaz ITipoPropiedadPlazoBusiness
        /// </summary>
        public ITipoPropiedadPlazoBusiness _TipoPropiedadPlazoBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public SolicitudController()
        {
            _SolicitudBusiness = new SolicitudBusiness();
            _PrimaCategoriaBusiness = new PrimaCategoriaPaisBusiness();
            _TipoPropiedadPlazoBusiness = new TipoPropiedadPlazoBusiness();
        }
        /// <summary>
        /// Consultar los formularios generales teniendo en cuenta la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        [HttpGet]
        public async Task<SolicitudDto> VerFormularioGeneralPorIdSolicitud(Int64 id)
        {
            var result = await _SolicitudBusiness.VerFormularioGeneralPorIdSolicitud(id);
            return result.Result;
        }
        /// <summary>
        /// Consultar los formularios seguro de vida teniendo en cuenta la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        [HttpGet]
        public async Task<SolicitudDto> VerFormularioSeguroVidaPorIdSolicitud(Int64 id)
        {
            var result = await _SolicitudBusiness.VerFormularioSeguroVidaPorIdSolicitud(id);
            return result.Result;
        }
        /// <summary>
        /// Consultar los formularios de reclamos teniendo en cuenta la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        [HttpGet]
        public async Task<SolicitudDto> VerFormularioReclamosPorIdSolicitud(Int64 id)
        {
            var result = await _SolicitudBusiness.VerFormularioReclamosPorIdSolicitud(id);
            return result.Result;
        }
        /// <summary>
        /// Consultar los formularios con referencias teniendo en cuenta la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        [HttpGet]
        public async Task<SolicitudDto> VerFormularioConReferenciasPorIdSolicitud(Int64 id)
        {
            var result = await _SolicitudBusiness.VerFormularioConReferenciasPorIdSolicitud(id);
            return result.Result;
        }
        /// <summary>
        /// Consultar los formularios de seguro vehicular teniendo en cuenta la solicitud
        /// </summary>
        /// <param name="id">Identificador de la solicitud</param>
        /// <returns>Solicitud</returns>
        [HttpGet]
        public async Task<SolicitudDto> VerFormularioSeguroVehicularPorIdSolicitud(Int64 id)
        {
            var result = await _SolicitudBusiness.VerFormularioSeguroVehicularPorIdSolicitud(id);
            return result.Result;
        }
        /// <summary>
        /// Crear la solicitud
        /// </summary>
        /// <param name="id">Solicitud</param>
        /// <returns>Solicitud</returns>
        [HttpPost]
        public async Task<SolicitudDto> CrearSolicitud(SolicitudDto solicitud)
        {
            var result = await _SolicitudBusiness.CrearSolicitud(solicitud);
            if (result.SuccessfulOperation)
            {
                try
                {
                    if (result.Result.MensajeValidacion == null)
                    {
                        EnvioCorreo(result.Result);
                    }
                }
                catch (Exception ex)
                {
                    result.Result.MensajeValidacion = Constants.MENSAJE_EMAIL;
                }
            }
            return result.Result;
        }

        /// <summary>
        /// Realizar el envio de correo de la solicitud
        /// </summary>
        /// <param name="solicitud">Solicitud</param>
        public void EnvioCorreo(SolicitudDto solicitud)
        {
            Hashtable parametros = new Hashtable();
            List<MailAddress> destinatario = new List<MailAddress>();
            TipoPlantilla tipoPlantilla = TipoPlantilla.Default;
            string tipoFormulario = string.Empty;
            ParametrosBusiness datoParametros = new ParametrosBusiness();
            string rol = "";

            string numeroSolicitud = solicitud.CodigoIdSolicitud;

            switch (solicitud.IdTipoSolicitud)
            {
                case 1:
                    tipoPlantilla = TipoPlantilla.MarcaAuto;
                    tipoFormulario = "Solicitud de Préstamo de Auto";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_MARCAAUTO).Result;
                    break;
                case 2:
                    tipoPlantilla = TipoPlantilla.PrestamoPersonal;
                    tipoFormulario = "Solicitud de Préstamo de Personal";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_PRESTAMOPERSONAL).Result;
                    break;
                case 3:
                    tipoPlantilla = TipoPlantilla.PrestamoHipotecario;
                    tipoFormulario = "Solicitud de Préstamo Hipotecario";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_PRESTAMOHIPOTECARIO).Result;
                    break;
                case 4:
                    tipoPlantilla = TipoPlantilla.TarjetaCredito;
                    tipoFormulario = "Solicitud de Tarjeta de Crédito";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_TARJETACREDITO).Result;
                    break;
                case 5:
                    tipoPlantilla = TipoPlantilla.TarjetaDebito;
                    tipoFormulario = "Solicitud de Tarjeta Débito";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_TARJETADEBITO).Result;
                    break;
                case 6:
                    tipoPlantilla = TipoPlantilla.PrestamoEducativo;
                    tipoFormulario = "Solicitud de Préstamo Educativo";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_PRESTAMOEDUCATIVO).Result;
                    break;
                case 7:
                    tipoPlantilla = TipoPlantilla.AperturaCuenta;
                    tipoFormulario = "Solicitud de Apertura de Cuenta";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_APERTURACUENTA).Result;
                    break;
                case 8:
                    tipoPlantilla = TipoPlantilla.SeguroVidaAccidente;
                    tipoFormulario = "Solicitud de Seguro de Vida y Accidentes";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_SEGUROVIDAACCIDENTE).Result;
                    break;
                case 9:
                    tipoPlantilla = TipoPlantilla.SeguroVehicular;
                    tipoFormulario = "Solicitud de Seguro Vehicular";
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_SEGUROVEHICULAR).Result;
                    break;
                case 10:
                    tipoPlantilla = TipoPlantilla.Reclamos;
                    rol = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.EMAIL_ENVIO_CORREO_RECLAMOS).Result;
                    break;
                default:
                    break;
            }

            if (solicitud.DatosPersonales != null && solicitud.DatosPersonales.Count > 0)
            {
                parametros.Add("[NOMBRE]", solicitud.DatosPersonales.FirstOrDefault().Nombre);
                parametros.Add("[APELLIDO]", solicitud.DatosPersonales.FirstOrDefault().Apellidos);
                parametros.Add("[TELEFONO]", solicitud.DatosPersonales.FirstOrDefault().Telefono);
                parametros.Add("[EMAIL]", solicitud.DatosPersonales.FirstOrDefault().Email);
                parametros.Add("[SEXO]", solicitud.DatosPersonales.FirstOrDefault().Masculino ? "Masculino" : "Femenino");
                parametros.Add("[NACIONALIDAD]", solicitud.DatosPersonales.FirstOrDefault().Nacionalidad);
                parametros.Add("[CIUDADRESIDENCIA]", solicitud.DatosPersonales.FirstOrDefault().CiudadResidencia);
                TipoIdentificacionBusiness tipoIdentificacion = new TipoIdentificacionBusiness();
                var tipoResult = tipoIdentificacion.VerPorIdNoAsincrono(solicitud.DatosPersonales.FirstOrDefault().IdTipoIdentificacion);
                if (tipoResult.SuccessfulOperation)
                {
                    parametros.Add("[TIPOIDENTIFICACION]", tipoResult.Result.Nombre);
                }
                parametros.Add("[NUMERO]", solicitud.DatosPersonales.FirstOrDefault().NumeroIdentificacion);
                parametros.Add("[PROFESION]", solicitud.DatosPersonales.FirstOrDefault().Profesion);
                destinatario.Add(new MailAddress(solicitud.DatosPersonales.FirstOrDefault().Email));
            }

            if (solicitud.DatosFinancieros != null && solicitud.DatosFinancieros.Count > 0)
            {
                parametros.Add("[ESCLIENTELAFISE]", solicitud.DatosFinancieros.FirstOrDefault().EsCliente ? "Si" : "No");
                parametros.Add("[POSEENOMINA]", solicitud.DatosFinancieros.FirstOrDefault().PoseeNomina ? "Si" : "No");
                parametros.Add("[POSEENEGOCIOPROPIO]", solicitud.DatosFinancieros.FirstOrDefault().NegocioPropio.HasValue ? solicitud.DatosFinancieros.FirstOrDefault().NegocioPropio.Value ? "Si" : "No" : "No");
                parametros.Add("[ESASALARIADO]", solicitud.DatosFinancieros.FirstOrDefault().Asalariado.HasValue ? solicitud.DatosFinancieros.FirstOrDefault().Asalariado.Value ? "Si" : "No" : "No");
                if (!string.IsNullOrEmpty(solicitud.DatosFinancieros.FirstOrDefault().PromedioIngresoMensualNombre))
                {
                    parametros.Add("[PROMEDIOINGRESO]", "<tr><td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Promedio de ingresos mensuales: </td>"
                                    + "<td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosFinancieros.FirstOrDefault().PromedioIngresoMensualNombre + " </td></tr>");
                }
                else
                {
                    parametros.Add("[PROMEDIOINGRESO]", "");
                }
                if (solicitud.IdTipoSolicitud == 8)
                {
                    parametros.Add("[ESZURDO]", solicitud.DatosFinancieros.FirstOrDefault().EsZurdo.HasValue ? solicitud.DatosFinancieros.FirstOrDefault().EsZurdo.Value ? "Si" : "No" : "No");
                    parametros.Add("[PESO]", solicitud.DatosFinancieros.FirstOrDefault().Peso);
                    parametros.Add("[ESTATURA]", solicitud.DatosFinancieros.FirstOrDefault().Estatura);
                }
                else if (solicitud.IdTipoSolicitud == 4 || solicitud.IdTipoSolicitud == 5)
                {
                    parametros.Add("[ENVIOESTADOCUENTA]", solicitud.DatosFinancieros.FirstOrDefault().EstadoCuenta.HasValue ? solicitud.DatosFinancieros.FirstOrDefault().EstadoCuenta.Value ? "Fisico" : "Electrónico" : "");
                    if (solicitud.DatosFinancieros.FirstOrDefault().EstadoCuenta.HasValue ? solicitud.DatosFinancieros.FirstOrDefault().EstadoCuenta.Value : false)
                    {
                        parametros.Add("[DIRECCION]", "<tr><td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Dirección: </td>"
                                        + "<td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosFinancieros.FirstOrDefault().Direccion + " </td></tr>");
                    }
                    else
                    {
                        parametros.Add("[DIRECCION]", "");
                    }
                }
            }

            if (solicitud.PlanFinanciacion != null && solicitud.PlanFinanciacion.Count > 0)
            {
                parametros.Add("[VALORBIEN]", string.IsNullOrEmpty(solicitud.PlanFinanciacion.FirstOrDefault().ValorBien.ToString()) ? "" : string.Format("{0:C}", solicitud.PlanFinanciacion.FirstOrDefault().ValorBien).Substring(1));
                parametros.Add("[MONTOFINANCIAR]", string.IsNullOrEmpty(solicitud.PlanFinanciacion.FirstOrDefault().MontoFinanciar.ToString()) ? "" : string.Format("{0:C}", solicitud.PlanFinanciacion.FirstOrDefault().MontoFinanciar).Substring(1));
                if (solicitud.IdTipoSolicitud == 2 || solicitud.IdTipoSolicitud == 6)
                {
                    GarantiaBusiness garantia = new GarantiaBusiness();
                    if (solicitud.PlanFinanciacion.FirstOrDefault().IdGarantia.HasValue)
                    {
                        var garantiaResult = garantia.VerGarantiaPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdGarantia.Value);
                        if (garantiaResult.SuccessfulOperation)
                        {
                            parametros.Add("[GARANTIA]", garantiaResult.Result.Nombre);
                            if (garantiaResult.Result.Nombre.Equals(Constants.INFORMACION_GARANTIA))
                            {
                                parametros.Add("[CODEUDOR]", CargarDatoCodeudor(solicitud));
                            }
                            else
                            {
                                parametros.Add("[CODEUDOR]", "");
                            }
                        }
                    }
                }
                parametros.Add("[PRIMAANTICIPO]", string.IsNullOrEmpty(solicitud.PlanFinanciacion.FirstOrDefault().PrimaAnticipo.ToString()) ? "" : string.Format("{0:C}", solicitud.PlanFinanciacion.FirstOrDefault().PrimaAnticipo).Substring(1));
                parametros.Add("[PLAZO]", solicitud.PlanFinanciacion.FirstOrDefault().Plazo);
                if (solicitud.IdTipoSolicitud == 1)
                {
                    parametros.Add("[AUTOUSADO]", solicitud.PlanFinanciacion.FirstOrDefault().AutoUsado ? "Si" : "No");
                    CategoriaBusiness categoria = new CategoriaBusiness();
                    if (solicitud.PlanFinanciacion.FirstOrDefault().IdCategoria.HasValue)
                    {
                        var categoriaResult = categoria.VerCategoriaPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdCategoria.Value);
                        if (categoriaResult.SuccessfulOperation)
                        {
                            parametros.Add("[CATEGORIA]", categoriaResult.Result.Nombre);

                        }
                    }
                    MarcaAutoBusiness marcaAuto = new MarcaAutoBusiness();
                    if (solicitud.PlanFinanciacion.FirstOrDefault().IdMarcaAuto.HasValue)
                    {
                        var marcaAutoresult = marcaAuto.VerMarcaAutoPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdMarcaAuto.Value);
                        if (marcaAutoresult.SuccessfulOperation)
                        {
                            parametros.Add("[MARCAAUTO]", marcaAutoresult.Result.Marca);
                        }
                    }
                    ModeloAutoBusiness modeloAuto = new ModeloAutoBusiness();
                    if (solicitud.PlanFinanciacion.FirstOrDefault().IdModelo.HasValue)
                    {
                        var modeloAutoresult = modeloAuto.VerModeloAutoPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdModelo.Value);
                        if (modeloAutoresult.SuccessfulOperation)
                        {
                            parametros.Add("[MODELO]", modeloAutoresult.Result.Modelo);
                        }
                    }
                    CasaComercialBusiness casaComercial = new CasaComercialBusiness();
                    if (solicitud.PlanFinanciacion.FirstOrDefault().IdCasaComercial.HasValue)
                    {
                        var casaComercialResult = casaComercial.VerCasaComercialPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdCasaComercial.Value);
                        if (casaComercialResult.SuccessfulOperation)
                        {
                            parametros.Add("[CASACOMERCIAL]", casaComercialResult.Result.Nombre);
                        }
                    }
                }
                else if (solicitud.IdTipoSolicitud == 3)
                {
                    string esConstruccionPropia = solicitud.PlanFinanciacion.FirstOrDefault().ConstruccionPropia.HasValue ? solicitud.PlanFinanciacion.FirstOrDefault().ConstruccionPropia.Value ? "Si" : "No" : "No";
                    parametros.Add("[CONSTRUCCIONPROPIA]", esConstruccionPropia);
                    if (esConstruccionPropia.Equals("No"))
                    {
                        parametros.Add("[CONSTRUCCIONPROPIADATA]", CargarDatosConstruccion(solicitud));
                    }
                    else
                    {
                        parametros.Add("[CONSTRUCCIONPROPIADATA]", "");
                    }
                }
                else if (solicitud.IdTipoSolicitud == 6)
                {
                    parametros.Add("[COSTOPROGRAMA]", string.IsNullOrEmpty(solicitud.PlanFinanciacion.FirstOrDefault().CostoPrograma.ToString()) ? "" : string.Format("{0:C}", solicitud.PlanFinanciacion.FirstOrDefault().CostoPrograma).Substring(1));
                }
            }

            if (solicitud.Referencias != null && solicitud.Referencias.Count > 0)
            {
                parametros.Add("[NOMBREREFERENCIA1]", solicitud.Referencias.FirstOrDefault().Nombre);
                parametros.Add("[APELLIDOREFERENCIA1]", solicitud.Referencias.FirstOrDefault().Apellidos);
                parametros.Add("[TELEFONOREFERENCIA1]", solicitud.Referencias.FirstOrDefault().Telefono);
                parametros.Add("[NOMBREREFERENCIA2]", solicitud.Referencias.LastOrDefault().Nombre);
                parametros.Add("[APELLIDOREFERENCIA2]", solicitud.Referencias.LastOrDefault().Apellidos);
                parametros.Add("[TELEFONOREFERENCIA2]", solicitud.Referencias.LastOrDefault().Telefono);
                SucursalBusiness sucursal = new SucursalBusiness();
                if (solicitud.DatosFinancieros.FirstOrDefault().IdSucursal.HasValue)
                {
                    var sucursalResult = sucursal.VerPorIdNoAsincrono(solicitud.DatosFinancieros.FirstOrDefault().IdSucursal.Value);
                    if (sucursalResult.SuccessfulOperation)
                    {
                        parametros.Add("[SUCURSAL]", sucursalResult.Result.Nombre);
                    }
                }
            }

            if (solicitud.Siniestro != null && solicitud.Siniestro.Count > 0)
            {
                parametros.Add("[FECHA]", solicitud.Siniestro.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"));
                parametros.Add("[NUMEROPOLIZA]", solicitud.Siniestro.FirstOrDefault().NumeroPoliza);
                List<Int64> pais = new List<long>();
                foreach (SiniestroPaisDto siniestroPais in solicitud.Siniestro.FirstOrDefault().SiniestroPais)
                {
                    pais.Add(siniestroPais.IdPais);
                }
                List<Int64> ciudad = new List<long>();
                foreach (SiniestroCiudadDto siniestroCiud in solicitud.Siniestro.FirstOrDefault().SiniestroCiudad)
                {
                    ciudad.Add(siniestroCiud.IdCiudad);
                }
                PaisBusiness paisBuss = new PaisBusiness();
                string paisDetalle = "";
                if (pais.Count > 0)
                {
                    var paisresult = paisBuss.VerPaisPorListaIdNoAsincrono(pais);
                    if (paisresult.SuccessfulOperation)
                    {
                        foreach (PaisDto paisD in paisresult.Result)
                        {
                            paisDetalle = paisDetalle + paisD.Nombre + ", ";
                        }
                    }
                }

                CiudadBusiness ciudadBuss = new CiudadBusiness();
                string ciudadDetalle = "";
                if (ciudad.Count > 0)
                {
                    var ciudadresult = ciudadBuss.VerCiudadesPorCiudadNoAsincrono(ciudad);
                    if (ciudadresult.SuccessfulOperation)
                    {
                        foreach (CiudadDto ciudadD in ciudadresult.Result)
                        {
                            ciudadDetalle = ciudadDetalle + ciudadD.Nombre + ", ";
                        }
                    }
                }
                parametros.Add("[PAIS]", paisDetalle);
                parametros.Add("[CIUDAD]", ciudadDetalle);
                parametros.Add("[DETALLE]", solicitud.Siniestro.LastOrDefault().Detalle);
            }

            if (solicitud.DatosBien != null && solicitud.DatosBien.Count > 0)
            {
                parametros.Add("[VALORBIEN]", string.Format("{0:C}", solicitud.DatosBien.FirstOrDefault().ValorBien).Substring(1));
                TipoVehiculoBusiness tipoVehiculo = new TipoVehiculoBusiness();
                if (solicitud.DatosBien.FirstOrDefault().IdTipoVehiculo != null)
                {
                    var tipoVeh = tipoVehiculo.VerPorIdNoAsincrono(solicitud.DatosBien.FirstOrDefault().IdTipoVehiculo.Value);
                    if (tipoVeh.SuccessfulOperation)
                    {
                        parametros.Add("[TIPOVEHICULO]", tipoVeh.Result.Nombre);
                    }

                    MarcaAutoBusiness marcaAuto = new MarcaAutoBusiness();
                    var marca = marcaAuto.VerMarcaAutoPorIdNoAsincrono(solicitud.DatosBien.FirstOrDefault().IdMarcaAuto.Value);
                    if (marca.SuccessfulOperation)
                    {
                        parametros.Add("[MARCAAUTO]", marca.Result.Marca);
                    }
                }
            }

            List<Attachment> attachment = null;
            if (solicitud.Soportes != null && solicitud.Soportes.Count > 0)
            {
                attachment = new List<Attachment>();
                foreach (SoporteDto archivo in solicitud.Soportes)
                {
                    if (!string.IsNullOrEmpty(archivo.RutaFisica))
                        attachment.Add(new Attachment((archivo.RutaFisica)));
                }
            }

            List<MailAddress> destinatarioAdmin = new List<MailAddress>();
            var portal = datoParametros.ObtenerParametroRutaNoAsincrono(Constants.IDENTIFICADOR_PORTAL).Result;
            RoleController rolController = new RoleController();
            var infoUsuario = rolController.GetUsersByRole(Convert.ToInt32(portal), rol);
            foreach (UserInfo info in infoUsuario)
            {
                if (info.Profile.City.Equals(solicitud.IdPais.ToString()))
                {
                    if (!string.IsNullOrEmpty(info.Email))
                    {
                        destinatarioAdmin.Add(new MailAddress(info.Email));
                    }
                }
            }

            MailHelper.EnvioCorreo(tipoPlantilla, parametros, true, tipoFormulario, numeroSolicitud, null, destinatarioAdmin, attachment);
            MailHelper.EnvioCorreo(tipoPlantilla, parametros, false, tipoFormulario, numeroSolicitud, null, destinatario, attachment);
        }
        /// <summary>
        /// Editar la solicitud
        /// </summary>
        /// <param name="id">Solicitud</param>
        /// <returns>Solicitud</returns>
        [HttpPost]
        public async Task<SolicitudDto> EditarSolicitud(SolicitudDto solicitud)
        {
            var result = await _SolicitudBusiness.EditarSolicitud(solicitud, base.UserInfo.Email.Replace("@lafise.com",""));
            if (result.SuccessfulOperation)
            {
                try
                {
                    if (solicitud.IdEstado.ToString().Equals(Constants.ESTADO_RECHAZADO) ||
                        solicitud.IdEstado.ToString().Equals(Constants.ESTADO_DESEMBOLSADO) ||
                        solicitud.IdEstado.ToString().Equals(Constants.ESTADO_TERMINACIONPROCESO))
                    {
                        EnvioCorreoGestores(result.Result);
                    }
                }
                catch (Exception ex)
                {
                    result.Result.MensajeValidacion = Constants.MENSAJE_EMAIL;
                }
            }
            return result.Result;
        }

        /// <summary>
        /// Enviar los correo despues de terminar el proceso de los gestores
        /// </summary>
        /// <param name="solicitud"></param>
        private void EnvioCorreoGestores(SolicitudDto solicitud)
        {
            Hashtable parametros = new Hashtable();
            List<MailAddress> destinatario = new List<MailAddress>();
            TipoPlantilla tipoPlantilla = TipoPlantilla.Default;
            string tipoFormulario = string.Empty;
            string numeroSolicitud = solicitud.CodigoIdSolicitud;

            switch (solicitud.IdEstado.ToString())
            {
                case Constants.ESTADO_RECHAZADO:
                    tipoPlantilla = TipoPlantilla.TerminacionRechazado;
                    break;
                case Constants.ESTADO_DESEMBOLSADO:
                    tipoPlantilla = TipoPlantilla.TerminacionAprobado;
                    break;
                case Constants.ESTADO_TERMINACIONPROCESO:
                    tipoPlantilla = TipoPlantilla.TerminacionPreaprobado;
                    break;
            }

            switch (solicitud.IdTipoSolicitud)
            {
                case 1:
                    tipoFormulario = "marca de auto";
                    break;
                case 2:
                    tipoFormulario = "préstamo personal";
                    break;
                case 3:
                    tipoFormulario = "préstamo hipotecario";
                    break;
                case 4:
                    tipoFormulario = "tarjeta de crédito";
                    break;
                case 5:
                    tipoFormulario = "tarjeta de débito";
                    break;
                case 6:
                    tipoFormulario = "préstamo educativo";
                    break;
                case 7:
                    tipoFormulario = "apertura de cuenta";
                    break;
                case 8:
                    tipoFormulario = "seguro de vida y accidentes";
                    break;
                case 9:
                    tipoFormulario = "seguro vehicular";
                    break;
                case 10:
                    tipoFormulario = "reclamos";
                    break;
                default:
                    break;
            }

            parametros.Add("[FORMULARIO]", tipoFormulario + " # " + numeroSolicitud);

            var result = _SolicitudBusiness.VerDatosPersonales(solicitud.Id);
            if (result.SuccessfulOperation)
            {
                destinatario.Add(new MailAddress(result.Result.Email));
                MailHelper.EnvioCorreo(tipoPlantilla, parametros, false, tipoFormulario, numeroSolicitud, null, destinatario);
            }
        }
        /// <summary>
        /// Ver la gestion de  la solicitud
        /// </summary>
        /// <param name="id">Identificador de los estados separados por coma</param>
        /// <returns>Lista de Solicitud</returns>
        [HttpPost]
        public async Task<ICollection<SolicitudFiltroDto>> VerGestorSolicitud(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.VerGestorSolicitud(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de consultar el porcentaje de prima por categoria y país
        /// </summary>
        /// <param name="primaCategoriaPais">entidad prima categoria pais</param>
        /// <returns>Entidad PrimaCategoriaPaisDto</returns>
        [HttpPost]
        public PrimaCategoriaPaisDto VerPorcentajePrimaPorCategoriaPais(PrimaCategoriaPaisDto primaCategoriaPais)
        {
            var result = _PrimaCategoriaBusiness.VerPorcentajePrimaPorCategoriaPais(primaCategoriaPais);
            return result.Result != null ? result.Result : new PrimaCategoriaPaisDto();
        }

        /// <summary>
        /// Método encargado de consultar el plazo de un tipo de propiedad
        /// </summary>
        /// <param name="idTipoPropiedad">identificador del tipo de propiedad</param>
        /// <returns>Entidad TipoPropiedadPlazoDto</returns>
        [HttpGet]
        public TipoPropiedadPlazoDto VerPlazoPorTipoPropiedad(long idTipoPropiedad)
        {
            var result = _TipoPropiedadPlazoBusiness.VerPlazoPorTipoPropiedad(idTipoPropiedad);
            return result.Result;
        }

        /// <summary>
        /// Metodo que se encarga de armar la información del codeudor
        /// </summary>
        /// <param name="solicitud">Solicitud creada</param>
        /// <returns>String codeudor</returns>
        public string CargarDatoCodeudor(SolicitudDto solicitud)
        {
            TipoIdentificacionBusiness tipoIdentificacion = new TipoIdentificacionBusiness();
            var tipoResult = tipoIdentificacion.VerPorIdNoAsincrono(solicitud.DatosPersonales.LastOrDefault().IdTipoIdentificacion);
            string tipoIdent = "";
            if (tipoResult.SuccessfulOperation)
            {
                tipoIdent = tipoResult.Result.Nombre;
            }
            string genero = "Masculino";
            if (!solicitud.DatosPersonales.LastOrDefault().Masculino)
            {
                genero = "Femenino";
            }

            string dato = "<tr><td style='border-bottom:1px solid #5cba55;color:#5cba55; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold; padding:20px 10px 10px;' colspan='2'>Datos del Codeudor</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Nombre:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosPersonales.LastOrDefault().Nombre + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Apellido:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosPersonales.LastOrDefault().Apellidos + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Teléfono:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosPersonales.LastOrDefault().Telefono + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Email:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosPersonales.LastOrDefault().Email + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Sexo:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + genero + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Nacionalidad:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosPersonales.LastOrDefault().Nacionalidad + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family: trebuchet ms, arial, helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Tipo de identificación:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + tipoIdent + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Número:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosPersonales.LastOrDefault().NumeroIdentificacion + "</td></tr>"
                        + "<tr><td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Profesión:</td>"
                        + "<td style='color:#575757; font-family:Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + solicitud.DatosPersonales.LastOrDefault().Profesion + "</td></tr>";

            return dato;
        }

        /// <summary>
        /// Cargar los datos cuando no es construccion propia
        /// </summary>
        /// <param name="solicitud"></param>
        /// <returns></returns>
        public string CargarDatosConstruccion(SolicitudDto solicitud)
        {
            string tipo = "";
            string urbanizad = "";
            string categ = "";
            TipoPropiedadBusiness tipoPropiedad = new TipoPropiedadBusiness();
            if (solicitud.PlanFinanciacion.FirstOrDefault().IdTipoPropiedad.HasValue)
            {
                var tipoPropiedadresult = tipoPropiedad.VerPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdTipoPropiedad.Value);
                if (tipoPropiedadresult.SuccessfulOperation)
                {
                    tipo = tipoPropiedadresult.Result.Tipo;
                }
            }
            UrbanizadoraBusiness urbanizadora = new UrbanizadoraBusiness();
            if (solicitud.PlanFinanciacion.FirstOrDefault().IdUrbanizadora.HasValue)
            {
                var urbanizadoraResult = urbanizadora.VerUrbanizadoraPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdUrbanizadora.Value);
                if (urbanizadoraResult.SuccessfulOperation)
                {
                    urbanizad = urbanizadoraResult.Result.Nombre;
                }
            }
            CategoriaBusiness categoria = new CategoriaBusiness();
            if (solicitud.PlanFinanciacion.FirstOrDefault().IdCategoria.HasValue)
            {
                var categoriaResult = categoria.VerCategoriaPorIdNoAsincrono(solicitud.PlanFinanciacion.FirstOrDefault().IdCategoria.Value);
                if (categoriaResult.SuccessfulOperation)
                {
                    categ = categoriaResult.Result.Nombre;

                }
            }
            string dato = "<tr><td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Categoría:</td>"
                          + "<td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + categ + "</td></tr>"
                          + "<tr><td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Tipo de propiedad:</td>"
                          + "<td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + tipo + "</td></tr>";
            if (!string.IsNullOrEmpty(urbanizad))
            {
                dato = dato + "<tr><td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-weight:bold; font-size:15px; padding:8px;'>Urbanizadora:</td>"
                    + "<td style='color:#575757; font-family: Trebuchet MS, Arial, Helvetica, sans-serif; font-size:15px; padding:8px; text-align:left;'>" + urbanizad + "</td></tr>";
            }

            return dato;
        }

        /// <summary>
        /// Consultar los datos para exportar las marcas auto
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarMarcaAutoDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarMarcaAutoDto>> CargarExportarMarcaAuto(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarMarcaAuto(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Consultar los datos para exportar prestamos personales
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarPrestamoPersonalDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarPrestamoPersonalDto>> CargarExportarPrestamoPersonal(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarPrestamoPersonal(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Consultar los datos para exportar prestamos hipotecarios
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarPrestamoHipotecarioDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarPrestamoHipotecarioDto>> CargarExportarPrestamoHipotecario(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarPrestamoHipotecario(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Consultar los datos para exportar tarjeta de credito o debito
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarTarjetaCreditoDebitoDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarTarjetaCreditoDebitoDto>> CargarExportarTarjetaCreditoDebito(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarTarjetaCreditoDebito(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Consultar los datos para exportar prestamo educativo
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarPrestamoEducativoDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarPrestamoEducativoDto>> CargarExportarPrestamoEducativo(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarPrestamoEducativo(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Consultar los datos para exportar apertura de cuenta
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarPrestamoEducativoDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarAperturaCuentaDto>> CargarExportarAperturaCuenta(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarAperturaCuenta(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Consultar los datos para exportar seguro de vida y accidente
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarSeguroVidaAccidenteDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarSeguroVidaAccidenteDto>> CargarExportarSeguroVidaAccidente(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarSeguroVidaAccidente(solicitudCondicion);
            return result.Result;
        }

        /// Consultar los datos para exportar seguro vehicular
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarSeguroVehicularDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarSeguroVehicularDto>> CargarExportarSeguroVehicular(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarSeguroVehicular(solicitudCondicion);
            return result.Result;
        }

        /// Consultar los datos para exportar reclamos
        /// </summary>
        /// <param name="id">Condiciones filtro</param>
        /// <returns>ExportarReclamosDto</returns>
        [HttpPost]
        public async Task<ICollection<ExportarReclamosDto>> CargarExportarReclamos(SolicitudCondicionesDto solicitudCondicion)
        {
            solicitudCondicion.IdPais = VerPaisUsuario();
            var result = await _SolicitudBusiness.CargarExportarReclamos(solicitudCondicion);
            return result.Result;
        }

        /// <summary>
        /// Metodo para consultar el identificador del país del usuario autenticado
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Int64 VerPaisUsuario()
        {
            Int64 idPais = -1;
            string pais = UserController.Instance.GetCurrentUserInfo().Profile.City;
            if (!string.IsNullOrEmpty(pais))
            {
                idPais = Convert.ToInt64(pais);
            }
            return idPais;
        }
    }
}