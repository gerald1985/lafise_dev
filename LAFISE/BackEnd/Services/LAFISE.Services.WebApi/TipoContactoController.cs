﻿/*==========================================================================
Archivo:            TipoContactoController
Descripción:        Controlador de TipoContacto               
Autor:              paola.munoz                         
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http; 
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los métodos para los TipoContacto
    /// </summary>
    [AllowAnonymous]
    public class TipoContactoController : DnnApiController
    {
           /// <summary>
        /// Instancia de la interfaz ITipoContactoBusiness
        /// </summary>
        public ITipoContactoBusiness _TipoContactoBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public TipoContactoController()
        {
            _TipoContactoBusiness = new TipoContactoBusiness();
        }

        /// <summary>
        /// Ver todos los TipoContacto 
        /// </summary>
        /// <returns>Lista de TipoContacto</returns>
        [HttpGet]
        public async Task<ICollection<TipoContactoDto>> VerTodos()
        {
            var result = await _TipoContactoBusiness.VerTodos();
            return result.Result;
        }
    }
}
