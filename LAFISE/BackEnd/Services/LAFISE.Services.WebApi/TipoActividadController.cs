﻿/*==========================================================================
Archivo:            TipoActividadController
Descripción:        Servicio Tipo Actividad                  
Autor:              juan.hincapie                           
Fecha de creación:  7/10/2015                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using DotNetNuke.Web.Api;

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Servicio Tipo Actividad  
    /// </summary>
    /// 
    [AllowAnonymous]
    public class TipoActividadController : DnnApiController
    {
        private ITipoActividadBusiness tipoActividadBusiness
        {
            get
            {
                return new TipoActividadBusiness();
            }
        }

        /// <summary>
        /// Consultar todos los tipos Actividad
        /// </summary>
        /// <returns>lista tipo vehiculo</returns>
        [HttpGet]
        public async Task<ICollection<TipoActividadDto>> VerTodos()
        {
            var result = await tipoActividadBusiness.VerTodosGrid();

            return result.Result;
        }
    }
}
