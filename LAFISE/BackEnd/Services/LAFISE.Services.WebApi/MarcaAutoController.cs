﻿/*==========================================================================
Archivo:            MarcaAutoController
Descripción:        Servicio de marca auto                   
Autor:              Juan.Hincapie                           
Fecha de creación:  9/8/2015 10:41:57 AM                                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
#region Referencias
using ARKIX.Business;
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para las Marca de auto.
    /// </summary>
    [AllowAnonymous]
    public class MarcaAutoController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IMarcaAutoBusiness
        /// </summary>
        public IMarcaAutoBusiness _MarcaAutoBusiness;

        /// <summary>
        /// Instancia de la interfaz IMarcaAutoBusiness
        /// </summary>
        public IModeloAutoBusiness _ModeloAutoBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public MarcaAutoController()
        {
            _MarcaAutoBusiness = new MarcaAutoBusiness();
            _ModeloAutoBusiness = new ModeloAutoBusiness();
        }

        /// <summary>
        /// Método encargado de consultar todas las marcas de auto.
        /// </summary>
        /// <returns>Lista con todas las marcas de auto.</returns>
        [HttpGet]
        public async Task<ICollection<MarcaAutoDto>> VerTodos()
        {
            var result = await _MarcaAutoBusiness.VerTodos();
            return result.Result;
        }

        /// <summary>
        /// Método encargado de consultar las marca de auto según los filtros de búsqueda
        /// </summary>
        /// <param name="marcaauto">Dto con las propiedades de la marca de auto</param>
        /// <returns>Lista con todas las marcas de auto</returns>
        [HttpPost]
        public async Task<ICollection<MarcaAutoFiltroDto>> ConsultarMarcasModelos(MarcaAutoDto filtros)
        {
            var result = await _MarcaAutoBusiness.VerMarcasModelos(filtros);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de crear las Marcas de Auto y la relación con sus respectivos modelos.
        /// </summary>
        /// <param name="Marca">Dto con las propiedades de la marca auto</param>
        /// <returns>Entidad MarcaAuto que se insertó.</returns>
        [HttpPost]
        public async Task<MarcaAutoDto> CrearMarcasModelos(MarcaAutoDto marca)
        {
            marca.Usuario = UserController.Instance.GetCurrentUserInfo().Username;
            var result = await _MarcaAutoBusiness.CrearMarcaAuto(marca);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de actualizar las Marcas de Auto y sus relaciones.
        /// </summary>
        /// <param name="Marca">Dto con las propiedades de la marca auto.</param>
        /// <returns>Entidad MarcaAuto que se editó.</returns>
        [HttpPost]
        public async Task<MarcaAutoDto> EditarMarcasModelos(MarcaAutoDto marca)
        {
            marca.Usuario = UserController.Instance.GetCurrentUserInfo().Username;
            var result = await _MarcaAutoBusiness.EditarMarcaAuto(marca);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de inactivar las Marcas de Auto.
        /// </summary>
        /// <param name="Marca">Dto con las propiedades de la marca auto.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        [HttpPost]
        public async Task<string> InactivarMarcaAuto(MarcaAutoDto marca)
        {
            marca.Usuario = UserController.Instance.GetCurrentUserInfo().Username;
            var result = await _MarcaAutoBusiness.Inactivar(marca);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de inactivar las Marcas de Auto.
        /// </summary>
        /// <param name="Marca">Dto con las propiedades de la marca auto.</param>
        /// <returns>true: Se Activo Correctamente; false: Se inactivo correctamente.</returns>
        [HttpPost]
        public async Task<string> InactivarModeloAuto(ModeloAutoDto modelo)
        {
            modelo.Usuario = UserController.Instance.GetCurrentUserInfo().Username;
            var result = await _MarcaAutoBusiness.InactivarModelo(modelo);
            return result.Result;
        }

        /// <summary>
        /// Metodo usado para eliminar MarcaAuto y sus modelos asociados
        /// </summary>
        /// <param name="id">Identificador de la marca auto.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        [HttpPost]
        public async Task<bool> EliminarMarcaAuto(long id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _MarcaAutoBusiness.EliminarMarcaAutoPorId(id,usuario);
            return result.Result;
        }

        /// <summary>
        /// Metodo usado para eliminar el Modelo de un auto.
        /// </summary>
        /// <param name="id">Identificador de el modelo auto.</param>
        /// <returns>true: Elimino Correctamente; false: No se logro eliminar.</returns>
        [HttpPost]
        public async Task<bool> EliminarModeloAuto(long id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await _MarcaAutoBusiness.EliminarModeloAutoPorId(id, usuario);
            return result.Result;
        }

        /// <summary>
        /// Método encargado de consultar el detalle de la marca auto.
        /// </summary>
        /// <param name="id">Identificador de la marca auto.</param>
        /// <returns>Lista con el detalle de las Marcas de Auto.</returns>
        [HttpGet]
        public async Task<ICollection<ModeloAutoDto>> ConsultarDetalleModelo(long id)
        {
            var result = await _MarcaAutoBusiness.ConsultarDetalleModelo(id);
            return result.Result;
        }

        /// <summary>
        /// Método que retorna las marcas de auto activas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ICollection<MarcaAutoDto>> ObtenerMarcasActivas()
        {
            var result = await _MarcaAutoBusiness.ObtenerMarcasActivas();
            return result.Result;
        }

        /// <summary>
        /// Método que retorna las marcas de auto activas
        /// </summary>
        /// <param name="pathIdPais">identificador del pais</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ICollection<MarcaAutoDto>> ObtenerMarcasActivasPorPais(string pathIdPais)
        {
            var result = await _MarcaAutoBusiness.ObtenerMarcasActivasPorPais(pathIdPais);
            return result.Result;
        }

        [HttpGet]
        public async Task<MarcaAutoDto> ConsultarRelaciones(long Id)
        {
            var result = await _MarcaAutoBusiness.ValidarRelaciones(Id);
            return result.Result;
        }

        [HttpGet]
        public async Task<MarcaAutoDto> ConsultarRelacionesModelos(long Id)
        {
            var result = await _MarcaAutoBusiness.ValidarRelacionesModelos(Id);
            return result.Result;
        }



        [HttpPost]
        public async Task<MarcaAutoDto> ConsultarPorNombre(MarcaAutoDto dtoTipoPropiedad)
        {
            var result = await _MarcaAutoBusiness.ValidarMarcaUnica(dtoTipoPropiedad.Id, dtoTipoPropiedad.Marca);
            return result.Result;
        }

        /// <summary>
        /// Método usado para cargar todas las marcas según sus ids
        /// </summary>
        /// <param name="listaMarcas">lista con el identificador de la marcas</param>
        /// <returns>Lista de marcas</returns>
        [HttpPost]
        public async Task<ICollection<MarcaAutoDto>> VerMarcasPorId(List<Int64> listaMarcas)
        {
            var result = await _MarcaAutoBusiness.VerMarcasPorId(listaMarcas);
            return result.Result;
        }


        /// <summary>
        /// Metodo para ver todas los modelos autos por marca
        /// </summary>
        /// <param name="Marca">Id marca</param>
        /// <returns>Lista de modelos</returns>
        [HttpGet]
        public async Task<ICollection<ModeloAutoDto>> VerModelosPorMarca(long Marca)
        {
            var result = await _ModeloAutoBusiness.VerModelosPorMarca(Marca);
            return result.Result;
        }

    }
}
