﻿using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para Cotizador
    /// </summary>
    [AllowAnonymous]
    public class CotizadorController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz ICotizadorBusiness
        /// </summary>
        public ICotizadorBusiness _CotizadorBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public CotizadorController()
        {
            _CotizadorBusiness = new CotizadorBusiness();
        }

        /// <summary>
        /// Crear el registro de 
        /// </summary>
        /// <param name="cotizador">Cotizador</param>
        /// <returns>Cotizador</returns>
        [HttpPost]
        public async Task<String> saveCustomer(CotizadorDto cotizador)
        {   
            if (IsValidEmail(cotizador.Email))
            {
                cotizador.FechaCreacion = DateTime.Now;
                var result = await _CotizadorBusiness.Crear(cotizador);
                if (result.Result != null)
                {
                    return "OK";
                } else
                {
                    return null;
                }
            } else
            {
                return "Email no válido, ingrese un correo válido para poder contactarle";
            }
            
        }

        bool IsValidEmail(string email)
        {
            try
            {
                bool valid_email = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (valid_email)
                {
                    var addr = new System.Net.Mail.MailAddress(email);
                    return addr.Address == email;
                } else
                {
                    return false;
                }
                
            }
            catch
            {
                return false;
            }
        }

    }
}