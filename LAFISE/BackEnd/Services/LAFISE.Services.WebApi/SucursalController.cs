﻿/*==========================================================================
Archivo:            SucursalController
Descripción:         Servicio de sucursal                  
Autor:              paola.munoz                           
Fecha de creación:  7/10/2015                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
#region Referencia
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
#endregion


namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para sucursal
    /// </summary>
    [AllowAnonymous]
    public class SucursalController : DnnApiController
    {
        private ISucursalBusiness sucursalBusiness
        {
            get
            {
                return new SucursalBusiness();
            }
        }

        /// <summary>
        /// Consultar Todas las sucursales
        /// </summary>
        /// <returns>Lista de sucursales</returns>
        [HttpGet]
        public async Task<ICollection<SucursalDto>> VerTodos()
        {
            var result = await sucursalBusiness.VerTodosGrid();

            return result.Result;
        }

        /// <summary>
        /// Consultar Todas las sucursales por pais
        /// </summary>
        /// <returns>Lista de sucursales</returns>
        [HttpGet]
        public async Task<ICollection<SucursalDto>> VerTodosPorPais(string url)
        {
            var result = await sucursalBusiness.VerTodosPorPais(url);

            return result.Result;
        }

        /// <summary>
        /// Editar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        [HttpPost]
        public async Task<SucursalDto> EditarSucursal(SucursalDto sucursal)
        {
            sucursal.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await sucursalBusiness.Editar(sucursal);

            return result.Result;
        }
        /// <summary>
        /// Inactivar sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>bool</returns>
        [HttpPost]
        public async Task<bool> InactivarSucursal(SucursalDto sucursal)
        {
            sucursal.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await sucursalBusiness.Inactivar(sucursal);

            return result.Result;
        }
        /// <summary>
        /// Crear sucursal
        /// </summary>
        /// <param name="sucursal">Sucursal</param>
        /// <returns>Sucursal</returns>
        [HttpPost]
        public async Task<SucursalDto> CrearSucursal(SucursalDto sucursal)
        {
            sucursal.Usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await sucursalBusiness.Crear(sucursal);

            return result.Result;
        }
        /// <summary>
        /// Eliminar sucursal
        /// </summary>
        /// <param name="id">Identificador de la sucursal</param>
        /// <returns>bool</returns>
        [HttpGet]
        public async Task<bool> EliminarSucursal(Int64 id)
        {
            string usuario = UserController.Instance.GetCurrentUserInfo().Username;

            var result = await sucursalBusiness.Eliminar(id, usuario);

            return result.Result;
        }
        /// <summary>
        /// Consultar filtro
        /// </summary>
        /// <param name="sucursal">SucursalFiltroDto</param>
        /// <returns>SucursalFiltroDto</returns>
        [HttpPost]
        public async Task<ICollection<SucursalFiltroDto>> ConsultarSucursalFiltro(SucursalFiltroDto sucursal)
        {
            string pais = UserController.Instance.GetCurrentUserInfo().Profile.City;
            if (pais != null && !pais.Equals(""))
            {
                sucursal.NombrePais = pais;
            }
            var result = await sucursalBusiness.SucursalFiltro(sucursal);

            return result.Result;
        }
        /// <summary>
        /// Consultar por identificador
        /// </summary>
        /// <param name="id">Identificdor de la sucursal</param>
        /// <returns>Sucursal</returns>
        [HttpGet]
        public async Task<SucursalDto> ConsultarPorId(Int64 id)
        {
            var result = await sucursalBusiness.VerPorId(id);

            return result.Result;
        }

        /// <summary>
        /// Consultar para la validacion por nombre
        /// </summary>
        /// <param name="sucursal">Entidad sucursal</param>
        /// <returns>Entidad que retornan</returns>
        [HttpPost]
        public async Task<SucursalDto> ConsultarPorNombre(SucursalDto sucursal)
        {
            var result = await sucursalBusiness.ValidarSucursalUnica(sucursal);
            return result.Result;
        }

        /// <summary>
        /// Consultar todos los sucursal activos
        /// </summary>
        /// <returns>lista sucursal activas</returns>
        [HttpGet]
        public async Task<ICollection<SucursalDto>> VerTodosActivos()
        {
            var result = await sucursalBusiness.VerTodosActivos();

            return result.Result;
        }
    }

}