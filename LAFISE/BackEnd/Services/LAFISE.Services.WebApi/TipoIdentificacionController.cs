﻿/*==========================================================================
Archivo:            TipoIdentificacionController
Descripción:        Servicio de TipoIdentificacion                     
Autor:              paola.munoz                         
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para tipo identificacion.
    /// </summary>

    [AllowAnonymous]
    public class TipoIdentificacionController: DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz ITipoIdentificacionBusiness
        /// </summary>
        public ITipoIdentificacionBusiness _TipoIdentificacionBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public TipoIdentificacionController()
        {
            _TipoIdentificacionBusiness = new TipoIdentificacionBusiness();
        }
        /// <summary>
        /// Ver todos los tipos de identificacion
        /// </summary>
        /// <returns>Lista de tipo de identificación</returns>
        [HttpGet]
        public async Task<ICollection<TipoIdentificacionDto>> VerTodos()
        {
            var result = await _TipoIdentificacionBusiness.VerTodos();
            return result.Result;
        }
        /// <summary>
        /// Ver tipo identificación teniendo en cuenta el identificador de pais
        /// </summary>
        /// <param name="path">url del sitio</param>
        /// <returns>Lista de tipo identificación</returns>
        [HttpGet]
        public async Task<ICollection<TipoIdentificacionDto>> VerTipoIdentificacionPorIdPais(string path)
        {
            var result = await _TipoIdentificacionBusiness.VerTipoIdentificacionPorIdPais(path);
            return result.Result;
        }
        /// <summary>
        /// Obtener Mascara de entrada por tipo de identificacion
        /// </summary>
        /// <param name="id">id de identificacion</param>
        /// <returns>mascara de entrada</returns>
        [HttpGet]
        public async Task<String> obtenerMascaraPorIdentificacion(Int64 id)
        {
            var result = await _TipoIdentificacionBusiness.obtenerMascaraPorIdentificacion(id);
            return result.Result;
        }
    }
}
