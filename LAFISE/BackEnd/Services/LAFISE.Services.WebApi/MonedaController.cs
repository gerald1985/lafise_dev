﻿/*==========================================================================
Archivo:            MonedaController
Descripción:        Servicio de Moneda                     
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos de moneda.
    /// </summary>
    [AllowAnonymous]
    public class MonedaController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IMonedaBusiness
        /// </summary>
        public IMonedaBusiness _MonedaBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public MonedaController()
        {
            _MonedaBusiness = new MonedaBusiness();
        }
        /// <summary>
        /// Ver todos las monedas
        /// </summary>
        /// <returns>Lista monedas</returns>
        [HttpGet]
        public async Task<ICollection<MonedaDto>> VerTodos()
        {
            var result = await _MonedaBusiness.VerTodos();
            return result.Result;
        }
        /// <summary>
        /// Ver moneda por identificador 
        /// </summary>
        /// <param name="id">Identificador de moneda</param>
        /// <returns>Moneda</returns>
        [HttpGet]
        public async Task<MonedaDto> VerPaisPorId(int id)
        {
            var result = await _MonedaBusiness.VerMonedaPorId(id);
            return result.Result;
        }

        /// <summary>
        /// Retorna el simbolo de la moneda seg'un portal
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet]
        public string ObtenerSimboloMoneda(string url)
        {
            var split = url.Split('/');
            if (split.Length > 3)
            {
                var result = _MonedaBusiness.ObtenerSimboloMoneda(split[3]);
                if (result != null)
                {
                    return result.Result;
                }
                return string.Empty;
            }
            return string.Empty;
        }
    }
}
