﻿/*==========================================================================
Archivo:            DepartamentoController
Descripción:        Servicio de Departamento                      
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                       
==========================================================================*/

#region Referencias
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para los departamentos.
    /// </summary>
    [AllowAnonymous]
    public class DepartamentoController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IDepartamentoBusiness
        /// </summary>
        public IDepartamentoBusiness _DepartamentoBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public DepartamentoController()
        {
            _DepartamentoBusiness = new DepartamentoBusiness();
        }
        /// <summary>
        /// Ver todos los departamentos
        /// </summary>
        /// <returns>Lista departamento</returns>
        [HttpGet]
        public async Task<ICollection<DepartamentoDto>> VerTodos()
        {
            var result = await _DepartamentoBusiness.VerTodos();
            return result.Result;
        }
        /// <summary>
        /// Ver departamento por el identificador
        /// </summary>
        /// <param name="id">Identificador de departameno</param>
        /// <returns>Departamento</returns>
        [HttpGet]
        public async Task<DepartamentoDto> VerDepartamentoPorId(int id)
        {
            var result = await _DepartamentoBusiness.VerDepartamentoPorId(id);
            return result.Result;
        }
        /// <summary>
        /// Ver departamento por país
        /// </summary>
        /// <param name="id">Identificador de país</param>
        /// <returns>Lista Departamento</returns>
        [HttpGet]
        public async Task<ICollection<DepartamentoDto>> VerDepartamentosPorPais(int id)
        {
            var result = await _DepartamentoBusiness.VerDepartamentosPorPais(id);
            return result.Result;
        }

        /// <summary>
        /// Ver lista de departamento por lista de país
        /// </summary>
        /// <param name="id">lista de país</param>
        /// <returns>Lista Departamento</returns>
        [HttpPost]
        public async Task<ICollection<DepartamentoDto>> VerListaDepartamentoPorPais(List<Int64> listaPais)
        {
            var result = await _DepartamentoBusiness.VerListaDepartamentoPorPais(listaPais);
            return result.Result;
        }
    }
}
