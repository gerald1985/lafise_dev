﻿/*==========================================================================
Archivo:            RangoSueldoController
Descripción:        Servicio de rango sueldo                   
Autor:              paola.munoz                           
Fecha de creación:  1/10/2015                                                               
Derechos Reservados (c), 2015 ARKIX S.A                                         
==========================================================================*/
#region Referencia
using DotNetNuke.Entities.Users;
using DotNetNuke.Web.Api;
using LAFISE.Business;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

namespace LAFISE.Services.WebApi
{
    /// <summary>
    /// Clase encargada de exponer los datos para rango sueldo
    /// </summary>
    [AllowAnonymous]
    public class RangoSueldoController : DnnApiController
    {
        /// <summary>
        /// Instancia de la interfaz IUrbanizadoraBusiness
        /// </summary>
        public IRangoSueldoBusiness _RangoSueldoBusiness;

        /// <summary>
        /// Constructor
        /// </summary>
        public RangoSueldoController()
        {
            _RangoSueldoBusiness = new RangoSueldoBusiness();
        }

        /// <summary>
        /// Editar rango sueldo
        /// </summary>
        /// <param name="rangoSueldo">rango sueldo y rango sueldo detalle</param>
        /// <returns>rango sueldo</returns>
        [HttpPost]
        public async Task<RangoSueldoDto> EditarRangoSueldo(RangoSueldoDto rangoSueldo)
        {
            rangoSueldo.Usuario = UserController.Instance.GetCurrentUserInfo().Username;
            var result = await _RangoSueldoBusiness.Editar(rangoSueldo);
            return result.Result;
        }
        /// <summary>
        /// Consultar por filtro
        /// </summary>
        /// <param name="pais">Identificadores de país concatenados por coma</param>
        /// <returns>Lista de RangoSueldoFiltroDto</returns>
        [HttpGet]
        public async Task<ICollection<RangoSueldoFiltroDto>> ConsultarRangoSueldoFiltro(string pais)
        {
            string paisUsuario = UserController.Instance.GetCurrentUserInfo().Profile.City;
            if (paisUsuario != null && !paisUsuario.Equals(""))
            {
                pais = paisUsuario;
            }
            var result = await _RangoSueldoBusiness.VerRangoSueldoFiltro(pais);
            return result.Result;
        }

        /// <summary>
        /// Consultar Rango Sueldo por pais
        /// </summary>
        /// <param name="url">Url</param>
        /// <returns>Lista de rango sueldo por pais</returns>
        [HttpGet]
        public async Task<ICollection<RangoSueldoPaisDto>> ConsultarRangoSueldoPais(string url)
        {
            var result = await _RangoSueldoBusiness.VerRangoSueldoPais(url);
            return result.Result;
        }


        /// <summary>
        /// Consultar detalle rango sueldo por filtro
        /// </summary>
        /// <param name="id"identificador rango sueldo></param>
        /// <returns>detalle rango sueldo</returns>
        [HttpGet]
        public async Task<ICollection<RangoSueldoDto>> ConsultarDetalleRangoSueldo(long id)
        {
            var result = await _RangoSueldoBusiness.ConsultarDetalleRangoSueldo(id);
            return result.Result;
        }

    }
}
