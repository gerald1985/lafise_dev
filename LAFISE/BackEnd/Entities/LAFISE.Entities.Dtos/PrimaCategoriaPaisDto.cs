﻿/*==========================================================================
Archivo:            PrimaCategoriaPaisDto
Descripción:        Relacion de categoría y país para sacar el porcentaje de prima                      
Autor:              steven.echavarria                          
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using System;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a PrimaCategoriaPais
    /// </summary>
    public class PrimaCategoriaPaisDto
    {
        /// <summary>
        /// Identificador de la prima categoría país (clave primaria)
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de la categoria (clave foranea)
        /// </summary>
        public Int64 IdCategoria { get; set; }
        /// <summary>
        /// Identificador del país (clave foranea)
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Porcentaje prima relacionado a categoría por país
        /// </summary>
        public Decimal PorcentajePrima { get; set; }
        /// <summary>
        /// Ruta del portal con el cual se consulta el id del país
        /// </summary>
        public String Path { get; set; }
    }
}
