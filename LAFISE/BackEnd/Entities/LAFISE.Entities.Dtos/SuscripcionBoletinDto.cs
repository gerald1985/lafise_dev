﻿/*==========================================================================
Archivo:            SuscripcionBoletinDto
Descripción:        Suscripción boletin                      
Autor:              steven.echavarria                          
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                         
==========================================================================*/

#region Referencias
using System;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto Correspondiente a la entidad SuscripcionBoletin
    /// </summary>
    public class SuscripcionBoletinDto
    {
        /// <summary>
        /// Identificador de la suscripción (clave primaria)
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre del suscriptor
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Email del suscriptor
        /// </summary>
        public String Email { get; set; }
        /// <summary>
        /// Fecha de creación de la solicitud
        /// </summary>
        public DateTime FechaCreacion { get; set; }
        /// <summary>
        /// Identificador de país
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
        /// <summary>
        /// Guarda la url donde se encuentra el modulo
        /// </summary>
        public string PathUrl { get; set; }

    }
}
