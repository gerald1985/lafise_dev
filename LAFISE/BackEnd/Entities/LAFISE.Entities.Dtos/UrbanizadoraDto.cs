/*==========================================================================
Archivo:            UrbanizadoraDto
Descripción:        Administrador de urbanizadora                    
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad Urbanizadora
    /// </summary>
    public class UrbanizadoraDto
    {
        /// <summary>
        /// Identificador de la urbanizadora
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de la urbanizadora
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Identificador del país
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Indica si la urbanizadora se encuentra activa
        /// </summary>
        public Boolean Activo { get; set; }
        /// <summary>
        /// Navegación con la lista de tipo propiedad
        /// </summary>
        public ICollection<TipoPropiedadDto> ListaPropiedades { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string mensajeError { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}