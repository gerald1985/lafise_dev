﻿/*==========================================================================
Archivo:            ExportarPrestamoEducativoDto
Descripción:        Dto para capturar la exportación de prestamo educativo                     
Autor:              paola.munoz                        
Fecha de creación:  17/12/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

using System;
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente al exportar marca auto
    /// </summary>
    public class ExportarPrestamoEducativoDto
    {
        /// <summary>
        /// Número de la solicitud
        /// </summary>
        public string NumeroSolicitud { get; set; }
        /// <summary>
        /// Nombre completo del que envio la solicitud
        /// </summary>
        public string NombreCompleto { get; set; }
        /// <summary>
        /// Estado de la solicitud
        /// </summary>
        public string EstadoSolicitud { get; set; }
        /// <summary>
        /// Fecha de Creacion de la solicitud
        /// </summary>
        public string FechaCreacion { get; set; }
        /// <summary>
        /// Fecha de Actualizacion  de la solicitud
        /// </summary>
        public string FechaActualizacion { get; set; }
        /// <summary>
        /// Nombre del Pais de la solicitud
        /// </summary>
        public string NombrePais { get; set; }
        /// <summary>
        /// Telefono del que envio la solicitud
        /// </summary>
        public string Telefono { get; set; }
        /// <summary>
        /// Email del que envio la solicitud
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Sexo del que envio la solicitud
        /// </summary>
        public string Sexo { get; set; }
        /// <summary>
        /// Nacionalidad del que envio la solicitud
        /// </summary>
        public string Nacionalidad { get; set; }
        /// <summary>
        /// Ciudad Residencia del que envio la solicitud
        /// </summary>
        public string CiudadResidencia { get; set; }
        /// <summary>
        /// Tipo de Identificacion del que envio la solicitud
        /// </summary>
        public string TipoIdentificacion { get; set; }
        /// <summary>
        /// Numero de Identificacion del que envio la solicitud
        /// </summary>
        public string NumeroIdentificacion { get; set; }
        /// <summary>
        /// Profesion del que envio la solicitud
        /// </summary>
        public string Profesion { get; set; }
        /// <summary>
        /// Es Cliente 
        /// </summary>
        public string EsCliente { get; set; }
        /// <summary>
        /// Posee Nomina
        /// </summary>
        public string PoseeNomina { get; set; }
        /// <summary>
        /// Negocio Propio
        /// </summary>
        public string NegocioPropio { get; set; }
        /// <summary>
        /// Asalariado
        /// </summary>
        public string Asalariado { get; set; }
        /// <summary>
        /// Promedio Ingreso Mensual
        /// </summary>
        public string PromedioIngresoMensualNombre { get; set; }
        /// <summary>
        /// Soporte Negocio Propio concatenado con coma
        /// </summary>
        public string SoporteNegocioPropio { get; set; }
        /// <summary>
        /// Soporte Ingresos concatenado con coma
        /// </summary>
        public string SoporteIngresos { get; set; }
        /// <summary>
        /// Costo Programa
        /// </summary>
        public decimal CostoPrograma { get; set; }
        /// <summary>
        /// Garantia
        /// </summary>
        public string Garantia { get; set; }
        /// <summary>
        ///Plazo
        /// </summary>
        public Int32 Plazo { get; set; }
        /// <summary>
        /// Costo Programa
        /// </summary>
        public decimal MontoFinanciar { get; set; }
        /// <summary>
        ///Nombre Codeudor
        /// </summary>
        public string NombreCodeudor { get; set; }
        /// <summary>
        ///Apellido Codeudor
        /// </summary>
        public string ApellidoCodeudor { get; set; }
        /// <summary>
        ///Telefono Codeudor
        /// </summary>
        public string TelefonoCodeudor { get; set; }
        /// <summary>
        ///Email Codeudor
        /// </summary>
        public string EmailCodeudor { get; set; }
        /// <summary>
        ///Sexo Codeudor
        /// </summary>
        public string SexoCodeudor { get; set; }
        /// <summary>
        ///Nacionalidad Codeudor
        /// </summary>
        public string NacionalidadCodeudor { get; set; }
        /// <summary>
        ///Ciudad Residencia Codeudor
        /// </summary>
        public string CiudadResidenciaCodeudor { get; set; }
        /// <summary>
        ///Tipo Identificacion Codeudor
        /// </summary>
        public string TipoIdentificacionCodeudor { get; set; }
        /// <summary>
        ///Profesion  Codeudor
        /// </summary>
        public string ProfesionCodeudor { get; set; }
        /// <summary>
        ///Comentario Inicial
        /// </summary>
        public string ComentarioInicial { get; set; }
        /// <summary>
        ///Estado Inicial
        /// </summary>
        public string EstadoInicial { get; set; }
        /// <summary>
        ///Comentario
        /// </summary>
        public string Comentario { get; set; }
        /// <summary>
        ///Estado Historico
        /// </summary>
        public string EstadoHistorico { get; set; }
        /// <summary>
        ///Tipo Contacto
        /// </summary>
        public string TipoContacto { get; set; }
    }
}
