﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LAFISE.Entities.Dtos
{
    public class PROC_CISCO_PaisesDto
    {
        public string ABV { get; set; }
        public string NombrePais { get; set; }
    }
}
