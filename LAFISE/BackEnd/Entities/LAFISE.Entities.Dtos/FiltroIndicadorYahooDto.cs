﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    public class FiltroIndicadorYahooDto
    {
        /// <summary>
        /// Identificador del Dto
        /// </summary>
        public string Simbolo { get; set; }
        public string FechaInicial { get; set; }
        public string FechaFinal { get; set; }
    }
}
