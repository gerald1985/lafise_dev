/*==========================================================================
Archivo:            UrbanizadoraTipoDto
Descripción:        Tipo de urbanizadora relacionada con urbanizadora                 
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad UrbanizadoraTipo
    /// </summary>
    public class UrbanizadoraTipoDto
    {
        /// <summary>
        /// Identificador de tipo de urbanizadorada
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de urbanizadora
        /// </summary>
        public Int64 IdUrbanizadora { get; set; }
        /// <summary>
        /// Identificador de tipo de propiedad
        /// </summary>
        public Int64 IdTipoPropiedad { get; set; }
    }
}