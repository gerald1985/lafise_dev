/*==========================================================================
Archivo:            CasaComercialDto
Descripción:        Administrador de casas comerciales para cada uno de los países en los que tiene presencia LAFISE                     
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto Correspondiente a la entidad CasaComericial.
    /// </summary>
    public class CasaComercialDto
    {
        /// <summary>
        /// Identificador Correspondiente a la entidad de CasaComercial.
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre Correspondiente a la entidad de CasaComercial.
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Id del pais asociado a la casa comercial.
        /// </summary>
        public long IdPais { get; set; }
        /// <summary>
        /// Nombre de los paises que se deben enviar para buscar las casas comerciales.
        /// </summary>
        public string Paises { get; set; }
        /// <summary>
        /// Tipo de vehículo nuevo correspondiente a la entidad de CasaComercial.
        /// </summary>
        public Boolean Nuevo { get; set; }
        /// <summary>
        /// Tipo de vehículo usado correspondiente a la entidad de CasaComercial.
        /// </summary>
        public Boolean Usado { get; set; }
        /// <summary>
        /// Fecha de creación correspondiente a la entidad de CasaComercial.
        /// </summary>
        public DateTime FechaCreacion { get; set; }
        /// <summary>
        /// Id de la marca asociada a la casa comercial.
        /// </summary>
        public long IdMarca { get; set; }
        /// <summary>
        /// Marca del vehículo asociada a la casa comercial.
        /// </summary>
        public string Marca { get; set; }
        /// <summary>
        /// Estado Activo o no correspondiente a la entidad de CasaComercial.
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Lista de modelos asociados a casa comercial.
        /// </summary>
        public ICollection<CasaComercialModeloAutoDto> listaModelos { get; set; }
        /// <summary>
        /// Lista de marcas asociados a casa comercial.
        /// </summary>
        public ICollection<MarcaAutoDto> listaMarcas { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string mensajeError { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}