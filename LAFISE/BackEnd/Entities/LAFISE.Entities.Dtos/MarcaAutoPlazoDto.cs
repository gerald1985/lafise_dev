﻿/*==========================================================================
Archivo:            MarcaAutoPlazoDto
Descripción:        Plazo que tendrá cada marca de auto
Autor:              juan.hincapie                          
Fecha de creación:  23/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/
#region Referencias
using System;
#endregion
namespace LAFISE.Entities.Dtos
{
    public class MarcaAutoPlazoDto
    {

        /// <summary>
        /// Identificador del TipoPropiedadPlazo (clave primaria)
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Identificador de la marca del auto (clave foranea)
        /// </summary>
        public long IdMarcaAuto { get; set; }
        /// <summary>
        /// Plazo al cual se debe pagar el prestamo 
        /// </summary>
        public int Plazo { get; set; }
    }
}
