﻿/*==========================================================================
Archivo:            SiniestroPaisDto
Descripción:        Relación de siniestro con país                 
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 4:21:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad  SiniestroPais
    /// </summary>
    public class SiniestroPaisDto
    {
        /// <summary>
        /// Identificador de siniestro país
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Identificador de siniestro
        /// </summary>
        public long IdSiniestro { get; set; }
        /// <summary>
        /// Identificador de país
        /// </summary>
        public long IdPais { get; set; }

        /// <summary>
        /// Nombre de país
        /// </summary>
        public string Pais { get; set; }

    }
}
