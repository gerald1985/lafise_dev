/*==========================================================================
Archivo:            PortalPaisDto
Descripción:        Administrador de los PortalPais                      
Autor:              paola.munoz                         
Fecha de creación:  10/11/2015 11:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad PortalPais
    /// </summary>
    public class PortalPaisDto
    {
        /// <summary>
        /// Identificador de PortalPais
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de PortalPais
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Sigla de PortalPais
        /// </summary>
        public String Sigla { get; set; }
        /// <summary>
        /// Identificador del país
        /// </summary>
        public Nullable<Int64> IdPais { get; set; }
        /// <summary>
        /// Sigla de ZonaHoraria
        /// </summary>
        public String ZonaHoraria { get; set; }
    }
}