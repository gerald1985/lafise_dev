/*==========================================================================
Archivo:            MarcaAutoDto
Descripción:        Administrador de la marca de los autos        
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto Correspondiente a la entidad MarcaAuto
    /// </summary>
    public class MarcaAutoDto
    {
        /// <summary>
        /// Identificador correspondiente a la entidad MarcaAuto.
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre de la Marca correspondiente a la entidad MarcaAuto.
        /// </summary>
        public string Marca { get; set; }
        /// <summary>
        /// Estado activo o no correspondiente a la entidad MarcaAuto.
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Nombre del Modelo para realizar el filtro de la consulta.
        /// </summary>
        public string Modelo { get; set; }
        /// <summary>
        /// Fecha de Creación correspondiente a la entidad
        /// </summary>
        public DateTime FechaCreacion { get; set; }
        /// <summary>
        /// Listado de modelos asociados a la entidad de ModeloAuto.
        /// </summary>
        public ICollection<ModeloAutoDto> listaModelos { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string mensajeError { get; set; }
        /// <summary>
        /// Relacion de la solicitud
        /// </summary>
        public string RelacionSolicitud { get; set; }
        /// <summary>
        /// Relaciona casas comerciales
        /// </summary>
        public string CasasComercialesRelacionadas { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}