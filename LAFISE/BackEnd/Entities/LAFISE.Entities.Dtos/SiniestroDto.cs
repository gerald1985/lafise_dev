﻿/*==========================================================================
Archivo:            SiniestroDto
Descripción:        Administrador de los siniestro de las solicitudes                     
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 4:21:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad Siniestro
    /// </summary>
    public class SiniestroDto
    {
        /// <summary>
        /// Identificador de los siniestros
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Identificador de la solicitud
        /// </summary>
        public long IdSolicitud { get; set; }
        /// <summary>
        /// Numero de poliza
        /// </summary>
        public string NumeroPoliza { get; set; }
        /// <summary>
        /// Fecha del siniestro
        /// </summary>
        public System.DateTime Fecha { get; set; }
        /// <summary>
        /// Detalle del siniestro
        /// </summary>
        public string Detalle { get; set; }
        /// <summary>
        /// Navegación de siniestroCiudad 
        /// </summary>
        public ICollection<SiniestroCiudadDto> SiniestroCiudad { get; set; }
        /// <summary>
        /// Navegación de SiniestroPais
        /// </summary>
        public ICollection<SiniestroPaisDto> SiniestroPais { get; set; }
    }
}
