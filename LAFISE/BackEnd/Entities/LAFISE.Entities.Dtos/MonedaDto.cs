﻿/*==========================================================================
Archivo:            PaisDto
Descripción:        Lista de monedas                      
Autor:              steven echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A
==========================================================================*/

#region Referencias
using System;


#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad Moneda
    /// </summary>
    public class MonedaDto
    {
        /// <summary>
        /// Identificador de moneda
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de moneda
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Simbolo de moneda
        /// </summary>
        public String Simbolo { get; set; }
    }
}
