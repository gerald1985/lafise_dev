﻿/*==========================================================================
Archivo:            UrbanizadoraTipoPropiedadDto
Descripción:        Relacion de urbanizadora y tipo de propiedad                      
Autor:              steven.echavarria                          
Fecha de creación:  18/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System;
using System.Collections.Generic;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad de UrbanizadoraTipoPropiedad
    /// </summary>
    public class UrbanizadoraTipoPropiedadDto
    {
        /// <summary>
        /// Identificador de UrbanizadoraTipoPropiedad
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre del país
        /// </summary>
        public string Pais { get; set; }
        /// <summary>
        /// Identificador del país
        /// </summary>
        public Int64 idPais { get; set; }
        /// <summary>
        /// Nombre de la urbanizadora
        /// </summary>
        public string Urbanizadora { get; set; }
        /// <summary>
        /// Nombre del tipo de propiedad
        /// </summary>
        public string TipoPropiedad { get; set; }
        /// <summary>
        /// Indica si se encuentra activo
        /// </summary>
        public bool Activo { get; set; }
    }
}
