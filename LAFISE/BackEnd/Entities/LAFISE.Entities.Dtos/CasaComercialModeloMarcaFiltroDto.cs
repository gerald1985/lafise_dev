﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class CasaComercialModeloMarcaFiltroDto
    {
        /// <summary>
        /// identificador del modelo
        /// </summary>
        public long IdModelo { get; set; }
        /// <summary>
        /// Identificador de marca auto
        /// </summary>
        public long IdMarcaAuto { get; set; }
        /// <summary>
        /// Marca
        /// </summary>
        public string Marca { get; set; }
        /// <summary>
        /// Modelo
        /// </summary>
        public string Modelo { get; set; }
        /// <summary>
        /// Imagen
        /// </summary>
        public string Imagen { get; set; }
        /// <summary>
        /// Activo
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Listado de modelos asociados a la entidad de ModeloAuto.
        /// </summary>
        public ICollection<ModeloAutoDto> listaModelos { get; set; }
    }
}
