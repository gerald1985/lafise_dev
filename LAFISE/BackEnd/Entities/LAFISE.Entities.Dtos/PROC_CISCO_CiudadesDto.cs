﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LAFISE.Entities.Dtos
{
    public class PROC_CISCO_CiudadesDto
    {
        public long Cod_Ciudad { get; set; }
        public string Nombre_Ciudad { get; set; }
    }
}
