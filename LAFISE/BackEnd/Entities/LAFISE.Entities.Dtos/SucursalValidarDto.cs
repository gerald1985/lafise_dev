﻿/*==========================================================================
Archivo:            SucursalValidarDto
Descripción:        Dto para el manejo de la validación de la sucursal              
Autor:              paola.munoz                           
Fecha de creación:  07/10/2015                                             
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la  validacion de sucursal
    /// </summary>
    public class SucursalValidarDto
    {
        /// <summary>
        /// Contador de tipo de sucursales
        /// </summary>
        public Int32 Contador { get; set; }
        /// <summary>
        /// Concatenar el tipo sucursales con coma en el caso que exista, sino retorna null
        /// </summary>
        public string Nombre { get; set; }
    }
}
