﻿/*==========================================================================
Archivo:            MarcaModeloAutoDto
Descripción:        Administrador de marca modelo del auto              
Autor:              steven.echavarria                          
Fecha de creación:  18/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System;
using System.Collections.Generic; 
#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la relacioón de marca y modelos.
    /// </summary>
    public class MarcaModeloAutoDto
    {
        /// <summary>
        /// Identificador correspondiente a la entidad MarcaAuto.
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre correspondiente a la entidad MarcaAuto.  
        /// </summary>
        public String Marca { get; set; }
        /// <summary>
        /// Listado de modelos asociados a la marca del auto.
        /// </summary>
        public List<ModeloAutoDto> Modelos { get; set; }  
    }
}
