﻿/*==========================================================================
Archivo:            RangoSueldoIdRangoDto
Descripción:        Unificacion de los dto RangoSueldoDto y RangoSueldoDetalleDto                  
Autor:              paola.munoz                           
Fecha de creación:  01/10/2015 4:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a las entidades  RangoSueldoDto y RangoSueldoDetalleDto  
    /// </summary>
    public class RangoSueldoIdRangoDto
    {
        /// <summary>
        /// Entidad de rango sueldo
        /// </summary>
        public RangoSueldoDto RangoSueldo { get;set;}
        /// <summary>
        /// Lista de la entidad rango sueldo detalle
        /// </summary>
        public ICollection<RangoSueldoDetalleDto> RangoSueldoDetalle { get; set; }
    }
}
