﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class CasaComercialModeloPaisDto
    {
        /// <summary>
        /// Identificador de casa comercial
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Identificador del Modelo
        /// </summary>
        public long IdModelo { get; set; }
        /// <summary>
        /// Nombre casa comercial
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Identificador de pais
        /// </summary>
        public long IdPais { get; set; }
        /// <summary>
        /// Ruta del identificador del país
        /// </summary>
        public string pathIdPais { get; set; }
    }
}
