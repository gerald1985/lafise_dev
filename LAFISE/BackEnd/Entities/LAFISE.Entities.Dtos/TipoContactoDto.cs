﻿/*==========================================================================
Archivo:            TipoContactoDto
Descripción:        Tipo de Contacto para la gestion de los formularios   
Autor:              paola.munoz                           
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System; 
#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a TipoContacto
    /// </summary>
    public class TipoContactoDto
    {
        /// <summary>
        /// Identificador del TipoContacto
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre del TipoContacto
        /// </summary>
        public String Nombre { get; set; }
       
    }
}
