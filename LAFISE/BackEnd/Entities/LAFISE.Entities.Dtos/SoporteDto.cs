﻿/*==========================================================================
Archivo:            SoporteDto
Descripción:        Administrador de los adjuntos de las solicitudes                     
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 3:57:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad Soporte
    /// </summary>
    public class SoporteDto
    {
        /// <summary>
        /// Identificador de los soportes
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre del soporte
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Identificador del tipo de soporte
        /// </summary>
        public Int64 IdTipoSoporte { get; set; }
        /// <summary>
        /// Identificador de la solicitud
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Ruta del documento adjunto
        /// </summary>
        public string Ruta { get; set; }
        /// <summary>
        /// Ruta fisica del archivo
        /// </summary>
        public string RutaFisica { get; set; }
        /// <summary>
        /// Indica si los adjuntos es del codeudor
        /// </summary>
        public bool EsCodeudor { get; set; }

    }
}
