﻿/*==========================================================================
Archivo:            HistoricoComentarioDto
Descripción:        Administra el historico de los comentarios en la gestion de la solicitud                   
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 4:16:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente al Historico de los Comentarios
    /// </summary>
    public class HistoricoComentarioDto
    {
        /// <summary>
        /// Identificador del historico de comentarios
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de las solicitudes
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Identificador del estado
        /// </summary>
        public Int64 IdEstado { get; set; }
        /// <summary>
        /// Estado
        /// </summary>
        public string Estado { get; set; }
        /// <summary>
        /// Comentarios de la solicitud
        /// </summary>
        public string Comentario { get; set; }
        /// <summary>
        /// Fecha de creación del comentario
        /// </summary>
        public DateTime FechaCreacion { get; set; }

    }
}
