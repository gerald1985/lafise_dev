/*==========================================================================
Archivo:            RangoSueldoDetalleDto
Descripción:        Detalle del rango sueldo relacionado con la entidad rango sueldo                     
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad RangoSueldoDetalleDto
    /// </summary>
    public class RangoSueldoDetalleDto
    {
        /// <summary>
        /// Identificador de rango sueldo detalle
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de rango sueldo
        /// </summary>
        public Int64 IdRangoSueldo { get; set; }
        /// <summary>
        /// Minimo sueldo
        /// </summary>
        public Double MinSueldo { get; set; }
        /// <summary>
        /// Maximo sueldo
        /// </summary>
        public Double MaxSueldo { get; set; }
    }
}