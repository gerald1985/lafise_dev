/*==========================================================================
Archivo:            TipoPropiedadDto
Descripción:        Administrador del tipo de propiedad                    
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad TipoPropiedadDto
    /// </summary>
    public class TipoPropiedadDto
    {
        /// <summary>
        /// Identificador del tipo de propiedad
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre del tipo de propiedad
        /// </summary>
        public String Tipo { get; set; }
        /// <summary>
        /// Indica si el tipo de propiedad esta activo
        /// </summary>
        public Boolean Activo { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string mensajeError { get; set; }
        /// <summary>
        /// Relacion solicitud
        /// </summary>
        public string RelacionSolicitud { get; set; }
        /// <summary>
        /// Relacion urbanizadora
        /// </summary>
        public string UrbanizadorasRelacionadas { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}