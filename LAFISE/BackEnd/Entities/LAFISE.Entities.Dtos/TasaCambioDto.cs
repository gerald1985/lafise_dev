﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    public class TasaCambioDto
    {
        /// <summary>
        /// Identificador de la tasa de cambio
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Descripción de la tasa de cambio
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// Valor de la compra
        /// </summary>
        public string ValorCompra { get; set; }
        /// <summary>
        /// Valor de la venta
        /// </summary>
        public string ValorVenta { get; set; }
        /// <summary>
        /// Simbolo de la compra    
        /// </summary>
        public string SimboloCompra { get; set; }
        /// <summary>
        /// Simbolo de la venta
        /// </summary>
        public string SimboloVenta { get; set; }
        /// <summary>
        /// Activo
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Identificador del pais
        /// </summary>
        public long IdPais { get; set; }
        /// <summary>
        /// Guarda la url donde se encuentra el modulo
        /// </summary>
        public string PathUrl { get; set; }
    }
}
