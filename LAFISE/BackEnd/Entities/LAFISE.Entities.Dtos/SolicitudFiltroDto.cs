﻿/*==========================================================================
Archivo:            SolicitudFiltroDto
Descripción:        Consulta la gestion de las solicitudes teniendo en cuenta el estado                      
Autor:              paola.munoz                           
Fecha de creación:  14/10/2015 6:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad de consulta de filtro de solicitud
    /// </summary>
    public class SolicitudFiltroDto
    {
        /// <summary>
        /// Identificador de la solicitud
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Numero de la solicitud
        /// </summary>
        public string NumeroSolicitud { get; set; }
        /// <summary>
        /// Nombre Completo de quien diligencio la solicitud
        /// </summary>
        public string NombreCompleto { get; set; }
        /// <summary>
        /// Estado de la solicitud
        /// </summary>
        public string EstadoSolicitud { get; set; }
        /// <summary>
        /// Fecha de creación de la solicitud
        /// </summary>
        public string  FechaCreacion { get; set; }
        /// <summary>
        /// Fecha de actualización de la solicitud
        /// </summary>
        public string FechaActualizacion { get; set; }
        /// <summary>
        /// Procedencia de la solicitud - ejm facebook
        /// </summary>
        public string Procedencia { get; set; }
        /// <summary>
        /// solicitud personalizada para control de asignaciones de casos
        /// </summary>
        public string NumeroSolicitudProducto { get; set; }

    }
}
