﻿/*==========================================================================
Archivo:            IndicadorDto
Descripción:        Administrador de los indicadores        
Autor:              carlos.arboleda                           
Fecha de creación:  12/02/2016 04:49:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad Indicador
    /// </summary>
    public class IndicadorDto
    {
        /// <summary>
        /// Identificador del Dto
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre del indicador usado para mostrar en el grid
        /// </summary>
        public String NombreIndicador { get; set; }
        /// <summary>
        /// Símbolo del indicador usado para consultar los valores actuales en la API de Yahoo
        /// </summary>
        public String SimboloActual { get; set; }
        /// <summary>
        /// Símbolo del indicador usado para consultar los valores históricos en la API de Yahoo
        /// </summary>
        public String SimboloHistorico { get; set; }
        /// <summary>
        /// Bandera usada para especificar si el indicador está activo o no
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string MensajeError { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
      
    }
}
