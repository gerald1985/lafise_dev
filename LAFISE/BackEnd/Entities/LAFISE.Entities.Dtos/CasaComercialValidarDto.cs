﻿/*==========================================================================
Archivo:            CasaComercialValidarDto
Descripción:        Validar la casa comercial              
Autor:              Juan.Hincapie                           
Fecha de creación:  01/10/2015                                             
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    public class CasaComercialValidarDto
    {
        /// <summary>
        /// Contador de casas comerciales
        /// </summary>
        public Int32 Contador { get; set; }
        /// <summary>
        /// Nombre de casas comerciales
        /// </summary>
        public string Nombre { get; set; }

    }
}
