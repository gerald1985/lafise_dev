﻿/*==========================================================================
Archivo:            TipoActividadDto
Descripción:        Dto del tipo de Actividad              
Autor:              juan.hincapie                           
Fecha de creación:  06/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    public class TipoActividadDto
    {
        /// <summary>
        /// Identificador del tipo de actividad
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre del tipo de actividad
        /// </summary>
        public string Nombre { get; set; }
    }
}
