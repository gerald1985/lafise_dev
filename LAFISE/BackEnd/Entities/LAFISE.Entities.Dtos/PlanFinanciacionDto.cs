﻿/*==========================================================================
Archivo:            PlanFinanciacionDto
Descripción:        Administrador del plan de financiacion en los formularios de las solicitudes                     
Autor:              paola.munoz                        
Fecha de creación:  13/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad plan financiero de las solicitudes
    /// </summary>
    public class PlanFinanciacionDto
    {
        /// <summary>
        /// Identificador del plan financiero
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de las solicitudes
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Valor de bien en el plan financiero
        /// </summary>
        public Nullable<decimal> ValorBien { get; set; }
        /// <summary>
        /// Valor de bien en el plan financiero para el envío del correo
        /// </summary>
        public string ValorBienFormato { get; set; }
        /// <summary>
        /// Monto a financiar en el plan financiero
        /// </summary>
        public decimal MontoFinanciar { get; set; }
        /// <summary>
        /// Monto a financiar en el plan financiero para el envío del correo
        /// </summary>
        public string MontoFinanciarFormato { get; set; }
        /// <summary>
        /// Prima o anticipo del plan financiero
        /// </summary>
        public Nullable<decimal> PrimaAnticipo { get; set; }
        /// <summary>
        /// Prima o anticipo del plan financiero para el envío del correo
        /// </summary>
        public string PrimaAnticipoFormato { get; set; }
        /// <summary>
        /// Plazo del prestamo
        /// </summary>
        public int Plazo { get; set; }
        /// <summary>
        /// Si el una solicitud de auto indica si es usado 
        /// </summary>
        public Boolean AutoUsado { get; set; }
        /// <summary>
        /// Identificador de la categoria
        /// </summary>
        public Nullable<long> IdCategoria { get; set; }
        /// <summary>
        /// Nombre de la categoria
        /// </summary>
        public string Categoria { get; set; }
        /// <summary>
        /// Identificador de la marca del auto
        /// </summary>
        public Nullable<long> IdMarcaAuto { get; set; }
        /// <summary>
        /// Nombre de la marca del auto
        /// </summary>
        public string MarcaAuto { get; set; }
        /// <summary>
        /// Identificador del modelo
        /// </summary>
        public Nullable<long> IdModelo { get; set; }
        /// <summary>
        /// nombre del modelo
        /// </summary>
        public string Modelo { get; set; }
        /// <summary>
        /// Identificador de la casa comercial
        /// </summary>
        public Nullable<long> IdCasaComercial { get; set; }
        /// <summary>
        /// Nombre de la casa comercial
        /// </summary>
        public string CasaComercial { get; set; }
        /// <summary>
        /// Otra marca de auto si selecciona "otro" en marca auto
        /// </summary>
        public string OtroMarcaAuto { get; set; }
        /// <summary>
        /// Otro modelo si selecciona "otro" en modelo
        /// </summary>
        public string OtroModelo { get; set; }
        /// <summary>
        /// Otra casa comercial si selecciona "otro" en casa comercial
        /// </summary>
        public string OtroCasaComercial { get; set; }
        /// <summary>
        /// Identificador de la garantia
        /// </summary>
        public Nullable<long> IdGarantia { get; set; }
        /// <summary>
        /// nombre de la garantia
        /// </summary>
        public string Garantia { get; set; }
        /// <summary>
        /// Indica si es construccion propia
        /// </summary>
        public Nullable<bool> ConstruccionPropia { get; set; }
        /// <summary>
        /// Identificador de tipo propiedad
        /// </summary>
        public Nullable<long> IdTipoPropiedad { get; set; }
        /// <summary>
        /// nombre de tipo propiedad
        /// </summary>
        public string TipoPropiedad { get; set; }
        /// <summary>
        /// Identificador de urbanizadora
        /// </summary>
        public Nullable<long> IdUrbanizadora { get; set; }
        /// <summary>
        /// nombre de urbanizadora
        /// </summary>
        public string Urbanizadora { get; set; }
        /// <summary>
        /// Costo del programa
        /// </summary>
        public Nullable<decimal> CostoPrograma { get; set; }
        /// <summary>
        /// Costo del programa para el envío del correo
        /// </summary>
        public string CostoProgramaFormato { get; set; }
        /// <summary>
        /// Simbolo de la moneda
        /// </summary>
        public string Simbolo { get; set; }
        /// <summary>
        /// Simbolo de la moneda
        /// </summary>
        public string NombreEmpresaInmobiliaria { get; set; }
        /// <summary>
        /// Simbolo de la moneda
        /// </summary>
        public string NombreProyectoResidencial { get; set; }
        /// <summary>
        /// Direccion de vivienda
        /// </summary>
        public string DireccionVivienda { get; set; }
        /// <summary>
        /// Proposito de financiamiento Vivienda primaria, Inversion para alquiler
        /// </summary>
        public string PropositoCompra { get; set; }
    }
}
