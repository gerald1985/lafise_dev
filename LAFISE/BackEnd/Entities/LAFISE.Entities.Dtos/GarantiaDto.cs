﻿/*==========================================================================
Archivo:            GarantiaDto
Descripción:        Garantia que se tendra en cuenta en los formularios   
Autor:              paola.munoz                           
Fecha de creación:  17/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System; 
#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a Garantia
    /// </summary>
    public class GarantiaDto
    {
        /// <summary>
        /// Idntificador de garantia
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de garantia
        /// </summary>
        public String Nombre { get; set; }
    }
}
