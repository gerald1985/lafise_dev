﻿/*==========================================================================
Archivo:            MarcaAutoFiltroDto
Descripción:        Dto que ayuda a la consulta con filtros de marca auto                    
Autor:              Juan.Hincapie                           
Fecha de creación:  08/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la consulta de marca autos por filtros. 
    /// </summary>
    public class MarcaAutoFiltroDto
    {
        /// <summary>
        /// Identificador Correspondiente a la entidad de MarcaAuto.
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de la Correspondiente a la entidad de MarcaAuto.
        /// </summary>
        public string Marca { get; set; }
        /// <summary>
        /// Estado a activo o no Correspondiente a la entidad de MarcaAuto.
        /// </summary>
        public Boolean Activo { get; set; }
        /// <summary>
        /// Nombres de los modelos separados por "," correspondientes a la marca de auto.
        /// </summary>
        public string Modelo { get; set; }
        /// <summary>
        /// Listado de modelos asociados a la entidad de ModeloAuto.
        /// </summary>
        public ICollection<ModeloAutoDto> listaModelos { get; set; }

    }
}
