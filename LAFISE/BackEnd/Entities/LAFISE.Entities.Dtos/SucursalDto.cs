﻿/*==========================================================================
Archivo:            SucursalDto
Descripción:        Administrador de la sucursal                      
Autor:              paola.munoz                           
Fecha de creación:  30/09/2015 5:10:05 P.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad Sucursal
    /// </summary>
    public class SucursalDto
    {
        /// <summary>
        /// Identificador de la sucursal
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador del país
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Identificador del departamento
        /// </summary>
        public Int64 IdDepartamento { get; set; }
        /// <summary>
        /// Identificador de la ciudad
        /// </summary>
        public Int64 IdCiudad { get; set; }
        /// <summary>
        /// Identificador del tipo de sucursal
        /// </summary>
        public Int64 IdTipoSucursal { get; set; }
        /// <summary>
        /// Nombre de la sucursal
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Indica si la sucursal se encuentra activa
        /// </summary>
        public Boolean Activo { get; set; }

        /// <summary>
        /// Mensaje para verificar la relacion de la solicitud en la sucursal
        /// </summary>
        public string RelacionSolicitud { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}
