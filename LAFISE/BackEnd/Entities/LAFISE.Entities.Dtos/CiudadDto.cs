﻿/*==========================================================================
Archivo:            CiudadDto
Descripción:        Administrador de la tabla ciudad                     
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using System;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente de  Ciudad
    /// </summary>
    public class CiudadDto
    {
        /// <summary>
        /// Identificador de la ciudad (clave primaria)
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de la ciudad
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Identificador del departamento (clave foranea)
        /// </summary>
        public Int64 IdDepartamento { get; set; }
    }
}
