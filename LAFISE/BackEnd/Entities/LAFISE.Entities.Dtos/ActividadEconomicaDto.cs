﻿/*==========================================================================
Archivo:            ActividadEconomicaDto
Descripción:        Actividad economica que se tendra en cuenta en el formulario de seguro de vida y accidentes             
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 3:37:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a Actividad Economica
    /// </summary>
    public class ActividadEconomicaDto
    {
        /// <summary>
        /// Identificador de la actividad economica (clave primaria)
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador del tipo de actividad (clave foranea)
        /// </summary>
        public Int64 IdTipoActividad { get; set; }
        /// <summary>
        /// Nombre del tipo de actividad 
        /// </summary>
        public string TipoActividad { get; set; }
        /// <summary>
        /// Identificador de los datos financieros (clave foranea)
        /// </summary>
        public Int64 IdDatosFinancieros { get; set; }
        /// <summary>
        /// Otra actividad economica.
        /// </summary>
        public string OtraActividad { get; set; }

    }
}
