﻿/*==========================================================================
Archivo:            ExportarSeguroVidaAccidenteDto
Descripción:        Dto para capturar la exportación de Seguro Vida Accidente                     
Autor:              paola.munoz                        
Fecha de creación:  17/12/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente al exportar marca auto
    /// </summary>
    public class ExportarSeguroVidaAccidenteDto
    {
        /// <summary>
        /// Número de la solicitud
        /// </summary>
        public string NumeroSolicitud { get; set; }
        /// <summary>
        /// Nombre completo del que envio la solicitud
        /// </summary>
        public string NombreCompleto { get; set; }
        /// <summary>
        /// Estado de la solicitud
        /// </summary>
        public string EstadoSolicitud { get; set; }
        /// <summary>
        /// Fecha de Creacion de la solicitud
        /// </summary>
        public string FechaCreacion { get; set; }
        /// <summary>
        /// Fecha de Actualizacion  de la solicitud
        /// </summary>
        public string FechaActualizacion { get; set; }
        /// <summary>
        /// Nombre del Pais de la solicitud
        /// </summary>
        public string NombrePais { get; set; }
        /// <summary>
        /// Telefono del que envio la solicitud
        /// </summary>
        public string Telefono { get; set; }
        /// <summary>
        /// Email del que envio la solicitud
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Sexo del que envio la solicitud
        /// </summary>
        public string Sexo { get; set; }
        /// <summary>
        /// Nacionalidad del que envio la solicitud
        /// </summary>
        public string Nacionalidad { get; set; }
        /// <summary>
        /// Ciudad Residencia del que envio la solicitud
        /// </summary>
        public string CiudadResidencia { get; set; }
        /// <summary>
        /// Tipo de Identificacion del que envio la solicitud
        /// </summary>
        public string TipoIdentificacion { get; set; }
        /// <summary>
        /// Numero de Identificacion del que envio la solicitud
        /// </summary>
        public string NumeroIdentificacion { get; set; }
        /// <summary>
        /// Profesion del que envio la solicitud
        /// </summary>
        public string Profesion { get; set; }
        /// <summary>
        /// Es Cliente 
        /// </summary>
        public string EsCliente { get; set; }
        /// <summary>
        /// Promedio Ingreso Mensual
        /// </summary>
        public string PromedioIngresoMensualNombre { get; set; }
        /// <summary>
        /// Actividad Economica
        /// </summary>
        public string ActividadEconomica { get; set; }
        /// <summary>
        /// Es Zurdo
        /// </summary>
        public string EsZurdo { get; set; }
        /// <summary>
        /// Estatura
        /// </summary>
        public string Estatura { get; set; }
        /// <summary>
        /// Peso
        /// </summary>
        public string Peso { get; set; }
        /// <summary>
        ///Comentario Inicial
        /// </summary>
        public string ComentarioInicial { get; set; }
        /// <summary>
        ///Estado Inicial
        /// </summary>
        public string EstadoInicial { get; set; }
        /// <summary>
        ///Comentario
        /// </summary>
        public string Comentario { get; set; }
        /// <summary>
        ///Estado Historico
        /// </summary>
        public string EstadoHistorico { get; set; }
        /// <summary>
        ///Tipo Contacto
        /// </summary>
        public string TipoContacto { get; set; }
    }
}
