﻿/*==========================================================================
Archivo:            TipoIdentificacionDto
Descripción:        Administrador de los tipo de identificación                      
Autor:              paola.munoz                         
Fecha de creación:  03/11/2015 04:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad tipoIdentificacion
    /// </summary>
    public class TipoIdentificacionDto
    {
        /// <summary>
        /// Idntificador de tipoidentificacion
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de tipoIdentificacion
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Identificador del país
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Mascara
        /// </summary>
        public String Mascara { get; set; }
    }
}
