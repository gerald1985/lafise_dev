﻿/*==========================================================================
Archivo:            ReferenciaDto
Descripción:        Maneja las referencias de las solicitudes                    
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 04:09:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad Referencia
    /// </summary>
    public class ReferenciaDto
    {
        /// <summary>
        /// Identificador de las referencias
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de las solicitudes
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Nombre de la referencia
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Apellido de las referencias
        /// </summary>
        public string Apellidos { get; set; }
        /// <summary>
        /// Telefono de las referencias
        /// </summary>
        public string Telefono { get; set; }
     
    }
}
