﻿/*==========================================================================
Archivo:            RangoSueldoPaisDto
Descripción:        Dto para el manejo de la consulta del rango sueldo teniendo en cuenta el pais.                    
Autor:              juan.hincapie                        
Fecha de creación:  18/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la consulta de rango sueldo por pais.
    /// </summary>
    public class RangoSueldoPaisDto
    {
        /// <summary>
        /// Identificador de rango sueldo detalle
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Identificador de rango sueldo
        /// </summary>
        public long IdRangoSueldo { get; set; }
        /// <summary>
        /// Identificador de moneda
        /// </summary>
        public long IdMoneda { get; set; }
        /// <summary>
        /// Identificador de pais
        /// </summary>
        public long IdPais { get; set; }
        /// <summary>
        /// Rango sueldo separados por espacio
        /// </summary>
        public string RangoSueldo { get; set; }
        /// <summary>
        /// Minimo sueldo
        /// </summary>
        public Double MinSueldo { get; set; }
        /// <summary>
        /// Maximo sueldo
        /// </summary>
        public Double MaxSueldo { get; set; }
        /// <summary>
        /// Nombre del país
        /// </summary>
        public string Pais { get; set; }
        /// <summary>
        /// Nombre de la moneda
        /// </summary>
        public string Moneda { get; set; }
    }
}
