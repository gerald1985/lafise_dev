﻿/*==========================================================================
Archivo:            DatosAutoDto
Descripción:        Objeto que ayuda a manejar los datos del auto del formulario Solicitud de seguro vehicular      
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 3:59:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente de  DatosAuto
    /// </summary>
    public class DatosBienDto
    {
        /// <summary>
        /// Identificador de la entidad DatosAuto
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de la solicitud relacionada
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Valor del bien de la entidad DatosAuto
        /// </summary>
        public decimal ValorBien { get; set; }
        /// <summary>
        /// Identificador de tipo vehiculo (relacion TipoVehiculo)
        /// </summary>
        public long? IdTipoVehiculo { get; set; }
        /// <summary>
        /// Tipo vehiculo 
        /// </summary>
        public string TipoVehiculo { get; set; }
        /// <summary>
        /// Identificador de MarcaAuto (relacion)
        /// </summary>
        public long? IdMarcaAuto { get; set; }
        /// <summary>
        /// MarcaAuto 
        /// </summary>
        public string MarcaAuto { get; set; }
        /// <summary>
        /// Identificador del modelo
        /// </summary>
        public long? IdModelo { get; set; }
        /// <summary>
        /// Modelo
        /// </summary>
        public string ModeloAuto { get; set; }
        /// <summary>
        /// Simbolo de la moneda
        /// </summary>
        public string Simbolo { get; set; }
        /// <summary>
        /// Tipo Bien
        /// </summary>
        public string TipoBien { get; set; }
        /// <summary>
        /// Identificador del tipo de bien
        /// </summary>
        public long IdTipoBien { get; set; }
    }
}
