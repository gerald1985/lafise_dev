﻿/*==========================================================================
Archivo:            SolicitudCondicionesDto
Descripción:        Objeto carga los filtros para la consulta de la solicitud                      
Autor:              paola.munoz                           
Fecha de creación:  14/10/2015 6:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad de consulta de filtro de solicitud
    /// </summary>
    public class SolicitudCondicionesDto
    {
        /// <summary>
        ///Identificador de los estados concatenados
        /// </summary>
        public string Estado { get; set; }
        /// <summary>
        /// Identificador del país
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Identificador del tipo de solicitud
        /// </summary>
        public Int64 IdTipoSolicitud { get; set; }
        
    }
}
