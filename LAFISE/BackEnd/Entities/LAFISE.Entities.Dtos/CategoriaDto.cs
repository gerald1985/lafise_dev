﻿/*==========================================================================
Archivo:            CategoriaDto
Descripción:        categoria que se tendra en cuenta en el formulario de prestamo hipotecario     
Autor:              steven.echavarria                           
Fecha de creación:  13/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System; 
#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a Categoria
    /// </summary>
    public class CategoriaDto
    {
        /// <summary>
        /// Identificador de categoria
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de categoria
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Identificador de tipo de solicitud
        /// </summary>
        public Int64 IdTipoSolicitud { get; set; }
    }
}
