﻿/*==========================================================================
Archivo:            ContactenosDto
Descripción:        Administrador del formulario de contactenos donde el usuario podrá  ponerse en contacto con LAFISE                       
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 8:59:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
/// <summary>
/// Dto correspondiente de  Contactenos
/// </summary>
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Documentacion de ContactenosDto
    /// </summary>
    public class ContactenosDto
    {
        /// <summary>
        /// identificador de la entidad de contactenos
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre completo de la entidad contactenos
        /// </summary>
        public string NombreCompleto { get; set; }

        public string Identificacion { get; set; }

        /// <summary>
        /// Indica si el usuario es cliente en la entidad contactenos
        /// </summary>
        public Boolean EsCliente { get; set; }
        /// <summary>
        /// Telefono de la entidad contactenos
        /// </summary>
        public string Telefono { get; set; }
        /// <summary>
        /// Email de la entidad contactenos
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Asunto de la entidad contactenos
        /// </summary>
        public string Asunto { get; set; }
        /// <summary>
        /// Mensaje de la entidad contactenos
        /// </summary>
        public string Mensaje { get; set; }
        /// <summary>
        /// Identificadir de pais (clave foranea)
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Fecha de creacion de contactenos
        /// </summary>
        public DateTime FechaCreacion { get; set; }

        /// <summary>
        /// Guarda la url donde se encuentra el modulo
        /// </summary>
        public string PathUrl { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}
