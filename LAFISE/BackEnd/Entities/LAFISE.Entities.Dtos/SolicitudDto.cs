﻿/*==========================================================================
Archivo:            SolicitudDto
Descripción:        Administrador de solicitudes        
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 11:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad Solicitud
    /// </summary>
    public class SolicitudDto
    {
        /// <summary>
        /// Identificador de solicitudes
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de estado
        /// </summary>
        public Nullable<Int64> IdEstado { get; set; }
        /// <summary>
        /// Identificador de tipo solicitud
        /// </summary>
        public Int64 IdTipoSolicitud { get; set; }
        /// <summary>
        /// Identificador de país
        /// </summary>
        public Int64 IdPais { get; set; }

        /// <summary>
        /// Codigo del país y identificador de la solicitud
        /// </summary>
        public string CodigoIdSolicitud { get; set; }
        /// <summary>
        /// Fecha de creación de la solicitud
        /// </summary>
        public DateTime FechaCreacion { get; set; }
        /// <summary>
        /// Fecha de actualización de la solicitud
        /// </summary>
        public Nullable<DateTime> FechaActualizacion { get; set; }
        /// <summary>
        /// Identificador de tipo de contacto
        /// </summary>
        public Nullable<Int64> IdTipoContacto { get; set; }
        /// <summary>
        /// Tipo de contacto
        /// </summary>
        public string TipoContacto { get; set; }
        /// <summary>
        /// Navegación por datos personales
        /// </summary>
        public ICollection<DatosPersonalesDto> DatosPersonales { get; set; }
        /// <summary>
        /// Navegación por datos financieros
        /// </summary>
        public ICollection<DatosFinancieroDto> DatosFinancieros { get; set; }
        /// <summary>
        /// Navegación por plan de financiación
        /// </summary>
        public ICollection<PlanFinanciacionDto> PlanFinanciacion { get; set; }
        /// <summary>
        /// Navegación por los soportes (adjuntos)
        /// </summary>
        public ICollection<SoporteDto> Soportes { get; set; }
        /// <summary>
        /// Navegación por datos del auto
        /// </summary>
        public ICollection<DatosBienDto> DatosBien { get; set; }
        /// <summary>
        /// Navegación por las referencias
        /// </summary>
        public ICollection<ReferenciaDto> Referencias { get; set; }
        /// <summary>
        /// navegación por los siniestros
        /// </summary>
        public ICollection<SiniestroDto> Siniestro { get; set; }
        /// <summary>
        /// Navegación por el historico de los comentarios
        /// </summary>
        public ICollection<HistoricoComentarioDto> HistoricoComentario { get; set; }
        /// <summary>
        /// Guarda la url donde se encuentra el modulo
        /// </summary>
        public string PathUrl { get; set; }
        /// <summary>
        /// Tipo de validación
        /// </summary>
        public string TipoValidacion { get; set; }
        /// <summary>
        /// Guarda el mensaje de validación
        /// </summary>
        public string MensajeValidacion { get; set; }
        /// <summary>
        /// Valor minimo prima.
        /// </summary>
        public decimal MinimoPrima { get; set; }
        /// <summary>
        /// Lista de archivos
        /// </summary>
        public List<string> listaArchivos { get; set; }
        /// <summary>
        /// Moneda
        /// </summary>
        public string Moneda { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
        /// <summary>
        /// Procedencia
        /// </summary>
        public string Procedencia { get; set; }
        /// <summary>
        /// IdSolicitudProducto
        /// </summary>
        public Int64 IdSolicitudProducto { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string BackOfficeUser { get; set; }

    }
}
