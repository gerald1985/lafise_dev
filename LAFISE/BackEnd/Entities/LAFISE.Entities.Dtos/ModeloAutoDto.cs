/*==========================================================================
Archivo:            ModeloAutoDto
Descripción:        Administrador del modelo del auto                  
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad ModeloAuto
    /// </summary>
    public class ModeloAutoDto
    {
        /// <summary>
        /// Identificador correspondiente a la entidad ModeloAuto
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre del modelo correspondiente a la entidad ModeloAuto
        /// </summary>
        public string Modelo { get; set; }
        /// <summary>
        /// Ruta de la imagen correspondiente a la entidad ModeloAuto
        /// </summary>
        public string Imagen { get; set; }
        /// <summary>
        /// Identificador de la marca  asociada al modelo del auto.
        /// </summary>
        public long IdMarcaAuto { get; set; }
        /// <summary>
        /// Estado activo o no correspondiente a la entidad ModeloAuto.
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Nombre de la marca asociada al modelo del auto.
        /// </summary>
        public string NombreMarca { get; set; }
        /// <summary>
        /// Ruta Completa de la Imagen
        /// </summary>
        public string RutaImagen { get; set; }

        public string RutaThumb { get; set; }
        /// <summary>
        /// Fecha de creación correspondiente a la entidad ModeloAuto.
        /// </summary>
        public DateTime FechaCreacion { get; set; }
        /// <summary>
        /// Indica si el estado de la transaccion: Add o edit
        /// </summary>
        public String EstadoTransaccion { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string MensajeError { get; set; }

        public string RelacionSolicitud { get; set; }

        public string CasasComercialesRelacionadas { get; set; }

        /// <summary>
        /// Indica si el modelo tiene relación en la tabla CasaComercialModelo
        /// </summary>
        public bool RelacionCasaComercial { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}