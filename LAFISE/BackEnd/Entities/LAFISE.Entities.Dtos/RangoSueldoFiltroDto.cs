/*==========================================================================
Archivo:            RangoSueldoFiltroDto
Descripción:        Dto para el manejo de la consulta del rango sueldo teniendo en cuenta los filtros                    
Autor:              paola.munoz                           
Fecha de creación:  29/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad RangoSueldoFiltro
    /// </summary>
    public class RangoSueldoFiltroDto
    {
        /// <summary>
        /// Identificador de rango sueldo
        /// </summary>
        public Int64 IdRangoSueldo { get; set; }
        /// <summary>
        /// Identificador de moneda
        /// </summary>
        public Int64 IdMoneda { get; set; }
        /// <summary>
        /// Identificador de pais
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Rango sueldo separados por espacio
        /// </summary>
        public string RangoSueldo { get; set; }
        /// <summary>
        /// Nombre del país
        /// </summary>
        public string Pais { get; set; }
        /// <summary>
        /// Nombre de la moneda
        /// </summary>
        public string Moneda { get; set; }

    }
}