/*==========================================================================
Archivo:            PaisDto
Descripción:        Administrador de los paises                      
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad Pais
    /// </summary>
    public class PaisDto
    {
        /// <summary>
        /// Idntificador de país
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de país
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Código del país
        /// </summary>
        public String Codigo { get; set; }
        /// <summary>
        /// Fecha de creación del país
        /// </summary>
        public DateTime FechaCreacion { get; set; }
    }
}