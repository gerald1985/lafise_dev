﻿/*==========================================================================
Archivo:            DatosFinancieroDto
Descripción:        Maneja de los datos financieros de los formularios de la solicitudes                    
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 1:37:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente de datos financieros
    /// </summary>
    public class DatosFinancieroDto
    {
        /// <summary>
        /// Identificador de datos financieros
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de la solicitud (relacion)
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Me indica si es cliente
        /// </summary>
        public Boolean EsCliente { get; set; }
        /// <summary>
        /// Me indica si posee nomina
        /// </summary>
        public Boolean PoseeNomina { get; set; }
        /// <summary>
        /// Me indica si tiene negocio propio
        /// </summary>
        public Nullable<bool> NegocioPropio { get; set; }
        /// <summary>
        /// Me indica si es asalariado
        /// </summary>
        public Nullable<bool> Asalariado { get; set; }
        /// <summary>
        /// Nombre del  PromedioIngresoMensual
        /// </summary>
        public string PromedioIngresoMensualNombre { get; set; }
        /// <summary>
        /// Me indica si es zurdo
        /// </summary>
        public Nullable<bool> EsZurdo { get; set; }
        /// <summary>
        /// Me indica si es zurdo Nombre
        /// </summary>
        public string EsZurdoNombre { get; set; }
        /// <summary>
        /// Estatura
        /// </summary>
        public string Estatura { get; set; }
        /// <summary>
        /// Peso
        /// </summary>
        public string Peso { get; set; }
        /// <summary>
        /// Es cuenta fisica 
        /// </summary>
        public Nullable<bool> EstadoCuenta { get; set; }
        /// <summary>
        /// Estado cuenta nombre
        /// </summary>
        public string EstadoCuentaNombre { get; set; }
        /// <summary>
        /// Direccion del plan de financiacion
        /// </summary>
        public string Direccion { get; set; }
        /// <summary>
        /// Identificador de la sucursal mas cercana
        /// </summary>
        public Nullable<Int64> IdSucursal { get; set; }
        /// <summary>
        /// nombre de la sucursal mas cercana
        /// </summary>
        public string Sucursal { get; set; }
        /// <summary>
        /// nombre del lugar de trabajo
        /// </summary>
        public string LugarTrabajo { get; set; }
        /// <summary>
        /// Instancia de actividad economica
        /// </summary>
        public ICollection<ActividadEconomicaDto> ActividadEconomica { get; set; }


        public string IndustriaTrabajo { get; set; }
        public Nullable<int> Antiguedad { get; set; }
        public string DireccionOficina { get; set; }
        public string TelefonoOficina { get; set; }
        public string SucursalEnvio { get; set; }
    }
}
