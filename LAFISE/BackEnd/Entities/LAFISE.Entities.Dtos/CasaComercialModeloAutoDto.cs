/*==========================================================================
Archivo:            CasaComercialModeloAutoDto
Descripción:        Dto relacionado con la casa comercial                      
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad CasaComercialModeloAuto
    /// </summary>
    public class CasaComercialModeloAutoDto
    {
        /// <summary>
        /// Identificador correspondiente a la entidad CasaComercialModeloAuto
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Identificador de la casa comercial asociado a la entidad CasaComercialModeloAuto
        /// </summary>
        public long IdCasaComercial { get; set; }
        /// <summary>
        /// Identificador del modelo asociado a la entidad CasaComercialModeloAuto
        /// </summary>
        public long IdModeloAuto { get; set; }
        /// <summary>
        /// Fecha de creación correspondiente a la entidad CasaComercialModeloAuto
        /// </summary>
        public DateTime FechaCreacion { get; set; }
    }
}