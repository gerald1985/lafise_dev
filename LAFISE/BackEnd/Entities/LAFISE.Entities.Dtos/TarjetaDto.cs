﻿/*==========================================================================
Archivo:            TarjetaDto
Descripción:        Administrador de las tarjetas                     
Autor:              paola.munoz                           
Fecha de creación:  30/09/2015 11:10:05 a.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad TarjetaDto
    /// </summary>
    public class TarjetaDto
    {
        /// <summary>
        /// Identificador de las tarjetas
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de la tarjeta de credito
        /// </summary>
        public String TarjetaCredito { get; set; }
        /// <summary>
        /// Imagen de la tarjeta de credito
        /// </summary>
        public String Imagen { get; set; }
        /// <summary>
        /// Indica si la tarjeta se encuentra activa
        /// </summary>
        public Boolean Activo { get; set; }

        /// <summary>
        /// Indica la ruta de la imagen
        /// </summary>
        public string RutaImagen { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}
