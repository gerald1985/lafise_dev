﻿/*==========================================================================
Archivo:            TipoPropiedadPlazoDto
Descripción:        Plazo que tendrá cada tipo de propiedad
Autor:              steven.echavarria                          
Fecha de creación:  18/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                      
==========================================================================*/

#region Referencias
using System;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a TipoPropiedadPlazo
    /// </summary>
    public class TipoPropiedadPlazoDto
    {
        /// <summary>
        /// Identificador del TipoPropiedadPlazo (clave primaria)
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador del tipo de propiedad (clave foranea)
        /// </summary>
        public Int64 IdTipoPropiedad { get; set; }
        /// <summary>
        /// Plazo al cual se debe pagar el prestamo 
        /// </summary>
        public Int32 Plazo { get; set; }
    }
}
