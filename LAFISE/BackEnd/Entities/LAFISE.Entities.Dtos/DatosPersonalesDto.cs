﻿/*==========================================================================
Archivo:            DatosPersonalesDto
Descripción:         Maneja de los datos personales de los formularios de la solicitudes            
Autor:              paola.munoz                           
Fecha de creación:  13/10/2015 1:35:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente de datos Personales
    /// </summary>
    public class DatosPersonalesDto
    {
        /// <summary>
        /// Identificador de los datos personales
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre de los datos personales
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Apellidos de los datos personales
        /// </summary>
        public string Apellidos { get; set; }
        /// <summary>
        /// Telefono de los datos personales
        /// </summary>
        public string Telefono { get; set; }
        /// <summary>
        /// Email de los datos personales
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Indica si tiene genero masculino
        /// </summary>
        public Boolean Masculino { get; set; }
        /// <summary>
        /// Genero
        /// </summary>
        public string Genero { get; set; }
        /// <summary>
        /// Nacionalidad en los datos personales
        /// </summary>
        public string Nacionalidad { get; set; }
        /// <summary>
        /// Ciudad residencia de los datos personales
        /// </summary>
        public string CiudadResidencia { get; set; }
        /// <summary>
        /// Identificador de tipo de identificación
        /// </summary>
        public Int64 IdTipoIdentificacion { get; set; }
        /// <summary>
        /// Tipo de identificación
        /// </summary>
        public string TipoIdentificacion { get; set; }
        /// <summary>
        /// Numero de identificacion de datos personales
        /// </summary>
        public string NumeroIdentificacion { get; set; }
        /// <summary>
        /// Profesion de los datos personales
        /// </summary>
        public string Profesion { get; set; }
        /// <summary>
        /// Fecha de nacimiento
        /// </summary>
        public DateTime FechaNacimiento { get; set; }
        /// <summary>
        /// Identificador de la solicitud
        /// </summary>
        public Int64 IdSolicitud { get; set; }
        /// <summary>
        /// Es codeudor 
        /// </summary>
        public Nullable<bool> EsCodeudor { get; set; }

        public string TipoDocumentoSolicitud { get; set; }

        public string EstadoCivil { get; set; }
        public Nullable<int> Dependientes { get; set; }
        public string DireccionResidencia { get; set; }
        public string TipoResidencia { get; set; }
        public string EstadoResidencia { get; set; }
        public Nullable<int> AniosResidencia { get; set; }
        public string Educacion { get; set; }

        /* INICIA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
        public string SegundoNombre { get; set; }
        public string SegundoApellido { get; set; }
        public string PrimerNombreBeneficiario { get; set; }
        public string SegundoNombreBeneficiario { get; set; }
        public string PrimerApellidoBeneficiario { get; set; }
        public string SegundoApellidoBeneficiario { get; set; }
        public Nullable<Int64> TipoIdBeneficiario { get; set; }
        public string NumeroIdBeneficiario { get; set; }
        public string DireccionBeneficiario { get; set; }
        public string ParentescoBeneficiario { get; set; }
        public string PrimerNombreTutor { get; set; }
        public string SegundoNombreTutor { get; set; }
        public string PrimerApellidoTutor { get; set; }
        public string SegundoApellidoTutor { get; set; }
        public Nullable<Int64> TipoIdTutor { get; set; }
        public string NumeroIdTutor { get; set; }
        public string ParentescoTutor { get; set; }
        public string TelefonoTutor { get; set; }
        public string EmailTutor { get; set; }
        /* FINALIZA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
    }
}
