﻿/*==========================================================================
Archivo:            SucursalFiltroDto
Descripción:        Gestion de la consulta de las sucursales teniendo en cuenta los filtros                      
Autor:              paola.munoz                          
Fecha de creación:  07/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad sucursal
    /// </summary>
    public class SucursalFiltroDto
    {
        /// <summary>
        /// Identificador de la sucursal
        /// </summary>
        public Int64 IdSucursal { get; set; }

        /// <summary>
        /// Identificador del país
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Nombre del país relacionado
        /// </summary>
        public string NombrePais { get; set; }

        /// <summary>
        /// Identificador del departamento
        /// </summary>
        public Int64 IdDepartamento { get; set; }

        /// <summary>
        /// Nombre del departamento relacionado
        /// </summary>
        public string NombreDepartamento { get; set; }

        /// <summary>
        /// Identificador de la ciudad
        /// </summary>
        public Int64 IdCiudad { get; set; }

        /// <summary>
        /// Nombre de la ciudad relacionada
        /// </summary>
        public string NombreCiudad { get; set; }

        /// <summary>
        /// Identificador del tipo de sucursal
        /// </summary>
        public Int64 IdTipoSucursal { get; set; }

        /// <summary>
        /// Nombre del tipo de sucursal relacionado
        /// </summary>
        public string TipoSucursal { get; set; }

        /// <summary>
        /// Nombre de la sucursal
        /// </summary>
        public string NombreSucursal { get; set; }
        
        /// <summary>
        /// Indica si la sucursal se encuentra activa
        /// </summary>
        public Boolean Activo { get; set; }
    }
}
