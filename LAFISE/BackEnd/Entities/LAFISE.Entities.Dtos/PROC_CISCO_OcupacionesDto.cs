﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LAFISE.Entities.Dtos
{
    public class PROC_CISCO_OcupacionesDto
    {
        public long Cod_Ocupacion { get; set; }
        public string Nombre_Ocupacion { get; set; }
    }
}
