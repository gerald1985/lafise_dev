﻿/*==========================================================================
Archivo:            DepartamentoDto
Descripción:        Administrador del departamento                      
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                         
==========================================================================*/

#region Referencias
using System;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad de departamento
    /// </summary>
    public class DepartamentoDto
    {
        /// <summary>
        /// Identificador del departamento
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre del departamento
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Identificador del pais
        /// </summary>
        public Int64 IdPais { get; set; }
    }
}
