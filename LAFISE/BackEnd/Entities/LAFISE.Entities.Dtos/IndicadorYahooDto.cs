﻿/*==========================================================================
Archivo:            IndicadorYahooDto
Descripción:        Administrador de los indicadores utilizados para los datos actuales       
Autor:              arley.lopez                           
Fecha de creación:  12/02/2016                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Aleriant S.A.S                                         
==========================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad IndicadorHistoricoYahoo
    /// </summary>
     public class IndicadorYahooDto
    {
        /// <summary>
        /// Identificador del Dto
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Símbolo del indicador usado para consultar los valores actuales en la API de Yahoo
        /// </summary>
        public string Simbolo { get; set; }
        /// <summary>
        /// Nombre del indicador usado para mostrar en el grid
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Valor Actual del indicador usado para mostrar en el grid
        /// </summary>
        public string ValorActual { get; set; }
        /// <summary>
        /// Precio de variación del indicador usado para mostrar en el grid
        /// </summary>
        public string Variacion { get; set; }
        /// <summary>
        /// Fecha usada para mostrar en el grid
        /// </summary>
        public string Fecha { get; set; }
        /// <summary>
        /// Hora usada para mostrar en el grid
        /// </summary>
        public string Hora { get; set; } 
    }
}
