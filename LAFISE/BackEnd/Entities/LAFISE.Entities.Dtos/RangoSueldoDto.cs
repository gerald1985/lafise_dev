/*==========================================================================
Archivo:            RangoSueldoDto
Descripción:        Administrador de rango sueldo                    
Autor:              ximena.echeverri                           
Fecha de creación:  07/09/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente a la entidad RangoSueldo
    /// </summary>
    public class RangoSueldoDto
    {
        /// <summary>
        /// Identificador de rango sueldo
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Identificador de país
        /// </summary>
        public Int64 IdPais { get; set; }
        /// <summary>
        /// Identificador de moneda
        /// </summary>
        public Int64 IdMoneda { get; set; }

        /// <summary>
        /// Lista de la entidad rango sueldo detalle
        /// </summary>
        public ICollection<RangoSueldoDetalleDto> RangoSueldoDetalle { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}