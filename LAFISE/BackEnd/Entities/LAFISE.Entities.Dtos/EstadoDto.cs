﻿/*==========================================================================
Archivo:            EstadoDto
Descripción:        Estado para la gestion de los dormularios   
Autor:              paola.munoz                           
Fecha de creación:  16/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias
using System; 
#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a Categoria
    /// </summary>
    public class EstadoDto
    {
        /// <summary>
        /// Identificador del estado
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre del estado
        /// </summary>
        public String Nombre { get; set; }
        /// <summary>
        /// Identificador de tipo de estado
        /// </summary>
        public Int64 IdTipoEstado { get; set; }
        /// <summary>
        /// Nombre del tipo estado
        /// </summary>
        public String TipoEstado { get; set; }
    }
}
