﻿#region Referencias
using System;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto Correspondiente a la entidad Cotizador
    /// </summary>
    public class CotizadorDto
    {

        public Int64 Id { get; set; }
        public String Nombre { get; set; }
        public String Email { get; set; }
        public String Telefono { get; set; }
        public String Pais { get; set; }
        public String PersonalId { get; set; }
        public Int32 Edad { get; set; }
        public String Marca { get; set; }
        public String Modelo { get; set; }
        public DateTime FechaCreacion { get; set; }


    }
}
