﻿/*==========================================================================
Archivo:            ExportarAperturaCuentaDto
Descripción:        Dto para capturar la exportación de apertura de cuenta            
Autor:              paola.munoz                        
Fecha de creación:  17/12/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    ///  Dto correspondiente al exportar marca auto
    /// </summary>
    public class ExportarAperturaCuentaDto
    {
        /// <summary>
        /// Número de la solicitud
        /// </summary>
        public string NumeroSolicitud { get; set; }
        /// <summary>
        /// Nombre completo del que envio la solicitud
        /// </summary>
        public string NombreCompleto { get; set; }
        /// <summary>
        /// Estado de la solicitud
        /// </summary>
        public string EstadoSolicitud { get; set; }
        /// <summary>
        /// Fecha de Creacion de la solicitud
        /// </summary>
        public string FechaCreacion { get; set; }
        /// <summary>
        /// Fecha de Actualizacion  de la solicitud
        /// </summary>
        public string FechaActualizacion { get; set; }
        /// <summary>
        /// Nombre del Pais de la solicitud
        /// </summary>
        public string NombrePais { get; set; }
        /// <summary>
        /// Telefono del que envio la solicitud
        /// </summary>
        public string Telefono { get; set; }
        /// <summary>
        /// Email del que envio la solicitud
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Sexo del que envio la solicitud
        /// </summary>
        public string Sexo { get; set; }
        /// <summary>
        /// Nacionalidad del que envio la solicitud
        /// </summary>
        public string Nacionalidad { get; set; }
        /// <summary>
        /// Ciudad Residencia del que envio la solicitud
        /// </summary>
        public string CiudadResidencia { get; set; }
        /// <summary>
        /// Tipo de Identificacion del que envio la solicitud
        /// </summary>
        public string TipoIdentificacion { get; set; }
        /// <summary>
        /// Numero de Identificacion del que envio la solicitud
        /// </summary>
        public string NumeroIdentificacion { get; set; }
        /// <summary>
        /// Profesion del que envio la solicitud
        /// </summary>
        public string Profesion { get; set; }
        /// <summary>
        /// Es Cliente 
        /// </summary>
        public string EsCliente { get; set; }
        /// <summary>
        /// Posee Nomina
        /// </summary>
        public string PoseeNomina { get; set; }
        /// <summary>
        /// Negocio Propio
        /// </summary>
        public string NegocioPropio { get; set; }
        /// <summary>
        /// Asalariado
        /// </summary>
        public string Asalariado { get; set; }
        /// <summary>
        /// Promedio Ingreso Mensual
        /// </summary>
        public string PromedioIngresoMensualNombre { get; set; }
        /// <summary>
        /// Soporte Negocio Propio concatenado con coma
        /// </summary>
        public string SoporteNegocioPropio { get; set; }
        /// <summary>
        /// Soporte Ingresos concatenado con coma
        /// </summary>
        public string SoporteIngresos { get; set; }
        /// <summary>
        ///Sucursal Cercana
        /// </summary>
        public string SucursalCercana { get; set; }
        /// <summary>
        ///Nombre Referencia 1
        /// </summary>
        public string NombreReferencia1 { get; set; }
        /// <summary>
        ///Apellidos Referencia 1
        /// </summary>
        public string ApellidosReferencia1 { get; set; }
        /// <summary>
        ///Telefono Referencia 1
        /// </summary>
        public string TelefonoReferencia1 { get; set; }
        /// <summary>
        ///Nombre Referencia 2
        /// </summary>
        public string NombreReferencia2 { get; set; }
        /// <summary>
        ///Apellidos Referencia 2
        /// </summary>
        public string ApellidosReferencia2 { get; set; }
        /// <summary>
        ///Telefono Referencia 2
        /// </summary>
        public string TelefonoReferencia2 { get; set; }
        /// <summary>
        ///Comentario Inicial
        /// </summary>
        public string ComentarioInicial { get; set; }
        /// <summary>
        ///Estado Inicial
        /// </summary>
        public string EstadoInicial { get; set; }
        /// <summary>
        ///Comentario
        /// </summary>
        public string Comentario { get; set; }
        /// <summary>
        ///Estado Historico
        /// </summary>
        public string EstadoHistorico { get; set; }
        /// <summary>
        ///Tipo Contacto
        /// </summary>
        public string TipoContacto { get; set; }
    }
}
