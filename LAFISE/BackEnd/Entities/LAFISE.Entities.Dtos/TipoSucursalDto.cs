/*==========================================================================
Archivo:            TipoSucursalDto
Descripción:        Administrador del tipo de sucursal              
Autor:              paola.munoz                           
Fecha de creación:  06/10/2015 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la entidad TipoSucursalDto
    /// </summary>
    public class TipoSucursalDto
    {
        /// <summary>
        /// Identificador del tipo de sucursal
        /// </summary>
        public Int64 Id { get; set; }
        /// <summary>
        /// Nombre del tipo de sucursal
        /// </summary>
        public String TipoSucursal1 { get; set; }
        /// <summary>
        /// Indica si el tipo de sucursal se encuentra activo
        /// </summary>
        public Boolean Activo { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string MensajeError { get; set; }
        /// <summary>
        ///Usuario
        /// </summary>
        public string Usuario { get; set; }
    }
}