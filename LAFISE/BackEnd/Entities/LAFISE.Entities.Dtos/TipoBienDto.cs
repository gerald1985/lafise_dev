﻿/*==========================================================================
Archivo:            TipoBienDto
Descripción:        Dto del tipo de vehículo              
Autor:              juan.hincapie                           
Fecha de creación:  06/01/2016 03:10:05 p.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{
    public class TipoBienDto
    {
        /// <summary>
        /// Identificador del tipo de bien
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre del tipo de bien
        /// </summary>
        public string Nombre { get; set; }
    }
}
