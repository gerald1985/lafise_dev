﻿/*==========================================================================
Archivo:            UrbanizadoraValidarDto
Descripción:        Validar la urbanizadora              
Autor:              paola.munoz                           
Fecha de creación:  01/10/2015                                             
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/

#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion


namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a validar la urbanizadora
    /// </summary>
    public class UrbanizadoraValidarDto
    {
        /// <summary>
        /// Contador de urbanizadora
        /// </summary>
        public Int32 Contador { get; set; }
        /// <summary>
        /// Nombre de las urbanizadoras concatenadas por coma
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string activo { get; set; }
    }
}
