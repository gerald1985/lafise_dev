﻿/*==========================================================================
Archivo:            IndicadoresHistoricosYahooDto
Descripción:        Administrador de los indicadores utilizados para los Históricos        
Autor:              arley.lopez                           
Fecha de creación:  12/02/2016                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Aleriant S.A.S                                         
==========================================================================*/


#region Referencias

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Dtos
{ 
    /// <summary>
    /// Dto correspondiente a la entidad IndicadorHistoricoYahoo
    /// </summary>
    public class IndicadoresHistoricosYahooDto
    {
        /// <summary>
        /// Fecha para el indicador del Dto
        /// </summary>
        public string Fecha { get; set; }
        /// <summary>
        /// Precio de apertura para el indicador del Dto
        /// </summary>
        public float Apertura { get; set; }
        /// <summary>
        /// Precio mas alto para el indicador del Dto
        /// </summary>
        public float MasAlto { get; set; }
        /// <summary>
        /// Precio mas bajo para el indicador del Dto
        /// </summary>
        public float MasBajo { get; set; }
        /// <summary>
        /// Precio de cierre para el indicador del Dto
        /// </summary>
        public float Cierre { get; set; }
        /// <summary>
        /// Precio del volumen para el indicador del Dto
        /// </summary>
        public float Volumen { get; set; }
        /// <summary>
        /// Precio de cierre ajustado para el indicador del Dto
        /// </summary>
        public float CierreAjustado { get; set; }
    }
}
