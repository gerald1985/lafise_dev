﻿/*==========================================================================
Archivo:            CasaComercialFiltroDto
Descripción:        Dto que maneja la consulta teniendo en cuenta los filtros de casa comercial                      
Autor:              paola.munoz                           
Fecha de creación:  30/09/2015 5:10:05 P.m.                                               
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Aleriant S.A.S                                         
==========================================================================*/
#region Referencias
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
namespace LAFISE.Entities.Dtos
{
    /// <summary>
    /// Dto correspondiente a la consulta de casas comerciales por filtros.
    /// </summary>
    public class CasaComercialFiltroDto
    {
        /// <summary>
        /// Id Correspondiente a la entidad de CasaComercial.
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre Correspondiente a la entidad de CasaComercial.
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        ///  Id del pais asociado a la casa comercial.
        /// </summary>
        public long IdPais { get; set; }
        /// <summary>
        /// Nombre del pais asociado a la casa comercial.
        /// </summary>
        public string NombrePais { get; set; }
        /// <summary>
        /// Tipo de vehículo nuevo correspondiente a la entidad de CasaComercial.
        /// </summary>
        public bool Nuevo { get; set; }
        /// <summary>
        /// Tipo de vehículo usado correspondiente a la entidad de CasaComercial.
        /// </summary>
        public bool Usado { get; set; }
        /// <summary>
        /// Fecha de creación correspondiente a la entidad de CasaComercial.
        /// </summary>
        public DateTime FechaCreacion { get; set; }
        /// <summary>
        /// Marcas separadas por "," asociadas a la casa comercial.
        /// </summary>
        public string Marcas { get; set; }
        /// <summary>
        /// Indica si la casa comercial se encuentra activa
        /// </summary>
        public bool Activo { get; set; }
    }
}
