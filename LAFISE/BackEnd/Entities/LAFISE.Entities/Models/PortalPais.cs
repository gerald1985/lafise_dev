using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class PortalPais
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
        public string ZonaHoraria { get; set; }
        public Nullable<long> IdPais { get; set; }
    }
}
