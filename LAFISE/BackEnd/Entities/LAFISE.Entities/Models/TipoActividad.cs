using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoActividad
    {
        public TipoActividad()
        {
            this.ActividadEconomica = new List<ActividadEconomica>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<ActividadEconomica> ActividadEconomica { get; set; }
    }
}
