using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class MarcaAuto
    {
        public MarcaAuto()
        {
            this.DatosBien = new List<DatosBien>();
            this.MarcaAutoPlazo = new List<MarcaAutoPlazo>();
            this.ModeloAuto = new List<ModeloAuto>();
            this.PlanFinanciacion = new List<PlanFinanciacion>();
        }

        public long Id { get; set; }
        public string Marca { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public bool Activo { get; set; }
        public virtual ICollection<DatosBien> DatosBien { get; set; }
        public virtual ICollection<MarcaAutoPlazo> MarcaAutoPlazo { get; set; }
        public virtual ICollection<ModeloAuto> ModeloAuto { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacion { get; set; }
    }
}
