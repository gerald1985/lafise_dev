using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class ModeloAuto
    {
        public ModeloAuto()
        {
            this.CasaComercialModeloAuto = new List<CasaComercialModeloAuto>();
            this.DatosBien = new List<DatosBien>();
            this.PlanFinanciacion = new List<PlanFinanciacion>();
        }

        public long Id { get; set; }
        public string Modelo { get; set; }
        public string Imagen { get; set; }
        public long IdMarcaAuto { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public bool Activo { get; set; }
        public virtual ICollection<CasaComercialModeloAuto> CasaComercialModeloAuto { get; set; }
        public virtual ICollection<DatosBien> DatosBien { get; set; }
        public virtual MarcaAuto MarcaAuto { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacion { get; set; }
    }
}
