using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoEstado
    {
        public TipoEstado()
        {
            this.Estado = new List<Estado>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Estado> Estado { get; set; }
    }
}
