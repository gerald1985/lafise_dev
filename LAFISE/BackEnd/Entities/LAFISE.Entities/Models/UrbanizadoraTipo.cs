using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class UrbanizadoraTipo
    {
        public long Id { get; set; }
        public long IdUrbanizadora { get; set; }
        public long IdTipoPropiedad { get; set; }
        public virtual TipoPropiedad TipoPropiedad { get; set; }
        public virtual Urbanizadora Urbanizadora { get; set; }
    }
}
