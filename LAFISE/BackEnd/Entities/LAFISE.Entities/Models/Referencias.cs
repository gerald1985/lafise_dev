using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Referencias
    {
        public long Id { get; set; }
        public long IdSolicitud { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Telefono { get; set; }
        public virtual Solicitud Solicitud { get; set; }
    }
}
