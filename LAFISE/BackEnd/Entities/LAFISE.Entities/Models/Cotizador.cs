using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Cotizador
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Pais { get; set; }
        public int Edad { get; set; }
        public string PersonalId { get; set; }
        public string Email { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public System.DateTime FechaCreacion { get; set; }
    }
}
