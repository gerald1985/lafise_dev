using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class SuscripcionBoletin
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public long IdPais { get; set; }
        public virtual Pais Pais { get; set; }
    }
}
