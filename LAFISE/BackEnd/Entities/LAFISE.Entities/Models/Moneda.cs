using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Moneda
    {
        public Moneda()
        {
            this.RangoSueldo = new List<RangoSueldo>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Simbolo { get; set; }
        public virtual ICollection<RangoSueldo> RangoSueldo { get; set; }
    }
}
