using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class SiniestroCiudad
    {
        public long Id { get; set; }
        public long IdSiniestro { get; set; }
        public long IdCiudad { get; set; }
        public virtual Ciudad Ciudad { get; set; }
        public virtual Siniestro Siniestro { get; set; }
    }
}
