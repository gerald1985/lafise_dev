using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Estado
    {
        public Estado()
        {
            this.HistoricoComentario = new List<HistoricoComentario>();
            this.Solicitud = new List<Solicitud>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public Nullable<long> IdTipoEstado { get; set; }
        public virtual TipoEstado TipoEstado { get; set; }
        public virtual ICollection<HistoricoComentario> HistoricoComentario { get; set; }
        public virtual ICollection<Solicitud> Solicitud { get; set; }
    }
}
