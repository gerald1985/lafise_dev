using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Soportes
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public long IdTipoSoporte { get; set; }
        public long IdSolicitud { get; set; }
        public string Ruta { get; set; }
        public bool EsCodeudor { get; set; }
        public virtual Solicitud Solicitud { get; set; }
        public virtual TipoSoporte TipoSoporte { get; set; }
    }
}
