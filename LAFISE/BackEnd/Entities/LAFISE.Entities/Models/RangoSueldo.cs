using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class RangoSueldo
    {
        public RangoSueldo()
        {
            this.RangoSueldoDetalle = new List<RangoSueldoDetalle>();
        }

        public long Id { get; set; }
        public long IdPais { get; set; }
        public long IdMoneda { get; set; }
        public virtual Moneda Moneda { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual ICollection<RangoSueldoDetalle> RangoSueldoDetalle { get; set; }
    }
}
