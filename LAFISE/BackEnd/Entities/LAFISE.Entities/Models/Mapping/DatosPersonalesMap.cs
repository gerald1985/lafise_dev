using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class DatosPersonalesMap : EntityTypeConfiguration<DatosPersonales>
    {
        public DatosPersonalesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Apellidos)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Telefono)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Nacionalidad)
                .IsRequired()
                .HasMaxLength(80);

            this.Property(t => t.CiudadResidencia)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.NumeroIdentificacion)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Profesion)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.TipoDocumentoSolicitud)
                .HasMaxLength(50);

            this.Property(t => t.EstadoCivil)
                .HasMaxLength(50);

            this.Property(t => t.DireccionResidencia)
                .HasMaxLength(500);

            this.Property(t => t.TipoResidencia)
                .HasMaxLength(50);

            this.Property(t => t.EstadoResidencia)
                .HasMaxLength(50);
    
            this.Property(t => t.Educacion)
                .HasMaxLength(100);

            this.Property(t => t.Educacion)
                .HasMaxLength(100);


            /* INICIAN DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */

            this.Property(t => t.SegundoNombre)
                .HasMaxLength(50);

            this.Property(t => t.SegundoApellido)
               .HasMaxLength(50);

            this.Property(t => t.PrimerNombreBeneficiario)
               .HasMaxLength(50);

            this.Property(t => t.SegundoNombreBeneficiario)
               .HasMaxLength(50);

            this.Property(t => t.PrimerApellidoBeneficiario)
               .HasMaxLength(50);

            this.Property(t => t.SegundoApellidoBeneficiario)
               .HasMaxLength(50);

            this.Property(t => t.NumeroIdBeneficiario)
               .HasMaxLength(15);

            this.Property(t => t.DireccionBeneficiario)
               .HasMaxLength(120);

            this.Property(t => t.ParentescoBeneficiario)
               .HasMaxLength(50);

            this.Property(t => t.PrimerNombreTutor)
               .HasMaxLength(50);

            this.Property(t => t.SegundoNombreTutor)
               .HasMaxLength(50);

            this.Property(t => t.PrimerApellidoTutor)
               .HasMaxLength(50);

            this.Property(t => t.SegundoApellidoTutor)
               .HasMaxLength(50);

            this.Property(t => t.NumeroIdTutor)
               .HasMaxLength(15);

            this.Property(t => t.ParentescoTutor)
               .HasMaxLength(50);

            this.Property(t => t.TelefonoTutor)
               .HasMaxLength(50);

            this.Property(t => t.EmailTutor)
               .HasMaxLength(100);

            /* FINALIZA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */

            // Table & Column Mappings
            this.ToTable("DatosPersonales");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Apellidos).HasColumnName("Apellidos");
            this.Property(t => t.Telefono).HasColumnName("Telefono");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Masculino).HasColumnName("Masculino");
            this.Property(t => t.Nacionalidad).HasColumnName("Nacionalidad");
            this.Property(t => t.CiudadResidencia).HasColumnName("CiudadResidencia");
            this.Property(t => t.IdTipoIdentificacion).HasColumnName("IdTipoIdentificacion");
            this.Property(t => t.NumeroIdentificacion).HasColumnName("NumeroIdentificacion");
            this.Property(t => t.Profesion).HasColumnName("Profesion");
            this.Property(t => t.FechaNacimiento).HasColumnName("FechaNacimiento");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.EsCodeudor).HasColumnName("EsCodeudor");
            this.Property(t => t.TipoDocumentoSolicitud).HasColumnName("TipoDocumentoSolicitud");

            this.Property(t => t.EstadoCivil).HasColumnName("EstadoCivil");
            this.Property(t => t.Dependientes).HasColumnName("Dependientes");
            this.Property(t => t.DireccionResidencia).HasColumnName("DireccionResidencia");
            this.Property(t => t.TipoResidencia).HasColumnName("TipoResidencia");
            this.Property(t => t.EstadoResidencia).HasColumnName("EstadoResidencia");
            this.Property(t => t.AniosResidencia).HasColumnName("AniosResidencia");
            this.Property(t => t.Educacion).HasColumnName("Educacion");

            /* INCIA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
            this.Property(t => t.SegundoNombre).HasColumnName("SegundoNombre");
            this.Property(t => t.SegundoApellido).HasColumnName("SegundoApellido");
            this.Property(t => t.PrimerNombreBeneficiario).HasColumnName("PrimerNombreBeneficiario");
            this.Property(t => t.SegundoNombreBeneficiario).HasColumnName("SegundoNombreBeneficiario");
            this.Property(t => t.PrimerApellidoBeneficiario).HasColumnName("PrimerApellidoBeneficiario");
            this.Property(t => t.SegundoApellidoBeneficiario).HasColumnName("SegundoApellidoBeneficiario");
            this.Property(t => t.TipoIdBeneficiario).HasColumnName("TipoIdBeneficiario");
            this.Property(t => t.NumeroIdBeneficiario).HasColumnName("NumeroIdBeneficiario");
            this.Property(t => t.DireccionBeneficiario).HasColumnName("DireccionBeneficiario");
            this.Property(t => t.ParentescoBeneficiario).HasColumnName("ParentescoBeneficiario");
            this.Property(t => t.PrimerNombreTutor).HasColumnName("PrimerNombreTutor");
            this.Property(t => t.SegundoNombreTutor).HasColumnName("SegundoNombreTutor");
            this.Property(t => t.PrimerApellidoTutor).HasColumnName("PrimerApellidoTutor");
            this.Property(t => t.SegundoApellidoTutor).HasColumnName("SegundoApellidoTutor");
            this.Property(t => t.TipoIdTutor).HasColumnName("TipoIdTutor");
            this.Property(t => t.NumeroIdTutor).HasColumnName("NumeroIdTutor");
            this.Property(t => t.ParentescoTutor).HasColumnName("ParentescoTutor");
            this.Property(t => t.TelefonoTutor).HasColumnName("TelefonoTutor");
            this.Property(t => t.EmailTutor).HasColumnName("EmailTutor");
            /* FINALIZA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */

            // Relationships
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.DatosPersonales)
                .HasForeignKey(d => d.IdSolicitud);
            this.HasRequired(t => t.TipoIdentificacion)
                .WithMany(t => t.DatosPersonales)
                .HasForeignKey(d => d.IdTipoIdentificacion);

        }
    }
}
