using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class UrbanizadoraMap : EntityTypeConfiguration<Urbanizadora>
    {
        public UrbanizadoraMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Urbanizadora");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.Activo).HasColumnName("Activo");

            // Relationships
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.Urbanizadora)
                .HasForeignKey(d => d.IdPais);

        }
    }
}
