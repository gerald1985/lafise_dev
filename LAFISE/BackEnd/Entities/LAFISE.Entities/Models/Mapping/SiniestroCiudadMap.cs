using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class SiniestroCiudadMap : EntityTypeConfiguration<SiniestroCiudad>
    {
        public SiniestroCiudadMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("SiniestroCiudad");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSiniestro).HasColumnName("IdSiniestro");
            this.Property(t => t.IdCiudad).HasColumnName("IdCiudad");

            // Relationships
            this.HasRequired(t => t.Ciudad)
                .WithMany(t => t.SiniestroCiudad)
                .HasForeignKey(d => d.IdCiudad);
            this.HasRequired(t => t.Siniestro)
                .WithMany(t => t.SiniestroCiudad)
                .HasForeignKey(d => d.IdSiniestro);

        }
    }
}
