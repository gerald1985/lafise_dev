using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class CasaComercialMap : EntityTypeConfiguration<CasaComercial>
    {
        public CasaComercialMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CasaComercial");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.Nuevo).HasColumnName("Nuevo");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");
            this.Property(t => t.Usado).HasColumnName("Usado");
            this.Property(t => t.Activo).HasColumnName("Activo");

            // Relationships
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.CasaComercial)
                .HasForeignKey(d => d.IdPais);

        }
    }
}
