using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class MarcaAutoMap : EntityTypeConfiguration<MarcaAuto>
    {
        public MarcaAutoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Marca)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("MarcaAuto");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Marca).HasColumnName("Marca");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");
            this.Property(t => t.Activo).HasColumnName("Activo");
        }
    }
}
