using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class ContactenoMap : EntityTypeConfiguration<Contacteno>
    {
        public ContactenoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.NombreCompleto)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Telefono)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Asunto)
                .IsRequired();

            this.Property(t => t.Mensaje)
                .IsRequired();

            this.Property(t => t.IdPais)
               .IsRequired();

            this.Property(t => t.FechaCreacion)
              .IsRequired();

            this.Property(t => t.Identificacion);

            // Table & Column Mappings
            this.ToTable("Contactenos");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.NombreCompleto).HasColumnName("NombreCompleto");
            this.Property(t => t.Identificacion).HasColumnName("Identificacion");
            this.Property(t => t.EsCliente).HasColumnName("EsCliente");
            this.Property(t => t.Telefono).HasColumnName("Telefono");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Asunto).HasColumnName("Asunto");
            this.Property(t => t.Mensaje).HasColumnName("Mensaje");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");
        }
    }
}
