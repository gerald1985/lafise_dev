using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class TipoPropiedadPlazoMap : EntityTypeConfiguration<TipoPropiedadPlazo>
    {
        public TipoPropiedadPlazoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("TipoPropiedadPlazo");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdTipoPropiedad).HasColumnName("IdTipoPropiedad");
            this.Property(t => t.Plazo).HasColumnName("Plazo");

            // Relationships
            this.HasRequired(t => t.TipoPropiedad)
                .WithMany(t => t.TipoPropiedadPlazo)
                .HasForeignKey(d => d.IdTipoPropiedad);

        }
    }
}
