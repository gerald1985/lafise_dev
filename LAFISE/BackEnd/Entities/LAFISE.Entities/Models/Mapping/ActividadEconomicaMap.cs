using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class ActividadEconomicaMap : EntityTypeConfiguration<ActividadEconomica>
    {
        public ActividadEconomicaMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ActividadEconomica");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdTipoActividad).HasColumnName("IdTipoActividad");
            this.Property(t => t.IdDatosFinancieros).HasColumnName("IdDatosFinancieros");
            this.Property(t => t.OtraActividad).HasColumnName("OtraActividad");

            // Relationships
            this.HasRequired(t => t.TipoActividad)
                .WithMany(t => t.ActividadEconomica)
                .HasForeignKey(d => d.IdTipoActividad);
            this.HasRequired(t => t.DatosFinancieros)
                .WithMany(t => t.ActividadEconomica)
                .HasForeignKey(d => d.IdDatosFinancieros);

        }
    }
}
