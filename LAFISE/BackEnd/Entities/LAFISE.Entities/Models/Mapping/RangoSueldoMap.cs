using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class RangoSueldoMap : EntityTypeConfiguration<RangoSueldo>
    {
        public RangoSueldoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("RangoSueldo");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdMoneda).HasColumnName("IdMoneda");

            // Relationships
            this.HasRequired(t => t.Moneda)
                .WithMany(t => t.RangoSueldo)
                .HasForeignKey(d => d.IdMoneda);
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.RangoSueldo)
                .HasForeignKey(d => d.IdPais);

        }
    }
}
