using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class CasaComercialModeloAutoMap : EntityTypeConfiguration<CasaComercialModeloAuto>
    {
        public CasaComercialModeloAutoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CasaComercialModeloAuto");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdCasaComercial).HasColumnName("IdCasaComercial");
            this.Property(t => t.IdModeloAuto).HasColumnName("IdModeloAuto");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");

            // Relationships
            this.HasRequired(t => t.CasaComercial)
                .WithMany(t => t.CasaComercialModeloAuto)
                .HasForeignKey(d => d.IdCasaComercial);
            this.HasRequired(t => t.ModeloAuto)
                .WithMany(t => t.CasaComercialModeloAuto)
                .HasForeignKey(d => d.IdModeloAuto);

        }
    }
}
