using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class SoportesMap : EntityTypeConfiguration<Soportes>
    {
        public SoportesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.Ruta)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Soporte");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.IdTipoSoporte).HasColumnName("IdTipoSoporte");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.Ruta).HasColumnName("Ruta");
            this.Property(t => t.EsCodeudor).HasColumnName("EsCodeudor");

            // Relationships
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.Soportes)
                .HasForeignKey(d => d.IdSolicitud);
            this.HasRequired(t => t.TipoSoporte)
                .WithMany(t => t.Soportes)
                .HasForeignKey(d => d.IdTipoSoporte);

        }
    }
}
