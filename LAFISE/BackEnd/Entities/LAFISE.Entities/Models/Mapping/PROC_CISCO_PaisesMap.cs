using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class PROC_CISCO_PaisesMap : EntityTypeConfiguration<PROC_CISCO_Paises>
    {
        public PROC_CISCO_PaisesMap()
        {
            // Primary Key
            this.HasKey(t => t.ABV);

            // Properties
            this.Property(t => t.ABV)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.NombrePais)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PROC_CISCO_Paises");
            this.Property(t => t.ABV).HasColumnName("ABV");
            this.Property(t => t.NombrePais).HasColumnName("NombrePais");
        }
    }
}
