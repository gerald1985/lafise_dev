using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class TasaCambioMap : EntityTypeConfiguration<TasaCambio>
    {
        public TasaCambioMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.SimboloCompra)
                .HasMaxLength(50);

            this.Property(t => t.SimboloVenta)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TasaCambio");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
            this.Property(t => t.ValorCompra).HasColumnName("ValorCompra");
            this.Property(t => t.ValorVenta).HasColumnName("ValorVenta");
            this.Property(t => t.SimboloCompra).HasColumnName("SimboloCompra");
            this.Property(t => t.SimboloVenta).HasColumnName("SimboloVenta");
            this.Property(t => t.Activo).HasColumnName("Activo");
            this.Property(t => t.IdPais).HasColumnName("IdPais");

            // Relationships
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.TasaCambio)
                .HasForeignKey(d => d.IdPais);

        }
    }
}
