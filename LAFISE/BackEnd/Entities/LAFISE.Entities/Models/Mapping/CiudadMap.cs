using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class CiudadMap : EntityTypeConfiguration<Ciudad>
    {
        public CiudadMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Ciudad");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.IdDepartamento).HasColumnName("IdDepartamento");

            // Relationships
            this.HasRequired(t => t.Departamento)
                .WithMany(t => t.Ciudad)
                .HasForeignKey(d => d.IdDepartamento);

        }
    }
}
