using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class LogFirmaMap : EntityTypeConfiguration<LogFirma>
    {
        public LogFirmaMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Usuario)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Modulo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Operacion)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.OperacionExitosa)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LogFirma");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.Fecha).HasColumnName("Fecha");
            this.Property(t => t.Modulo).HasColumnName("Modulo");
            this.Property(t => t.Operacion).HasColumnName("Operacion");
            this.Property(t => t.OperacionExitosa).HasColumnName("OperacionExitosa");
        }
    }
}
