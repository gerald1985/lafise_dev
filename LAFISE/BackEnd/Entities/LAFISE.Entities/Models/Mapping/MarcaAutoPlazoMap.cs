﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Models.Mapping
{
    public class MarcaAutoPlazoMap : EntityTypeConfiguration<MarcaAutoPlazo>
    {
        public MarcaAutoPlazoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("MarcaAutoPlazo");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdMarcaAuto).HasColumnName("IdMarcaAuto");
            this.Property(t => t.Plazo).HasColumnName("Plazo");

            // Relationships
            this.HasRequired(t => t.MarcaAuto)
                .WithMany(t => t.MarcaAutoPlazo)
                .HasForeignKey(d => d.IdMarcaAuto);

        }
    }
}
