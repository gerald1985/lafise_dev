using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class TarjetaMap : EntityTypeConfiguration<Tarjeta>
    {
        public TarjetaMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.TarjetaCredito)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.Imagen)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Tarjeta");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TarjetaCredito).HasColumnName("TarjetaCredito");
            this.Property(t => t.Imagen).HasColumnName("Imagen");
            this.Property(t => t.Activo).HasColumnName("Activo");
        }
    }
}
