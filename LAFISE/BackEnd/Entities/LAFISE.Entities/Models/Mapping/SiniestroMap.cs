using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class SiniestroMap : EntityTypeConfiguration<Siniestro>
    {
        public SiniestroMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.NumeroPoliza)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Detalle)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Siniestro");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.NumeroPoliza).HasColumnName("NumeroPoliza");
            this.Property(t => t.Fecha).HasColumnName("Fecha");
            this.Property(t => t.Detalle).HasColumnName("Detalle");

            // Relationships
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.Siniestro)
                .HasForeignKey(d => d.IdSolicitud);

        }
    }
}
