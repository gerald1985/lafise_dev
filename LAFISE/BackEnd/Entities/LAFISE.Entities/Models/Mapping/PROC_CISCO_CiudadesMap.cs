using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class PROC_CISCO_CiudadesMap : EntityTypeConfiguration<PROC_CISCO_Ciudades>
    {
        public PROC_CISCO_CiudadesMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod_Ciudad);

            // Properties
            this.Property(t => t.Cod_Ciudad)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Nombre_Ciudad)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PROC_CISCO_Ciudades");
            this.Property(t => t.Cod_Ciudad).HasColumnName("Cod_Ciudad");
            this.Property(t => t.Nombre_Ciudad).HasColumnName("Nombre_Ciudad");
        }
    }
}
