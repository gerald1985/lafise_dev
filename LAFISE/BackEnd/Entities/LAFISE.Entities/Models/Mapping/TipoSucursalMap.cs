using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class TipoSucursalMap : EntityTypeConfiguration<TipoSucursal>
    {
        public TipoSucursalMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.TipoSucursal1)
                .IsRequired()
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TipoSucursal");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TipoSucursal1).HasColumnName("TipoSucursal");
            this.Property(t => t.Activo).HasColumnName("Activo");
        }
    }
}
