using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class CotizadorMap : EntityTypeConfiguration<Cotizador>
    {
        public CotizadorMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired();

            this.Property(t => t.Email)
                .IsRequired();

            this.Property(t => t.Telefono)
                .IsRequired();

            this.Property(t => t.Pais)
                .IsRequired();

            this.Property(t => t.PersonalId)
                .IsRequired();

            this.Property(t => t.Edad)
                .IsRequired();

            this.Property(t => t.Marca)
                .IsRequired();

            this.Property(t => t.Modelo)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Cotizador");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Telefono).HasColumnName("Telefono");
            this.Property(t => t.Pais).HasColumnName("Pais");
            this.Property(t => t.PersonalId).HasColumnName("PersonalId");
            this.Property(t => t.Edad).HasColumnName("Edad");
            this.Property(t => t.Marca).HasColumnName("Marca");
            this.Property(t => t.Modelo).HasColumnName("Modelo");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");
        }
    }
}
