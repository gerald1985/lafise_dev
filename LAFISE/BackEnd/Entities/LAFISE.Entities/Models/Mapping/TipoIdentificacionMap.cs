using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class TipoIdentificacionMap : EntityTypeConfiguration<TipoIdentificacion>
    {
        public TipoIdentificacionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TipoIdentificacion");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.Mascara).HasColumnName("Mascara");

            // Relationships
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.TipoIdentificacion)
                .HasForeignKey(d => d.IdPais);

        }
    }
}
