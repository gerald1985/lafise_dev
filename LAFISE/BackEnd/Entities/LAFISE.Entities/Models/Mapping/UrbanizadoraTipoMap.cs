using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class UrbanizadoraTipoMap : EntityTypeConfiguration<UrbanizadoraTipo>
    {
        public UrbanizadoraTipoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("UrbanizadoraTipo");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdUrbanizadora).HasColumnName("IdUrbanizadora");
            this.Property(t => t.IdTipoPropiedad).HasColumnName("IdTipoPropiedad");

            // Relationships
            this.HasRequired(t => t.TipoPropiedad)
                .WithMany(t => t.UrbanizadoraTipo)
                .HasForeignKey(d => d.IdTipoPropiedad);
            this.HasRequired(t => t.Urbanizadora)
                .WithMany(t => t.UrbanizadoraTipo)
                .HasForeignKey(d => d.IdUrbanizadora);

        }
    }
}
