using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class SiniestroPaiMap : EntityTypeConfiguration<SiniestroPais>
    {
        public SiniestroPaiMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("SiniestroPais");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSiniestro).HasColumnName("IdSiniestro");
            this.Property(t => t.IdPais).HasColumnName("IdPais");

            // Relationships
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.SiniestroPais)
                .HasForeignKey(d => d.IdPais);
            this.HasRequired(t => t.Siniestro)
                .WithMany(t => t.SiniestroPais)
                .HasForeignKey(d => d.IdSiniestro);

        }
    }
}
