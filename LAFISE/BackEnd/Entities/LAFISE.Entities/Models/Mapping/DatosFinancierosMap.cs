using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class DatosFinancierosMap : EntityTypeConfiguration<DatosFinancieros>
    {
        public DatosFinancierosMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.PromedioIngresoMensualNombre)
                .HasMaxLength(100);

            this.Property(t => t.Estatura)
                .HasMaxLength(10);

            this.Property(t => t.Peso)
                .HasMaxLength(10);

            this.Property(t => t.Direccion)
                .HasMaxLength(200);

            this.Property(t => t.IndustriaTrabajo)
                .HasMaxLength(100);

            this.Property(t => t.DireccionOficina)
                .HasMaxLength(500);

            this.Property(t => t.TelefonoOficina)
                .HasMaxLength(50);
            this.Property(t => t.SucursalEnvio)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DatosFinancieros");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.EsCliente).HasColumnName("EsCliente");
            this.Property(t => t.PoseeNomina).HasColumnName("PoseeNomina");
            this.Property(t => t.NegocioPropio).HasColumnName("NegocioPropio");
            this.Property(t => t.Asalariado).HasColumnName("Asalariado");
            this.Property(t => t.PromedioIngresoMensualNombre).HasColumnName("PromedioIngresoMensualNombre");
            this.Property(t => t.EsZurdo).HasColumnName("EsZurdo");
            this.Property(t => t.Estatura).HasColumnName("Estatura");
            this.Property(t => t.Peso).HasColumnName("Peso");
            this.Property(t => t.EstadoCuenta).HasColumnName("EstadoCuenta");
            this.Property(t => t.Direccion).HasColumnName("Direccion");
            this.Property(t => t.LugarTrabajo).HasColumnName("LugarTrabajo");
            this.Property(t => t.IdSucursal).HasColumnName("IdSucursal");

            this.Property(t => t.IndustriaTrabajo).HasColumnName("IndustriaTrabajo");
            this.Property(t => t.Antiguedad).HasColumnName("Antiguedad");
            this.Property(t => t.DireccionOficina).HasColumnName("DireccionOficina");
            this.Property(t => t.TelefonoOficina).HasColumnName("TelefonoOficina");

            this.Property(t => t.SucursalEnvio).HasColumnName("SucursalEnvio");

            // Relationships
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.DatosFinancieros)
                .HasForeignKey(d => d.IdSolicitud);
            this.HasOptional(t => t.Sucursal)
                .WithMany(t => t.DatosFinancieros)
                .HasForeignKey(d => d.IdSucursal);

        }
    }
}
