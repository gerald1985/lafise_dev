using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class PlanFinanciacionMap : EntityTypeConfiguration<PlanFinanciacion>
    {
        public PlanFinanciacionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.OtroMarcaAuto)
                .HasMaxLength(100);

            this.Property(t => t.OtroModelo)
                .HasMaxLength(100);

            this.Property(t => t.OtroCasaComercial)
                .HasMaxLength(100);

            this.Property(t => t.NombreProyectoResidencial)
                .HasMaxLength(100);

            this.Property(t => t.NombreEmpresaInmobiliaria)
                .HasMaxLength(100);

            this.Property(t => t.DireccionVivienda)
                .HasMaxLength(500);

            this.Property(t => t.PropositoCompra)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("PlanFinanciacion");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.ValorBien).HasColumnName("ValorBien");
            this.Property(t => t.MontoFinanciar).HasColumnName("MontoFinanciar");
            this.Property(t => t.PrimaAnticipo).HasColumnName("PrimaAnticipo");
            this.Property(t => t.Plazo).HasColumnName("Plazo");
            this.Property(t => t.AutoUsado).HasColumnName("AutoUsado");
            this.Property(t => t.IdCategoria).HasColumnName("IdCategoria");
            this.Property(t => t.IdMarcaAuto).HasColumnName("IdMarcaAuto");
            this.Property(t => t.IdModelo).HasColumnName("IdModelo");
            this.Property(t => t.IdCasaComercial).HasColumnName("IdCasaComercial");
            this.Property(t => t.OtroMarcaAuto).HasColumnName("OtroMarcaAuto");
            this.Property(t => t.OtroModelo).HasColumnName("OtroModelo");
            this.Property(t => t.OtroCasaComercial).HasColumnName("OtroCasaComercial");
            this.Property(t => t.IdGarantia).HasColumnName("IdGarantia");
            this.Property(t => t.ConstruccionPropia).HasColumnName("ConstruccionPropia");
            this.Property(t => t.IdTipoPropiedad).HasColumnName("IdTipoPropiedad");
            this.Property(t => t.IdUrbanizadora).HasColumnName("IdUrbanizadora");
            this.Property(t => t.CostoPrograma).HasColumnName("CostoPrograma");
            this.Property(t => t.NombreEmpresaInmobiliaria).HasColumnName("NombreEmpresaInmobiliaria");
            this.Property(t => t.NombreProyectoResidencial).HasColumnName("NombreProyectoResidencial");
            this.Property(t => t.DireccionVivienda).HasColumnName("DireccionVivienda");
            this.Property(t => t.PropositoCompra).HasColumnName("PropositoCompra");


            // Relationships
            this.HasOptional(t => t.CasaComercial)
                .WithMany(t => t.PlanFinanciacion)
                .HasForeignKey(d => d.IdCasaComercial);
            this.HasOptional(t => t.Categoria)
                .WithMany(t => t.PlanFinanciacions)
                .HasForeignKey(d => d.IdCategoria);
            this.HasOptional(t => t.Garantia)
                .WithMany(t => t.PlanFinanciacion)
                .HasForeignKey(d => d.IdGarantia);
            this.HasOptional(t => t.MarcaAuto)
                .WithMany(t => t.PlanFinanciacion)
                .HasForeignKey(d => d.IdMarcaAuto);
            this.HasOptional(t => t.ModeloAuto)
                .WithMany(t => t.PlanFinanciacion)
                .HasForeignKey(d => d.IdModelo);
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.PlanFinanciacion)
                .HasForeignKey(d => d.IdSolicitud);
            this.HasOptional(t => t.TipoPropiedad)
                .WithMany(t => t.PlanFinanciacion)
                .HasForeignKey(d => d.IdTipoPropiedad);
            this.HasOptional(t => t.Urbanizadora)
                .WithMany(t => t.PlanFinanciacion)
                .HasForeignKey(d => d.IdUrbanizadora);

        }
    }
}
