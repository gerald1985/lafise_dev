using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class RangoSueldoDetalleMap : EntityTypeConfiguration<RangoSueldoDetalle>
    {
        public RangoSueldoDetalleMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("RangoSueldoDetalle");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdRangoSueldo).HasColumnName("IdRangoSueldo");
            this.Property(t => t.MinSueldo).HasColumnName("MinSueldo");
            this.Property(t => t.MaxSueldo).HasColumnName("MaxSueldo");

            // Relationships
            this.HasRequired(t => t.RangoSueldo)
                .WithMany(t => t.RangoSueldoDetalle)
                .HasForeignKey(d => d.IdRangoSueldo);

        }
    }
}
