﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Models.Mapping
{
    public class ParametroMap : EntityTypeConfiguration<Parametro>
    {
        public ParametroMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Valor)
              .IsRequired()
              .HasMaxLength(200);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .HasMaxLength(500);


            // Table & Column Mappings
            this.ToTable("Parametros");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.Descripcion).HasColumnName("Descripcion");
        }
    }
}
