using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class SolicitudMap : EntityTypeConfiguration<Solicitud>
    {
        public SolicitudMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Moneda)
                .HasMaxLength(30);

            this.Property(t => t.Procedencia)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Solicitud");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.IdTipoSolicitud).HasColumnName("IdTipoSolicitud");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");
            this.Property(t => t.FechaActualizacion).HasColumnName("FechaActualizacion");
            this.Property(t => t.IdTipoContacto).HasColumnName("IdTipoContacto");
            this.Property(t => t.Moneda).HasColumnName("Moneda");
            this.Property(t => t.Procedencia).HasColumnName("Procedencia");
            this.Property(t => t.IdSolicitudProducto).HasColumnName("IdSolicitudProducto");
            this.Property(t => t.BackOfficeUser).HasColumnName("BackOfficeUser");



            // Relationships
            this.HasOptional(t => t.Estado)
                .WithMany(t => t.Solicitud)
                .HasForeignKey(d => d.IdEstado);
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.Solicitud)
                .HasForeignKey(d => d.IdPais);
            this.HasOptional(t => t.TipoContacto)
                .WithMany(t => t.Solicitud)
                .HasForeignKey(d => d.IdTipoContacto);
            this.HasRequired(t => t.TipoSolicitud)
                .WithMany(t => t.Solicitud)
                .HasForeignKey(d => d.IdTipoSolicitud);

        }
    }
}
