using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class PrimaCategoriaPaisMap : EntityTypeConfiguration<PrimaCategoriaPais>
    {
        public PrimaCategoriaPaisMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PrimaCategoriaPais");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdCategoria).HasColumnName("IdCategoria");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.PorcentajePrima).HasColumnName("PorcentajePrima");

            // Relationships
            this.HasRequired(t => t.Categoria)
                .WithMany(t => t.PrimaCategoriaPais)
                .HasForeignKey(d => d.IdCategoria);
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.PrimaCategoriaPais)
                .HasForeignKey(d => d.IdPais);

        }
    }
}
