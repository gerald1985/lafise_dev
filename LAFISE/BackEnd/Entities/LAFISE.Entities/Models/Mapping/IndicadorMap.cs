using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class IndicadorMap : EntityTypeConfiguration<Indicador>
    {
        public IndicadorMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.NombreIndicador)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.SimboloActual)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.SimboloHistorico)
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("Indicador");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.NombreIndicador).HasColumnName("NombreIndicador");
            this.Property(t => t.SimboloActual).HasColumnName("SimboloActual");
            this.Property(t => t.SimboloHistorico).HasColumnName("SimboloHistorico");
            this.Property(t => t.Activo).HasColumnName("Activo");
        }
    }
}
