using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class PortalPaisMap : EntityTypeConfiguration<PortalPais>
    {
        public PortalPaisMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(50);

            // Properties
            this.Property(t => t.ZonaHoraria)
                .HasMaxLength(50);

            this.Property(t => t.Sigla)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("PortalPais");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Sigla).HasColumnName("Sigla");
            this.Property(t => t.ZonaHoraria).HasColumnName("ZonaHoraria");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
        }
    }
}
