using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class ModeloAutoMap : EntityTypeConfiguration<ModeloAuto>
    {
        public ModeloAutoMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Modelo)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Imagen)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("ModeloAuto");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Modelo).HasColumnName("Modelo");
            this.Property(t => t.Imagen).HasColumnName("Imagen");
            this.Property(t => t.IdMarcaAuto).HasColumnName("IdMarcaAuto");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");
            this.Property(t => t.Activo).HasColumnName("Activo");

            // Relationships
            this.HasRequired(t => t.MarcaAuto)
                .WithMany(t => t.ModeloAuto)
                .HasForeignKey(d => d.IdMarcaAuto);

        }
    }
}
