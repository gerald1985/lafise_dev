using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class SuscripcionBoletinMap : EntityTypeConfiguration<SuscripcionBoletin>
    {
        public SuscripcionBoletinMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("SuscripcionBoletin");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");
            this.Property(t => t.IdPais).HasColumnName("IdPais");

            // Relationships
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.SuscripcionBoletin)
                .HasForeignKey(d => d.IdPais);

        }
    }
}
