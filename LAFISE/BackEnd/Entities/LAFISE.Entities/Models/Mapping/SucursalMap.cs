using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class SucursalMap : EntityTypeConfiguration<Sucursal>
    {
        public SucursalMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("Sucursal");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdPais).HasColumnName("IdPais");
            this.Property(t => t.IdDepartamento).HasColumnName("IdDepartamento");
            this.Property(t => t.IdCiudad).HasColumnName("IdCiudad");
            this.Property(t => t.IdTipoSucursal).HasColumnName("IdTipoSucursal");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Activo).HasColumnName("Activo");

            // Relationships
            this.HasRequired(t => t.Ciudad)
                .WithMany(t => t.Sucursal)
                .HasForeignKey(d => d.IdCiudad);
            this.HasRequired(t => t.Departamento)
                .WithMany(t => t.Sucursal)
                .HasForeignKey(d => d.IdDepartamento);
            this.HasRequired(t => t.Pais)
                .WithMany(t => t.Sucursal)
                .HasForeignKey(d => d.IdPais);
            this.HasRequired(t => t.TipoSucursal)
                .WithMany(t => t.Sucursal)
                .HasForeignKey(d => d.IdTipoSucursal);

        }
    }
}
