using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class ReferenciasMap : EntityTypeConfiguration<Referencias>
    {
        public ReferenciasMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Apellidos)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Telefono)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Referencia");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.Apellidos).HasColumnName("Apellidos");
            this.Property(t => t.Telefono).HasColumnName("Telefono");

            // Relationships
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.Referencias)
                .HasForeignKey(d => d.IdSolicitud);

        }
    }
}
