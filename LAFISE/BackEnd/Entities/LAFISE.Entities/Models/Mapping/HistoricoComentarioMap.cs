using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class HistoricoComentarioMap : EntityTypeConfiguration<HistoricoComentario>
    {
        public HistoricoComentarioMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Comentario)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("HistoricoComentario");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.IdEstado).HasColumnName("IdEstado");
            this.Property(t => t.Comentario).HasColumnName("Comentario");
            this.Property(t => t.FechaCreacion).HasColumnName("FechaCreacion");

            // Relationships
            this.HasRequired(t => t.Estado)
                .WithMany(t => t.HistoricoComentario)
                .HasForeignKey(d => d.IdEstado);
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.HistoricoComentario)
                .HasForeignKey(d => d.IdSolicitud);

        }
    }
}
