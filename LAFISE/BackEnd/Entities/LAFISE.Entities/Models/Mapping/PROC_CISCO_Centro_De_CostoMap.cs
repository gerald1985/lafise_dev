using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class PROC_CISCO_Centro_De_CostoMap : EntityTypeConfiguration<PROC_CISCO_Centro_De_Costo>
    {
        public PROC_CISCO_Centro_De_CostoMap()
        {
            // Primary Key
            this.HasKey(t => t.id_centro);

            // Properties
            this.Property(t => t.id_centro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Nombre_Centro)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("PROC_CISCO_Centro_De_Costo");
            this.Property(t => t.id_centro).HasColumnName("id_centro");
            this.Property(t => t.Nombre_Centro).HasColumnName("Nombre_Centro");
        }
    }
}
