using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class PROC_CISCO_OcupacionesMap : EntityTypeConfiguration<PROC_CISCO_Ocupaciones>
    {
        public PROC_CISCO_OcupacionesMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod_Ocupacion);

            // Properties
            this.Property(t => t.Cod_Ocupacion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Nombre_Ocupacion)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("PROC_CISCO_Ocupaciones");
            this.Property(t => t.Cod_Ocupacion).HasColumnName("Cod_Ocupacion");
            this.Property(t => t.Nombre_Ocupacion).HasColumnName("Nombre_Ocupacion");
        }
    }
}
