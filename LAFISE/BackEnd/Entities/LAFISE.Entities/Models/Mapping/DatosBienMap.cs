using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LAFISE.Entities.Models.Mapping
{
    public class DatosBienMap : EntityTypeConfiguration<DatosBien>
    {
        public DatosBienMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("DatosBien");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IdSolicitud).HasColumnName("IdSolicitud");
            this.Property(t => t.ValorBien).HasColumnName("ValorBien");
            this.Property(t => t.IdTipoVehiculo).HasColumnName("IdTipoVehiculo");
            this.Property(t => t.IdMarcaAuto).HasColumnName("IdMarcaAuto");
            this.Property(t => t.IdModelo).HasColumnName("IdModelo");
            this.Property(t => t.IdTipoBien).HasColumnName("IdTipoBien");

            // Relationships
            this.HasOptional(t => t.ModeloAuto)
                .WithMany(t => t.DatosBien)
                .HasForeignKey(d => d.IdModelo);
            this.HasOptional(t => t.TipoVehiculo)
                .WithMany(t => t.DatosBiens)
                .HasForeignKey(d => d.IdTipoVehiculo);
            this.HasOptional(t => t.MarcaAuto)
                .WithMany(t => t.DatosBien)
                .HasForeignKey(d => d.IdMarcaAuto);
            this.HasRequired(t => t.Solicitud)
                .WithMany(t => t.DatosBien)
                .HasForeignKey(d => d.IdSolicitud);
            this.HasRequired(t => t.TipoBien)
                .WithMany(t => t.DatosBiens)
                .HasForeignKey(d => d.IdTipoBien);

        }
    }
}
