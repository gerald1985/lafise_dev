using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class CasaComercialModeloAuto
    {
        public long Id { get; set; }
        public long IdCasaComercial { get; set; }
        public long IdModeloAuto { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public virtual CasaComercial CasaComercial { get; set; }
        public virtual ModeloAuto ModeloAuto { get; set; }
    }
}
