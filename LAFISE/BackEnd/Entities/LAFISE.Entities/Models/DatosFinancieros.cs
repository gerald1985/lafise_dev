using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class DatosFinancieros
    {
        public DatosFinancieros()
        {
            this.ActividadEconomica = new List<ActividadEconomica>();
        }

        public long Id { get; set; }
        public long IdSolicitud { get; set; }
        public bool EsCliente { get; set; }
        public bool PoseeNomina { get; set; }
        public Nullable<bool> NegocioPropio { get; set; }
        public Nullable<bool> Asalariado { get; set; }
        public string PromedioIngresoMensualNombre { get; set; }
        public Nullable<bool> EsZurdo { get; set; }
        public string Estatura { get; set; }
        public string Peso { get; set; }
        public Nullable<bool> EstadoCuenta { get; set; }
        public string Direccion { get; set; }
        public Nullable<long> IdSucursal { get; set; }
        public virtual ICollection<ActividadEconomica> ActividadEconomica { get; set; }
        public virtual Solicitud Solicitud { get; set; }
        public string LugarTrabajo { get; set; }
        public virtual Sucursal Sucursal { get; set; }

        public string IndustriaTrabajo { get; set; }
        public Nullable<int> Antiguedad { get; set; }
        public string DireccionOficina { get; set; }
        public string TelefonoOficina { get; set; }

        public string SucursalEnvio { get; set; }
    }
}
