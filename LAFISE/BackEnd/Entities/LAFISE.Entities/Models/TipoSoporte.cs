using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoSoporte
    {
        public TipoSoporte()
        {
            this.Soportes = new List<Soportes>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Soportes> Soportes { get; set; }
    }
}
