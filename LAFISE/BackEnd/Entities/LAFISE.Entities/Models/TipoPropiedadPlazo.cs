using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoPropiedadPlazo
    {
        public long Id { get; set; }
        public long IdTipoPropiedad { get; set; }
        public int Plazo { get; set; }
        public virtual TipoPropiedad TipoPropiedad { get; set; }
    }
}
