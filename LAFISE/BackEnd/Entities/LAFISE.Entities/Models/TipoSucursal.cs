using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoSucursal
    {
        public TipoSucursal()
        {
            this.Sucursal = new List<Sucursal>();
        }

        public long Id { get; set; }
        public string TipoSucursal1 { get; set; }
        public bool Activo { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
    }
}
