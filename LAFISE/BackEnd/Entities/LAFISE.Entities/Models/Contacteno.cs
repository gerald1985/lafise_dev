using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Contacteno
    {
        public long Id { get; set; }
        public string NombreCompleto { get; set; }
        public string Identificacion { get; set; }
        public bool EsCliente { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Asunto { get; set; }
        public string Mensaje { get; set; }
        public long IdPais { get; set;}
        public DateTime FechaCreacion { get; set; }
    }
}
