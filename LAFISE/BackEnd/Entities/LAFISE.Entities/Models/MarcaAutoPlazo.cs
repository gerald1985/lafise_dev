﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAFISE.Entities.Models
{
    public partial class MarcaAutoPlazo
    {
        public long Id { get; set; }
        public long IdMarcaAuto { get; set; }
        public int Plazo { get; set; }
        public virtual MarcaAuto MarcaAuto { get; set; }
    }
}
