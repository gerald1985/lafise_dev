using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using LAFISE.Entities.Models.Mapping;

namespace LAFISE.Entities.Models
{
    public partial class LafiseContext : DbContext
    {
        static LafiseContext()
        {
            Database.SetInitializer<LafiseContext>(null);
        }

        public LafiseContext()
            : base("Name=LafiseContext")
        {
        }

        public DbSet<ActividadEconomica> ActividadEconomica { get; set; }
        public DbSet<Contacteno> Contactenos { get; set; }
        public DbSet<DatosBien> DatosBien { get; set; }
        public DbSet<DatosFinancieros> DatosFinancieros { get; set; }
        public DbSet<DatosPersonales> DatosPersonales { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<Garantia> Garantias { get; set; }
        public DbSet<HistoricoComentario> HistoricoComentario { get; set; }
        public DbSet<Indicador> Indicadors { get; set; }
        public DbSet<LogFirma> LogFirma { get; set; }
        public DbSet<PlanFinanciacion> PlanFinanciacion { get; set; }
        public DbSet<PortalPais> PortalPais { get; set; }
        public DbSet<Referencias> Referencias { get; set; }
        public DbSet<Siniestro> Siniestro { get; set; }
        public DbSet<SiniestroCiudad> SiniestroCiudad { get; set; }
        public DbSet<SiniestroPais> SiniestroPais { get; set; }
        public DbSet<Solicitud> Solicitud { get; set; }
        public DbSet<Soportes> Soportes { get; set; }
        public DbSet<TipoActividad> TipoActividad { get; set; }
        public DbSet<TipoContacto> TipoContacto { get; set; }
        public DbSet<TipoIdentificacion> TipoIdentificacion { get; set; }
        public DbSet<TipoSolicitud> TipoSolicitud { get; set; }
        public DbSet<TipoSoporte> TipoSoporte { get; set; }
        public DbSet<CasaComercial> CasaComercial { get; set; }
        public DbSet<CasaComercialModeloAuto> CasaComercialModeloAuto { get; set; }
        public DbSet<MarcaAuto> MarcaAuto { get; set; }
        public DbSet<ModeloAuto> ModeloAuto { get; set; }
        public DbSet<Pais> Pais { get; set; }
        public DbSet<RangoSueldo> RangoSueldo { get; set; }
        public DbSet<RangoSueldoDetalle> RangoSueldoDetalle { get; set; }
        public DbSet<TipoPropiedad> TipoPropiedad { get; set; }
        public DbSet<TipoPropiedadPlazo> TipoPropiedadPlazo { get; set; }
        public DbSet<Urbanizadora> Urbanizadora { get; set; }
        public DbSet<UrbanizadoraTipo> UrbanizadoraTipo { get; set; }
        public DbSet<Moneda> Moneda { get; set; }
        public DbSet<Tarjeta> Tarjeta { get; set; }
        public DbSet<TipoSucursal> TipoSucursal { get; set; }
        public DbSet<Sucursal> Sucursal { get; set; }
        public DbSet<Departamento> Departamento { get; set; }
        public DbSet<Ciudad> Ciudad { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<TipoEstado> TipoEstadoes { get; set; }
        public DbSet<TipoVehiculo> TipoVehiculo { get; set; }
        public DbSet<Parametro> Parametro { get; set; }
        public DbSet<PrimaCategoriaPais> PrimaCategoriaPais { get; set; }
        public DbSet<MarcaAutoPlazo> MarcaAutoPlazo { get; set; }
        public DbSet<TipoBien> TipoBien { get; set; }
        public DbSet<SuscripcionBoletin> SuscripcionBoletin { get; set; }
        public DbSet<Cotizador> Cotizador { get; set; }
        public DbSet<TasaCambio> TasaCambio { get; set; }


        public DbSet<PROC_CISCO_Centro_De_Costo> PROC_CISCO_Centro_De_Costo { get; set; }
        public DbSet<PROC_CISCO_Paises> PROC_CISCO_Paises { get; set; }
        public DbSet<PROC_CISCO_Ciudades> PROC_CISCO_Ciudades { get; set; }
        public DbSet<PROC_CISCO_Ocupaciones> PROC_CISCO_Ocupaciones { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ActividadEconomicaMap());
            modelBuilder.Configurations.Add(new ContactenoMap());
            modelBuilder.Configurations.Add(new DatosBienMap());
            modelBuilder.Configurations.Add(new DatosFinancierosMap());
            modelBuilder.Configurations.Add(new DatosPersonalesMap());
            modelBuilder.Configurations.Add(new EstadoMap());
            modelBuilder.Configurations.Add(new GarantiaMap());
            modelBuilder.Configurations.Add(new HistoricoComentarioMap());
            modelBuilder.Configurations.Add(new IndicadorMap());
            modelBuilder.Configurations.Add(new LogFirmaMap());
            modelBuilder.Configurations.Add(new PlanFinanciacionMap());
            modelBuilder.Configurations.Add(new PortalPaisMap());
            modelBuilder.Configurations.Add(new ReferenciasMap());
            modelBuilder.Configurations.Add(new SiniestroMap());
            modelBuilder.Configurations.Add(new SiniestroCiudadMap());
            modelBuilder.Configurations.Add(new SiniestroPaiMap());
            modelBuilder.Configurations.Add(new SolicitudMap());
            modelBuilder.Configurations.Add(new SoportesMap());
            modelBuilder.Configurations.Add(new SucursalMap());
            modelBuilder.Configurations.Add(new TipoActividadMap());
            modelBuilder.Configurations.Add(new TipoContactoMap());
            modelBuilder.Configurations.Add(new TipoIdentificacionMap());
            modelBuilder.Configurations.Add(new TipoSolicitudMap());
            modelBuilder.Configurations.Add(new TipoSoporteMap());
            modelBuilder.Configurations.Add(new CasaComercialMap());
            modelBuilder.Configurations.Add(new CasaComercialModeloAutoMap());
            modelBuilder.Configurations.Add(new MarcaAutoMap());
            modelBuilder.Configurations.Add(new ModeloAutoMap());
            modelBuilder.Configurations.Add(new PaisMap());
            modelBuilder.Configurations.Add(new RangoSueldoMap());
            modelBuilder.Configurations.Add(new RangoSueldoDetalleMap());
            modelBuilder.Configurations.Add(new TipoPropiedadMap());
            modelBuilder.Configurations.Add(new TipoPropiedadPlazoMap());
            modelBuilder.Configurations.Add(new UrbanizadoraMap());
            modelBuilder.Configurations.Add(new UrbanizadoraTipoMap());
            modelBuilder.Configurations.Add(new MonedaMap());
            modelBuilder.Configurations.Add(new TarjetaMap());
            modelBuilder.Configurations.Add(new TipoSucursalMap());
            modelBuilder.Configurations.Add(new DepartamentoMap());
            modelBuilder.Configurations.Add(new CiudadMap());
            modelBuilder.Configurations.Add(new CategoriaMap());
            modelBuilder.Configurations.Add(new TipoEstadoMap());
            modelBuilder.Configurations.Add(new TipoVehiculoMap());
            modelBuilder.Configurations.Add(new ParametroMap());
            modelBuilder.Configurations.Add(new PrimaCategoriaPaisMap());
            modelBuilder.Configurations.Add(new MarcaAutoPlazoMap());
            modelBuilder.Configurations.Add(new TipoBienMap());
            modelBuilder.Configurations.Add(new SuscripcionBoletinMap());
            modelBuilder.Configurations.Add(new CotizadorMap());
            modelBuilder.Configurations.Add(new TasaCambioMap());


            modelBuilder.Configurations.Add(new PROC_CISCO_Centro_De_CostoMap());
            modelBuilder.Configurations.Add(new PROC_CISCO_PaisesMap());
            modelBuilder.Configurations.Add(new PROC_CISCO_CiudadesMap());
            modelBuilder.Configurations.Add(new PROC_CISCO_OcupacionesMap());
        }
    }
}
