using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class HistoricoComentario
    {
        public long Id { get; set; }
        public long IdSolicitud { get; set; }
        public long IdEstado { get; set; }
        public string Comentario { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual Solicitud Solicitud { get; set; }
    }
}
