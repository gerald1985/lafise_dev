using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class PROC_CISCO_Paises
    {
        public string ABV { get; set; }
        public string NombrePais { get; set; }
    }
}
