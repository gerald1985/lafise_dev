using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Urbanizadora
    {
        public Urbanizadora()
        {
            this.PlanFinanciacion = new List<PlanFinanciacion>();
            this.UrbanizadoraTipo = new List<UrbanizadoraTipo>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public long IdPais { get; set; }
        public bool Activo { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacion { get; set; }
        public virtual ICollection<UrbanizadoraTipo> UrbanizadoraTipo { get; set; }
    }
}
