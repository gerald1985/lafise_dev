using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoBien
    {
        public TipoBien()
        {
            this.DatosBiens = new List<DatosBien>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<DatosBien> DatosBiens { get; set; }
    }
}
