using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Tarjeta
    {
        public long Id { get; set; }
        public string TarjetaCredito { get; set; }
        public string Imagen { get; set; }
        public bool Activo { get; set; }
    }
}
