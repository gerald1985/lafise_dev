using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoIdentificacion
    {
        public TipoIdentificacion()
        {
            this.DatosPersonales = new List<DatosPersonales>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public long IdPais { get; set; }
        public string Mascara { get; set; }
        public virtual ICollection<DatosPersonales> DatosPersonales { get; set; }
        public virtual Pais Pais { get; set; }

    }
}
