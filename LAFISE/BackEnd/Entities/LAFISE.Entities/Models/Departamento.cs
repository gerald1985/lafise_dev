using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Departamento
    {
        public Departamento()
        {
            this.Ciudad = new List<Ciudad>();
            this.Sucursal = new List<Sucursal>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public long IdPais { get; set; }
        public virtual ICollection<Ciudad> Ciudad { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
    }
}
