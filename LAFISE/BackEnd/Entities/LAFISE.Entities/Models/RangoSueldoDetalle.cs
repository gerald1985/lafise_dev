using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class RangoSueldoDetalle
    {
        public long Id { get; set; }
        public long IdRangoSueldo { get; set; }
        public double MinSueldo { get; set; }
        public double MaxSueldo { get; set; }
        public virtual RangoSueldo RangoSueldo { get; set; }
    }
}
