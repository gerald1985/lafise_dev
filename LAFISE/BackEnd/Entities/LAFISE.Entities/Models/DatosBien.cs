using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class DatosBien
    {
        public long Id { get; set; }
        public long IdSolicitud { get; set; }
        public decimal ValorBien { get; set; }
        public Nullable<long> IdTipoVehiculo { get; set; }
        public Nullable<long> IdMarcaAuto { get; set; }
        public Nullable<long> IdModelo { get; set; }
        public long IdTipoBien { get; set; }
        public virtual ModeloAuto ModeloAuto { get; set; }
        public virtual TipoVehiculo TipoVehiculo { get; set; }
        public virtual MarcaAuto MarcaAuto { get; set; }
        public virtual Solicitud Solicitud { get; set; }
        public virtual TipoBien TipoBien { get; set; }
    }
}
