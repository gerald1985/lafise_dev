using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Ciudad
    {
        public Ciudad()
        {
            this.SiniestroCiudad = new List<SiniestroCiudad>();
            this.Sucursal = new List<Sucursal>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public long IdDepartamento { get; set; }
        public virtual Departamento Departamento { get; set; }
        public virtual ICollection<SiniestroCiudad> SiniestroCiudad { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
    }
}
