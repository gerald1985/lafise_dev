using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class PlanFinanciacion
    {
        public long Id { get; set; }
        public long IdSolicitud { get; set; }
        public Nullable<decimal> ValorBien { get; set; }
        public decimal MontoFinanciar { get; set; }
        public Nullable<decimal> PrimaAnticipo { get; set; }
        public int Plazo { get; set; }
        public Nullable<bool> AutoUsado { get; set; }
        public Nullable<long> IdCategoria { get; set; }
        public Nullable<long> IdMarcaAuto { get; set; }
        public Nullable<long> IdModelo { get; set; }
        public Nullable<long> IdCasaComercial { get; set; }
        public string OtroMarcaAuto { get; set; }
        public string OtroModelo { get; set; }
        public string OtroCasaComercial { get; set; }
        public Nullable<long> IdGarantia { get; set; }
        public Nullable<bool> ConstruccionPropia { get; set; }
        public Nullable<long> IdTipoPropiedad { get; set; }
        public Nullable<long> IdUrbanizadora { get; set; }
        public Nullable<decimal> CostoPrograma { get; set; }
        public virtual CasaComercial CasaComercial { get; set; }
        public virtual Categoria Categoria { get; set; }
        public virtual Garantia Garantia { get; set; }
        public virtual MarcaAuto MarcaAuto { get; set; }
        public virtual ModeloAuto ModeloAuto { get; set; }
        public virtual Solicitud Solicitud { get; set; }
        public virtual TipoPropiedad TipoPropiedad { get; set; }
        public virtual Urbanizadora Urbanizadora { get; set; }

        public string NombreEmpresaInmobiliaria { get; set; }
        public string NombreProyectoResidencial { get; set; }
        public string DireccionVivienda { get; set; }
        public string PropositoCompra { get; set; }
    }
}
