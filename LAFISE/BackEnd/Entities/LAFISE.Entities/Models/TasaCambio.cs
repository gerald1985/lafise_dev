using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TasaCambio
    {
        public long Id { get; set; }
        public string Descripcion { get; set; }
        public double ValorCompra { get; set; }
        public double ValorVenta { get; set; }
        public string SimboloCompra { get; set; }
        public string SimboloVenta { get; set; }
        public bool Activo { get; set; }
        public long IdPais { get; set; }
        public virtual Pais Pais { get; set; }
    }
}
