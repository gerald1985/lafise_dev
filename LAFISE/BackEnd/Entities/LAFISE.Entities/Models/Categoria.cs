using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Categoria
    {
        public Categoria()
        {
            this.PlanFinanciacions = new List<PlanFinanciacion>();
            this.PrimaCategoriaPais = new List<PrimaCategoriaPais>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public long IdTipoSolicitud { get; set; }
        public virtual TipoSolicitud TipoSolicitud { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacions { get; set; }
        public virtual ICollection<PrimaCategoriaPais> PrimaCategoriaPais { get; set; }
    }
}
