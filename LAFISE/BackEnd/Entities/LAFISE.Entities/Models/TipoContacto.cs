using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoContacto
    {
        public TipoContacto()
        {
            this.Solicitud = new List<Solicitud>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Solicitud> Solicitud { get; set; }
    }
}
