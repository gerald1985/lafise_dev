using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Solicitud
    {
        public Solicitud()
        {
            this.DatosBien = new List<DatosBien>();
            this.DatosFinancieros = new List<DatosFinancieros>();
            this.DatosPersonales = new List<DatosPersonales>();
            this.HistoricoComentario = new List<HistoricoComentario>();
            this.PlanFinanciacion = new List<PlanFinanciacion>();
            this.Referencias = new List<Referencias>();
            this.Siniestro = new List<Siniestro>();
            this.Soportes = new List<Soportes>();
        }

        public long Id { get; set; }
        public Nullable<long> IdEstado { get; set; }
        public long IdTipoSolicitud { get; set; }
        public long IdPais { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaActualizacion { get; set; }
        public Nullable<long> IdTipoContacto { get; set; }
        public string Moneda { get; set; }
        public string Procedencia { get; set; }
        public long IdSolicitudProducto { get; set; }
        public string BackOfficeUser { get; set;  }


        public virtual ICollection<DatosBien> DatosBien { get; set; }
        public virtual ICollection<DatosFinancieros> DatosFinancieros { get; set; }
        public virtual ICollection<DatosPersonales> DatosPersonales { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual ICollection<HistoricoComentario> HistoricoComentario { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacion { get; set; }
        public virtual ICollection<Referencias> Referencias { get; set; }
        public virtual ICollection<Siniestro> Siniestro { get; set; }
        public virtual TipoContacto TipoContacto { get; set; }
        public virtual TipoSolicitud TipoSolicitud { get; set; }
        public virtual ICollection<Soportes> Soportes { get; set; }

    }
}
