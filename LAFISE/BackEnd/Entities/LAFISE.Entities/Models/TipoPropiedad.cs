using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoPropiedad
    {
        public TipoPropiedad()
        {
            this.PlanFinanciacion = new List<PlanFinanciacion>();
            this.TipoPropiedadPlazo = new List<TipoPropiedadPlazo>();
            this.UrbanizadoraTipo = new List<UrbanizadoraTipo>();
        }

        public long Id { get; set; }
        public string Tipo { get; set; }
        public bool Activo { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacion { get; set; }
        public virtual ICollection<TipoPropiedadPlazo> TipoPropiedadPlazo { get; set; }
        public virtual ICollection<UrbanizadoraTipo> UrbanizadoraTipo { get; set; }
    }
}
