using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Garantia
    {
        public Garantia()
        {
            this.PlanFinanciacion = new List<PlanFinanciacion>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacion { get; set; }
    }
}
