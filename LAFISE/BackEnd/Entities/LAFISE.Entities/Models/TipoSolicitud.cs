using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class TipoSolicitud
    {
        public TipoSolicitud()
        {
            this.Categorias = new List<Categoria>();
            this.Solicitud = new List<Solicitud>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Categoria> Categorias { get; set; }
        public virtual ICollection<Solicitud> Solicitud { get; set; }
    }
}
