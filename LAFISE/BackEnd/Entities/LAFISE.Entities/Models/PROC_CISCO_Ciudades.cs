using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class PROC_CISCO_Ciudades
    {
        public long Cod_Ciudad { get; set; }
        public string Nombre_Ciudad { get; set; }
    }
}
