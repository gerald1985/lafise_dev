using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Pais
    {
        public Pais()
        {
            this.CasaComercial = new List<CasaComercial>();
            this.Urbanizadora = new List<Urbanizadora>();
            this.Departamento = new List<Departamento>();
            this.RangoSueldo = new List<RangoSueldo>();
            this.SiniestroPais = new List<SiniestroPais>();
            this.Solicitud = new List<Solicitud>();
            this.Sucursal = new List<Sucursal>();
            this.TipoIdentificacion = new List<TipoIdentificacion>();
            this.SuscripcionBoletin = new List<SuscripcionBoletin>();
            this.TasaCambio = new List<TasaCambio>();

        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public virtual ICollection<CasaComercial> CasaComercial { get; set; }
        public virtual ICollection<Departamento> Departamento { get; set; }
        public virtual ICollection<RangoSueldo> RangoSueldo { get; set; }
        public virtual ICollection<SiniestroPais> SiniestroPais { get; set; }
        public virtual ICollection<Solicitud> Solicitud { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
        public virtual ICollection<TipoIdentificacion> TipoIdentificacion { get; set; }
        public virtual ICollection<Urbanizadora> Urbanizadora { get; set; }
        public virtual ICollection<PrimaCategoriaPais> PrimaCategoriaPais { get; set; }
        public virtual ICollection<SuscripcionBoletin> SuscripcionBoletin { get; set; }
        public virtual ICollection<TasaCambio> TasaCambio { get; set; }
    }
}
