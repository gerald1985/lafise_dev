using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class LogFirma
    {
        public long Id { get; set; }
        public string Usuario { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Modulo { get; set; }
        public string Operacion { get; set; }
        public string OperacionExitosa { get; set; }
    }
}
