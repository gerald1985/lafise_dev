using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class CasaComercial
    {
        public CasaComercial()
        {
            this.CasaComercialModeloAuto = new List<CasaComercialModeloAuto>();
            this.PlanFinanciacion = new List<PlanFinanciacion>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public long IdPais { get; set; }
        public bool Nuevo { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public bool Usado { get; set; }
        public bool Activo { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual ICollection<CasaComercialModeloAuto> CasaComercialModeloAuto { get; set; }
        public virtual ICollection<PlanFinanciacion> PlanFinanciacion { get; set; }
    }
}
