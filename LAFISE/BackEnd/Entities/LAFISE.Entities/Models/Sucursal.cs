using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Sucursal
    {
        public Sucursal()
        {
            this.DatosFinancieros = new List<DatosFinancieros>();
        }

        public long Id { get; set; }
        public long IdPais { get; set; }
        public long IdDepartamento { get; set; }
        public long IdCiudad { get; set; }
        public long IdTipoSucursal { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
        public virtual Ciudad Ciudad { get; set; }
        public virtual ICollection<DatosFinancieros> DatosFinancieros { get; set; }
        public virtual Departamento Departamento { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual TipoSucursal TipoSucursal { get; set; }
    }
}
