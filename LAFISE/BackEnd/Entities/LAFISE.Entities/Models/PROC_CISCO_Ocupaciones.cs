using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class PROC_CISCO_Ocupaciones
    {
        public long Cod_Ocupacion { get; set; }
        public string Nombre_Ocupacion { get; set; }
    }
}
