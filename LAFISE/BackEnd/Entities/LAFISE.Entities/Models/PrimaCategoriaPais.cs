using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class PrimaCategoriaPais
    {
        public long Id { get; set; }
        public long IdCategoria { get; set; }
        public long IdPais { get; set; }
        public decimal PorcentajePrima { get; set; }
        public virtual Categoria Categoria { get; set; }
        public virtual Pais Pais { get; set; }
    }
}
