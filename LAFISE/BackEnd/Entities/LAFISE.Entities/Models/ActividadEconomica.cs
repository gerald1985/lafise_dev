using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class ActividadEconomica
    {
        public long Id { get; set; }
        public long IdTipoActividad { get; set; }
        public long IdDatosFinancieros { get; set; }
        public string OtraActividad { get; set; }
        public virtual TipoActividad TipoActividad { get; set; }
        public virtual DatosFinancieros DatosFinancieros { get; set; }
    }
}
