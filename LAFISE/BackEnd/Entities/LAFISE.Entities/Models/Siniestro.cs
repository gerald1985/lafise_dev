using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Siniestro
    {
        public Siniestro()
        {
            this.SiniestroCiudad = new List<SiniestroCiudad>();
            this.SiniestroPais = new List<SiniestroPais>();
        }

        public long Id { get; set; }
        public long IdSolicitud { get; set; }
        public string NumeroPoliza { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Detalle { get; set; }
        public virtual Solicitud Solicitud { get; set; }
        public virtual ICollection<SiniestroCiudad> SiniestroCiudad { get; set; }
        public virtual ICollection<SiniestroPais> SiniestroPais { get; set; }
    }
}
