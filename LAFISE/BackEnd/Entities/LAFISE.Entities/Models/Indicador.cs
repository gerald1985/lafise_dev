using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class Indicador
    {
        public long Id { get; set; }
        public string NombreIndicador { get; set; }
        public string SimboloActual { get; set; }
        public string SimboloHistorico { get; set; }
        public bool Activo { get; set; }
    }
}
