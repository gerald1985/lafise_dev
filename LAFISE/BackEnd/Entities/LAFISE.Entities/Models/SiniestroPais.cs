using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class SiniestroPais
    {
        public long Id { get; set; }
        public long IdSiniestro { get; set; }
        public long IdPais { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual Siniestro Siniestro { get; set; }
    }
}
