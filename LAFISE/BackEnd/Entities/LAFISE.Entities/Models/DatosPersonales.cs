using System;
using System.Collections.Generic;

namespace LAFISE.Entities.Models
{
    public partial class DatosPersonales
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public bool Masculino { get; set; }
        public string Nacionalidad { get; set; }
        public string CiudadResidencia { get; set; }
        public long IdTipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string Profesion { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public long IdSolicitud { get; set; }
        public Nullable<bool> EsCodeudor { get; set; }
        public string TipoDocumentoSolicitud { get; set; }
        public virtual Solicitud Solicitud { get; set; }
        public virtual TipoIdentificacion TipoIdentificacion { get; set; }

        public string EstadoCivil { get; set; }
        public Nullable<int> Dependientes { get; set; }
        public string DireccionResidencia { get; set; }
        public string TipoResidencia { get; set; }
        public string EstadoResidencia { get; set; }
        public Nullable<int> AniosResidencia { get; set; }
        public string Educacion { get; set; }

        /* INICIA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
        public string SegundoNombre { get;  set; }
        public string SegundoApellido { get;  set; }
        public string PrimerNombreBeneficiario { get;  set; }
        public string SegundoNombreBeneficiario { get;  set; }
        public string PrimerApellidoBeneficiario { get;  set; }
        public string SegundoApellidoBeneficiario { get;  set; }
        public Nullable<Int64> TipoIdBeneficiario { get;  set; }
        public string NumeroIdBeneficiario { get;  set; }
        public string DireccionBeneficiario { get;  set; }
        public string ParentescoBeneficiario { get;  set; }
        public string PrimerNombreTutor { get;  set; }
        public string SegundoNombreTutor { get;  set; }
        public string PrimerApellidoTutor { get;  set; }
        public string SegundoApellidoTutor { get;  set; }
        public Nullable<Int64> TipoIdTutor { get;  set; }
        public string NumeroIdTutor { get;  set; }
        public string ParentescoTutor { get;  set; }
        public string TelefonoTutor { get;  set; }
        public string EmailTutor { get;  set; }
        /* FINALIZA DATOS UTILIZADOS POR LA TAREA QUE AUTOMATIZARA LA GESTION DE TARJETAS PREPAGO */
    }
}
