﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LAFISE.Common.FrontEnd
{
    /// <summary>
    /// Clase para el manejo de los valores constantes del Backend
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Este es la ruta de archivos de tarjeta
        /// </summary>
        public const string RUTA_ARCHIVOS_TARJETA = "RUTA_ARCHIVOS_TARJETA";

        /// <summary>
        /// Este es la ruta de archivos de marca auto
        /// </summary>
        public const string RUTA_ARCHIVOS_MODELOS = "RUTA_ARCHIVOS_MODELOS";


        /// <summary>
        /// Este es la ruta de archivos thumb de marca auto
        /// </summary>
        public const string RUTA_ARCHIVOS_THUMB_MODELOS = "RUTA_ARCHIVOS_THUMB_MODELOS";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de marca auto
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_MARCA = "RUTA_ARCHIVOS_FORM_MARCA";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de prestamo hipotecario
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_PRESTAMO_HIPOTECARIO = "RUTA_ARCHIVOS_FORM_PRESTAMO_HIPOTECARIO";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de prestamo educativo
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_PRESTAMO_EDUCATIVO = "RUTA_ARCHIVOS_FORM_PRESTAMO_EDUCATIVO";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de prestamo personal
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_PRESTAMO_PERSONAL = "RUTA_ARCHIVOS_FORM_PRESTAMO_PERSONAL";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de apertura de cuenta
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_APERTURA_CUENTA = "RUTA_ARCHIVOS_FORM_APERTURA_CUENTA";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de solicitud de tarjeta de crédito
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_TARJETA_CREDITO = "RUTA_ARCHIVOS_FORM_TARJETA_CREDITO";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de solicitud de tarjeta débito
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_TARJETA_DEBITO = "RUTA_ARCHIVOS_FORM_TARJETA_DEBITO";

        /// <summary>
        ///  Este es la ruta de archivos del formulario de solicitud de seguro vehicular
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_SOLICITUD_VEHICULAR = "RUTA_ARCHIVOS_FORM_SOLICITUD_VEHICULAR";


        /// <summary>
        ///  Este es la ruta de archivos del formulario de solicitud de seguro vida accidentes
        /// </summary>
        public const string RUTA_ARCHIVOS_FORM_SOLICITUD_VIDA_ACCIDENTES = "RUTA_ARCHIVOS_FORM_SOLICITUD_VIDA_ACCIDENTES";

    }
}
