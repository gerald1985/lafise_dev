﻿using System;

namespace LAFISE.Common.FrontEnd
{
    public class EnumUtils : Attribute
    {
        /// <summary>
        /// Atributo
        /// </summary>
        private string _stringValue;

        /// <summary>
        /// Atributo
        /// </summary>
        public string StringValue
        {
            get { return _stringValue; }
            set { _stringValue = value; }
        }

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="value"></param>
        public EnumUtils(string value) { StringValue = value; }

        /// <summary>
        /// Metodo encargado de retornar el value de un Enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringValue(Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());
            var attribs = fieldInfo.GetCustomAttributes(typeof(EnumUtils), false) as EnumUtils[];
            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }
    }
}
