﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LAFISE.Common.FrontEnd
{
    /// <summary>
    /// Clase para el manejo de los métodos de extensión
    /// </summary>
    public static class Extensions
    {

        /// <summary>
        /// Obtiene el valor del atributo de un enumerador
        /// </summary>
        /// <param name="value">Valor del enumerador</param>
        /// <returns>string resultado</returns>
        public static string GetStringValue(this Enum value)
        {
            Type type = value.GetType();

            FieldInfo fieldInfo = type.GetField(value.ToString());

            EnumUtils[] attribs = fieldInfo.GetCustomAttributes(typeof(EnumUtils), false) as EnumUtils[];

            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }
    }
}
