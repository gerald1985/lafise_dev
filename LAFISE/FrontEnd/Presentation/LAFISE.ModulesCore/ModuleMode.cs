﻿namespace LAFISE.ModulesCore
{
    /// <summary>
    /// Representa los modos en los cuales puede estar un módulo de DNN
    /// </summary>
    public enum ModuleMode
    {
        /// <summary>
        /// Modo vista
        /// </summary>
        View,

        /// <summary>
        /// Modo edición
        /// </summary>
        Edit,

        /// <summary>
        /// Modo inserción
        /// </summary>
        Insert
    }
}
