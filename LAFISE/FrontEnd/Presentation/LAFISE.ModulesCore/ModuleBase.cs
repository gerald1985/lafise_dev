﻿#region Referencias

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetNuke.Entities.Modules;
using System.Web.UI;

#endregion

namespace LAFISE.ModulesCore
{
    /// <summary>
    /// Clase base para los módulos
    /// </summary>
    public abstract class ModuleBase : PortalModuleBase
    {
        /// <summary>
        /// Mensaje que se le presenta al usuario despues de una operación
        /// </summary>
        public string Mensaje { get; set; }

        /// <summary>
        /// Modo en el cual está el módulo
        /// </summary>
        public ModuleMode EstadoModulo { get; set; }


        /// <summary>
        /// Registrar archivos embebidos
        /// </summary>
        /// <param name="key"></param>
        /// <param name="resourceName"></param>
        public void RegisterResource(string key, string resourceName)
        {
            ClientScriptManager cs = Page.ClientScript;
            Type rsType = typeof(ModuleBase);
            if (!cs.IsClientScriptBlockRegistered(key))
                cs.RegisterClientScriptInclude(key, "App/"+ resourceName);
        }

        //protected override void OnPreRender(EventArgs e)
        //{
        //    RegisterResource("Angular", "Base/angular.min.js");
        //    RegisterResource("Bootstrap", "Base/bootstrap.min.js");
        //    RegisterResource("UIBootstrap", "Base/ui-bootstrap.min.js");
        //}
    }
}
