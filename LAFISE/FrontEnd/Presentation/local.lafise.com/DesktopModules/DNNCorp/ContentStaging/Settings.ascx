﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="DotNetNuke.Enterprise.ContentStaging.Settings" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.UI.WebControls" Assembly="DotNetNuke.Web" %>
<div class="dnnFormMessage dnnFormInfo">
    <dnn:dnnlabel ID="Dnnlabel1" text="Description" runat="server" />
</div>
<br />
<dnn:duallistbox id="ModulesDualList" runat="server" addimageurl="~/images/rt.gif"
    addallimageurl="~/images/ffwd.gif" removeimageurl="~/images/lt.gif" removeallimageurl="~/images/frev.gif"
    containerstyle-horizontalalign="Center">
    <AvailableListBoxStyle CssClass="NormalTextBox" Height="130px" Width="275px" />
    <HeaderStyle CssClass="NormalBold" />
    <SelectedListBoxStyle CssClass="NormalTextBox" Height="130px" Width="275px"  />
</dnn:duallistbox>
