/************************************************************/
/*****              SqlDataProvider                     *****/
/*****                                                  *****/
/*****                                                  *****/
/***** Note: To manually execute this script you must   *****/
/*****       perform a search and replace operation     *****/
/*****       for {databaseOwner} and {objectQualifier}  *****/
/*****                                                  *****/
/************************************************************/

-- ****************************
-- Folders
-- ****************************
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'{databaseOwner}{objectQualifier}CreateStagedFolder') AND OBJECTPROPERTY(id, N'IsPROCEDURE') = 1)
    DROP PROCEDURE {databaseOwner}{objectQualifier}CreateStagedFolder
GO

CREATE PROCEDURE {databaseOwner}{objectQualifier}CreateStagedFolder
	@FolderId			int,
	@PortalId			int,
	@UniqueId			uniqueidentifier
AS

	SET IDENTITY_INSERT {databaseOwner}{objectQualifier}Folders ON

		INSERT INTO {databaseOwner}{objectQualifier}Folders(
			FolderId,
			PortalId,
			FolderPath,
			StorageLocation,
			IsProtected,
			IsCached,
			LastUpdated,
			CreatedByUserID,
			CreatedOnDate,
			UniqueId,
			VersionGuid,
			FolderMappingID			
		)
		VALUES (
			@FolderId,
			@PortalId,
			newid(),
			0,
			0,
			0,
			getdate(),
			-1,
			getdate(),
			@UniqueId,
			newid(),
			0
		)

	SET IDENTITY_INSERT {databaseOwner}{objectQualifier}Folders OFF

GO

/************************************************************/
/*****              SqlDataProvider                     *****/
/************************************************************/