<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ContentStagingView.ascx.cs" Inherits="DotNetNuke.Enterprise.ContentStaging.Views.ContentStagingView" EnableViewState="true" %>
<%@ Register TagPrefix="dnn" TagName="label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="dnnweb" Namespace="DotNetNuke.Web.UI.WebControls" Assembly="DotNetNuke.Web" %>
<div class="dnnForm dnnContentStaging dnnClear" id="dnnContentStaging">
	<fieldset>
		<div class="dnnFormItem">
			<dnn:label ID="dnnlblLocalPortalAddress" resourcekey="LocalPortalAddress" runat="server" Suffix=":" ControlName="hlLocalAddressLink" />
			<asp:HyperLink ID="hlLocalAddressLink" Target="_blank" runat="server" />
		</div>
		<div class="dnnFormItem">
			<dnn:label ID="dnnlblLocalPortalToken" resourcekey="LocalPortalToken" runat="server" Suffix=":" ControlName="ResetLocalToken" />
			<asp:Label ID="dnnlblLocalTokenLable" runat="server" />
			<asp:LinkButton id="ResetLocalToken" runat="server" cssclass="dnnSecondaryAction" resourcekey="ResetLocal" />
		</div>
	</fieldset>
	<fieldset class="fsTargetHeader">
        <h2 id="dnnContentStaging-csTarget" class="dnnFormSectionHead"><%=LocalizeString("TargetHeader")%></h2>
		<div class="dnnFormItem" id="divPairedPanel" runat="server">
			<dnn:label ID="dnnlblTargetPortalAddress" resourcekey="TargetPortalAddress" runat="server" />
			<asp:Label ID="lblTargetPortalAddressValueLable" runat="server" />
			<asp:LinkButton id="DisconnectPortalsLink" resourcekey="ResetTarget" runat="server" CssClass="dnnSecondaryAction" />
		</div>
		<div id="divUnpaired" runat="server">
			<div class="dnnFormItem">
				<dnn:label ID="dnnlblTargetPortalAddressUP" resourcekey="TargetPortalAddress" runat="server" Suffix=":" ControlName="TargetPortalAddressText" />
				<asp:TextBox id="TargetPortalAddressText" runat="server" />
				<asp:RequiredFieldValidator ControlToValidate="TargetPortalAddressText" Display="Dynamic" runat="server" ID="valTargetPortalAddy" CssClass="dnnFormMessage dnnFormError" resourcekey="InvalidTargetAddress" ValidationGroup="Connect" />
			</div>
			<div class="dnnFormItem">
				<dnn:label ID="dnnlblTargetPortalToken" resourcekey="TargetPortalToken" runat="server" Suffix=":" ControlName="TargetPortalToken" />
				<asp:TextBox id="TargetPortalToken"  runat="server" />
				<asp:RequiredFieldValidator ControlToValidate="TargetPortalToken" Display="Dynamic" runat="server" ID="valTargetPortalToken" CssClass="dnnFormMessage dnnFormError" resourcekey="LocalPortalToken" ValidationGroup="Connect" />
			</div>
			<ul class="dnnActions dnnClear">
				<li><asp:LinkButton id="ConnectPortalsLink" resourcekey="ConnectPortals" runat="server" ValidationGroup="Connect" CssClass="dnnPrimaryAction" /></li>
				<li><asp:LinkButton id="TestConnectionLink" resourcekey="TestConnection" runat="server" ValidationGroup="Connect" CssClass="dnnSecondaryAction" /></li>
			</ul>
		</div>
		<div id="divPublish" class="fsPublishPanel" runat="server">
	        <h2 id="dnnContentStaging-csPublish" class="dnnFormSectionHead"><a href=""><%=LocalizeString("PublishHeader")%></a></h2>
			<div id="divAuthPanel" runat="server" class="csPublish-Auth">
				<div class="dnnFormMessage dnnFormWarning">
					<asp:label id="CantPublishLabel" resourcekey="CantPublish" runat="server" />
				</div>
				<div class="dnnFormItem">
					<dnn:label ID="dnnlblUsername" runat="server" resourcekey="Username" ControlName="usernameTextbox" Suffix=":" />
					<asp:TextBox id="usernameTextbox" runat="server" />
					<asp:RequiredFieldValidator ControlToValidate="usernameTextbox" Display="Dynamic" runat="server" ID="valUser" CssClass="dnnFormMessage dnnFormError" resourcekey="valUser" ValidationGroup="Authenticate" />
				</div>
				<div class="dnnFormItem">
					<dnn:label ID="dnnlblPassword" runat="server" resourcekey="Password" ControlName="passwordTextbox" Suffix=":" />
					<asp:TextBox id="passwordTextbox" runat="server" textmode="Password" />
					<asp:RequiredFieldValidator ControlToValidate="passwordTextbox" Display="Dynamic" runat="server" ID="valPass" CssClass="dnnFormMessage dnnFormError" resourcekey="valPass" ValidationGroup="Authenticate" />
				</div>
				<div class="dnnFormItem">
					<asp:Label ID="autheticateMessageLabel" runat="server" Visible="false" CssClass="dnnFormMessage dnnFormValidationSummary" />
				</div>
				<ul class="dnnActions dnnClear">
					<li><asp:LinkButton id="authenticateButton" runat="server" cssclass="dnnPrimaryAction" resourcekey="Authenticate" ValidationGroup="Authenticate" /></li>
				</ul>
			</div>
			<div id="divStageContentPanel" runat="server">
				<div class="dnnFormItem">
					<dnn:label ID="plSyncUserFolders" runat="server" ControlName="cbSyncUserFolders" Suffix=":" />
					<asp:CheckBox id="cbSyncUserFolders" runat="server" AutoPostBack="True" />
				</div>
				<dnnweb:dnnGrid id="SummaryGrid" runat="server" autogeneratecolumns="false">
					<MasterTableView>
						<Columns>
							<dnnweb:DnnGridBoundColumn DataField="Type" headertext="Type" HeaderStyle-Width="0" ItemStyle-Wrap="false" />
							<dnnweb:DnnGridBoundColumn DataField="Updated" HeaderText="Updated" HeaderStyle-Width="0" ItemStyle-Wrap="false" />
							<dnnweb:DnnGridBoundColumn DataField="Added" HeaderText="Added" HeaderStyle-Width="0" ItemStyle-Wrap="false" />
							<dnnweb:DnnGridBoundColumn DataField="Deleted" HeaderText="Deleted" HeaderStyle-Width="0" />
						</Columns>
					</MasterTableView>
				</dnnweb:dnnGrid>
				<ul class="dnnActions dnnClear">
					<li><asp:LinkButton id="AnalyzeContentCommand" runat="server" cssclass="dnnPrimaryAction" resourcekey="AnalyzeContent" /></li>
					<li><asp:LinkButton id="PublishCommand" runat="server" cssclass="dnnSecondaryAction" resourcekey="Publish" /></li>
				</ul>
				<div>
					<telerik:RadTreeView id="StagingTree" runat="server" />
				</div>
				<div id="divErrorPanel">
					<asp:DataList ID="ErrorDataList" runat="Server" ItemStyle-CssClass="dnnFormMessage dnnFormValidationSummary" CellPadding="5">
						<ItemTemplate>
							<%#Container.DataItem%>
						</ItemTemplate>
					</asp:DataList>
					<asp:DataList ID="WarningDataList" runat="Server" ItemStyle-ForeColor="Orange" CellPadding="10">
						<ItemTemplate>
							<%#Container.DataItem%>
						</ItemTemplate>
					</asp:DataList>
				</div>
			</div>
		</div>
	</fieldset>
</div>