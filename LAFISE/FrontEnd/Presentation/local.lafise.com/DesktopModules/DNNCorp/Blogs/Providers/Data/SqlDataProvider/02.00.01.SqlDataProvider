﻿/********************************************************
 * SPROC: Blogs_Entry_Search
 ********************************************************/
IF OBJECT_ID(N'{databaseOwner}[{objectQualifier}Blogs_Entry_Search]', N'P') IS NULL
	EXEC('CREATE PROCEDURE {databaseOwner}[{objectQualifier}Blogs_Entry_Search] AS BEGIN SELECT 1 END');
GO

ALTER PROCEDURE {databaseOwner}{objectQualifier}Blogs_Entry_Search
	@UserID INT ,
	@ModuleId INT ,
	@BlogID INT ,
	@StartDate DATETIME ,
	@EndDate DATETIME ,
	@Tags NVARCHAR(1024) ,
	@PageSize INT ,
	@PageIndex INT ,
	@ShowUnpublished BIT ,
	@Filter INT ,
	@GroupId INT ,
	@AuthorId INT  = -1
AS
BEGIN
	DECLARE @Today datetime = getutcdate();
	DECLARE @TitleMetaDataID int =
		(SELECT TOP 1 MetaDataID FROM {databaseOwner}[{objectQualifier}MetaData] WITH (NOLOCK) WHERE MetaDataName = 'Title');

	DECLARE @tblResult TABLE (RowNumber INT, TotalRecords INT, EntryId INT)
	DECLARE @tblTaxonomy TABLE (ContentItemID INT);
	DECLARE @tblComments TABLE (ContentItemID INT);

	--extract the ContentItems matching "my comments"
	IF @Filter=2
	BEGIN
		INSERT INTO @tblComments(ContentItemID)
		SELECT j.ContentItemId FROM {databaseOwner}{objectQualifier}Journal AS j WITH (NOLOCK)
			INNER JOIN {databaseOwner}{objectQualifier}Journal_Comments AS jc WITH (NOLOCK) ON jc.JournalId = j.JournalId AND jc.UserId = @UserID
	END

	--If tags, match the tags on ContentItems
	IF @Tags IS NOT NULL AND LEN(@Tags)> 0
	BEGIN
		INSERT INTO @tblTaxonomy(ContentItemID)
		SELECT CIT.ContentItemID from {databaseOwner}[{objectQualifier}ContentItems_Tags] CIT WITH (NOLOCK)
			INNER JOIN {databaseOwner}[{objectQualifier}Taxonomy_Terms] TT with (NOLOCK) ON CIT.TermID = TT.TermID
			INNER JOIN {databaseOwner}[{objectQualifier}ConvertListToTable](',', @Tags) T ON TT.Name = T.RowValue

		IF (SELECT COUNT(*) FROM @tblTaxonomy) > 0 --There are content items to match the tags
		BEGIN
			WITH EntrySet
			AS ( SELECT ROW_NUMBER() OVER ( ORDER BY E.Pinned DESC, E.PublishOnDate DESC ) AS RowNumber ,
						COUNT(*) OVER () AS [TotalRecords],
						E.EntryId

				FROM    {databaseOwner}{objectQualifier}Blogs_Blog B WITH (NOLOCK)
							INNER JOIN {databaseOwner}{objectQualifier}Blogs_Entry E WITH (NOLOCK) ON B.BlogId = E.BlogId
							INNER JOIN @tblTaxonomy X ON X.ContentItemId = E.ContentItemId
							INNER JOIN {databaseOwner}{objectQualifier}ContentItems CI WITH (NOLOCK) ON E.ContentItemId = CI.ContentItemID

				WHERE   CI.ModuleID = @ModuleId
						AND ( @GroupId < 1 OR B.GroupId = @GroupId )
						AND ( @BlogID < 1 OR B.BlogId = @BlogID )

						AND
						(
							(@Filter = 0 AND E.PublishOnDate BETWEEN @StartDate AND @EndDate)
						  OR
							(@Filter = 1 AND B.BlogId IN (SELECT BlogId FROM {databaseOwner}{objectQualifier}Blogs_Blog BL WITH (NOLOCK) WHERE BL.UserId = @UserID))
						  OR
							(@Filter = 2 AND E.ContentItemId IN (SELECT ContentItemID FROM @tblComments))
						  OR
							(@Filter = 3 AND CI.CreatedByUserID = @AuthorId)
						)
						AND ( @ShowUnpublished = 1 OR E.PublishOnDate > @Today OR (Authorized = 1 AND E.Approved = 1) )
				)
				INSERT INTO @tblResult(RowNumber, Totalrecords, EntryId)
				SELECT * FROM EntrySet
				WHERE   RowNumber BETWEEN ( @PageIndex * @PageSize + 1 ) AND  ( ( @PageIndex + 1 ) * @PageSize )
		END
	END
	ELSE
	BEGIN
		WITH  EntrySet
		AS ( SELECT ROW_NUMBER() OVER ( ORDER BY E.Pinned DESC, E.PublishOnDate DESC ) AS RowNumber ,
					COUNT(*) OVER () AS [TotalRecords],
					E.EntryId

			FROM    {databaseOwner}{objectQualifier}Blogs_Blog B WITH (NOLOCK)
						INNER JOIN {databaseOwner}{objectQualifier}Blogs_Entry E WITH (NOLOCK) ON B.BlogId = E.BlogId
						INNER JOIN {databaseOwner}{objectQualifier}ContentItems CI WITH (NOLOCK) ON E.ContentItemId = CI.ContentItemID

			WHERE    CI.ModuleID = @ModuleId
					AND ( @GroupId < 1 OR B.GroupId = @GroupId )
					AND ( @BlogID < 1 OR B.BlogId = @BlogID )
					AND ( @ShowUnpublished = 1 OR E.PublishOnDate > @Today OR (Authorized = 1 AND E.Approved = 1) )
					AND
					(
						(@Filter = 0 AND E.PublishOnDate BETWEEN @StartDate AND @EndDate)
					  OR
						(@Filter = 1 AND B.BlogId IN (SELECT BlogId FROM {databaseOwner}{objectQualifier}Blogs_Blog BL WITH (NOLOCK) WHERE BL.UserId = @UserID))
					  OR
						(@Filter = 2 AND E.ContentItemId IN (SELECT ContentItemID FROM @tblComments))
					  OR
						(@Filter = 3 AND CI.CreatedByUserID = @AuthorId)
					)
		)

		INSERT INTO @tblResult(RowNumber, Totalrecords, EntryId)
		SELECT * FROM EntrySet
		WHERE RowNumber BETWEEN ( @PageIndex * @PageSize + 1 ) AND ( ( @PageIndex + 1 ) * @PageSize )
	END

	-- extract all data for the page selected
	SELECT	T.RowNumber,
			T.TotalRecords,
			E.* ,
			B.PortalId ,
			B.UserId ,
			B.Authorized ,
			B.GroupId,
			B.AuthorMode,
			CI.Content ,
			CI.ContentTypeID ,
			CI.TabID ,
			CI.ModuleID ,
			CI.ContentKey ,
			CI.Indexed ,
			CI.CreatedByUserID ,
			CI.[CreatedOnDate] ,
			CI.LastModifiedByUserID ,
			CI.[LastModifiedOnDate],
			CIMD.[MetaDataValue] AS ContentTitle

	FROM @tblResult T
			INNER JOIN {databaseOwner}{objectQualifier}Blogs_Entry E WITH (NOLOCK) ON T.EntryId = E.EntryId
			INNER JOIN {databaseOwner}{objectQualifier}Blogs_Blog B WITH (NOLOCK) ON B.BlogId = E.BlogId
			INNER JOIN {databaseOwner}{objectQualifier}ContentItems CI WITH (NOLOCK) ON E.ContentItemId = CI.ContentItemID
			LEFT OUTER JOIN {databaseOwner}{objectQualifier}ContentItems_MetaData CIMD WITH (NOLOCK)
					ON CI.[ContentItemID] = CIMD.ContentItemID AND CIMD.MetaDataID = @TitleMetaDataID

END

GO

/********************************************************
 * SPROC: Blogs_Entry_GetByContentItem
 ********************************************************/
IF OBJECT_ID(N'{databaseOwner}[{objectQualifier}Blogs_Entry_GetByContentItem]', N'P') IS NULL
	EXEC('CREATE PROCEDURE {databaseOwner}[{objectQualifier}Blogs_Entry_GetByContentItem] AS BEGIN SELECT 1 END');
GO

ALTER PROCEDURE {databaseOwner}{objectQualifier}Blogs_Entry_GetByContentItem
	@ContentItemId INT,
	@ModuleId INT
AS
BEGIN
	SELECT  E.*,
			B.UserId ,
			B.AuthorMode ,
			B.PortalId,
			U.UserName ,
			U.DisplayName ,
			CI.CreatedByUserID ,
			CI.CreatedOnDate ,
			CI.ContentKey ,
			CI.Indexed ,
			CI.Content ,
			CI.ContentItemID ,
			CI.LastModifiedByUserID ,
			CI.LastModifiedOnDate ,
			CI.ModuleID ,
			CI.TabID ,
			CI.ContentTypeID ,
			CIMD.[MetaDataValue] AS [ContentTitle] ,
			( SELECT    1
			) AS TotalRecords
	FROM    {databaseOwner}{objectQualifier}Blogs_Entry E WITH(NOLOCK)
				INNER JOIN {databaseOwner}{objectQualifier}Blogs_Blog B WITH(NOLOCK) ON B.BlogId = E.BlogId
				INNER JOIN {databaseOwner}{objectQualifier}Users U WITH(NOLOCK) ON U.UserID = B.UserId
				INNER JOIN {databaseOwner}{objectQualifier}ContentItems CI WITH(NOLOCK) ON E.ContentItemId = CI.ContentItemID
				LEFT OUTER JOIN {databaseOwner}{objectQualifier}ContentItems_MetaData CIMD WITH(NOLOCK)
														ON	CIMD.ContentItemID = E.ContentItemId
															AND CIMD.MetaDataID = ( SELECT
																					MetaDataID
																					FROM
																					{databaseOwner}{objectQualifier}MetaData
																					WHERE
																					MetaDataName = 'Title'
																				  )
	WHERE   E.ContentItemId = @ContentItemId
	AND		CI.ModuleID = @ModuleId
END
GO

/* Sitemap Provider (first one is old sproc name) */
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'{databaseOwner}{objectQualifier}Blogs_Entry_GetPublicByPortal') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE {databaseOwner}{objectQualifier}Blogs_Entry_GetPublicByPortal
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'{databaseOwner}{objectQualifier}Blogs_Entry_GetSitemap') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE {databaseOwner}{objectQualifier}Blogs_Entry_GetSitemap
GO

CREATE PROCEDURE {databaseOwner}[{objectQualifier}Blogs_Entry_GetSitemap]
	@PortalId INT
AS
BEGIN
	SELECT  E.*,
			B.UserId ,
			B.PortalId,
			B.AuthorMode,
			(CASE B.GroupId WHEN NULL THEN -1 ELSE B.GroupId END) AS [GroupId],
			CI.CreatedByUserID ,
			CI.CreatedOnDate ,
			CI.ContentKey ,
			CI.Indexed ,
			CI.Content ,
			CI.ContentItemID ,
			CI.LastModifiedByUserID ,
			CI.LastModifiedOnDate ,
			CI.ModuleID ,
			CI.TabID ,
			CI.ContentTypeID ,
			COUNT(*) OVER () AS TotalRecords,
			CIMD.[MetaDataValue] AS [ContentTitle]
	FROM    {databaseOwner}{objectQualifier}Blogs_Entry E WITH (NOLOCK) 
			INNER JOIN {databaseOwner}{objectQualifier}Blogs_Blog B WITH (NOLOCK) ON B.BlogId = E.BlogId
			INNER JOIN {databaseOwner}{objectQualifier}ContentItems CI WITH (NOLOCK) ON E.ContentItemId = CI.ContentItemID
			LEFT OUTER JOIN {databaseOwner}{objectQualifier}ContentItems_MetaData CIMD WITH (NOLOCK) ON CIMD.ContentItemID = E.ContentItemId
				AND CIMD.MetaDataID = (SELECT MetaDataID FROM {databaseOwner}[{objectQualifier}MetaData] WHERE MetaDataName = 'Title')
			LEFT OUTER JOIN {databaseOwner}{objectQualifier}Roles R WITH (NOLOCK) ON B.GroupId = R.RoleID
	WHERE   B.PortalId = @PortalId
			AND PublishOnDate <= GETUTCDATE()
			AND B.Authorized = 1
			AND E.Approved = 1
			AND ((GroupId < 1) OR (GroupId > 0 AND IsPublic = 1))
END
GO

/* Group Mode setting migration */
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'{databaseOwner}{objectQualifier}Blogs_Settings_GetUpgradeTransfer') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE {databaseOwner}{objectQualifier}Blogs_Settings_GetUpgradeTransfer
GO

CREATE PROCEDURE {databaseOwner}[{objectQualifier}Blogs_Settings_GetUpgradeTransfer]
AS 
	INSERT INTO {databaseOwner}{objectQualifier}ModuleSettings 
		(ModuleID, SettingName, SettingValue, CreatedByUserID, CreatedOnDate, LastModifiedByUserID, LastModifiedOnDate)
	SELECT 
		ModuleID, 'BLOG_FILTERMODE', '1', -1, GETUTCDATE(), -1, GETUTCDATE()
	FROM    {databaseOwner}{objectQualifier}Blogs_Setting BS
		INNER JOIN {databaseOwner}{objectQualifier}TabModules TM ON BS.TabId = TM.TabID
		AND		[Key] = 'BLOG_FILTERMODE'
		AND		[Value] = '1'

	DELETE FROM    {databaseOwner}{objectQualifier}Blogs_Setting
		WHERE  [KEY] IN ('EnablePlusOne', 'EnableTwitter', 'EnableLinkedIn', 'FacebookAppId', 'SocialSharingMode', 'ShowSeoFriendlyUrl')
GO

DECLARE @DesktopModuleId INT
SET @DesktopModuleId = ( SELECT DesktopModuleID FROM {databaseOwner}[{objectQualifier}DesktopModules] WHERE  ModuleName = 'Blogs' AND FolderName = 'DNNCorp/Blogs')

DECLARE @EditedActionTypeId INT = (SELECT ActionTypeId FROM {databaseOwner}{objectQualifier}Analytics_Dim_ActionType WHERE ActionType = 'Edited')

DECLARE @CreatedActionTypeId INT = (SELECT ActionTypeId FROM {databaseOwner}{objectQualifier}Analytics_Dim_ActionType WHERE ActionType = 'Created')

UPDATE {databaseOwner}[{objectQualifier}Mechanics_ScoringActionDefinition]
	SET ActionType = @CreatedActionTypeId
	WHERE ActionName = 'PostedComment' AND DesktopModuleId = @DesktopModuleId AND ActionType = @EditedActionTypeId

UPDATE {databaseOwner}{objectQualifier}Analytics_Fact_UserScoringLog
	SET ActionTypeId =  @CreatedActionTypeId
FROM  {databaseOwner}{objectQualifier}Analytics_Fact_UserScoringLog l
	JOIN  {databaseOwner}{objectQualifier}Analytics_Dim_ActionDefinition d ON l.ActionDefinitionId = d.ActionDefinitionId
WHERE l.ActionTypeId =  @EditedActionTypeId AND d.ActionName = 'PostedComment'
GO