﻿// DotNetNuke® - http://www.dnnsoftware.com
//
// Copyright (c) 2002-2015, DNN Corp.
// All rights reserved.

﻿"use strict";
define(['jquery', 'knockout'], function ($, ko) {

    var init = function (koObject, connectionHelper, pluginFolder, util) {
    }

    var onSave = function (koObject) {
    }

    return {
        init: init,
        onSave: onSave
    }
});