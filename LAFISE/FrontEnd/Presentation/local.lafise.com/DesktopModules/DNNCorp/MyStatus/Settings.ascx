<%@ Control Language="C#" AutoEventWireup="false" Inherits="DotNetNuke.Professional.MyStatus.Settings" Codebehind="Settings.ascx.cs" %>
<%@ Register TagName="label" TagPrefix="dnn" Src="~/controls/labelcontrol.ascx" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<h2 class="dnnFormSectionHead">
    <a class="dnnSectionExpanded">
        <%= LocalizeString("MyStatusSettings") %>
    </a>
</h2>
<fieldset>
    <div class="dnnFormItem">
        <dnn:label id="dlblTemplate" runat="server" controlname="ddlTemplates" resourcekey="Template" />
        <asp:DropDownList runat="server" ID="ddlTemplates" Visible="False">
            <asp:ListItem Value="compact" resourcekey="Compact"></asp:ListItem>
            <asp:ListItem Value="expanded" resourcekey="Expanded"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dnnFormItem">
        <dnn:label runat="server" controlname="cblCollections" resourcekey="Collections" />
        <asp:CheckBoxList runat="server" ID="cblCollections" DataValueField="CollectionKey" DataTextField="FullDescription" DataSelectedField="Enabled" />
    </div>
</fieldset>

<h2 class="dnnFormSectionHead">
    <a class="dnnSectionExpanded">
        <%= LocalizeString("VisualSettings") %>
    </a>
</h2>
<fieldset>
    <h3>
        <%= LocalizeString("Colors") %>
    </h3>
    <div class="dnnFormItem">
        <dnn:Label ResourceKey="Color1" runat="server" /> 
        <telerik:RadColorPicker ID="ctlColor1" Runat="server" CssClass="dnnLeft" ShowIcon="True" />
    </div>
    <div class="dnnFormItem">
        <dnn:Label ResourceKey="Color2" runat="server" /> 
        <telerik:RadColorPicker ID="ctlColor2" Runat="server" CssClass="dnnLeft" ShowIcon="True" />
    </div>
</fieldset>