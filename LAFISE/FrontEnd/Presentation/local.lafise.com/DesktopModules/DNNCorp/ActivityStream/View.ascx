<%@ Control language="C#" Inherits="DotNetNuke.Professional.ActivityStream.View" AutoEventWireup="false"  Codebehind="View.ascx.cs" %>
<%@ Register Namespace="DotNetNuke.Professional.ActivityStream.Controls" Assembly="DotNetNuke.Professional.ActivityStream" TagPrefix="dnn" %>
<%@ Register tagPrefix="social" tagName="CommentCard" src="~/DesktopModules/DNNCorp/SocialLibrary/Controls/CommentCard.ascx" %>
<%@ Register TagPrefix="social" TagName="LikeControl" Src="~/DesktopModules/DNNCorp/SocialLibrary/Controls/LikeControl.ascx" %>
<%@ Register TagPrefix="social" TagName="GroupedJournalControl" Src="~/DesktopModules/DNNCorp/ActivityStream/GroupedJournalControl.ascx" %>

<div id="userFileManager"></div>
<social:LikeControl runat="server" ID="ctlLikeControl" />
<social:CommentCard runat="server" ID="ctlCommentCard" />
<social:GroupedJournalControl runat="server" id="ctlGroupedJournalControl" />
<% if (!HideScopeWrapper)
   { %>
<div class="journalContainer" id="activityStream-ScopeWrapper">
    <% if (ShowEditor)
       { %>
    <div class="journalTools">
        <div id="avatar" runat="server" class="avatar">
            <span>
                <em>
                    <img src="<%= AvatarUrl %>" alt="avatar">
                </em>
            </span>
        </div>
        <div class="journalEditor-Container">
            <div id="journalEditor">     
                <div id="journalClose"></div>   
                <textarea id="journalContent"></textarea>
                <div id="tbar">
                    <span id="tbar-perm"></span>
                    <% if (AllowFiles)
                       { %>
                    <span id="tbar-attach"></span>
                    <% } %>
                    <% if (AllowPhotos)
                       { %>
                    <span id="tbar-photo"></span>
                    <% } %>
                    <div class="securityMenu dnnClear">
                        <div class="handle"></div>
                        <ul>
                            <li><b><%= LocalizeString("WhoWillSee.Text") %></b></li>
                            <% if (!InGroupMode || IsPublicGroup)
                               { %>
                            <li><input type="radio" name="privacy" value="E" checked="<%= !InGroupMode ? "checked" : "" %>" /><%= LocalizeString("Everyone.Text") %></li>
                            <li><input type="radio" name="privacy" value="C" /><%= LocalizeString("Community.Text") %></li>
                                <% if (InGroupMode)
                                   { %>
                            <li><input type="radio" name="privacy" value="R" checked="checked" /><%= LocalizeString("GroupMembers.Text") %></li>
                                <% } %>
                            <% }
                               else
                               { %>
                            <li><input type="radio" name="privacy" value="R" checked="checked" /><%= LocalizeString("GroupMembers.Text") %></li>
                            <% } %>
                            <% if (IsUserProfilePage)
                               { %>
                            <li><input type="radio" name="privacy" value="F" /><%= LocalizeString("Friends.Text") %></li>
                            <li><input type="radio" name="privacy" value="U" /><%= LocalizeString("Private.Text") %></li> 
                            <% } %>
                        </ul>
                    </div>
                </div>
                <a href="#" id="share" class="dnnPrimaryAction"><%= LocalizeString("Share.Text") %></a>
                <div id="journalPlaceholder"><%= LocalizeString("SharePlaceHolder.Text") %></div>
                <div class="dnnClear"></div>
            </div>
            <div id="journalOptionArea">
                <% if (AllowFiles || AllowPhotos)
                   { %>
                <div class="fileUploadArea">
                    <div class="jpa" id="tbar-attach-Area">
                        <div class="journal_onlineFileShare">
                            <span id="tbar-photoText"><%= LocalizeString("SelectPhoto.Text") %></span> 
                            <span id="tbar-fileText"><%= LocalizeString("SelectFile.Text") %></span>
                            <div>
                                <a href="javascript:void(0)" id="photoFromSite" class="dnnSecondaryAction"><%= LocalizeString("BrowseFromSite.Text") %></a> 
                            </div>
                        </div>
                        <div class="journal_localFileShare">
                            <span class="browser-upload-btn"><%= LocalizeString("UploadFromLocal.Text") %></span>
                            <div class="dnnForm">
                                <input id="uploadFileId" type="file" name="files[]" multiple />
                            </div>
                        </div>
                        <div style="clear:both; padding: 0; margin: 0;"></div>
                    </div>
                    <div id="itemUpload">
                        <div class="fileupload-error dnnFormMessage dnnFormValidationSummary" style="display:none;"></div>
                        <div class="progress_bar_wrapper">
                                <div class="progress_context" style="display:none;">
                                    <div class="progress-bar green">
                                        <div style="width:0px;"><span></span></div>
                                    </div>
                                </div>
                            </div>
                
                        <div class="filePreviewArea"></div>
                    </div>
                </div>
                <% } %>
                <div class="jpa" id="linkArea">
                    <div id="linkClose"></div>
                    <div id="imagePreviewer">
                        <div id="image">
                            <span>
                                <em>
                                    <img src="about:blank" alt=""/>
                                </em>
                            </span>
                        </div>
                        <span id="imgPrev">&lt;&lt;</span>
                        <span id="imgNext">&gt;&gt;</span>
                        <span id="imgCount">1 <%= LocalizeString("Of.Text") %> 10</span>
                    </div>
                    <div id="linkInfo">
                        <b></b>
                        <p></p>
                    </div>
                    <div class="dnnClear"></div>
                </div>
            </div>
        </div>
    </div>
    <% } %>   
    <% if (Request.IsAuthenticated)
       { %>
    <div class="journalViewTools-container">
        <select id="selectActivityType" runat="server" class="select-view"  data-bind="options: allowableFilterTypes, optionsText: 'FilterType', optionsValue: 'FilterTypeId', selectedOptions: selectedFilter, optionsCaption: selectActivityTypeCaption() "></select>
        <div class="dnnClear"></div>
    </div>
    <% } %>
    <div class="journalGetNewUpdates-container">
        <a href="javascript:void(0)" id="getNew" style="display:none"><%= LocalizeString("SeeNewUpdates.Text") %></a>
    </div>
    <div id="journalItems" data-bind="stopBindings: true">
        <dnn:JournalListControl ID="ctlJournalList" runat="server"></dnn:JournalListControl>
    </div>
    <div id='journalBottomContorls'class="journalBottomContorls-container moduleTopInsetShadowBox">
        <a href="javascript:void(0)" style="display:none;" id="getMore" class="dnnPrimaryAction"><%= LocalizeString("GetMore.Text") %></a>
        <a href="javascript:void(0)" style="display:none;" id="showAll" class="dnnPrimaryAction"><%= LocalizeString("ShowAll.Text") %></a>
    </div>
</div>
<% } %>
<%-- Filter Params --%>
<input type="hidden" id="filterId" value="0"/>
<input type="hidden" id="profileId" value="0"/>
<input type="hidden" id="journalTypeId" value="0"/>
<%-- END Filter Params --%>

<script type="text/javascript">
    var globalActivityStreamListView = false;

    function activityStreamInit(settings) {
        settings.moduleScope = document.getElementById('activityStream-ScopeWrapper');
        if (!settings.moduleScope) return;
        settings.grouping = <%= EnableGrouping.ToString().ToLower() %>;
        settings.initialNextListIndex = <%= ctlJournalList.JsVariableNameNextListIndex %>;   
        settings.initialDynamicJs = <%= ctlJournalList.JsVariableNameDynamicJs %>;  
        settings.initialDynamicCss = <%= ctlJournalList.JsVariableNameDynamicCss %>;

        $.fn.setVideoEnabled(<%=IsVideoEnabled.ToString().ToLowerInvariant() %>);
        
        try {
            globalActivityStreamListView = new window.dnn.activityStream.ListView(settings);
            window.dnn.social.applyBindings(globalActivityStreamListView, settings.moduleScope);
        } catch (ex) {
            window.dnn.social.topLevelExceptionHandler(settings, ex);
        }
    }
</script>
