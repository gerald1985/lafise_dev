﻿// DotNetNuke® - http://www.dnnsoftware.com
// Copyright (c) 2002-2015, DNN Corp.
// by DNN Corporation
// All Rights Reserved
(function ($) {
    "use strict";

    var journalItem = {
            JournalType: 'status',
            Title: '',
            Summary: '',
            Body: '',
            ItemData: null,
            Security: 'E',
            Files: []
        },

        cancelRequest = false,
        
        attachedDocuments = [],
        
        uploadDocuments = false,

        videoEnabled = true,
        
        resetJournalItem = function () {
            var prevSec = journalItem.Security || 'E';
            journalItem = {};
            journalItem.JournalType = 'status';
            journalItem.Title = '';
            journalItem.Summary = '';
            journalItem.Body = '';
            journalItem.ItemData = null;
            journalItem.Security = prevSec;
            journalItem.Tags = [];
            journalItem.Users = [];
            journalItem.Files = [];
            attachedDocuments = [];
        };

    $.fn.setVideoEnabled = function(isVideoEnabled) {
        videoEnabled = isVideoEnabled;
    };

    $.isImage = function(value) {
        if (value != null) {
            return value.match(/^((jpg|png|gif|jpe|jpeg|tiff))$/i) != null;
        } else {
            return false;
        }
    };

    $.attachPhoto = function (fileId, fileName, path) {
        if (journalItem.JournalType !== 'file') {
            $('#tbar-attach-Area').hide();
            journalItem.JournalType = 'photo';
            $(".filePreviewArea").append($("<div class='preview-photo-item'><img src='" + path + "' /></div>")).show();
            journalItem.Files.push(fileId);
            attachedDocuments.push({ fileId: fileId, fileName: fileName, path: path });
            $("#share").removeAttr('disabled');
            $("#share").unbind('keyup', 'isDirtyHandler');
        } else {
            $.attachDocument(fileId, fileName, path);
        }
    };

    $.attachDocument = function(fileId, fileName, path) {
        $('#tbar-attach-Area').hide();
        journalItem.JournalType = 'file';
        journalItem.Files.push(fileId);
        attachedDocuments.push({ fileId: fileId, fileName: fileName, path: path });
        clearFilePreviewContainer();
        $.each(attachedDocuments, function (i, v) {
            $.renderAttachedDocument(v.fileId, v.fileName, v.path);
        });
        $("#share").removeAttr('disabled');
        $("#share").unbind('keyup', 'isDirtyHandler');
    };

    $.renderAttachedDocument = function(fileId, fileName, path) {
        $(".filePreviewArea").show().append($("<div class='tbar-attach-doc'><img src='" + path + "' /><p>" + fileName + "</p></div>"));
    };
    
    function clearFilePreviewContainer() {
        var filePreviewContainer = $('.filePreviewArea');
        filePreviewContainer.hide();
        var msnry = filePreviewContainer.data('masonry');
        if (msnry) filePreviewContainer.masonry('destroy');
        filePreviewContainer.empty();
    }

    $.fn.journalTools = function (options) {
        $.fn.journalTools.defaultOptions = {
            placeHolder: '#journalPlaceholder',
            shareButton: '#share',
            closeButton: '#journalClose',
            optionsArea: '#journalOptionArea',
            content: '#journalContent',
            photoTool: '#tbar-photo',
            attachTool: '#tbar-attach',
            permTool: '#tbar-perm',
            securityMenu: '.securityMenu',
            photoText: '#tbar-photoText',
            fileText: '#tbar-fileText',
            maxLength: 250,
            fileManagerSelector: '#userFileManager',
            noActivityStreamPlaceHolder: '#noActivityStreamPlaceHolder'
        };

        var opts = $.extend({}, $.fn.journalTools.defaultOptions, options),
            $wrap = this,
            $placeHolder = $wrap.find(opts.placeHolder),
            $shareButton = $wrap.find(opts.shareButton),
            $closeButton = $wrap.find(opts.closeButton),
            $optionsArea = $wrap.find(opts.optionsArea),
            $content = $wrap.find(opts.content),
            $photoTool = $wrap.find(opts.photoTool),
            $attachTool = $wrap.find(opts.attachTool),
            $permTool = $wrap.find(opts.permTool),
            $photoArea = $wrap.find(opts.photoTool + '-Area'),
            $attachArea = $wrap.find(opts.attachTool + '-Area'),
            $photoText = $wrap.find(opts.photoText),
            $fileText = $wrap.find(opts.fileText),
            $contentShow = false,
            $maxLength = opts.maxLength,
            $securityMenu = $wrap.find(opts.securityMenu),
            $userFileManager = $wrap.find(opts.fileManagerSelector),
            $noActivityStreamPlaceHolder = $wrap.find(opts.noActivityStreamPlaceHolder),
            pid = opts.pid,
            gid = opts.gid;

        journalItem.Security = $securityMenu.find('input:checked').val() || 'E';

        var isDirtyHandler = function () {
            if ($content.val())
                $shareButton.removeAttr('disabled');
            else
                $shareButton.attr('disabled', 'disabled');
        };

        $permTool.unbind('click').bind('click', function () {
            $securityMenu.toggle();
        });

        $securityMenu.find('.handle').unbind('click').bind('click', function () {
            $securityMenu.toggle();
        });
        
        $(opts.securityMenu + ' input').unbind('click').bind('click', function () {
            journalItem.Security = $(this).val();
        });

        $photoTool.unbind('click').bind('click', function () {
            photoToolClick();
        });
        
        $attachTool.unbind('click').bind('click', function () {
            attachToolClick();
        });

        $placeHolder.unbind('click').bind('click', function () {
            showContent();
        });
        
        $closeButton.unbind('click').bind('click', function () {
            closeEditor();
        });
        
        $content.unbind('keypress').bind('keypress', function (event) {           
            return true;
        });
        
        function closeEditor() {
            $closeButton.hide();
            $contentShow = false;
            $securityMenu.hide();
            $shareButton.attr('disabled', 'disabled').hide();
            $photoArea.hide();
            $attachArea.hide();
            $optionsArea.hide();
            $('#tbar span').removeClass('selected');

            var linkArea = $('#linkArea');
            $('#linkArea #imagePreviewer').hide();
            linkArea.find('#linkInfo b').text('');
            linkArea.find('#linkInfo p').text('');
            $content.data('linkedUp', false);
            linkArea.hide();
            linkArea.data('url', '');

            $attachTool.unbind('click');
            $photoTool.unbind('click');
            $photoTool.click(function () {
                photoToolClick();
            });
            $attachTool.click(function () {
                attachToolClick();
            });
            
            clearFilePreviewContainer();
            $content.unbind('keyup', isDirtyHandler);
            $content.animate({
                height: '0'
            }, 400, function () {
                $content.val('').hide();
                $placeHolder.show();
                resetJournalItem();
            });
        }
        
        function getNewItem(data, callback) {
            $.ajax({
                type: "POST",
                url: opts.servicesFramework.getServiceRoot('DNNCorp/ActivityStream') + "ActivityStreamServices/GetListForProfile",
                beforeSend: opts.servicesFramework.setModuleHeaders,
                data: data,
                success: function (response) {
                    if (typeof (callback) != "undefined") {
                        callback(response.Content);
                    }

                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
        }

        function insertNewItem(html) {
            $(html).hide().prependTo("#journalItems").fadeIn();
            var viewModel = window.ko.contextFor(document.getElementById('activityStream-ScopeWrapper'));
            viewModel.$root.pluginInit();
            globalActivityStreamListView.nextListIndex++;
        }
        
        var showContent = function () {
            $placeHolder.hide();
            $shareButton.attr('disabled', 'disabled').show();
            $content.show().animate({
                height: '+=65'
            }, 400, function () {
                $contentShow = true;
                $content.focus();
                $content.bind('keyup', isDirtyHandler);
                $closeButton.show();
            });
        };
        
        var photoToolClick = function () {
            $('#tbar-attach-Area').show();
            if ($photoTool.hasClass('selected')) {
                $photoTool.removeClass('selected');
                $attachArea.hide();
                clearFilePreviewContainer();
                resetJournalItem();
                $attachTool.unbind('click').bind('click', attachToolClick);
            } else {
                if (!$contentShow) showContent();
                $userFileManager.userFileManager.setFileExtensions('jpg,png,gif,jpe,jpeg,tiff');
                $photoTool.addClass('selected');
                $attachTool.unbind('click').bind('click', clickDisable);
                $optionsArea.show();
                $attachArea.show();
                $fileText.hide();
                $photoText.show();
                uploadDocuments = false;
            }
        };
        
        var attachToolClick = function () {
            $('#tbar-attach-Area').show();
            if ($attachTool.hasClass('selected')) {
                $attachTool.removeClass('selected');
                $attachArea.hide();
                clearFilePreviewContainer();
                resetJournalItem();
                $photoTool.unbind('click').bind('click', photoToolClick);
            } else {
                if (!$contentShow) showContent();
                $userFileManager.userFileManager.setFileExtensions('');
                $attachTool.addClass('selected');
                $photoTool.unbind('click').bind('click', clickDisable);
                $attachArea.show();
                $optionsArea.show();
                $fileText.show();
                $photoText.hide();
                uploadDocuments = true;
            }
        };
        
        var clickDisable = function (e) {
            e.preventDefault();
        };
        
        $shareButton.unbind('click').bind('click', function (e) {
            cancelRequest = true;
            e.preventDefault();
            var data = {};
            data.text = encodeURIComponent($content.val());
            data.profileId = pid;
            data.groupId = gid;
            data.journalType = journalItem.JournalType;
            data.securitySet = journalItem.Security;
            data.files = journalItem.Files;
           
            var itemData = journalItem.ItemData;
            if (itemData != null) {
                data.itemData = JSON.stringify(itemData);
            } else {
                data.itemData = '';
            }
            
            if ((data.text == '' && data.itemData == '' && data.files.length < 1) || data.text == '%3Cbr%3E') {
                return false;
            }
            var tmpValue = $content.val();
            tmpValue = tmpValue.replace(/<(?:.|\n)*?>/gm, '').replace(/\s+/g, '').replace(/&nbsp;/g, '');
            if (tmpValue == '' && data.itemData == '' && data.files.length < 1) {
                return false;
            }
            $shareButton.attr('disabled', 'disabled');
            $.ajax({
                type: "POST",
                url: opts.servicesFramework.getServiceRoot('DNNCorp/ActivityStream') + "ActivityStreamServices/Create",
                data: data,
                beforeSend: opts.servicesFramework.setModuleHeaders,
                success: function () {
                    var req = {};
                    req.ProfileId = pid;
                    req.GroupId = gid;
                    req.RowIndex = 0;
                    req.MaxRows = 1;
                    closeEditor();
                    getNewItem(req, insertNewItem);
                    $noActivityStreamPlaceHolder.hide();
                },
                error: function (xhr, status, error) {
                    var parseException = function (json) {
                        try {
                            var parsed = $.parseJSON(json) || { ExceptionMessage: 'Unknown error' };
                            var msg = null;
                            switch (typeof parsed) {
                                case 'undefined':
                                    break;
                                case 'string':
                                    msg = parsed;
                                    break;
                                case 'object':
                                    if (parsed.hasOwnProperty('ExceptionMessage')) {
                                        msg = parsed.ExceptionMessage;
                                    } else if (parsed.hasOwnProperty('Message')) {
                                        msg = parsed.Message;
                                    } else if (parsed.hasOwnProperty('Exception')) {
                                        msg = parsed.Exception;
                                    }

                                    if (String.isEmpty(msg)) {
                                        msg = 'Unknown error';
                                    }
                                    break;
                            }

                            return msg;
                        }
                        catch (e) {
                            return null;
                        }
                    };

                    var parsedMessage = parseException(xhr.responseText);

                    $.dnnAlert({
                        title: 'Error',
                        text: parsedMessage || error
                    });
                },
                complete: function(xhr, status) {
                    $shareButton.removeAttr('disabled');
                }

            });
            return false;
        });
    };
    
    $.fn.previewify = function (options) {
        var images = [];
        var currImage = 0;
        var opts = $.extend({}, $.fn.previewify.defaultOptions, options),
            $wrap = this,
            $textArea = $wrap.find(opts.textareaSelector),
            $clearLink = $wrap.find(opts.clearPreviewSelector),
            $previewArea = $wrap.find(opts.previewSelector);

        function updatePageValues(url, clearUrl) {
            if ($previewArea.data('url') === url) {
                if (clearUrl) $previewArea.data('url', '');
                return;
            }
            cancelRequest = false;
            $textArea.data('linkedUp', true);
            $previewArea.data('url', url);
            $clearLink.show();

            var data = { Url: url };
            $.ajax({
                type: "POST",
                url: opts.servicesFramework.getServiceRoot('DNNCorp/ActivityStream') + 'ActivityStreamServices/PreviewURL',
                beforeSend: opts.servicesFramework.setModuleHeaders,
                data: data,
                success: function (d) {
                    buildLinkPreview(d);
                },
                error: function () { }
            });
        }
        
        function loadImg() {
            $(opts.previewSelector + ' #imgCount').text(currImage + 1 + ' of ' + images.length);
            $previewArea.find('#image img').fadeOut(function () {
                $(this).hide().attr('src', 'about:blank').attr('src', images[currImage].Url).fadeIn();
            });
            journalItem.ItemData.ImageUrl = images[currImage].Url;
        }
        
        function prevImg() {
            currImage = currImage - 1;
            if (currImage < 0) {
                currImage = images.length - 1;
            };
            if (currImage > images.length - 1) {
                currImage = 0;
            };
            loadImg();
        }
        
        function nextImg() {
            currImage = currImage + 1;
            if (currImage < 0) {
                currImage = images.length - 1;
            };
            if (currImage > images.length - 1) {
                currImage = 0;
            };
            loadImg();
        }
        
        function buildLinkPreview(link) {
            if (!link.Url || cancelRequest) {
                removePageValues();
                cancelRequest = false;
                return;
            }
            
            journalItem.ItemData = {};
            if (link.Images.length > 0) {
                images = link.Images;
                journalItem.ItemData.ImageUrl = link.Images[0].Url;
                $previewArea.find('#image img').attr('src', link.Images[0].Url);
                currImage = 0;
                
                if (link.Images.length > 1) {
                    $(opts.previewSelector + ' #imgPrev').unbind('click').bind('click', prevImg).show();
                    $(opts.previewSelector + ' #imgNext').unbind('click').bind('click', nextImg).show();
                    $(opts.previewSelector + ' #imgCount').text(currImage + 1 + ' of ' + images.length).show();
                } else {
                    $(opts.previewSelector + ' #imgPrev').hide();
                    $(opts.previewSelector + ' #imgNext').hide();
                    $(opts.previewSelector + ' #imgCount').hide();
                }
                $(opts.previewSelector + ' #imagePreviewer').show();
            } else {
                $(opts.previewSelector + ' #imagePreviewer').hide();
            }
            
            if (!link.Description) link.Description = "";
            if (!link.Title) link.Title = link.Url;
            link.Description = link.Description.replace('&amp;', '&');
            link.Description = decodeURIComponent(link.Description);
            $previewArea.find('#linkClose').show();
            $previewArea.find('#linkInfo b').html(link.Title);
            $previewArea.find('#linkInfo p').html(link.Description);
            $previewArea.show();
            $previewArea.parent().show();
            journalItem.JournalType = (link.VideoUrl && videoEnabled) ? 'video': 'link';
            journalItem.ItemData.Title = link.Title;
            journalItem.ItemData.Description = escape(link.Description);
            journalItem.ItemData.Url = journalItem.JournalType == 'video' ? link.VideoUrl : link.Url;
        }

        function removePageValues() {
            $(opts.previewSelector + ' #imagePreviewer').hide();
            images = [];
            currImage = 0;
            $previewArea.find('#linkInfo b').text('');
            $previewArea.find('#linkInfo p').text('');
            $textArea.data('linkedUp', false);
            $previewArea.hide();
            $clearLink.hide();
            journalItem.JournalType = 'status';
            journalItem.ItemData = null;
            findUrl($textArea.val(), true);
        }

        function isLinkedUp() {
            return $textArea.data('linkedUp');
        }

        function findUrl(enteredText, clearUrl) {
            if (typeof (enteredText) === 'undefined') {
                enteredText = $textArea.val();
            }
            var replacePattern1, replacePattern2, url;
            replacePattern1 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
            url = replacePattern1.exec(enteredText);
            if (!url) {
                replacePattern2 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
                url = replacePattern2.exec(enteredText);
            }
            if (url) {
                url = (url.toString().split(',')[0]);
                updatePageValues(url, clearUrl);
            }
        }

        $textArea.data('linkedUp', false);

        $textArea.keyup(function (e) {

            var code = e.keyCode || e.which;
            if (code !== 13 && code !== 32) {
                return true;
            }
            if (!isLinkedUp()) {
                findUrl($(this).val());
            }

            return true;
        });

        $textArea.bind('paste', function () {
            setTimeout(function () {
                $textArea.val($textArea.val());
                if (!isLinkedUp()) {
                    findUrl();
                }
            }, 100);
        });

        $clearLink.click(function (e) {
            e.preventDefault();
            removePageValues();
        });

    };
    
    $.fn.previewify.defaultOptions = {
        textareaSelector: 'textarea',
        previewSelector: 'div',
        clearPreviewSelector: 'a'
    };
    
    $.fn.journalComments = function (options) {
        $.fn.journalComments.defaultOptions = {
            maxLength: 250
        };

        var opts = $.extend({}, $.fn.journalComments.defaultOptions, options),
            $id = $(this).attr('id'),
            $maxLength = opts.maxLength,
            $loadmore = $('#' + $id + '-loadmore'),
            $textarea = $('#' + $id + '-txt'),
            $textareaRow = $('#' + $id + '-txtrow'),
            $fakeinput = $('#' + $id + '-faketxt'),
            $fakeinputRow = $('#' + $id + '-faketxtrow'),
            $delete = $('#' + $id + ' .jcmt-remove'),
            $postButton = $('#' + $id + '-postbtn'),
            $cancelButton = $('#' + $id + '-cancelbtn');

        var expandAllComments = function () {
            $loadmore.remove();
            $('#' + $id + ' > li[id^="cmt-"]').slideDown('fast');
        };

        var deleteComment = function (commentId) {
            var data = {};
            data.JournalId = $id.replace('jcmt-', '');
            data.CommentId = commentId; 
            postComment('CommentDelete', data);
            $('li#cmt-' + data.CommentId).remove();
            expandAllComments();

            var totalComments = $('#' + $id + ' > li[id^="cmt-"]').length;
            if (totalComments == 0) {
                $fakeinputRow.hide();
                $textareaRow.hide();
            }
        };

        var commentComplete = function (data) {
            $textareaRow.hide();
            $textarea.val('');
            $fakeinputRow.show();
            var li = $(data);
            li.insertBefore($fakeinputRow);
            $(li).find('.jcmt-remove').dnnConfirm({
                text: opts.resxConfirm,
                yesText: opts.resxConfirmYes,
                noText: opts.resxConfirmNo,
                title: opts.resxConfirmTitle,
                isButton: true,
                callbackTrue: function () {
                    var commentId = li.attr('id').replace('cmt-', '');
                    deleteComment(commentId);
                }
            });

            $(li).find('a[id^="likecomment-"]').each(function () {
                if ($(this).data("clickBinded")) {
                    return;
                }
                $(this).data("clickBinded", true);
                $(this).click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr('id');
                    var cid = id.replace('likecomment-', '');
                    var d = {};
                    d.commentId = cid;
                    var viewModel = window.ko.contextFor(document.getElementById('activityStream-ScopeWrapper'));
                    var like = $(this).text() === opts.resxLike;
                    viewModel.$root.socialPost(like ? 'LikeComment' : 'UnlikeComment', d, viewModel.$root.buildCommentLikes, cid);
                });
            });
        };

        var postComment = function (method, data, callback, journalId) {
            var sf = opts.servicesFramework;
            $.ajax({
                type: "POST",
                url: sf.getServiceRoot('DNNCorp/ActivityStream') + 'ActivityStreamServices/' + method,
                beforeSend: sf.setModuleHeaders,
                data: data,
                success: function (d) {
                    if (typeof (callback) != "undefined") {
                        callback(d, journalId);
                    }
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
        };

        $delete.each(function () {
            var commentId = $(this).attr('id').replace('jcmt-', '').replace('-remove', '');
            $(this).dnnConfirm({
                text: opts.resxConfirm,
                yesText: opts.resxConfirmYes,
                noText: opts.resxConfirmNo,
                title: opts.resxConfirmTitle,
                isButton: true,
                callbackTrue: function() {
                    deleteComment(commentId);
                }
            });
        });

        $('a', $loadmore).on('click', expandAllComments);

        $postButton.on('click', function () {
            var txt = $textarea.val();
            if (txt) {
                var jid = $id.replace('jcmt-', '');
                var data = {};
                data.JournalId = jid;
                data.Comment = encodeURIComponent($textarea.val());
                var tmpValue = $textarea.val();
                tmpValue = tmpValue.replace(/<(?:.|\n)*?>/gm, '').replace(/\s+/g, '').replace(/&nbsp;/g, '');
                if (tmpValue == '') {
                    return;
                }
                if (data.Comment == '' || data.Comment == '%3Cbr%3E') {
                    return;
                }
                postComment('CommentSave', data, commentComplete, jid);
            }
        });

        $cancelButton.on('click', function () {
            $textareaRow.hide();
            $textarea.val('');
            var totalComments = $('#' + $id + ' > li[id^="cmt-"]').length;
            if (totalComments > 0) {
                $fakeinputRow.show();
            }
        });

        $fakeinput.on('focus', function () {
            expandAllComments();
            $fakeinputRow.hide();
            $textareaRow.show();
            $textarea.focus();
        });

    };
    
    var supportAjaxUpload = function () {
        var xhr = new XMLHttpRequest;
        return !!(xhr && ('upload' in xhr) && ('onprogress' in xhr.upload));
    };

    $.fn.dnnUserFileUpload = function (options) {
        var opts = $.extend({}, $.fn.dnnUserFileUpload.defaultOptions, options),
            $wrap = $(this),
            $fileUploadWrapperSelector = $(opts.fileUploadWrapperSelector);

        function displayError(message) {
            $(opts.progressContextSelector).hide('fade');
            var $message = $(opts.errorMessageSelector);
            $message.text(message).show();
            setTimeout((function () { $message.hide('fade'); }), 4000);
        }

        // error response 
        $fileUploadWrapperSelector.bind('fileuploadfail', function (e, data) {
            opts.complete(data);
            displayError(opts.serverErrorMessage);
        });

        // success response
        $fileUploadWrapperSelector.bind('fileuploaddone', function (e, data) {
            opts.complete(data);
            var result;
            if (data.result && data.result[0]) {
                if (data.result[0].body) {
                    result = $.parseJSON(data.result[0].body.innerText)[0];
                } else {
                    result = data.result[0];
                }
                
                if (result.success) {
                    opts.callback(result);
                }
                else {
                    displayError(result.message);
                }
            }
        });

        var url = opts.addImageServiceUrl;
        if (!supportAjaxUpload()) {
            var antiForgeryToken = $('input[name="__RequestVerificationToken"]').val();
            url += '?__RequestVerificationToken=' + antiForgeryToken;
        }

        var $fileupload = $fileUploadWrapperSelector;
        if (!$fileupload.data('loaded')) {
            var uploadedFilesCount = 0;
            $fileupload.fileupload({
                dataType: 'json',
                url: url,
                maxFileSize: opts.maxFileSize,
                beforeSend: opts.beforeSend,
                add: function (e, data) {
                    var uploadFile = data.files[0];
                    if (uploadDocuments || (/\.(jpg|png|gif|jpe|jpeg|tiff)$/i).test(uploadFile.name)) {
                        uploadedFilesCount++;
                        if (uploadedFilesCount <= opts.maxFiles) {
                            data.context = $(opts.progressContextSelector);
                            data.context.show('fade');
                            data.submit();
                        }
                    }
                },
                progressall: function (e, data) {
                    if (data.context) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        data.context.find(opts.progressBarSelector).css('width', progress + '%').find('span').html(progress + '%');
                    }
                },
                done: function (e, data) {
                    uploadedFilesCount = 0;
                    if (data.context) {
                        data.context.find(opts.progressBarSelector).css('width', '100%').find('span').html('100%');
                        setTimeout((function () {
                            data.context.hide('fade');
                            var filePreviewContainer = $(".filePreviewArea");
                            if (filePreviewContainer.find('.preview-photo-item').length && !dnn.social.MobileView.onMobile) {
                                filePreviewContainer.imagesLoaded(function () {
                                    filePreviewContainer.masonry({
                                        columnWidth: 160,
                                        itemSelector: '.preview-photo-item'
                                    });
                                });
                            }
                        }), 2000);
                    }
                }
            }).data('loaded', true);
        }
        $wrap.show();

    };

    $.fn.dnnUserFileUpload.defaultOptions = {
        fileUploadWrapperSelector: '.fileUploadArea', 
        addImageServiceUrl: '/DesktopModules/Journal/API/FileUpload/UploadFile',
        progressContextSelector: '.progress_context',
        progressFileNameSelector: '.upload_file_name',
        progressBarSelector: '.progress-bar div', 
        errorMessageSelector: '.fileupload-error',
        serverErrorMessage: 'Unexpected error. This generally happens when the file is too large.',
        beforeSend: null, 
        maxFiles: 5,
        callback: function (result) {
        },
        complete: function (data) {
        }
    };

}(jQuery, window));

