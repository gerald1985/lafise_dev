﻿// DotNetNuke® - http://www.dnnsoftware.com
// Copyright (c) 2002-2015, DNN Corp.
// by DNN Corporation
// All Rights Reserved

if (typeof dnn === 'undefined' || !dnn) dnn = {};
if (typeof dnn.activityStream === 'undefined') dnn.activityStream = {};

(function (dnn, $, ko) {
    'use strict';

    dnn.activityStream.ActivityType = function (name, id) {
        this.FilterType = name;
        this.FilterTypeId = id;
    };
    
    dnn.activityStream.ListView = function ListView(settings) {

        if (settings) {
            this.grouping = settings.grouping;
            this.nextListIndex = settings.initialNextListIndex;
        }
        else {
            this.grouping = false;
            this.nextListIndex = 1;
        }

        var that = this;
        
        this.buildLikes = function(data, journalId) {
            var currLike = $('#like-' + journalId).text();
            if (currLike == settings.resxLike) {
                $('#like-' + journalId).fadeOut(function() {
                    $(this).text(settings.resxUnLike).fadeIn();
                });
            } else {
                $('#like-' + journalId).fadeOut(function() {
                    $(this).text(settings.resxLike).fadeIn();
                });
            }
            $('#jid-' + journalId + ' > div.journalitem > div.likes').fadeOut(function () {
                var bigLikes = $(this).next();
                if (!bigLikes.length) return;
                var smallLikes = bigLikes.next();
                if (!smallLikes.length) return;
                bigLikes.html(data.LikeList.Big);
                smallLikes.html(data.LikeList.Small);
                var likeString = that.isMobileView() ? data.LikeList.Small : data.LikeList.Big;
                $(this).html(likeString).fadeIn(function() {
                    var likeControl = dnn.social.initLikeControl();
                    likeControl.bindControls($(this));
                });
            });
        };

        this.buildCommentLikes = function(data, commentId) {
            var currLike = $('#likecomment-' + commentId).text();
            if (currLike == settings.resxLike) {
                $('#likecomment-' + commentId).fadeOut(function() {
                    $(this).text(settings.resxUnLike).fadeIn();
                });
            } else {
                $('#likecomment-' + commentId).fadeOut(function() {
                    $(this).text(settings.resxLike).fadeIn();
                });
            }
            $('#cmt-' + commentId + ' .likes').fadeOut(function () {
                var bigLikes = $(this).next();
                if (!bigLikes.length) return;
                var smallLikes = bigLikes.next();
                if (!smallLikes.length) return;
                bigLikes.html(data.LikeList.Big);
                smallLikes.html(data.LikeList.Small);
                var likeString = that.isMobileView() ? data.LikeList.Small : data.LikeList.Big;
                $(this).html(likeString).fadeIn(function () {
                    var likeControl = dnn.social.initLikeControl();
                    likeControl.bindControls($(this));
                });
            });
        };
        
        this.journalDelete = function(jid) {
            var data = {};
            data.JournalId = jid;
            that.journalPost('SoftDelete', data, that.journalRemove, jid);
        };
    
        this.journalRemove= function(data, jid) {
            $('#jid-' + jid).slideUp(function(){
                $(this).remove();
                var rows = $('#journalItems > .journalrow').length;
                if(rows == 0)
                    $('#noActivityStreamPlaceHolder').show();
            });
        };

        this.journalPost = function(method, data, callback, journalId) {
            var sf = $.ServicesFramework(settings.moduleId);
            $.ajax({
                type: "POST",
                url: sf.getServiceRoot('DNNCorp/ActivityStream') + "ActivityStreamServices/" + method,
                beforeSend: sf.setModuleHeaders,
                data: data,
                success: function(d) {
                    if (typeof(callback) != "undefined") {
                        callback(d, journalId);
                    }
                },
                error: function(xhr, status, error) {
                    alert(error);
                }
            });
        };

        this.socialPost = function(method, data, callback, journalId) {
            var sf = $.ServicesFramework(settings.moduleId);
            $.ajax({
                type: "POST",
                url: sf.getServiceRoot('DNNCorp/ActivityStream') + "Social/" + method,
                beforeSend: sf.setModuleHeaders,
                data: window.ko.toJSON(data),
                contentType: 'application/json; charset=UTF-8',
                dataType: 'json',
                success: function(d) {
                    if (typeof(callback) != "undefined") {
                        callback(d, journalId);
                    }
                },
                error: function(xhr, status, error) {
                    alert(error);
                }
            });
        };

        this.scroll = function(direction) {
            var data = {};
            data.ProfileId = settings.pid;
            data.GroupId = settings.gid;
            data.MaxRows = settings.pageSize;
            data.FilterId = $('#filterId').attr('value');
            data.JournalTypeId = $('#journalTypeId').attr('value');
            var largestJournalId = $(".journalrow")[0] ? $(".journalrow")[0].id.replace('jid-', '') : 0;

            if (direction == 'up') {
                data.NewerThanJournalId = largestJournalId;
                data.RowIndex = 1;
            } else if (direction == 'down') {
                data.OlderThanJournalId = largestJournalId;
                data.RowIndex = that.nextListIndex;
            }
            data.Grouping = that.grouping;
            that.getItems(direction, data);
        };

        this.getItems = function(direction, data) {
            var sf = $.ServicesFramework(settings.moduleId);
            var rows = $(".journalrow").get();
            $.ajax({
                type: "POST",
                url: sf.getServiceRoot('DNNCorp/ActivityStream') + 'ActivityStreamServices/GetListForProfile',
                beforeSend: sf.setModuleHeaders,
                data: data,
                success: function (response) {
                    if (response.NextListIndex) {
                        that.nextListIndex = parseInt(response.NextListIndex);
                    }
                    if (response.CssFiles && response.CssFiles.length) {
                        $.each(response.CssFiles, function(i, url) {
                            dnn.social.loadCss(url);
                        });
                    }
                    var parallelLoadJs = [];
                    if (response.JsFiles && response.JsFiles.length) {
                        $.each(response.JsFiles, function (i, url) {
                            var func = function (cb) { dnn.social.loadScript(url, cb); };
                            parallelLoadJs.push(func);
                        });
                    }
                    dnn.social.asyncParallel(parallelLoadJs, function () {
                        if (response.Settings && response.Settings.length) {
                            $.each(response.Settings, function (i, s) {
                                $('head').append(s);
                            });
                        }
                        if (response.Content.length > 0) {
                            var newRows, diff;
                            if (direction == 'up') {
                                $("#getNew").hide();
                                $("#journalItems").prepend(response.Content);
                                newRows = $(".journalrow").get();
                                diff = (newRows.length - rows.length);
                                if (diff >= settings.pageSize) {
                                    $("#journalItems").html(response.Content);
                                    $("#getMore").show();
                                }
                                that.pluginInit();
                                setTimeout(that.autoCheckNewItems, 20000);
                            } else if (direction == 'down') {
                                $("#journalItems").append(response.Content);
                                that.pluginInit();
                                newRows = $(".journalrow").get();
                                diff = (newRows.length - rows.length);
                                if (diff < settings.pageSize) {
                                    $("#getMore").hide();
                                }
                            } else if (direction == 'new') {
                                $("#getNew").hide();
                                $("#showAll").hide();
                                $("#journalItems").html(response.Content);
                                that.pluginInit();
                                newRows = $(".journalrow").get();
                                diff = (newRows.length - rows.length);
                                if (diff >= settings.pageSize) {
                                    $("#getMore").show();
                                }
                            }
                        } else {
                            $("#getMore").hide();
                        }
                        
                        dnn.social.EventQueue.push(
                         function () {
                             dnn.social.bindCommentContext(document.getElementById(settings.journalListControlId));
                         });
                    });
                },
                error: function(xhr, status, error) {
                    if (error.length > 0) {
                        alert(error);
                    }
                }
            });
        };

        dnn.social.ipc.register(settings, function (source, message) {
              if (message.FilterId == undefined) {
                  $('#filterId').attr('value', 0);
              } else {
                  $('#filterId').attr('value', message.FilterId);
              }
              if (message.ProfileId == undefined) {
                  $('#profileId').attr('value', 0);
              } else {
                  $('#profileId').attr('value', message.ProfileId);
              }
              if (message.JournalTypeId == undefined) {
                  $('#journalTypeId').attr('value', 0);
              } else {
                  $('#journalTypeId').attr('value', message.JournalTypeId);
              }

              var data = {};
              data.ProfileId = settings.pid;
              data.GroupId = settings.gid;
              data.RowIndex = 0;
              data.MaxRows = settings.pageSize;
              data.FilterId = $('#filterId').attr('value');
              data.JournalTypeId = $('#journalTypeId').attr('value');
              $('#journalItems').children().remove('.journalrow');
              $("#getMore").hide();
              data.Grouping = that.grouping;
              that.getItems('down', data);
          });
        
        this.social = new dnn.social.Module(settings);

        if (settings.allowableFilterTypes && settings.allowableFilterTypes.length) {
            var allowableFilterTypes = [];
            $.each(settings.allowableFilterTypes, function (index) {
                allowableFilterTypes.push(
                    new dnn.activityStream.ActivityType(settings.allowableFilterTypes[index].JournalType,
                        settings.allowableFilterTypes[index].JournalTypeId));
            });
            this.allowableFilterTypes = ko.observableArray(allowableFilterTypes);
        }
        else {
            this.allowableFilterTypes = ko.observableArray([]);
        }

        this.selectedFilter = ko.observable('');
        this.selectActivityTypeCaption = ko.observable(that.social.getLocalizationController().getString('SelectActivityType'));
        this.selectedFilter.subscribe(function () {
            dnn.social.ipc.post(settings, 'DNNCorp/ActivityStream', { JournalTypeId: that.selectedFilter()[0] });
        });

        this.switchingView = function() {
            $(".ui-dialog-content").dialog("close");
        };

        this.switchedView = function (fromPlugin) {
            if (that.dynamicPlugins && that.dynamicPlugins.length) {
                $.each(that.dynamicPlugins, function (i, p) {
                    p.init(settings);
                });
            }

            $('div.likes', settings.moduleScope).each(function () {
                var bigLikes = $(this).next();
                if (!bigLikes.length) return;
                var smallLikes = bigLikes.next();
                if (!smallLikes.length) return;
                var isMobile = that.isMobileView();
                var likeString = isMobile ? smallLikes.html() : bigLikes.html();
                $(this).html(likeString);
                isMobile ? $(this).addClass('dnnMobileLike') : $(this).removeClass('dnnMobileLike');
            });
            var likeControl = dnn.social.initLikeControl();
            likeControl.bindControls();

            $('span.journal-grouped-section', settings.moduleScope).each(function () {
                var bigGrouped = $(this).next();
                if (!bigGrouped.length) return;
                var smallGrouped = bigGrouped.next();
                if (!smallGrouped.length) return;
                var isMobile = that.isMobileView();
                var groupedString = isMobile ? smallGrouped.html() : bigGrouped.html();
                $(this).html(groupedString);
                isMobile ? $(this).addClass('dnnMobileLike') : $(this).removeClass('dnnMobileLike');
            });
            var groupedControl = dnn.activityStream.initGroupedJournalControl();
            groupedControl.bindControls();
            
            $('.journalrow .jlink > iframe').each(function() {
                var w = $(this).width();
                var h = $(this).height();
                var r = h / w;
                w = $(this).parent().width();
                h = w * r;
                $(this).css({ 'width': w, 'height': h });
            });

            var gallerySettings = { size: 180, margin: 10, isMobile: that.isMobileView(), count: 6 };
            $(".gallery-control").each(function () {
                if(fromPlugin)
                    dnn.social.initGalleryControl($(this), gallerySettings);
                else 
                    dnn.social.reinitGalleryControl($(this), gallerySettings);
            });
        };

        this.pluginInit = function () {
            var commentOpts = {};
            commentOpts.servicesFramework = $.ServicesFramework(settings.moduleId);
            commentOpts.maxLength = settings.maxLength;
            commentOpts.resxLike = settings.resxLike;
            commentOpts.resxConfirm = settings.confirmText;
            commentOpts.resxConfirmYes = settings.confirmYesText;
            commentOpts.resxConfirmNo = settings.confirmNoText;
            commentOpts.resxConfirmTitle = settings.confirmTitleText;
            
            $('.jcmt').each(function () {
                if ($(this).data("journalCommentsBinded")) return;
                $(this).data("journalCommentsBinded", true);
                $(this).journalComments(commentOpts);
            });
            
            var rows = $(".journalrow");
            if (rows.length == settings.pageSize) $("#getMore").show();
            
            $('a[id^="cmtbtn-"]').each(function () {
                if ($(this).data("clickBinded")) return;
                $(this).data("clickBinded", true);
                $(this).click(function (e) {
                    e.preventDefault();
                    var jid = $(this).attr('id').replace('cmtbtn-', '');
                    var totalComments = $('#jcmt-' + jid + '> li[id^="cmt-"]').length;
                    var cmtfakeinput = $("#jcmt-" + jid + "-faketxtrow");
                    var cmtarea = $("#jcmt-" + jid + "-txtrow");
                    var cmttextarea = $("#jcmt-" + jid + "-txt");
                    var cmtloadmore = $("#jcmt-" + jid + "-loadmore");
                    if (cmtarea.css('display') == 'none') {
                        cmtloadmore.remove();
                        $('#jcmt-' + jid + '> li[id^="cmt-"]').show();
                        cmtfakeinput.hide();
                        cmtarea.slideDown('fast',
                            function () {
                                $("#jcmt-" + jid + "-txt").focus();
                            });
                    } else {
                        cmtarea.slideUp('fast');
                        $('textarea', cmtarea).text('');
                        cmttextarea.val('');
                        if (totalComments > 0) {
                            cmtfakeinput.show();
                        }
                    }
                });
            });
            
            $('a[id^="like-"]').each(function () {
                if ($(this).data("clickBinded")) return;
                $(this).data("clickBinded", true);
                $(this).click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr('id');
                    var jid = id.replace('like-', '');
                    var data = {};
                    data.JournalId = jid;
                    that.journalPost('ToggleLike', data, that.buildLikes, jid);
                });
            });
            
            $('a[id^="likecomment-"]').each(function () {
                if ($(this).data("clickBinded")) return;
                $(this).data("clickBinded", true);
                $(this).click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr('id');
                    var cid = id.replace('likecomment-', '');
                    var data = {};
                    data.commentId = cid;
                    var like = $(this).text() === settings.resxLike;
                    that.socialPost(like ? 'LikeComment' : 'UnlikeComment', data, that.buildCommentLikes, cid);
                });
            });
            
            $(".journalrow .journalitem-remove").each(function () {
                if ($(this).data("confirmBinded")) return;
                $(this).data("confirmBinded", true);
                var parentJournalItem = $(this).parents('.journalrow');
                var jid = parentJournalItem.attr('id').replace('jid-', '');
                $(this).dnnConfirm({
                    text: settings.confirmText,
                    yesText: settings.confirmYesText,
                    noText: settings.confirmNoText,
                    title: settings.confirmTitleText,
                    isButton: true,
                    callbackTrue: function () {
                        that.journalDelete(jid);
                    }
                });
            });

            $(".journalrow .jlink a.journalVideoEmbed").each(function() {
                if ($(this).data("embedBinded")) return;
                $(this).data("embedBinded", true);
                $(this).click(function () {
                    var parent = $(this).parent();
                    var width = parent.width();
                    var height = Math.round(width / 1280 * 720);
                    var src = $(this).attr('href');
                    if (!src) return false;
                    if (src.indexOf('?') > 0) src += '&autoplay=1';
                    else src += '?autoplay=1';
                    
                    parent.prepend('<iframe class="youtube-player" type="text/html" width="' + width + '" height="' + height + '" src="' + src + '" frameborder="0" allowfullscreen></iframe>');
                    
                    $(this).remove();
                    return false;
                });
            });
            
            if (!$("#getMore").data("clickBinded")) {
                $("#getMore").click(function (e) {
                    that.scroll('down');
                    e.preventDefault();
                });
                $("#getMore").data("clickBinded", true);
            }

            if (!$("#getNew").data("clickBinded")) {
                $("#getNew").click(function (e) {
                    that.scroll('up');
                    e.preventDefault();
                });
                $("#getNew").data("clickBinded", true);
            }
            
            if (!$("#showAll").data("clickBinded")) {                
                $("#showAll").click(function (e) {
                    var data = {};
                    data.ProfileId = settings.pid;
                    data.GroupId = settings.gid;
                    data.MaxRows = settings.pageSize;
                    data.FilterId = $('#filterId').attr('value');
                    data.JournalTypeId = $('#journalTypeId').attr('value');
                    data.Grouping = that.grouping;
                    that.getItems("new", data);
                    e.preventDefault();
                });
                $("#showAll").data("clickBinded", true);
            }

            that.switchedView(true);
        };
        
        this.autoCheckNewItems = function() {
            var data = {};
            data.profileId = settings.pid;
            data.groupId = settings.gid;
            data.filterId = $('#filterId').attr('value');
            data.journalTypeId = $('#journalTypeId').attr('value');
            data.newerThanJournalId = $(".journalrow")[0] ? $(".journalrow")[0].id.replace('jid-', '') : 0;
            var sf = $.ServicesFramework(settings.moduleId);

            $.ajax({
                type: "GET",
                url: sf.getServiceRoot('DNNCorp/ActivityStream') + 'ActivityStreamServices/CheckNewUpdates',
                beforeSend: sf.setModuleHeaders,
                data: data,
                success: function (d) {
                    if (d > 0) {
                        $("#getNew").show();
                        clearTimeout(that.autoCheckNewItems);
                    } else {
                        setTimeout(that.autoCheckNewItems, 20000);
                    }
                },
                error: function () {
                    clearTimeout(that.autoCheckNewItems);
                }
            });
        };
        
        dnn.social.MobileView.init($, ko, this, this.switchingView, this.switchedView);
        
        this.binded = function () {            
            var sf = $.ServicesFramework(settings.moduleId);
            var journalServiceBase = sf.getServiceRoot('DNNCorp/ActivityStream');
            $('.juser').click(function () {
                var uid = $(this).attr('id').replace('user-', '');
                window.location.href = settings.profilePage.replace('[uid]', uid);
            });
            var maxUploadSize = settings.maxUploadSize;
            $('.fileUploadArea').dnnUserFileUpload({
                maxFileSize: maxUploadSize,
                maxFiles: settings.maxFiles,
                serverErrorMessage: settings.serverErrorMessage,
                addImageServiceUrl: journalServiceBase + 'FileUpload/UploadFile',
                beforeSend: sf.setModuleHeaders,
                callback: function (result) {
                    if ($.isImage(result.extension)) {
                        $.attachPhoto(result.file_id, result.name, result.url);
                    } else {
                        $.attachDocument(result.file_id, result.name, result.thumbnail_url);
                    }
                },
                complete: function () {
                }
            });
            
            var jopts = {};
            jopts.maxLength = settings.maxLength;
            jopts.servicesFramework = sf;
            jopts.pid = settings.pid;
            jopts.gid = settings.gid;
            $('body').journalTools(jopts);
            if (settings.journalId > 0) {
                $("#showAll").show();
            }

            $('a#activityStream_noActivityLink').click(function(){
                if (settings.showEditor) {
                    if ($('#journalContent').css('display') == 'none') {
                        $('#journalPlaceholder').click();
                    }
                    $('#jounalContent').focus();
                }
                else
                    window.location.href = settings.loginUrl;
            });
            
            $('#userFileManager').userFileManager({
                title: settings.fileManagerTitle,
                cancelText: settings.fileManagerCancelText,
                attachText: settings.fileManagerAttachText,
                getItemsServiceUrl: sf.getServiceRoot('InternalServices') + 'UserFile/GetItems',
                nameHeaderText: settings.fileManagerNameHeaderText,
                typeHeaderText: settings.fileManagerTypeHeaderText,
                lastModifiedHeaderText: settings.fileManagerLastModifiedHeaderText,
                fileSizeText: settings.fileManagerFileSizeText,
                templatePath: settings.fileManagerTemplatePath,
                templateName: 'Default',
                templateExtension: '.html',
                attachCallback: function (file) {
                    if ($.isImage(file.type)) {
                        $.attachPhoto(file.id, file.name, file.thumb_url);
                    } else {
                        $.attachDocument(file.id, file.name, file.thumb_url);
                    }
                }
            });
            
            var opts = {};
            opts.textareaSelector = '#journalContent';
            opts.clearPreviewSelector = '#linkClose';
            opts.previewSelector = '#linkArea';
            opts.servicesFramework = $.ServicesFramework(settings.moduleId);
            $('body').previewify(opts);
            
            var focusController = new dnn.social.FocusController($, ko, null, null, window.document, []);
            var hideCard = function() { dnn.social.hideCommentContext(); };
            $(window.document).bind('focused', hideCard);
            dnn.social.bindCommentContext(settings.moduleScope, this);

            if (settings.initialDynamicCss && settings.initialDynamicCss.length) {
                $.each(settings.initialDynamicCss, function (i, url) {
                    dnn.social.loadCss(url);
                });
            }
            
            var parallelLoadJs = [];
            if (settings.initialDynamicJs && settings.initialDynamicJs.length) {
                $.each(settings.initialDynamicJs, function (i, url) {
                    var func = function (cb) { dnn.social.loadScript(url, cb); };
                    parallelLoadJs.push(func);
                });
            }

            dnn.social.asyncParallel(parallelLoadJs, function() {
                that.pluginInit();
            });

            setTimeout(that.autoCheckNewItems, 20000);

            if ($.fn.dnnFileInput) {
                $.fn.dnnFileInput.defaultOptions = {
                    buttonClass: 'dnnSecondaryAction',
                    showSelectedFileNameAsButtonText: false
                };
            }
        };
    };

})(window.dnn, window.jQuery, window.ko);