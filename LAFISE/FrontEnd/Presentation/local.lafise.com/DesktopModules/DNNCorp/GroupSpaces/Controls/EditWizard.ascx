﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditWizard.ascx.cs" Inherits="DotNetNuke.Professional.GroupSpaces.Controls.EditWizard" %>
<%@ Register TagPrefix="dnn" TagName="FilePickerUploader" Src="~/controls/filepickeruploader.ascx" %>

<asp:Panel ID="ScopeWrapper" runat="server">
    <div id="groupFields" class="dnnForm group-container">
        <div class="dnnFormItem">
            <div class="dnnLabel">
                <label>              
                    <span class="dnnFormRequired"><%= GetString("GroupName") %></span>
                </label>
            </div>
            <input type="text" maxlength="150" id="groupName" data-bind="value: groupName" class="required group-add-name customValidate validateGroupName" />
        </div>
        <div class="dnnFormItem">
            <div class="dnnLabel">
                <label>
                    <span class="dnnFormRequired"><%= GetString("Description") %></span>
                </label>
            </div>            
            <textarea class="required" rows="7" cols="30" id="groupDescription" data-bind="value: description"></textarea>
        </div>
        <div class="dnnFormItem">
            <div class="dnnLabel">
                <label>
                    <span class="dnnFormRequired"><%= GetString("GroupPicture") %></span>
                </label>
            </div>
            <div>
                <dnn:FilePickerUploader id="groupImageFile" runat="server" class="required" />
            </div>
        </div>
        <div class="dnnFormItem">
            <div class="dnnLabel">
                <label for="tags">
                    <span class="dnnFormRequired"><%= GetString("Tags") %></span>
                </label>
            </div>
            <input type="text" id="groupTags" class="required" />
        </div>
        <br/>
        <div class="dnnFormItem">
            <div class="dnnLabel">
                <label>
                    <span class="dnnFormRequired"><%= GetString("Accessibility") %></span>
                </label>
            </div>
            <div class="accessibility">                       
                <div>
                    <div class="accessibility-label">
                        <input type="radio" name="public" value="true" data-bind="checked: isPublic" /> 
                        <label><%=GetString("Public.Text")%></label>
                    </div>
                    <div class="accessibility-help"><%=GetString("Public.Help")%></div>
                    <div class="accessibility-review">
                        <input id="reviewMembers" type="checkbox" data-bind="checked: reviewMembers" /><label><%=GetString("ReviewNewMembers") %></label>                    
                        <div class="mustBeApproved"><%=GetString("MembersMustBeApproved")%></div>                    
                    </div>
                    
                </div>
                <div>
                    <div class="accessibility-label-private">
                        <input type="radio" name="private" value="false" data-bind="checked: isPublic" />
                        <label><%=GetString("Private.Text")%></label>
                    </div>
                    <div class="accessibility-help-private"><%=GetString("Private.Help")%></div>
                </div>
            </div>    
        </div>
        <hr/>
        <ul class="dnnActions dnnClear">
	        <li><a data-bind="click: $root.update" class="dnnPrimaryAction"><%= GetString("SaveGroup") %></a></li>
	        <li><a href="#" data-bind="click: $root.cancel" class="dnnSecondaryAction"><%= GetString("Cancel") %></a></li>
        </ul>

    </div>
</asp:Panel>

<script type="text/javascript">
    function editGroupInit(settings) {
        settings.moduleScope = $(settings.moduleScope)[0];
        var validator = new window.dnn.social.Validator($, window.ko, settings);
        var sf = $.ServicesFramework(settings.moduleId);
        settings.validator = validator;
        settings.validate = validator.validate;

        try {
            window.dnn.social.applyBindings(new window.dnn.groupSpaces.EditWizard($, window.ko, sf, settings), settings.moduleScope);
        } catch (ex) {
            window.dnn.social.topLevelExceptionHandler(settings, ex);
        }
        window.dnn.social.scopeWrapperLoaded('<%= ScopeWrapper.ClientID %>');
    }

</script>
