﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Upload.ascx.cs" Inherits="DotNetNuke.Enterprise.Publisher.Upload" %>

<asp:Panel ID="ScopeWrapper" runat="server" CssClass="upload">
    <ul class="dnnButtonGroup">
        <li>
            <a href="#upload-file" class="upload-file" 
                data-bind="click: activateTab, css: { active: activeTab() == 'upload-file' }">
                <%= LocalizeString("UploadFile") %>
            </a>
        </li>
        <li>
            <a href="#stored-location" class="stored-location" 
                data-bind="click: activateTab, css: { active: activeTab() == 'stored-location' }">
                <%= LocalizeString("StoredLocation") %>
            </a>
        </li>
        <li>
            <a href="#from-url" class="from-url" 
                data-bind="click: activateTab, css: { active: activeTab() == 'from-url' } ">
                <%= LocalizeString("FromUrl") %>
            </a>
        </li>
    </ul>
    <div id="upload-file" class="upload-file" data-bind="visible: activeTab() == 'upload-file'">
        <div class="droparea" data-bind="visible: !uploading(), event: { dragover: dragOver, dragleave: dragLeave }, css: { dragover: draggingOver }">
            <div class="drag-and-drop"><%= LocalizeString("DragAndDrop") %></div>
            <div class="or-borders"><div class="or"><%= LocalizeString("Or") %></div></div>
            <div class="browse"><%= LocalizeString("Browse") %></div>
            <input type="file" multiple name="postfile" id="uploader">
        </div>
        <div class="progress" data-bind="visible: uploading()">
            <div class="upload-status-progress complete">
                <div class="bar">
                    <span data-bind="style: { width: uploadPercent() + '%' }"></span>
                </div>
                <span class="btn-cancel"></span>
            </div>
        </div>
    </div>
    <div id="stored-location" class="stored-location" data-bind="visible: activeTab() == 'stored-location'">
        <div class="stored-location-top">
            
            <input type="text" data-bind="value: searchInput, valueUpdate: 'afterkeydown'" class="stored-location-search" placeholder="<%= LocalizeString("Search") %>">
            
            <div id="folderPicker" class="folder dnnDropDownList"
                    data-bind="folderPicker: {
                                koElement: currentFolder,
                                selectFolderCallback: selectedFolderChanged,
                                selectedItemText: '<%= LocalizeString("RecentUploads") %>',
                                options: {
                                    selectedItemCss: 'selected-item',
                                    internalStateFieldId: null,
                                    services: {
                                        parameters: {
                                            permission: 'READ,ADD'
                                        }
                                    },
                                    itemList: {
                                        firstItem: {
                                            key: -1, value: '<%= LocalizeString("RecentUploads") %>'
                                        },
                                        disableUnspecifiedOrder: true
                                    }
                                }
                            }">
                <div class="selected-item">
                    <a href="javascript:void(0);" class="selected-value"><%=LocalizeString("SelectAFolder") %></a>
                </div>
            </div>
        </div>
        <div class="stored-location-list" data-bind="foreach: storedFiles">
            <span class="thumb_wrapper" data-bind="css: { checked: id === $root.selectedFileId() }, click: $root.itemClick">
                <span class="thumb_holder">
                    <img data-bind="attr: { src: thumb, rel: thumb, title: title }">
                    <span class="active-state"></span>
                </span>
                <div class="name" data-bind="text: title"></div>
            </span>        
        </div>
        
        <div class="buttons">
            <a href="javascript:void(0);" class="insert-image" data-bind="click: setStoredImage"><%= LocalizeString("InsertImage") %></a>            
        </div>
    </div>
    <div id="from-url" class="from-url" data-bind="visible: activeTab() == 'from-url'">
            
        <div data-bind="visible: !uploadingFromUrl()">
            <label for="file-url"><%= LocalizeString("EnterUrl") %> (<span class="example_url"><%= LocalizeString("ExampleUrl") %></span>)</label>
            <input type="text" id="file-url" data-bind="value: uploadUrl, valueUpdate: 'afterkeydown', css: { error: uploadUrl() != '' && !previewAvailable() }">

            <a href="javascript:void(0);" class="upload" data-bind="click: uploadFromUrl"><%= LocalizeString("Upload") %></a>

            <div class="preview"><img data-bind="visible: previewAvailable, attr: { src: previewUrl }, event: { error: hidePreview }" /></div>            
        </div>
        <div data-bind="visible: uploadingFromUrl()" class="uploading">
            <div></div>
        </div>
    </div>
     <script type="text/javascript">
         jQuery(function() {
             var moduleId = <%= ModuleId %>;
             dnn.modules.publisher.RequestUtils.init(moduleId);
             var config = {
                 moduleId: moduleId,
                 tabId: <%= TabId %>,
                 bindingElementSelector: '#<%= ScopeWrapper.ClientID %>'
             };
             dnn.modules.publisher.UploadManager.init(config);
         });
    </script>

</asp:Panel>