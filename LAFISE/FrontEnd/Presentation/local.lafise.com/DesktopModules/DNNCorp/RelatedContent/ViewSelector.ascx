﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewSelector.ascx.cs"
    Inherits="DotNetNuke.Professional.RelatedContent.ViewSelector" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<div align="center">
    <asp:dropdownlist runat="server" id="drpView" AutoPostBack="False">
        <asp:ListItem resourcekey="Management" Value="Management"></asp:ListItem>
        <asp:ListItem resourcekey="Display" Value="Display"></asp:ListItem>
    </asp:dropdownlist>
    <asp:button runat="server" id="btnAdd" onclick="btnAdd_Clicked" resourcekey="Add" cssclass="dnnPrimaryAction" />
</div>