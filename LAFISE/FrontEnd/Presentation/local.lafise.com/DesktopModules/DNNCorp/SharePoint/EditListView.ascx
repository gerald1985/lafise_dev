﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditListView.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.Views.EditListView" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<div class="dnnForm">
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblConnection" runat="server" ControlName="ddlConnection" />
		<asp:DropDownList ID="ddlConnection" AutoPostBack="true" runat="server">
		</asp:DropDownList>
		<asp:LinkButton ID="lnkConnect" Text="Connect" runat="server" />&nbsp;
		<asp:LinkButton ID="lnkEdit" Text="Edit" runat="server" />
	</div>
	<asp:Panel ID="pnlDetails" Visible="false" runat="server">
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblListsTree" runat="server" ControlName="tvLists" />
		
		<telerik:RadTreeView ID="tvLists" CssClass="dnnFormItemTree" Runat="server">
		</telerik:RadTreeView>
	</div>
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblPortals" runat="server" ControlName="ddlPortals" />
		<asp:DropDownList ID="ddlPortals" AutoPostBack="true" AppendDataBoundItems="true" runat="server">
			<asp:ListItem Text="All Sites" Value="-1" />
		</asp:DropDownList>
	</div>
	</asp:Panel>
	<ul class="dnnActions dnnClear">
			<li><asp:LinkButton ID="lnkCreate" Visible="false" resourcekey="lnkCreate" runat="server" CssClass="dnnPrimaryAction" />&nbsp;</li>
			<li><asp:HyperLink ID="lnkCancel" resourcekey="lnkCancel" runat="server" CssClass="dnnSecondaryAction"></asp:HyperLink></li>
	</ul>
</div>
