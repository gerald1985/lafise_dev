﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageConnectionsView.ascx.cs"
	Inherits="DotNetNuke.Enterprise.SharePoint.Views.ManageConnectionsView" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<h2 class="dnnFormSectionHead"><a href="#">Create/Manage SharePoint Connections</a></h2>

<div class="dnnClear">
	<div class="dnnForm">
		<div class="dnnFormItem">
        <dnn:Label resourcekey="lblFilterBy" runat="server" ControlName="ddlFilter" />
				<asp:DropDownList ID="ddlFilter" AutoPostBack="true" AppendDataBoundItems="true" runat="server">
					<asp:ListItem Text="All Sites" Value="-1" />
				</asp:DropDownList>		
		</div>
	</div>
	<asp:GridView ID="gvConnections" CssClass="dnnGrid dnnGridSharePoint" Width="100%" BorderStyle="None" GridLines="None"
		DataKeyNames="ConnectionId" 
		AutoGenerateColumns="false" runat="server" >
		<HeaderStyle CssClass="dnnGridHeader" />
		<RowStyle CssClass="dnnGridItem" />
		<AlternatingRowStyle CssClass="dnnGridAltItem" />
		<Columns>
			<asp:TemplateField ItemStyle-Width="40px">
				<ItemTemplate>
					<asp:ImageButton ImageUrl="~/icons/sigma/Edit_16X16_Standard.png" OnClientClick='<%#GetEditUrl(Container) %>' runat="server" />
					<asp:ImageButton ImageUrl="~/icons/sigma/Delete_16X16_Standard.png" CommandName="Delete" CommandArgument='<%#Container.DisplayIndex %>' 
						OnClientClick="return confirm('Are you sure to delete this connection?')" runat="server" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="ConnectionName" HeaderText="Connection Name" />
			<asp:BoundField DataField="SharePointSiteUrl" HeaderText="SharePoint Site Url" />
			<asp:TemplateField HeaderText="SharePoint" >
				<ItemTemplate>
					<%#GetSharePointVersion((byte)Eval("SharePointVersion"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="SharePointUserName" HeaderText="User Name" />
		</Columns>
	</asp:GridView>
</div>

<ul class="dnnActions dnnClear">
    <li><asp:HyperLink ID="lnkAddConnection" resourcekey="lnkAddConnection" runat="server" CssClass="dnnPrimaryAction"></asp:HyperLink> &nbsp;</li>
</ul>

