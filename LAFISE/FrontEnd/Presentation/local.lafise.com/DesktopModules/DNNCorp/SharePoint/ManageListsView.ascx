﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageListsView.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.Views.ManageListsView" %>
<%@ Register Assembly="DotNetNuke.Web" Namespace="DotNetNuke.Web.UI.WebControls" TagPrefix="dnn" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<h2 class="dnnFormSectionHead"><a href="#">Create/Manage SharePoint Lists</a></h2>

<div class="dnnClear">
	<div class="dnnForm">
		<div class="dnnFormItem">
        <dnn:Label resourcekey="lblFilterBy" runat="server" ControlName="ddlFilter" />
				<asp:DropDownList ID="ddlFilter" AutoPostBack="true" AppendDataBoundItems="true" runat="server">
					<asp:ListItem Text="All Sites" Value="-1" />
				</asp:DropDownList>		
		</div>
	</div>

	<table class="dnnGrid dnnGridSharePoint" style="">
	<tr class="dnnGridHeader">
		<th style="width:50px"></th>
		<th style="width:450px">List Name</th>
		<th style="width:60px">List Items</th>
		<th style="width:130px">Site Associations</th>
		<th style="width:130px">Live or Synchronized</th>
	</tr>
	<asp:Repeater ID="rpLists" runat="server">
		<ItemTemplate>
		<tr class="<%# GetRowCssClass( Container ) %> dnnSharePointCollapseRow">
			<td>
				<asp:ImageButton ID="lnkEdit" Visible='<%# (int)Eval("SynchType") == 1%>' ImageUrl="~/icons/sigma/Edit_16X16_Standard.png" 
						OnClientClick='<%#GetEditUrl(Container) %>' runat="server" />
				<asp:ImageButton ID="lnkDelete" Visible='<%# (int)Eval("SynchType") == 1%>' ImageUrl="~/icons/sigma/Delete_16X16_Standard.png" 
						CommandName="Delete" CommandArgument='<%#Eval("ListId") %>' 
						OnClientClick="return confirm('Are you sure to delete this list?')" runat="server" />
			</td>
			<td><%#Eval("ListName") %></td>
			<td><%# GetItemCount(Container)%></td>
			<td><%# GetSiteName(Container) %></td>
			<td><%#Eval("SynchType")%></td>
		</tr>
		<tr class="dnnSharePointRowDetails">
			<td colspan="2" style="width:500px">
				<div class="dnnForm" style="width:50%">
					<div class="dnnFormItem">
						<asp:Label resourcekey="lblListName" runat="server" AssociatedControlID="lblListNameValue" />
						<asp:Label ID="lblListNameValue" Text='<%#Eval("ListName") %>' runat="server" />
					</div>
					<div class="dnnFormItem" runat="server" visible='<%#Eval("SynchType").ToString() == "Synchronized"%>'>
						<asp:Label resourcekey="lblListItemCount" runat="server" AssociatedControlID="lblListItemCountValue" />
						<asp:Label ID="lblListItemCountValue" Text='<%# GetItemCount(Container) %>' runat="server" />
					</div>
					<div class="dnnFormItem">
						<asp:Label resourcekey="lblSiteName" runat="server" AssociatedControlID="lblSiteNameValue" />
						<asp:Label ID="lblSiteNameValue" Text='<%# GetSiteName(Container) %>' runat="server" />
					</div>
					<div class="dnnFormItem">
						<asp:Label resourcekey="lblLastSync" runat="server" AssociatedControlID="lblLastSyncValue" />
						<asp:Label ID="lblLastSyncValue" Text='<%# GetLastSync(Container) %>' runat="server" />
					</div>
					<div class="dnnFormItem">
						<asp:Label resourcekey="lblSyncType" runat="server" AssociatedControlID="lblSyncTypeValue" />
						<asp:Label ID="lblSyncTypeValue" Text='<%#Eval("SynchType")%>' runat="server" />
						
					</div>
				</div>
			
			</td>
			<td colspan="3" style="width:320px">
				<asp:GridView ID="gvColumns" DataSource='<%# GetColumnsSource(Container) %>' AutoGenerateColumns="false" CssClass="dnnGrid" GridLines="None" runat="server" >
					<HeaderStyle CssClass="dnnGridHeader" />
					<RowStyle CssClass="dnnGridItem" />
					<AlternatingRowStyle CssClass="dnnGridAltItem" />
					<Columns>
						<asp:BoundField DataField="Title" HeaderText="Column Name" />
						<asp:BoundField DataField="Type" HeaderText="Type" />
					</Columns>
				</asp:GridView>
			</td>
		</tr>
		</ItemTemplate>
	</asp:Repeater>

	</table>
</div>
<ul class="dnnActions dnnClear">
    <li><asp:HyperLink ID="lnkAddSyncList" resourcekey="lnkAddSyncList" runat="server" CssClass="dnnPrimaryAction" />&nbsp;</li>
    <li><a id="dnnAddSharePointListDialog" class="dnnPrimaryAction" href="#"><%=LocalizeString("dnnAddSharePointListDialog")%></a></li>
</ul>

<script type="text/javascript">
	/*globals jQuery, window, Sys */
	(function ($, Sys) {
		function setUpSharePointListsPane() {
			$('#dnnAddSharePointListDialog').click(function (event) {
				event.preventDefault();
				$.dnnAlert({
					title: '<%=LocalizeString("dnnAddSharePointListDialog")%>',
					text: $("#dnnSharePointTokenInfo").html(),
					width: 800
				});
			});

			$(".dnnSharePointCollapseRow").click(function () {
				$(this).next().slideToggle();
			});
		}

		$(document).ready(function () {
			setUpSharePointListsPane();

			Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
				setUpSharePointListsPane();
			});
		});
	} (jQuery, window.Sys));
</script>