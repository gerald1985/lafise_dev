﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimeControl.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.Controls.DateTimeControl" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<%@ Register TagPrefix="dnn" Assembly="DotNetNuke.Web" Namespace="DotNetNuke.Web.UI.WebControls" %>
<div class="dnnSPDateTimeControl">
<dnn:DnnDatePicker ID="dnnDate" runat="server" CssClass="dnnFormInput dnnSPFormDate"/>&nbsp;
<dnn:DnnTimePicker ID="dnnTime" Visible="false"  runat="server" CssClass="dnnFormInput dnnSPFormTime"/>
<asp:RequiredFieldValidator ID="rfValidator" ControlToValidate="dnnDate" Display="Dynamic"
		CssClass="dnnFormMessage dnnFormError" Visible="false" Enabled="false" runat="server" />
</div>