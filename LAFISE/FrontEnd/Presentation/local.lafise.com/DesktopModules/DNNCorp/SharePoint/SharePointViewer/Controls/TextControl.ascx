﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextControl.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.Controls.TextControl" %>
<asp:TextBox ID="txtTextbox" runat="server" />
<asp:RequiredFieldValidator ID="rfValidator" ControlToValidate="txtTextbox" 
	Display="Dynamic"
	CssClass="dnnFormMessage dnnFormError"
	Visible="false" Enabled="false"	runat="server" /><asp:Label Id="lblSuffix" Visible="false" runat="server" />
<asp:RegularExpressionValidator ID="regexValidator" ControlToValidate="txtTextbox"
	Display="Dynamic"
	CssClass="dnnFormMessage dnnFormError"
	Visible="false" Enabled="false"	runat="server" />
<asp:CompareValidator ID="typeValidator"  ControlToValidate="txtTextbox"
	Type="Double"
	Operator="DataTypeCheck"
	Display="Dynamic"
	CssClass="dnnFormMessage dnnFormError"
	Visible="false" Enabled="false"	runat="server" />
