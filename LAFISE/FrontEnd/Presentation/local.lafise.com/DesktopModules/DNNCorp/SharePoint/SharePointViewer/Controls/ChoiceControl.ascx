﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChoiceControl.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.Controls.ChoiceControl" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.UI.WebControls" Assembly="DotNetNuke.Web" %>
<dnn:DnnComboBox ID="ddlList" runat="server"></dnn:DnnComboBox>