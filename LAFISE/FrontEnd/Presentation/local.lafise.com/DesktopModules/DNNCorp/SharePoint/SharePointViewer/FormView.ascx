﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormView.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.FormView" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<div class="dnnForm dnnSPList dnnClear" id="dnnSPList">
	<div class="dnnFormMessage dnnFormInfo" id="dvFormMessage" runat="server" visible="false">
		<asp:Label ID="lblSettingsMessage" runat="server" resourcekey="lblSettingsMessage"></asp:Label>
	</div>
	<fieldset>
	<asp:Repeater ID="rpSharePointForm" DataSource='<%#ModuleFields %>' runat="server"
		EnableViewState="true">
		<ItemTemplate>
			<div class="dnnFormItem">
				<dnn:Label ID="lblFormLabel" Text='<%#Eval("DisplayName") %>' AssociatedControlID="phControl" runat="server" />
				<asp:PlaceHolder ID="phControl" runat="server" EnableViewState="true" />
			</div>
		</ItemTemplate>
	</asp:Repeater>
	</fieldset>
  <ul class="dnnActions dnnClear">
      <li><asp:LinkButton ID="btnSave" resourcekey="btnSave" runat="server" CssClass="dnnPrimaryAction" /></li>
			<li><asp:HyperLink ID="lnkCancel" resourcekey="lnkCancel" runat="server" CssClass="dnnSecondaryAction" /></li>
  </ul>
</div>
