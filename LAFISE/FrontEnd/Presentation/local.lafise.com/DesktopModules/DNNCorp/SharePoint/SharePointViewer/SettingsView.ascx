﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SettingsView.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.SettingsView" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.UI.WebControls" Assembly="DotNetNuke.Web" %>
<script type="text/javascript">
	var previouslySelectedSequenceItemValue;
	var selectedSequenceItemValue;
	var comboValuesAreUpdating = false;

	function onSequenceSelectedIndexChanging(sender, eventArgs) {
		if (!comboValuesAreUpdating) {
			var selectedSequenceItem = sender.get_selectedItem();
			previouslySelectedSequenceItemValue = selectedSequenceItem != null ? Number(selectedSequenceItem.get_text()) : Number(sender.get_text());
		}
	}

	function onSequenceSelectedIndexChanged(sender, eventArgs) {
		if (!comboValuesAreUpdating) {
			var selectedSequenceItem = eventArgs.get_item();
			selectedSequenceItemValue = selectedSequenceItem != null ? Number(selectedSequenceItem.get_text()) : Number(sender.get_text());
			var increasedSequence = (selectedSequenceItemValue > previouslySelectedSequenceItemValue);
			var iteratedOverNewNumber = false;
			$(".sequenceCBOs").each(function () {
				comboValuesAreUpdating = true;
				var comboSequence = $find($(this).attr("id"));
				var currentSelectedSequenceItemValue = Number(comboSequence.get_selectedItem().get_text());

				if (increasedSequence) {

					if (currentSelectedSequenceItemValue != selectedSequenceItemValue || (currentSelectedSequenceItemValue == selectedSequenceItemValue)) {

						if (currentSelectedSequenceItemValue == selectedSequenceItemValue && !iteratedOverNewNumber) {
							iteratedOverNewNumber = true;
						}
						else {
							if (currentSelectedSequenceItemValue > previouslySelectedSequenceItemValue && currentSelectedSequenceItemValue <= selectedSequenceItemValue) {
								var sequenceComboItem = comboSequence.findItemByText(currentSelectedSequenceItemValue - 1);
								if (sequenceComboItem) {
									sequenceComboItem.select();
								}
							}
						}
					}
				}
				else {
					if (currentSelectedSequenceItemValue != selectedSequenceItemValue || (currentSelectedSequenceItemValue == selectedSequenceItemValue && !iteratedOverNewNumber)) {

						if (currentSelectedSequenceItemValue == selectedSequenceItemValue) {
							iteratedOverNewNumber = true;
						}

						if (currentSelectedSequenceItemValue >= selectedSequenceItemValue && currentSelectedSequenceItemValue < previouslySelectedSequenceItemValue) {
							var sequenceComboItem = comboSequence.findItemByText(currentSelectedSequenceItemValue + 1);
							if (sequenceComboItem) {
								sequenceComboItem.select();
							}
						}

					}
				}

			});
			comboValuesAreUpdating = false;
		}
	}
</script>
<div class="dnnForm dnnHTMLSettings dnnClear">
	<div class="dnnFormItem">
		<dnn:label id="plDisplayMode" controlname="cboDisplayMode" runat="server" />		
		<dnn:DnnComboBox ID="cboDisplayMode" runat="server" AutoPostBack="True" CssClass="dnnFixedSizeComboBox" AppendDataBoundItems="true" >
			<Items>
				<dnn:DnnComboBoxItem ResourceKey="defaultComboSelection" Value="-1" />
				<dnn:DnnComboBoxItem ResourceKey="cboiListOnly" Value="0" />
				<dnn:DnnComboBoxItem ResourceKey="cboiListWithForms" Value="1" />
				<dnn:DnnComboBoxItem ResourceKey="cboiSubmissionOnly" Value="2" />
			</Items>
		</dnn:DnnComboBox>
	</div>
	<div class="dnnFormItem">
		<dnn:label id="plListName" controlname="cboLists" runat="server" />		
		<dnn:DnnComboBox ID="cboLists" runat="server" DataValueField="ListId" AutoPostBack="True" CssClass="dnnFixedSizeComboBox" AppendDataBoundItems="true" >
			<Items>
				<dnn:DnnComboBoxItem resourcekey="defaultComboSelection" Value="-1" />
			</Items>
		</dnn:DnnComboBox>
	</div>
	<div class="dnnFormItem">
		<dnn:label id="plPageSize" controlname="txtPageSize" runat="server" />		
		<dnn:DNNTextBox ID="txtPageSize" runat="server" Text="10"></dnn:DNNTextBox>
		<asp:CompareValidator ID="vldPageSize" resourcekey="vldPageSize" Type="Integer" runat="server" Text="*" CssClass="NormalRed" ControlToValidate="txtPageSize" Operator="DataTypeCheck"></asp:CompareValidator>
	</div>
	<div class="dnnFormItem">
		<asp:DataGrid ID="grdModuleFieldList" runat="server" Width="100%" GridLines="None"  OnItemDataBound="grdModuleFieldList_ItemDataBound" CellSpacing="0" CellPadding="0" CssClass="dnnGrid"
		 AutoGenerateColumns="False">
            <headerstyle cssclass="dnnGridHeader" />
            <itemstyle cssclass="dnnGridItem" />
            <alternatingitemstyle cssclass="dnnGridAltItem" />

            <footerstyle cssclass="dnnGridFooter" />
            <pagerstyle cssclass="dnnGridPager" />
            <Columns>
				<asp:TemplateColumn>
					<ItemTemplate>
						<asp:CheckBox ID="chkDisplay" runat="server" Checked='<%# Eval("IsActive") %>' />
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="DisplayName">
				
					<ItemTemplate>
						<dnn:DNNTextBox ID="txtDisplayName" runat="server" EmptyMessage='<%# Eval("SharePointFieldModel.Title") %>' Text='<%# Eval("ModuleFieldModel.DisplayName") %>'></dnn:DNNTextBox>
						<asp:HiddenField Value='<%# Eval("ModuleFieldModel.FieldGuid") %>' ID="hdnFieldGuid" runat="server" />
						<asp:HiddenField Value='<%# Eval("ModuleFieldId") %>' ID="hdnModuleFieldId" runat="server" />
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:BoundColumn DataField="Type" HeaderText="Type" />
                <asp:BoundColumn DataField="IsRequired" HeaderText="Required" />	
				
				<asp:TemplateColumn HeaderText="RequiredErrorMessage">
					<ItemTemplate>
						<dnn:DNNTextBox ID="txtRequiredErrorMessage" runat="server" Text='<%# Eval("RequiredErrorMessage") %>'></dnn:DNNTextBox>
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Regex">
					<ItemTemplate>
						<dnn:DNNTextBox ID="txtRegex" runat="server" Text='<%# Eval("Regex") %>' CssClass="dnnFormItem"></dnn:DNNTextBox>
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="RegexErrorMessage">
					<ItemTemplate>
						<dnn:DNNTextBox ID="txtRegexErrorMessage" runat="server" Text='<%# Eval("RegexErrorMessage") %>'></dnn:DNNTextBox>
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Order">
					<ItemTemplate>
					<dnn:DnnComboBox ID="cboSequenceId" runat="server" CssClass="dnnSmallSizeComboBox sequenceCBOs" OnClientSelectedIndexChanged="onSequenceSelectedIndexChanged" OnClientSelectedIndexChanging="onSequenceSelectedIndexChanging"></dnn:DnnComboBox>
					</ItemTemplate>
				</asp:TemplateColumn>
			</Columns>
			</asp:DataGrid>
	</div>
</div>