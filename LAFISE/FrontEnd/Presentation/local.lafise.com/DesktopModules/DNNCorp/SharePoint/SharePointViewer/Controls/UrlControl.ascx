﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UrlControl.ascx.cs"
	Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.Controls.UrlControl" %>

<div class="dnnSPUrlControl">
<div>
	<asp:Label ID="lblUrl" runat="server" /><br />
	<asp:TextBox ID="txtUrl" runat="server" />
	<asp:RequiredFieldValidator ID="rfValidator" ControlToValidate="txtUrl" Display="Dynamic"
		CssClass="dnnFormMessage dnnFormError" Visible="false" Enabled="false" runat="server" />
</div>
<div>
	<asp:Label ID="lblDescription" runat="server" /><br />
	<asp:TextBox ID="txtDescription" runat="server" />
</div>
</div>
