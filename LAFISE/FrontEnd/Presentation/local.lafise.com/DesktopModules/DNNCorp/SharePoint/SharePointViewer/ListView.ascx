﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListView.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.ListView" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<%@ Register TagPrefix="dnn" Assembly="DotNetNuke.Web" Namespace="DotNetNuke.Web.UI.WebControls" %>
<%@ Register src="FormView.ascx" tagname="FormView" tagprefix="dnn" %>

<asp:MultiView ID="mvSharePointViewer" ActiveViewIndex="0" runat="server">
	<asp:View  ID="viewList" runat="server">

		<div class="dnnForm dnnSPList dnnClear" id="dnnSPList">
			<div class="dnnFormMessage dnnFormInfo" id="dvFormMessage" runat="server" visible="false">
				<asp:Label ID="lblSettingsMessage" runat="server" resourcekey="lblSettingsMessage"></asp:Label>
			</div>
			<div>
				<asp:PlaceHolder ID="phGridPlaceHolder" runat="server" />
			</div>
			<ul class="dnnActions dnnClear">
				<li>
					<asp:HyperLink ID="lnkAdd" runat="server" CssClass="dnnPrimaryAction" resourcekey="lnkAdd"/>
				</li>
			</ul>
		</div>

	</asp:View>
	<asp:View ID="viewForm" runat="server">

		<dnn:FormView ID="FormView" runat="server" />

	</asp:View>
</asp:MultiView>




