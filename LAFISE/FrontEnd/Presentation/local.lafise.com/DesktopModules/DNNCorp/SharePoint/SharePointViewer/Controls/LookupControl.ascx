﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupControl.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.SharePointViewer.Views.Controls.LookupControl" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.UI.WebControls" Assembly="DotNetNuke.Web" %>
<dnn:DnnComboBox ID="ddlLookup" runat="server"></dnn:DnnComboBox>
