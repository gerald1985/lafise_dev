﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditConnectionView.ascx.cs"
	Inherits="DotNetNuke.Enterprise.SharePoint.Views.EditConnectionView" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<div class="dnnForm">
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblConnectionName" runat="server" ControlName="txtConnectionName" CssClass="dnnFormRequired"  />
		<asp:TextBox ID="txtConnectionName" runat="server" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="txtConnectionName" 
                CssClass="dnnFormMessage dnnFormError" ResourceKey="required" />
	</div>
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblSiteUrl" runat="server" ControlName="txtSiteUrl" CssClass="dnnFormRequired" />
		<asp:TextBox ID="txtSiteUrl" runat="server" />
		<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSiteUrl" 
                CssClass="dnnFormMessage dnnFormError" ResourceKey="required" />
	</div>
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblUsername" runat="server" ControlName="txtUsername" CssClass="dnnFormRequired" />
		<asp:TextBox ID="txtUsername" runat="server" />
	</div>
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblPassword" runat="server" ControlName="txtPassword" CssClass="dnnFormRequired" />
		<asp:TextBox ID="txtPassword" TextMode="Password" runat="server" />
	</div>
	<div class="dnnformitem" style="display:none">
		<dnn:label resourcekey="lblanonymous" runat="server" controlname="chbAnonymous" />
		<asp:checkbox id="chbAnonymous" runat="server" />
	</div>
	<div class="dnnFormItem">
		<dnn:Label resourcekey="lblPortal" runat="server" ControlName="ddlPortal" />
		<asp:DropDownList ID="ddlPortal" AppendDataBoundItems="true" runat="server">
			<asp:ListItem Text="All Sites" Value="-1" />
		</asp:DropDownList>		
	</div>
	<ul class="dnnActions dnnClear">
			<li><asp:LinkButton ID="lnkSave" resourcekey="lnkSave" runat="server" CssClass="dnnPrimaryAction" />&nbsp;</li>
			<li><asp:HyperLink ID="lnkCancel" resourcekey="lnkCancel" runat="server" CssClass="dnnSecondaryAction"></asp:HyperLink></li>
	</ul>
</div>

<script type="text/javascript">
	jQuery(function ($) {
		if ($("#<%=chbAnonymous.ClientID %>:checked").length > 0) {
			var $txtusr = $("#<%=txtUsername.ClientID %>").removeClass("dnnFormRequired");
			var $txtpsw = $("#<%=txtPassword.ClientID %>").removeClass("dnnFormRequired");
		}

		$("#<%=chbAnonymous.ClientID %>").change(function () {
			var $txtusr = $("#<%=txtUsername.ClientID %>").toggleClass("dnnFormRequired");
			var $txtpsw = $("#<%=txtPassword.ClientID %>").toggleClass("dnnFormRequired");
		});
	});
</script>