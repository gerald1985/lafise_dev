<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SharePointView.ascx.cs" Inherits="DotNetNuke.Enterprise.SharePoint.Views.SharePointView" EnableViewState="true" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="dnn" TagName="EventViewer" Src="~/DesktopModules/Admin/LogViewer/LogViewer.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="ManageLists" Src="ManageListsView.ascx"   %>
<%@ Register TagPrefix="dnn" TagName="ManageConnections" Src="ManageConnectionsView.ascx"   %>

<dnn:DnnCssInclude runat="server" FilePath="~/DesktopModules/Admin/LogViewer/module.css" />

<div style="display:none;" id="dnnSharePointTokenInfo">
	<div class="dnnForm dnnClear dnnFormSharePointInfo">
		<div class="dnnFormMessage dnnFormInfo">
			A synchronized SharePoint List must be set up in SharePoint. Please use the SharePoint 
			Connector in SharePoint to create your sync.
		</div>
    <div class="dnnFormItem">
        <dnn:Label resourcekey="lblAddressTitle" runat="server" ControlName="lblAddressDlg" />
        <asp:Label ID="lblAddressDlg" runat="server" />
    </div>  
    <div class="dnnFormItem">
      <dnn:Label resourcekey="lblTokenTitle" runat="server" ControlName="lblTokenDlg" />
      <b>[ <asp:Label ID="lblTokenSource" runat="server" /> ]</b> <asp:Label ID="lblTokenDlg" runat="server" />
    </div>
	</div>
</div>

<div class="dnnForm dnnFormSharePoint dnnClear" id="dnnFormSharePoint">
    <div class="dnnFormItem">
        <dnn:Label ID="lblAddressTitle" runat="server" ControlName="lblAddress" />
        <asp:Label ID="lblAddress" runat="server" />
    </div>  
    <div class="dnnFormItem">
        <dnn:Label ID="lblTokenTitle" runat="server" ControlName="lblToken" />
			<asp:DropDownList ID="ddlTokenType" runat="server" AutoPostBack="true">
				<asp:ListItem Text="Server Token" Value="HOST" />
				<asp:ListItem Text="Site Token" Value="SITE"/>
			</asp:DropDownList>
        <asp:Label ID="lblToken" runat="server" />
    </div>
    <ul class="dnnActions dnnClear">
        <li><asp:linkbutton ID="ResetServerToken" runat="server" CssClass="dnnPrimaryAction" 
					ResourceKey="ResetSharePointTokenKey" /></li>
    </ul>
</div>
<div class="dnnForm dnnClear" id="dnnSharePointTabs">
		<ul class="dnnAdminTabNav">
			<li><a href="#LogEntries">Log Entries</a></li>
			<li><a href="#SharePointLists">SharePoint Lists</a></li>
			<li><a href="#SharePointConnections">SharePoint Connections</a></li>
		</ul>

		<div id="LogEntries" class="dnnClear">
			<dnn:EventViewer ID="logViewer" runat="server" />
		</div>
		<div id="SharePointLists">
			<dnn:ManageLists ID="ManageLists1" runat="server" />
		</div>
		<div id="SharePointConnections">
			<dnn:ManageConnections ID="ManageConnections1" runat="server" />
		</div>
</div>

<script type="text/javascript">
	/*globals jQuery, window, Sys */
	(function ($, Sys)
	{
		function setUpSharePointForm()
		{
			$('#<%= ResetServerToken.ClientID %>').dnnConfirm({
				text: 'Are you sure to recycle token?',
				yesText: '<%= Localization.GetString("Yes.Text", Localization.SharedResourceFile) %>',
				noText: '<%= Localization.GetString("No.Text", Localization.SharedResourceFile) %>',
				title: '<%= Localization.GetString("Confirm.Text", Localization.SharedResourceFile) %>'
			});

			jQuery(function ($) {
				$('#dnnSharePointTabs').dnnTabs();
			});
		}

		$(document).ready(function ()
		{
			setUpSharePointForm();

			Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function ()
			{
				setUpSharePointForm();
			});
		});
	} (jQuery, window.Sys));
</script>