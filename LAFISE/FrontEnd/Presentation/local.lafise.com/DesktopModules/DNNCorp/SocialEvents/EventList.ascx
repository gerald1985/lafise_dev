﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EventList.ascx.cs" Inherits="DotNetNuke.Professional.SocialEvents.EventList" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>

<%@ Register src="EditEvent.ascx" tagPrefix="dnn" tagName="EditEvent" %>

<div class="dnnSEIndex dnnClear">
    
	<%if (AddVisible)
	  {%>
	<div class="seEventActions dnnClear">
		<ul class="eventMenu dnnRight">
			<% if (AddVisible)
			   {%><li><a class="dnnTertiaryAction AddSocialEvent" href="#"><%= Localization.GetString("AddEvent", LocalResourceFile) %></a></li><% }%>
		</ul>
	</div>
	<% } %>
	
    <asp:Repeater runat="server" ID="rptEvents" OnItemDataBound="rptEvents_OnItemDataBound">
		<ItemTemplate>
			<div class="seEventItem dnnClear">
				<div class="seSidebar dnnLeft">
					<asp:HyperLink ID="hlAuthor" runat="server"><asp:Image runat="server" ID="imgUser" /></asp:HyperLink>
					<div><cite><%= Localization.GetString("Organizer", LocalResourceFile) %></cite></div>
				</div>
				<div class="seDetailContent dnnLeft">
					<h2>
						<asp:HyperLink ID="hlName" runat="server" />
						<span class="dnnRight">
							<asp:HyperLink runat="server" ID="hlEdit" CssClass="seEdit SocialEventEdit dnnLeft"/>
							<asp:HyperLink runat="server" ID="hlDelete" CssClass="seDelete SocialEventDelete dnnLeft" NavigateUrl="javascript:void(0)"/>
						</span>
					</h2>  
					<div>
					    <strong><%= Localization.GetString("when", LocalResourceFile)%></strong>
                        <asp:Label runat="server" ID="lblStartDate" DateFormat="MMMM dd, yyyy hh:mm tt"/>&nbsp;<asp:Literal ID="litZone" runat="server" />
					</div>
					<div><strong><%= Localization.GetString("where", LocalResourceFile)%></strong><asp:Label runat="server" ID="lblAddress" /></div>
					<asp:Panel runat="server" ID="phAttend" class="seAttend dnnClear">
						<div class="dnnRight">						
							<ul class="buttonGroup eventMenu">                               
								<li><asp:HyperLink runat="server" ID="cmdMayBe" EventId='<%#DataBinder.Eval(Container.DataItem, "EventId") %>'  StatusType="3" NavigateUrl="#" CssClass="dnnTertiaryAction eventAttendStatus" resourcekey="Maybe" /></li>
								<li><asp:HyperLink runat="server" ID="cmdNo" EventId='<%#DataBinder.Eval(Container.DataItem, "EventId") %>'  StatusType="2" NavigateUrl="#" CssClass="dnnTertiaryAction eventAttendStatus" resourcekey="No" /></li>
								<li><asp:HyperLink runat="server" ID="cmdYes" EventId='<%#DataBinder.Eval(Container.DataItem, "EventId") %>'  StatusType="1" NavigateUrl="#" CssClass="dnnTertiaryAction eventAttendStatus" resourcekey="Yes" /></li>
								<li><strong><asp:Label runat="server" ID="lblAttending" /></strong>&nbsp;&nbsp;</li>
							</ul>	
						</div>
					</asp:Panel>	
				</div>
			</div>
		</ItemTemplate>
	</asp:Repeater>

	<%if (PreviousVisible || NextVisible)
	  {%>

	<div class="seEventActions dnnClear">
		<ul class="seventMenu dnnRight">
			<% if (PreviousVisible)
			   {%><li><a class="dnnPrimaryAction" href="<%= PreviousUrl %>"><%= Localization.GetString("Previous", LocalResourceFile) %></a></li><% }%>
			<% if (NextVisible)
			   {%><li><a class="dnnPrimaryAction" href="<%= NextUrl %>"><%= Localization.GetString("Next", LocalResourceFile) %></a></li><% }%>
		</ul>
	</div>
	<% } %>
</div>

<div >
	<dnn:EditEvent ID="ctlEdit" runat="server" />
</div>


<script type="text/javascript">
	new SocialEventGuestManager(jQuery, ko, {
		portalId: '<%= ModuleContext.PortalId %>',
		groupId: '<%= GroupId %>',
		tabId: '<%= ModuleContext.TabId %>',
		baseUrl: '<%= ResolveUrl("~/DesktopModules/DNNCorp/SocialEvents/API/SocialEventService/") %>',
		servicesFramework: $.ServicesFramework(<%=ModuleId %>)
	}).init();
</script>