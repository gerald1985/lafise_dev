﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EventDetailView.ascx.cs" Inherits="DotNetNuke.Professional.SocialEvents.EventDetailView" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<%@ Register src="GuestList.ascx" tagPrefix="social" tagName="guestlist" %>
<%@ Register src="EditEvent.ascx" tagPrefix="dnn" tagName="EditEvent" %>
<div class="moduleContainer moduleShadowBox dnnClear">
    <div class="moduleTitleContainer">
        <div class="event-status-indicator activeIndicator"></div>      
        <div class="event-title-container">
           <h3 class="event-title"><asp:Literal runat="server" ID="litName"/></h3>
           <div class="dnnClear"></div>
           <p class="event-title-returnPrev"><asp:HyperLink runat="server" ID="hlReturn" >Return to previous page</asp:HyperLink></p>
        </div>       
        <ul class="event-social-actions">
            <li><a href="#" class="event-social-like"></a></li>
            <li><a href="#" class="event-social-bookmark on"></a></li>
            <li><a href="#" class="event-social-rss"></a></li>
        </ul>       
        <div class="dnnClear"></div>
    </div>
    <div class="moduleThreadContainer moduleTopInsetShadowBox">
        <div class="event-info-panel">
            <ul class="dnnLeft">
                <li><span>11/19/2011</span></li>
                <li><span>200 Views</span></li>
                <li><a href="#" class="event-like-button"></a><span>25</span></li>
            </ul>
            <ul class="dnnRight">
                <li><span>When: <asp:Label runat="server" ID="lblStartDate" DateFormat="MMMM dd, yyyy hh:mm tt" /></span></li>
                <li><span><asp:Literal ID="litZone" runat="server" /></span></li>
            </ul>
            <div class="dnnClear"></div>
        </div>       
        <div class="event-main-container">
            <div class="event-content-container">
                <div class="event-avator-container">
					<div class="event-avator"><asp:Image runat="server" ID="imgUser" /></div>  
				</div>
                <div class="event-content event-main-content">
                    <div class="dnnClear"><strong><%= Localization.GetString("where", LocalResourceFile)%></strong><asp:Label runat="server" ID="lblAddress" /></div>
                    <p><asp:Label runat="server" ID="lblDetails" /></p>
                    <div class="dnnClear"></div>
                    <asp:Panel runat="server" ID="phMap" class="seMap">
				        <div id="map_canvas"></div>
				        <script type="text/javascript">
				            jQuery(document).ready(function () {
				                var address = jQuery(".EventAddress").text();
				                jQuery("#map_canvas").goMap({
				                    zoom: 14,
				                    maptype: 'ROADMAP',
				                    scrollwheel: false,
				                    markers: [{ address: address, html: { content: address } }]
				                });
				                return false;
				            });
				        </script>
		            </asp:Panel>
                    <ul class="event-comments-actions">
						<li><a href="#" class="event-comments-reply dnnSocialLink"><img src='<%= ResolveUrl("images/reply.png") %>' />Comment</a></li>
						<li><a href="#" class="event-comments-like dnnSocialLink"><img src='<%= ResolveUrl("images/like.png") %>' />Like (2)</a></li>   
						<li><a href="#" class="event-comments-report dnnSocialLink"><img src='<%= ResolveUrl("images/report.png") %>' />Report</a></li>				
					</ul>
                    <ul class="event-comments-actions">
                        <li><asp:HyperLink runat="server" ID="hlEdit" CssClass="seEdit SocialEventEdit dnnLeft" /></li>
					    <li><asp:HyperLink runat="server" ID="hlDelete" CssClass="seDelete SocialEventDelete dnnLeft" NavigateUrl="javascript:void(0)" /></li>
                    </ul>
                    <div class="dnnClear"></div>
                    <asp:Panel runat="server" ID="phAttend" CssClass="dnnClear">
                        <ul class="event-comments-actions">
					        <li><asp:HyperLink runat="server" ID="cmdMayBe" StatusType="3" NavigateUrl="#" CssClass="altButton" resourcekey="Maybe" /></li>
					        <li><asp:HyperLink runat="server" ID="cmdNo" StatusType="2" NavigateUrl="#" CssClass="altButton" resourcekey="No" /></li>
					        <li><asp:HyperLink runat="server" ID="cmdYes" StatusType="1" NavigateUrl="#" CssClass="altButton" resourcekey="Yes" /></li>
                        </ul>
                        <div class="dnnRight"><social:guestlist ID="ctlGuestList" runat="server" /></div>
                    </asp:Panel>
                </div>
                <div class="dnnClear"></div>
            </div>          
        </div>
        <div class="event-comments-panel moduleTopInsetShadowBox">
			<div class="event-avator">
				<asp:Image ID="imgCurrUsr" runat="server" />
			</div>		 
			<a class="secondaryButton event-comment-button">Attending?</a>
			<span>Page 1 of 22</span>
			<ul class="event-pager-ul">
				<li><span>1</span></li>
				<li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><span>...</span></li>
				<li><a href="#">22</a></li>
			</ul>
			<div class="event-pager-rightside">
				<span>11 Results</span>
				<a href="#" class="event-pager-prev"></a>
				<a href="#" class="event-pager-next"></a>
			</div>
			<div class="dnnClear"></div>
		</div>
        <div class="dnnClear"></div>
    </div>
    </div>
<div>
	<dnn:EditEvent ID="ctlEdit" runat="server" />
</div>
<script type="text/javascript">
	new SocialEventGuestManager(jQuery, ko, {
		portalId: '<%= ModuleContext.PortalId %>',
		groupId: '<%= GroupId %>',
		tabId: '<%= ModuleContext.TabId %>',
		baseUrl: '<%= ResolveUrl("~/DesktopModules/DNNCorp/SocialEvents/API/SocialEventService/") %>',
		servicesFramework: $.ServicesFramework(<%=ModuleId %>),
		guestsDialogTitle: '<%= Localization.GetSafeJSString("GuestDialogTitle.Text", LocalResourceFile) %>',
		profilePage: '<%=DotNetNuke.Common.Globals.NavigateURL(PortalSettings.UserTabId, string.Empty, new String[] { "userId=xxx" })%>'
	}).init();
</script>
<script language="javascript" type="text/javascript">
	/*globals jQuery, window, Sys */
	(function ($, Sys) {
		function setupseEventDetailView() {
			var po = document.createElement('script');
			po.type = 'text/javascript';
			po.async = true;
			po.src = 'https://apis.google.com/js/plusone.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

			!function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (!d.getElementById(id)) {
					js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js";
					fjs.parentNode.insertBefore(js, fjs);
				}
			} (document, "script", "twitter-wjs");

			!function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				fjs.parentNode.insertBefore(js, fjs);
			} (document, 'script', 'facebook-jssdk');
		};

		$(document).ready(function () {
			//setupseEventDetailView();
			//Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
			//	setupseEventDetailView();
			//});
		});

	} (jQuery, window.Sys));
	</script> 