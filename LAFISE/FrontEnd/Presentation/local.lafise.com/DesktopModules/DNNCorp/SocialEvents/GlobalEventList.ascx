﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalEventList.ascx.cs" Inherits="DotNetNuke.Professional.SocialEvents.GlobalEventList" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<div class="dnnSEIndex dnnClear">
	<asp:Repeater runat="server" ID="rptEvents" OnItemDataBound="rptEvents_OnItemDataBound">
		<ItemTemplate>
			<div class="seEventItem dnnClear">
				<div class="seSidebar dnnLeft">
					<asp:HyperLink ID="hlAuthor" runat="server"><asp:Image runat="server" ID="imgUser" /></asp:HyperLink>
					<div><cite><%= Localization.GetString("Organizer", LocalResourceFile) %></cite></div>
				</div>
				<div class="seDetailContent dnnLeft">
					<h2><asp:HyperLink ID="hlName" runat="server" /></h2>  
					<div><strong><%= Localization.GetString("when", LocalResourceFile)%></strong><asp:Label runat="server" ID="lblStartDate" DateFormat="MMMM dd, yyyy hh:mm tt"/></div>
					<div><strong><%= Localization.GetString("where", LocalResourceFile)%></strong><asp:Label runat="server" ID="lblAddress" /></div>
				</div>
			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>
