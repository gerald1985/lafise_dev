﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GuestList.ascx.cs" Inherits="DotNetNuke.Professional.SocialEvents.GuestList" %>
<%@ Import Namespace="DotNetNuke.Services.Localization" %>
<ul class="plActions dnnClear">
	<li><asp:HyperLink runat="server" ID="lblGoing" StatusType="1" EventId="<%#EventId%>" /></li>
	<li><asp:HyperLink runat="server" ID="lblMayBe" StatusType="3" EventId="<%#EventId%>" /></li>
	<li><asp:HyperLink runat="server" ID="lblNotGoing" StatusType="2" EventId="<%#EventId%>" /></li>
</ul>
<script type="text/html" id="AttendeesTemplate">
	<div class="dnnClear seAttendeesItem" data-bind="css: {'seAttendeesAltItem': isAlternate}">
		<div class="dnnLeft">
            <a data-bind="attr: {href: profilePage}">
			    <img data-bind="attr: {src: '<%= ResolveUrl("~/") %>' + AvatarPath, alt: DisplayName}" src="<%= ResolveUrl("~/") %>images/spacer.gif" height="50" width="50" />
            </a>
		</div>
		<div class="seaiUserProfile">
			<div class="seAttendeeName" ><a data-bind="attr: {href: profilePage}"><span data-bind="text: DisplayName"></span></a></div>
			<div class="seAttendeeEmail"><strong><%= Localization.GetString("Email", LocalResourceFile) %></strong><a data-bind="attr: {href: 'mailto:' + Email}, text: Email"></a></div>
		</div>
	</div>
</script>
<div id="AttendingListContainer" class="seAttendees dnnClear" data-bind="template: { name: 'AttendeesTemplate', foreach: Attendies}"></div>