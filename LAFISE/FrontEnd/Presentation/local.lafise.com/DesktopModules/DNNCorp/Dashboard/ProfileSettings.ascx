﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileSettings.ascx.cs" Inherits="DotNetNuke.Professional.Dashboard.ProfileSettings" %>
<%@ Register tagPrefix="dashboard" tagName="GoalSettings" src="Controls/GoalSettings.ascx" %>

<div class="dnnClear dnnDashboardSettings">
    <h2 class="dnnFormSectionHead">
        <a class="dnnSectionExpanded">
            <%= LocalizeString("ParticipationHeader") %>
        </a>
    </h2>
    <fieldset class="dashboard-goal-config">
        <h5><%= LocalizeString("DecimalNote") %></h5>
        <br/>

        <div class="dashboard-participation">
            <h3><%= LocalizeString("Influence") %></h3>
            <h5><%= LocalizeString("InfluenceNote") %></h5>

            <div class="dashboard-influence">
                <dashboard:GoalSettings runat="server" ID="ctlInfluence" />
            </div>
            
            <br />

            <h3><%= LocalizeString("Engagement") %></h3>
            <h5><%= LocalizeString("EngagementNote") %></h5>

            <div class="dashboard-engagement">
                <dashboard:GoalSettings runat="server" ID="ctlEngagement" />
            </div>
        </div>
    </fieldset>
</div>