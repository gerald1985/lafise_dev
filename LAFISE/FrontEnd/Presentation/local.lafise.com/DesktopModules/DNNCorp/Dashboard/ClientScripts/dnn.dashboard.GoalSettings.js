﻿// DotNetNuke® - http://www.dnnsoftware.com
//
// Copyright (c) 2002-2015, DNN Corp.
// All rights reserved.

if (typeof dnn === 'undefined' || !dnn) dnn = {};
if (typeof dnn.dashboard === 'undefined') dnn.dashboard = {};

(function () {
    'use strict';

    dnn.dashboard.GoalSettings = function (settings) {
        var that = this;

        dnn.dashboard.GoalSettings.instances.push(this);

        var list = settings.list;

        this.social = new dnn.social.Module(settings);

        this.options = [];

        $.each(settings.availableOptions,
            function(key, value) {
                that.options.push({ value: key, text: value });
            });

        this.rows = ko.observableArray([]);

        this.add = function() {
            that.rows.push(that.generateRow(that.findUnusedGoal()));
        };

        this.findUnusedGoal = function () {
            var keys = Object.keys(settings.availableOptions);
            
            for (var i = 0; i < keys.length; ++i) {
                if (that.isUnused(keys[i])) {
                    return keys[i];
                }
            }

            return undefined;
        };

        this.isUnused = function (k) {
            for (var i = 0; i < that.rows().length; ++i) {
                if (that.rows()[i].goal() === k) {
                    return false;
                }
            }

            return true;
        };

        this.generateRow = function(goal, weight, value, minValue, maxValue) {
            var row = {
                edit: ko.observable(typeof value === 'undefined'),
                goal: ko.observable(goal || '')
            };


            row.weight = ko.observable(weight || 1);

            row.value = ko.observable(value || 1);
            row.minValue = ko.observable(minValue || 0);
            row.maxValue = ko.observable(maxValue || 1000);

            row.accept = function () {
                if (row.validate()) {
                    row.edit(false);
                }
            };

            row.change = function() {
                row.edit(true);
            };

            row.cancel = function () {
                row.weight(weight || 1);
                row.value(value || 1);
                row.minValue = minValue || 1;
                row.maxValue = maxValue || 1000;
                row.edit(false);
            };

            row.toggleEdit = function () {
                if (row.validate()) {
                    row.edit(!row.edit());
                }
            };

            row.remove = function() {
                var index = that.rows.indexOf(row);
                if (index >= 0) {
                    that.rows.splice(index, 1);
                }
            };

            row.validate = function() {
                var localizer = that.social.getLocalizationController();

                if (row.value() < row.minValue() || row.value() > row.maxValue() ||
                    row.weight() < row.minValue() || row.weight() > row.maxValue()) {
                    $.dnnAlert({
                        title: localizer.getString('InvalidValueTitle'),
                        text: localizer.getString('InvalidValueText')
                    });

                    return false;
                }

                var duplicate = false;

                $.each(that.rows(),
                    function(rowIndex, rowValue) {
                        if (rowValue != row && rowValue.goal() == row.goal()) {
                            duplicate = true;
                            return false;
                        }
                        return true;
                    });

                if (duplicate) {
                    $.dnnAlert({
                        title: localizer.getString('DuplicateGoalTitle'),
                        text: localizer.getString('DuplicateGoalText')
                    });

                    return false;
                }

                return true;
            };

            return row;
        };
        
        this.getOptionText = function(value) {
            if (typeof value !== 'function') {
                return 'Unknown';
            }

            return settings.availableOptions[value()] || 'Unknown';
        };

        this.insertSetting = function (setting) {
            var row = that.generateRow(setting.GoalName, setting.Weight, setting.Value, setting.MinValue, setting.MaxValue);
            if (row != null) {
                that.rows.push(row);
            }
        };

        this.getUpdateButton = function() {
            return $('a').filter(
                function() {
                    return this.id.match(/ModuleSettings_cmdUpdate$/);
                });
        };

        this.validate = function() {
            var validated = true;

            $.each(that.rows(),
                function(rowIndex, rowValue) {
                    if (!rowValue.validate()) {
                        validated = false;
                        return false;
                    }

                    return true;
                });

            return validated;
        };

        this.validateAndSave = function () {
            var all = true;

            $.each(dnn.dashboard.GoalSettings.instances,
                function(index, instance) {

                    if (!instance.validate()) {
                        all = false;
                        return false;
                    }

                    $.each(instance.rows(),
                        function(rowIndex, rowValue) {
                            rowValue.accept();
                        });

                    if (!instance.save()) {
                        all = false;
                        return false;
                    }

                    return true;
                });

            return all;
        };

        this.save = function () {
            var successful = false;

            var service = that.social.getService('Settings');

            var success = function() {
                successful = true;
            };

            var failure = function(xhr, status) {
                var localizer = that.social.getLocalizationController();

                $.dnnAlert({
                    title: localizer.getString('GoalSaveFailureTitle'),
                    text: localizer.getString('GoalSaveFailureText').format(list, status || 'Unknown error')
                });
            };

            var params = {
                target: list,
                list: that.rows()
            };

            service.postsync('SaveGoals', params, success, failure);

            return successful;
        };

        this.bindControls = function () {
            var button = that.getUpdateButton();
            if (button != null) {
                var location = button.attr('href');
                if (location === 'javascript:void(0)') {
                    return;
                }
                
                button.click(
                    function (e) {
                        if (that.validateAndSave()) {
                            window.location = location;
                        } else {
                            e.preventDefault();
                        }

                        return false;
                    });
                
                button.attr('href', 'javascript:void(0)');
            }
        };

        this.binded = function() {
            this.bindControls();
            $.each(settings.currentSettings,
               function (index, value) {
                   that.insertSetting(value);
               });
            };
        };

    dnn.dashboard.GoalSettings.instances = [];
    
})();