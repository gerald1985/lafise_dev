﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SolicitudSeguroVidaAccidentes.ascx.cs" Inherits="SolicitudSeguroVidaAccidentes" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" data-ng-app="SeguroVidaAccidentesModule">
    <div data-ng-controller="SeguroVidaAccidentesController">
        <div class="lafise-group">
            <ng-form name="addSolicitud">
            <div class="lafise-container-Datos-Personales">
                <div class="lafise-group">
                    <div class="lafise-container-etiqueta">
                        <output class="lafise-label">Datos personales</output>
                    </div>
                </div>
                <!-- Nombres  -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Nombres</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="nombre"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.Nombre"
                            maxlength="100"
                            required />
                    </div>
                </div>
                <!-- Apellidos-->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Apellidos</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="apellidos"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.Apellidos"
                            maxlength="100"
                            required />
                    </div>
                </div>
                <!-- Teléfono -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Tel&eacute;fono</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="telefono"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.Telefono"
                            maxlength="50"
                            required />
                    </div>
                </div>
                <!-- Email -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Email</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="email"
                            name="correo"
                            id="correo"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.Email"
                            data-ng-blur="FormatoEmail();"
                            maxlength="200"
                            required />
                    </div>
                </div>
                <div id="alertaemail" data-ng-show="showMsgEmail" class="'alert alert-danger alert-dismissible'" role="alert">
                    <button
                        type="button"
                        data-ng-click="hideValidacion()"
                        class="close"
                        data-dismiss="alert"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span ng-bind="msgValidarCorreo">
                    </span>
                </div>
                <!-- Genero -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Sexo</output>
                    </div>
                    <div class="lafise-container-text">
                        <select
                            id="genero"
                            name="genero"
                            class="lafise-select"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.Masculino"
                            required>
                            <option value="">Seleccione</option>
                            <option value="false">Femenino</option>
                            <option value="true">Masculino</option>
                        </select>
                    </div>
                </div>
                <!--Nacionalidad-->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Nacionalidad</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="nacionalidad"
                            id="nacionalidad"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.Nacionalidad"
                            maxlength="80"
                            required />
                    </div>
                </div>
                <!--Ciudad de residencia-->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Ciudad de residencia</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="ciudadResidencia"
                            id="ciudadResidencia"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.CiudadResidencia"
                            maxlength="100"
                            required />
                    </div>
                </div>
                <!-- Seleccionar TipoIdentificacion -->
                <div class="lafise-group">
                    <div data-ng-controller="TipoIdentificacionController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Tipo identificaci&oacute;n</output>
                        </div>
                        <div class="lafise-container-select">
                            <select
                                class="lafise-select"
                                data-ng-class="{submitted:addSolicitud.submitted}"
                                data-ng-model="newDatosPersonales.IdTipoIdentificacion"
                                data-ng-options="tipoIdentificacion.Id as tipoIdentificacion.Nombre for tipoIdentificacion in tipoIdentificaciones"
                                required>
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--Número de identificación-->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">N&uacute;mero de identificaci&oacute;n</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="numeroIdentificacion"
                            id="numeroIdentificacion"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.NumeroIdentificacion"
                            maxlength="50"
                            required />
                    </div>
                </div>
                <!--Profesión-->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Profesi&oacute;n</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="profesion"
                            id="profesion"
                            class="lafise-text"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            data-ng-model="newDatosPersonales.Profesion"
                            maxlength="200"
                            required />
                    </div>
                </div>
            </div>
            <div class="lafise-container-datos-financiero-laborales">
                <div class="lafise-container-etiqueta">
                    <output class="lafise-title">Datos financieros y laborales</output>
                </div>
                <div class="lafise-group contentCheck">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Es cliente LAFISE</output>
                    </div>
                    <div class="lafise-container-check">
                        <input type="checkbox" name="EsCliente" data-ng-model="newDatosFinancieros.EsCliente" />
                    </div>
                </div>
                <div class="lafise-group contentCheck">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Posee n&oacute;mina LAFISE</output>
                    </div>
                    <div class="lafise-container-check">
                        <input type="checkbox" name="poseeNomina" data-ng-model="newDatosFinancieros.PoseeNomina" data-ng-change="PoseeNominaChanged(newDatosFinancieros.PoseeNomina)" />
                    </div>
                </div>
                <div class="lafise-container-posee-nomina" data-ng-show="newDatosFinancieros.ContainerPoseeNomina">
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Promedio de ingresos mensuales (Moneda): </output>
                        </div>
                        <div class="lafise-container-select">
                            <select
                                id="lafise-prom-ingresos"
                                data-ng-class="{submitted:addSolicitud.submitted}"
                                name="promingresos"
                                class="lafise-select"
                                data-ng-model="newDatosFinancieros.PromedioIngresoSelected"
                                data-ng-options="rangosueldo.RangoSueldo for rangosueldo in rangoSueldoPais"
                                 data-ng-change="PromIngresosChanged(newDatosFinancieros.PromedioIngresoSelected)"
                                data-ng-required="newDatosFinancieros.ContainerPoseeNomina">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="lafise-group contentCheck">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Tiene negocio propio</output>
                    </div>
                    <div class="lafise-container-check">
                        <input type="checkbox" name="negociopropio" data-ng-model="newDatosFinancieros.NegocioPropio" />
                    </div>
                </div>
                <div data-ng-show="newDatosFinancieros.NegocioPropio">
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Adjuntar soporte de negocio propio</output>
                        </div>
                        <div class="lafise-container-file">
                            <button
                                id="file"
                                type="file"
                                ngf-select="uploadFiles($files, $invalidFiles,'NP')"
                                accept="application/pdf"
                                ngf-validate="{size: {min: 0, max: '5MB'}, pattern: '.pdf'}"
                                ngf-max-size="5 MB"
                                ngf-multiple="true"
                                data-ng-required="newDatosFinancieros.NegocioPropio"
                                data-ng-class="{submitted:addSolicitud.submitted}"
                                name="archivoNP"
                                ng-model="newDatosFinancieros.ArchivosNP">
                                Examinar
                            </button>
                        </div>
                        <div class="lafise-lista-Archivos" data-ng-repeat="item in ListaArchivosNP">
                            <div class="lafise-nombre-archivo" data-ng-hide="item.showAlert">
                                <output name="fileInvalid{{$index}}" data-ng-model="item.Nombre">{{item.Nombre}}</output>
                                <span class="lafise-eliminar-archivo" data-ng-click="deleteFile($index,'NP',false)">X</span>
                            </div>
                            <div id="alertArchivo{{$index}}NP" data-ng-show="item.showAlert" class="alert alert-danger alert-dismissible" role="alert">
                                <button
                                    type="button"
                                    data-ng-click="deleteFile($index,'NP',true)"
                                    class="close"
                                    data-dismiss="alert"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="lafise-Archivo-Invalido">{{item.Nombre}}</div>
                                <span class="lafise-Mensaje-Validacion">{{item.Error}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lafise-group contentCheck">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Es asalariado</output>
                    </div>
                    <div class="lafise-container-check">
                        <input type="checkbox" name="asalariado" data-ng-model="newDatosFinancieros.Asalariado" />
                    </div>
                </div>
                <div data-ng-show="newDatosFinancieros.Asalariado">
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Adjuntar soporte de ingresos</output>
                        </div>
                        <div class="lafise-container-file">
                            <button
                                id="file"
                                type="file"
                                ngf-select="uploadFiles($files, $invalidFiles,'SI')"
                                accept="application/pdf"
                                ngf-validate="{size: {min: 0, max: '5MB'}, pattern: '.pdf'}"
                                ngf-max-size="5 MB"
                                ngf-multiple="true"
                                ng-required="newDatosFinancieros.Asalariado"
                                data-ng-class="{submitted:addSolicitud.submitted}"
                                name="archivoSI"
                                ng-model="newDatosFinancieros.ArchivosSI">
                                Examinar
                            </button>
                        </div>
                        <div class="lafise-lista-Archivos" data-ng-repeat="item in ListaArchivosSI">
                            <div class="lafise-nombre-archivo" data-ng-hide="item.showAlert">
                                <output name="fileInvalid{{$index}}" data-ng-model="item.Nombre">{{item.Nombre}}</output>
                                <span class="lafise-eliminar-archivo" data-ng-click="deleteFile($index,'SI',false)">X</span>
                            </div>
                            <div id="alertArchivo{{$index}}SI" data-ng-show="item.showAlert" class="alert alert-danger alert-dismissible" role="alert">
                                <button
                                    type="button"
                                    data-ng-click="deleteFile($index,'SI',true)"
                                    class="close"
                                    data-dismiss="alert"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="lafise-Archivo-Invalido">{{item.Nombre}}</div>
                                <span class="lafise-Mensaje-Validacion">{{item.Error}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Actividad econ&oacute;mica</output>
                    </div>
                    <div class="lafise-container-CheckBox">
                        <div data-ng-controller="TipoActividadController">
                            <div class="{{!someSelected(TipoActividad) ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addSolicitud.submitted}">
                                <div data-ng-repeat="tipo in tiposActividad">
                                    <input type="checkbox"
                                        name="tipoActividad"
                                        checklist-model="TipoActividad"
                                        checklist-value="tipo"
                                        data-ng-click="SeleccionarActividad($event,tipo.Id,tipo.Nombre,$index)"
                                        data-ng-model="tipo.seleccionado"
                                        >
                                    {{tipo.Nombre}}
                                  <div class="lafise-container-text" data-ng-show="tipo.mostrarOtro">
                                      <input type="text"
                                          name="otraActividad"
                                          id="otraActividad{{$index}}"
                                          class="lafise-text"
                                          data-ng-class="{submitted:addSolicitud.submitted}"
                                          data-ng-model="tipo.OtraActividad"
                                          data-ng-required="tipo.mostrarOtro"
                                          maxlength="200" />
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lafise-group contentCheck">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Es zurdo</output>
                    </div>
                    <div class="lafise-container-CheckBox">
                        <input
                            type="checkbox"
                            name="esZurdo"
                            data-ng-model="newDatosFinancieros.EsZurdo" />
                    </div>
                </div>
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Estatura</output>
                    </div>
                    <div class="lafise-container-Text">
                        <input
                            type="text"
                            name="Estatura"
                            data-ng-model="newDatosFinancieros.Estatura"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            required />
                    </div>
                </div>
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Peso</output>
                    </div>
                    <div class="lafise-container-Text">
                        <input
                            type="text"
                            name="Peso"
                            data-ng-model="newDatosFinancieros.Peso"
                            data-ng-class="{submitted:addSolicitud.submitted}"
                            required />
                    </div>
                </div>
            </div>
            <div class="signupform" data-ng-controller="CaptchaController as captcha">
                <div class="{{!addSolicitud.$valid ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addSolicitud.submitted}">
                    <div vc-recaptcha key="captcha.publicKey"></div>
                </div>
                <!-- Botones Enviar y Cancelar -->
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Enviar" data-ng-click="submitted=true;add(captcha.signup())" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" />
                    </div>
                </div>
            </div>
        </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>
<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ng-file-upload-all.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/checklist-model.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="/Scripts/angular.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="/Scripts/jquery.signalR-2.1.2.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="Scripts/angular-signalr-hub.min.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="/signalr/hubs" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/FrontEnd/SeguroVidaAccidentes/SeguroVidaAccidentesModule.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/FrontEnd/SeguroVidaAccidentes/SeguroVidaAccidentesFactory.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/FrontEnd/SeguroVidaAccidentes/SeguroVidaAccidentesController.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionModule.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionController.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionFactory.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Comunes/TipoActividad/TipoActividadModule.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/Comunes/TipoActividad/TipoActividadController.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="~/App/Comunes/TipoActividad/TipoActividadFactory.js" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/Base/js/angular-recaptcha.min.js" Priority="18" />
<dnn:DnnJsInclude ID="DnnJsInclude19" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaModule.js" Priority="19" />
<dnn:DnnJsInclude ID="DnnJsInclude20" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaController.js" Priority="20" />
<dnn:DnnJsInclude ID="DnnJsInclude21" runat="server" FilePath="//www.google.com/recaptcha/api.js?hl=es&onload=vcRecaptchaApiLoaded&render=explicit" Priority="21" />
<dnn:DnnJsInclude ID="DnnJsInclude22" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="22" />
