﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TasaCambio.ascx.cs" Inherits="TasaCambio" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" data-ng-app="TasaCambioModule">
    <div data-ng-controller="TasaCambioController">
        <div class="lafise-group">
            <div class="lista" data-ng-repeat="item in ListaTasa">
                <div class="lafise-TasaCambio">
                    <div class="lafise-descripcion"  data-ng-bind="item.Descripcion"></div>
                    <div class="lafise-valorCompra" data-ng-bind="item.ValorCompra"></div>
                    <div class="lafise-valorVenta" data-ng-bind="item.ValorVenta"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/FrontEnd/TasaCambio/TasaCambioModule.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/FrontEnd/TasaCambio/TasaCambioFactory.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/FrontEnd/TasaCambio/TasaCambioController.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="6" />

