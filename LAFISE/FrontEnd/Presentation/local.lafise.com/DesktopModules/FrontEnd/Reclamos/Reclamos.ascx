﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Reclamos.ascx.cs" Inherits="Reclamos" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link href="../../../App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" data-ng-app="ReclamosModule">
    <div data-ng-controller="ReclamosController">
        <div class="lafise-group">
            <ng-form name="addReclamos" ng-submit="add()" class="ng-dirty">
                <!-- Crear formulario de reclamos -->
                <div class="lafise-container-datos-personales">
                    <!-- Etiqueta datos personales -->
                    <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Datos personales</output>
                        </div>
                    </div>
                    <!-- Nombres  -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nombres</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="nombre"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.Nombre"
                                maxlength="100"
                                required />
                        </div>
                    </div>
                    <!-- Apellidos-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Apellidos</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="apellidos"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.Apellidos"
                                maxlength="100"
                                required />
                        </div>
                    </div>
                    <!-- Teléfono -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Teléfono</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="telefono"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.Telefono"
                                maxlength="50"
                                required />
                        </div>
                    </div>
                    <!-- Email -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Email</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="email"
                                name="correo"
                                id="correo"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.Email"
                                data-ng-blur="FormatoEmail();"
                                maxlength="50"
                                required />
                        </div>
                    </div>
                    <!-- Alerta - Email inválido -->
                    <div id="validacionEmail" data-ng-show="showMsgEmail" ng-class="'alert alert-danger alert-dismissible'" role="alert">
                        <button
                            type="button"
                            data-ng-click="hideValidacion()"
                            class="close"
                            data-dismiss="alert"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span  ng-bind="msgValidarCorreo">
                        </span>
                    </div>
                    <!-- Genero -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Sexo</output>
                        </div>
                        <div class="lafise-container-text">
                            <select
                                id="genero"
                                name="genero"
                                class="lafise-select"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.Masculino"
                                required>
                                <option value="">Seleccione</option>
                                <option value="false">Femenino</option>
                                <option value="true">Masculino</option>
                            </select>
                        </div>
                    </div>
                    <!--Nacionalidad-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nacionalidad</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="nacionalidad"
                                id="nacionalidad"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.Nacionalidad"
                                maxlength="80"
                                required />
                        </div>
                    </div>
                    <!-- Seleccionar TipoIdentificacion -->
                    <div class="lafise-group">
                        <div data-ng-controller="TipoIdentificacionController">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Tipo Identificación</output>
                            </div>
                            <div class="lafise-container-select">
                                <select
                                    id="tipoIdentificacion"
                                    name="pais"
                                    class="lafise-select"
                                    data-ng-class="{submitted:addReclamos.submitted}"
                                    data-ng-model="newDatosPersonales.IdTipoIdentificacion"
                                    data-ng-options="tipoIdentificacion.Id as tipoIdentificacion.Nombre for tipoIdentificacion in tipoIdentificaciones"
                                    required>
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--Ciudad de residencia-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Ciudad de residencia</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="ciudadResidencia"
                                id="ciudadResidencia"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.CiudadResidencia"
                                maxlength="100"
                                required />
                        </div>
                    </div>
                    <!--Número de identificación-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Número de identificación</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="numeroIdentificacion"
                                id="numeroIdentificacion"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.NumeroIdentificacion"
                                maxlength="50"
                                required />
                        </div>
                    </div>
                    <!--Profesión-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Profesión</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="profesion"
                                id="profesion"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newDatosPersonales.Profesion"
                                maxlength="200"
                                required />
                        </div>
                    </div>
                </div>
                <div class="lafise-container-siniestro">
                    <!-- Etiqueta Registro siniestro -->
                    <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Registro siniestro</output>
                        </div>
                    </div>
                    <!--Fecha del siniestro-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Fecha del siniestro</output>
                        </div>
                        <div class="lafise-container-text">
                            <p class="input-group">
                                <input type="text"
                                    class="form-control"
                                    uib-datepicker-popup="{{'dd/MM/yyyy'}}"
                                    ng-model="newSiniestros.Fecha"
                                    is-open="status.opened"
                                    ng-required="true"
                                    close-text="Cerrar"
                                    clear-text="Limpiar"
                                    current-text="Hoy"
                                    max-date="maxDate"
                                    disabled="disabled"
                                    data-ng-class="{submitted:addReclamos.submitted}"
                                    required />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                                </span>
                            </p>
                        </div>
                    </div>
                    <!--Número de póliza-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Número de póliza</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                name="numeroPoliza"
                                id="numeroPoliza"
                                class="lafise-text"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newSiniestros.NumeroPoliza"
                                maxlength="200"
                                required />
                        </div>
                    </div>
                    <!-- Seleccionar Pais -->
                    <div class="lafise-group">
                        <div data-ng-controller="PaisController">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Pa&iacute;s</output>
                            </div>
                            <div class="lafise-container-select">
                                <div class="{{paisesSelected.Paises.length == 0 ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addReclamos.submitted}">
                                    <ui-select multiple data-ng-model="paisesSelected.Paises" data-ng-required="!paisesSelected.Paises.length > 0" theme="select2" style="width: 300px;" data-ng-change="mostrarCiudad()">
                                        <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                                        <ui-select-choices repeat="pais.Id as pais in paises | filter:$select.search" >
                                        {{pais.Nombre}}
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Seleccionar Ciudad -->
                    <div class="lafise-group">
                        <div data-ng-controller="CiudadController">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Ciudad</output>
                            </div>
                            <div class="{{ciudadSelected.Ciudad.length == 0 ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addReclamos.submitted}">
                                <ui-select multiple data-ng-model="ciudadSelected.Ciudad" data-ng-required="!ciudadSelected.Ciudad.length > 0" theme="select2" style="width: 300px;">
                                <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                                <ui-select-choices repeat="ciudad.Id as ciudad in ciudadPais | filter:$select.search">
                                {{ciudad.Nombre}}
                             </ui-select-choices>
                            </div>
                        </div>
                    </div>
                    <!--Detalles y hechos del siniestro-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Detalles y hechos del siniestro</output>
                        </div>
                        <div class="{{newSiniestros.Detalle =='' ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addReclamos.submitted}">
                            <textarea
                                name="detalleSiniestro"
                                id="detalleSiniestro"
                                class="lafise-textarea"
                                cols="40"
                                rows="5"
                                data-ng-class="{submitted:addReclamos.submitted}"
                                data-ng-model="newSiniestros.Detalle"
                                required>
                         </textarea>
                        </div>
                    </div>
                </div>
                <!-- Botones Actualizar, Guardar y Cancelar -->
                <div class="signupform" data-ng-controller="CaptchaController as captcha">
                    <div class="{{!addSolicitud.$valid ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addReclamos.submitted}">
                        <div vc-recaptcha key="captcha.publicKey"></div>
                    </div>
                    <!-- Botones Actualizar, Guardar y Cancelar -->
                    <div class="lafise-container-options">
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Enviar" data-ng-click="submitted=true;add(captcha.signup())" />
                        </div>
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" />
                        </div>
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/angular-locale_es-es.js" Priority="2"/>
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="3"  />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/angular-sanitize.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/select.min.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="/Scripts/angular.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="/Scripts/jquery.signalR-2.1.2.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="Scripts/angular-signalr-hub.min.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="/signalr/hubs" Priority="9" />

<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/Comunes/Pais/PaisModule.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Comunes/Pais/PaisFactory.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/Pais/PaisController.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionModule.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionFactory.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionController.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/Comunes/Ciudad/CiudadModule.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="~/App/Comunes/Ciudad/CiudadFactory.js" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/Comunes/Ciudad/CiudadController.js" Priority="18" />
<dnn:DnnJsInclude ID="DnnJsInclude19" runat="server" FilePath="~/App/FrontEnd/Reclamos/ReclamosModule.js" Priority="19" />
<dnn:DnnJsInclude ID="DnnJsInclude20" runat="server" FilePath="~/App/FrontEnd/Reclamos/ReclamosFactory.js" Priority="20" />
<dnn:DnnJsInclude ID="DnnJsInclude21" runat="server" FilePath="~/App/FrontEnd/Reclamos/ReclamosController.js" Priority="21" />
<dnn:DnnJsInclude ID="DnnJsInclude22" runat="server" FilePath="~/App/Base/js/angular-recaptcha.min.js" Priority="22" />
<dnn:DnnJsInclude ID="DnnJsInclude23" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaModule.js" Priority="23" />
<dnn:DnnJsInclude ID="DnnJsInclude24" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaController.js" Priority="24" />
<dnn:DnnJsInclude ID="DnnJsInclude25" runat="server" FilePath="//www.google.com/recaptcha/api.js?hl=es&onload=vcRecaptchaApiLoaded&render=explicit" Priority="25" />
<dnn:DnnJsInclude ID="DnnJsInclude26" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="26" />
