﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SuscripcionBoletin.ascx.cs" Inherits="SuscripcionBoletin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" id="suscripcionBoletinModule">
    <div data-ng-controller="SuscripcionBoletinController">
        <div class="lafise-group">
            <ng-form name="addSuscripcion" ng-submit="add()">
                <!-- Crear suscripción a boletin -->
                <div class="lafise-container-suscripcion-boletin">
                    <!-- Etiqueta Suscripción a boletines -->
                    <div class="lafise-group">
                        <!-- Nombre  -->
                        <div class="lafise-group">
                            <div class="lafise-container-text">
                                <input type="text"
                                    name="nombre"
                                    placeholder="NOMBRES"
                                    class="lafise-text"
                                    data-ng-class="{submitted:addSuscripcion.submitted}"
                                    data-ng-model="newSuscripcion.Nombre"
                                    maxlength="100"
                                    required />
                            </div>
                        </div>
                        <!-- Email -->
                        <div class="lafise-group">
                            <div class="lafise-container-text">
                                <input type="email"
                                    name="correo"
                                    placeholder="CORREO ELECTRÓNICO"
                                    id="correo"
                                    class="lafise-text"
                                    data-ng-class="{submitted:addSuscripcion.submitted}"
                                    data-ng-model="newSuscripcion.Email"
                                    data-ng-blur="FormatoEmail()"
                                    data-ng-focus="closeAlert($index)"
                                    maxlength="200"
                                    required />
                            </div>
                        </div>
                        <!-- Alerta - Email inválido -->
                        <div id="validacionEmail" data-ng-show="showMsgEmail" data-ng-class="'alert alert-danger alert-dismissible'" role="alert">
                            <button
                                type="button"
                                data-ng-click="hideValidacion()"
                                class="close"
                                data-dismiss="alert"
                                aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <span data-ng-bind="msgValidarCorreo"></span>
                        </div>
                        <!--Acepta ley de tratamiento y uso de datos LAFISE-->
                        <div class="lafise-group contentCheck">
                            <div class="lafise-container-check">
                                <input
                                    type="checkbox"
                                    name="aceptoLey"
                                    data-ng-model="newSuscripcion.AceptoLey" > Acepto la ley de tratamiento y uso de datos de LAFISE
                            </div>
                        </div>
                    </div>
                    <!-- Boton Suscribirme -->
                    <div class="lafise-container-options">
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Suscribirme" data-ng-click="submitted=true;add()" />
                        </div>
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg" dismiss-on-timeout="5000"></uib-alert>
    </div>
</div>

<dnn:dnnjsinclude id="DnnJsInclude1" runat="server" filepath="~/App/Base/js/angular.min.js" priority="1" />
<dnn:dnnjsinclude id="DnnJsInclude3" runat="server" filepath="~/App/Base/js/ui-bootstrap-tpls.min.js" priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude20" runat="server" FilePath="~/App/FrontEnd/SuscripcionBoletin/SuscripcionBoletinModule.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude21" runat="server" FilePath="~/App/FrontEnd/SuscripcionBoletin/SuscripcionBoletinFactory.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude22" runat="server" FilePath="~/App/FrontEnd/SuscripcionBoletin/SuscripcionBoletinController.js" Priority="5" />
<dnn:dnnjsinclude id="DnnJsInclude19" runat="server" filepath="~/App/Base/js/VentanaCargando.js" priority="6" />

