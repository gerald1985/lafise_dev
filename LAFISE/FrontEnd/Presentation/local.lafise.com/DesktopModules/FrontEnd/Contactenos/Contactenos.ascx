﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Contactenos.ascx.cs" Inherits="Contactenos" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link href="../../../App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" data-ng-app="ContactenosModule">
    <div data-ng-controller="ContactenosController">
        <div class="lafise-group">
            <ng-form name="addContactenos" ng-submit="add()" class="ng-dirty">
                <!-- Crear Contactenos -->
                <!-- Nombres y apellidos -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Nombres y Apellidos</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="nombreCompleto"
                            class="lafise-text"
                            data-ng-class="{submitted:addContactenos.submitted}"
                            data-ng-model="newContactenos.NombreCompleto"
                            maxlength="200"
                            required />
                    </div>
                </div>
                <!-- Es cliente Lafise -->
                <div class="lafise-group contentCheck">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Es cliente LAFISE</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="checkbox" name="esClienteLafise" data-ng-model="newContactenos.EsCliente" data-ng-checked="isChecked(newContactenos.EsCliente)" />
                    </div>
                </div>
                <!-- Teléfono -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Teléfono</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="telefono"
                            class="lafise-text"
                            data-ng-class="{submitted:addContactenos.submitted}"
                            data-ng-model="newContactenos.Telefono"
                            maxlength="50"
                            required />
                    </div>
                </div>
                <!-- Email -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Email</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="email"
                            name="correo"
                            id="correo"
                            class="lafise-text"
                            data-ng-class="{submitted:addContactenos.submitted}"
                            data-ng-model="newContactenos.Email"
                            data-ng-blur="FormatoEmail();"
                            maxlength="50"
                            required />
                    </div>
                </div>
                <!-- Alerta - Email inválido -->
                <div id="validacionEmail" data-ng-show="showMsgEmail" ng-class="'alert alert-danger alert-dismissible'" role="alert">
                    <button
                        type="button"
                        data-ng-click="hideValidacion()"
                        class="close"
                        data-dismiss="alert"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>                   
                        <span ng-bind="msgValidarCorreo"></span>
                </div>
                

                <!-- Asunto -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Asunto</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="asunto"
                            class="lafise-text"
                            data-ng-class="{submitted:addContactenos.submitted}"
                            data-ng-model="newContactenos.Asunto"
                            required />
                    </div>
                </div>
                <!-- Mensaje -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Mensaje</output>
                    </div>
                    <div class="{{newContactenos.Mensaje =='' ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addContactenos.submitted}">
                        <textarea
                            name="mensaje"
                            id="mensaje"
                            class="lafise-textarea"
                            cols="40"
                            rows="5"
                            data-ng-class="{submitted:addContactenos.submitted}"
                            data-ng-model="newContactenos.Mensaje"
                            required>
                         </textarea>
                    </div>
                </div>
                <!--Captcha-->
                <div class="signupform" data-ng-controller="CaptchaController as captcha">
                    <div class="{{!addSolicitud.$valid ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addContactenos.submitted}">
                        <div vc-recaptcha key="captcha.publicKey"></div>
                    </div>
                    <div class="lafise-leyenda" data-ng-show="verLeyenda"> 
                        <div class="lafise-container-check">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Acepto</output>
                            </div>
                            <div class="{{!someSelected(AutorizoLeyenda.Autorizo) ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addContactenos.submitted}">
                                <input 
                                    type="checkbox" 
                                    name="autorizoLeyenda" 
                                    data-ng-model="AutorizoLeyenda.Autorizo"
                                    data-ng-required="!someSelected(AutorizoLeyenda.Autorizo) && verLeyenda" />
                            </div>
                        </div>
                        <div data-ng-include="'/App/Base/html/LeyendaAutorizacionPanama.html'"></div>
                    </div>
                    <!-- Botones Enviar y Cancelar -->
                    <div class="lafise-container-options">
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Enviar" data-ng-click="submitted=true;add(captcha.signup())" />
                        </div>
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" />
                        </div>
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->

        <div ng-show="showMsg">
            <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
        </div>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/FrontEnd/Contactenos/ContactenosModule.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/FrontEnd/Contactenos/ContactenosFactory.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/FrontEnd/Contactenos/ContactenosController.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/Base/js/angular-recaptcha.min.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaModule.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaController.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="//www.google.com/recaptcha/api.js?hl=es&onload=vcRecaptchaApiLoaded&render=explicit" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude19" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="10" />
