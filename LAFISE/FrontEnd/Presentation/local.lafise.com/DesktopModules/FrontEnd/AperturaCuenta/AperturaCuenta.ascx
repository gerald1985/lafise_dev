﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AperturaCuenta.ascx.cs" Inherits="AperturaCuenta" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />


<div class="lafise-container" data-ng-app="AperturaCuentaModule">
    <div data-ng-controller="AperturaCuentaController">
        <div class="lafise-group">
            <ng-form name="addSolicitud" ng-submit="add()">
                <!-- Crear formulario de prestamo educativo -->
                <div class="lafise-container-datos-personales">
                     <!-- Etiqueta datos personales -->
                     <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Datos personales</output>
                        </div>
                    </div>
                     <!-- Nombres  -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nombres</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="nombre"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Nombre"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                     <!-- Apellidos-->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Apellidos</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="apellidos"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Apellidos"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                     <!-- Teléfono -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Tel&eacute;fono</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="telefono"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Telefono" 
                                   maxlength="50"
                                   required />
                        </div>
                    </div>
                     <!-- Email -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Email</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="email"
                                   name="correo"
                                   id="correo"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Email"
                                   data-ng-blur="FormatoEmail();" 
                                   maxlength="200" 
                                   required />
                        </div>
                    </div>
                    <!-- Alerta - Email inválido -->
                    <div id="validacionEmail" data-ng-show="showMsgEmail" ng-class="'alert alert-danger alert-dismissible'" role="alert">
                        <button
                            type="button"
                            data-ng-click="hideValidacion()"
                            class="close"
                            data-dismiss="alert"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span ng-bind="msgValidarCorreo">
                        </span>
                    </div>
                     <!-- Genero -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Sexo</output>
                        </div>
                        <div class="lafise-container-text">
                             <select
                                    id="genero"
                                    name="genero"
                                    class="lafise-select" 
                                    data-ng-class="{submitted:addSolicitud.submitted}" 
                                    data-ng-model="newDatosPersonales.Masculino" 
                                    required>
                                    <option value="">Seleccione</option>
                                    <option value="false">Femenino</option>
                                    <option value="true">Masculino</option>
                                </select>
                        </div>
                    </div>
                     <!--Nacionalidad-->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nacionalidad</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="nacionalidad"
                                   id="nacionalidad"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Nacionalidad"
                                   maxlength="80" 
                                   required />
                        </div>
                    </div>
                     <!--Ciudad de residencia-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Ciudad de residencia</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="ciudadResidencia"
                                   id="ciudadResidencia"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.CiudadResidencia"
                                   maxlength="100" 
                                   required />
                        </div>
                    </div>
                     <!-- Seleccionar TipoIdentificacion -->
                     <div class="lafise-group">
                        <div data-ng-controller="TipoIdentificacionController">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Tipo Identificaci&oacute;n</output>
                            </div>
                            <div class="lafise-container-select">                      
                                <select
                                    class="lafise-select" 
                                    data-ng-class="{submitted:addSolicitud.submitted}" 
                                    data-ng-model="newDatosPersonales.IdTipoIdentificacion" 
                                    data-ng-options="tipoIdentificacion.Id as tipoIdentificacion.Nombre for tipoIdentificacion in tipoIdentificaciones"
                                    required>
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                     <!--Número de identificación-->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">N&uacute;mero de identificaci&oacute;n</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="numeroIdentificacion"
                                   id="numeroIdentificacion"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.NumeroIdentificacion"
                                   maxlength="50" 
                                   required />
                        </div>
                    </div>
                     <!--Profesión-->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Profesi&oacute;n</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="profesion"
                                   id="profesion"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Profesion"
                                   maxlength="200" 
                                   required />
                        </div>
                    </div>
                </div>
                <div class="lafise-container-datos-financieros-laborales">
                    <!-- Etiqueta Datos financieros y laborales -->
                    <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Datos financieros y laborales</output>
                        </div>
                    </div>
                    <!--Es cliente LAFISE-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Es cliente LAFISE</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="EsCliente" 
                                data-ng-model="newDatosFinancieros.EsCliente" />
                        </div>
                    </div>
                    <!--Posee nómina LAFISE-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Posee n&oacute;mina LAFISE</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="poseeNomina" 
                                data-ng-model="newDatosFinancieros.PoseeNomina" 
                                data-ng-change="PoseeNominaChanged(newDatosFinancieros.PoseeNomina)" />
                        </div>
                    </div>
                    <!--Promedio de ingresos mensuales-->
                    <div class="lafise-container-posee-nomina" data-ng-show="newDatosFinancieros.ContainerPoseeNomina">
                        <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Promedio de ingresos mensuales (Moneda): </output>
                            </div>
                            <div class="lafise-container-select">
                                  <select
                                    id="lafise-prom-ingresos"
                                    data-ng-class="{submitted:addSolicitud.submitted}"
                                    name="promingresos"
                                    class="lafise-select"
                                    data-ng-model="newDatosFinancieros.PromedioIngresoSelected"
                                    data-ng-options="rangosueldo.RangoSueldo for rangosueldo in rangoSueldoPais"
                                    data-ng-required="newDatosFinancieros.ContainerPoseeNomina"
                                    data-ng-change="PromIngresosChanged(newDatosFinancieros.PromedioIngresoSelected)">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--Tiene negocio propio-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Tiene Negocio propio</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="negociopropio" 
                                data-ng-model="newDatosFinancieros.NegocioPropio" />
                        </div>
                    </div>
                    <!--Adjuntar soporte de negocio propio-->
                    <div data-ng-show="newDatosFinancieros.NegocioPropio">
                        <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Adjuntar soporte de negocio propio</output>
                            </div>
                            <div class="lafise-container-file">
                                <button
                                    id="file"
                                    type="file"
                                    ngf-select="uploadFiles($files, $invalidFiles,'NP')"
                                    accept="application/pdf"
                                    ngf-validate="{size: {min: 0, max: '5MB'}, pattern: '.pdf'}"
                                    ngf-max-size="5 MB"
                                    ngf-multiple="true"
                                    data-ng-required="newDatosFinancieros.NegocioPropio"
                                    data-ng-class="{submitted:addSolicitud.submitted}"
                                    name="archivoNP"
                                    ng-model="newDatosFinancieros.ArchivosNP">
                                    Examinar
                                </button>
                            </div>
                            <div class="lafise-lista-Archivos" data-ng-repeat="item in ListaArchivosNP">
                                <div class="lafise-nombre-archivo" data-ng-hide="item.showAlert">
                                    <output name="fileInvalid{{$index}}" data-ng-model="item.Nombre">{{item.Nombre}}</output>
                                    <span class="lafise-eliminar-archivo" data-ng-click="deleteFile($index,'NP',false)">X</span>
                                </div>
                                <div id="alertArchivo{{$index}}NP" data-ng-show="item.showAlert" class="alert alert-danger alert-dismissible" role="alert">
                                    <button
                                        type="button"
                                        data-ng-click="deleteFile($index,'NP',true)"
                                        class="close"
                                        data-dismiss="alert"
                                        aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="lafise-Archivo-Invalido">{{item.Nombre}}</div>
                                    <span class="lafise-Mensaje-Validacion">{{item.Error}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Es asalariado-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Es Asalariado</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="asalariado" 
                                data-ng-model="newDatosFinancieros.Asalariado" />
                        </div>
                    </div>
                    <!--Adjuntar soporte de ingresos-->
                    <div data-ng-show="newDatosFinancieros.Asalariado">
                        <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Adjuntar soporte de ingresos</output>
                            </div>
                            <div class="lafise-container-file">
                                <button
                                    id="file"
                                    type="file"
                                    ngf-select="uploadFiles($files, $invalidFiles,'SI')"
                                    accept="application/pdf"
                                    ngf-validate="{size: {min: 0, max: '5MB'}, pattern: '.pdf'}"
                                    ngf-max-size="5 MB"
                                    ngf-multiple="true"
                                    ng-required="newDatosFinancieros.Asalariado"
                                    data-ng-class="{submitted:addSolicitud.submitted}"
                                    name="archivoSI"
                                    ng-model="newDatosFinancieros.ArchivosSI">
                                    Examinar
                                </button>
                            </div>
                            <div class="lafise-lista-Archivos" data-ng-repeat="item in ListaArchivosSI">
                                <div class="lafise-nombre-archivo" data-ng-hide="item.showAlert">
                                    <output name="fileInvalid{{$index}}" data-ng-model="item.Nombre">{{item.Nombre}}</output>
                                    <span class="lafise-eliminar-archivo" data-ng-click="deleteFile($index,'SI',false)">X</span>
                                </div>
                                <div id="alertArchivo{{$index}}SI" data-ng-show="item.showAlert" class="alert alert-danger alert-dismissible" role="alert">
                                    <button
                                        type="button"
                                        data-ng-click="deleteFile($index,'SI',true)"
                                        class="close"
                                        data-dismiss="alert"
                                        aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="lafise-Archivo-Invalido">{{item.Nombre}}</div>
                                    <span class="lafise-Mensaje-Validacion">{{item.Error}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Sucursal -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Sucursal</output>
                        </div>
                        <div class="lafise-container-select">                      
                            <select
                                class="lafise-select" 
                                data-ng-model="newDatosFinancieros.IdSucursal" 
                                data-ng-options="sucursal.Id as sucursal.Nombre for sucursal in sucursales" 
                                required>
                                <option value=''>Seleccione</option>
                            </select>
                        </div>
                   </div>
                </div>
                <div class="lafise-container-referencias-personales">
                     <!-- Etiqueta Referencias personales -->
                     <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Referencias Personales</output>
                        </div>
                    </div>
                     <!-- Mensaje informativo a las referencias -->
                     <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <p class="lafise-label">Las referencias a ingresar deben ser personales no familiares</p>
                        </div>
                    </div>
                     <!-- Referencia 1 -->
                     <div class="lafise-group">
                        <div class="lafise-container-etiqueta-subtitulo">
                            <output class="lafise-label">Referencia 1</output>
                        </div>
                    </div>
                     <!-- Nombres  -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nombres</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="nombre"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newReferencia1.Nombre"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                     <!-- Apellidos-->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Apellidos</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="apellidos"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newReferencia1.Apellidos"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                     <!-- Teléfono -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Tel&eacute;fono</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="telefono"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newReferencia1.Telefono" 
                                   maxlength="50"
                                   required />
                        </div>
                    </div>
                     <!-- Referencia 2 -->
                     <div class="lafise-group">
                        <div class="lafise-container-etiqueta-subtitulo">
                            <output class="lafise-label">Referencia 2</output>
                        </div>
                    </div>
                     <!-- Nombres  -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nombres</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="nombre"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newReferencia2.Nombre"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                     <!-- Apellidos-->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Apellidos</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="apellidos"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newReferencia2.Apellidos"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                     <!-- Teléfono -->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Tel&eacute;fono</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="telefono"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newReferencia2.Telefono" 
                                   maxlength="50"
                                   required />
                        </div>
                    </div>
                </div>
                <!--Captcha-->
                <div class="signupform" data-ng-controller="CaptchaController as captcha">
                    <div class="{{!addSolicitud.$valid ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addSolicitud.submitted}">
                        <div vc-recaptcha key="captcha.publicKey"></div>
                    </div>
                    <div class="lafise-leyenda" data-ng-show="verLeyenda"> 
                        <div class="lafise-container-check">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Acepto</output>
                            </div>
                            <div class="{{!someSelected(AutorizoLeyenda.Autorizo) ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addSolicitud.submitted}">
                                <input 
                                    type="checkbox" 
                                    name="autorizoLeyenda" 
                                    data-ng-model="AutorizoLeyenda.Autorizo"
                                    data-ng-required="!someSelected(AutorizoLeyenda.Autorizo) && verLeyenda" />
                            </div>
                        </div>
                        <div data-ng-include="'/App/Base/html/LeyendaAutorizacionPanama.html'"></div>
                    </div>
                    <!-- Botones Enviar y Cancelar -->                 
                    <div class="lafise-container-options">
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Enviar" data-ng-click="submitted=true;add(captcha.signup())"/>
                        </div>
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()"  />
                        </div>
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ng-file-upload-all.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="/Scripts/angular.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="/Scripts/jquery.signalR-2.1.2.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="Scripts/angular-signalr-hub.min.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="/signalr/hubs" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/FrontEnd/AperturaCuenta/AperturaCuentaModule.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/FrontEnd/AperturaCuenta/AperturaCuentaFactory.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/FrontEnd/AperturaCuenta/AperturaCuentaController.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionModule.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionFactory.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionController.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Base/js/angular-recaptcha.min.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaModule.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaController.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="//www.google.com/recaptcha/api.js?hl=es&onload=vcRecaptchaApiLoaded&render=explicit" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="18" />
