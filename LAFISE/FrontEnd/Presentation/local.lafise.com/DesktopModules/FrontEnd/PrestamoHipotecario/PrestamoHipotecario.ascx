﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestamoHipotecario.ascx.cs" Inherits="PrestamoHipotecario" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div id="google_translate_element">
    <script>
        function googleTranslateElementInit() {
            new google.translate.TranslateElement(
                { pageLanguage: 'es' },
                'google_translate_element'
            );

            /*
                To remove the "powered by google",
                uncomment one of the following code blocks.
                NB: This breaks Google's Attribution Requirements:
                https://developers.google.com/translate/v2/attribution#attribution-and-logos
            */

            //Native (but only works in browsers that support query selector)
            if(typeof(document.querySelector) == 'function') {
                document.querySelector('.goog-logo-link').setAttribute('style', 'display: none');
                document.querySelector('.goog-te-gadget').setAttribute('style', 'font-size: 0');
            }

            // If you have jQuery - works cross-browser - uncomment this
            jQuery('.goog-logo-link').css('display', 'none');
            jQuery('.goog-te-gadget').css('font-size', '0');
        }
    </script>
    hola
</div>


<div class="lafise-container" data-ng-app="PrestamoHipotecarioModule">
    <div data-ng-controller="PrestamoHipotecarioController">

        <div class="lafise-group">
            <ng-form name="addSolicitud" ng-submit="add()">
                <!-- Crear formulario de prestamo hipotecario -->
                <div class="lafise-container-datos-personales">
                    <!-- Etiqueta datos personales -->
                    <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Datos personales</output>
                        </div>
                    </div>
                    <!-- Nombres  -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nombres</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="nombre"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Nombre"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                    <!-- Apellidos-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Apellidos</output>
                        </div>
                        <div class="lafise-container-text">
                            <input type="text"
                                   name="apellidos"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Apellidos"
                                   maxlength="100"
                                   required />
                        </div>
                    </div>
                    <!-- Teléfono -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Tel&eacute;fono</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="telefono"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Telefono" 
                                   maxlength="50"
                                   required />
                        </div>
                    </div>
                    <!-- Email -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Email</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="email"
                                   name="correo"
                                   id="correo"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Email"
                                   data-ng-blur="FormatoEmail();" 
                                   maxlength="200" 
                                   required />
                        </div>
                    </div>
                    <!-- Alerta - Email inválido -->
                    <div id="validacionEmail" data-ng-show="showMsgEmail" ng-class="'alert alert-danger alert-dismissible'" role="alert">
                        <button
                            type="button"
                            data-ng-click="hideValidacion()"
                            class="close"
                            data-dismiss="alert"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span ng-bind="msgValidarCorreo">
                        </span>
                    </div>
                    <!-- Genero -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Sexo</output>
                        </div>
                        <div class="lafise-container-text">
                             <select
                                    id="genero"
                                    name="genero"
                                    class="lafise-select" 
                                    data-ng-class="{submitted:addSolicitud.submitted}" 
                                    data-ng-model="newDatosPersonales.Masculino" 
                                    required>
                                    <option value="">Seleccione</option>
                                    <option value="false">Femenino</option>
                                    <option value="true">Masculino</option>
                                </select>
                        </div>
                    </div>
                    <!--Nacionalidad-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Nacionalidad</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="nacionalidad"
                                   id="nacionalidad"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Nacionalidad"
                                   maxlength="80" 
                                   required />
                        </div>
                    </div>
                    <!--Ciudad de residencia-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Ciudad de residencia</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="ciudadResidencia"
                                   id="ciudadResidencia"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.CiudadResidencia"
                                   maxlength="100" 
                                   required />
                        </div>
                    </div>
                    <!-- Seleccionar TipoIdentificacion -->
                    <div class="lafise-group">
                        <div data-ng-controller="TipoIdentificacionController">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Tipo Identificaci&oacute;n</output>
                            </div>
                            <div class="lafise-container-select">                      
                                <select
                                    class="lafise-select" 
                                    data-ng-class="{submitted:addSolicitud.submitted}" 
                                    data-ng-model="newDatosPersonales.IdTipoIdentificacion" 
                                    data-ng-options="tipoIdentificacion.Id as tipoIdentificacion.Nombre for tipoIdentificacion in tipoIdentificaciones"
                                    required>
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--Número de identificación-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">N&uacute;mero de identificaci&oacute;n</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="numeroIdentificacion"
                                   id="numeroIdentificacion"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.NumeroIdentificacion"
                                   maxlength="50" 
                                   required />
                        </div>
                    </div>
                    <!--Profesión-->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Profesi&oacute;n</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="text"
                                   name="profesion"
                                   id="profesion"
                                   class="lafise-text"
                                   data-ng-class="{submitted:addSolicitud.submitted}" 
                                   data-ng-model="newDatosPersonales.Profesion"
                                   maxlength="200" 
                                   required />
                        </div>
                    </div>
                </div>
                <div class="lafise-container-datos-financieros-laborales">
                    <!-- Etiqueta Datos financieros y laborales -->
                    <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Datos financieros y laborales</output>
                        </div>
                    </div>
                    <!--Es cliente LAFISE-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Es cliente LAFISE</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="EsCliente" 
                                data-ng-model="newDatosFinancieros.EsCliente" />
                        </div>
                    </div>
                    <!--Posee nómina LAFISE-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Posee n&oacute;mina LAFISE</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="poseeNomina" 
                                data-ng-model="newDatosFinancieros.PoseeNomina" 
                                data-ng-change="PoseeNominaChanged(newDatosFinancieros.PoseeNomina)" />
                        </div>
                    </div>
                    <!--Promedio de ingresos mensuales-->
                    <div class="lafise-container-posee-nomina" data-ng-show="newDatosFinancieros.ContainerPoseeNomina">
                        <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Promedio de ingresos mensuales (Moneda): </output>
                            </div>
                            <div class="lafise-container-select">
                                <select
                                    id="lafise-prom-ingresos"
                                    data-ng-class="{submitted:addSolicitud.submitted}"
                                    name="promingresos"
                                    class="lafise-select"
                                    data-ng-model="newDatosFinancieros.PromedioIngresoSelected"
                                    data-ng-options="rangosueldo.RangoSueldo for rangosueldo in rangoSueldoPais"
                                    data-ng-required="newDatosFinancieros.ContainerPoseeNomina"
                                    data-ng-change="PromIngresosChanged(newDatosFinancieros.PromedioIngresoSelected)">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--Tiene negocio propio-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Tiene Negocio propio</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="negociopropio" 
                                data-ng-model="newDatosFinancieros.NegocioPropio" />
                        </div>
                    </div>
                    <!--Adjuntar soporte de negocio propio-->
                    <div data-ng-show="newDatosFinancieros.NegocioPropio">
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Adjuntar soporte de negocio propio</output>
                        </div>
                        <div class="lafise-container-file">
                            <button
                                id="file"
                                type="file"
                                ngf-select="uploadFiles($files, $invalidFiles,'NP')"
                                accept="application/pdf"
                                ngf-validate="{size: {min: 0, max: '5MB'}, pattern: '.pdf'}"
                                ngf-max-size="5 MB"
                                ngf-multiple="true"
                                data-ng-required="newDatosFinancieros.NegocioPropio"
                                data-ng-class="{submitted:addSolicitud.submitted}"
                                name="archivoNP"
                                ng-model="newDatosFinancieros.ArchivosNP">
                                Examinar
                            </button>
                        </div>
                        <div class="lafise-lista-Archivos" data-ng-repeat="item in ListaArchivosNP">
                            <div class="lafise-nombre-archivo" data-ng-hide="item.showAlert">
                                <output name="fileInvalid{{$index}}" data-ng-model="item.Nombre">{{item.Nombre}}</output>
                                <span class="lafise-eliminar-archivo" data-ng-click="deleteFile($index,'NP',false)">X</span>
                            </div>
                            <div id="alertArchivo{{$index}}NP" data-ng-show="item.showAlert" class="alert alert-danger alert-dismissible" role="alert">
                                <button
                                    type="button"
                                    data-ng-click="deleteFile($index,'NP',true)"
                                    class="close"
                                    data-dismiss="alert"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="lafise-Archivo-Invalido">{{item.Nombre}}</div>
                                <span class="lafise-Mensaje-Validacion">{{item.Error}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                    <!--Es asalariado-->
                    <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Es Asalariado</output>
                        </div>
                        <div class="lafise-container-check">
                            <input 
                                type="checkbox" 
                                name="asalariado" 
                                data-ng-model="newDatosFinancieros.Asalariado" />
                        </div>
                    </div>
                    <!--Adjuntar soporte de ingresos-->
                    <div data-ng-show="newDatosFinancieros.Asalariado">
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Adjuntar soporte de ingresos</output>
                        </div>
                        <div class="lafise-container-file">
                            <button
                                id="file"
                                type="file"
                                ngf-select="uploadFiles($files, $invalidFiles,'SI')"
                                accept="application/pdf"
                                ngf-validate="{size: {min: 0, max: '5MB'}, pattern: '.pdf'}"
                                ngf-max-size="5 MB"
                                ngf-multiple="true"
                                ng-required="newDatosFinancieros.Asalariado"
                                data-ng-class="{submitted:addSolicitud.submitted}"
                                name="archivoSI"
                                ng-model="newDatosFinancieros.ArchivosSI">
                                Examinar
                            </button>
                        </div>
                        <div class="lafise-lista-Archivos" data-ng-repeat="item in ListaArchivosSI">
                            <div class="lafise-nombre-archivo" data-ng-hide="item.showAlert">
                                <output name="fileInvalid{{$index}}" data-ng-model="item.Nombre">{{item.Nombre}}</output>
                                <span class="lafise-eliminar-archivo" data-ng-click="deleteFile($index,'SI',false)">X</span>
                            </div>
                            <div id="alertArchivo{{$index}}SI" data-ng-show="item.showAlert" class="alert alert-danger alert-dismissible" role="alert">
                                <button
                                    type="button"
                                    data-ng-click="deleteFile($index,'SI',true)"
                                    class="close"
                                    data-dismiss="alert"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="lafise-Archivo-Invalido">{{item.Nombre}}</div>
                                <span class="lafise-Mensaje-Validacion">{{item.Error}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="lafise-container-plan-financiamiento">
                     <!-- Etiqueta Plan de financiamiento -->
                     <div class="lafise-group">
                        <div class="lafise-container-etiqueta">
                            <output class="lafise-label">Plan de financiamiento</output>
                        </div>
                    </div>
                     <div data-ng-controller="MonedaController">
                         <!--Valor del bien (Moneda)-->
                         <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Valor del bien (moneda)</output>
                            </div>
                            <div class="lafise-container-text">
                                <moneda
                                    id="ValorBien" 
                                    ng-model="newPlanFinanciamiento.ValorBienFormato"
                                    name="ValorBien"
                                    requerido="{{addSolicitud.submitted}}" 
                                    data-v-Min="0.00" 
                                    data-v-Max="999999999.99"
                                    required></moneda>
                            </div>
                        </div>
                         <!--Monto a financiar (Moneda)-->
                         <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Monto a financiar (moneda)</output>
                            </div>
                            <div class="lafise-container-text">
                                <moneda 
                                    id="MontoFinanciar"
                                    ng-model="newPlanFinanciamiento.MontoFinanciarFormato"  
                                    name="MontoFinanciar" 
                                    requerido="{{addSolicitud.submitted}}"
                                    data-v-Min="0.00" 
                                    data-v-Max="999999999.99"
                                    required></moneda>
                            </div>
                        </div>
                         <!--Prima o anticipo-->
                         <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Prima o anticipo</output>
                            </div>
                            <div class="lafise-container-text">
                                <moneda 
                                    id="PrimaAnticipo"
                                    ng-model="newPlanFinanciamiento.PrimaAnticipoFormato"  
                                    name="PrimaAnticipo" 
                                    requerido="{{addSolicitud.submitted}}"
                                    data-v-Min="0.00" 
                                    data-v-Max="999999999.99"
                                    required></moneda>
                            </div>
                        </div>
                        <input type="hidden" id="minimoprima" />
                     </div>
                     <!--Plazo-->
                     <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Plazo (Mes)</output>
                        </div>
                        <div class="lafise-container-text">
                              <moneda id="Plazo" ng-model="newPlanFinanciamiento.Plazo"  name="Plazo"  simbolo="" requerido="{{addSolicitud.submitted}}" data-a-Sep="" data-a-Pad="false" data-v-Min="0" data-v-Max="99" required></moneda>
                        </div>
                    </div>
                     <!--Construcción propia-->
                     <div class="lafise-group contentCheck">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Construcci&oacute;n propia</output>
                        </div>
                        <div class="lafise-container-text">
                             <input type="checkbox" 
                                    name="ConstruccionPropia" 
                                    value="ConstruccionPropia"
                                    data-ng-change="toggleConstruccionPropia()"
                                    data-ng-model="newPlanFinanciamiento.ConstruccionPropia">
                        </div>
                    </div>
                     <div data-ng-hide="newPlanFinanciamiento.ConstruccionPropia">
                         <!-- Categoria -->
                        <div class="lafise-group">
                                <div class="lafise-container-label">
                                    <output class="lafise-label">Categoria</output>
                                </div>
                                <div class="lafise-container-select">                      
                                    <select
                                        class="lafise-select" 
                                        data-ng-class="{submitted:!newPlanFinanciamiento.ConstruccionPropia && addSolicitud.submitted}" 
                                        data-ng-model="newPlanFinanciamiento.IdCategoria" 
                                        data-ng-options="categoria.Id as categoria.Nombre for categoria in categorias" 
                                        data-ng-required="!newPlanFinanciamiento.ConstruccionPropia">
                                        <option value=''>Seleccione</option>
                                    </select>
                                </div>
                        </div>
                         <!-- Tipo de propiedad -->
                        <div class="lafise-group">
                                <div class="lafise-container-label">
                                    <output class="lafise-label">Tipo de propiedad</output>
                                </div>
                                <div class="lafise-container-select">                      
                                    <select
                                        class="lafise-select" 
                                        data-ng-class="{submitted:!newPlanFinanciamiento.ConstruccionPropia && addSolicitud.submitted}" 
                                        data-ng-model="newPlanFinanciamiento.IdTipoPropiedad" 
                                        data-ng-options="tipo.Id as tipo.Tipo for tipo in tiposPropiedad" 
                                        data-ng-required="!newPlanFinanciamiento.ConstruccionPropia">
                                        <option value=''>Seleccione</option>
                                    </select>
                                </div>
                        </div>
                         <div data-ng-hide="newPlanFinanciamiento.IdTipoPropiedad != 1">
                            <!-- Urbanizadora -->
                          <div class="lafise-group">
                                  <div class="lafise-container-label">
                                      <output class="lafise-label">Urbanizadora</output>
                                  </div>
                                  <div class="lafise-container-select">                      
                                      <select
                                          class="lafise-select" 
                                          data-ng-class="{submitted:newPlanFinanciamiento.IdTipoPropiedad == 1 && addSolicitud.submitted}" 
                                          data-ng-model="newPlanFinanciamiento.IdUrbanizadora" 
                                          data-ng-options="urba.Id as urba.Nombre for urba in urbanizadoras" 
                                          data-ng-required="newPlanFinanciamiento.IdTipoPropiedad == 1">
                                          <option value=''>Seleccione</option>
                                      </select>
                                  </div>
                          </div>
                        </div>
                    </div>              
                </div>
                <!--Captcha-->
                <div class="signupform" data-ng-controller="CaptchaController as captcha">
                  <div class="{{!addSolicitud.$valid ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addSolicitud.submitted}"> 
                        <div vc-recaptcha key="captcha.publicKey"></div>
                      </div>
                    <div class="lafise-leyenda" data-ng-show="verLeyenda"> 
                        <div class="lafise-container-check">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Acepto</output>
                            </div>
                            <div class="{{!someSelected(AutorizoLeyenda.Autorizo) ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addSolicitud.submitted}">
                                <input 
                                    type="checkbox" 
                                    name="autorizoLeyenda" 
                                    data-ng-model="AutorizoLeyenda.Autorizo"
                                    data-ng-required="!someSelected(AutorizoLeyenda.Autorizo) && verLeyenda" />
                            </div>
                        </div>
                        <div data-ng-include="'/App/Base/html/LeyendaAutorizacionPanama.html'"></div>
                    </div>
                    <!-- Botones Enviar y Cancelar -->                 
                    <div class="lafise-container-options">
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Enviar" data-ng-click="submitted=true;add(captcha.signup())"/>
                        </div>
                        <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()"  />
                        </div>
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>

    </div>
</div>


<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ng-file-upload-all.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="/Scripts/angular.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="/Scripts/jquery.signalR-2.1.2.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="Scripts/angular-signalr-hub.min.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="/signalr/hubs" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/FrontEnd/PrestamoHipotecario/PrestamoHipotecarioModule.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/FrontEnd/PrestamoHipotecario/PrestamoHipotecarioFactory.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/FrontEnd/PrestamoHipotecario/PrestamoHipotecarioController.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionModule.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionFactory.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Comunes/TipoIdentificacion/TipoIdentificacionController.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Comunes/Moneda/MonedaModule.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Comunes/Moneda/MonedaController.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/Comunes/Moneda/MonedaFactory.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="~/App/Base/js/angular-recaptcha.min.js" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaModule.js" Priority="18" />
<dnn:DnnJsInclude ID="DnnJsInclude19" runat="server" FilePath="~/App/Comunes/Captcha/CaptchaController.js" Priority="19" />
<dnn:DnnJsInclude ID="DnnJsInclude20" runat="server" FilePath="//www.google.com/recaptcha/api.js?hl=es&onload=vcRecaptchaApiLoaded&render=explicit" Priority="20" />
<dnn:DnnJsInclude ID="DnnJsInclude21" runat="server" FilePath="~/App/Base/js/autoNumeric-min.js" Priority="21" />
<dnn:DnnJsInclude ID="DnnJsInclude22" runat="server" FilePath="~/App/Base/js/Moneda.js" Priority="22" />
<dnn:DnnJsInclude ID="DnnJsInclude23" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="23" />

