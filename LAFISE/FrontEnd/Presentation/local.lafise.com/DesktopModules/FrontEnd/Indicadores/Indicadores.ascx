﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Indicadores.ascx.cs" Inherits="Indicadores" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link href="../../../App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" ng-app="IndicadorModule">

    <div data-ng-controller="IndicadorController">

        <!-- Grid de valores actuales-->
        <div data-ng-show="showGridValoresActuales">
            <div id="gridIndicador" class="gridStyle lafise-grid" ui-grid-pagination ui-grid-selection ui-grid="gridOpts">
                <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
            </div>
        </div>

        <!-- Grid historico-->
        <div data-ng-show="showGridHistorico" class="historico">
            <div class="lafise-group">
                <div class="lafise-container-label">
                    <output class="lafise-label">Fecha inicial</output>
                </div>
                <div class="lafise-container-text">
                    <p class="input-group">
                        <input type="text"
                            class="form-controlinicial"
                            uib-datepicker-popup="{{'yyyy-MM-dd'}}"
                            ng-model="filtros.FechaInicial"
                            is-open="status.openedInicial"
                            ng-required="true"
                            close-text="Cerrar"
                            clear-text="Limpiar"
                            current-text="Hoy"
                            max-date="maxDateInicial"
                            min-date="minDateInicial"
                            ng-change="seleccionFechaInicial()"
                            name="fechaInicial"
                            disabled="disabled"
                            required />
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="openInicial($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                    </p>
                </div>

            </div>
            <div class="lafise-group">
                <div class="lafise-container-label">
                    <output class="lafise-label">Fecha final</output>
                </div>
                <div class="lafise-container-text">
                    <p class="input-group">
                        <input type="text"
                            class="form-controlfinal"
                            uib-datepicker-popup="{{'yyyy-MM-dd'}}"
                            ng-model="filtros.FechaFinal"
                            is-open="status.openedFinal"
                            ng-required="true"
                            close-text="Cerrar"
                            clear-text="Limpiar"
                            current-text="Hoy"
                            max-date="maxDateFinal"
                            min-date="minDateFinal"
                            ng-change="seleccionFechaFinal()"
                            name="fechaFinal"
                            disabled="disabled"
                            required />
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="openFinal($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                    </p>
                </div>
            </div>
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Generar" data-ng-click="generar()" />
                <input type="button" class="lafise-button" value="Regresar" data-ng-click="regresar()" />
            </div>
            <div id="graficaHistorico">
                <highchart id="chart1" config="chartConfig"></highchart>
            </div>
            <div id="gridIndicador" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOptsHistorico">
                <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
            </div>

        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>

    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/angular-locale_es-es.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/highstock.src.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/highcharts-ng.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/Base/js/angular-sanitize.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/Base/js/select.min.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/FrontEnd/Indicadores/IndicadorModule.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/FrontEnd/Indicadores/IndicadorFactory.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/FrontEnd/Indicadores/IndicadorController.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="12" />
