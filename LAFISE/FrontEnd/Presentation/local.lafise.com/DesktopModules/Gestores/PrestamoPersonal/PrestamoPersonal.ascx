﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestamoPersonal.ascx.cs" Inherits="PrestamoPersonal" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link href="/App/Base/css/grid.css" rel="stylesheet" />
<div data-ng-app="SolicitudModule">
    <div data-ng-controller="SolicitudController">
        <div class="tabbable" data-ng-hide="formMode">
            <ul class="nav nav-tabs nav-justified">
                <li data-ng-class="{active: activeTab1}">
                    <a href="" data-ng-click="OpenTab(true,false,false,2)">Bandeja Entrada</a>
                </li>
                <li ng-class="{active: activeTab2}">
                    <a href="" data-ng-click="OpenTab(false,true,false,2)">Bandeja Proceso</a>
                </li>
                <li ng-class="{active: activeTab3}">
                    <a href="" data-ng-click="OpenTab(false,false,true,2)">Bandeja Terminados</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" data-ng-class="{active: activeTab1}">
                    <div class="lafise-group">
                        <exportar filtros="FiltroSolicitudes"></exportar>
                    </div>
                    <div class="lafise-container-grid" data-ng-init="FiltroSolicitudes.IdTipoSolicitud='2'">
                        <div id="gridGestorEntrada" class="gridStyle lafise-grid" ui-grid="gridOpts" ui-grid-pagination ui-grid-selection>
                            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" data-ng-class="{active: activeTab2}">
                    <div class="lafise-group">
                        <exportar filtros="FiltroSolicitudes"></exportar>
                    </div>
                    <div class="lafise-container-grid">
                        <div id="gridGestorProceso" class="gridStyle lafise-grid" ui-grid="gridOpts" ui-grid-pagination ui-grid-selection>
                            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" data-ng-class="{active: activeTab3}">
                    <div class="lafise-group">
                        <exportar filtros="FiltroSolicitudes"></exportar>
                    </div>
                    <div class="lafise-container-grid">
                        <div id="gridGestorTerminados" class="gridStyle lafise-grid" ui-grid="gridOpts" ui-grid-pagination ui-grid-selection>
                            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="lafise-container-form" data-ng-show="formMode">
            <formulariobase esmarcaauto="false" eshipotecario="false" espersonal="true" estarjetacredito="false" estarjetadebito="false" esaperturacuenta="false" eseducativo="false" essegurovida="false" essegurovehicular="false" esreclamos="false" solicitud="solicitudObj"></formulariobase>

            <!-- Seccion de Estados-->
            <ng-form name="editSolicitud">
                <div class="lafise-container-estados" data-ng-hide="solicitudObj.bandejaTerminados">
                    <div class="lafise-container-Tipo-Contacto" data-ng-show="solicitudObj.bandejaProceso">
                        <!-- Tipo contacto -->
                        <div class="lafise-group">
                            <div class="lafise-container-label">
                                <output class="lafise-label">Tipo de contacto</output>
                            </div>
                            <div class="lafise-container-select">
                                <select class="lafise-select"
                                    name="tipocontacto"
                                    data-ng-model="newSolicitud.IdTipoContacto"
                                    data-ng-class="{submitted:editSolicitud.submitted}"
                                    data-ng-options="tipocontacto.Id as tipocontacto.Nombre for tipocontacto in tiposContacto"
                                    data-ng-change="selectListas();"
                                    data-ng-required="solicitudObj.bandejaProceso">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="alertTipoContacto" data-ng-show="showMsgTipoContacto" ng-class="'alert alert-danger alert-dismissible'" role="alert">
                        <button
                            type="button"
                            data-ng-click="hideValidacion(false,true)"
                            class="close"
                            data-dismiss="alert"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="lafise-Mensaje-Validacion" ng-bind="msgValidarTipoContacto"></span>
                    </div>
                    <!-- Estado -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Estado</output>
                        </div>
                        <div class="lafise-container-select">
                            <select class="lafise-select"
                                name="estado"
                                data-ng-model="newSolicitud.IdEstado"
                                data-ng-class="{submitted:editSolicitud.submitted}"
                                data-ng-options="estado.Id as estado.Nombre for estado in estados"
                                data-ng-change="selectListas();"
                                required>
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </div>
                    <div id="alertEstado" data-ng-show="showMsgEstado" ng-class="'alert alert-danger alert-dismissible'" role="alert">
                        <button
                            type="button"
                            data-ng-click="hideValidacion(true,false)"
                            class="close"
                            data-dismiss="alert"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="lafise-Mensaje-Validacion" ng-bind="msgValidarEstado"></span>
                    </div>
                    <!-- Comentario -->
                    <div class="lafise-group">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Comentario</output>
                        </div>
                        <div class="lafise-container-textarea">
                            <textarea name="comentario"
                                data-ng-class="{submitted:editSolicitud.submitted}"
                                data-ng-model="newSolicitud.comentario"
                                required></textarea>
                        </div>
                    </div>
                </div>

                <div class="lafise-container-options">
                    <div class="lafise-group" data-ng-hide="solicitudObj.bandejaTerminados">
                        <input type="button" class="lafise-button" value="Enviar" data-ng-click="add()" />
                    </div>
                    <div class="lafise-group" data-ng-hide="solicitudObj.bandejaTerminados">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" />
                    </div>
                    <div class="lafise-group" data-ng-hide="!solicitudObj.bandejaTerminados">
                        <input type="button" class="lafise-button" value="Regresar" data-ng-click="cancel()" />
                    </div>
                </div>

            </ng-form>
        </div>
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
    <div class="loading-spiner-holder" data-loading>
        <div class="loading-spiner">
            <img src="/images/ajax-loader.gif" />Cargando
        </div>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="/Scripts/angular.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="/Scripts/jquery.signalR-2.1.2.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="Scripts/angular-signalr-hub.min.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="/signalr/hubs" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/Comunes/Solicitud/SolicitudModule.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/Comunes/Solicitud/SolicitudController.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/Comunes/Solicitud/SolicitudFactory.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Gestores/ExportarBase/ExportarBaseModule.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Gestores/ExportarBase/ExportarBaseFactory.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Base/js/ExportarBase.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Gestores/FormularioBase/FormularioBaseModule.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Gestores/FormularioBase/FormularioBaseFactory.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/Base/js/FormularioBase.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/Base/js/autoNumeric-min.js" Priority="18" />
<dnn:DnnJsInclude ID="DnnJsInclude19" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="19" />


