﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Urbanizadora.ascx.cs" Inherits="Urbanizadora" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" data-ng-app="UrbanizadoraModule">
    <div data-ng-controller="UrbanizadoraController">
        <!-- Plantilla de la ventana modal al eliminar urbanizadora -->
        <script type="text/ng-template" id="plantillaEliminarUrbanizadora.html">
            <div class="modal-header">
                <h3 class="modal-title">Confirmaci&oacute;n</h3>
            </div>
            <div class="modal-body">
                <div class="contenido">¿Est&aacute; seguro que desea eliminar la urbanizadora?<div>           
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
                <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
            </div>
        </script>
        <div class="lafise-filters">
            <!-- Filtro Pais -->
            <div class="lafise-group">
                <div data-ng-controller="PaisController">
                    <output class="lafise-label">Pais</output>
                    <ui-select multiple ng-model="paisesSelected.Paises" theme="select2" style="width: 300px;" data-ng-disabled="paises.length == 1">
                    <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                    <ui-select-choices repeat="pais.Id as pais in paises | filter:$select.search">
                    {{pais.Nombre}}
                    </ui-select-choices>
                </ui-select>
                </div>
            </div>
            <!-- Filtro Urbanizadora -->
            <div class="lafise-group">
                <output class="lafise-label">Urbanizadora</output>
                <input type="text"
                    class="lafise-text"
                    id="urbanizadoraFilter"
                    data-ng-model="urbanizadoraSelected"
                    maxlength="200" />
            </div>
            <!-- Filtro Tipo Propiedad -->
            <div class="lafise-group">
                <div data-ng-controller="TipoPropiedadController">
                    <output class="lafise-label">Tipo Propiedad</output>
                    <ui-select multiple ng-model="tipoPropiedadesSelected.Tipos" theme="select2" style="width: 300px;">
                    <ui-select-match placeholder="Seleccione">{{$item.Tipo}}</ui-select-match>
                    <ui-select-choices repeat="tipo.Id as tipo in tipoPropiedadesActivas | filter:$select.search">
                    {{tipo.Tipo}}
                    </ui-select-choices>
                </ui-select>
                </div>
            </div>
        </div>
        <div class="lafise-container-options">
            <!-- Botones Buscar, Crear y Exportar -->
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="getData()" data-ng-model="searchText" />
            </div>
            <div class="lafise-group">
                <input type="button" value="Crear Urbanizadora" class="lafise-button" data-ng-click="toggleCreate()" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <!-- Grid -->
        <div id="gridUrbanizadora" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
        </div>
        <!-- Información del grid a exportar -->
        <div id="exportable" style="display: none">
            <table id="exportarGrid" width="80%">
                <thead>
                    <tr>
                        <th>Pais</th>
                        <th>Urbanizadora</th>
                        <th>Tipo propiedad</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in urbanizadoras">
                        <td align="left">{{item.Pais}}</td>
                        <td align="left">{{item.Urbanizadora}}</td>
                        <td align="left">{{item.TipoPropiedad}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lafise-group">
            <ng-form ng-show="formMode" name="addUrbanizadora" ng-submit="add()" class="ng-dirty">
                <!-- Crear Urbanizadora -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Urbanizadora</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                               name="urbanizadora"
                               class="lafise-text"
                               data-ng-class="{submitted:addUrbanizadora.submitted}" 
                               data-ng-model="urbanizadoraCreated" 
                               data-ng-blur="UniqueName();"
                                maxlength="200"
                               required />
                    </div>
                </div>
                <!-- Seleccionar Pais -->
                <div class="lafise-group">

                    <div data-ng-controller="PaisController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Pa&iacute;s</output>
                        </div>
                        <div class="lafise-container-select">                      
                            <select
                                id="crearPais"
                                name="pais"
                                class="lafise-select" 
                                data-ng-class="{submitted:addUrbanizadora.submitted}" 
                                data-ng-model="newPais.IdPais" 
                                data-ng-options="pais.Id as pais.Nombre for pais in paises" 
                                data-ng-disabled="paises.length == 1"
                                required>
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </div>

                </div>

                <!-- Seleccionar Tipo Propiedad -->        
                <div class="lafise-group">
                    <div data-ng-controller="TipoPropiedadController">
                        <div class="{{!someSelected(seleccionadas.tipoPropiedades) ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addUrbanizadora.submitted}">
                            <label data-ng-repeat="tipo in tipoPropiedadesActivas">
                                <input type="checkbox" 
                                    name="tipoPropiedades" 
                                    checklist-model="seleccionadas.tipoPropiedades" 
                                    checklist-value="tipo" 
                                    data-ng-checked="isChecked(tipo)"
                                    data-ng-click="updateSelection($event, tipo.Id)"
                                    data-ng-required="!someSelected(seleccionadas.tipoPropiedades)"> {{tipo.Tipo}}
                            </label>
                        </div>
                    </div>
                </div>
                  <!-- Botones Actualizar, Guardar y Cancelar -->                 
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Actualizar" data-ng-click="update()" data-ng-show="editMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-click="submitted=true;add()" data-ng-show="addMode"/>
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando</div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:dnnjsinclude id="DnnJsInclude1" runat="server" filepath="~/App/Base/js/angular.min.js" priority="1" />
<dnn:dnnjsinclude id="DnnJsInclude2" runat="server" filepath="~/App/Base/js/ui-bootstrap-tpls.min.js" priority="2" />
<dnn:dnnjsinclude id="DnnJsInclude3" runat="server" filepath="~/App/Base/js/ui-grid.min.js" priority="3" />
<dnn:dnnjsinclude id="DnnJsInclude4" runat="server" filepath="~/App/Base/js/angular-sanitize.js" priority="4" />
<dnn:dnnjsinclude id="DnnJsInclude5" runat="server" filepath="~/App/Base/js/select.min.js" priority="5" />
<dnn:dnnjsinclude id="DnnJsInclude6" runat="server" filepath="~/App/Base/js/checklist-model.js" priority="6" />
<dnn:dnnjsinclude id="DnnJsInclude7" runat="server" filepath="~/App/Base/js/FileSaver.js" priority="7" />
<dnn:dnnjsinclude id="DnnJsInclude8" runat="server" filepath="~/App/BackOffice/Urbanizadora/UrbanizadoraModule.js" priority="8" />
<dnn:dnnjsinclude id="DnnJsInclude9" runat="server" filepath="~/App/BackOffice/Urbanizadora/UrbanizadoraFactory.js" priority="9" />
<dnn:dnnjsinclude id="DnnJsInclude10" runat="server" filepath="~/App/BackOffice/Urbanizadora/UrbanizadoraController.js" priority="10" />
<dnn:dnnjsinclude id="DnnJsInclude11" runat="server" filepath="~/App/Comunes/Pais/PaisModule.js" priority="11" />
<dnn:dnnjsinclude id="DnnJsInclude12" runat="server" filepath="~/App/Comunes/Pais/PaisFactory.js" priority="12" />
<dnn:dnnjsinclude id="DnnJsInclude13" runat="server" filepath="~/App/Comunes/Pais/PaisController.js" priority="13" />
<dnn:dnnjsinclude id="DnnJsInclude14" runat="server" filepath="~/App/BackOffice/TipoPropiedad/TipoPropiedadModule.js" priority="14" />
<dnn:dnnjsinclude id="DnnJsInclude15" runat="server" filepath="~/App/BackOffice/TipoPropiedad/TipoPropiedadFactory.js" priority="15" />
<dnn:dnnjsinclude id="DnnJsInclude16" runat="server" filepath="~/App/BackOffice/TipoPropiedad/TipoPropiedadController.js" priority="16" />
<dnn:dnnjsinclude id="DnnJsInclude17" runat="server" filepath="~/App/Base/js/ModalEliminarController.js" priority="17" />
<dnn:dnnjsinclude id="DnnJsInclude18" runat="server" filepath="~/App/Base/js/VentanaCargando.js" priority="18" />


