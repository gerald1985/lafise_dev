﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Indicador.ascx.cs" Inherits="Indicador" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<%--<link href="/App/Base/css/grid.css" rel="stylesheet" />--%>
<link href="../../../App/Base/css/grid.css" rel="stylesheet" />


<div class="lafise-container" data-ng-app="IndicadorModule">
    <!-- Plantilla de la ventana modal al eliminar un indicador -->
    <script type="text/ng-template" id="modalEliminar.html">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <div class="contenido">¿Est&aacute seguro que desea eliminar el indicador?<div>           
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
        </div>
    </script>
    <div data-ng-controller="IndicadorController">

        <!-- Botón para crear un nuevo indicador-->
        <div class="lafise-group">
            <input type="button" class="lafise-button" data-ng-click="create()" value="Crear indicador" />
        </div>
        <!-- Grid de valores actuales-->
        <div>
            <div id="gridIndicador" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
                <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la b&uacutesqueda.</div>
            </div>
        </div>
        
        <div class="lafise-group">
            <ng-form data-ng-show="formMode" name="addIndicador" novalidate>
               
                <!-- Nombre -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Nombre Indicador</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"                       
                            name="nombreIndicador"
                            class="lafise-text"
                            data-ng-class="{submitted:addIndicador.submitted}"
                            data-ng-model="newIndicador.NombreIndicador"
                            data-ng-blur="UniqueName();"
                            maxlength="50"
                            data-ng-required="true" />
                    </div>
                </div>

                <!-- Símbolo Actual -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">S&iacute;mbolo Actual</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="simboloActual"
                            class="lafise-text"
                            data-ng-class="{submitted:addIndicador.submitted}"
                            data-ng-blur="verificarIndicador();"
                            data-ng-model="newIndicador.SimboloActual"
                            maxlength="50"
                            required />
                    </div>
                </div>

                <!-- Símbolo Histórico -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">S&iacute;mbolo Hist&oacute;rico</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            name="simboloHistorico"
                            class="lafise-text"
                            data-ng-class="{submitted:addIndicador.submitted}"
                            data-ng-model="newIndicador.SimboloHistorico"
                            data-ng-blur="verificarIndicadorHistorico();"
                            maxlength="50"/>
                    </div>
                </div>

                  <!-- Botones Actualizar, Guardar y Cancelar -->  
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" data-ng-click="update()" value="Actualizar" data-ng-show="editMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-click="submitted=true;add()" data-ng-show="addMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert data-ng-show="showMsg" data-ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>


    </div>
</div>


<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/angular-sanitize.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/select.min.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/BackOffice/Indicador/IndicadorModule.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/BackOffice/Indicador/IndicadorFactory.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/BackOffice/Indicador/IndicadorController.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/Base/js/ModalEliminarController.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="11" />
