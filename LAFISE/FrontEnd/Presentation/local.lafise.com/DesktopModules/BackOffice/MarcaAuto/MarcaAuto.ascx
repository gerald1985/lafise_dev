﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MarcaAuto.ascx.cs" Inherits="MarcaAuto" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" data-ng-app="MarcaAutoModule">
    <div data-ng-controller="MarcaAutoController">
        <script type="text/ng-template" id="modalEliminar.html">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <div class="contenido">¿Esta seguro que desea eliminar la marca de auto?<div>           
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
        </div>
    </script>
        <script type="text/ng-template" id="modalEliminarModelo.html">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <div class="contenido">¿Esta seguro que desea eliminar el modelo de auto?<div>           
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
        </div>
    </script>
        <div class="lafise-group">
            <output class="lafise-label">Marca</output>
            <input type="text" class="lafise-text" data-ng-model="filters.Marca" maxlength="30" />
        </div>
        <div class="lafise-group">
            <output class="lafise-label">Modelo</output>
            <input type="text" class="lafise-text" data-ng-model="filters.Modelo" maxlength="30" />
        </div>
        <div class="lafise-container-options">
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="refreshData()" />
            </div>

            <div class="lafise-group">
                <input type="button" value="Crear Marca de Auto" class="lafise-button" data-ng-click="toggleCreate()" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <div class="lafise-container-grid">
            <div id="gridMarcaAuto" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
                <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
            </div>
        </div>
        <div id="exportable" style="display: none">
            <table width="80%">
                <thead>
                    <tr>
                        <th>Marca Auto</th>
                        <th>Modelos Auto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in listaMarcas">
                        <td align="left">{{item.Marca}}</td>
                        <td align="left">{{item.Modelo}} </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lafise-group">
            <ng-form data-ng-show="formMode" name="addMarcaAuto"> 
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Marca</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text" name="Marca" id="Marca" class="lafise-text" maxlength="30" data-ng-class="{submitted:addMarcaAuto.submitted}" data-ng-model="newMarcaAuto.Marca" ng-model-options="{ updateOn: 'blur' }" blacklist="{{blackListMarcas.name}}" ng-blur="clearValidate($event)" required />
                        <div id="alertModelo" data-ng-show="addMarcaAuto.Marca.$error.blacklist" class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" data-ng-click="closeAlertDetalleMarca()" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        La marca de auto {{newMarcaAuto.Marca}} ya existe, por favor validar
                        </div>
                    </div>
                </div>
                <div class="lafise-group examinar">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Modelos</output>
                    </div>
                    <div id="modelosList" data-ng-repeat="item in modelos">
                        <div class="content-modelos" data-ng-show="item.deleteRow">
                            <div data-ng-show="item.modeEdit">
                                <input type="text" name="Modelo{{$index}}" id="modelo-{{$index}}" class="lafise-text" maxlength="30" data-ng-model="item.Modelo" ng-model-options="{ updateOn: 'blur' }"  data-ng-class="{submitted:addMarcaAuto.submitted}" blacklist="{{blackListModelos.name}}" ng-blur="clearValidateModelo($event,$index)" required />
                                <button id="file-{{$index}}" class="lafise-file" name="fileInvalid{{$index}}" data-ng-model="item.imagen" type="file" ngf-select="uploadFiles($file, $invalidFiles,$index,item.Modelo)" accept="image/*" ngf-validate="{size: {min: 0, max: '10MB'},                  pattern: '.jpg,.gif,.png,.jpeg'}" ngf-max-size="1 MB">Examinar</button>
                                <div id="info-{{$index}}" class="lafise-file-info" data-ng-model="item.imagen">{{item.imagen}}</div>


                                <div id="alertImagen{{$index}}" data-ng-show="addMarcaAuto.fileInvalid{{$index}}.$error.archivoInvalido" class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" data-ng-click="CloseAlertImagenModelo($index)" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{item.errorPeso}}{{item.errorFormato}} 
                                </div>
                                <span id="lafise-option-delete" class="lafise-file-delete" data-ng-show="item.showDelete" data-ng-click="deleteItem($index)">X</span>
                                <span id="lafise-option-cancelEdit" class="lafise-file-cancel-edit" data-ng-show="item.editCancel" data-ng-click="cancelEdit($index)">X</span>
                                <div data-ng-show="addMarcaAuto.Modelo{{$index}}.$error.blacklist" class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" data-ng-click="CloseAlertDetalleModelo($index)"><span aria-hidden="true">&times;</span></button>
                                    El modelo {{item.Modelo}} ya se encuentra agregado, por favor validar
                                </div>
                            </div>
                            <div data-ng-hide="item.modeEdit">
                                <span class="lafise-label" data-ng-model="item.Modelo">{{item.Modelo}}</span>
                                <zoom big="item.RutaImagen" small="item.RutaThumb"></zoom>
                                <div class="lafise-container-options">
                                    <div class="lafise-group">
                                        <a target="_self" class="lafise-link" data-ng-href="{{item.RutaImagen}}" download="{{item.imagen}}">Descargar</a>
                                    </div>
                                    <div class="lafise-group">
                                        <input type="button" value="Editar" class="lafise-button" data-ng-click="editModelo($index)" />
                                    </div>
                                    <div class="lafise-group">
                                        <input type="button" value="Activar/Inactivar" data-ng-class="{'activo': item.Activo, 'inactivo': !item.Activo}" class="lafise-button" data-ng-click="activeModelo($index)" />
                                    </div>
                                    <div class="lafise-group">
                                        <input type="button" value="Eliminar" class="lafise-button" data-ng-click="openDeleteModelos($index);" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <input type="button" data-ng-click="addItem()" value="Agregar" />
                    </div>
                </div>
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-show="editMode" data-ng-click="update()" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-show="addMode" data-ng-click="add()" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>



<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ng-file-upload-all.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/jquery.ez-plus.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/BackOffice/MarcaAuto/MarcaAutoModule.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/BackOffice/MarcaAuto/MarcaAutoFactory.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/BackOffice/MarcaAuto/MarcaAutoController.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/Base/js/ModalEliminarController.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Base/js/blackList.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/BackOffice/MarcaAuto/ModalEliminarModeloController.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Base/js/Zoom.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Base/js/angular-uuid2.min.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="15" />



