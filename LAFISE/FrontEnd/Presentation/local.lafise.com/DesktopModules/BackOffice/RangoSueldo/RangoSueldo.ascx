﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RangoSueldo.ascx.cs" Inherits="RangoSueldo" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" data-ng-app="RangoSueldoModule">
    <div data-ng-controller="RangoSueldoController">
        <!-- Filtro Pais -->
        <div class="lafise-group">
            <div data-ng-controller="PaisController">
                <output class="lafise-label">Pais</output>
                <ui-select multiple ng-model="paisesSelected.Paises" theme="select2" style="width: 300px;" data-ng-disabled="paises.length == 1">
                    <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                    <ui-select-choices repeat="pais.Id as pais in paises | filter:$select.search">
                    {{pais.Nombre}}
                    </ui-select-choices>
                </ui-select>
            </div>
        </div>
        <div class="lafise-container-options">
            <!-- Botones Buscar y Exportar -->
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="getData()" data-ng-model="searchText" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <!-- Grid -->
        <div id="gridRangoSueldos" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
        </div>
        <!-- Información del grid a exportar -->
        <div id="exportable" style="display: none">
            <table width="80%">
                <thead>
                    <tr>
                        <th>Pais</th>
                        <th>Moneda</th>
                        <th>Rango sueldo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in rangoSueldos">
                        <td align="left">{{item.Pais}}</td>
                        <td align="left">{{item.Moneda}}</td>
                        <td align="left">{{item.RangoSueldo}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lafise-group">
            <ng-form ng-show="formMode" name="editRangoSueldo" ng-submit="update()">
                <!-- Seleccionar Pais Edición-->
                <div class="lafise-group">
                    <div data-ng-controller="PaisController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Pa&iacute;s</output>
                        </div>
                        <div class="lafise-container-select">                      
                            <select 
                                name="pais"
                                class="lafise-select" 
                                data-ng-class="{submitted:editRangoSueldo.submitted}" 
                                data-ng-model="newPais.IdPais" 
                                data-ng-options="pais.Id as pais.Nombre for pais in paises" 
                                data-ng-disabled="true"
                                required>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Seleccionar Moneda Edición-->
                <div class="lafise-group">
                    <div data-ng-controller="MonedaController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Moneda</output>
                        </div>
                        <div class="lafise-container-select">                      
                            <select
                                name="moneda"
                                class="lafise-select" 
                                data-ng-class="{submitted:editRangoSueldo.submitted}" 
                                data-ng-model="monedaCreated.selected" 
                                data-ng-options="moneda.Id as moneda.Nombre for moneda in monedas" 
                                required>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Rangos</output>
                    </div>
                    <div class="lafise-container-text">   
                        <input type="button" data-ng-click="addItem()" value="Agregar" />
                    </div>
                </div>
                <div class="lafise-group">
                     <!-- Rangos sueldo Edición-->
                    <div id="rangosList" data-ng-repeat="item in rangos">
                         <div class="content-modelos"> 
                             <moneda
                                    id="RangoMin" 
                                    ng-model="item.MinSueldo"
                                    data-ng-disabled="item.disabled" 
                                    name="RangoMin"
                                    requerido="{{editRangoSueldo.submitted}}" 
                                    data-v-Min="0.00" 
                                    data-v-Max="999999999.99"
                                    required></moneda>
                              <input type="hidden" id="minimoRango{{$index}}" data-a-sign="{{Simbolo}}" />
                             <moneda
                                    id="RangoMax{{$index}}" 
                                    ng-model="item.MaxSueldo"
                                    name="RangoMax"
                                    requerido="{{editRangoSueldo.submitted}}" 
                                    data-v-Min="0.00" 
                                    data-v-Max="999999999.99"
                                    data-ng-blur="ValidarMayor($index)"
                                    required></moneda>
                             <input type="hidden" id="maximoRango{{$index}}" data-a-sign="{{Simbolo}}" />
                             <span data-ng-show="item.showDelete" data-ng-click="deleteItem($index)">X</span>
                         </div>
                     </div>
                </div>
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar"  data-ng-show="editMode" data-ng-click="submitted=true;update()"  />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
             </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/angular-sanitize.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/select.min.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/Base/js/ui-grid-unstable.min.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/BackOffice/RangoSueldo/RangoSueldoModule.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/BackOffice/RangoSueldo/RangoSueldoFactory.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/BackOffice/RangoSueldo/RangoSueldoController.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Comunes/Pais/PaisModule.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/Pais/PaisFactory.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Comunes/Pais/PaisController.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Comunes/Moneda/MonedaModule.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Comunes/Moneda/MonedaFactory.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/Comunes/Moneda/MonedaController.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="~/App/Base/js/autoNumeric-min.js" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/Base/js/Moneda.js" Priority="18" />
<dnn:DnnJsInclude ID="DnnJsInclude19" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="19" />
