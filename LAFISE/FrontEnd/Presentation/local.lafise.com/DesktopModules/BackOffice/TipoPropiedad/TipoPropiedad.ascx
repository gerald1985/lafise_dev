﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TipoPropiedad.ascx.cs" Inherits="TipoPropiedad" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link href="/App/Base/css/grid.css" rel="stylesheet" />
<div class="lafise-container" data-ng-app="TipoPropiedadModule">
    <div data-ng-controller="TipoPropiedadController">
        <script type="text/ng-template" id="modalEliminar.html">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <div class="contenido">“¿Esta seguro que desea eliminar el tipo de propiedad?<div>           
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
        </div>
    </script>

        <div class="lafise-group">
            <output class="lafise-label">Tipo Propiedad</output>
            <input type="text" class="lafise-text" data-ng-model="searchText" maxlength="250" />

        </div>
        <div class="lafise-container-options">
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="refreshData()" data-ng-model="searchText" />
            </div>
            <div class="lafise-group">
                <input type="button" value="Crear Tipo propiedad" class="lafise-button" data-ng-click="toggleCreate()" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <div id="gridTipoPropiedades" class="gridStyle lafise-grid" ui-grid="gridOpts" ui-grid-pagination>
            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>

        </div>
        <div id="exportable" style="display: none">
            <table width="80%">
                <thead>
                    <tr>
                        <th>Tipo Propiedad</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in tipoPropiedades">
                        <td align="left">{{item.Tipo}}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="lafise-group">
            <ng-form data-ng-show="formMode" name="addTipoPropiedad">
                <div class="lafise-container-label">
                    <output class="lafise-label">Tipo Propiedad</output>
                </div>
                <div class="lafise-container-text">
                    <input type="text" data-ng-class="{submitted:addTipoPropiedad.submitted}" class="lafise-text" data-ng-model="newTipoPropiedad.Tipo" data-ng-blur="UniqueName();" maxlength="250" required />
                </div>
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" data-ng-click="update()" value="Guardar" data-ng-show="editMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-click="add()" data-ng-show="addMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <uib-alert ng-show="showMsg" data-ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>



<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/BackOffice/TipoPropiedad/TipoPropiedadModule.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/BackOffice/TipoPropiedad/TipoPropiedadFactory.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/BackOffice/TipoPropiedad/TipoPropiedadController.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/Base/js/ModalEliminarController.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="9" />
