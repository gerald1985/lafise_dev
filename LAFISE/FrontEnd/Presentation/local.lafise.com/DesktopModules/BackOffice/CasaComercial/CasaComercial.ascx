﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CasaComercial.ascx.cs" Inherits="CasaComercial" %>

<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />
<div class="casaComercial" data-ng-app="CasaComercialModule">

    <div data-ng-controller="CasaComercialController">
        <!-- Plantilla de la ventana modal al eliminar casa comercial -->
        <script type="text/ng-template" id="plantillaEliminarCasaComercial.html">
            <div class="modal-header">
                <h3 class="modal-title">Confirmaci&oacute;n</h3>
            </div>
            <div class="modal-body">
                <div class="contenido">¿Est&aacute; seguro que desea eliminar la casa comercial?<div>           
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
                <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
            </div>
        </script>
        <div class="lafise-filters">
            <!-- Filtro Pais -->
            <div class="lafise-group">
                <div data-ng-controller="PaisController">
                    <output class="lafise-label">Pais</output>
                    <ui-select multiple ng-model="paisesSelected.Paises" theme="select2" style="width: 300px;" data-ng-disabled="paises.length == 1">
                    <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                    <ui-select-choices repeat="pais.Id as pais in paises | filter:$select.search">
                    {{pais.Nombre}}
                    </ui-select-choices>
                </ui-select>
                </div>
            </div>
            <!-- Filtro Casa comercial -->
            <div class="lafise-group">
                <output class="lafise-label">Casa comercial</output>
                <input type="text"
                    class="lafise-text"
                    data-ng-model="casaComercialSelected"
                    maxlength="50" />
            </div>
            <!-- Filtro Marca -->
            <div class="lafise-group">
                <output class="lafise-label">Marca</output>
                <ui-select multiple ng-model="marcasSelected.Marcas" theme="select2" style="width: 300px;">
                    <ui-select-match placeholder="Seleccione">{{$item.Marca}}</ui-select-match>
                    <ui-select-choices repeat="marca.Id as marca in marcas | filter:$select.search">
                    {{marca.Marca}}
                    </ui-select-choices>
                </ui-select>
            </div>
        </div>
        <div class="lafise-container-options">
            <!-- Botones Buscar, Crear y Exportar -->
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="getData()" data-ng-model="searchText" />
            </div>
            <div class="lafise-group">
                <input type="button" value="Crear casa comercial" class="lafise-button" data-ng-click="toggleCreate()" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <!-- Grid -->
        <div id="gridCasaComercial" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
        </div>
        <!-- Información del grid a exportar -->
        <div id="exportable" style="display: none">
            <table id="exportarGrid" width="80%">
                <thead>
                    <tr>
                        <th>Pais</th>
                        <th>Casa comercial</th>
                        <th>Marca</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in casasComerciales">
                        <td align="left">{{item.NombrePais}}</td>
                        <td align="left">{{item.Nombre}}</td>
                        <td align="left">{{item.Marcas}}</td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div class="lafise-group">
            <ng-form ng-show="formMode" name="addCasaComercial">
              <!-- Crear Casa comercial -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Casa comercial</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                               class="lafise-text"
                               data-ng-class="{submitted:addCasaComercial.submitted}" 
                               data-ng-model="newCasaComercial.Nombre" 
                               data-ng-blur="UniqueName();"
                               maxlength="50"
                               required />
                    </div>
                </div>
                <!-- Seleccionar Pais -->
                <div class="lafise-group">
                    <div data-ng-controller="PaisController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Pa&iacute;s</output>
                        </div>
                        <div class="lafise-container-select">                      
                            <select
                                class="lafise-select" 
                                data-ng-class="{submitted:addCasaComercial.submitted}" 
                                data-ng-model="newPais.IdPais" 
                                data-ng-options="pais.Id as pais.Nombre for pais in paises"
                                data-ng-disabled="paises.length == 1" 
                                required>
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Seleccionar Tipo auto -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Tipo auto</output>
                    </div>
                    <div class="lafise-container-check">
                        <div class="{{!someSelected(seleccionadosTipoAutos) ? 'ng-invalid' : '' }}" data-ng-class="{submitted:addCasaComercial.submitted}">
                            <label data-ng-repeat="tipo in tipoAutos">
                                <input type="checkbox" 
                                    checklist-model="seleccionadosTipoAutos" 
                                    checklist-value="tipo" 
                                    data-ng-checked="isCheckedTipoAuto(tipo)"
                                    data-ng-click="updateSelectionTipoAuto($event, tipo.Id)"
                                    data-ng-required="!someSelected(seleccionadosTipoAutos)"> {{tipo.TipoAuto}}
                            </label>
                        </div>
                    </div>
                </div>
                <!-- Seleccionar Marca -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                         <output class="lafise-label">Marcas de auto</output>
                    </div>
                         
                    <div class="{{!marcasCreated.Marcas.length > 0 ? 'ng-invalid' : '' }} lafise-container-text" data-ng-class="{submitted:addCasaComercial.submitted}">
                        <ui-select multiple data-ng-model="marcasCreated.Marcas" data-ng-required="!marcasCreated.Marcas.length > 0" theme="select2" style="width: 300px;">
                            <ui-select-match placeholder="Seleccione">{{$item.Marca}}</ui-select-match>
                            <ui-select-choices ui-disable-choice="marca.RelacionCasa == true" repeat="marca.Id as marca in marcas | filter:$select.search">
                            {{marca.Marca}}
                            </ui-select-choices>
                        </ui-select>
                    </div>
                </div>
                <div class="lafise-group">
                    <div class="lafise-container-label">  
                        <input type="button" data-ng-click="addMarcasModelos()" value="Agregar Modelo" data-ng-disabled="!marcasCreated.Marcas.length > 0" />
                     </div>
                </div>
                <!-- Seleccionar Modelos Creación y Edición -->        
                <div class="lafise-group">
                        <div id="marcasList" data-ng-repeat="item in modelos">
                            <span data-ng-click="deleteMarcaModelo($index, item)">{{item.Marca}} X</span>
                                <br />
                            <div data-ng-repeat="modelo in item.listaModelos">
                                <input type="checkbox" 
                                    checklist-model="seleccionadas.modelos" 
                                    checklist-value="modelo" 
                                    data-ng-checked="isChecked(modelo)"
                                    data-ng-click="updateSelection($event, modelo.Id)"> {{modelo.Modelo}}
                                    <zoom big="modelo.RutaImagen" small="modelo.RutaThumb"></zoom>
                            </div>
                        </div>
                </div>
                
                <!-- Botones Actualizar, Guardar y Cancelar -->                 
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Actualizar" data-ng-click="update()" data-ng-show="editMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-click="submitted=true;add()" data-ng-show="addMode"/>
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/angular-sanitize.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/select.min.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/Base/js/checklist-model.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/BackOffice/CasaComercial/CasaComercialModule.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/BackOffice/CasaComercial/CasaComercialFactory.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/BackOffice/CasaComercial/CasaComercialController.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Comunes/Pais/PaisModule.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/Pais/PaisFactory.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Comunes/Pais/PaisController.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Base/js/ModalEliminarController.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Base/js/jquery.ez-plus.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/Base/js/Zoom.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="~/App/Base/js/underscore.js" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="18" />

