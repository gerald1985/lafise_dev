﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Sucursal.ascx.cs" Inherits="Sucursal" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" ng-app="SucursalModule">
    <script type="text/ng-template" id="modalEliminar.html">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <div class="contenido">¿Esta seguro que desea eliminar la sucursal?<div>           
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
    </script>
    <div data-ng-controller="SucursalController">
        <!-- Filtro Pais -->
        <div class="lafise-group">
            <div data-ng-controller="PaisController">
                <output class="lafise-label">Pais</output>
                <ui-select multiple data-ng-model="paisesSelected.Paises" theme="select2" data-ng-disabled="paises.length == 1" style="width: 300px;" data-ng-change="mostrarDepartamentoFiltro()">
                    <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                    <ui-select-choices repeat="pais.Id as pais in paises | filter:$select.search" >
                    {{pais.Nombre}}
                    </ui-select-choices>
                </ui-select>
            </div>
        </div>
        <!-- Filtro Departamento -->
        <div class="lafise-group">
            <div>
                <output class="lafise-label">Zona/Departamento</output>
                <ui-select multiple data-ng-model="departamentoSelected.Departamento" theme="select2" data-ng-disabled="disabled" style="width: 300px;" data-ng-change="mostrarCiudadFiltro()">
                    <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                    <ui-select-choices repeat="departamento.Id as departamento in departamentoPais | filter:$select.search">
                    {{departamento.Nombre}}
                    </ui-select-choices>
                </ui-select>
            </div>
        </div>
        <!-- Filtro Ciudad -->
        <div class="lafise-group">
            <div>
                <output class="lafise-label">Ciudad</output>
                <ui-select multiple data-ng-model="ciudadSelected.Ciudad" theme="select2" data-ng-disabled="disabled" style="width: 300px;">
                    <ui-select-match placeholder="Seleccione">{{$item.Nombre}}</ui-select-match>
                    <ui-select-choices repeat="ciudad.Id as ciudad in ciudadDepartamento | filter:$select.search">
                    {{ciudad.Nombre}}
                    </ui-select-choices>
                </ui-select>
            </div>
        </div>
        <!-- Filtro Tipo Sucursal -->
        <div class="lafise-group">
            <output class="lafise-label">Tipo Sucursal</output>
            <select class="lafise-select" id="ddTipoSucursal" data-ng-options="item.Id as item.TipoSucursal1 for item in tipoSucursalActivos" data-ng-model="tipoSucursalSelected.TipoSucursal">
            </select>
        </div>
        <!-- Filtro Nombre de la Sucursal -->
        <div class="lafise-group">
            <output class="lafise-label">Nombre de la Sucursal</output>
            <input type="text" class="lafise-text" data-ng-model="nombreSucursalSelected" />
        </div>
        <div class="lafise-options">
            <!-- Botones Buscar, Crear y Exportar -->
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="getData()" />
            </div>
            <div class="lafise-group">
                <input type="button" value="Crear Sucursal" class="lafise-button" data-ng-click="toggleCreate()" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <!-- Grid -->
        <div id="gridSucursal" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
        </div>
        <!-- Información del grid a exportar -->
        <div id="exportable" style="display: none">
            <table width="80%">
                <thead>
                    <tr>
                        <th>País</th>
                        <th>Zona/Departamento</th>
                        <th>Ciudad</th>
                        <th>Tipo Sucursal</th>
                        <th>Nombre de la Sucursal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in sucursal">
                        <td align="left">{{item.NombrePais}}</td>
                        <td align="left">{{item.NombreDepartamento}}</td>
                        <td align="left">{{item.NombreCiudad}}</td>
                        <td align="left">{{item.TipoSucursal}}</td>
                        <td align="left">{{item.NombreSucursal}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lafise-group">
            <ng-form data-ng-show="formMode" name="addSucursal" novalidate>
                <!-- Crear Sucursar -->
                <div class="lafise-group">
                    <div class="lafise-container-etiqueta">
                        <output class="lafise-label">Sucursal</output>
                    </div>
                </div>
                <!-- Seleccionar Pais -->
                <div class="lafise-group">
                    <div data-ng-controller="PaisController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Pa&iacute;s</output>
                        </div>
                        <div class="lafise-container-select">
                            <select
                                id="crearPais"
                                name="pais"
                                class="lafise-select"
                                data-ng-class="{submitted:addSucursal.submitted}"
                                data-ng-model="newPais.IdPais"
                                data-ng-options="pais.Id as pais.Nombre for pais in paises"
                                data-ng-selected="data.unit == item.id"
                                data-ng-change="mostrarDepartamento()"
                                data-ng-disabled="paises.length == 1"
                                required>
                                <option value=''>Seleccione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Seleccionar Departamento -->
                <div class="lafise-group">
                    <div data-ng-controller="DepartamentoController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Zona/Departamento</output>
                        </div>
                        <div class="lafise-container-select">
                            <select
                                id="crearDepartamento"
                                name="departamento"
                                class="lafise-select"
                                data-ng-class="{submitted:addSucursal.submitted}"
                                data-ng-model="newSucursal.IdDepartamento"
                                data-ng-options="departamentoCreated.Id as departamentoCreated.Nombre for departamentoCreated in departamentosPais"
                                data-ng-selected="data.unit == item.id"
                                data-ng-change="mostrarCiudad()"
                                required>
                                <option value=''>Seleccione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Seleccionar Ciudad -->
                <div class="lafise-group">
                    <div data-ng-controller="CiudadController">
                        <div class="lafise-container-label">
                            <output class="lafise-label">Ciudad</output>
                        </div>
                        <div class="lafise-container-select">
                            <select
                                id="crearCiudad"
                                name="ciudad"
                                class="lafise-select"
                                data-ng-class="{submitted:addSucursal.submitted}"
                                data-ng-model="newSucursal.IdCiudad"
                                data-ng-options="ciudadCreated.Id as ciudadCreated.Nombre for ciudadCreated in ciudadDepartamentos"
                                data-ng-selected="data.unit == item.id"
                                required>
                                <option value=''>Seleccione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Seleccionar Tipo Sucursal -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Tipo Sucursal</output>
                    </div>
                    <div class="lafise-container-select">
                        <select id="ddlTipoSucursalCrear"
                            name="tipoSucursal"
                            class="lafise-select"
                            data-ng-class="{submitted:addSucursal.submitted}"
                            data-ng-model="newSucursal.IdTipoSucursal"
                            data-ng-options="item.Id as item.TipoSucursal1 for item in tipoSucursalActivos"
                            data-ng-selected="data.unit == item.id"
                            required>
                            <option value=''>Seleccione</option>
                        </select>
                    </div>
                </div>
                <!-- Nombre de la Sucursal -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Nombre de la sucursal</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text"
                            data-ng-class="{submitted:addSucursal.submitted}"
                            class="lafise-text"
                            data-ng-model="newSucursal.Nombre"
                            data-ng-blur="UniqueName();" maxlength="300" required />
                    </div>
                </div>
                <!-- Botones Actualizar, Guardar y Cancelar -->
                <div class="lafise-container-options">
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" data-ng-click="update()" value="Actualizar" data-ng-show="editMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-click="add()" data-ng-show="addMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />

<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/angular-sanitize.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/select.min.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/Comunes/Pais/PaisModule.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/Comunes/Pais/PaisFactory.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/Comunes/Pais/PaisController.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/Comunes/Departamento/DepartamentoModule.js" Priority="10" />
<dnn:DnnJsInclude ID="DnnJsInclude11" runat="server" FilePath="~/App/Comunes/Departamento/DepartamentoFactory.js" Priority="11" />
<dnn:DnnJsInclude ID="DnnJsInclude12" runat="server" FilePath="~/App/Comunes/Departamento/DepartamentoController.js" Priority="12" />
<dnn:DnnJsInclude ID="DnnJsInclude13" runat="server" FilePath="~/App/Comunes/Ciudad/CiudadModule.js" Priority="13" />
<dnn:DnnJsInclude ID="DnnJsInclude14" runat="server" FilePath="~/App/Comunes/Ciudad/CiudadFactory.js" Priority="14" />
<dnn:DnnJsInclude ID="DnnJsInclude15" runat="server" FilePath="~/App/Comunes/Ciudad/CiudadController.js" Priority="15" />
<dnn:DnnJsInclude ID="DnnJsInclude16" runat="server" FilePath="~/App/BackOffice/TipoSucursal/TipoSucursalModule.js" Priority="16" />
<dnn:DnnJsInclude ID="DnnJsInclude17" runat="server" FilePath="~/App/BackOffice/TipoSucursal/TipoSucursalFactory.js" Priority="17" />
<dnn:DnnJsInclude ID="DnnJsInclude18" runat="server" FilePath="~/App/BackOffice/TipoSucursal/TipoSucursalController.js" Priority="18" />
<dnn:DnnJsInclude ID="DnnJsInclude19" runat="server" FilePath="~/App/BackOffice/Sucursal/SucursalModule.js" Priority="19" />
<dnn:DnnJsInclude ID="DnnJsInclude20" runat="server" FilePath="~/App/BackOffice/Sucursal/SucursalFactory.js" Priority="20" />
<dnn:DnnJsInclude ID="DnnJsInclude21" runat="server" FilePath="~/App/BackOffice/Sucursal/SucursalController.js" Priority="21" />
<dnn:DnnJsInclude ID="DnnJsInclude22" runat="server" FilePath="~/App/Base/js/ModalEliminarController.js" Priority="22" />
<dnn:DnnJsInclude ID="DnnJsInclude23" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="23" />
