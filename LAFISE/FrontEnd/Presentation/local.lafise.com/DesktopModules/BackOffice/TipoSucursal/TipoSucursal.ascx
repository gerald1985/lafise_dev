﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TipoSucursal.ascx.cs" Inherits="TipoSucursal" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link href="../../../App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" ng-app="TipoSucursalModule">
    <!-- Plantilla de la ventana modal al eliminar tipo sucursal -->
    <script type="text/ng-template" id="modalEliminar.html">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <div class="contenido">¿Esta seguro que desea eliminar el tipo de sucursal?<div>           
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
        </div>
    </script>
    <div data-ng-controller="TipoSucursalController">
        <!-- Filtro Tipo Sucursal -->
        <div class="lafise-group">
            <output class="lafise-label">Tipo Sucursal</output>
            <input type="text" class="lafise-text" data-ng-model="tipoSucursalSelected.TipoSucursal" />
        </div>
        <!-- Botones Buscar, Crear y Exportar -->
        <div class="lafise-options">
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="getData()" />
            </div>
            <div class="lafise-group">
                <input type="button" value="Crear Tipo Sucursal" class="lafise-button" data-ng-click="toggleCreate()" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <!-- Grid -->
        <div id="gridTipoSucursal" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
        </div>
        <!-- Información del grid a exportar -->
        <div id="exportable" style="display: none">
            <table width="80%">
                <thead>
                    <tr>
                        <th>Tipo Sucursal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in tipoSucursal">
                        <td align="left">{{item.TipoSucursal1}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lafise-group">
            <ng-form data-ng-show="formMode" name="addTipoSucursal" novalidate>
                  <!-- Crear Tipo sucursal -->
                <div class="lafise-container-label">
                    <output class="lafise-label">Tipo Sucursal</output>
               </div>
                <div class="lafise-container-text">
                      <input type="text" data-ng-class="{submitted:addTipoSucursal.submitted}" class="lafise-text" data-ng-model="newTipoSucursal.TipoSucursal1" data-ng-blur="UniqueName();" maxlength="300" required />
                </div>
                 <!-- Botones Actualizar, Guardar y Cancelar -->   
                <div class="lafise-container-options">
                     <div class="lafise-group">
                        <input type="button" class="lafise-button" data-ng-click="update()" value="Actualizar" data-ng-show="editMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Guardar" data-ng-click="add()" data-ng-show="addMode" />
                    </div>
                    <div class="lafise-group">
                        <input type="button" class="lafise-button" value="Cancelar" data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
        </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert data-ng-show="showMsg" data-ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Modules/LafiseModule.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/BackOffice/TipoSucursal/TipoSucursalModule.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/BackOffice/TipoSucursal/TipoSucursalFactory.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/BackOffice/TipoSucursal/TipoSucursalController.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/Base/js/ModalEliminarController.js" Priority="9" />
<dnn:dnnjsinclude id="DnnJsInclude10" runat="server" filepath="~/App/Base/js/VentanaCargando.js" priority="10" />