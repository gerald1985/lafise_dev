﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Tarjeta.ascx.cs" Inherits="Tarjeta" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement"
    Assembly="DotNetNuke.Web.Client" %>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/ng-grid/2.0.11/ng-grid.min.css" rel="stylesheet" />
<link href="//cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/3.0.7/ui-grid.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link href="/App/Base/css/grid.css" rel="stylesheet" />

<div class="lafise-container" ng-app="TarjetaModule">

    <div data-ng-controller="TarjetaController">
        <!-- Plantilla de la ventana modal al eliminar tarjeta -->
        <script type="text/ng-template" id="modalEliminar.html">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <div class="contenido">¿Esta seguro que desea eliminar la tarjeta de crédito?<div>           
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" ng-click="ok()">Aceptar</button>
                <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
            </div>
        </script>
        <!-- Filtro tarjeta crédito -->
        <div class="lafise-group">
            <output class="lafise-label">Tarjeta Crédito</output>
            <input type="text" class="lafise-text" data-ng-model="TarjetaSelected.TarjetaCredito" />
        </div>
        <!-- Botones Buscar, Crear y Exportar -->
        <div class="lafise-container-options">
            <div class="lafise-group">
                <input type="button" class="lafise-button" value="Buscar" data-ng-click="getData()" data-ng-model="searchText" />
            </div>
            <div class="lafise-group">
                <input type="button" value="Crear Tarjeta crédito" class="lafise-button" data-ng-click="toggleCreate()" />
            </div>
            <div class="lafise-group">
                <button data-ng-click="exportData()">Exportar</button>
            </div>
        </div>
        <!-- Grid -->
        <div id="gridTarjeta" class="gridStyle lafise-grid" ui-grid-pagination ui-grid="gridOpts">
            <div class="watermark" data-ng-show="!gridOpts.data.length">No existen registros que coincidan con la búsqueda.</div>
        </div>
        <!-- Información del grid a exportar -->
        <div id="exportable" style="display: none">
            <table width="80%">
                <thead>
                    <tr>
                        <th>Tarjera Crédito</th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="item in tarjeta">
                        <td align="left">{{item.TarjetaCredito}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lafise-group">
            <ng-form ng-show="formMode" name="addTarjeta" novalidate>
                  <!-- Crear tarjeta crédito -->
                <div class="lafise-group">
                    <div class="lafise-container-label">
                        <output class="lafise-label">Tarjeta Crédito</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text" data-ng-class="{submitted:addTarjeta.submitted}" class="lafise-text" name="tarjeta" data-ng-model="newTarjeta.TarjetaCredito" data-ng-blur="UniqueName();" maxlength="300" required />
                    </div>
                </div>
                <div class="lafise-group examinar"> 
                    <div class="lafise-container-label">
                        <output class="lafise-label">Imagen</output>
                    </div>
                    <div class="lafise-container-text">
                        <input type="text" data-ng-class="{submitted:addTarjeta.submitted}" class="lafise-text" name="imagen" data-ng-model="newTarjeta.Imagen"  maxlength="500" data-ng-disabled="true" required  />
                        <button id="file" type="file" data-ngf-select="uploadFiles($file, $invalidFiles)" accept="image/*" ngf-validate="{size: {min: 0, max: '10MB'},                  pattern: '.jpg,.gif,.png,.jpeg'}" ngf-max-size="1 MB" >Examinar</button>  
                        <div id="info" name="fileInvalid" style="font: smaller"  data-ng-model="newTarjeta.Imagen">{{tarjetaImagen.errorPeso}} {{tarjetaImagen.errorFormato}} </div>
                    </div>
                </div>
                 <!-- Botones Actualizar, Guardar y Cancelar -->  
                <div class="lafise-container-options">
                     <div class="lafise-group">
                             <input type="button" class="lafise-button" data-ng-click="update()" value="Actualizar" data-ng-show="editMode" />
                     </div>
                     <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Guardar" data-ng-click="add()" data-ng-show="addMode" />
                     </div>
                    <div class="lafise-group">
                            <input type="button" class="lafise-button" value="Cancelar"  data-ng-click="cancel()" data-ng-show="formMode" />
                    </div>
                </div>
            </ng-form>
        </div>
        <div class="loading-spiner-holder" data-loading>
            <div class="loading-spiner">
                <img src="/images/ajax-loader.gif" />Cargando
            </div>
        </div>
        <!-- Alerta -->
        <uib-alert ng-show="showMsg" ng-repeat="alert in alerts" type="{{::alert.type}}" close="closeAlert($index)" ng-bind="alert.msg"></uib-alert>
    </div>
</div>

<dnn:DnnJsInclude ID="DnnJsInclude1" runat="server" FilePath="~/App/Base/js/angular.min.js" Priority="1" />
<dnn:DnnJsInclude ID="DnnJsInclude2" runat="server" FilePath="~/App/Base/js/ng-file-upload-all.min.js" Priority="2" />
<dnn:DnnJsInclude ID="DnnJsInclude3" runat="server" FilePath="~/App/Base/js/ui-bootstrap-tpls.min.js" Priority="3" />
<dnn:DnnJsInclude ID="DnnJsInclude4" runat="server" FilePath="~/App/Base/js/ui-grid.min.js" Priority="4" />
<dnn:DnnJsInclude ID="DnnJsInclude5" runat="server" FilePath="~/App/Base/js/FileSaver.js" Priority="5" />
<dnn:DnnJsInclude ID="DnnJsInclude6" runat="server" FilePath="~/App/BackOffice/Tarjeta/TarjetaModule.js" Priority="6" />
<dnn:DnnJsInclude ID="DnnJsInclude7" runat="server" FilePath="~/App/BackOffice/Tarjeta/TarjetaFactory.js" Priority="7" />
<dnn:DnnJsInclude ID="DnnJsInclude8" runat="server" FilePath="~/App/BackOffice/Tarjeta/TarjetaController.js" Priority="8" />
<dnn:DnnJsInclude ID="DnnJsInclude9" runat="server" FilePath="~/App/Base/js/ModalEliminarController.js" Priority="9" />
<dnn:DnnJsInclude ID="DnnJsInclude10" runat="server" FilePath="~/App/Base/js/VentanaCargando.js" Priority="10" />
