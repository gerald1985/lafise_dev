﻿<%@ WebHandler Language="C#" Class="UploadTarjetaHandler" %>

using System;
using System.Web;
using LAFISE.Common.FrontEnd;

public class UploadTarjetaHandler : IHttpHandler
{
    public LAFISE.Business.IParametrosBusiness _ParametrosBusiness = new LAFISE.Business.ParametrosBusiness();
    public void ProcessRequest(HttpContext context)
    {
        var ruta = _ParametrosBusiness.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_TARJETA);
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                var rimagen = ruta.Result.SuccessfulOperation ? ruta.Result.Result : string.Empty;
                string fname = context.Server.MapPath(rimagen + file.FileName);
                file.SaveAs(fname);
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("File/s uploaded successfully!");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}