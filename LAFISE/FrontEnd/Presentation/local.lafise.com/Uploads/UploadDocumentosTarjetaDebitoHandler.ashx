﻿<%@ WebHandler Language="C#" Class="UploadDocumentosTarjetaDebitoHandler" %>

using System;
using System.Web;
using LAFISE.Common.FrontEnd;
using LAFISE.Business;

public class UploadDocumentosTarjetaDebitoHandler : IHttpHandler
{
    public IParametrosBusiness _ParametrosBusiness = new ParametrosBusiness();
    public void ProcessRequest(HttpContext context)
    {
        string rDoc = string.Empty;
        string fname = string.Empty;
        string filename = string.Empty;
        string rutafisica = string.Empty;
        var ruta = _ParametrosBusiness.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_FORM_TARJETA_DEBITO);
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                rDoc = ruta.Result.SuccessfulOperation ? ruta.Result.Result : string.Empty;
                filename = file.FileName;
                fname = context.Server.MapPath(rDoc + filename);
                rutafisica = HttpUtility.JavaScriptStringEncode(context.Server.MapPath(rDoc));
                file.SaveAs(fname);
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("{\"status\":\"ok\",\"ruta\":\"" + rDoc + "\", \"filename\": \"" + rutafisica + "\" }");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}