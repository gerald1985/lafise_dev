﻿<%@ WebHandler Language="C#" Class="UploadModelosHandler" %>

using System;
using System.Web;
using LAFISE.Common.FrontEnd;
using LAFISE.Business;
using System.Drawing;
using System.Drawing.Drawing2D;

public class UploadModelosHandler : IHttpHandler
{
    public IParametrosBusiness _ParametrosBusiness = new ParametrosBusiness();
    public void ProcessRequest(HttpContext context)
    {
        var ruta = _ParametrosBusiness.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_MODELOS);
        var rutaThumb = _ParametrosBusiness.ObtenerParametroRuta(Constants.RUTA_ARCHIVOS_THUMB_MODELOS);
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                var nombreArchivo = context.Request.Form["NombreArchivo"];
                var rimagen = ruta.Result.SuccessfulOperation ? ruta.Result.Result : string.Empty;
                var rimgenthumb = rutaThumb.Result.SuccessfulOperation ? rutaThumb.Result.Result : string.Empty;
                string fname = context.Server.MapPath(rimagen + nombreArchivo);
                string fnamethumb = context.Server.MapPath(rimgenthumb + nombreArchivo);


                ResizeImagen(file, fname, 1024, 768);
                ResizeImagen(file, fnamethumb, 100, 100);
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("File/s uploaded successfully!");
    }

    public void ResizeImagen(HttpPostedFile Original, string Path, int Width, int Height)
    {
        Bitmap NuevaImagen = new Bitmap(Width, Height);
        Image image = Image.FromStream(Original.InputStream);

        using (Graphics gr = Graphics.FromImage(NuevaImagen))
        {
            gr.SmoothingMode = SmoothingMode.HighQuality;
            gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
            gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
            gr.DrawImage(image, new Rectangle(0, 0, Width, Height));
        }
        NuevaImagen.Save(Path);
        NuevaImagen.Dispose();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}