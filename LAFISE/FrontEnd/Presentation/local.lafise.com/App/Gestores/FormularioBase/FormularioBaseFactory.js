﻿angular.module("FormularioBaseModule").factory("FormularioBaseFactory", function ($http, $q) {

    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var obtenerSolicitud = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Solicitud/VerFormularioGeneralPorIdSolicitud?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud con referencias por id
    //Parametro id: Identificador de la solicitud
    var obtenerSolicitudReferencias = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Solicitud/VerFormularioConReferenciasPorIdSolicitud?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud seguro de vida por id
    //Parametro id: Identificador de la solicitud
    var obtenerSolicitudSeguroVida = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Solicitud/VerFormularioSeguroVidaPorIdSolicitud?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud seguro vehicular por id
    //Parametro id: Identificador de la solicitud
    var obtenerSolicitudSeguroVehicular = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Solicitud/VerFormularioSeguroVehicularPorIdSolicitud?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }



    //Función encargada de consumir el servicio para la consulta de solicitud de reclamos por id
    //Parametro id: Identificador de la solicitud
    var obtenerSolicitudReclamos = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Solicitud/VerFormularioReclamosPorIdSolicitud?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }



    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("Ha ocurrido un error desconocido");
        }
        return $q.reject(response.data.message);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerSolicitud: obtenerSolicitud,
        obtenerSolicitudReferencias: obtenerSolicitudReferencias,
        obtenerSolicitudSeguroVida: obtenerSolicitudSeguroVida,
        obtenerSolicitudReclamos: obtenerSolicitudReclamos,
        obtenerSolicitudSeguroVehicular: obtenerSolicitudSeguroVehicular
    });


});