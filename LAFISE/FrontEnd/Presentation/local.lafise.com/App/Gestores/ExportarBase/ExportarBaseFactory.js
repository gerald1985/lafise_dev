﻿angular.module("ExportarBaseModule").factory("ExportarBaseFactory", function ($http, $q) {

    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarMarcaAuto = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarMarcaAuto"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarPrestamoPersonal = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarPrestamoPersonal"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarPrestamoHipotecario = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarPrestamoHipotecario"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarTarjetaCreditoDebito = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarTarjetaCreditoDebito"
        });
        return result.then(handleSuccess, handleError);
    }



    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarPrestamoEducativo = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarPrestamoEducativo"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarAperturaCuenta = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarAperturaCuenta"
        });
        return result.then(handleSuccess, handleError);
    }



    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarSeguroVidaAccidente = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarSeguroVidaAccidente"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarSeguroVehicular = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarSeguroVehicular"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de solicitud por id
    //Parametro id: Identificador de la solicitud
    var ObtenerExportarReclamos = function (FiltroSolicitudExportar) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitudExportar,
            url: appContext + "/Solicitud/CargarExportarReclamos"
        });
        return result.then(handleSuccess, handleError);
    }











    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("Ha ocurrido un error desconocido");
        }
        return $q.reject(response.data.message);
    }

    //Retorna cada una de las funciones del factory
    return ({
        ObtenerExportarMarcaAuto: ObtenerExportarMarcaAuto,
        ObtenerExportarPrestamoPersonal: ObtenerExportarPrestamoPersonal,
        ObtenerExportarPrestamoHipotecario: ObtenerExportarPrestamoHipotecario,
        ObtenerExportarTarjetaCreditoDebito: ObtenerExportarTarjetaCreditoDebito,
        ObtenerExportarPrestamoEducativo: ObtenerExportarPrestamoEducativo,
        ObtenerExportarAperturaCuenta: ObtenerExportarAperturaCuenta,
        ObtenerExportarSeguroVidaAccidente: ObtenerExportarSeguroVidaAccidente,
        ObtenerExportarSeguroVehicular: ObtenerExportarSeguroVehicular,
        ObtenerExportarReclamos: ObtenerExportarReclamos
    });


});