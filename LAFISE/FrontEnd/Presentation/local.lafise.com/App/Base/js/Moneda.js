﻿angular.module('FormatoMoneda', []).directive('moneda', function ($compile, $timeout) {
    return {
        scope: {
            //simbolo: '@',
            requerido: "@"
        },
        controller: function ($scope, $attrs, $element, $compile, $timeout) {
            $scope.init = function () {
                $scope.$watch('requerido', function (newValue) {
                    if ($scope.requerido != undefined) {
                        if (eval($scope.requerido)) {
                            $element.addClass("submitted")
                        }
                        else {
                            $element.removeClass("submitted")
                        }
                    }
                    setTimeout(function () {
                        //if ($scope.simbolo != undefined) {
                        $element.autoNumeric('init', {
                            wEmpty: 'sign'
                        });
                        //}
                    }, 500);
                }, true);

            };
            $scope.init();
        },
        replace: true,
        template: '<input type="text" class="formato lafise-text"  />'
    };
});