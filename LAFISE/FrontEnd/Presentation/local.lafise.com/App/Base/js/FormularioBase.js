﻿angular.module('FormularioBaseModule').directive('formulariobase', function ($compile, $timeout,$filter, FormularioBaseFactory) {
    return {
        restrict: 'EA',
        scope: {
            solicitud: "=",
            esmarcaauto: "@",
            eshipotecario: "@",
            espersonal: "@",
            estarjetacredito: "@",
            estarjetadebito: "@",
            eseducativo: "@",
            esaperturacuenta: "@",
            essegurovida: "@",
            essegurovehicular: "@",
            esreclamos: "@"
        },
        link: function ($scope, $attrs, $element, $compile, $timeout) {
            $scope.$watch('solicitud', function (Value) {

                if (Value) {
                    $scope.Obj = Value;
                    $scope.Obj.invoke = function () {
                        $scope.setForm($scope.Obj.IdSolicitud);
                    }
                }
            }, true);
            $scope.setForm = function (IdSolicitud) {

                $scope.showFilesNP = false;
                $scope.showFilesI = false;
                $scope.showFilesNPCodeudor = false;
                $scope.showFilesICodeudor = false;
                $scope.showFilesCM = false;
                $scope.esmarcaauto = eval($scope.esmarcaauto);
                $scope.eshipotecario = eval($scope.eshipotecario);
                $scope.espersonal = eval($scope.espersonal);
                $scope.estarjetacredito = eval($scope.estarjetacredito);
                $scope.estarjetadebito = eval($scope.estarjetadebito);
                $scope.eseducativo = eval($scope.eseducativo);
                $scope.esaperturacuenta = eval($scope.esaperturacuenta);
                $scope.essegurovida = eval($scope.essegurovida);
                $scope.essegurovehicular = eval($scope.essegurovehicular);
                $scope.esreclamos = eval($scope.esreclamos);
                $scope.Soportes = [];
                $scope.Referencias = [];
                $scope.ActividadEconomica = [];

                //Lista de los datos personales
                $scope.newDatosPersonales = {
                    Nombre: '',
                    Apellidos: '',
                    Telefono: '',
                    Email: '',
                    Genero: '',
                    Nacionalidad: '',
                    CiudadResidencia: '',
                    TipoIdentificacion: '',
                    NumeroIdentificacion: '',
                    Profesion: '',
                    EsCodeudor: false
                };

                //Lista de los datos personales
                $scope.newDatosPersonalesCodeudor = {
                    Nombre: '',
                    Apellidos: '',
                    Telefono: '',
                    Email: '',
                    Genero: '',
                    Nacionalidad: '',
                    CiudadResidencia: '',
                    TipoIdentificacion: '',
                    NumeroIdentificacion: '',
                    Profesion: '',
                    EsCodeudor: false
                };


                //Lista de los datos financieros
                $scope.newDatosFinancieros = {
                    EsCliente: false,
                    PoseeNomina: false,
                    NegocioPropio: false,
                    Asalariado: false,
                    PromedioIngresoSelected: '',
                    PromedioIngresoMensual: '',
                    EstadoCuenta: '',
                    Direccion: '',
                    EsZurdoNombre: '',
                    Estatura: '',
                    Peso: '',
                    ActividadEconomica: [],
                    Sucursal: ''

                };


                //Lista del datos del auto
                $scope.newDatosAuto = {
                    ValorBien: '',
                    TipoVehiculo: '',
                    MarcaAuto: '',
                    Modelo: '',
                    TipoBien: ''
                };

                //Lista de siniestros
                $scope.newSiniestros = {
                    NumeroPoliza: '',
                    Fecha: '',
                    Detalle: '',
                    SiniestroCiudad: '',
                    SiniestroPais: ''
                };


                //Lista del plan de financiamiento
                $scope.newPlanFinanciamiento = {
                    ValorBien: '',
                    MontoFinanciar: '',
                    PrimaAnticipo: '',
                    Plazo: '',
                    AutoUsado: false,
                    OpcionOtro: false,
                    Categoria: '',
                    MarcaAuto: '',
                    Modelo: '',
                    CasaComercial: '',
                    OtroMarcaAuto: '',
                    OtroModelo: '',
                    OtroCasaComercial: '',
                    selectedItem: '',
                    ValorBienFormato: '',
                    MontoFinanciarFormato: '',
                    PrimaAnticipoFormato: '',
                    MinimoPrima: '',
                    TipoPropiedad: '',
                    ConstruccionPropia: false,
                    Urbanizadora: '',
                    Garantia: ''
                };


                $scope.newHistoricoComentarioProceso =
                    {
                        IdEstado: '',
                        Estado: '',
                        Comentario: ''
                    }


                $scope.newHistoricoComentarioTerminados =
                    {
                        IdEstado: '',
                        Estado: '',
                        Comentario: '',
                        TipoContacto: ''
                    }


                //Servicio para obtener la solicitud según el id
                FormularioBaseFactory.obtenerSolicitud(IdSolicitud).then(function (response) {
                    //Set Datos Personales.
                    $scope.newDatosPersonales.Nombre = response.DatosPersonales[0].Nombre;
                    $scope.newDatosPersonales.Apellidos = response.DatosPersonales[0].Apellidos;
                    $scope.newDatosPersonales.Telefono = response.DatosPersonales[0].Telefono;
                    $scope.newDatosPersonales.Email = response.DatosPersonales[0].Email;
                    $scope.newDatosPersonales.Genero = response.DatosPersonales[0].Genero;
                    $scope.newDatosPersonales.Nacionalidad = response.DatosPersonales[0].Nacionalidad;
                    $scope.newDatosPersonales.CiudadResidencia = response.DatosPersonales[0].CiudadResidencia;
                    $scope.newDatosPersonales.TipoIdentificacion = response.DatosPersonales[0].TipoIdentificacion;
                    $scope.newDatosPersonales.NumeroIdentificacion = response.DatosPersonales[0].NumeroIdentificacion;
                    $scope.newDatosPersonales.Profesion = response.DatosPersonales[0].Profesion;
                    if (response.DatosPersonales[1] != undefined) {
                        $scope.newDatosPersonales.EsCodeudor = response.DatosPersonales[1].EsCodeudor;
                        if ($scope.newDatosPersonales.EsCodeudor) {
                            $scope.newDatosPersonalesCodeudor.Nombre = response.DatosPersonales[1].Nombre;
                            $scope.newDatosPersonalesCodeudor.Apellidos = response.DatosPersonales[1].Apellidos;
                            $scope.newDatosPersonalesCodeudor.Telefono = response.DatosPersonales[1].Telefono;
                            $scope.newDatosPersonalesCodeudor.Email = response.DatosPersonales[1].Email;
                            $scope.newDatosPersonalesCodeudor.Genero = response.DatosPersonales[1].Genero;
                            $scope.newDatosPersonalesCodeudor.Nacionalidad = response.DatosPersonales[1].Nacionalidad;
                            $scope.newDatosPersonalesCodeudor.CiudadResidencia = response.DatosPersonales[1].CiudadResidencia;
                            $scope.newDatosPersonalesCodeudor.TipoIdentificacion = response.DatosPersonales[1].TipoIdentificacion;
                            $scope.newDatosPersonalesCodeudor.NumeroIdentificacion = response.DatosPersonales[1].NumeroIdentificacion;
                            $scope.newDatosPersonalesCodeudor.Profesion = response.DatosPersonales[1].Profesion;
                            $scope.newDatosPersonalesCodeudor.EsCodeudor = response.DatosPersonales[1].EsCodeudor;
                        }
                    }

                    //Set Estados Bandeja Procesos
                    if ($scope.Obj.bandejaProceso || $scope.Obj.bandejaTerminados) {
                        $scope.newHistoricoComentarioProceso.Estado = response.HistoricoComentario[0].Estado;
                        $scope.newHistoricoComentarioProceso.Comentario = response.HistoricoComentario[0].Comentario;
                    }

                    //Set Estados Bandeja Terminados
                    if ($scope.Obj.bandejaTerminados) {
                        $scope.newHistoricoComentarioTerminados.TipoContacto = response.TipoContacto;
                        $scope.newHistoricoComentarioTerminados.Estado = response.HistoricoComentario[1] != undefined ? response.HistoricoComentario[1].Estado : response.HistoricoComentario[0].Estado;
                        $scope.newHistoricoComentarioTerminados.Comentario = response.HistoricoComentario[1] != undefined ? response.HistoricoComentario[1].Comentario : response.HistoricoComentario[0].Comentario;
                    }


                    if (!$scope.esreclamos) {
                        //Set Datos Financieros.
                        $scope.newDatosFinancieros.EsCliente = response.DatosFinancieros[0].EsCliente;
                        $scope.newDatosFinancieros.PoseeNomina = response.DatosFinancieros[0].PoseeNomina;
                        $scope.newDatosFinancieros.NegocioPropio = response.DatosFinancieros[0].NegocioPropio;
                        $scope.newDatosFinancieros.Asalariado = response.DatosFinancieros[0].Asalariado;
                        $scope.newDatosFinancieros.PromedioIngresoMensual = response.DatosFinancieros[0].PromedioIngresoMensualNombre;
                        $scope.newDatosFinancieros.EstadoCuenta = response.DatosFinancieros[0].EstadoCuentaNombre;
                        $scope.newDatosFinancieros.Direccion = response.DatosFinancieros[0].Direccion;
                        $scope.newDatosFinancieros.EsZurdoNombre = response.DatosFinancieros[0].EsZurdoNombre;
                        $scope.newDatosFinancieros.Estatura = response.DatosFinancieros[0].Estatura;
                        $scope.newDatosFinancieros.Peso = response.DatosFinancieros[0].Peso;
                        $scope.newDatosFinancieros.Sucursal = response.DatosFinancieros[0].Sucursal;
                    }


                    //Set Adjuntos
                    angular.forEach(response.Soportes, function (value, key) {
                        $scope.Soportes.push({ Nombre: value.Nombre, Ruta: value.Ruta, Tipo: value.IdTipoSoporte,EsCodeudor:value.EsCodeudor });
                    });


                    if ($scope.estarjetacredito || $scope.estarjetadebito || $scope.esaperturacuenta) {
                        //Set Referencias
                        FormularioBaseFactory.obtenerSolicitudReferencias(IdSolicitud).then(function (response) {
                            angular.forEach(response.Referencias, function (value, key) {
                                $scope.Referencias.push({ Nombre: value.Nombre, Apellidos: value.Apellidos, Telefono: value.Telefono });
                            });
                        });
                    }
                    else if ($scope.essegurovida) {
                        //Set Datos financieros 
                        FormularioBaseFactory.obtenerSolicitudSeguroVida(IdSolicitud).then(function (response) {
                            angular.forEach(response.DatosFinancieros[0].ActividadEconomica, function (value, key) {
                                $scope.newDatosFinancieros.ActividadEconomica.push({ IdTipoActividad: value.IdTipoActividad, TipoActividad: value.TipoActividad, OtraActividad: value.OtraActividad });
                            });
                        });
                    }
                    else if ($scope.essegurovehicular) {
                        //Set Datos Auto 
                        FormularioBaseFactory.obtenerSolicitudSeguroVehicular(IdSolicitud).then(function (response) {
                            $('#valorbienformatoauto').autoNumeric('init', { aSign: response.DatosBien[0].Simbolo + ' ' });
                            $scope.newDatosAuto.ValorBien = $('#valorbienformatoauto').autoNumeric('set', response.DatosBien[0].ValorBien == null ? 0 : response.DatosBien[0].ValorBien)[0].value;
                            $scope.newDatosAuto.TipoVehiculo = response.DatosBien[0].TipoVehiculo;
                            $scope.newDatosAuto.MarcaAuto = response.DatosBien[0].MarcaAuto;
                            $scope.newDatosAuto.Modelo = response.DatosBien[0].Modelo;
                            $scope.newDatosAuto.TipoBien = response.DatosBien[0].TipoBien;
                        });
                    }
                    else if ($scope.esreclamos) {
                        //Set Registro siniestro 
                        FormularioBaseFactory.obtenerSolicitudReclamos(IdSolicitud).then(function (response) {
                            $scope.newSiniestros.NumeroPoliza = response.Siniestro[0].NumeroPoliza;
                            $scope.newSiniestros.Fecha = response.Siniestro[0].Fecha;
                            $scope.newSiniestros.Detalle = response.Siniestro[0].Detalle;
                            $scope.ultimaciudad = response.Siniestro[0].SiniestroCiudad.length - 1;
                            $scope.ultimopais = response.Siniestro[0].SiniestroPais.length - 1;
                            angular.forEach(response.Siniestro[0].SiniestroCiudad, function (value, key) {
                                $scope.newSiniestros.SiniestroCiudad += (key == $scope.ultimaciudad) ? value.Ciudad + '' : value.Ciudad + ',';
                            });
                            angular.forEach(response.Siniestro[0].SiniestroPais, function (value, key) {
                                $scope.newSiniestros.SiniestroPais += (key == $scope.ultimopais) ? value.Pais + '' : value.Pais + ',';
                            });
                        });
                    }
                    else {
                        //Set Plan Financiamiento
                        $('#valorbienformato').autoNumeric('init', { aSign: response.PlanFinanciacion[0].Simbolo + ' ' });
                        $scope.newPlanFinanciamiento.ValorBien = $('#valorbienformato').autoNumeric('set', response.PlanFinanciacion[0].ValorBien == null ? 0 : response.PlanFinanciacion[0].ValorBien)[0].value;
                        $('#montofinanciarformato').autoNumeric('init', { aSign: response.PlanFinanciacion[0].Simbolo + ' ' });
                        $scope.newPlanFinanciamiento.MontoFinanciar = $('#montofinanciarformato').autoNumeric('set', response.PlanFinanciacion[0].MontoFinanciar == null ? 0 : response.PlanFinanciacion[0].MontoFinanciar)[0].value
                        $('#primaformato').autoNumeric('init', { aSign: response.PlanFinanciacion[0].Simbolo + ' ' });
                        $scope.newPlanFinanciamiento.PrimaAnticipo = $('#primaformato').autoNumeric('set', response.PlanFinanciacion[0].PrimaAnticipo == null ? 0 : response.PlanFinanciacion[0].PrimaAnticipo)[0].value;
                        $scope.newPlanFinanciamiento.Plazo = response.PlanFinanciacion[0].Plazo;
                        $scope.newPlanFinanciamiento.AutoUsado = response.PlanFinanciacion[0].AutoUsado;
                        $scope.newPlanFinanciamiento.Categoria = response.PlanFinanciacion[0].Categoria;
                        $scope.newPlanFinanciamiento.MarcaAuto = response.PlanFinanciacion[0].MarcaAuto;
                        $scope.newPlanFinanciamiento.Modelo = response.PlanFinanciacion[0].Modelo;
                        $scope.newPlanFinanciamiento.CasaComercial = response.PlanFinanciacion[0].CasaComercial;
                        $scope.newPlanFinanciamiento.OtroMarcaAuto = response.PlanFinanciacion[0].OtroMarcaAuto;
                        $scope.newPlanFinanciamiento.OtroModelo = response.PlanFinanciacion[0].OtroModelo;
                        $scope.newPlanFinanciamiento.OtroCasaComercial = response.PlanFinanciacion[0].OtroCasaComercial;
                        $scope.newPlanFinanciamiento.ConstruccionPropia = response.PlanFinanciacion[0].ConstruccionPropia;
                        $scope.newPlanFinanciamiento.TipoPropiedad = response.PlanFinanciacion[0].TipoPropiedad;
                        $scope.newPlanFinanciamiento.Urbanizadora = response.PlanFinanciacion[0].Urbanizadora;
                        $scope.newPlanFinanciamiento.Garantia = response.PlanFinanciacion[0].Garantia;
                        $scope.newPlanFinanciamiento.CostoPrograma = response.PlanFinanciacion[0].CostoPrograma;
                    }
                });

                $scope.MostrarAdjuntos = function (negocio, ingresos, matricula) {
                    if (negocio) {
                        $scope.showFilesNP = !$scope.showFilesNP;
                    }
                    if (ingresos) {
                        $scope.showFilesI = !$scope.showFilesI;
                    }
                    if (matricula) {
                        $scope.showFilesCM = !$scope.showFilesCM;
                    }
                }

                 $scope.MostrarAdjuntosCodeudor = function (negocio, ingresos) {
                    if (negocio) {
                        $scope.showFilesNPCodeudor = !$scope.showFilesNPCodeudor;
                    }
                    if (ingresos) {
                        $scope.showFilesICodeudor = !$scope.showFilesICodeudor;
                    }
                    
                }

            };
        },
        templateUrl: '/DesktopModules/Gestores/FormularioBase/FormularioBase.html'
    };
});