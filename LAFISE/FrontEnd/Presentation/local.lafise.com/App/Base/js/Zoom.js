﻿angular.module('zoom-model', []).directive('zoom', function ($compile) {
    return {
        restrict: 'EA',
        scope: {
            small: "=",
            big: "="
        },
        controller: function ($scope, $attrs, $element, $compile) {
            $scope.init = function () {
                $scope.$watch('small + big ', function (newValue, oldValue) {
                    if (($scope.small != undefined) && ($scope.small != '')) {
                        var str = '<img class="original lafise-label-zoom" with="100px" height="100px"  ng-src="' + $scope.small + '" data-zoom-image="' + $scope.big + '"  /><div></div>';
                        var e = $compile(str)($scope);
                        $element.html(e);
                        $(".original").ezPlus({
                            zoomWindowWidth: 300,
                            zoomWindowHeight: 300
                        });
                    }

                }, true);

            };
            $scope.init();

        }
    };
});