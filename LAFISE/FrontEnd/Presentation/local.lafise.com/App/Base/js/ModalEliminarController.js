﻿angular.module('ModalEliminar', []).controller('ModalEliminarController', function ($scope, $modalInstance, Entity) {

    $scope.ok = function () {
        $modalInstance.close($scope.delete(Entity));
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $scope.closeAlert(0);
    };
});