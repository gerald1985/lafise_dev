﻿angular.module('ExportarBaseModule').directive('exportar', function (ExportarBaseFactory) {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            filtros: "=",
        },
        controller: ["$scope", function ($scope) {
            $scope.$watch('filtros', function (Value) {

                if (Value) {
                    $scope.setExport($scope.filtros);
                }
                $scope.exportData = function (tipo) {
                    var blob = new Blob(["\ufeff", document.getElementById('exportable' + tipo).innerHTML], {
                        type: "application/vnd.ms-excel;charset=utf-8"
                    });
                    var nombreArchivo = '';
                    switch (parseInt($scope.filtros.IdTipoSolicitud)) {
                        case 1:
                            nombreArchivo = "SolicitudMarcaAuto_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 2:
                            nombreArchivo = "SolicitudPrestamoPersonal_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 3:
                            nombreArchivo = "SolicitudPrestamoHipotecario_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 4:
                            nombreArchivo = "SolicitudTarjetaCredito_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 5:
                            nombreArchivo = "SolicitudTarjetaDébito_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 6:
                            nombreArchivo = "SolicitudPrestamoEducativo_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 7:
                            nombreArchivo = "SolicitudAperturaCuenta_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 8:
                            nombreArchivo = "SolicitudSeguroVida_Accidentes_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 9:
                            nombreArchivo = "SolicitudSeguroVehicular_" + new Date().toLocaleDateString() + ".xls";
                            break;
                        case 10:
                            nombreArchivo = "SolicitudReclamos_" + new Date().toLocaleDateString() + ".xls";
                            break;
                    }
                    saveAs(blob, nombreArchivo);
                }

            }, true);
            $scope.setExport = function (Filtros) {
                //Servicio para obtener la  exportar según el id
                $scope.listaExportar = [];
                switch (parseInt(Filtros.IdTipoSolicitud)) {
                    case 1:
                        ExportarBaseFactory.ObtenerExportarMarcaAuto(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 2:
                        ExportarBaseFactory.ObtenerExportarPrestamoPersonal(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 3:
                        ExportarBaseFactory.ObtenerExportarPrestamoHipotecario(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 4:
                        ExportarBaseFactory.ObtenerExportarTarjetaCreditoDebito(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 5:
                        ExportarBaseFactory.ObtenerExportarTarjetaCreditoDebito(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 6:
                        ExportarBaseFactory.ObtenerExportarPrestamoEducativo(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 7:
                        ExportarBaseFactory.ObtenerExportarAperturaCuenta(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 8:
                        ExportarBaseFactory.ObtenerExportarSeguroVidaAccidente(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 9:
                        ExportarBaseFactory.ObtenerExportarSeguroVehicular(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                    case 10:
                        ExportarBaseFactory.ObtenerExportarReclamos(Filtros).then(function (response) {
                            $scope.listaExportar = response;
                        });
                        break;
                }
            }
        }],
        templateUrl: '/DesktopModules/Gestores/ExportarBase/ExportarBase.html'
    };
});