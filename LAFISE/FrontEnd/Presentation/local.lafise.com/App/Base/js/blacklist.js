﻿angular.module('blacklist-model', []).directive('blacklist', function () {
    return {
        restrict: "A",
        scope: {
            blacklist: '@'
        },
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {
            var blist = [];
            ngModel.$parsers.unshift(function (value) {
                if (blist.length === 0) {
                    blist = scope.blacklist.split(',');
                }
                //console.log(value);
                if (blist[blist.length - 1] === '') {
                    blist.splice(blist.length - 1, 1);
                }

                ngModel.$setValidity('blacklist', blist.indexOf(value) === -1)
                return value;
            });
        }
    };
});