﻿/*==========================================================================
Archivo:            TipoVehiculoController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de tipo identificacion usadas en los diferentes CUS
Autor:              juan.hincapie                     
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("TipoVehiculoModule").controller("TipoVehiculoController", function ($scope, TipoVehiculoFactory) {

    //Método para obtener los datos del servicio y poblar las listas de selección.
    var getData = function () {
        $scope.tiposVehiculo = [];
        TipoVehiculoFactory.obtenerTipoVehiculo().then(function (response) {
            $scope.tiposVehiculo = response;
        });
    }

    //Llamado a la función para que se llene las listas de selección.
    getData();

});