﻿angular.module('paraPruebasModule', ['formato-moneda', 'ui.bootstrap']).controller('paraPruebaController', ['$scope', function ($scope) {
    $scope.modeloPrueba = '';//Modelo para capturar el dato
    $scope.formatoMoneda = "C=";//Formato de la moneda
    $scope.Guardar = function () {
        alert($scope.pruebas.$valid);
    }


  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };

  //Desactiva las casillas de los fines de semana
  //$scope.disabled = function(date, mode) {
  //  return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  //};

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
  $scope.maxDate = new Date(2020, 5, 22);

  $scope.open = function($event) {
    $scope.status.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  //$scope.dateOptions = {
  //  formatYear: 'yy',
  //  startingDay: 1
  //};

  $scope.status = {
    opened: false
  };
}]);