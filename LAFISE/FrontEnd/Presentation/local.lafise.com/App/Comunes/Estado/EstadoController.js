﻿/*==========================================================================
Archivo:            MonedaController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de monedas usadas en los diferentes CUS
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("EstadoModule").controller("EstadoController", function ($scope, $rootScope, $filter, EstadoFactory) {

    //Método para obtener los datos del servicio y poblar las listas de selección.
    $scope.getData = function (IdTipoEstado) {
        $scope.estados = [];
        EstadoFactory.obtenerEstados(IdTipoEstado).then(function (response) {
            $scope.estados = response;
        });
    }

    //Llamado a la función para que se llene las listas de selección.
    $scope.getData;



});