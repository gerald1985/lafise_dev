﻿/*==========================================================================
Archivo:            EstadoFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de consultar todas los estado por tipo de estado
Autor:              juan.hincapie                          
Fecha de creación:  17/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("EstadoModule").factory("EstadoFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de estados.
    var obtenerEstados = function (IdTipoEstado) {
        var result = $http({
            method: "GET",
            url: appContext + "/Estado/VerTodosIdSolicitud?idTipoEstado=" + IdTipoEstado
        });
        return result.then(handleSuccess, handleError);
    }



    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("Ha ocurrido un error desconocido");
        }
        return $q.reject(response.data.message);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerEstados: obtenerEstados
    });

});