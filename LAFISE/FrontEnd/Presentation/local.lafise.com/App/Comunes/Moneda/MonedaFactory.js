﻿/*==========================================================================
Archivo:            MonedaFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de consultar todas las monedas
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("MonedaModule").factory("MonedaFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de monedas.
    var obtenerMonedas = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/Moneda/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }

    var obtenerSimbolo = function () {
        var result = $http({
            method: 'GET',
            url: appContext + '/Moneda/ObtenerSimboloMoneda?url=' + window.location.href
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("Ha ocurrido un error desconocido");
        }
        return $q.reject(response.data.message);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerMonedas: obtenerMonedas,
        obtenerSimbolo: obtenerSimbolo
    });

});