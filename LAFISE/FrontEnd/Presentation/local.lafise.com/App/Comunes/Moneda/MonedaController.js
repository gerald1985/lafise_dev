﻿/*==========================================================================
Archivo:            MonedaController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de monedas usadas en los diferentes CUS
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("MonedaModule").controller("MonedaController", function ($scope, $rootScope, $filter, MonedaFactory) {
    $scope.Simbolo = '';
    //Método para obtener los datos del servicio y poblar las listas de selección.
    var getData = function () {
        $scope.monedas = [];
        MonedaFactory.obtenerMonedas().then(function (response) {
            $scope.monedas = response;
        });
    }

    $scope.cargarSimbolo = function () {
        MonedaFactory.obtenerSimbolo().then(function (respuesta) {
            $scope.Simbolo = respuesta;
        });
    };


    //Llamado a la función para que se llene las listas de selección.
    getData();
    $scope.cargarSimbolo();


});