﻿/*==========================================================================
Archivo:            TipoIdentificacionController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de tipo identificacion usadas en los diferentes CUS
Autor:              paola.munoz                          
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("TipoIdentificacionModule").controller("TipoIdentificacionController", function ($scope, $rootScope, $filter, TipoIdentificacionFactory) {

    //Método para obtener los datos del servicio y poblar las listas de selección.
    var getData = function () {
        $scope.tipoIdentificaciones = [];
        TipoIdentificacionFactory.obtenerTipoIdentificacionPorPais(window.location.href).then(function (response) {
            $scope.tipoIdentificaciones = response;
        });
    }

    //Llamado a la función para que se llene las listas de selección.
    getData();

});