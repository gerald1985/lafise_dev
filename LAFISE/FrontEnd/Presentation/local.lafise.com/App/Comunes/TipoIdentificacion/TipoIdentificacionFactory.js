﻿/*==========================================================================
Archivo:            TipoIdentificacionFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de consultar todos los tipos de identificación
Autor:              paola.munoz                       
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("TipoIdentificacionModule").factory("TipoIdentificacionFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de TipoIdentificacion
    var obtenerTipoIdentificacionPorPais = function (path) {
        var result = $http({
            method: "GET",
            url: appContext + "/TipoIdentificacion/VerTipoIdentificacionPorIdPais?path=" + path
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerTipoIdentificacionPorPais: obtenerTipoIdentificacionPorPais
    });

});