﻿angular.module("SolicitudModule").controller("SolicitudController", function ($scope, $filter, i18nService, $timeout, SolicitudFactory, Hub) {
    i18nService.setCurrentLang('es');
    $scope.formMode = false;


    $scope.FiltroSolicitudes = {
        Estado: null,
        IdPais: -1,
        IdTipoSolicitud: ''
    }


    $scope.newSolicitud =
        {
            Id: '',
            IdEstado: '',
            comentario: '',
            HistoricoComentario: [],
            PathUrl: window.location.href,
            IdTipoSolicitud: ''
        }
    $scope.FiltroEstado = 1;

    $scope.solicitudObj = {};
    $scope.estados = [];
    $scope.tiposContacto = [];

    //Configuración del Grid.
    $scope.gridOpts = {
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.showForm(row.entity.IdSolicitud)
            });
        }
    };



    $scope.showForm = function (Id) {
        $scope.closeAlert(0);
        $scope.formMode = true;
        $scope.resetSolicitud();
        $scope.solicitudObj.IdSolicitud = Id;
        $scope.solicitudObj.bandejaProceso = $scope.activeTab2;
        $scope.solicitudObj.bandejaTerminados = $scope.activeTab3;
        $scope.newSolicitud.Id = Id;
        $scope.solicitudObj.invoke();
        $scope.showMsgTipoContacto = false;
        $scope.showMsgEstado = false;
    }




    $scope.OpenTab = function (tab1, tab2, tab3, type) {
        $scope.activeTab1 = tab1;
        $scope.activeTab2 = tab2;
        $scope.activeTab3 = tab3;

        if (tab1) {
            $scope.FiltroSolicitudes.Estado = null;
            $scope.FiltroSolicitudes.IdPais = '';
            $scope.FiltroSolicitudes.IdTipoSolicitud = type;
            $scope.FiltroEstado = 1;
        }
        if (tab2) {
            $scope.FiltroSolicitudes.Estado = "1,3";
            $scope.FiltroSolicitudes.IdPais = '';
            $scope.FiltroSolicitudes.IdTipoSolicitud = type;
            $scope.FiltroEstado = 2;
        }
        if (tab3) {
            $scope.FiltroSolicitudes.Estado = "2,4,5";
            $scope.FiltroSolicitudes.IdPais = '';
            $scope.FiltroSolicitudes.IdTipoSolicitud = type;
        }

        $scope.getData();
        $scope.getListas($scope.FiltroEstado, $scope.activeTab2);
    };


    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.resetSolicitud = function () {
        $scope.newSolicitud =
    {
        Id: '',
        IdEstado: '',
        comentario: '',
        HistoricoComentario: [],
        PathUrl: window.location.href
    }
    }

    $scope.activeTab1 = true;
    $scope.activeTab2 = false;
    $scope.activeTab3 = false



    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.editSolicitud.submitted = false;
        $scope.gridApi.selection.clearSelectedRows();
        $scope.resetSolicitud();
        $scope.closeAlert(0);
        $scope.getData();
    }


    $scope.hideValidacion = function (tipocontacto, estado) {
        if (!tipocontacto) {
            $scope.showMsgTipoContacto = tipocontacto;
        }
        if (!estado) {
            $scope.showMsgEstado = estado;
        }
    }

    $scope.listaSolicitudes = [];

    //Llamado al hub - SignalR
    var hub = new Hub('gestorBandejaEntradaHub', {

        //client side methods
        listeners: {
            'broadcastSolicitud': function (entity) {
                var solicitud = { IdSolicitud: entity.IdSolicitud, NumeroSolicitud: entity.NumeroSolicitud, NombreCompleto: entity.NombreCompleto, FechaCreacion: $filter('date')(entity.FechaCreacion, 'dd/MM/yyyy') };

                SolicitudFactory.obtenerPaisUsuario().then(function (idPais) {
                    if (entity.IdTipoSolicitud == $scope.FiltroSolicitudes.IdTipoSolicitud && entity.IdPais == idPais) {
                        $scope.gridOpts.data.unshift(solicitud);
                        $scope.$applyAsync();
                    }
                });
            }
        },

        errorHandler: function (error) {
            console.error(error);
        },

        stateChanged: function (state) {
            switch (state.newState) {
                case $.signalR.connectionState.connecting:
                    $scope.Estado = 'conectando';
                    //console.log($scope.Estado);
                    //$scope.$apply();
                    break;
                case $.signalR.connectionState.connected:
                    $scope.Estado = 'conectado';
                    //console.log($scope.Estado);
                    //$scope.$apply();
                    break;
                case $.signalR.connectionState.reconnecting:
                    $scope.Estado = 'reconectando';
                    //console.log($scope.Estado);
                    //$scope.$apply();
                    break;
                case $.signalR.connectionState.disconnected:
                    $scope.Estado = 'desconectado';
                    //console.log($scope.Estado);
                    //$scope.$apply();
                    break;
            }
        }
    });



    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {
        SolicitudFactory.ObtenerSolicitudes($scope.FiltroSolicitudes).then(function (response) {
            $scope.gridOpts.columnDefs = [];
            if ($scope.activeTab1) {
                $scope.gridOpts.columnDefs.push({ name: 'NumeroSolicitud', field: 'NumeroSolicitud', displayName: 'Número de solicitud' },
                { name: 'NombreCompleto', field: 'NombreCompleto', displayName: 'Nombre de usuario' },
                { name: 'FechaCreacion', field: 'FechaCreacion', displayName: 'Fecha' },
                { name: 'Procedencia', field: 'Procedencia', displayName: 'Procedencia' });
                $scope.gridOpts.data = response;
            }

            if ($scope.activeTab2) {
                $scope.gridOpts.columnDefs.push({ name: 'NumeroSolicitud', field: 'NumeroSolicitud', displayName: 'Número de solicitud' },
                 { name: 'NombreCompleto', field: 'NombreCompleto', displayName: 'Nombre de usuario' },
                 { name: 'FechaCreacion', field: 'FechaCreacion', displayName: 'Fecha Creación' },
                 { name: 'FechaActualizacion', field: 'FechaActualizacion', displayName: 'Fecha Actualización' },
                 { name: 'Procedencia', field: 'Procedencia', displayName: 'Procedencia' });
                $scope.gridOpts.data = response;
            }

            if ($scope.activeTab3) {
                $scope.gridOpts.columnDefs.push({ name: 'NumeroSolicitud', field: 'NumeroSolicitud', displayName: 'Número de solicitud' },
                { name: 'NombreCompleto', field: 'NombreCompleto', displayName: 'Nombre de usuario' },
                { name: 'EstadoSolicitud', field: 'EstadoSolicitud', displayName: 'Estado de la solicitud' },
                { name: 'FechaCreacion', field: 'FechaCreacion', displayName: 'Fecha Creación' },
                { name: 'FechaActualizacion', field: 'FechaActualizacion', displayName: 'Fecha Actualización' },
                { name: 'Procedencia', field: 'Procedencia', displayName: 'Procedencia' });
                $scope.gridOpts.data = response;
            }

        });
    }
    //Método para obtener los datos del servicio y poblar las listas de selección.
    $scope.getListas = function (IdTipoEstado, estipoContacto) {
        if (estipoContacto) {
            SolicitudFactory.obtenerTiposContacto().then(function (response) {
                $scope.tiposContacto = response;
            });
        }

        SolicitudFactory.obtenerEstados(IdTipoEstado).then(function (response) {
            $scope.estados = response;
        });
    }
    $scope.getListas($scope.FiltroEstado, false);

    $scope.selectListas = function () {
        $scope.hideValidacion(!$scope.editSolicitud.tipocontacto.$valid, !$scope.editSolicitud.estado.$valid);
    }

    $scope.add = function () {
        if ($scope.editSolicitud.$valid) {
            $scope.newSolicitud.IdTipoSolicitud = $scope.FiltroSolicitudes.IdTipoSolicitud;
            $scope.newSolicitud.HistoricoComentario.push({ IdEstado: $scope.newSolicitud.IdEstado, Comentario: $scope.newSolicitud.comentario });
            SolicitudFactory.editarSolicitud($scope.newSolicitud).then(function (response) {
                if ($scope.newSolicitud.IdEstado === 1 || $scope.newSolicitud.IdEstado == 3) {
                    $scope.OpenTab(false, true, false, $scope.FiltroSolicitudes.IdTipoSolicitud);
                }
                if ($scope.newSolicitud.IdEstado === 2 || $scope.newSolicitud.IdEstado === 4 || $scope.newSolicitud.IdEstado === 5) {
                    $scope.OpenTab(false, false, true, $scope.FiltroSolicitudes.IdTipoSolicitud);
                }
                $scope.editSolicitud.submitted = false;
                $scope.formMode = false;
                $scope.setAlert('success', 'La información ha sido guardada exitosamente');
            });
        }
        else {
            $scope.editSolicitud.submitted = true;
            if ($scope.activeTab2) {
                if (!$scope.editSolicitud.tipocontacto.$valid) {
                    $scope.msgValidarTipoContacto = 'Se debe seleccionar al menos un tipo de contacto, por favor validar';
                    $scope.showMsgTipoContacto = true;
                }
            }
            if (!$scope.editSolicitud.estado.$valid) {
                $scope.msgValidarEstado = 'Se debe seleccionar al menos una estado, por favor validar';
                $scope.showMsgEstado = true;
            }
        }
    }
    $scope.getData();

});