﻿angular.module("SolicitudModule").factory("SolicitudFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";
    var ObtenerSolicitudes = function (FiltroSolicitud) {
        var result = $http({
            method: "POST",
            data: FiltroSolicitud,
            url: appContext + "/Solicitud/VerGestorSolicitud"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de tipos de contacto.
    var obtenerTiposContacto = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/TipoContacto/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }


    //Función encargada de consumir el servicio para la consulta de estados.
    var obtenerEstados = function (IdTipoEstado) {
        var result = $http({
            method: "GET",
            url: appContext + "/Estado/VerTodosIdSolicitud?idTipoEstado=" + IdTipoEstado
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de crear una solicitud de marca auto
    //Parametro entidad: recibe la entidad de la solicitud
    var editarSolicitud = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Solicitud/EditarSolicitud"
        });
        return sendRequest(result);
    }


    //Función encargada de consumir el servicio para la consulta de pais por usuario.
    var obtenerPaisUsuario = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/Solicitud/VerPaisUsuario"
        });
        return result.then(handleSuccess, handleError);
    }


    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function handleSuccess(response) {
        return response.data;
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    return ({
        ObtenerSolicitudes: ObtenerSolicitudes,
        obtenerEstados: obtenerEstados,
        obtenerTiposContacto: obtenerTiposContacto,
        editarSolicitud: editarSolicitud,
        obtenerPaisUsuario: obtenerPaisUsuario
    });

});