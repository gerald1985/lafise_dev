﻿/*==========================================================================
Archivo:            TipoBienController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de tipo de bienes usadas en los diferentes CUS
Autor:              juan.hincapie                     
Fecha de creación:  07/01/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("TipoBienModule").controller("TipoBienController", function ($scope, TipoBienFactory) {

    //Método para obtener los datos del servicio y poblar las listas de selección.
    var getData = function () {
        $scope.tiposBien = [];
        TipoBienFactory.obtenerTipoBien().then(function (response) {
            $scope.tiposBien = response;
        });
    }

    //Llamado a la función para que se llene las listas de selección.
    getData();

});