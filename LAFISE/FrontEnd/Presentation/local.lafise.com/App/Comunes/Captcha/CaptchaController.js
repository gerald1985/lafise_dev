﻿angular.module("CaptchaModule").controller('CaptchaController', ['vcRecaptchaService', '$http', function (vcRecaptchaService, $http) {
    var vm = this;
    vm.publicKey = "6LfuaxITAAAAAPNBZtqcJqPq6VWjYijYm1fv4C_G";
    vm.signup = function () {
        if (vcRecaptchaService.getResponse() === "") {         
            return false;
        } else {
            return true;
        }
    }
}])