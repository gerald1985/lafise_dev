﻿/*==========================================================================
Archivo:            TipoActividadFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de consultar todos los tipos de actividad economica
Autor:              juan.hincapie
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("TipoActividadModule").factory("TipoActividadFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";


    //Función encargada de consumir el servicio para la consulta de TipoVehiculo
    var obtenerTipoActividad = function (path) {
        var result = $http({
            method: "GET",
            url: appContext + "/TipoActividad/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerTipoActividad: obtenerTipoActividad
    });
});