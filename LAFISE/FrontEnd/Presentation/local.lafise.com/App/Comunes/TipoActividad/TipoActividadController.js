﻿/*==========================================================================
Archivo:            TipoActividadController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de tipo identificacion usadas en los diferentes CUS
Autor:              juan.hincapie                     
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("TipoActividadModule").controller("TipoActividadController", function ($scope, $rootScope, TipoActividadFactory) {
    $rootScope.tiposActividad = [];
    //Método para obtener los datos del servicio y poblar las listas de selección.
    var getData = function () {

        TipoActividadFactory.obtenerTipoActividad().then(function (response) {
            angular.forEach(response, function (value, key) {
                $rootScope.tiposActividad.push({ IdTipoActividad: value.Id, Nombre: value.Nombre, mostrarOtro: false, OtraActividad: '', seleccionado: false });
            });

        });
    }

    //Llamado a la función para que se llene las listas de selección.
    getData();

});