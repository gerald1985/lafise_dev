﻿/*==========================================================================
Archivo:            PaisController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de paises usadas en los diferentes CUS
Autor:              steven.echavarria                          
Fecha de creación:  27/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("PaisModule").controller("PaisController", function ($scope, $rootScope, $filter, PaisFactory) {

    //Método para obtener los datos del servicio y poblar las listas de selección.
    var getData = function () {
        $scope.paises = [];
        PaisFactory.obtenerPaises().then(function (response) {
            $scope.paises = response;
            if ($scope.paises.length == 1) {
                $rootScope.paisesSelected.Paises = [$scope.paises[0].Id];
                $scope.newPais.IdPais = $scope.paises[0].Id;
            } else {
                $rootScope.paisesSelected.Paises = [];
            }
        });
    }

    //Objeto usado para manipular los valores de la lista de paises 
    $rootScope.paisesSelected = [];

    //Llamado a la función para que se llene las listas de selección.
    getData();


});