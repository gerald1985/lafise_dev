﻿/*==========================================================================
Archivo:            DepartamentoFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de consultar todos los departamentos
Autor:              paola.munoz                       
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("DepartamentoModule").factory("DepartamentoFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";
    //Metodo para consultar todos los departamentos
    var obtenerDepartamento = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/Departamento/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }
    //Metodo para consultar los departamento por pa´s
    var obtenerDepartamentoPais = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Departamento/VerDepartamentosPorPais?id="+id
        });
        return result.then(handleSuccess, handleError);
    }

    //Metodo para consultar los departamento por lista de país
    var obtenerListaDepartamentoPais = function (listaPais) {
        var result = $http({
            method: "POST",
            data: listaPais,
            url: appContext + "/Departamento/VerListaDepartamentoPorPais"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }
    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }
    //Retorna cada una de las funciones del factory
    return ({
        obtenerDepartamento: obtenerDepartamento,
        obtenerDepartamentoPais: obtenerDepartamentoPais,
        obtenerListaDepartamentoPais: obtenerListaDepartamentoPais
    });

});