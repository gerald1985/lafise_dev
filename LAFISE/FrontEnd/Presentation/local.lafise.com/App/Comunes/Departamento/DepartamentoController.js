﻿/*==========================================================================
Archivo:            DepartamentoController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de departamento usadas en los diferentes CUS
Autor:              paola.munoz                          
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("DepartamentoModule").controller("DepartamentoController", function ($scope, $rootScope, $filter, DepartamentoFactory) {
    //Lista de departamentos
    var getData = function () {
        $scope.departamentos = [];
        DepartamentoFactory.obtenerDepartamento().then(function (response) {
            $scope.departamentos = response;
        });
    }
    //Departamentos seleccionados
    $rootScope.departamentoSelected = [];
    //Metodo inicial
    getData();


});