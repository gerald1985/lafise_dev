﻿/*==========================================================================
Archivo:            CiudadFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de consultar todos las ciudades
Autor:              paola.munoz                       
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("CiudadModule").factory("CiudadFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";
    //Metodo para obtener todas las ciudades
    var obtenerCiudades = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/Ciudad/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }
    //Metodo para obtener las ciudades por departamento
    var obtenerCiudadPorDepartamento = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Ciudad/VerCiudadesPorDepartamento?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }
    //Metodo para obtener las ciudades por país
    var obtenerCiudadPorPais = function (idPais) {
        var result = $http({
            method: "POST",
            data: idPais,
            url: appContext + "/Ciudad/VerCiudadesPorPais"
        });
        return result.then(handleSuccess, handleError);
    }

    //Metodo para consultar las ciudades por lista de departamento
    var obtenerListaCiudadDepartamento = function (listaDepartamento) {
        var result = $http({
            method: "POST",
            data: listaDepartamento,
            url: appContext + "/Ciudad/VerListaCiudadesPorDepartamento"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }
    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerCiudades: obtenerCiudades,
        obtenerCiudadPorDepartamento: obtenerCiudadPorDepartamento,
        obtenerCiudadPorPais: obtenerCiudadPorPais,
        obtenerListaCiudadDepartamento: obtenerListaCiudadDepartamento
    });

});