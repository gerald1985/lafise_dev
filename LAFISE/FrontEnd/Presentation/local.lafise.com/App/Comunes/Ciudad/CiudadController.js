﻿/*==========================================================================
Archivo:            CiudadController
Descripción:        Objeto creado para implementar la presentación de las
                    listas de cuidad usadas en los diferentes CUS
Autor:              paola.munoz                          
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("CiudadModule").controller("CiudadController", function ($scope, $rootScope, $filter, CiudadFactory) {
    //Lista de ciudades
    var getData = function () {
        $scope.ciudades = [];
        CiudadFactory.obtenerCiudades().then(function (response) {
            $scope.ciudades = response;
            $rootScope.ciudadSelected.Ciudad = [];
        });
    }
    //Ciudades seleccionadas
    $rootScope.ciudadSelected = [];
    //Metodo inicial
    getData();


});