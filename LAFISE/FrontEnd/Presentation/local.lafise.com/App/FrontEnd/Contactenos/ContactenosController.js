﻿angular.module("ContactenosModule").controller("ContactenosController", function ($scope, $filter, $uibModal, ContactenosFactory) {
    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.showMsg = false;

    $scope.Iniciar = function () {

        if (window.location.href.includes('blp')) {
            $scope.verLeyenda = true
        } else { $scope.verLeyenda = false }

        //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
        $scope.newContactenos = {
            Id: '',
            NombreCompleto: '',
            EsCliente: false,
            Telefono: '',
            Email: '',
            Asunto: '',
            Mensaje: '',
            IdPais: '',
            PathUrl: window.location.href
        };

        $scope.AutorizoLeyenda = { Autorizo: false };
    }

    $scope.Iniciar();

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta segun, el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción
    $scope.cancel = function () {
        $scope.newContactenos.Id = '';
        $scope.newContactenos.NombreCompleto = '';
        $scope.newContactenos.EsCliente = false;
        $scope.newContactenos.Telefono = '';
        $scope.newContactenos.Email = '';
        $scope.newContactenos.Asunto = '';
        $scope.newContactenos.Mensaje = '';
        $scope.addContactenos.submitted = false;
        $scope.closeAlert(0);
    };


    //Función para insertar un contactenos en la base de datos.
    $scope.add = function (captcha) {
        if ($scope.addContactenos.$valid) {

            ContactenosFactory.crearContactenos(this.newContactenos).then(function (response) {
                $scope.addContactenos.submitted = false;
                $scope.setAlert('success', 'Gracias, su información ha sido enviada correctamente.');
                limpiarCampos();
            });
        } else {
            $scope.setAlert('danger', 'La solicitud no ha podido ser enviada. Por favor revise los campos requeridos');
            $scope.addContactenos.submitted = true;
        }
        grecaptcha.reset();
    };

    //Función para limpiar los campos de la solicitud de prestamo hipotecario
    var limpiarCampos = function () {
        $scope.newContactenos.Id = '';
        $scope.newContactenos.NombreCompleto = '';
        $scope.newContactenos.EsCliente = false;
        $scope.newContactenos.Telefono = '';
        $scope.newContactenos.Email = '';
        $scope.newContactenos.Asunto = '';
        $scope.newContactenos.Mensaje = '';
        $scope.Iniciar();
    };

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        limpiarCampos();
    }

    //Función para validar que el correo
    $scope.FormatoEmail = function () {
        if ($scope.addContactenos.correo.$error.email != undefined && $scope.addContactenos.correo.$error.email) {
            $scope.msgValidarCorreo = 'El e-mail ' + document.getElementById('correo').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmail = true;
            $scope.newContactenos.Email = '';
        } else {
            $scope.showMsgEmail = false;
        }
    };

    //Verificar si algún checkbox fue seleccionado
    $scope.someSelected = function (object) {
        return Object.keys(object).some(function (key) {
            return object[key];
        });
    }

    //Función para esconder la validación del formato correo
    $scope.hideValidacion = function () {
        $scope.showMsgEmail = false;
    };
});