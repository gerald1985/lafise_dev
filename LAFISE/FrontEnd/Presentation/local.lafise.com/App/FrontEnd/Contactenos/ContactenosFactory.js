﻿/*==========================================================================
Archivo:            ContactenosFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del Formulario de contactenos
Autor:              paola.munoz                        
Fecha de creación:  29/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("ContactenosModule").factory("ContactenosFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

   
    //Función encargada de crear un tarjeta
    //Parametro entidad: recibe la entidad de tarjeta
    var crearContactenos = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Contactenos/CrearContactenos"
        });
        return sendRequest(result);
    }

    //Retorna cada una de las funciones del factory
    return ({
        crearContactenos: crearContactenos
    });

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

});
