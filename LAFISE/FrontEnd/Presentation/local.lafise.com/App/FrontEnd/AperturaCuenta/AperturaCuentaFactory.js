﻿/*==========================================================================
Archivo:            AperturaCuentaFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del Formulario Solicitud de apertura de cuenta
Autor:              steven.echavarria
Fecha de creación:  20/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("AperturaCuentaModule").factory("AperturaCuentaFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de sucursales.
    var obtenerSucursales = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/Sucursal/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }


    //Función para obtener el rango sueldo por el pais correspondiente al portal.
    var obtenerRangoSueldoPorPais = function (url) {
        var result = $http({
            method: "GET",
            url: appContext + "/RangoSueldo/ConsultarRangoSueldoPais?url=" + url
        });
        return result.then(handleSuccess, handleError);
    }


    //Función encargada de crear una solicitud de apertura de cuenta
    //Parametro entidad: recibe la entidad de apertura de cuenta
    var crearSolicitud = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Solicitud/CrearSolicitud"
        });
        return sendRequest(result);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerSucursales: obtenerSucursales,
        obtenerRangoSueldoPorPais: obtenerRangoSueldoPorPais,
        crearSolicitud: crearSolicitud
    });

});