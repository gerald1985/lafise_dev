﻿angular.module("SeguroVidaAccidentesModule").controller("SeguroVidaAccidentesController", function ($scope, $rootScope, Upload, SeguroVidaAccidentesFactory, Hub) {

    $scope.esGuardado = false;
    $scope.Iniciar = function () {
        //Objeto para almacenar el prestamo hipotecario
        $scope.newSolicitud = {
            IdTipoSolicitud: 8,
            PathUrl: window.location.href,
            DatosPersonales: [],
            DatosFinancieros: [],
            Soportes: []
        };

        //Lista de los datos personales
        $scope.newDatosPersonales = {
            Nombre: '',
            Apellidos: '',
            Telefono: '',
            Email: '',
            Masculino: '',
            Nacionalidad: '',
            CiudadResidencia: '',
            IdTipoIdentificacion: '',
            NumeroIdentificacion: '',
            Profesion: '',
            EsCodeudor: false,

        };

        //Lista de los datos financieros
        $scope.newDatosFinancieros = {
            EsCliente: false,
            PoseeNomina: false,
            NegocioPropio: false,
            Asalariado: false,
            PromedioIngresoSelected: '',
            PromedioIngresoMensualNombre: '',
            ContainerPoseeNomina: true,
            ArchivosNP: '',
            ArchivosSI: '',
            EsZurdo: '',
            Estatura: '',
            Peso: '',
            ActividadEconomica: []
        };

        //Lista del actividad economica
        $scope.newActividadEconomica =
            {
                IdTipoActividad: '',
                OtraActividad: ''
            };

        $scope.TipoActividad = [],
        $scope.ListaArchivosNP = [];
        $scope.ListaArchivosSI = [];

    }

    $scope.Iniciar();

    $scope.SeleccionarActividad = function (event, id, nombre, index) {
        if ((nombre === 'Otro')) {
            if ((event.target.checked)) {
                $rootScope.tiposActividad[index].mostrarOtro = true;
            }
            else {
                $rootScope.tiposActividad[index].mostrarOtro = false;
                $rootScope.tiposActividad[index].OtraActividad = '';
            }
        }
    }

    //Verificar si algún checkbox fue seleccionado
    $scope.someSelected = function (object) {
        return Object.keys(object).some(function (key) {
            return object[key];
        });
    }

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.PromIngresosChanged = function (item) {
        $scope.newDatosFinancieros.PromedioIngresoMensualNombre = item.RangoSueldo;
    }

    // Función para cargar varios archivos
    $scope.uploadFiles = function (files, error, type) {
        angular.forEach(files, function (value, key) {
            if (value.name != '') {
                if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                    $scope.ListaArchivosNP.push({ Nombre: value.name, IdTipoSoporte: 2, Ruta: '', EsCodeudor: false, showAlert: false, file: value, tipo: 'NP', RutaFisica: '' });
                }
                if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                    $scope.ListaArchivosSI.push({ Nombre: value.name, IdTipoSoporte: 1, Ruta: '', EsCodeudor: false, showAlert: false, file: value, tipo: 'SI', RutaFisica: '' });
                }
            }
        });

        angular.forEach(error, function (value, key) {
            switch (value.$error) {
                case 'maxSize':
                    {
                        if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                            $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                        }
                        if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                            $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                        }
                        break;
                    }
                case 'pattern':
                    {
                        if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                            $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                        }
                        if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                            $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                        }
                        break;
                    }
            }
        });
    }

    $scope.PoseeNominaChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = !state;
    }

    $scope.EsClienteChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = state;
    }

    $scope.deleteFile = function (index, type, esError) {
        if (type === 'NP') {
            if ($scope.ListaArchivosNP.length == 1) {
                $scope.newDatosFinancieros.ArchivosNP = '';
                $scope.addSolicitud.archivoNP.$setValidity('required', false);
                $scope.ListaArchivosNP.splice(index, 1);
            }
            else
                $scope.ListaArchivosNP.splice(index, 1);
        }

        if (type === 'SI') {
            if ($scope.ListaArchivosSI.length == 1) {
                $scope.newDatosFinancieros.ArchivosSI = '';
                $scope.addSolicitud.archivoSI.$setValidity('required', false);
                $scope.ListaArchivosSI.splice(index, 1);
            } else
                $scope.ListaArchivosSI.splice(index, 1);
        }
    }

    $scope.msgValidarCorreo = '';
    $scope.showMsgEmail = false;
    //Función para validar que el correo
    $scope.FormatoEmail = function () {
        if ($scope.addSolicitud.correo.$error.email != undefined && $scope.addSolicitud.correo.$error.email) {
            $scope.msgValidarCorreo = 'El e-mail ' + document.getElementById('correo').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmail = true;
            $scope.newDatosPersonales.Email = '';
        } else {
            $scope.showMsgEmail = false;
        }
    };


    $scope.hideValidacion = function () {
        $scope.showMsgEmail = false;
    }


    $scope.getListas = function () {
        $scope.rangoSueldoPais = [];
        SeguroVidaAccidentesFactory.obtenerRangoSueldoPorPais($scope.newSolicitud.PathUrl).then(function (response) {
            $scope.rangoSueldoPais = response;
        });
        localStorage.clear();
    }

    //Función para cancelar la solicitud de prestamo hipotecario
    $scope.cancel = function (captcha) {
        limpiarCampos();
    };

    //Función para limpiar los campos de la solicitud de prestamo hipotecario
    var limpiarCampos = function () {
        $scope.newDatosPersonales = {};
        $scope.newDatosFinancieros = {};
        $scope.newDatosAuto = {};
        $scope.newActividadEconomica = {};
        $scope.ListaArchivosNP = [];
        $scope.ListaArchivosSI = [];

        angular.forEach($rootScope.tiposActividad, function (value, key) {
            $rootScope.tiposActividad[key].mostrarOtro = false;
            $rootScope.tiposActividad[key].OtraActividad = '';
            $rootScope.tiposActividad[key].seleccionado = false;
        });
        $scope.esGuardado = false;
        $scope.Iniciar();
    };


    $scope.SaveFiles = function (file, count) {
        if (file) {
            file.upload = Upload.upload({
                url: '/Uploads/UploadDocumentosSolicitudSeguroVidaAccidentesHandler.ashx',
                data: { file: file }
            });

            file.upload.then(function (response) {
                file.result = response.data;
                if (response.data.ruta != '') {
                    localStorage.setItem("ruta", response.data.ruta);
                    localStorage.setItem("filename", response.data.filename);

                }
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt, response) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));
                if (file.progress === 100) {
                    if (count == 1 && $scope.esGuardado == false) {
                        $scope.guardarSolicitud();
                        $scope.esGuardado = true;
                    }
                }
            });
        }
    };


    //Función para insertar una solicitud de marca auto en la base de datos.
    $scope.add = function (captcha) {
        if ($scope.addSolicitud.$valid) {
            $scope.Archivos = $scope.ListaArchivosNP.concat($scope.ListaArchivosSI);
            var count = $scope.Archivos.length;

            if (count == 0) {
                $scope.guardarSolicitud();
            }
            else {
                angular.forEach($scope.Archivos, function (value, key) {
                    if (value.file != '') {
                        $scope.SaveFiles(value.file, count)
                        count--;
                    }
                });
            }
        } else {
            $scope.addSolicitud.submitted = true;
            $scope.setAlert('danger', 'La solicitud no ha podido ser enviada. por favor revise los campos requeridos');
        }
    };


    //Llamado al hub - SignalR
    var hub = new Hub('gestorBandejaEntradaHub', {

        //server side methods
        methods: ['sendsolicitud'],

        errorHandler: function (error) {
            console.error(error);
        }
    });

    //Función encargada de guardar la solicitud
    $scope.guardarSolicitud = function () {
        setTimeout(function () {
            angular.forEach($scope.Archivos, function (value, key) {
                if (localStorage.ruta != undefined && localStorage.filename != undefined) {
                    value.Ruta = localStorage.ruta + value.file.name;
                    value.RutaFisica = localStorage.filename + value.file.name;
                }
            });
            $scope.newDatosPersonales.Masculino = $scope.newDatosPersonales.Masculino == "true" ? true : false;
            $scope.newSolicitud.DatosPersonales = [$scope.newDatosPersonales];
            $scope.newDatosFinancieros.ActividadEconomica = $scope.TipoActividad;
            $scope.newSolicitud.DatosFinancieros = [$scope.newDatosFinancieros];

            $scope.newSolicitud.Soportes = $scope.Archivos;
            SeguroVidaAccidentesFactory.crearSolicitud($scope.newSolicitud).then(function (response) {
                if (response.data.MensajeValidacion !== null) {
                    $scope.setAlert('danger', response.data.MensajeValidacion);
                    $scope.addSolicitud.submitted = true;
                    $scope.esGuardado = false;
                }
                else {
                    $scope.addSolicitud.submitted = false;
                    $scope.setAlert('success', 'Gracias, su información ha sido enviada correctamente.');
                    limpiarCampos();
                    localStorage.removeItem("ruta");
                    localStorage.removeItem('filename');

                    //Envío de solicitud al hub - SignalR
                    var solicitud = { IdTipoSolicitud: response.data.IdTipoSolicitud, IdSolicitud: response.data.Id, NumeroSolicitud: response.data.CodigoIdSolicitud, NombreCompleto: response.data.DatosPersonales[0].Nombre + ' ' + response.data.DatosPersonales[0].Apellidos, FechaCreacion: response.data.FechaCreacion, IdPais: response.data.IdPais };
                    hub.sendsolicitud(solicitud);
                }
                grecaptcha.reset();
            });
        }, 2000);
    };

    $scope.getListas();
});