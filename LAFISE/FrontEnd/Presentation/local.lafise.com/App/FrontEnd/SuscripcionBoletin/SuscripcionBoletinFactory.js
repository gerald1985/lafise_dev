﻿/*==========================================================================
Archivo:            SuscripcionBoletinFactory
Descripción:        Objeto creado para implementar el llamado de los servicios 
                    del Formulario Suscripción a boletines
Autor:              steven.echavarria
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

angular.module("SuscripcionBoletinModule").factory("SuscripcionBoletinFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de crear una suscripción a boletin
    //Parametro entidad: recibe la entidad de suscripción a boletin
    var crearSuscripcion = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/SuscripcionBoletin/CrearSuscripcionBoletin"
        });
        return sendRequest(result);
    }

    //Función encargada de consultar una suscripción por su nombre y así validar su existencia
    //Parametro entidad: recibe la entidad de suscripción a boletin  
    var obtenerSuscripcionPorEmail = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/SuscripcionBoletin/ValidarSuscripcionPorEmail"
        });
        return sendRequest(result);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        crearSuscripcion: crearSuscripcion,
        obtenerSuscripcionPorEmail: obtenerSuscripcionPorEmail
    });

});