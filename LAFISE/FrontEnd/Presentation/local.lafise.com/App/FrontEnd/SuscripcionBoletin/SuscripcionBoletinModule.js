﻿angular.module('SuscripcionBoletinModule', ['ui.bootstrap', 'CargandoModule']);

angular.element(document).ready(function () {
    var suscripcionBoletinModule = document.getElementById("suscripcionBoletinModule");
    angular.bootstrap(suscripcionBoletinModule, ["SuscripcionBoletinModule"]);
});