﻿/*==========================================================================
Archivo:            SuscripcionBoletinController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del Formulario Suscripción a boletines
Autor:              steven.echavarria                    
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

angular.module("SuscripcionBoletinModule").controller("SuscripcionBoletinController", function ($scope, $uibModal, SuscripcionBoletinFactory) {

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.showMsg = false;
    $scope.showMsgEmail = false;

    $scope.Iniciar = function () {
        //Objeto para almacenar la suscripción a boletin
        $scope.newSuscripcion = {
            PathUrl: window.location.href,
            Nombre: '',
            Email: '',
            AceptoLey: false
        };
    }

    $scope.Iniciar();

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para validar que el correo
    $scope.FormatoEmail = function () {
        if ($scope.addSuscripcion.correo.$error.email != undefined && $scope.addSuscripcion.correo.$error.email) {
            $scope.msgValidarCorreo = 'El e-mail ' + document.getElementById('correo').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmail = true;
            $scope.newSuscripcion.Email = '';
        }
        else {
            $scope.showMsgEmail = false;
            $scope.UniqueEmail();
        }
    };

    //Función para esconder la validación del formato correo
    $scope.hideValidacion = function () {
        $scope.showMsgEmail = false;
    };

    //Función para validar que el nombre de la casa comercial sea única en la bd al momento de insertar o actualizar.
    $scope.UniqueEmail = function () {
        SuscripcionBoletinFactory.obtenerSuscripcionPorEmail(this.newSuscripcion).then(function (response) {
            if (response.data != null) {
                $scope.setAlert('danger', 'El email ' + response.data.Email + ' ya se encuentra registrado');
                $scope.newSuscripcion.Email = '';
            }
            else {
                $scope.closeAlert(0);
            }
        });
    };


    //Función para insertar una solicitud de apertura de cuenta en la base de datos.
    $scope.add = function () {
        if ($scope.addSuscripcion.$valid) {

            if ($scope.newSuscripcion.AceptoLey != false) {
                SuscripcionBoletinFactory.crearSuscripcion(this.newSuscripcion).then(function (response) {
                    $scope.addSuscripcion.submitted = false;
                    $scope.setAlert('success', 'Gracias, su información ha sido enviada correctamente.');
                    limpiarCampos();
                });
            } else { $scope.setAlert('danger', 'La suscripción no ha podido ser enviada. Debe aceptar la ley de tratamiento y uso de datos de LAFISE  '); }
        }
        else { $scope.addSuscripcion.submitted = true; }
    };

    //Función para limpiar los campos de la solicitud de prestamo hipotecario
    var limpiarCampos = function () {
        $scope.Iniciar();
    };

});