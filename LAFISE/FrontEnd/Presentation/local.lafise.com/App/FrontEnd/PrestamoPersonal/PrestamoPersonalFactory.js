﻿/*==========================================================================
Archivo:            PrestamoPersonalFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de crear los prestamos personales
Autor:              paola.munoz                         
Fecha de creación:  23/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("PrestamoPersonalModule").factory("PrestamoPersonalFactory", function ($http, $q) {

    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de crear un prestamo personal
    //Parametro entidad: recibe la entidad de prestamo personal
    var crearPrestamoPersonal = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Solicitud/CrearSolicitud"
        });
        return sendRequest(result);
    }

    //Función para obtener el rango sueldo por el pais correspondiente al portal.
    var obtenerRangoSueldoPorPais = function (url) {
        var result = $http({
            method: "GET",
            url: appContext + "/RangoSueldo/ConsultarRangoSueldoPais?url=" + url
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de garantias.
    var obtenerGarantias = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/Garantia/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }

    //Metodo para consultar el simbolo de moneda
    var obtenerSimboloMoneda = function () {
        var result = $http({
            method: 'GET',
            url: appContext + '/Moneda/ObtenerSimboloMoneda?url=' + window.location.href
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        crearPrestamoPersonal: crearPrestamoPersonal,
        obtenerRangoSueldoPorPais: obtenerRangoSueldoPorPais,
        obtenerGarantias: obtenerGarantias,
        obtenerSimboloMoneda: obtenerSimboloMoneda
    });

});