﻿angular.module("MarcaAutoModule").controller("MarcaAutoController", function ($scope, $filter, $timeout, Upload, MarcaAutoFactory, Hub) {

    $scope.esGuardado = false;

    $scope.Iniciar = function () {

        if (window.location.href.includes('blp')) {
            $scope.verLeyenda = true
        } else { $scope.verLeyenda = false }

        //Objeto para almacenar el prestamo hipotecario
        $scope.newSolicitud = {
            IdTipoSolicitud: 1,
            PathUrl: window.location.href,
            DatosPersonales: [],
            DatosFinancieros: [],
            PlanFinanciacion: [],
            Soportes: []
        };

        //Lista de los datos personales
        $scope.newDatosPersonales = {
            Nombre: '',
            Apellidos: '',
            Telefono: '',
            Email: '',
            Masculino: '',
            Nacionalidad: '',
            CiudadResidencia: '',
            IdTipoIdentificacion: '',
            NumeroIdentificacion: '',
            Profesion: '',
            EsCodeudor: false,

        };

        //Lista de los datos financieros
        $scope.newDatosFinancieros = {
            EsCliente: false,
            PoseeNomina: false,
            NegocioPropio: false,
            Asalariado: false,
            PromedioIngresoSelected: '',
            PromedioIngresoMensual: '',
            PromedioIngresoMensualNombre: '',
            ContainerPoseeNomina: true,
            ArchivosNP: '',
            ArchivosSI: ''
        };

        //Lista del plan de financiamiento
        $scope.newPlanFinanciamiento = {
            ValorBien: '',
            MontoFinanciar: '',
            PrimaAnticipo: '',
            Plazo: '',
            AutoUsado: false,
            OpcionOtro: false,
            IdCategoria: '',
            IdMarcaAuto: '',
            IdModelo: '',
            IdCasaComercial: '',
            OtroMarcaAuto: '',
            OtroModelo: '',
            OtroCasaComercial: '',
            selectedItem: '',
            ValorBienFormato: '',
            MontoFinanciarFormato: '',
            PrimaAnticipoFormato: '',
            MinimoPrima: ''

        };

        $scope.ListaArchivosNP = [];
        $scope.ListaArchivosSI = [];

        $scope.AutorizoLeyenda = { Autorizo: false };
    }

    $scope.Iniciar();

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.PromIngresosChanged = function (item) {
        $scope.newDatosFinancieros.PromedioIngresoMensualNombre = item.RangoSueldo;
    }

    $scope.uploadFiles = function (files, error, type) {
        angular.forEach(files, function (value, key) {
            if (value.name != '') {
                if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                    $scope.ListaArchivosNP.push({ Nombre: value.name, IdTipoSoporte: 2, Ruta: '', EsCodeudor: false, showAlert: false, file: value, tipo: 'NP', RutaFisica: '' });
                }
                if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                    $scope.ListaArchivosSI.push({ Nombre: value.name, IdTipoSoporte: 1, Ruta: '', EsCodeudor: false, showAlert: false, file: value, tipo: 'SI', RutaFisica: '' });
                }
            }
        });

        angular.forEach(error, function (value, key) {
            switch (value.$error) {
                case 'maxSize':
                    {
                        if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                            $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                        }
                        if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                            $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                        }
                        break;
                    }
                case 'pattern':
                    {
                        if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                            $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                        }
                        if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                            $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                        }
                        break;
                    }
            }
        });
    }


    $scope.PoseeNominaChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = !state;
    }

    $scope.EsClienteChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = state;
    }

    $scope.deleteFile = function (index, type, esError) {
        if (type === 'NP') {
            if ($scope.ListaArchivosNP.length == 1) {
                $scope.newPlanFinanciamiento.ArchivosNP = '';
                $scope.addSolicitud.archivoNP.$setValidity('required', false);
                $scope.ListaArchivosNP.splice(index, 1);
            }
            else
                $scope.ListaArchivosNP.splice(index, 1);
        }

        if (type === 'SI') {
            if ($scope.ListaArchivosSI.length == 1) {
                $scope.newPlanFinanciamiento.ArchivosSI = '';
                $scope.addSolicitud.archivoSI.$setValidity('required', false);
                $scope.ListaArchivosSI.splice(index, 1);
            } else
                $scope.ListaArchivosSI.splice(index, 1);
        }
    }

    $scope.msgValidarCorreo = '';
    $scope.showMsgEmail = false;
    //Función para validar que el correo
    $scope.FormatoEmail = function () {
        if ($scope.addSolicitud.correo.$error.email != undefined && $scope.addSolicitud.correo.$error.email) {
            $scope.msgValidarCorreo = 'El e-mail ' + document.getElementById('correo').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmail = true;
            $scope.newDatosPersonales.Email = '';
        } else {
            $scope.showMsgEmail = false;
        }
    };

    $scope.hideValidacion = function () {
        $scope.showMsgEmail = false;
    }

    $scope.getListas = function () {
        $scope.rangoSueldoPais = [];
        $scope.categorias = [];
        $scope.marcas = [];
        MarcaAutoFactory.obtenerRangoSueldoPorPais($scope.newSolicitud.PathUrl).then(function (response) {
            $scope.rangoSueldoPais = response;
        });
        MarcaAutoFactory.obtenerCategorias().then(function (response) {
            $scope.categorias = response;
        });

        MarcaAutoFactory.obtenerMarcasActivasPorPais(window.location.href).then(function (response) {
            $scope.marcas = response;
        });
        localStorage.clear();
    }

    $scope.SeleccionarMarca = function (item) {
        $scope.modelos = [];
        $scope.newPlanFinanciamiento.IdMarcaAuto = item.Id;
        if (item.Marca === 'Otro') {
            $scope.newPlanFinanciamiento.OpcionOtro = true;
        } else {
            MarcaAutoFactory.obtenerModelos($scope.newPlanFinanciamiento.IdMarcaAuto).then(function (response) {
                $scope.modelos = response;
            });
            $scope.newPlanFinanciamiento.OpcionOtro = false;
        }
    }

    $scope.SeleccionarModelo = function (IdMod) {
        if (IdMod != undefined) {
            $scope.CasasComerciales = [];
            $scope.entidad =
            {
                IdModelo: IdMod,
                pathIdPais: window.location.href
            };
            MarcaAutoFactory.obtenerCasasComercialesModeloPais($scope.entidad).then(function (response) {
                $scope.CasasComerciales = response;
            });
        }
    }

    //Función para cancelar la solicitud de prestamo hipotecario
    $scope.cancel = function (captcha) {
        limpiarCampos();
    };

    //Función para limpiar los campos de la solicitud de prestamo hipotecario
    var limpiarCampos = function () {
        $scope.newDatosPersonales = {};
        $scope.newDatosFinancieros = {};
        $scope.newPlanFinanciamiento = {};
        $scope.ListaArchivosNP = [];
        $scope.ListaArchivosSI = [];
        $scope.esGuardado == false
        $scope.Iniciar();
    };


    $scope.SaveFiles = function (file, count) {
        if (file) {
            file.upload = Upload.upload({
                url: '/Uploads/UploadDocumentosMarcasHandler.ashx',
                data: { file: file }
            });

            file.upload.then(function (response) {
                //$timeout(function () {
                file.result = response.data;
                if (response.data.ruta != '') {
                    localStorage.setItem("ruta", response.data.ruta);
                    localStorage.setItem("filename", response.data.filename);

                }
                //});
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt, response) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));

                if (file.progress === 100) {
                    if (count == 1 && $scope.esGuardado == false) {
                        $scope.guardarSolicitud();
                        $scope.esGuardado = true;
                    }
                }
            });
        }

    };


    //Función para insertar una solicitud de marca auto en la base de datos.
    $scope.add = function (captcha) {
        if ($scope.addSolicitud.$valid) {

            $scope.Archivos = $scope.ListaArchivosNP.concat($scope.ListaArchivosSI);
            var count = $scope.Archivos.length;

            if (count == 0) {
                $scope.guardarSolicitud();
            }
            else {
                angular.forEach($scope.Archivos, function (value, key) {
                    if (value.file != '') {
                        $scope.SaveFiles(value.file, count)
                        count--;
                    }
                });
            }
        }
        else {
            $scope.addSolicitud.submitted = true;
            $scope.setAlert('danger', 'La solicitud no ha podido ser enviada. por favor revise los campos requeridos');
        }
    };

    //Llamado al hub - SignalR
    var hub = new Hub('gestorBandejaEntradaHub', {

        //server side methods
        methods: ['sendsolicitud'],

        errorHandler: function (error) {
            console.error(error);
        }
    });

    //Función encargada de guardar la solicitud
    $scope.guardarSolicitud = function () {
        setTimeout(function () {
            angular.forEach($scope.Archivos, function (value, key) {
                if (localStorage.ruta != undefined && localStorage.filename != undefined) {
                    value.Ruta = localStorage.ruta + value.file.name;
                    value.RutaFisica = localStorage.filename + value.file.name;
                }
            });
            $scope.newDatosPersonales.Masculino = $scope.newDatosPersonales.Masculino == "true" ? true : false;
            $scope.newSolicitud.DatosPersonales = [$scope.newDatosPersonales];
            $scope.newSolicitud.DatosFinancieros = [$scope.newDatosFinancieros];
            $scope.newPlanFinanciamiento.ValorBien = $('#ValorBien').autoNumeric('get');
            $scope.newPlanFinanciamiento.MontoFinanciar = $('#MontoFinanciar').autoNumeric('get');
            $scope.newPlanFinanciamiento.PrimaAnticipo = $('#PrimaAnticipo').autoNumeric('get');
            $scope.newSolicitud.PlanFinanciacion = [$scope.newPlanFinanciamiento];
            $scope.newSolicitud.Soportes = $scope.Archivos;
            MarcaAutoFactory.crearSolicitud($scope.newSolicitud).then(function (response) {
                if (response.data.MensajeValidacion !== null) {
                    if (response.data.TipoValidacion != undefined && response.data.TipoValidacion === 'OperacionErroneaPrima') {
                        $('#minimoprima').autoNumeric('init');
                        var valorminimo = $('#minimoprima').autoNumeric('set', response.data.MinimoPrima);
                        $scope.setAlert('danger', "El valor de la prima deberá ser mayor o igual a " + $(valorminimo).val());
                        $scope.addSolicitud.submitted = true;
                    }
                    else {
                        $scope.setAlert('danger', response.data.MensajeValidacion);
                        $scope.addSolicitud.submitted = true;
                    }
                    $scope.esGuardado = false;
                }
                else {
                    $scope.addSolicitud.submitted = false;
                    $scope.setAlert('success', 'Gracias, su información ha sido enviada correctamente.');
                    limpiarCampos();
                    localStorage.removeItem("ruta");
                    localStorage.removeItem('filename');


                    //Envío de solicitud al hub - SignalR
                    var solicitud = { IdTipoSolicitud: response.data.IdTipoSolicitud, IdSolicitud: response.data.Id, NumeroSolicitud: response.data.CodigoIdSolicitud, NombreCompleto: response.data.DatosPersonales[0].Nombre + ' ' + response.data.DatosPersonales[0].Apellidos, FechaCreacion: response.data.FechaCreacion, IdPais: response.data.IdPais };
                    hub.sendsolicitud(solicitud);
                }
                grecaptcha.reset();
            });
        }, 2000);
    };

    //Verificar si algún checkbox fue seleccionado
    $scope.someSelected = function (object) {
        return Object.keys(object).some(function (key) {
            return object[key];
        });
    }

    $scope.getListas();


});