﻿/*==========================================================================
Archivo:            IndicadorController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    de los indicadores 
Autor:              carlos.arboleda                        
Fecha de creación:  17/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

angular.module("IndicadorModule").controller("IndicadorController", function ($scope, i18nService, IndicadorFactory) {
    //Traducir el grid
    i18nService.setCurrentLang('es');
    //Grid para mostrar la tabla de valores históricos
    $scope.showGridHistorico = false;
    //Grid para mostrar la tabla de valores actuales
    $scope.showGridValoresActuales = true;
    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {
        IndicadorFactory.obtenerIndicadorActivos().then(function (response) {
            $scope.gridOpts.data = response;
        });
    };

    //Objeto para el manejo de las propieades de las gráficas y tablas de históricos
    $scope.filtros = {
        Simbolo: '',
        FechaInicial: '',
        FechaFinal: '',
        NombreIndicador: ''
    };

    //Configuración del Grid de valores actuales
    $scope.gridOpts = {
        columnDefs: [
        { name: 'Nombre', field: 'Nombre', displayName: 'Nombre' },
        { name: 'ValorActual', field: 'ValorActual', displayName: 'Valor Actual' },
        { name: 'Variacion', field: 'Variacion', displayName: 'Variación' },
        { name: 'Fecha', field: 'Fecha', displayName: 'Fecha' },
        { name: 'Hora', field: 'Hora', displayName: 'Hora' },
        { field: 'image_url', cellTemplate: '<input class="verHistoricoGrid" title="Ver Histórico" type="button" value="Ver Histórico" ng-click="grid.appScope.historico(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Histórico' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
    };

    //Configuración del Grid de históricos
    $scope.gridOptsHistorico = {
        columnDefs: [
        { name: 'Fecha', field: 'Fecha', displayName: 'Fecha' },
        { name: 'Apertura', field: 'Apertura', displayName: 'Apertura' },
        { name: 'MasAlto', field: 'MasAlto', displayName: 'Más Alto' },
        { name: 'MasBajo', field: 'MasBajo', displayName: 'Más Bajo' },
        { name: 'Cierre', field: 'Cierre', displayName: 'Cierre' },
        { name: 'Volumen', field: 'Volumen', displayName: 'Volumen' },
         { name: 'CierreAjustado', field: 'CierreAjustado', displayName: 'Cierre Ajustado' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true
    };

    //Función para ver los valores históricos de un indicador
    $scope.historico = function (entidad) {
        $scope.filtros.Simbolo = entidad.Simbolo;
        $scope.filtros.NombreIndicador = entidad.Nombre;
        $scope.filtros.FechaInicial = $scope.generarFechaInicial(30);
        $scope.filtros.FechaFinal = new Date();
        //asignamos las restricciones de las fechas
        $scope.minDateFinal = $scope.filtros.FechaInicial;
        $scope.maxDateInicial = $scope.filtros.FechaFinal;
        $scope.maxDateFinal = $scope.filtros.FechaFinal;
        //llamamos la función para consultar el servicio
        var resp = entidad.Simbolo;
        if (resp == false || resp == '') {

            return false;
        }
        else
        {
            $scope.consultarHistorico($scope.filtros);
            $scope.showGridValoresActuales = false;
        }

    }

    //Función para generar la fecha inicial que se cuenta n días atrás a partir de la fecha actual 
    $scope.generarFechaInicial = function (dias) {
        var milisegundos = parseInt(35 * 24 * 60 * 60 * 1000);
        var fecha = new Date();
        var day = fecha.getDate();
        // el mes es devuelto entre 0 y 11
        var month = fecha.getMonth() + 1;
        var year = fecha.getFullYear();
        //Obtenemos los milisegundos desde media noche del 1/1/1970
        var tiempo = fecha.getTime();
        //Calculamos los milisegundos sobre la fecha que hay que sumar o restar
        milisegundos = parseInt((dias * -1) * 24 * 60 * 60 * 1000);
        //Modificamos la fecha actual
        var total = fecha.setTime(tiempo + milisegundos);
        day = fecha.getDate();
        month = fecha.getMonth() + 1;
        year = fecha.getFullYear();
        if (month < 10) {
            month = "0" + month;
        }
        if (day < 10) {
            day = "0" + day;
        }
        //creamos el string de fecha en el formato requerido
        var fechaInicial = year + "-" + month + "-" + day;
        return fechaInicial;
    }

    //Función para crear la gráfica de históricos con highstock.js
    $scope.crearGraficaHistorico = function (data, nombreIndicador) {
        //Configuración de la gráfica
        $scope.chartConfig = {

            options: {
                chart: {
                    borderWidth: 1,
                    plotBorderWidth: 1,
                    spacingBottom: 50,
                    animation: true
                },
                rangeSelector: {
                    buttons: [
                 {
                     type: 'all',
                     count: 1,
                     text: 'Todo'
                 }],
                    inputEnabled: false, 
                    selected: 1 
                },
                scrollbar: {
                    enabled: true
                },
                navigator: {
                    enabled: false
                },
                colors: ['#228B22'],
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Históricos para ' + nombreIndicador
                },
                xAxis: {
                    type: 'datetime',
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
            },
            series: [],
            useHighStocks: true
        }
        $scope.chartConfig.series.push(
            {
            id: 1,
            name: 'Ciere',
            data: $scope.generarArray(data),
            tooltip: {
                valueDecimals: 2,
                valuePrefix: '$',
                xDateFormat: '%Y-%m-%d',
                shared : false
            }}
        );
    }

    //Funcion para generar y retornar un array de arrays que servirá como fuente de datos para la creacion de la gráfica
    $scope.generarArray = function (data) {
        var dataToSend = [];
        for (var i = 0; i < data.length; i++) {
            var date = new Date(data[i].Fecha).getTime();
            var value = parseFloat(data[i].Cierre);;
                 
            dataToSend.push([
                date, value
            ]);
        }
        return dataToSend;
    }

    //Función para manipular la apertura del calendario de fecha final
    $scope.openFinal = function ($event) {
        $scope.status.openedFinal = true;
    };

    //Función para manipular la apertura del calendario de fecha inicial
    $scope.openInicial = function ($event) {
        $scope.status.openedInicial = true;
    };

    //atributo para identificar si un calenadrio está abierto o no
    $scope.status = {
        opened: false
    };

    //Función para validar la fecha mínima de la fecha final
    $scope.seleccionFechaInicial = function () {
        $scope.minDateFinal = $scope.filtros.FechaInicial;
    }

    //Función para validar la fecha máxima de la fecha inicial
    $scope.seleccionFechaFinal = function () {
        $scope.maxDateInicial = $scope.filtros.FechaFinal;
    }

    //Función para generar de nuevo la tabla y la gráfica de históricos con los valores seleccionados en los calendarios
    $scope.generar = function () {
        $scope.consultarHistorico($scope.filtros);
    }

    //Función para mostrar la tabla de los valores actuales y ocultar la gráfica y tabla de los históricos
    $scope.regresar = function () {
        $scope.showGridHistorico = false;
        $scope.showGridValoresActuales = true;
    }

    //Función para consultar los datos históricos de un indicador 
    $scope.consultarHistorico = function (filtro) {
        IndicadorFactory.obtenerHistoricoFiltro(filtro).then(function (response) {
            $scope.crearGraficaHistorico(response, filtro.NombreIndicador);
            $scope.gridOptsHistorico.data = response;
            $scope.showGridHistorico = true;
        })
    }

    $scope.getData();
});