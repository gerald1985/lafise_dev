﻿/*==========================================================================
Archivo:            IndicadorFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    indicadores
Autor:              arley.lopez                        
Fecha de creación:  15/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("IndicadorModule").factory("IndicadorFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de todos los indicadores activos . 
    var obtenerIndicadorActivos = function () {
        var result = $http({
            method: "get",
            url: appContext + "/Indicador/VerTodosActivos"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta del historico de los indicadores.
    //Parametro entidad: 
    var obtenerHistoricoFiltro = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Indicador/VerHistorico"
        });
        return result.then(handleSuccess, handleError);
    }
    

    
    //Retorna cada una de las funciones del factory
    return ({
        obtenerIndicadorActivos: obtenerIndicadorActivos,
        obtenerHistoricoFiltro: obtenerHistoricoFiltro
    });

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

});
