﻿/*==========================================================================
Archivo:            TasaCambioController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del Formulario tasa de cambio
Autor:              steven.echavarria                    
Fecha de creación:  23/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

angular.module("TasaCambioModule").controller("TasaCambioController", function ($scope, TasaCambioFactory) {

    $scope.Iniciar = function () {
        //Objeto para llevar la información para consultar las tasas de cambio
        $scope.TasaCambio = {
            PathUrl: window.location.href,
            Descripcion: '',
            ValorCompra: '',
            ValorVenta: '',
            SimboloCompra: '',
            SimboloVenta:'',
            Activo: true,
            IdPais:-1
        };

        //Lista de casas comerciales.
        $scope.ListaTasa = [];

        TasaCambioFactory.obtenerTasaCambio(this.TasaCambio).then(function (response) {

            angular.forEach(response.data, function (value, key) {
                $scope.ListaTasa.push({ Descripcion: value.Descripcion, ValorCompra: value.ValorCompra, ValorVenta: value.ValorVenta });
            });
        });
    }

    $scope.Iniciar();
});