﻿/*==========================================================================
Archivo:            TasaCambioFactory
Descripción:        Objeto creado para implementar el llamado de los servicios 
                    del Formulario tasa de cambio
Autor:              paola.munoz
Fecha de creación:  25/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

angular.module("TasaCambioModule").factory("TasaCambioFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consultar las tasas de cambio teniendo en cuenta el portal
    //Parametro entidad: recibe la entidad de tasa de cambio  
    var obtenerTasaCambio = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/TasaCambio/VerPorPaisActivo"
        });
        return sendRequest(result);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerTasaCambio: obtenerTasaCambio
    });

});