﻿/*==========================================================================
Archivo:            PrestamoHipotecarioController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del Formulario Solicitud prestamos hipotecario
Autor:              steven.echavarria                    
Fecha de creación:  12/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("PrestamoHipotecarioModule").controller("PrestamoHipotecarioController", function ($scope, $timeout, Upload, PrestamoHipotecarioFactory, Hub) {

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.showMsg = false;
    $scope.showMsgEmail = false;
    $scope.esGuardado = false;

    $scope.Iniciar = function () {

        if (window.location.href.includes('blp')) {
            $scope.verLeyenda = true
        } else { $scope.verLeyenda = false }

        //Objeto para almacenar el prestamo hipotecario
        $scope.newSolicitud = {
            IdTipoSolicitud: 3,
            PathUrl: window.location.href,
            DatosPersonales: [],
            DatosFinancieros: [],
            PlanFinanciacion: [],
            Soportes: []
        };

        //Lista de los datos personales
        $scope.newDatosPersonales = {
            Nombre: '',
            Apellidos: '',
            Telefono: '',
            Email: '',
            Masculino: '',
            Nacionalidad: '',
            CiudadResidencia: '',
            IdTipoIdentificacion: '',
            NumeroIdentificacion: '',
            Profesion: '',
            EsCodeudor: false
        };

        //Lista de los datos financieros
        $scope.newDatosFinancieros = {
            EsCliente: false,
            PoseeNomina: false,
            NegocioPropio: false,
            Asalariado: false,
            PromedioIngresoSelected: '',
            PromedioIngresoMensualNombre: '',
            ContainerPoseeNomina: true,
            ArchivosNP: '',
            ArchivosSI: '',
            selectedItem: ''
        };

        //Lista del plan de financiamiento
        $scope.newPlanFinanciamiento = {
            ValorBien: '',
            MontoFinanciar: '',
            PrimaAnticipo: '',
            Plazo: '',
            ConstruccionPropia: false,
            IdCategoria: null,
            IdTipoPropiedad: null,
            IdUrbanizadora: null,
            ValorBienFormato: '',
            MontoFinanciarFormato: '',
            PrimaAnticipoFormato: ''
        };

        //Objeto usado como filtro para la búsqueda del porcentaje de prima por categoría y país
        $scope.filtroPorcentajePrima = {
            IdCategoria: '',
            Path: ''
        };

        $scope.AutorizoLeyenda = { Autorizo: false };
    }

    $scope.Iniciar();

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };


    //Función para validar que el correo
    $scope.FormatoEmail = function () {
        if ($scope.addSolicitud.correo.$error.email != undefined && $scope.addSolicitud.correo.$error.email) {
            $scope.msgValidarCorreo = 'El e-mail ' + document.getElementById('correo').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmail = true;
            $scope.newDatosPersonales.Email = '';
        } else {
            $scope.showMsgEmail = false;
        }
    };

    //Función para esconder la validación del formato correo
    $scope.hideValidacion = function () {
        $scope.showMsgEmail = false;
    };


    //Función para mostrar los campos categoria, tipo de propiedad y urbanizadora
    $scope.toggleConstruccionPropia = function () {
        $scope.newPlanFinanciamiento.IdCategoria = '';
        $scope.newPlanFinanciamiento.IdTipoPropiedad = '';
        $scope.newPlanFinanciamiento.IdUrbanizadora = '';
    }


    //Sección Datos Financieros
    $scope.ListaArchivosNP = [];
    $scope.ListaArchivosSI = [];

    //Carga y valida los archivos a adjuntar
    $scope.uploadFiles = function (files, error, type) {
        angular.forEach(files, function (value, key) {
            if (value.name != '') {
                if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                    $scope.ListaArchivosNP.push({ Nombre: value.name, IdTipoSoporte: 2, Ruta: '', EsCodeudor: false, showAlert: false, file: value, tipo: 'NP', RutaFisica: '' });
                }
                if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                    $scope.ListaArchivosSI.push({ Nombre: value.name, IdTipoSoporte: 1, Ruta: '', EsCodeudor: false, showAlert: false, file: value, tipo: 'SI', RutaFisica: '' });
                }
            }
        });

        angular.forEach(error, function (value, key) {
            switch (value.$error) {
                case 'maxSize':
                    {
                        if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                            $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                        }
                        if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                            $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                        }
                        break;
                    }
                case 'pattern':
                    {
                        if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                            $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                        }
                        if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                            $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                        }
                        break;
                    }
            }
        });
    }

    //Función encargada de eliminar un archivo adjunto 
    $scope.deleteFile = function (index, type, esError) {
        if (type === 'NP') {
            if ($scope.ListaArchivosNP.length == 1) {
                $scope.newPlanFinanciamiento.ArchivosNP = '';
                $scope.addSolicitud.archivoNP.$setValidity('required', false);
                $scope.ListaArchivosNP.splice(index, 1);
            }
            else
                $scope.ListaArchivosNP.splice(index, 1);
        }

        if (type === 'SI') {
            if ($scope.ListaArchivosSI.length == 1) {
                $scope.newPlanFinanciamiento.ArchivosSI = '';
                $scope.addSolicitud.archivoSI.$setValidity('required', false);
                $scope.ListaArchivosSI.splice(index, 1);
            } else
                $scope.ListaArchivosSI.splice(index, 1);
        }
    }

    //Función encargada de alamacenar los archivos adjuntos en una carpeta fisica.
    $scope.SaveFiles = function (file, count) {
        if (file) {
            file.upload = Upload.upload({
                url: '/Uploads/UploadDocumentosPrestamosHipotecariosHandler.ashx',
                data: { file: file }
            });

            file.upload.then(function (response) {
                //$timeout(function () {
                file.result = response.data;
                if (response.data.ruta != '') {
                    localStorage.setItem("ruta", response.data.ruta);
                    localStorage.setItem("filename", response.data.filename);

                }
                //});
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt, response) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));

                if (file.progress === 100) {
                    if (count == 1 && $scope.esGuardado == false) {
                        $scope.guardarSolicitud();
                        $scope.esGuardado = true;
                    }
                }
            });
        }
    };

    //Función encargada de obtener el valor texto del item seleccionado en la lista de rangos de sueldo
    $scope.PromIngresosChanged = function (item) {
        if (typeof item !== "undefined" && item != '') {
            $scope.newDatosFinancieros.PromedioIngresoMensualNombre = item.RangoSueldo;
        }
    }

    //Función encargada de poner visible o no la selección de Promedio de ingresos mensuales según la opción Posee Nomina
    $scope.PoseeNominaChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = !state;
    }

    //Función encargada de poner visible o no la selección de Promedio de ingresos mensuales según la opción Es Cliente
    $scope.EsClienteChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = state;
    }

    //Método para poblar las listas de selección - Categoria, Tipo de propiedad y Urbanizadora
    var getListas = function () {
        $scope.rangoSueldoPais = [];
        $scope.categorias = [];
        $scope.tiposPropiedad = [];
        $scope.urbanizadoras = [];
        //Servicio para obtener los rangos de sueldo según el país
        PrestamoHipotecarioFactory.obtenerRangoSueldoPorPais($scope.newSolicitud.PathUrl).then(function (response) {
            $scope.rangoSueldoPais = response;
        });
        //Servicio para obtener las categorias
        PrestamoHipotecarioFactory.obtenerCategorias($scope.newSolicitud.IdTipoSolicitud).then(function (response) {
            $scope.categorias = response;
        });
        //Servicio para obtener los tipos de propiedad
        PrestamoHipotecarioFactory.obtenerTiposPropiedad().then(function (response) {
            $scope.tiposPropiedad = response;
        });
        //Servicio para obtener las urbanizadoras
        PrestamoHipotecarioFactory.obtenerUrbanizadorasPorPais(window.location.href).then(function (response) {
            $scope.urbanizadoras = response;
        });
    };



    //Función para insertar una solicitud de prestamo hipotecario en la base de datos.
    $scope.add = function (captcha) {

        if ($scope.addSolicitud.$valid) {

            $scope.Archivos = $scope.ListaArchivosNP.concat($scope.ListaArchivosSI);
            var count = $scope.Archivos.length;

            if (count == 0) {
                $scope.guardarSolicitud();
            }
            else {
                angular.forEach($scope.Archivos, function (value, key) {
                    if (value.file != '') {
                        $scope.SaveFiles(value.file, count)
                        count--;
                    }
                });
            }
        }
        else {
            $scope.addSolicitud.submitted = true;
            $scope.setAlert('danger', 'La solicitud no ha podido ser enviada. por favor revise los campos requeridos');
        }
    };


    //Llamado al hub - SignalR
    var hub = new Hub('gestorBandejaEntradaHub', {

        //server side methods
        methods: ['sendsolicitud'],

        errorHandler: function (error) {
            console.error(error);
        }
    });


    //Función encargada de guardar la solicitud
    $scope.guardarSolicitud = function () {
        setTimeout(function () {
            angular.forEach($scope.Archivos, function (value, key) {
                if (localStorage.ruta != undefined && localStorage.filename != undefined) {
                    value.Ruta = localStorage.ruta + value.file.name;
                    value.RutaFisica = localStorage.filename + value.file.name;
                }
            });
            $scope.newDatosPersonales.Masculino = $scope.newDatosPersonales.Masculino == "true" ? true : false;
            $scope.newSolicitud.DatosPersonales = [$scope.newDatosPersonales];
            $scope.newSolicitud.DatosFinancieros = [$scope.newDatosFinancieros];
            $scope.newPlanFinanciamiento.ValorBien = $('#ValorBien').autoNumeric('get');
            $scope.newPlanFinanciamiento.MontoFinanciar = $('#MontoFinanciar').autoNumeric('get');
            $scope.newPlanFinanciamiento.PrimaAnticipo = $('#PrimaAnticipo').autoNumeric('get');
            $scope.newSolicitud.PlanFinanciacion = [$scope.newPlanFinanciamiento];
            $scope.newSolicitud.Soportes = $scope.Archivos;
            PrestamoHipotecarioFactory.crearSolicitud($scope.newSolicitud).then(function (response) {

                if (response.data.MensajeValidacion !== null) {
                    if (response.data.TipoValidacion != undefined && response.data.TipoValidacion === 'OperacionErroneaPrima') {
                        $('#minimoprima').autoNumeric('init');
                        var valorminimo = $('#minimoprima').autoNumeric('set', response.data.MinimoPrima);
                        $scope.setAlert('danger', "El valor de la prima deberá ser mayor o igual a " + $(valorminimo).val());
                        $scope.addSolicitud.submitted = true;
                    }
                    else {
                        $scope.setAlert('danger', response.data.MensajeValidacion);
                        $scope.addSolicitud.submitted = true;
                    }
                    $scope.esGuardado = false;
                }
                else {
                    $scope.addSolicitud.submitted = false;
                    $scope.setAlert('success', 'Gracias, su información ha sido enviada correctamente.');
                    limpiarCampos();
                    localStorage.removeItem("ruta");
                    localStorage.removeItem('filename');

                    //Envío de solicitud al hub - SignalR
                    var solicitud = { IdTipoSolicitud: response.data.IdTipoSolicitud, IdSolicitud: response.data.Id, NumeroSolicitud: response.data.CodigoIdSolicitud, NombreCompleto: response.data.DatosPersonales[0].Nombre + ' ' + response.data.DatosPersonales[0].Apellidos, FechaCreacion: response.data.FechaCreacion, IdPais: response.data.IdPais };
                    hub.sendsolicitud(solicitud);
                }
                grecaptcha.reset();
            });
        }, 2000);
    };

    //Función para cancelar la solicitud de prestamo hipotecario
    $scope.cancel = function (captcha) {
        limpiarCampos();
    };

    //Función para limpiar los campos de la solicitud de prestamo hipotecario
    var limpiarCampos = function () {
        $scope.AutorizoLeyenda = false;
        $scope.newDatosPersonales = {};
        $scope.newDatosFinancieros = {};
        $scope.newPlanFinanciamiento = {};
        $scope.ListaArchivosNP = [];
        $scope.ListaArchivosSI = [];
        $scope.esGuardado = false;
        $scope.Iniciar();
    };

    //Verificar si algún checkbox fue seleccionado
    $scope.someSelected = function (object) {
        return Object.keys(object).some(function (key) {
            return object[key];
        });
    }

    //Llamado al método para poblar las listas de selección - Categoria, Tipo de propiedad y Urbanizadora
    getListas();



    $scope.EnviarSolicitud = function () {
        var solicitud = { NumeroSolicitud: $scope.newSolicitud.NumeroSolicitud, NombreCompleto: $scope.newSolicitud.NombreCompleto, FechaCreacion: $scope.newSolicitud.FechaCreacion };
        console.log(solicitud);
        hub.sendsolicitud(solicitud);
    };

});
