﻿/*==========================================================================
Archivo:            PrestamoHipotecarioFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del Formulario Solicitud Prestamo Hipotecario
Autor:              steven.echavarria
Fecha de creación:  12/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("PrestamoHipotecarioModule").factory("PrestamoHipotecarioFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de categorias.
    var obtenerCategorias = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Categoria/VerTodosIdSolicitud?idTipoSolicitud=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de tipos de propiedad.
    var obtenerTiposPropiedad = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/TipoPropiedad/VerTodosActivos"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de urbanizadoras.
    var obtenerUrbanizadoras = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/Urbanizadora/VerTodosActivos"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de urbanizadoras por país.
    var obtenerUrbanizadorasPorPais = function (urlPais) {
        var result = $http({
            method: "GET",
            url: appContext + "/Urbanizadora/VerTodosActivosPorPais?pathIdPais=" + urlPais
        });
        return result.then(handleSuccess, handleError);
    }

    //Función para obtener el rango sueldo por el pais correspondiente al portal.
    var obtenerRangoSueldoPorPais = function (url) {
        var result = $http({
            method: "GET",
            url: appContext + "/RangoSueldo/ConsultarRangoSueldoPais?url=" + url
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta del porcentaje de prima por categoría y país
    //Parametro entidad: recibe las propiedades IdCategoria y IdPais necesarias para la búsqueda del porcentaje de prima
    var obtenerPorcentajePrima = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Solicitud/VerPorcentajePrimaPorCategoriaPais"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta del plazo de un tipo de propiedad
    //Parametro idTipoPropiedad: identificador del tipo de propiedad
    var obtenerPlazoTipoPropiedad = function (idTipoPropiedad) {
        var result = $http({
            method: "GET",
            url: appContext + "/Solicitud/VerPlazoPorTipoPropiedad?idTipoPropiedad=" + idTipoPropiedad
        });
        return result.then(handleSuccess, handleError);
    }

    var obtenerSimboloMoneda = function () {
        var result = $http({
            method: 'GET',
            url: appContext + '/Moneda/ObtenerSimboloMoneda?url=' + window.location.href
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de crear una solicitud de prestamo hipotecario
    //Parametro entidad: recibe la entidad de prestamo hipotecario
    var crearSolicitud = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Solicitud/CrearSolicitud"
        });
        return sendRequest(result);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("Ha ocurrido un error desconocido");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerCategorias: obtenerCategorias,
        obtenerTiposPropiedad: obtenerTiposPropiedad,
        obtenerUrbanizadoras: obtenerUrbanizadoras,
        obtenerUrbanizadorasPorPais: obtenerUrbanizadorasPorPais,
        obtenerRangoSueldoPorPais: obtenerRangoSueldoPorPais,
        obtenerPorcentajePrima: obtenerPorcentajePrima,
        obtenerPlazoTipoPropiedad: obtenerPlazoTipoPropiedad,
        obtenerSimboloMoneda: obtenerSimboloMoneda,
        crearSolicitud: crearSolicitud
    });

});