﻿/*==========================================================================
Archivo:            PrestamoEducativoController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del Formulario Solicitud prestamo educativo
Autor:              steven.echavarria                    
Fecha de creación:  19/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("PrestamoEducativoModule").controller("PrestamoEducativoController", function ($scope, $uibModal, Upload, PrestamoEducativoFactory, Hub) {

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.showMsg = false;
    $scope.showMsgEmail = false;
    $scope.showMsgEmailCodeudor = false;
    $scope.esGuardado = false;

    $scope.Iniciar = function () {
        //Objeto para almacenar el prestamo educativo
        $scope.newSolicitud = {
            IdTipoSolicitud: 6,
            PathUrl: window.location.href,
            DatosPersonales: [],
            DatosFinancieros: [],
            PlanFinanciacion: [],
            Soportes: []
        };

        //Lista de los datos personales
        $scope.newDatosPersonales = {
            Nombre: '',
            Apellidos: '',
            Telefono: '',
            Email: '',
            Masculino: '',
            Nacionalidad: '',
            CiudadResidencia: '',
            IdTipoIdentificacion: '',
            NumeroIdentificacion: '',
            Profesion: '',
            EsCodeudor: false
        };

        //Lista de los datos personales del codeudor
        $scope.newDatosPersonalesCodeudor = {
            Nombre: '',
            Apellidos: '',
            Telefono: '',
            Email: '',
            Masculino: '',
            Nacionalidad: '',
            CiudadResidencia: '',
            IdTipoIdentificacion: '',
            NumeroIdentificacion: '',
            Profesion: '',
            ArchivosNPCodeudor: '',
            ArchivosSICodeudor: '',
            EsCodeudor: true
        };

        //Lista de los datos financieros
        $scope.newDatosFinancieros = {
            EsCliente: false,
            PoseeNomina: false,
            NegocioPropio: false,
            Asalariado: false,
            PromedioIngresoSelected: '',
            PromedioIngresoMensualNombre: '',
            ContainerPoseeNomina: true,
            ArchivosNP: '',
            ArchivosSI: '',
            selectedItem: ''
        };

        //Lista del plan de financiamiento
        $scope.newPlanFinanciamiento = {
            CostoPrograma: '',
            CostoProgramaFormato: '',
            IdGarantia: '',
            Plazo: '',
            MontoFinanciar: '',
            MontoFinanciarFormato: ''
        };
    }

    $scope.Iniciar();

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };


    //Función para validar que el correo
    $scope.FormatoEmail = function () {
        if ($scope.addSolicitud.correo.$error.email != undefined && $scope.addSolicitud.correo.$error.email) {
            $scope.msgValidarCorreo = 'El e-mail ' + document.getElementById('correo').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmail = true;
            $scope.newDatosPersonales.Email = '';
        }
        else { $scope.showMsgEmail = false; }

        if ($scope.addSolicitud.correoCodeudor.$error.email != undefined && $scope.addSolicitud.correoCodeudor.$error.email) {
            $scope.msgValidarCorreoCodeudor = 'El e-mail ' + document.getElementById('correoCodeudor').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmailCodeudor = true;
            $scope.newDatosPersonalesCodeudor.Email = '';
        }
        else { $scope.showMsgEmailCodeudor = false; }
    };

    //Función para esconder la validación del formato correo
    $scope.hideValidacion = function (esCodeudor) {
        if (!esCodeudor) {
            $scope.showMsgEmail = false;
        }
        else { $scope.showMsgEmailCodeudor = false; }
    };


    //Sección Datos Financieros
    $scope.ListaArchivosNP = [];
    $scope.ListaArchivosSI = [];
    $scope.ListaArchivosNPCodeudor = [];
    $scope.ListaArchivosSICodeudor = [];

    //Carga y valida los archivos a adjuntar
    $scope.uploadFiles = function (files, error, type, esCodeudor) {
        angular.forEach(files, function (value, key) {
            if (value.name != '') {

                if (!esCodeudor) {
                    if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                        $scope.ListaArchivosNP.push({ Nombre: value.name, IdTipoSoporte: 2, Ruta: '', EsCodeudor: esCodeudor, showAlert: false, file: value, tipo: 'NP', RutaFisica: '' });
                    }
                    if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                        $scope.ListaArchivosSI.push({ Nombre: value.name, IdTipoSoporte: 1, Ruta: '', EsCodeudor: esCodeudor, showAlert: false, file: value, tipo: 'SI', RutaFisica: '' });
                    }
                }
                else {
                    if (type === 'NP' && $scope.ListaArchivosNPCodeudor.length < 10) {
                        $scope.ListaArchivosNPCodeudor.push({ Nombre: value.name, IdTipoSoporte: 2, Ruta: '', EsCodeudor: esCodeudor, showAlert: false, file: value, tipo: 'NP', RutaFisica: '' });
                    }
                    if (type === 'SI' && $scope.ListaArchivosSICodeudor.length < 10) {
                        $scope.ListaArchivosSICodeudor.push({ Nombre: value.name, IdTipoSoporte: 1, Ruta: '', EsCodeudor: esCodeudor, showAlert: false, file: value, tipo: 'SI', RutaFisica: '' });
                    }
                }
            }
        });

        angular.forEach(error, function (value, key) {
            switch (value.$error) {
                case 'maxSize':
                    {
                        if (!esCodeudor) {
                            if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                                $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                            }
                            if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                                $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                            }
                        }
                        else {
                            if (type === 'NP' && $scope.ListaArchivosNPCodeudor.length < 10) {
                                $scope.ListaArchivosNPCodeudor.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                            }
                            if (type === 'SI' && $scope.ListaArchivosSICodeudor.length < 10) {
                                $scope.ListaArchivosSICodeudor.push({ file: '', Nombre: value.name, tipo: type, Error: "El tamaño máximo permitido por archivo es de " + value.$errorParam, showAlert: true });
                            }
                        }
                        break;
                    }
                case 'pattern':
                    {
                        if (!esCodeudor) {
                            if (type === 'NP' && $scope.ListaArchivosNP.length < 10) {
                                $scope.ListaArchivosNP.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                            }
                            if (type === 'SI' && $scope.ListaArchivosSI.length < 10) {
                                $scope.ListaArchivosSI.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                            }
                        }
                        else {
                            if (type === 'NP' && $scope.ListaArchivosNPCodeudor.length < 10) {
                                $scope.ListaArchivosNPCodeudor.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                            }
                            if (type === 'SI' && $scope.ListaArchivosSICodeudor.length < 10) {
                                $scope.ListaArchivosSICodeudor.push({ file: '', Nombre: value.name, tipo: type, Error: "Solo acepta formato " + value.$errorParam, showAlert: true });
                            }
                        }
                        break;
                    }
            }
        });
    }


    //Función encargada de eliminar un archivo adjunto 
    $scope.deleteFile = function (index, type, esError, esCodeudor) {
        if (type === 'NP') {
            if (!esCodeudor) {
                if ($scope.ListaArchivosNP.length == 1) {
                    $scope.newDatosFinancieros.ArchivosNP = '';
                    $scope.addSolicitud.archivoNP.$setValidity('required', false);
                    $scope.ListaArchivosNP.splice(index, 1);
                }
                else
                    $scope.ListaArchivosNP.splice(index, 1);
            }
            else {
                if ($scope.ListaArchivosNPCodeudor.length == 1) {
                    $scope.newDatosPersonalesCodeudor.ArchivosNPCodeudor = '';
                    $scope.ListaArchivosNPCodeudor.splice(index, 1);
                }
                else
                    $scope.ListaArchivosNPCodeudor.splice(index, 1);
            }
        }

        if (type === 'SI') {
            if (!esCodeudor) {
                if ($scope.ListaArchivosSI.length == 1) {
                    $scope.newDatosFinancieros.ArchivosSI = '';
                    $scope.addSolicitud.archivoSI.$setValidity('required', false);
                    $scope.ListaArchivosSI.splice(index, 1);
                } else
                    $scope.ListaArchivosSI.splice(index, 1);
            }
            else {
                if ($scope.ListaArchivosSICodeudor.length == 1) {
                    $scope.newDatosPersonalesCodeudor.ArchivosSICodeudor = '';
                    $scope.ListaArchivosSICodeudor.splice(index, 1);
                }
                else
                    $scope.ListaArchivosSICodeudor.splice(index, 1);
            }
        }
    }

    //Función encargada de almacenar los archivos adjuntos en una carpeta fisica.
    $scope.SaveFiles = function (file, count) {
        if (file) {
            file.upload = Upload.upload({
                url: '/Uploads/UploadDocumentosPrestamosEducativosHandler.ashx',
                data: { file: file }
            });

            file.upload.then(function (response) {
                file.result = response.data;
                if (response.data.ruta != '') {
                    localStorage.setItem("ruta", response.data.ruta);
                    localStorage.setItem("filename", response.data.filename);
                }
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt, response) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));
                if (file.progress === 100) {
                    if (count == 1 && $scope.esGuardado == false) {
                        $scope.guardarSolicitud();
                        $scope.esGuardado = true;
                    }
                }
            });
        }
    };

    //Función encargada de obtener el valor texto del item seleccionado en la lista de rangos de sueldo
    $scope.PromIngresosChanged = function (item) {
        if (typeof item !== "undefined" && item != '') {
            $scope.newDatosFinancieros.PromedioIngresoMensualNombre = item.RangoSueldo;
        }
    }

    //Función encargada de poner visible o no la selección de Promedio de ingresos mensuales según la opción Posee Nomina
    $scope.PoseeNominaChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = !state;
    }

    //Función encargada de poner visible o no la selección de Promedio de ingresos mensuales según la opción Es Cliente
    $scope.EsClienteChanged = function (state) {
        $scope.newDatosFinancieros.ContainerPoseeNomina = state;
    }


    //Método para poblar la lista de selección - Garantias
    var getListas = function () {
        $scope.rangoSueldoPais = [];
        $scope.garantias = [];
        //Servicio para obtener los rangos de sueldo según el país
        PrestamoEducativoFactory.obtenerRangoSueldoPorPais($scope.newSolicitud.PathUrl).then(function (response) {
            $scope.rangoSueldoPais = response;
        });
        //Servicio para obtener las categorias
        PrestamoEducativoFactory.obtenerGarantias().then(function (response) {
            $scope.garantias = response;
        });
    };

    //Función para insertar una solicitud de prestamo educativo en la base de datos.
    $scope.add = function (captcha) {
        if ($scope.addSolicitud.$valid) {
            $scope.Archivos = $scope.ListaArchivosNP.concat($scope.ListaArchivosSI).concat($scope.ListaArchivosNPCodeudor).concat($scope.ListaArchivosSICodeudor);
            var count = $scope.Archivos.length;
            if (count == 0) {
                $scope.guardarSolicitud();
            }
            else {
                angular.forEach($scope.Archivos, function (value, key) {
                    if (value.file != '') {
                        $scope.SaveFiles(value.file, count)
                        count--;
                    }
                });
            }
        } else {
            $scope.addSolicitud.submitted = true;
            $scope.setAlert('danger', 'La solicitud no ha podido ser enviada. Por favor revise los campos requeridos');
        }
    };

    //Llamado al hub - SignalR
    var hub = new Hub('gestorBandejaEntradaHub', {

        //server side methods
        methods: ['sendsolicitud'],

        errorHandler: function (error) {
            console.error(error);
        }
    });

    //Funcíón encargada de guardar la solicitud
    $scope.guardarSolicitud = function () {
        setTimeout(function () {
            angular.forEach($scope.Archivos, function (value, key) {
                if (localStorage.ruta != undefined && localStorage.filename != undefined) {
                    value.Ruta = localStorage.ruta + value.file.name;
                    value.RutaFisica = localStorage.filename + value.file.name;
                }
            });
            $scope.newDatosPersonales.Masculino = $scope.newDatosPersonales.Masculino == "true" ? true : false;
            $scope.newSolicitud.DatosPersonales.push($scope.newDatosPersonales);
            if ($scope.newPlanFinanciamiento.IdGarantia == 2) {
                $scope.newSolicitud.DatosPersonales.push($scope.newDatosPersonalesCodeudor);
            }
            $scope.newSolicitud.DatosFinancieros = [$scope.newDatosFinancieros];
            $scope.newPlanFinanciamiento.CostoPrograma = $('#CostoPrograma').autoNumeric('get');
            $scope.newPlanFinanciamiento.MontoFinanciar = $('#MontoFinanciar').autoNumeric('get');
            $scope.newSolicitud.PlanFinanciacion = [$scope.newPlanFinanciamiento];
            $scope.newSolicitud.Soportes = $scope.Archivos;
            PrestamoEducativoFactory.crearSolicitud($scope.newSolicitud).then(function (response) {
                if (response.data.MensajeValidacion !== null) {
                    $scope.setAlert('danger', response.data.MensajeValidacion);
                    $scope.addSolicitud.submitted = true;
                    $scope.esGuardado = false;
                }
                else {
                    $scope.addSolicitud.submitted = false;
                    $scope.setAlert('success', 'Gracias, su información ha sido enviada correctamente.');
                    limpiarCampos();
                    localStorage.removeItem("ruta");
                    localStorage.removeItem('filename');

                    //Envío de solicitud al hub - SignalR
                    var solicitud = { IdTipoSolicitud: response.data.IdTipoSolicitud, IdSolicitud: response.data.Id, NumeroSolicitud: response.data.CodigoIdSolicitud, NombreCompleto: response.data.DatosPersonales[0].Nombre + ' ' + response.data.DatosPersonales[0].Apellidos, FechaCreacion: response.data.FechaCreacion, IdPais: response.data.IdPais };
                    hub.sendsolicitud(solicitud);
                }
                grecaptcha.reset();
            });
        }, 2000);
    };

    //Función para cancelar la solicitud de prestamo hipotecario
    $scope.cancel = function (captcha) {
        limpiarCampos();
    };

    //Función para limpiar los campos de la solicitud de prestamo hipotecario
    var limpiarCampos = function () {
        $scope.newDatosPersonales = {};
        $scope.newDatosPersonalesCodeudor = {};
        $scope.newDatosFinancieros = {};
        $scope.newPlanFinanciamiento = {};
        $scope.ListaArchivosNP = [];
        $scope.ListaArchivosSI = [];
        $scope.ListaArchivosNPCodeudor = [];
        $scope.ListaArchivosSICodeudor = [];
        $scope.esGuardado = false;
        $scope.Iniciar();
    };


    //Llamado al método para poblar las listas de selección - Rango Sueldo y Garantias
    getListas();

});