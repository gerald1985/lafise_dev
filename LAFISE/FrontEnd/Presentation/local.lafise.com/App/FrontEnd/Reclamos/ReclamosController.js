﻿/*==========================================================================
Archivo:            ReclamosController
Descripción:        Objeto creado para implementar la presentación de los 
                    reclamos usadas en los diferentes CUS
Autor:              paola.munoz                        
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("ReclamosModule").controller("ReclamosController", function ($scope, $rootScope, $filter, PaisFactory, CiudadFactory, ReclamosFactory, Hub) {

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.showMsg = false;
    $scope.Iniciar = function () {
        //Objeto para almacenar la solicitud
        $scope.newSolicitud = {
            IdTipoSolicitud: 10,
            PathUrl: window.location.href,
            DatosPersonales: [],
            Siniestro: []
        };

        //Lista de los datos personales
        $scope.newDatosPersonales = {
            Nombre: '',
            Apellidos: '',
            Telefono: '',
            Email: '',
            Masculino: '',
            Nacionalidad: '',
            CiudadResidencia: '',
            IdTipoIdentificacion: '',
            NumeroIdentificacion: '',
            Profesion: '',
            EsCodeudor: false
        };

        //Lista de siniestros
        $scope.newSiniestros = {
            NumeroPoliza: '',
            Fecha: '',
            Detalle: '',
            SiniestroCiudad: [],
            SiniestroPais: []
        };

        //capturar el pais del formulario
        $scope.newPais = {
            IdPais: ''
        };

        $scope.ciudadTemp = [];
    }

    $scope.Iniciar();

    //Método para obtener los datos del servicio y poblar las listas de selección.
    var getData = function () {
        $scope.paises = [];
        
        PaisFactory.obtenerPaises().then(function (response) {
            $scope.paises = response;
        });
    }

    //Mostrar las ciudades teniendo en cuenta el país seleccionado
    $scope.mostrarCiudad = function () {
        CiudadFactory.obtenerCiudadPorPais($scope.paisesSelected.Paises).then(function (response) {
            if (response != null) {
                $scope.ciudadPais = response;

                angular.forEach($scope.ciudadSelected.Ciudad, function (ciudadSelect, index) {
                    angular.forEach($scope.ciudadPais, function (idCiudad) {
                        if (parseInt(idCiudad.Id) == parseInt(ciudadSelect)) {
                            $scope.ciudadTemp.push(ciudadSelect);
                            return;
                        }
                    });
                });
                if ($scope.ciudadTemp.length > 0) {
                    $scope.ciudadSelected.Ciudad = [];
                    $scope.ciudadSelected.Ciudad = $scope.ciudadTemp;
                    $scope.ciudadTemp = [];
                } else {
                    $scope.ciudadSelected.Ciudad = $scope.ciudadTemp;
                }
                    
            } else {
                $scope.ciudadPais = null;
            }
        });
    };

    //Función para validar que el correo
    $scope.FormatoEmail = function () {
        if ($scope.addReclamos.correo.$error.email != undefined && $scope.addReclamos.correo.$error.email) {
            $scope.msgValidarCorreo = 'El e-mail ' + document.getElementById('correo').value + '  no tiene la estructura correcta, por favor validar';
            $scope.showMsgEmail = true;
            $scope.newDatosPersonales.Email = '';
        } else {
            $scope.showMsgEmail = false;
        }
    };

    //Función para esconder la validación del formato correo
    $scope.hideValidacion = function () {
        $scope.showMsgEmail = false;
    };

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta segun, el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };


    //Llamado al hub - SignalR
    var hub = new Hub('gestorBandejaEntradaHub', {

        //server side methods
        methods: ['sendsolicitud'],

        errorHandler: function (error) {
            console.error(error);
        }
    });

    //Función para insertar una reclamación en la base de datos.
    $scope.add = function (captcha) {
        
        if ($scope.newSiniestros.Fecha > new Date()) {
            $scope.newSiniestros.Fecha='';
        }

        if ($scope.addReclamos.$valid) {

            $scope.newSolicitud.DatosPersonales = [$scope.newDatosPersonales];

            $scope.newSiniestroPais = [];
            angular.forEach($scope.paisesSelected.Paises, function (idPais) {
                $scope.newSiniestroPais.push({
                    IdPais: idPais
                });
            });

            $scope.newSiniestroCuidad = [];
            angular.forEach($scope.ciudadSelected.Ciudad, function (idCiudad) {
                $scope.newSiniestroCuidad.push({
                    IdCiudad: idCiudad
                });
            });

            $scope.newSiniestros.SiniestroCiudad = $scope.newSiniestroCuidad;
            $scope.newSiniestros.SiniestroPais = $scope.newSiniestroPais;
            $scope.newSolicitud.Siniestro = [$scope.newSiniestros];
            ReclamosFactory.crearReclamos(this.newSolicitud).then(function (response) {
                $scope.addReclamos.submitted = false;
                $scope.setAlert('success', 'Gracias, su información ha sido enviada correctamente.');
                limpiarCampos();
                //Envío de solicitud al hub - SignalR
                var solicitud = { IdTipoSolicitud: response.data.IdTipoSolicitud, IdSolicitud: response.data.Id, NumeroSolicitud: response.data.CodigoIdSolicitud, NombreCompleto: response.data.DatosPersonales[0].Nombre + ' ' + response.data.DatosPersonales[0].Apellidos, FechaCreacion: response.data.FechaCreacion, IdPais: response.data.IdPais };
                hub.sendsolicitud(solicitud);
                grecaptcha.reset();
            });
        } else {
            $scope.setAlert('danger', 'La solicitud no ha podido ser enviada. Por favor revise los campos requeridos');
            $scope.addReclamos.submitted = true;
        }
    };


    //Función para limpiar los campos de la solicitud de prestamo hipotecario
    var limpiarCampos = function () {

        $scope.newDatosPersonales.Nombre = '';
        $scope.newDatosPersonales.Apellidos = '';
        $scope.newDatosPersonales.Telefono = '';
        $scope.newDatosPersonales.Email = '';
        $scope.newDatosPersonales.Masculino = '';
        $scope.newDatosPersonales.Nacionalidad = '';
        $scope.newDatosPersonales.CiudadResidencia = '';
        $scope.newDatosPersonales.IdTipoIdentificacion = '';
        $scope.newDatosPersonales.NumeroIdentificacion = '';
        $scope.newDatosPersonales.Profesion = '';
        $scope.newSiniestros.NumeroPoliza = '';
        $scope.newSiniestros.Fecha = '';
        $scope.newSiniestros.Detalle = '';
        $scope.paisesSelected.Paises = [];
        $scope.ciudadSelected.Ciudad = [];
        $scope.Iniciar();
    };


    //Picker
    $scope.today = function () {
        $scope.newSiniestros.Fecha = new Date();
    };
    $scope.today();
    $scope.open = function ($event) {
        $scope.status.opened = true;
    };
    $scope.status = {
        opened: false
    };

    $scope.maxDate = new Date();
    //Fin picker

    //Llamado a la función para que se llene las listas de selección.
    getData();


});