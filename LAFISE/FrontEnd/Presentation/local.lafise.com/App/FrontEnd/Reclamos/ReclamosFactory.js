﻿/*==========================================================================
Archivo:            ReclamosFactory
Descripción:        Objeto creado para implementar el llamado del servicio
                    encargado de crear los reclamos
Autor:              paola.munoz                         
Fecha de creación:  03/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("ReclamosModule").factory("ReclamosFactory", function ($http, $q) {

    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de crear un reclamo
    //Parametro entidad: recibe la entidad de reclamos
    var crearReclamos = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Solicitud/CrearSolicitud"
        });
        return sendRequest(result);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        crearReclamos: crearReclamos
    });

});