﻿/*==========================================================================
Archivo:            CasaComercialController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del CUS003 - Administrar casas comerciales
Autor:              steven.echavarria                        
Fecha de creación:  06/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("CasaComercialModule").controller("CasaComercialController", function ($scope, $rootScope, i18nService, $uibModal, CasaComercialFactory) {

    //Traduce el grid
    i18nService.setCurrentLang('es');

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = false;
    $scope.showMsg = false;
    $scope.isEdit = false;

    //Objeto que contiene las propiedades del filtro
    $scope.filtros = {
        Paises: '',
        Nombre: '',
        Marca: ''
    };

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newCasaComercial = {
        Id: '',
        Nombre: '',
        IdPais: '',
        Nuevo: 0,
        FechaCreacion: '',
        Usado: 0,
        Activo: '',
        listaModelos: [],
        ListaMarcasAuto: ''
    };

    //capturar el pais del formulario
    $scope.newPais = {
        IdPais: ''
    };

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Objeto que contiene los tipos de autos para crear la casa comercial
    $scope.tipoAutos = [{ Id: 1, Value: 1, TipoAuto: "Nuevo" }, { Id: 2, Value: 2, TipoAuto: "Usado" }];

    //Lista de tipos de autos seleccionadas para crear la casa comercial
    $scope.seleccionadosTipoAutos = [];

    //Función encargada de seleccionar o no cada item de la lista de tipo de autos para la posterior creación o edición
    $scope.isCheckedTipoAuto = function (tipo) {
        var items = $scope.seleccionadosTipoAutos;
        if (items != '') {
            for (var i = 0; i < items.length; i++) {
                if (tipo.Id == items[i].Id) {
                    return true;
                }
            }
        }
        return false;
    };

    //Función encargado de eliminar cada item que se deseleccione de la lista de los tipos de autos seleccionados
    $scope.updateSelectionTipoAuto = function ($event, id) {
        var checkbox = $event.target;
        if (!checkbox.checked) {
            var index = -1;
            var itemsUpdate = $scope.seleccionadosTipoAutos;
            for (var i = 0; i < itemsUpdate.length; i++) {
                if (id == itemsUpdate[i].Id) {
                    index = i;
                }
            }
            if (index != -1) {
                $scope.seleccionadosTipoAutos.splice(index, 1);
            }
        }
    };

    //Objeto usado para manipular el valor de las marcas a filtrar
    $scope.marcasSelected = [];

    //Objeto usado para manipular el valor de las marcas a crear
    $scope.marcasCreated = [];

    //Objeto usado para manipular el valor del nombre de la casa comercial a filtrar
    $scope.casaComercialSelected = '';

    //Lista de casas comerciales.
    $scope.casasComerciales = [];

    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {
        $scope.formMode = false;

        if (typeof $scope.paisesSelected !== "undefined" && typeof $scope.paisesSelected.Paises !== "undefined" && $scope.paisesSelected.Paises != '') {
            $scope.filtros.Paises = $scope.paisesSelected.Paises.join();
        }
        else { $scope.filtros.Paises = null; }

        $scope.filtros.Nombre = $scope.casaComercialSelected != '' ? $scope.casaComercialSelected : null;


        if (typeof $scope.marcasSelected !== "undefined" && typeof $scope.marcasSelected.Marcas !== "undefined" && $scope.marcasSelected.Marcas != '') {
            $scope.filtros.Marca = $scope.marcasSelected.Marcas.join();
        }
        else { $scope.filtros.Marca = null; }

        CasaComercialFactory.obtenerCasasComerciales($scope.filtros).then(function (response) {
            $scope.gridOpts.data = response;
            $scope.casasComerciales = response;
        })
    }

    //Función para el manejo de mensajes de validación al eliminar una urbanizadora
    $scope.open = function (entidad) {
        var modalInstance = $uibModal.open({
            animation: false,
            templateUrl: 'plantillaEliminarCasaComercial.html',
            scope: $scope,
            controller: 'ModalEliminarController',
            size: 'sm',
            resolve:
                {
                    Entity: function () {
                        return entidad;
                    }
                }
        });
    };

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.addCasaComercial.submitted = false;
        $scope.closeAlert(0);
    };

    //Configuración del Grid.
    $scope.gridOpts = {
        columnDefs: [
        { name: 'Pais', field: 'NombrePais', displayName: 'País' },
        { name: 'Nombre', field: 'Nombre', displayName: 'Casa comercial' },
        { name: 'Marcas', field: 'Marcas', displayName: 'Marcas' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activar/Desactivar" value="Activo/Inactivo" ng-click="grid.appScope.active(row.entity)">', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
    };

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "CasaComercial_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };

    //Función para validar que el nombre de la casa comercial sea única en la bd al momento de insertar o actualizar.
    $scope.UniqueName = function () {
        CasaComercialFactory.obtenerCasaComercialPorNombre($scope.newCasaComercial).then(function (response) {
            if (response != null) {
                $scope.setAlert('danger', 'La casa comercial ' + $scope.newCasaComercial.Nombre + ' ya existe, por favor validar');
                $scope.newCasaComercial.Nombre = '';
            }
            else {
                $scope.closeAlert(0);
            }
        });
    };


    // Adiciona nuevo objeto a la lista de modelos.
    $scope.addMarcasModelos = function () {
        $scope.filtros.Marca = $scope.marcasCreated.Marcas.join();
        CasaComercialFactory.obtenerModelosPorMarcas($scope.filtros.Marca).then(function (response) {
            $scope.modelos = response;
            $scope.closeAlert(0);
        });
    }

    //Elimina un ubjeto de la lista de modelos
    $scope.deleteMarcaModelo = function (index, item) {
        var algo = item;
        $scope.modelos.splice(index, 1);
        var indexSeleccionado = _.findLastIndex($scope.seleccionadas.modelos, { IdMarcaAuto: item.Id });
        if (indexSeleccionado != -1) {
            $scope.seleccionadas.modelos.splice(indexSeleccionado, 1);
        }
    }


    //Función para insertar una casa comercial en la base de datos.
    $scope.add = function () {
        if ($scope.addCasaComercial.$valid) {
            if ($scope.modelos.length > 0) {
                $scope.newCasaComercial.Nuevo = 0;
                $scope.newCasaComercial.Usado = 0;
                $scope.seleccionadosTipoAutos.forEach(function (item, index) {
                    switch (item.Id) {
                        case 1:
                            $scope.newCasaComercial.Nuevo = 1;
                            break;
                        case 2:
                            $scope.newCasaComercial.Usado = 1;
                            break;
                    }
                });
                $scope.newCasaComercial.IdPais = $scope.newPais.IdPais;
                $scope.newCasaComercial.Activo = true;
                $scope.listaCasaComercialModelo = [];
                $scope.ListaIdMarcasSeleccionados = [];
                $scope.seleccionadas.modelos.forEach(function (item, index) {
                    $scope.listaCasaComercialModelo.push({ IdModeloAuto: item.Id });
                    $scope.ListaIdMarcasSeleccionados.push(item.IdMarcaAuto);
                });
                $scope.newCasaComercial.listaModelos = $scope.listaCasaComercialModelo;


                $scope.ListaIdMarcasTodos = [];
                $scope.modelos.forEach(function (item, index) {
                    item.listaModelos.forEach(function (item, index) {
                        $scope.ListaIdMarcasTodos.push(item.IdMarcaAuto);
                    });
                });

                var listaMarcasDifference = _.difference($scope.ListaIdMarcasTodos, $scope.ListaIdMarcasSeleccionados);
                var listaMarcasNoSelecccionadas = _.uniq(listaMarcasDifference);


                $scope.NombreMarcas = [];
                if (listaMarcasNoSelecccionadas.length > 0) {
                    CasaComercialFactory.obtenerMarcasPorId(listaMarcasNoSelecccionadas).then(function (response) {
                        response.forEach(function (item, index) {
                            $scope.NombreMarcas.push(item.Marca);
                        });
                        if ($scope.NombreMarcas.length > 0) {
                            $scope.setAlert('danger', 'Se debe agregar al menos un modelo para la(s) siguientes marcas: ' + $scope.NombreMarcas.join(', ') + ', por favor validar');
                            $scope.addCasaComercial.submitted = true;
                        }
                    });
                }
                else {
                    //Servicio que crea la casa comercial 
                    CasaComercialFactory.crearCasaComercial($scope.newCasaComercial).then(function (response) {
                        if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                            $scope.setAlert('danger', 'La casa comercial ' + $scope.newCasaComercial.Nombre + ' ya existe, por favor validar');
                        }
                        else {
                            $scope.formMode = false;
                            $scope.addCasaComercial.submitted = false;
                            $scope.setAlert('success', 'La casa comercial ha sido guardada exitosamente');
                            $scope.getData();
                            $scope.gridOpts.paginationCurrentPage = Math.ceil($scope.gridOpts.totalItems / $scope.gridOpts.paginationPageSize);
                            $scope.seleccionadas.modelos = [];
                            $scope.listaCasaComercialModelo = [];
                        }
                    });
                }
            }
            else {
                $scope.setAlert('danger', 'Se debe agregar al menos un modelo para la marca de auto seleccionada, por favor validar');
                $scope.addCasaComercial.submitted = true;
            }
        } else {
            $scope.addCasaComercial.submitted = true;
        }
    };


    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.toggleEdit();
        $scope.closeAlert(0);
        //Servicio que obtiene el detalle de la urbanizadora seleccionada
        CasaComercialFactory.obtenerDetalleCasaComercial(entidad.Id).then(function (response) {
            if (response != null) {
                //Obtiene el detalle
                $scope.newCasaComercial.Nombre = response.Nombre;

                $scope.newPais.IdPais = response.IdPais;

                $scope.seleccionadosTipoAutos = [];
                if (response.Nuevo == true) {
                    $scope.seleccionadosTipoAutos.push($scope.tipoAutos[0]);
                }
                if (response.Usado == true) {
                    $scope.seleccionadosTipoAutos.push($scope.tipoAutos[1]);
                }


                $scope.modelos = [];
                $scope.seleccionadas.modelos = [];
                _.each($scope.marcas, function (item) { item.RelacionCasa = false });
                angular.forEach(response.listaMarcas, function (value, key) {

                    $scope.marcasCreated.Marcas.push(value.Id);
                    $scope.modelos.push(value);

                    $scope.modelos[key].listaModelos.forEach(function (item, index) {
                        if (item.RelacionCasaComercial == true) {
                            $scope.seleccionadas.modelos.push(item);
                        }
                    });

                    var indexSeleccionado = _.findLastIndex($scope.marcas, { Id: value.Id });
                    if (indexSeleccionado != -1) {
                        $scope.marcas[indexSeleccionado].RelacionCasa = true;
                    }
                });


                ////Crea el objeto para la edición - parte 1
                $scope.newCasaComercial.Id = entidad.Id;
                $scope.newCasaComercial.Activo = entidad.Activo;

            }
        })
    };


    //Función para actualizar una casa comercial en la base de datos.
    $scope.update = function () {
        if ($scope.addCasaComercial.$valid) {
            if ($scope.modelos.length > 0) {
                //Crea el objeto para la edición - parte 2
                $scope.newCasaComercial.Nuevo = 0;
                $scope.newCasaComercial.Usado = 0;
                $scope.seleccionadosTipoAutos.forEach(function (item, index) {
                    switch (item.Id) {
                        case 1:
                            $scope.newCasaComercial.Nuevo = 1;
                            break;
                        case 2:
                            $scope.newCasaComercial.Usado = 1;
                            break;
                    }
                });
                $scope.newCasaComercial.IdPais = $scope.newPais.IdPais;
                $scope.newCasaComercial.Activo = true;
                $scope.listaCasaComercialModelo = [];
                $scope.ListaIdMarcasSeleccionados = [];
                $scope.seleccionadas.modelos.forEach(function (item, index) {
                    $scope.listaCasaComercialModelo.push({ IdModeloAuto: item.Id });
                    $scope.ListaIdMarcasSeleccionados.push(item.IdMarcaAuto);
                });
                $scope.newCasaComercial.listaModelos = $scope.listaCasaComercialModelo;

                $scope.ListaIdMarcasTodos = [];
                $scope.modelos.forEach(function (item, index) {
                    item.listaModelos.forEach(function (item, index) {
                        $scope.ListaIdMarcasTodos.push(item.IdMarcaAuto);
                    });
                });

                var listaMarcasDifference = _.difference($scope.ListaIdMarcasTodos, $scope.ListaIdMarcasSeleccionados);
                var listaMarcasNoSelecccionadas = _.uniq(listaMarcasDifference);

                $scope.NombreMarcas = [];
                if (listaMarcasNoSelecccionadas.length > 0) {
                    CasaComercialFactory.obtenerMarcasPorId(listaMarcasNoSelecccionadas).then(function (response) {
                        response.forEach(function (item, index) {
                            $scope.NombreMarcas.push(item.Marca);
                        });
                        if ($scope.NombreMarcas.length > 0) {
                            $scope.setAlert('danger', 'Se debe agregar al menos un modelo para la(s) siguientes marcas: ' + $scope.NombreMarcas.join(', ') + ', por favor validar');
                            $scope.addCasaComercial.submitted = true;
                        }
                    });
                }
                else {
                    //Servicio que actualiza la urbanizadora seleccionada
                    CasaComercialFactory.actualizarCasaComercial($scope.newCasaComercial).then(function (response) {
                        if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                            $scope.setAlert('danger', 'La casa comercial ' + $scope.newCasaComercial.Nombre + ' ya existe, por favor validar');
                        }
                        else {
                            $scope.formMode = false;
                            $scope.addCasaComercial.submitted = false;
                            $scope.setAlert('success', 'La casa comercial ha sido actualizada exitosamente');
                            $scope.getData();

                            $scope.seleccionadas.modelos = [];
                            $scope.listaCasaComercialModelo = [];
                        }
                    });
                }
            }
            else {
                $scope.setAlert('danger', 'Se debe agregar al menos un modelo para la marca de auto seleccionada, por favor validar');
                $scope.addCasaComercial.submitted = true;
            }
        } else {
            $scope.addCasaComercial.submitted = true;
        }
    };



    //Función para activar o inactivar una casa comercial, depende que esté o no, asociada a un formulario
    $scope.active = function (entidad) {
        entidad.Activo = !entidad.Activo;
        CasaComercialFactory.inactivarCasaComercial(entidad).then(function (response) {
            if (response.data.mensajeError === 'OperacionErroneaFK') {
                $scope.setAlert('danger', 'No es posible inactivar la casa comercial ' + entidad.Nombre + ', ésta se encuentra asociada a un formulario');
            }
            else {
                $scope.getData();
                $scope.closeAlert(0);
            }
        })
    }

    //Función para eliminar una casa comercial, depende que esté o no, asociada a un formulario
    $scope.delete = function (entidad) {
        CasaComercialFactory.eliminarCasaComercial(entidad.Id).then(function (response) {
            if (response.data.mensajeError === 'OperacionErroneaFK') {
                $scope.setAlert('danger', 'No es posible eliminar la casa comercial ' + entidad.Nombre + ', ésta se encuentra asociada a un formulario');
            }
            else {
                $scope.setAlert('success', 'La casa comercial ' + entidad.Nombre + ', ha sido eliminada');
                $scope.getData();
                $scope.seleccionadas.modelos = [];
                $scope.listaCasaComercialModelo = [];
            }
        })
    }

    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.tipoAutos = [{ Id: 1, Value: 1, TipoAuto: "Nuevo" }, { Id: 2, Value: 2, TipoAuto: "Usado" }];
        $scope.marcasCreated.Marcas = [];
        $scope.seleccionadas.modelos = [];
        $scope.seleccionadosTipoAutos = [];
        $scope.listaCasaComercialModelo = [];
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
        $scope.isEdit = true;
    }

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        $scope.newCasaComercial.Nombre = '';
        if ($rootScope.paisesSelected.Paises.length != 1) {
            $scope.newPais.IdPais = '';
        }

        $scope.marcasCreated.Marcas = [];
        $scope.modelos = [];
        $scope.seleccionadas.modelos = [];
        $scope.seleccionadosTipoAutos = [];
        $scope.listaCasaComercialModelo = [];
        $scope.tipoAutos = [{ Id: 1, Value: 1, TipoAuto: "Nuevo" }, { Id: 2, Value: 2, TipoAuto: "Usado" }];
        $scope.marcas = [];
        $scope.getMarcas();
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
        $scope.isEdit = false;
    }

    //Lista de marcas.
    $scope.marcas = [];

    //Método para obtener las marcas que estén activas
    $scope.getMarcas = function () {
        CasaComercialFactory.obtenerMarcasAutoActivas().then(function (response) {
            $scope.marcas = response;
        })
    }


    //Lista de tipos de propiedades seleccionadas usadas en el filtro del admin de urbanizadora
    $scope.seleccionadas = {
        modelos: []
    }

    //Función encargada de seleccionar o no cada item de la lista de tipo de modelos para la posterior creación o edición
    $scope.isChecked = function (tipo) {
        var items = $scope.seleccionadas.modelos;
        if (items != '') {
            for (var i = 0; i < items.length; i++) {
                if (tipo.Id == items[i].Id) {
                    return true;
                }
            }
        }
        return false;
    };

    //Función encargado de eliminar cada item que se deseleccione de la lista de los tipos de modelos seleccionados
    $scope.updateSelection = function ($event, id) {
        var checkbox = $event.target;
        if (!checkbox.checked) {
            var index = -1;
            var itemsUpdate = $scope.seleccionadas.modelos;
            for (var i = 0; i < itemsUpdate.length; i++) {
                if (id == itemsUpdate[i].Id) {
                    index = i;
                }
            }
            if (index != -1) {
                $scope.seleccionadas.modelos.splice(index, 1);
            }
        }
    };

    //Verificar si algún checkbox fue seleccionado
    $scope.someSelected = function (object) {
        return Object.keys(object).some(function (key) {
            return object[key];
        });
    }

    //Llamado a la función para que se llene el grid.
    $scope.getData();
    //Llamado a la función para que se poble el filtro de marcas
    $scope.getMarcas();

});
