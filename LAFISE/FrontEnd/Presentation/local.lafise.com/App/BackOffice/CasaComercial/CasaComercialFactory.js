﻿/*==========================================================================
Archivo:            CasaComercialFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del CUS003 - Administrar casas comerciales
Autor:              steven.echavarria
Fecha de creación:  05/11/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("CasaComercialModule").factory("CasaComercialFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de casas comerciales.
    //Parametro entidad: recibe los filtros de búsqueda: Pais, Casa comercial y Marca.
    var obtenerCasasComerciales = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/CasaComercial/ConsultarCasaComercialFiltros"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de modelos por cada marca a filtrar
    //Parametro marcas: id's de marcas
    var obtenerModelosPorMarcas = function (marcas) {
        var result = $http({
            method: "GET",
            url: appContext + "/CasaComercial/ConsultarCasaComercialModeloMarcaFiltros?marcas=" + marcas
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de las marcas de autos que estén activas.
    var obtenerMarcasAutoActivas = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/MarcaAuto/ObtenerMarcasActivas"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consultar una casa comercial por su nombre y así validar su existencia
    //Parametro entidad: recibe el nombre y el id de la urbanizadora.  
    var obtenerCasaComercialPorNombre = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/CasaComercial/ValidarCasaComercialPorNombre"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de obtener el detalle de una casa comercial
    //Parametro id: identificador de la casa comercial.  
    var obtenerDetalleCasaComercial = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/CasaComercial/ConsultarDetalleCasaComercial?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de obtener el nombre de una o varias marcas según su id.
    //Parametro entidad: lista de enteros (id)
    var obtenerMarcasPorId = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/MarcaAuto/VerMarcasPorId"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la creación de una casa comercial
    //Parametro entidad: recibe las propiedades: Nombre, IdPais, Nuevo, FechaCreacion, Usado, Activo, ListaModelos  
    var crearCasaComercial = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/CasaComercial/CrearCasaComercial"
        });
        return sendRequest(result);
    }

    //Función encargada de consumir el servicio para la actualización de una casa comercial
    //Parametro entidad: recibe las propiedades: Id, Nombre, IdPais, Nuevo, FechaCreacion, Usado, Activo, ListaModelos   
    function actualizarCasaComercial(entidad) {

        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/CasaComercial/EditarCasaComercial'
        });
        return sendRequest(result);
    }

    //Función encargada de consumir el servicio para la inactivación de una casa comercial
    //Parametro entidad: recibe las propiedades: Id, Activo
    var inactivarCasaComercial = function (entidad) {

        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/CasaComercial/InactivarCasaComercial'
        });
        return sendRequest(result);
    }

    //Función encargada de consumir el servicio para eliminar una casa comercial
    //Parametro id: identificador de la casa comercial. 
    function eliminarCasaComercial(id) {

        var result = $http({
            method: 'POST',
            url: appContext + '/CasaComercial/EliminarCasaComercialPorId/' + id
        });

        return sendRequest(result);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerCasasComerciales: obtenerCasasComerciales,
        obtenerModelosPorMarcas: obtenerModelosPorMarcas,
        obtenerMarcasAutoActivas: obtenerMarcasAutoActivas,
        obtenerCasaComercialPorNombre: obtenerCasaComercialPorNombre,
        obtenerDetalleCasaComercial: obtenerDetalleCasaComercial,
        obtenerMarcasPorId: obtenerMarcasPorId,
        crearCasaComercial: crearCasaComercial,
        actualizarCasaComercial: actualizarCasaComercial,
        inactivarCasaComercial: inactivarCasaComercial,
        eliminarCasaComercial: eliminarCasaComercial
    });
});
