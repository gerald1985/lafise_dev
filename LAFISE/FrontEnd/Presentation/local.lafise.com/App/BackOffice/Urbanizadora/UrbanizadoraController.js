﻿/*==========================================================================
Archivo:            UrbanizadoraController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del CUS005- Administrar urbanizadora
Autor:              steven.echavarria                          
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("UrbanizadoraModule").controller("UrbanizadoraController", function ($scope, $rootScope, i18nService, $filter, $uibModal, UrbanizadoraFactory) {

    //Traduce el grid
    i18nService.setCurrentLang('es');

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = false;
    $scope.showMsg = false;
    $scope.isEdit = false;

    //Objeto que contiene las propiedades del filtro
    $scope.filtros = {
        Pais: '',
        Urbanizadora: '',
        TipoPropiedad: ''
    };

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newUrbanizadora = {
        Id: '',
        Nombre: '',
        IdPais: '',
        Activo: '',
        ListaPropiedades: '',
    };

    //Objeto usado para manipular el valor del nombre de la urbanizadora a filtrar
    $scope.urbanizadoraSelected = '';

    //capturar el pais del formulario
    $scope.newPais = {
        IdPais: ''
    };

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Lista de urbanizadoras.
    $scope.urbanizadoras = [];

    //Método para obtener los datos del servicio y poblar el grid.
    $scope.getData = function () {
        $scope.formMode = false;

        if (typeof $scope.paisesSelected !== "undefined" && typeof $scope.paisesSelected.Paises !== "undefined" && $scope.paisesSelected.Paises != '') {
            $scope.filtros.Pais = $scope.paisesSelected.Paises.join();
        }
        else { $scope.filtros.Pais = null; }

        if ($scope.urbanizadoraSelected != '') {
            $scope.filtros.Urbanizadora = $scope.urbanizadoraSelected;
        }
        else { $scope.filtros.Urbanizadora = null; }

        if (typeof $scope.tipoPropiedadesSelected !== "undefined" && typeof $scope.tipoPropiedadesSelected.Tipos !== "undefined" && $scope.tipoPropiedadesSelected.Tipos != '') {
            $scope.filtros.TipoPropiedad = $scope.tipoPropiedadesSelected.Tipos.join();
        }
        else { $scope.filtros.TipoPropiedad = null; }

        UrbanizadoraFactory.obtenerUrbanizadoras($scope.filtros).then(function (response) {
            $scope.gridOpts.data = response;
            $scope.urbanizadoras = response;
        });
    }


    //Función para el manejo de mensajes de validación al eliminar una urbanizadora
    $scope.open = function (entidad) {
        var modalInstance = $uibModal.open({
            animation: false,
            templateUrl: 'plantillaEliminarUrbanizadora.html',
            scope: $scope,
            controller: 'ModalEliminarController',
            size: 'sm',
            resolve:
                {
                    Entity: function () {
                        return entidad;
                    }
                }
        });
    };

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.addUrbanizadora.submitted = false;
        $scope.selection = [];
        $scope.closeAlert(0);
    };

    //Configuración de grid
    $scope.gridOpts = {
        columnDefs: [
        { name: 'Pais', field: 'Pais', displayName: 'País' },
        { name: 'Urbanizadora', field: 'Urbanizadora', displayName: 'Urbanizadora' },
        { name: 'TipoPropiedad', field: 'TipoPropiedad', displayName: 'Tipo Propiedad' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activar/Desactivar" value="Activo/Inactivo" ng-click="grid.appScope.active(row.entity)">', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
    };


    //Función para validar que el nombre de la urbanizadora sea única en la bd al momento de insertar o actualizar.
    $scope.UniqueName = function () {
        $scope.newUrbanizadora.Nombre = $scope.urbanizadoraCreated;
        UrbanizadoraFactory.obtenerUrbanizadoraPorNombre($scope.newUrbanizadora).then(function (response) {
            if (response != null) {
                $scope.setAlert('danger', 'La urbanizadora ' + $scope.urbanizadoraCreated + ' ya existe, por favor validar');
                $scope.urbanizadoraCreated = '';
            }
            else {
                $scope.closeAlert(0);
            }
        });
    };


    //Objeto que contiene el id del país en la creación de la urbanizadora
    $scope.paisCreated = { selected: "" };

    //Función para insertar una urbanizadora en la base de datos.
    $scope.add = function () {

        if ($scope.addUrbanizadora.$valid) {

            $scope.newUrbanizadora.Nombre = $scope.urbanizadoraCreated;
            $scope.newUrbanizadora.IdPais = $scope.newPais.IdPais;
            $scope.newUrbanizadora.Activo = true;
            $scope.newUrbanizadora.ListaPropiedades = $scope.seleccionadas.tipoPropiedades;

            //Servicio que crea la urbanizadora 
            UrbanizadoraFactory.crearUrbanizadora($scope.newUrbanizadora).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'La urbanizadora ' + $scope.newUrbanizadora.Nombre + ' ya existe, por favor validar');
                }
                else {
                    $scope.formMode = false;
                    $scope.addUrbanizadora.submitted = false;
                    $scope.setAlert('success', 'La urbanizadora ha sido guardada exitosamente');
                    $scope.getData();
                    $scope.gridOpts.paginationCurrentPage = $scope.gridOpts.paginationPageSize;
                    $rootScope.seleccionadas.tipoPropiedades = [];
                }
            });
        } else {
            $scope.addUrbanizadora.submitted = true;
        }
    };

    //Función para actualizar una urbanizadora en la base de datos.
    $scope.update = function () {
        if ($scope.addUrbanizadora.$valid) {

            //Crea el objeto para la edición - parte 2
            $scope.newUrbanizadora.Nombre = $scope.urbanizadoraCreated;
            $scope.newUrbanizadora.IdPais = $scope.newPais.IdPais;
            $scope.newUrbanizadora.ListaPropiedades = $scope.seleccionadas.tipoPropiedades;

            //Servicio que actualiza la urbanizadora seleccionada
            UrbanizadoraFactory.actualizarUrbanizadora($scope.newUrbanizadora).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'La urbanizadora ' + $scope.newUrbanizadora.Nombre + ' ya existe, por favor validar');
                }
                else {
                    $scope.formMode = false;
                    $scope.addUrbanizadora.submitted = false;
                    $scope.setAlert('success', 'La urbanizadora ha sido actualizada exitosamente');
                    $scope.getData();


                    $rootScope.seleccionadas.tipoPropiedades = [];


                }
            })

        } else {
            $scope.addUrbanizadora.submitted = true;
        }
    }

    //Función para activar o inactivar una urbanizadora, depende que esté o no, asociada a un formulario
    $scope.active = function (entidad) {
        entidad.Activo = !entidad.Activo;
        UrbanizadoraFactory.inactivarUrbanizadora(entidad).then(function (response) {
            if (response.data.mensajeError === 'OperacionErroneaFK') {
                $scope.setAlert('danger', 'No es posible inactivar la urbanizadora ' + entidad.Urbanizadora + ', ésta se encuentra asociada a un formulario');
            }
            else {
                $scope.getData();
                $scope.closeAlert(0);
            }
        })
    }

    //Función para eliminar una urbanizadora, depende que esté o no, asociada a un formulario
    $scope.delete = function (entidad) {
        UrbanizadoraFactory.eliminarUrbanizadora(entidad.Id).then(function (response) {
            if (response.data.mensajeError === 'OperacionErroneaFK') {
                $scope.setAlert('danger', 'No es posible eliminar la urbanizadora ' + entidad.Urbanizadora + ', ésta se encuentra asociada a un formulario');
            }
            else {
                $scope.setAlert('success', 'La urbanizadora ' + entidad.Urbanizadora + ', ha sido eliminada');
                $scope.getData();
                $rootScope.seleccionadas.tipoPropiedades = [];
            }
        })
    }

    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
        $scope.isEdit = true;
    }

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        $scope.urbanizadoraCreated = '';
        $rootScope.seleccionadas.tipoPropiedades = [];
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
        $scope.isEdit = false;
    }

    $scope.tipoPropiedad = [];

    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.toggleEdit();
        $scope.closeAlert(0);
        //Servicio que obtiene el detalle de la urbanizadora seleccionada
        UrbanizadoraFactory.obtenerDetalleUrbanizadora(entidad.Id).then(function (response) {
            if (response != null) {
                var algo = response;

                //Obtiene el detalle
                $scope.urbanizadoraCreated = response.Nombre;
                $scope.newPais.IdPais = response.IdPais;
                $rootScope.seleccionadas.tipoPropiedades = [];
                $rootScope.seleccionadas.tipoPropiedades = response.ListaPropiedades;

                //Crea el objeto para la edición - parte 1
                $scope.newUrbanizadora.Id = entidad.Id;
                $scope.newUrbanizadora.Activo = entidad.Activo;

            }
        })
    }

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "Urbanizadora_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };

    //Llamado a la función para que se llene el grid.
    $scope.getData();

});