﻿/*==========================================================================
Archivo:            UrbanizadoraFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del CUS005- Administrar urbanizadora
Autor:              steven.echavarria                          
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("UrbanizadoraModule").factory("UrbanizadoraFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de urbanizadoras.
    //Parametro entidad: recibe los filtros de búsqueda: Pais, Urbanizadora y TipoPropiedad.  
    var obtenerUrbanizadoras = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Urbanizadora/VerUrbanizadorasTiposPropiedades"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consultar una urbanizadora por su nombre y así validar su existencia
    //Parametro entidad: recibe el nombre y el id de la urbanizadora.  
    var obtenerUrbanizadoraPorNombre = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Urbanizadora/ValidarUrbanizadoraPorNombre"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de obtener el detalle de una urbanizadora
    //Parametro id: identificador de la urbanizadora.  
    var obtenerDetalleUrbanizadora = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/Urbanizadora/ConsultarDetalleUrbanizadora?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la creación de una urbanizadora
    //Parametro entidad: recibe las propiedades: Nombre, IdPais, Activo, ListaPropiedades  
    var crearUrbanizadora = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Urbanizadora/CrearUrbanizadora"
        });
        return sendRequest(result);
    }

    //Función encargada de consumir el servicio para la actualización de una urbanizadora
    //Parametro entidad: recibe las propiedades: Id, Nombre, IdPais, Activo, ListaPropiedades  
    function actualizarUrbanizadora(entidad) {

        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Urbanizadora/EditarUrbanizadora'
        });
        return sendRequest(result);
    }

    //Función encargada de consumir el servicio para la inactivación de una urbanizadora
    //Parametro entidad: recibe las propiedades: Id, Activo
    var inactivarUrbanizadora = function (entidad) {

        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Urbanizadora/InactivarUrbanizadora'
        });
        return sendRequest(result);
    }

    //Función encargada de consumir el servicio para eliminar una urbanizadora
    //Parametro id: identificador de la urbanizadora. 
    function eliminarUrbanizadora(id) {

        var result = $http({
            method: 'POST',
            url: appContext + '/Urbanizadora/EliminarUrbanizadoraPorId/' + id
        });

        return sendRequest(result);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerUrbanizadoras: obtenerUrbanizadoras,
        obtenerUrbanizadoraPorNombre: obtenerUrbanizadoraPorNombre,
        obtenerDetalleUrbanizadora: obtenerDetalleUrbanizadora,
        crearUrbanizadora: crearUrbanizadora,
        actualizarUrbanizadora: actualizarUrbanizadora,
        inactivarUrbanizadora: inactivarUrbanizadora,
        eliminarUrbanizadora: eliminarUrbanizadora
    });

});