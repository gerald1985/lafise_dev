﻿/*==========================================================================
Archivo:            TipoSucursalFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del CUS010- Administrar tipo sucursal
Autor:              paola.munoz                        
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("TipoSucursalModule").factory("TipoSucursalFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";
    //Función encargada de consumir el servicio para la consulta todos los tipo sucursal. 
    var obtenerTipoSucursal = function (TipoSucursal) {
        var result = $http({
            method: "get",
            url: appContext + "/TipoSucursal/ConsultarTipoSucursalFiltro?tipoSucursal=" + TipoSucursal
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consultar un tipo sucursal por su nombre y así validar su existencia
    //Parametro entidad: recibe el nombre y el id de tipo sucursal.  
    var obtenerNombreUnico = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/TipoSucursal/ConsultarPorNombre"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de validar un tipo sucursal 
    //Parametro entidad: recibe el id de tipo sucursal.  
    var obtenerValidar = function (id) {
        var result = $http({
            method: "get",
            url: appContext + "/TipoSucursal/ValidarTipoSucursal?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de crear un tipo sucursal 
    //Parametro entidad: recibe la entidad de tipo sucursal.
    var crearTipoSucursal = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/TipoSucursal/CrearTipoSucursal"
        });
        return sendRequest(result);
    }

    //Función encargada de actualizar un tipo sucursal 
    //Parametro entidad: recibe la entidad de tipo sucursal
    var actualizarTipoSucursal = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/TipoSucursal/EditarTipoSucursal'
        });
        return sendRequest(result);
    }

    //Función encargada de inactivar un tipo sucursal 
    //Parametro entidad: recibe la entidad de tipo sucursal
    var inactivarTipoSucursal = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/TipoSucursal/InactivarTipoSucursal'
        });
        return sendRequest(result);
    }

    //Función encargada de eliminar un tipo sucursal 
    //Parametro entidad: recibe el id  de tipo sucursal
    var eliminarTipoSucursal = function (id) {
        var result = $http({
            method: 'POST',
            url: appContext + '/TipoSucursal/EliminarTipoSucursal/' + id
        });

        return sendRequest(result);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerTipoSucursal: obtenerTipoSucursal,
        crearTipoSucursal: crearTipoSucursal,
        actualizarTipoSucursal: actualizarTipoSucursal,
        inactivarTipoSucursal: inactivarTipoSucursal,
        eliminarTipoSucursal: eliminarTipoSucursal,
        obtenerNombreUnico: obtenerNombreUnico,
        obtenerValidar: obtenerValidar
    });

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

});
