﻿angular.module("TipoSucursalModule").controller("TipoSucursalController", function ($scope, $filter, i18nService, $uibModal, TipoSucursalFactory) {
    //Traducir el grid
    i18nService.setCurrentLang('es');

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = false;
    $scope.showMsg = false;
    $scope.isEdit = false;

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newTipoSucursal = {
        Id: '',
        TipoSucursal1: '',
        Activo: true
    };

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Lista de tipo sucursal.
    $scope.tipoSucursal = [];

    //Objeto que contiene el nombre del tipo sucursal de los filtros de busqueda
    $scope.tipoSucursalSelected = { TipoSucursal: "" };

    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {

        TipoSucursalFactory.obtenerTipoSucursal($scope.tipoSucursalSelected.TipoSucursal).then(function (response) {
            $scope.gridOpts.data = response;
            $scope.tipoSucursal = response;
        });

    }

    //Función para el manejo de mensajes de validación al eliminar un tipo de sucursal y abrir la ventana modal en caso de que permita eliminar
    $scope.open = function (entidad) {
        $scope.canDelete = true;
        $scope.newTipoSucursal.Id = entidad.Id;
        $scope.newTipoSucursal.TipoSucursal1 = entidad.TipoSucursal1;
        TipoSucursalFactory.obtenerValidar(entidad.Id).then(function (response) {
            if (response === '') {
                    $scope.canDelete = true;
                }
                else {
                    $scope.setAlert('danger', 'No es posible eliminar el tipo de sucursal, se encuentra asociado a las sucursales: ' + response);
                    $scope.canDelete = false;
                }
            
            if ($scope.canDelete) {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'modalEliminar.html',
                    scope: $scope,
                    controller: 'ModalEliminarController',
                    size: 'sm',
                    resolve:
                        {
                            Entity: function () {
                                return entidad;
                            }
                        }
                });
            }
        });
    };

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta segun, el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.addTipoSucursal.submitted = false;
        $scope.closeAlert(0);
    };

    //Configuración del Grid.
    $scope.gridOpts = {
        columnDefs: [
        { name: 'Tipo Sucursal', field: 'TipoSucursal1', displayName: 'Tipo Sucursal' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activa/Desactivar" value="Activo/Inactivo"  ng-click="grid.appScope.active(row.entity)">', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
    };

    //Función para insertar un tipo de sucursal en la base de datos.
    $scope.add = function () {
        if ($scope.addTipoSucursal.$valid) {
            TipoSucursalFactory.crearTipoSucursal(this.newTipoSucursal).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'El tipo de sucursal ' + $scope.newTipoSucursal.TipoSucursal1 + ' ya existe, por favor validar');
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.gridOpts.paginationCurrentPage = Math.ceil($scope.gridOpts.totalItems / $scope.gridOpts.paginationPageSize);
                    $scope.addTipoSucursal.submitted = false;
                    $scope.setAlert('success', 'El tipo sucursal ha sido guardada exitosamente');
                }
            });
        } else {
            $scope.addTipoSucursal.submitted = true;
        }
    };

    //Función para actualizar un tipo de sucursal en la base de datos.
    $scope.update = function () {
        if ($scope.addTipoSucursal.$valid) {
            TipoSucursalFactory.actualizarTipoSucursal(this.newTipoSucursal).then(function (response) {
                if (response.data.MensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'El tipo de sucursal ' + $scope.newTipoSucursal.TipoSucursal1 + ' ya existe, por favor validar');
                    $scope.newTipoSucursal.TipoSucursal1 = '';
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.addTipoSucursal.submitted = false;
                    $scope.setAlert('success', 'El tipo sucursal ha sido guardada exitosamente');
                }
            })
        } else {
            $scope.addTipoSucursal.submitted = true;
        }
    }

    //Función para activar o inactivar un tipo sucursal
    $scope.active = function (entidad) {
        $scope.closeAlert(0);
        entidad.Activo = !entidad.Activo;
        TipoSucursalFactory.inactivarTipoSucursal(entidad).then(function (response) {
            if (response.data === '') {
                $scope.getData();
            }
            else {
                $scope.setAlert('danger', 'No es posible inactivar el tipo de sucursales, se encuentra asociado a las sucursales: ' + response.data);
                $scope.getData();
            }

        })
    }

    //Función para eliminar un tipo sucursal en la base de datos.
    $scope.delete = function (entidad) {
        $scope.closeAlert(0);
        TipoSucursalFactory.eliminarTipoSucursal(entidad.Id).then(function (response) {
            if (response.data === '') {
                $scope.getData();
                $scope.setAlert('success', 'EL tipo de sucursal  ' + entidad.TipoSucursal1 + ' ha sido eliminada');
            }
            else {
                $scope.setAlert('danger', 'No es posible eliminar el tipo de sucursal, se encuentra asociado a las sucursales: ' + response.data);
                $scope.getData();
            }
        })
    }

    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
        $scope.isEdit = true;
    }

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        $scope.newTipoSucursal.TipoSucursal1 = '';
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
        $scope.isEdit = false;
    }

    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.closeAlert(0);
        $scope.toggleEdit();
        $scope.newTipoSucursal.Id = entidad.Id;
        $scope.newTipoSucursal.TipoSucursal1 = entidad.TipoSucursal1;
        $scope.newTipoSucursal.Activo = entidad.Activo;
    }

    //Función para validar que el nombre del tipo sucursal sea único en la tabla al momento de insertar o actualizar.
    $scope.UniqueName = function () {
            TipoSucursalFactory.obtenerNombreUnico(this.newTipoSucursal).then(function (response) {
                if (response != null) {
                    $scope.setAlert('danger', 'El tipo de sucursal  ' + $scope.newTipoSucursal.TipoSucursal1 + ' ya existe, por favor validar');
                    $scope.newTipoSucursal.TipoSucursal1 = '';
                }
                else {
                    $scope.closeAlert(0);
                }
            });
    };

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "TipoSucursal_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };

    $scope.getData();

});