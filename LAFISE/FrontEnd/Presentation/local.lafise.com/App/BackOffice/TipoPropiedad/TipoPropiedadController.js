﻿angular.module("TipoPropiedadModule").controller("TipoPropiedadController", function ($scope, $rootScope, i18nService, $filter, $uibModal, TipoPropiedadFactory) {
    i18nService.setCurrentLang('es');


    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = false;
    $scope.showMsg = false;


    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newTipoPropiedad = {
        Id: '',
        Tipo: '',
        Activo: true
    };

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Lista de tipo propiedades.
    $scope.tipoPropiedades = [];

    //Lista de tipo propiedades activas
    $scope.tipoPropiedadesActivas = [];

    //Lista de tipos de propiedades seleccionadas usadas en el filtro del admin de urbanizadora
    $rootScope.seleccionadas = {
        tipoPropiedades: []
    }

    //Función encargada de seleccionar o no cada item de la lista de tipo de propiedades para la posterior creración o edición
    $rootScope.isChecked = function (tipo) {
        var items = $scope.seleccionadas.tipoPropiedades;
        if (items != '') {
            for (var i = 0; i < items.length; i++) {
                if (tipo.Id == items[i].Id) {
                    return true;
                }
            }
        }
        return false;
    };

    //Función encargado de eliminar cada item que se deseleccione de la lista de los tipos de propiedades seleccionados
    $scope.updateSelection = function ($event, id) {
        var checkbox = $event.target;
        if (!checkbox.checked) {
            var index = -1;
            var itemsUpdate = $scope.seleccionadas.tipoPropiedades;
            for (var i = 0; i < itemsUpdate.length; i++) {
                if (id == itemsUpdate[i].Id) {
                    index = i;
                }
            }
            if (index != -1) {
                $scope.seleccionadas.tipoPropiedades.splice(index, 1);
            }
        }
    };

    //Lista de tipo propiedades seleccionadas usadas en el modulo de administrar urbanizadora
    $rootScope.tipoPropiedadesSelected = [];

    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function (save) {
        TipoPropiedadFactory.obtenerTiposPropiedades().then(function (response) {
            $scope.gridOpts.data = response;
            $scope.tipoPropiedades = response;
            if (save) {
                $scope.gridOpts.paginationCurrentPage = Math.ceil(response.length / $scope.gridOpts.paginationPageSize);
            }

        });
    }

    //Método para obtener los datos del servicio que estén activas y poblar las listas de selección
    $scope.getDataActivos = function () {
        TipoPropiedadFactory.obtenerTiposPropiedadesActivas().then(function (response) {
            $scope.tipoPropiedadesActivas = response;
        });
    }

    //Función para el manejo de mensajes de validación al eliminar un tipo de propiedad y abrir la ventana modal en caso de que permita eliminar
    $scope.open = function (Entidad) {
        $scope.canDelete = true;
        TipoPropiedadFactory.obtenerRelaciones(Entidad.Id).then(function (response) {
            if (response.UrbanizadorasRelacionadas != null && response.RelacionSolicitud === 'OperacionErroneaFK') {
                $scope.setAlert('danger', 'No es posible eliminar el tipo de propiedad, ésta se encuentra asociada a las urbanizadoras ' + response.UrbanizadorasRelacionadas + ', y a solicitudes enviadas.');
                $scope.canDelete = false;
            }
            else {
                if (response.UrbanizadorasRelacionadas != null) {
                    $scope.setAlert('danger', 'No es posible eliminar el tipo de propiedad, se encuentra asociado a las urbanizadoras:' + response.UrbanizadorasRelacionadas);
                    $scope.canDelete = false;
                }
                if (response.RelacionSolicitud === 'OperacionErroneaFK') {
                    $scope.setAlert('danger', 'No es posible eliminar el tipo de propiedad, ésta se encuentra asociado a solicitudes enviadas');
                    $scope.canDelete = false;
                }
            }
            if ($scope.canDelete) {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'modalEliminar.html',
                    scope: $scope,
                    controller: 'ModalEliminarController',
                    size: 'sm',
                    resolve:
                        {
                            Entity: function () {
                                return Entidad;
                            }
                        }
                });
            }
        });
    };

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta segun, el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.addTipoPropiedad.submitted = false;
        $scope.closeAlert(0);
    };

    //Función para validar que el nombre del tipo propiedad sea único en la tabla al momento de insertar o actualizar.
    $scope.UniqueName = function () {
        TipoPropiedadFactory.obtenerNombreUnico(this.newTipoPropiedad).then(function (response) {
            if (response != null) {
                $scope.setAlert('danger', 'El tipo de propiedad ' + $scope.newTipoPropiedad.Tipo + ' ya existe, por favor validar');
                $scope.newTipoPropiedad.Tipo = '';
            }
            else {
                $scope.closeAlert(0);
            }
        });
    };

    //Configuración del Grid.
    $scope.gridOpts = {
        columnDefs: [
        { name: 'Tipo Propiedad', field: 'Tipo', displayName: 'Tipo Propiedad' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activa/Desactivar" value="Activo/Inactivo"  ng-click="grid.appScope.active(row.entity)">', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true
    };

    //Función para insertar un tipo de propiedad en la base de datos.
    $scope.add = function () {
        if ($scope.addTipoPropiedad.$valid) {
            TipoPropiedadFactory.crearTipoPropiedad(this.newTipoPropiedad).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'El tipo de propiedad ' + $scope.newTipoPropiedad.Tipo + ' ya existe, por favor validar');
                }
                else {
                    $scope.getData(true);
                    $scope.formMode = false;
                    $scope.addTipoPropiedad.submitted = false;
                    $scope.setAlert('success', 'El tipo propiedad ha sido guardada exitosamente');
                }
            });
        } else {
            $scope.addTipoPropiedad.submitted = true;
        }
    };

    //Función para actualizar un tipo de propiedad en la base de datos.
    $scope.update = function () {
        if ($scope.addTipoPropiedad.$valid) {
            TipoPropiedadFactory.actualizarTipoPropiedad(this.newTipoPropiedad).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'El tipo de propiedad ' + $scope.newTipoPropiedad.Tipo + ' ya existe, por favor validar');
                }
                else {
                    $scope.getData();
                    $scope.formMode = false;
                    $scope.addTipoPropiedad.submitted = false;
                    $scope.setAlert('success', 'El tipo propiedad ha sido guardada exitosamente');
                }
            })
        } else {
            $scope.addTipoPropiedad.submitted = true;
        }
    }

    //Función para activar o inactivar un tipo de propiedad, en caso de que este asociado a una urbanizadora se mostrara la alerta con las urbanizadoras asociadas.
    $scope.active = function (entidad) {
        entidad.Activo = !entidad.Activo;
        TipoPropiedadFactory.inactivarTipoPropiedad(entidad).then(function (response) {
            if (response.data === '') {
                $scope.getData();
                $scope.closeAlert(0);
            }
            else {
                $scope.setAlert('danger', 'No es posible inactivar el tipo de propiedad, se encuentra asociado a las urbanizadoras: ' + response.data);
                $scope.getData();
            }
        })
    }

    //Función para eliminar un tipo de propiedad en la base de datos.
    $scope.delete = function (Entidad) {
        TipoPropiedadFactory.eliminarTipoPropiedad(Entidad.Id).then(function (response) {
            $scope.getData();
            $scope.setAlert('success', 'EL tipo de propiedad ' + Entidad.Tipo + ' ha sido eliminada');
        })
    }

    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
    }

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        $scope.newTipoPropiedad.Tipo = '';
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
    }

    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.toggleEdit();
        $scope.newTipoPropiedad.Id = entidad.Id;
        $scope.newTipoPropiedad.Tipo = entidad.Tipo;
    }


    //Lista con los tipo de propiedades mostrados en el módulo  de administrar urbanizadora.
    $rootScope.selection = [];

    //Verificar si algún checkbox fue seleccionado
    $scope.someSelected = function (object) {
        return Object.keys(object).some(function (key) {
            return object[key];
        });
    }

    //Función para realizar el filtro en el grid.
    $scope.refreshData = function () {
        $scope.gridOpts.data = $filter('filter')($scope.tipoPropiedades, $scope.searchText, undefined);
    };

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "TipoPropiedad_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };


    //Llamado a la función para que se llene el grid.
    $scope.getData();
    //Llamado a la función para que se poblen las las listas de selección.
    $scope.getDataActivos();



});


