﻿angular.module("TipoPropiedadModule").factory("TipoPropiedadFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    var obtenerRelaciones = function (Id) {
        var result = $http({
            method: "get",
            url: appContext + "/TipoPropiedad/ConsultarRelaciones/" + Id
        });
        return result.then(handleSuccess, handleError);
    }


    var obtenerNombreUnico = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/TipoPropiedad/ConsultarPorNombre"
        });
        return result.then(handleSuccess, handleError);
    }

    var obtenerTiposPropiedades = function () {
        var result = $http({
            method: "get",
            url: appContext + "/TipoPropiedad/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }

    var obtenerTiposPropiedadesActivas = function () {
        var result = $http({
            method: "GET",
            url: appContext + "/TipoPropiedad/VerTodosActivos"
        });
        return result.then(handleSuccess, handleError);
    }

    var crearTipoPropiedad = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/TipoPropiedad/CrearTipoPropiedad"
        });
        return sendRequest(result);
    }

    var actualizarTipoPropiedad = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/TipoPropiedad/EditarTipoPropiedad'
        });
        return sendRequest(result);
    }

    var inactivarTipoPropiedad = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/TipoPropiedad/InactivarTipoPropiedad'
        });
        return sendRequest(result);
    }

    var eliminarTipoPropiedad = function (id) {

        var result = $http({
            method: 'POST',
            url: appContext + '/TipoPropiedad/EliminarTipoPropiedad/' + id
        });

        return sendRequest(result);
    }

    return ({
        obtenerTiposPropiedades: obtenerTiposPropiedades,
        obtenerTiposPropiedadesActivas: obtenerTiposPropiedadesActivas,
        crearTipoPropiedad: crearTipoPropiedad,
        actualizarTipoPropiedad: actualizarTipoPropiedad,
        inactivarTipoPropiedad: inactivarTipoPropiedad,
        eliminarTipoPropiedad: eliminarTipoPropiedad,
        obtenerNombreUnico: obtenerNombreUnico,
        obtenerRelaciones: obtenerRelaciones
    });

    function handleSuccess(response) {
        return response.data;
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
});
