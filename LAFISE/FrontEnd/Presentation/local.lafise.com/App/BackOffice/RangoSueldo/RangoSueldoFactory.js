﻿/*==========================================================================
Archivo:            RangoSueldoFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del CUS004 – Administrar rangos de sueldo
Autor:              steven.echavarria                          
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("RangoSueldoModule").factory("RangoSueldoFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta de rango sueldos.
    //Parametro entidad: recibe los filtros de búsqueda: Pais 
    var obtenerRangoSueldos = function (pais) {

        var result = $http({
            method: "GET",
            url: appContext + "/RangoSueldo/ConsultarRangoSueldoFiltro?pais=" + pais
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de obtener el detalle de un rango sueldo
    //Parametro id: identificador del rango sueldo.
    var obtenerDetalleRangoSueldo = function (id) {
        var result = $http({
            method: "GET",
            url: appContext + "/RangoSueldo/ConsultarDetalleRangoSueldo?id=" + id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la actualización de un rango sueldo
    //Parametro entidad: recibe las propiedades: Id, IdPais, IdMoneda, MinSueldo, MaxSueldo
    function actualizarRangoSueldo(entidad) {

        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/RangoSueldo/EditarRangoSueldo'
        });
        return sendRequest(result);
    }

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerRangoSueldos: obtenerRangoSueldos,
        obtenerDetalleRangoSueldo: obtenerDetalleRangoSueldo,
        actualizarRangoSueldo: actualizarRangoSueldo
    });

});