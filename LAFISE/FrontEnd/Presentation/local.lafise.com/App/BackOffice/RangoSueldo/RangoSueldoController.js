﻿/*==========================================================================
Archivo:            RangoSueldoController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del CUS004 – Administrar rangos de sueldo
Autor:              steven.echavarria                          
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/

angular.module("RangoSueldoModule").controller("RangoSueldoController", function ($scope, $rootScope, i18nService, $filter, $uibModal, RangoSueldoFactory, $timeout) {

    //Traduce el grid
    i18nService.setCurrentLang('es');

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.showMsg = false;
    $scope.isEdit = false;

    //Objeto que contiene las propiedades del filtro
    $scope.filtros = {
        Pais: ''
    };

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.editRangoSueldo = {
        Id: '',
        IdPais: '',
        IdMoneda: '',
        RangoSueldoDetalle: '',
    };

    //capturar el pais del formulario
    $scope.newPais = {
        IdPais: ''
    };

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Lista de rango sueldos.
    $scope.rangoSueldos = [];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta según el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.editRangoSueldo.submitted = false;
        $scope.closeAlert(0);
    };

    //Configuración de grid
    $scope.gridOpts = {
        rowHeight: 80,
        columnDefs: [
        { name: 'Pais', field: 'Pais', displayName: 'País' },
        { name: 'Moneda', field: 'Moneda', displayName: 'Moneda' },
        { name: 'RangoSueldo', field: 'ListaRangoSueldo', cellTemplate: '<div ng-repeat="item in row.entity[col.field]">{{item}}</div>' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
    };

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "RangosSueldo_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };

    //Objeto que contiene el id del país en la creación de los rangos sueldos
    $scope.paisCreated = { selected: "" };

    //Objeto que contiene el id de la moneda en la creación de los rangos sueldos
    $scope.monedaCreated = { selected: "" };

    //Método para obtener los datos del servicio y poblar el grid.
    $scope.getData = function () {
        $scope.formMode = false;

        if (typeof $scope.paisesSelected !== "undefined" && typeof $scope.paisesSelected.Paises !== "undefined" && $scope.paisesSelected.Paises != '') {
            $scope.filtros.Pais = $scope.paisesSelected.Paises.join();
        }
        else { $scope.filtros.Pais = ''; }

        RangoSueldoFactory.obtenerRangoSueldos($scope.filtros.Pais).then(function (response) {
            var listaRangoSueldo = [];
            response.forEach(function (item, index) {
                listaRangoSueldo = item.RangoSueldo.split(',')
                response[index].ListaRangoSueldo = listaRangoSueldo;
                listaRangoSueldo = [];
            });
            $scope.gridOpts.data = response;
            $scope.rangoSueldos = response;
        });
    }


    var count = 1;
    // Lista de rangos asociados a una marca.
    $scope.rangos = [{ Id: '', MinSueldo: '', MaxSueldo: '', index: 0, showDelete: false, disabled: false }];

    // funcion para limpiar la lista de rangos
    $scope.setRangos = function () {
        $scope.rangos = [{ Id: '', MinSueldo: '', MaxSueldo: '', index: 0, showDelete: false, disabled: false }];
    }


    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.setRangos();
        $scope.toggleEdit();
        $scope.closeAlert(0);
        //Servicio que obtiene el detalle de la urbanizadora seleccionada
        RangoSueldoFactory.obtenerDetalleRangoSueldo(entidad.IdRangoSueldo).then(function (response) {
            if (response != null) {
                var algo = response;

                //Obtiene el detalle
                $scope.newPais.IdPais = entidad.IdPais;
                $scope.monedaCreated.selected = entidad.IdMoneda;
                $scope.editRangoSueldo.Id = entidad.IdRangoSueldo;

                angular.forEach(response[0].RangoSueldoDetalle, function (value, key) {


                    if ($scope.rangos[0].Id === '') {
                        $scope.rangos[key] = { Id: value.Id, MinSueldo: value.MinSueldo, MaxSueldo: value.MaxSueldo };
                    }
                    else {
                        $scope.rangos.push({ Id: value.Id, MinSueldo: value.MinSueldo, MaxSueldo: value.MaxSueldo, showDelete: true, disabled: true });
                    }

                    $timeout(function () {
                        $('#minimoRango' + key).autoNumeric('init');
                        $('#maximoRango' + key).autoNumeric('init');
                        var valorMinimo = $('#minimoRango' + key).autoNumeric('set', value.MinSueldo);
                        var valorMaximo = $('#maximoRango' + key).autoNumeric('set', value.MaxSueldo);

                        $scope.rangos[key].MinSueldo = valorMinimo[0].value;
                        $scope.rangos[key].MaxSueldo = valorMaximo[0].value;
                    });
                });
            }
        })
    }

    //Función para actualizar el rango sueldo en la BD.
    $scope.update = function () {

        if ($scope.editRangoSueldo.$valid) {

            $scope.editRangoSueldo.RangoSueldoDetalle = $scope.rangos;
            $scope.editRangoSueldo.IdPais = $scope.newPais.IdPais;
            $scope.editRangoSueldo.IdMoneda = $scope.monedaCreated.selected;
            RangoSueldoFactory.actualizarRangoSueldo(this.editRangoSueldo).then(function (response) {
                $scope.getData();
                $scope.gridOpts.paginationCurrentPage = $scope.gridOpts.paginationPageSize;
                $scope.formMode = false;
                $scope.editRangoSueldo.submitted = false;
                $scope.setAlert('success', 'El rango ha sido guardado exitosamente');
            });
        } else {
            $scope.editRangoSueldo.submitted = true;
        }
    };

    // Adiciona nuevo objeto a la lista de rangos.
    $scope.addItem = function () {

        if ($scope.editRangoSueldo.$valid) {
            $scope.closeAlert(0);
            var key = $scope.rangos.length - 1;
            var valMaximo = $('#RangoMax' + key).autoNumeric('get');
            $scope.rangos.push({
                IdRangoSueldo: '', MinSueldo: parseInt(valMaximo) + 1, MaxSueldo: '', index: count, showDelete: true, disabled: true
            });
            count++;

            $timeout(function () {
                $('#minimoRango' + ($scope.rangos.length - 1)).autoNumeric('init');
                var valorMinimo = $('#minimoRango' + ($scope.rangos.length - 1)).autoNumeric('set', parseInt(valMaximo) + 1);
                $scope.rangos[$scope.rangos.length - 1].MinSueldo = valorMinimo[0].value;
            });

        } else {
            $scope.editRangoSueldo.submitted = true;
        }
    };

    //Elimina un ubjeto de la lista de rangos
    $scope.deleteItem = function (index) {


        $('#minimoRango' + index).autoNumeric('init');
        $('#maximoRango' + index).autoNumeric('init');
        var validarMinimo = $('#minimoRango' + index).autoNumeric('get');
        $('#minimoRango' + index).autoNumeric('set', parseInt(validarMinimo));
        $scope.rangos.splice(index, 1);
        var validarMaximo = $('#maximoRango' + (index - 1)).autoNumeric('get');
        var max = parseInt(validarMaximo) + 1;
        var rangoMax = $('#maximoRango' + index).autoNumeric('set', max);
        if (index != $scope.rangos.length) {
            $scope.rangos[index].MinSueldo = rangoMax[0].value;
        } else { $scope.rangos.splice(index, 1); }
        $scope.closeAlert(0);
    };


    //Función para validar que el valor del limite superior del rango sea inferior al valor del limite inferior.
    $scope.ValidarMayor = function (index) {

        $('#minimoRango' + index).autoNumeric('init');
        $('#maximoRango' + index).autoNumeric('init');
        var validarMaximo = $('#RangoMax' + index).autoNumeric('get');
        var validarMinimo = $('#minimoRango' + index).autoNumeric('get');


        if (parseInt(validarMinimo) >= parseInt(validarMaximo)) {
            $scope.rangos[index].MaxSueldo = '';
            $scope.setAlert('danger', 'El límite superior del campo deberá ser superior a  ' + $('#minimoRango' + index).autoNumeric('set', parseInt(validarMinimo))[0].value);
            $scope.editRangoSueldo.$valid = false;
        } else { $scope.editRangoSueldo.$valid = true; }

        //if (typeof $scope.rangos[index + 1].MinSueldo !== "undefined") {

        //    var minPosterior = parseInt($scope.rangos[index + 1].MinSueldo);
        //    $scope.rangos[index + 1].MinSueldo = parseInt($scope.rangos[index].MaxSueldo) + 1;

        //    if (parseInt($scope.rangos[index + 1].MinSueldo) > parseInt($scope.rangos[index + 1].MaxSueldo)) {
        //        $scope.rangos[index + 1].MaxSueldo = '';
        //        $scope.setAlert('danger', 'El límite superior del campo deberá ser superior a  ' + parseInt($scope.rangos[index + 1].MinSueldo) + '');
        //        $scope.editRangoSueldo.$valid = false;
        //    } else { $scope.editRangoSueldo.$valid = true; }

        //}

    };


    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.isEdit = true;
    }

    //Función para cancelar la edicción.
    $scope.cancel = function () {
        $scope.closeAlert(0);
        $scope.formMode = false;
    };


    //Llamado a la función para que se llene el grid.
    $scope.getData();

});

//Función que recorta la longitud del valor ingresado por el valor configurado en la propiedad maxlength
function maxLengthInput(object) {
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}