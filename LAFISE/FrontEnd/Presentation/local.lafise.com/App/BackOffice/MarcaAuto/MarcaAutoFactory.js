﻿angular.module("MarcaAutoModule").factory("MarcaAutoFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";


    var obtenerMarcas = function (filtros) {
        var result = $http({
            method: "POST",
            data: filtros,
            url: appContext + "/MarcaAuto/ConsultarMarcasModelos"
        });
        return result.then(handleSuccess, handleError);
    }

    var obtenerModelosPorMarca = function (Id) {
        var result = $http({
            method: "GET",
            url: appContext + "/MarcaAuto/ConsultarDetalleModelo/" + Id
        });
        return result.then(handleSuccess, handleError);
    }

    var crearMarcaAuto = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/MarcaAuto/CrearMarcasModelos"
        });
        return sendRequest(result);
    }

    var inactivarMarcaAuto = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/MarcaAuto/InactivarMarcaAuto'
        });
        return sendRequest(result);
    }

    var inactivarModeloAuto = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/MarcaAuto/InactivarModeloAuto'
        });
        return sendRequest(result);
    }

    var actualizarMarcaAuto = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/MarcaAuto/EditarMarcasModelos'
        });
        return sendRequest(result);
    }

    var obtenerRelaciones = function (Id) {
        var result = $http({
            method: "get",
            url: appContext + "/MarcaAuto/ConsultarRelaciones/" + Id
        });
        return result.then(handleSuccess, handleError);
    }

    var obtenerRelacionesModelos = function (Id) {
        var result = $http({
            method: "get",
            url: appContext + "/MarcaAuto/ConsultarRelacionesModelos/" + Id
        });
        return result.then(handleSuccess, handleError);
    }

    var eliminarMarcaAuto = function (id) {

        var result = $http({
            method: 'POST',
            url: appContext + '/MarcaAuto/EliminarMarcaAuto/' + id
        });

        return sendRequest(result);
    }


    var eliminarModeloAuto = function (id) {

        var result = $http({
            method: 'POST',
            url: appContext + '/MarcaAuto/EliminarModeloAuto/' + id
        });

        return sendRequest(result);
    }
    var obtenerNombreUnico = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/MarcaAuto/ConsultarPorNombre"
        });
        return result.then(handleSuccess, handleError);
    }

    function handleSuccess(response) {
        return response.data;
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }


    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return ({
        obtenerMarcas: obtenerMarcas,
        crearMarcaAuto: crearMarcaAuto,
        obtenerModelosPorMarca: obtenerModelosPorMarca,
        actualizarMarcaAuto: actualizarMarcaAuto,
        inactivarMarcaAuto: inactivarMarcaAuto,
        obtenerRelaciones: obtenerRelaciones,
        eliminarMarcaAuto: eliminarMarcaAuto,
        obtenerNombreUnico: obtenerNombreUnico,
        inactivarModeloAuto: inactivarModeloAuto,
        obtenerRelacionesModelos: obtenerRelacionesModelos,
        eliminarModeloAuto: eliminarModeloAuto
    });



});