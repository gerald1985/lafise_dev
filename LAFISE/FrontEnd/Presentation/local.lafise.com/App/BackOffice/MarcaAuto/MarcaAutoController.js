﻿angular.module("MarcaAutoModule").controller("MarcaAutoController", function ($scope, $rootScope, i18nService, $filter, Upload, $timeout, $uibModal, uuid2, MarcaAutoFactory) {
    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = false;
    i18nService.setCurrentLang('es');
    $scope.blackListMarcas = { name: '' };
    $scope.blackListModelos = { name: '' };

    $scope.clearValidate = function (evento) {
        $timeout(function () {
            if ($scope.addMarcaAuto.Marca.$error.blacklist) {
                evento.currentTarget.value = "";
            }
        })
    }

    $scope.clearValidateModelo = function (evento, index) {
        $timeout(function () {
            var e = eval("$scope.addMarcaAuto.Modelo" + index)
            if (e.$error.blacklist) {
                evento.currentTarget.value = "";
            }
        })
    }

    $scope.closeAlertDetalleMarca = function () {
        $scope.addMarcaAuto.Marca.$setValidity("blacklist", true);
        $scope.newMarcaAuto.Marca = '';
    }

    $scope.CloseAlertDetalleModelo = function (index) {
        eval("$scope.addMarcaAuto.Modelo" + index).$setValidity("blacklist", true);
        $scope.modelos[index].Modelo = '';
    }

    $scope.CloseAlertImagenModelo = function (index) {
        eval("$scope.addMarcaAuto.fileInvalid" + index).$setValidity("archivoInvalido", true);
        $scope.modelos[index].imagen = '';
    }

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newMarcaAuto = {
        Id: '',
        Marca: '',
        listaModelos: [],
        Activo: true
    };
    var count = 1;
    // Lista de modelos asociados a una marca.
    $scope.modelos = [{ Id: '', Modelo: '', Activo: true, imagen: '', index: 0, errorPeso: '', errorFormato: '', showDelete: false, file: '', modeEdit: true, RutaImagen: '', RutaThumb: '', editCancel: false, EstadoTransaccion: '', deleteRow: true }];

    $scope.DefaultModelos = function () {
        $scope.modelos = [{ Id: '', Modelo: '', Activo: true, imagen: '', index: 0, errorPeso: '', errorFormato: '', showDelete: false, file: '', modeEdit: true, RutaImagen: '', RutaThumb: '', editCancel: false, EstadoTransaccion: '', deleteRow: true }];
    }

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta segun, el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para obtener los modelos de cada marca.
    $scope.GetDetail = function (id) {
        $scope.DefaultModelos();
        MarcaAutoFactory.obtenerModelosPorMarca(id).then(function (response) {
            angular.forEach(response, function (value, key) {
                if ($scope.modelos[0].Id === '') {
                    $scope.modelos[key] = { Id: value.Id, Activo: value.Activo, Modelo: value.Modelo, imagen: value.Imagen, modeEdit: false, RutaImagen: value.RutaImagen, RutaThumb: value.RutaThumb, deleteRow: true, index: key };
                }
                else
                    $scope.modelos.push({ Id: value.Id, Activo: value.Activo, Modelo: value.Modelo, imagen: value.Imagen, modeEdit: false, RutaImagen: value.RutaImagen, RutaThumb: value.RutaThumb, deleteRow: true, index: key });
            });
        });
    }

    $scope.editModelo = function (index) {
        $scope.modelos[index].EstadoTransaccion = 'Edit';
        $scope.modelos[index].modeEdit = true;
        $scope.modelos[index].editCancel = true;
        localStorage.setItem("modelo", $scope.modelos[index].Modelo);
        localStorage.setItem("imagen", $scope.modelos[index].imagen);
        $scope.blackListModelos.name = $scope.blackListModelos.name.replaceAll(',' + $scope.modelos[index].Modelo, '');
    }

    $scope.cancelEdit = function (index) {
        $scope.modelos[index].EstadoTransaccion = '';
        $scope.modelos[index].modeEdit = false;
        $scope.modelos[index].Modelo = localStorage.modelo;
        $scope.modelos[index].imagen = localStorage.imagen;
        localStorage.removeItem("modelo");
        localStorage.removeItem("imagen");
    }

    $scope.activeModelo = function (index) {

        var entidad = { Id: $scope.modelos[index].Id, Activo: !$scope.modelos[index].Activo }
        MarcaAutoFactory.inactivarModeloAuto(entidad).then(function (response) {
            if (response.data === '') {
                $scope.modelos[index].EstadoTransaccion = 'Edit';
                $scope.modelos[index].Activo = !$scope.modelos[index].Activo;
                $scope.getData();
                $scope.closeAlert(0);
            }
            else {
                $scope.modelos[index].EstadoTransaccion = '';
                $scope.setAlert('danger', 'No es posible inactivar el modelo, se encuentra asociado en las casas comerciales: ' + response.data);
                $scope.getData();
            }
        })
    }

    // Adiciona nuevo objeto a la lista de modelos.
    $scope.addItem = function () {
        $scope.modelos.push({
            Modelo: '', Activo: true, imagen: '', index: count, errorPeso: '', errorFormato: '', showDelete: true, file: '', modeEdit: true, EstadoTransaccion: 'Add', deleteRow: true
        });
        $scope.addMarcaAuto.submitted = false;
        count++;
    };

    $scope.deleteItem = function (index) {
        $scope.modelos.EstadoTransaccion = '';
        $scope.modelos.splice(index, 1);
    };


    $scope.filters = {
        Marca: '',
        Modelo: ''
    };

    $scope.filtros = {
        Marca: null,
        Modelo: null
    };

    //Lista de marcas.
    $scope.listaMarcas = [];


    $scope.uploadFiles = function (file, errFiles, index, nombreModelo) {
        $scope.f = file;
        if (errFiles.length != 0) {
            switch (errFiles[0].$error) {
                case 'maxSize':
                    {
                        $scope.errFile = 'El tamaño máximo del archivo es ' + errFiles[0].$errorParam;
                        $scope.modelos[index].errorPeso = $scope.errFile;
                        $scope.modelos[index].errorFormato = '';
                        $scope.modelos[index].imagen = errFiles[0].name;
                        eval("$scope.addMarcaAuto.fileInvalid" + index).$setValidity("archivoInvalido", false);
                        break;
                    }
                case 'pattern':
                    {
                        $scope.errFile = 'Únicamente se permite cargar imágenes con extensión GIF, PNG, JPEG y JPG';
                        $scope.modelos[index].errorFormato = $scope.errFile;
                        $scope.modelos[index].errorPeso = '';
                        $scope.modelos[index].imagen = errFiles[0].name;
                        eval("$scope.addMarcaAuto.fileInvalid" + index).$setValidity("archivoInvalido", false);
                        break;
                    }
            }
        }
        else {
            if ($scope.f != null) {
                var extArray = $scope.f.name.split('.');
                var ext = extArray[extArray.length - 1];
                localStorage.setItem("guid", uuid2.newuuid());
                var nombreArchivo = nombreModelo + '-' + localStorage.guid + '.' + ext;

                $scope.modelos[index].imagen = nombreArchivo;
                $scope.modelos[index].file = $scope.f;
                $scope.modelos[index].errorPeso = '';
                $scope.modelos[index].errorFormato = '';
                eval("$scope.addMarcaAuto.fileInvalid" + index).$setValidity("archivoInvalido", true);
            }
        }
    };


    $scope.SaveFiles = function (file, nombreModelo, index) {
        if (file) {
            var extArray = file.name.split('.');
            var ext = extArray[extArray.length - 1];

            var nombreArchivo = nombreModelo + '-' + localStorage.guid + '.' + ext;
            $scope.modelos[index].imagen = nombreArchivo;
            file.upload = Upload.upload({
                url: '/Uploads/UploadModelosHandler.ashx',
                data: { file: file, NombreArchivo: nombreArchivo }
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    localStorage.removeItem("guid");
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));
            });
        }
    };

    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {
        MarcaAutoFactory.obtenerMarcas($scope.filtros).then(function (response) {
            $scope.gridOpts.data = response;
            $scope.listaMarcas = response;
            angular.forEach($scope.listaMarcas, function (value, key) {
                $scope.blackListMarcas.name += value.Marca + ',';
                $scope.blackListModelos.name += value.Modelo.replaceAll(', ', ',') + ',';
            });

        });
    }

    //Configuración del Grid.
    $scope.gridOpts = {
        columnDefs: [
        { name: 'Marca', field: 'Marca', displayName: 'Marca' },
        { name: 'Modelo', field: 'Modelo', displayName: 'Modelo' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activa/Desactivar" value="Activo/Inactivo" ng-click="grid.appScope.active(row.entity)" >', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
    };

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.newMarcaAuto.Marca = '';
        $scope.DefaultModelos();
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
        $scope.closeAlert(0);
    }


    //Función para cancelar la insercción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.modelos = [{ Id: '', Modelo: '', Activo: true, imagen: '', index: 0, errorPeso: '', errorFormato: '', showDelete: false, file: '', modeEdit: true }];
        $scope.addMarcaAuto.submitted = false;
        $scope.closeAlert(0);
    };


    //Función para realizar el filtro en el grid.
    $scope.refreshData = function () {
        $scope.gridOpts.data = $filter('filter')($scope.listaMarcas, { Marca: $scope.filters.Marca, Modelo: $scope.filters.Modelo }, undefined);
    };

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "MarcaAuto_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };

    //Función para activar o inactivar una marca, en caso de que este asociado a una urbanizadora se mostrara la alerta con las urbanizadoras asociadas.
    $scope.active = function (entidad) {
        entidad.Activo = !entidad.Activo;
        MarcaAutoFactory.inactivarMarcaAuto(entidad).then(function (response) {
            if (response.data === '') {
                $scope.getData();
                $scope.closeAlert(0);
            }
            else {
                $scope.setAlert('danger', 'No es posible inactivar la marca de auto, se encuentra asociada a las casas comerciales: ' + response.data);
                $scope.getData();
            }
        })
    }


    //Función para insertar las marcas de auto a la BD.
    $scope.add = function () {

        if (typeof $scope.addMarcaAuto.$error.archivoInvalido !== "undefined") {
            $scope.addMarcaAuto.$valid = false;
        } else if ($scope.newMarcaAuto.Marca == '' || $scope.modelos[0].Modelo == '') {
            $scope.addMarcaAuto.$valid = false;
        } else { $scope.addMarcaAuto.$valid = true; }


        if ($scope.addMarcaAuto.$valid) {
            // Almacena todos los archivos cargados.
            indexIMG = 0;
            angular.forEach($scope.modelos, function (value, key) {
                if (value.file != '') {
                    $scope.SaveFiles(value.file, value.Modelo, indexIMG);
                    indexIMG++;
                }
            });
            $scope.newMarcaAuto.listaModelos = $scope.modelos;
            MarcaAutoFactory.crearMarcaAuto(this.newMarcaAuto).then(function (response) {
                $scope.getData(true);
                $scope.gridOpts.paginationCurrentPage = Math.ceil($scope.gridOpts.totalItems / $scope.gridOpts.paginationPageSize);
                $scope.formMode = false;
                $scope.addMarcaAuto.submitted = false;
                $scope.setAlert('success', 'La marca de auto ha sido guardada exitosamente');
            });
        } else {
            $scope.addMarcaAuto.submitted = true;
        }
    };

    $scope.update = function () {

        if (typeof $scope.addMarcaAuto.$error.archivoInvalido !== "undefined") {
            $scope.addMarcaAuto.$valid = false;
        } else { $scope.addMarcaAuto.$valid = true; }

        if ($scope.addMarcaAuto.$valid) {
            indexIMG = 0;
            angular.forEach($scope.modelos, function (value, key) {
                if (value.file != '') {
                    $scope.SaveFiles(value.file, value.Modelo, indexIMG);
                    indexIMG++;
                }
            });
            $scope.newMarcaAuto.listaModelos = $scope.modelos;
            MarcaAutoFactory.actualizarMarcaAuto(this.newMarcaAuto).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'El tipo de propiedad ' + $scope.newTipoPropiedad.Tipo + ' ya existe, por favor validar');
                }
                else {
                    $scope.getData();
                    $scope.formMode = false;
                    $scope.addMarcaAuto.submitted = false;
                    $scope.setAlert('success', 'La marca de auto ha sido guardada exitosamente');
                }
            });
        } else {
            $scope.addMarcaAuto.submitted = true;
        }
    }


    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.toggleEdit();
        $scope.newMarcaAuto.Id = entidad.Id;
        $scope.newMarcaAuto.Marca = entidad.Marca;
        $scope.GetDetail(entidad.Id);
        $scope.blackListMarcas.name = '';
        angular.forEach($scope.listaMarcas, function (value, key) {
            if (value.Id != entidad.Id) {
                $scope.blackListMarcas.name += value.Marca + ',';
            }
        });
        $scope.closeAlert(0);
    }

    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
    }

    $scope.openDeleteModelos = function (index) {
        var id = $scope.modelos[index].Id;
        $scope.modelos[index].index = index;
        $scope.canDelete = true;
        MarcaAutoFactory.obtenerRelacionesModelos(id).then(function (response) {
            if (response.CasasComercialesRelacionadas != null && response.RelacionSolicitud === 'OperacionErroneaFK') {
                $scope.setAlert('danger', 'No es posible eliminar la marca de auto, se encuentra asociado a las casas comerciales: ' + response.CasasComercialesRelacionadas + ', y a solicitudes enviadas.');
                $scope.canDelete = false;
            }
            else {
                if (response.CasasComercialesRelacionadas != null) {
                    $scope.setAlert('danger', 'No es posible eliminar la marca de auto, se encuentra asociado a las casas comerciales: ' + response.CasasComercialesRelacionadas);
                    $scope.canDelete = false;
                }
                if (response.RelacionSolicitud === 'OperacionErroneaFK') {
                    $scope.setAlert('danger', 'No es posible eliminar el tipo de propiedad, ésta se encuentra asociado a solicitudes enviadas');
                    $scope.canDelete = false;
                }
            }
            if ($scope.canDelete) {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'modalEliminarModelo.html',
                    scope: $scope,
                    controller: 'ModalEliminarModeloController',
                    size: 'sm',
                    resolve:
                        {
                            Index: function () {
                                return index
                            }
                        }
                });
            }
        });
    }


    //Función para el manejo de mensajes de validación al eliminar una marca y abrir la ventana modal en caso de que permita eliminar
    $scope.open = function (Entidad) {
        $scope.canDelete = true;
        MarcaAutoFactory.obtenerRelaciones(Entidad.Id).then(function (response) {
            if (response.CasasComercialesRelacionadas != null && response.RelacionSolicitud === 'OperacionErroneaFK') {
                $scope.setAlert('danger', 'No es posible eliminar la marca de auto, se encuentra asociado a las casas comerciales: ' + response.CasasComercialesRelacionadas + ', y a solicitudes enviadas.');
                $scope.canDelete = false;
            }
            else {
                if (response.CasasComercialesRelacionadas != null) {
                    $scope.setAlert('danger', 'No es posible eliminar la marca de auto, se encuentra asociado a las casas comerciales: ' + response.CasasComercialesRelacionadas);
                    $scope.canDelete = false;
                }
                if (response.RelacionSolicitud === 'OperacionErroneaFK') {
                    $scope.setAlert('danger', 'No es posible eliminar el tipo de propiedad, ésta se encuentra asociado a solicitudes enviadas');
                    $scope.canDelete = false;
                }
            }
            if ($scope.canDelete) {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'modalEliminar.html',
                    scope: $scope,
                    controller: 'ModalEliminarController',
                    size: 'sm',
                    resolve:
                        {
                            Entity: function () {
                                return Entidad;
                            }
                        }
                });
            }
        });
    };

    //Función para eliminar la marca de un auto en la base de datos.
    $scope.delete = function (Entidad) {
        MarcaAutoFactory.eliminarMarcaAuto(Entidad.Id).then(function (response) {
            $scope.getData();
            $scope.setAlert('success', 'La marca de auto ' + Entidad.Marca + ' ha sido eliminada');
        })
    }


    //Función para eliminar la marca de un auto en la base de datos.
    $scope.deleteModelo = function (index) {
        var Id = $scope.modelos[index].Id;
        // MarcaAutoFactory.eliminarModeloAuto(Id).then(function (response) {
        //  $scope.getData();
        $scope.modelos[index].EstadoTransaccion = 'Delete';
        $scope.closeAlert(0);
        $scope.modelos[index].deleteRow = false;
        //$scope.modelos.splice(index, 1);
        //   })
    }

    //Función para validar que el nombre de la marca sea único en la tabla al momento de insertar o actualizar.
    $scope.UniqueName = function () {
        MarcaAutoFactory.obtenerNombreUnico(this.newMarcaAuto).then(function (response) {
            if (response != null) {
                $scope.setAlert('danger', 'La marca de auto ' + $scope.newMarcaAuto.Marca + ' ya existe, por favor validar');
                $scope.newMarcaAuto.Marca = '';
            }
            else {
                $scope.closeAlert(0);
            }
        });
    };


    $scope.getData();


});