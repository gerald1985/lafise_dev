﻿angular.module('MarcaAutoModule').controller('ModalEliminarModeloController', function ($scope, $modalInstance, Index) {

    $scope.ok = function () {
        $modalInstance.close($scope.deleteModelo(Index));
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $scope.closeAlert(0);
    };
});