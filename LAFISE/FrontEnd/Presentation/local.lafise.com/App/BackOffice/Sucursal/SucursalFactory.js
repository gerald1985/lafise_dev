﻿/*==========================================================================
Archivo:            SucursalFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del CUS011- Administrar sucursal
Autor:              paola.munoz                        
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("SucursalModule").factory("SucursalFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta todos las sucursal. 
    var obtenerSucursal = function () {
        var result = $http({
            method: "get",
            url: appContext + "/Sucursal/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta de sucursales.
    //Parametro entidad: recibe los filtros de búsqueda: Pais, departamento, ciudad, TipoSucursal  y NombreSucursal
    var obtenerSucursalFiltro = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Sucursal/ConsultarSucursalFiltro"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consultar una sucursal por su nombre y así validar su existencia
    //Parametro entidad: recibe el nombre y el id de sucursal. 
    var obtenerNombreUnico = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Sucursal/ConsultarPorNombre"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consultar una sucursal y sus relaciones con los formularios
    //Parametro entidad: recibe el id de  sucursal. 
    var obtenerRelaciones = function (id) {
        var result = $http({
            method: "get",
            url: appContext + "/Sucursal/ConsultarRelaciones?id="+id
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consumir el servicio para la consulta todos los tipo sucursal activos. 
    var obtenerTipoSucursalActivos = function () {
        var result = $http({
            method: "get",
            url: appContext + "/TipoSucursal/VerTodosActivos"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de crear una sucursal 
    //Parametro entidad: recibe la entidad de sucursal.
    var crearSucursal = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Sucursal/CrearSucursal"
        });
        return sendRequest(result);
    }

    //Función encargada de activar una sucursal 
    //Parametro entidad: recibe la entidad de sucursal.
    var actualizarSucursal = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Sucursal/EditarSucursal'
        });
        return sendRequest(result);
    }

    //Función encargada de inactivar una sucursal 
    //Parametro entidad: recibe la entidad de sucursal.
    var inactivarSucursal = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Sucursal/InactivarSucursal'
        });
        return sendRequest(result);
    }

    //Función encargada de eliminar una sucursal 
    //Parametro entidad: recibe la entidad de sucursal.
    var eliminarSucursal = function (id) {

        var result = $http({
            method: 'get',
            url: appContext + '/Sucursal/EliminarSucursal/' + id
        });

        return sendRequest(result);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerSucursal: obtenerSucursal,
        obtenerSucursalFiltro: obtenerSucursalFiltro,
        obtenerNombreUnico: obtenerNombreUnico,
        crearSucursal: crearSucursal,
        actualizarSucursal: actualizarSucursal,
        inactivarSucursal: inactivarSucursal,
        eliminarSucursal: eliminarSucursal,
        obtenerRelaciones: obtenerRelaciones,
        obtenerTipoSucursalActivos: obtenerTipoSucursalActivos
    });

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

});
