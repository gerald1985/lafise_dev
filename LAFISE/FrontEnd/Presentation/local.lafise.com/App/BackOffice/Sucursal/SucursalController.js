﻿angular.module("SucursalModule").controller("SucursalController", function ($scope, $filter, $uibModal, i18nService, SucursalFactory, DepartamentoFactory, CiudadFactory) {
    //Traducir el grid
    i18nService.setCurrentLang('es');

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = false;
    $scope.showMsg = false;
    $scope.isEdit = false;
    $scope.departamentosPais = [];

    //Objeto que contiene las propiedades del filtro
    $scope.filtros = {
        NombrePais: '',
        NombreDepartamento: '',
        NombreCiudad: '',
        TipoSucursal: '',
        NombreSucursal: ''
    };

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newSucursal = {
        Id: '',
        IdPais: '',
        IdDepartamento: '',
        IdCiudad: '',
        IdTipoSucursal: '',
        Nombre: '',
        Activo: true
    };

    //capturar el pais del formulario
    $scope.newPais = {
        IdPais: ''
    };

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Lista de sucursales.
    $scope.sucursal = [];

    //Lista de tipo sucursal activas
    $scope.tipoSucursalActivos = [];

    //Objeto que contiene el identificador del tipo sucursal de los filtros de busqueda
    $scope.tipoSucursalSelected = { TipoSucursal: "" };

    //Objeto usado para manipular el valor de nombre de sucursal
    $scope.nombreSucursalSelected = '';

    $scope.departamentoTemp = [];

    $scope.ciudadTemp = [];

    $scope.ciudadDepartamento = [];

    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {
        //console.log($scope.tipoSucursalSelecccionada);
        $scope.closeAlert(0);
        if (typeof $scope.paisesSelected !== "undefined" && typeof $scope.paisesSelected.Paises !== "undefined" && $scope.paisesSelected.Paises != '') {
            $scope.filtros.NombrePais = $scope.paisesSelected.Paises.join();
        } else {
            $scope.filtros.NombrePais = null;
        }

        if (typeof $scope.departamentoSelected !== "undefined" && typeof $scope.departamentoSelected.Departamento !== "undefined" && $scope.departamentoSelected.Departamento != '') {
            $scope.filtros.NombreDepartamento = $scope.departamentoSelected.Departamento.join();
        } else {
            $scope.filtros.NombreDepartamento = null;
        }

        if (typeof $scope.ciudadSelected !== "undefined" && typeof $scope.ciudadSelected.Ciudad !== "undefined" && $scope.ciudadSelected.Ciudad != '') {
            $scope.filtros.NombreCiudad = $scope.ciudadSelected.Ciudad.join();
        } else {
            $scope.filtros.NombreCiudad = null;
        }

        if ($scope.tipoSucursalSelected.TipoSucursal !== undefined && $scope.tipoSucursalSelected.TipoSucursal != '') {
            $scope.filtros.TipoSucursal = $scope.tipoSucursalSelected.TipoSucursal;
        } else {
            $scope.filtros.TipoSucursal = null;
        }

        if ($scope.nombreSucursalSelected != '') {
            $scope.filtros.NombreSucursal = $scope.nombreSucursalSelected;
        } else {
            $scope.filtros.NombreSucursal = null;
        }

        SucursalFactory.obtenerSucursalFiltro($scope.filtros).then(function (response) {
            $scope.gridOpts.data = response;
            $scope.sucursal = response;
        });

        SucursalFactory.obtenerTipoSucursalActivos().then(function (response) {
            $scope.tipoSucursalActivos = response;
        });
    };

    //Función para el manejo de mensajes de validación al eliminar una sucursal y abrir la ventana modal en caso de que permita eliminar
    $scope.open = function (entidad) {
        $scope.canDelete = true;
        $scope.newSucursal.Id = entidad.IdSucursal;
        $scope.newSucursal.Nombre = entidad.NombreSucursal;
        if ($scope.canDelete) {
            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'modalEliminar.html',
                scope: $scope,
                controller: 'ModalEliminarController',
                size: 'sm',
                resolve:
                    {
                        Entity: function () {
                            return entidad;
                        }
                    }
            });
        }
    };

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta segun, el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la inserción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.addSucursal.submitted = false;
        $scope.newSucursal.Nombre = '';
        $scope.newSucursal.Id = '';
        $scope.newSucursal.IdDepartamento = '';
        $scope.newSucursal.IdCiudad = '';
        $scope.newSucursal.IdTipoSucursal = '';
        $scope.closeAlert(0);
    };


    //Configuración del Grid.
    $scope.gridOpts = {
        columnDefs: [
        { name: 'NombrePais', field: 'NombrePais', displayName: 'País' },
        { name: 'NombreDepartamento', field: 'NombreDepartamento', displayName: 'Zona/Departamento' },
        { name: 'NombreCiudad', field: 'NombreCiudad', displayName: 'Ciudad' },
        { name: 'TipoSucursal', field: 'TipoSucursal', displayName: 'Tipo Sucursal' },
        { name: 'NombreSucursal', field: 'NombreSucursal', displayName: 'Nombre de la Sucursal' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activa/Desactivar" value="Activo/Inactivo"  ng-click="grid.appScope.active(row.entity)">', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
    };

    //Función para insertar una sucursal en la base de datos.
    $scope.add = function () {
        if ($scope.addSucursal.$valid) {
            $scope.newSucursal.IdPais = $scope.newPais.IdPais;
            SucursalFactory.crearSucursal(this.newSucursal).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'La sucursal ' + sucursal.newSucursal.TipoSucursal1 + ' ya existe, por favor validar');
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.gridOpts.paginationCurrentPage = Math.ceil($scope.gridOpts.totalItems / $scope.gridOpts.paginationPageSize);
                    $scope.addSucursal.submitted = false;
                    $scope.setAlert('success', 'La sucursal ha sido guardada exitosamente');
                }
            });
        } else {
            $scope.addSucursal.submitted = true;
        }
    };

    //Función para actualizar una sucursal en la base de datos.
    $scope.update = function () {
        if ($scope.addSucursal.$valid) {
            $scope.newSucursal.IdPais = $scope.newPais.IdPais;
            SucursalFactory.actualizarSucursal(this.newSucursal).then(function (response) {
                if (response.data.MensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'La sucursal ' + $scope.newSucursal.Nombre + ' ya existe, por favor validar');
                    $scope.newSucursal.Nombre = '';
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.addSucursal.submitted = false;
                    $scope.setAlert('success', 'La sucursal ha sido guardada exitosamente');
                }
            })
        } else {
            $scope.addSucursal.submitted = true;
        }
    }

    //Función para activar o inactivar un tipo sucursal
    $scope.active = function (entidad) {
        $scope.closeAlert(0);
        entidad.Id = entidad.IdSucursal;
        entidad.Activo = !entidad.Activo;
        SucursalFactory.inactivarSucursal(entidad).then(function (response) {
            $scope.getData();
        })
    }

    //Función para eliminar la sucursal en la base de datos.
    $scope.delete = function (entidad) {
        $scope.closeAlert(0);
        SucursalFactory.eliminarSucursal(entidad.IdSucursal).then(function (response) {
            if (response.data === true) {
                $scope.getData();
                $scope.setAlert('success', 'La sucursal ' + entidad.NombreSucursal + ' ha sido eliminada');
            }
            else {
                $scope.setAlert('danger', 'No es posible eliminar el tipo de sucursal, se encuentra asociado a las sucursales: ' + response.data);
                $scope.getData();
            }
        })
    }

    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.closeAlert(0);
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
        $scope.isEdit = true;
    }

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        $scope.newSucursal.Nombre = '';
        $scope.newSucursal.Id = '';
        if ($scope.newPais.IdPais != '') {
            $scope.mostrarDepartamento();
        }
        $scope.newSucursal.IdDepartamento = '';
        $scope.newSucursal.IdCiudad = '';
        $scope.newSucursal.IdTipoSucursal = '';
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
        $scope.isEdit = false;
    }

    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.closeAlert(0);
        $scope.toggleEdit();
        $scope.newSucursal.Id = entidad.IdSucursal;
        $scope.newSucursal.IdPais = entidad.IdPais;
        $scope.newPais.IdPais = entidad.IdPais;
        DepartamentoFactory.obtenerDepartamentoPais($scope.newSucursal.IdPais).then(function (response) {
            if (response != null) {
                $scope.departamentosPais = response;
            }
        });
        $scope.newSucursal.IdDepartamento = entidad.IdDepartamento;
        CiudadFactory.obtenerCiudadPorDepartamento($scope.newSucursal.IdDepartamento).then(function (response) {
            if (response != null) {
                $scope.ciudadDepartamentos = response;
            }
        });
        $scope.newSucursal.IdCiudad = entidad.IdCiudad;
        $scope.newSucursal.IdTipoSucursal = entidad.IdTipoSucursal;
        $scope.newSucursal.Nombre = entidad.NombreSucursal;
        $scope.newSucursal.Activo = entidad.Activo;
    }

    //Función para validar que el nombre del tipo sucursal sea único en la tabla al momento de insertar o actualizar.
    $scope.UniqueName = function () {
        SucursalFactory.obtenerNombreUnico(this.newSucursal).then(function (response) {
            if (response != null) {
                $scope.setAlert('danger', 'La sucursal  ' + $scope.newSucursal.Nombre + ' ya existe, por favor validar');
                $scope.newSucursal.Nombre = '';
            }
            else {
                $scope.closeAlert(0);
            }
        });
    };

    //Mostrar los departamentos teniendo en cuenta el país seleccionado
    $scope.mostrarDepartamento = function () {
        DepartamentoFactory.obtenerDepartamentoPais($scope.newPais.IdPais).then(function (response) {
            if (response != null) {
                $scope.departamentosPais = response;
            }
        });
    };

    //Mostrar las ciudades teniendo en cuenta el departamento seleccionado
    $scope.mostrarCiudad = function () {
        CiudadFactory.obtenerCiudadPorDepartamento($scope.newSucursal.IdDepartamento).then(function (response) {
            if (response != null) {
                $scope.ciudadDepartamentos = response;
            }
        });
    };

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "Sucursal_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };

    //Mostrar los departamentos teniendo en cuenta el país seleccionado
    $scope.mostrarDepartamentoFiltro = function () {
        DepartamentoFactory.obtenerListaDepartamentoPais($scope.paisesSelected.Paises).then(function (response) {
            if (response != null) {
                $scope.departamentoPais = response;

                angular.forEach($scope.departamentoSelected.Departamento, function (departamentoSelect, index) {
                    angular.forEach($scope.departamentoPais, function (idDepartamento) {
                        if (parseInt(idDepartamento.Id) == parseInt(departamentoSelect)) {
                            $scope.departamentoTemp.push(departamentoSelect);
                            return;
                        }
                    });
                });
                if ($scope.departamentoTemp.length > 0) {
                    $scope.departamentoSelected.Departamento = [];
                    $scope.departamentoSelected.Departamento = $scope.departamentoTemp;
                    $scope.departamentoTemp = [];
                } else {
                    $scope.departamentoSelected.Departamento = $scope.departamentoTemp;
                }
                $scope.mostrarCiudadFiltro();
            } else {
                $scope.departamentoPais = null;
            }
        });
    };

    //Mostrar las ciudades teniendo en cuenta los departamentos seleccionado
    $scope.mostrarCiudadFiltro = function () {
        CiudadFactory.obtenerListaCiudadDepartamento($scope.departamentoSelected.Departamento).then(function (response) {
            if (response != null) {
                $scope.ciudadDepartamento = response;

                angular.forEach($scope.ciudadSelected.Ciudad, function (ciudadSelect, index) {
                    angular.forEach($scope.ciudadDepartamento, function (idCiudad) {
                        if (parseInt(idCiudad.Id) == parseInt(ciudadSelect)) {
                            $scope.ciudadTemp.push(ciudadSelect);
                            return;
                        }
                    });
                });
                if ($scope.ciudadTemp.length > 0) {
                    $scope.ciudadSelected.Ciudad = [];
                    $scope.ciudadSelected.Ciudad = $scope.ciudadTemp;
                    $scope.ciudadTemp = [];
                } else {
                    $scope.ciudadSelected.Ciudad = $scope.ciudadTemp;
                }

            } else {
                $scope.ciudadDepartamento = null;
            }
        });
    };


    $scope.getData();
});