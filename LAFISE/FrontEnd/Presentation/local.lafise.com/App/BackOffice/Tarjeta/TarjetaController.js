﻿angular.module("TarjetaModule").controller("TarjetaController", function ($scope, Upload, i18nService, $timeout, $filter, $uibModal, TarjetaFactory) {
    //Traducir el grid
    i18nService.setCurrentLang('es');

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = false;
    $scope.showMsg = false;
    $scope.isEdit = false;

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newTarjeta = {
        Id: '',
        TarjetaCredito: '',
        Imagen: '',
        Activo: true,
        RutaImagen:''
    };

    //Guardar la información de la imagen
    $scope.tarjetaImagen = {
        errorPeso: '',
        errorFormato: '',
        file:''
    }

    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    //Lista de tarjeta.
    $scope.tarjeta = [];

    //Objeto que contiene el identificador del tarjeta de los filtros de busqueda
    $scope.TarjetaSelected = { TarjetaCredito: '' };

    //Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {
        $scope.closeAlert(0);
       
        TarjetaFactory.obtenerTarjeta($scope.TarjetaSelected.TarjetaCredito).then(function (response) {
            $scope.gridOpts.data = response;
            $scope.tarjeta = response;
        });
    };

    //Función para el manejo de mensajes de validación al eliminar una tarjeta y abrir la ventana modal en caso de que permita eliminar
    $scope.open = function (entidad) {
        var modalInstance = $uibModal.open({
            animation: false,
            templateUrl: 'modalEliminar.html',
            scope: $scope,
            controller: 'ModalEliminarController',
            size: 'sm',
            resolve:
                {
                    Entity: function () {
                        return entidad;
                    }
                }
        });
    };

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta segun, el index siempre debe ser 0.
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción o la edicción.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.addTarjeta.submitted = false;
        $scope.closeAlert(0);
    };

    //Configuración del Grid.
    $scope.gridOpts = {
        columnDefs: [
        { name: 'TarjetaCredito', field: 'TarjetaCredito', displayName: 'Tarjeta Crédito' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activa/Desactivar" value="Activo/Inactivo"  ng-click="grid.appScope.active(row.entity)">', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
    };

    //Función para insertar un tipo de sucursal en la base de datos.
    $scope.add = function () {
        if ($scope.newTarjeta.Imagen == '') {
            $scope.addTarjeta.fileInvalid.$setValidity("archivoInvalido", false);
        }
        if ($scope.addTarjeta.$valid) {
            $scope.SaveFiles($scope.tarjetaImagen.file);
            TarjetaFactory.crearTarjeta(this.newTarjeta).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'La tarjeta de crédito ' + $scope.newTarjeta.TarjetaCredito + ' ya existe, por favor validar');
                    $scope.newTarjeta.TarjetaCredito = '';
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.gridOpts.paginationCurrentPage = Math.ceil($scope.gridOpts.totalItems / $scope.gridOpts.paginationPageSize);
                    $scope.addTarjeta.submitted = false;
                    $scope.setAlert('success', 'La tarjeta de crédito ha sido guardada exitosamente');
                }

            });
        } else {
            $scope.addTarjeta.submitted = true;
        }
    };

    //Función para actualizar un tipo de sucursal en la base de datos.
    $scope.update = function () {
        if ($scope.addTarjeta.$valid) {
            $scope.SaveFiles($scope.tarjetaImagen.file);
            TarjetaFactory.actualizarTarjeta(this.newTarjeta).then(function (response) {
                if (response.data.MensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'La tarjeta de crédito ' + $scope.newTipoSucursal.TipoSucursal1 + ' ya existe, por favor validar');
                    $scope.newTipoSucursal.TipoSucursal1 = '';
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.addTarjeta.submitted = false;
                    $scope.setAlert('success', 'La tarjeta de crédito ha sido guardada exitosamente');
                }
            })
        } else {
            $scope.addTarjeta.submitted = true;
        }
    }

    //Función para activar o inactivar una tarjeta
    $scope.active = function (entidad) {
        $scope.closeAlert(0);
        entidad.Activo = !entidad.Activo;
        TarjetaFactory.inactivarTarjeta(entidad).then(function (response) {
            $scope.getData();
        })
    }

    //Función para eliminar una tarjeta en la base de datos.
    $scope.delete = function (entidad) {
        TarjetaFactory.eliminarTarjeta(entidad.Id).then(function (response) {
            $scope.getData();
            $scope.setAlert('success', 'La tarjeta de crédito ' + entidad.TarjetaCredito + ' ha sido eliminada');
        })
    }

    //Función para mostrar el formulario en modo de edicción.
    $scope.toggleEdit = function () {
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
        $scope.isEdit = true;
    }

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        $scope.newTarjeta.TarjetaCredito = '';
        $scope.newTarjeta.Imagen = '';
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
        $scope.isEdit = false;

    }

    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.closeAlert(0);
        $scope.toggleEdit();
        $scope.newTarjeta.Id = entidad.Id;
        $scope.newTarjeta.TarjetaCredito = entidad.TarjetaCredito;
        $scope.newTarjeta.Imagen = entidad.Imagen;
        $scope.newTarjeta.Activo = entidad.Activo;
        $scope.newTarjeta.RutaImagen = entidad.RutaImagen;
    }

  
    //Función para validar que el nombre de la tarjeta sea único en la tabla al momento de insertar o actualizar.
    $scope.UniqueName = function () {
        TarjetaFactory.obtenerNombreUnico(this.newTarjeta).then(function (response) {
            if (response != null) {
                $scope.setAlert('danger', 'El tipo de sucursal  ' + $scope.newTarjeta.TarjetaCredito + ' ya existe, por favor validar');
                $scope.newTarjeta.TarjetaCredito = '';
            }
            else {
                $scope.closeAlert(0);
            }
        });
    };

    //exportar a excel los datos del grid
    $scope.exportData = function () {
        $scope.formMode = false;
        var blob = new Blob(["\ufeff", document.getElementById('exportable').innerHTML], {
            type: "application/vnd.ms-excel;charset=utf-8"
        });
        var nombreArchivo = "Tarjeta_" + new Date().toLocaleDateString() + ".xls";
        saveAs(blob, nombreArchivo);
    };

    $scope.uploadFiles = function (file, errFiles) {
        $scope.f = file;
        if (errFiles.length != 0) {
            switch (errFiles[0].$error) {
                case 'maxSize':
                    {
                        $scope.errFile = 'El tamaño máximo del archivo es ' + errFiles[0].$errorParam;
                        $scope.tarjetaImagen.errorPeso = $scope.errFile;
                        $scope.tarjetaImagen.errorFormato = '';
                        $scope.newTarjeta.Imagen = '';
                        $scope.addTarjeta.fileInvalid.$setValidity("archivoInvalido", false);

                        break;
                    }
                case 'pattern':
                    {
                        $scope.errFile = 'Únicamente se permite cargar imágenes con extensión GIF, PNG, JPEG y JPG';
                        $scope.tarjetaImagen.errorFormato = $scope.errFile;
                        $scope.tarjetaImagen.errorPeso = '';
                        $scope.newTarjeta.Imagen = '';
                        $scope.addTarjeta.fileInvalid.$setValidity("archivoInvalido", false);
                        break;
                    }
            }
        }
        else {
            if ($scope.f != null) {
                $scope.newTarjeta.Imagen = $scope.f.name;
                $scope.tarjetaImagen.file = $scope.f;
                $scope.tarjetaImagen.errorPeso = '';
                $scope.tarjetaImagen.errorFormato = '';
                $scope.newTarjeta.RutaImagen = '';
                $scope.addTarjeta.fileInvalid.$setValidity("archivoInvalido", true);
            }
        }
    };

    $scope.SaveFiles = function (file) {
        if (file) {
            file.upload = Upload.upload({
                url: '/Uploads/UploadTarjetaHandler.ashx',
                data: { file: file }
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                                         evt.loaded / evt.total));
            });
        }
    };

    $scope.getData();

});