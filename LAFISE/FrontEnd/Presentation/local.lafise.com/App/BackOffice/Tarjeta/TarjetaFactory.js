﻿/*==========================================================================
Archivo:            TarjetaFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    del CUS007- Administrar tarjeta
Autor:              paola.munoz                        
Fecha de creación:  27/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                            
==========================================================================*/
angular.module("TarjetaModule").factory("TarjetaFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta todos las tarjetas. 
    var obtenerTarjeta = function (tarjeta) {
        var result = $http({
            method: "get",
            url: appContext + "/Tarjeta/ConsultarTarjetaFiltro?tarjetaCredito=" + tarjeta
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de consultar una tarjeta por su nombre y así validar su existencia
    //Parametro entidad: recibe el nombre y el id de tarjeta.  
    var obtenerNombreUnico = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Tarjeta/ConsultarPorNombre"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de crear un tarjeta
    //Parametro entidad: recibe la entidad de tarjeta
    var crearTarjeta = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Tarjeta/CrearTarjeta"
        });
        return sendRequest(result);
    }

    //Función encargada de actualizar un tarjeta
    //Parametro entidad: recibe la entidad de tarjeta
    var actualizarTarjeta = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Tarjeta/EditarTarjeta'
        });
        return sendRequest(result);
    }

    //Función encargada de inactivar un tarjeta
    //Parametro entidad: recibe la entidad de tarjeta
    var inactivarTarjeta = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Tarjeta/InactivarTarjeta'
        });
        return sendRequest(result);
    }

    //Función encargada de eliminar un tarjeta
    //Parametro entidad: recibe el id  de tarjeta
    var eliminarTarjeta = function (id) {

        var result = $http({
            method: 'POST',
            url: appContext + '/Tarjeta/EliminarTarjeta/' + id
        });

        return sendRequest(result);
    }

    //Retorna cada una de las funciones del factory
    return ({
        obtenerTarjeta: obtenerTarjeta,
        crearTarjeta: crearTarjeta,
        actualizarTarjeta: actualizarTarjeta,
        inactivarTarjeta: inactivarTarjeta,
        eliminarTarjeta: eliminarTarjeta,
        obtenerNombreUnico: obtenerNombreUnico
    });

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }

    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

});
