﻿/*==========================================================================
Archivo:            IndicadorController
Descripción:        Objeto creado para implementar la lógica de la presentación
                    del CRUD de indicadores - Administrar indicadores
Autor:              carlos.arboleda                      
Fecha de creación:  19/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/

angular.module("IndicadorModule").controller("IndicadorController", function ($scope, i18nService, $uibModal,IndicadorFactory) {
    //Traducir el grid
    i18nService.setCurrentLang('es');

    //Propiedades para mostrar y ocultar elementos en el modulo.
    $scope.formMode = false;
    $scope.editMode = false;
    $scope.addMode = true;
    $scope.showMsg = false;
    $scope.isEdit = false;

    //Objeto para el manejo de la entidad dentro del grid, inserción, actualización y eliminación.
    $scope.newIndicador = {
        Id: '',
        NombreIndicador: '',
        SimboloActual: '',
        SimboloHistorico: '',
        Activo: true
    };

    $scope.filtros = {
        Simbolo: '',
        FechaInicial: '',
        FechaFinal: ''
    };


    //Objeto para el manejo de los mensajes de alerta.
    $scope.alerts = [{ type: '', msg: '' }];

    ////Método para obtener los datos del servicio y llenar el grid.
    $scope.getData = function () {
        IndicadorFactory.obtenerIndicadores().then(function (response) {
            $scope.gridOpts.data = response;
        });
    };

    //Configuración del Grid que contiene todos los indicadores de la base de datos
    $scope.gridOpts = {
        columnDefs: [
        { name: 'NombreIndicador', field: 'NombreIndicador', displayName: 'Nombre Indicador' },
        { name: 'SimboloActual', field: 'SimboloActual', displayName: 'Simbolo Actual' },
        { name: 'SimboloHistorico', field: 'SimboloHistorico', displayName: 'Simbolo Historico' },
        { field: 'Activo', cellTemplate: '<input type="button" title="Activar/Desactivar" value="Activo/Inactivo"  ng-click="grid.appScope.active(row.entity)">', sortable: false, cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return (grid.getCellValue(row, col)) ? 'activo' : 'inactivo'; }, displayName: 'Cambiar Estado' },
        { field: 'image_url', cellTemplate: '<input class="editarGrid" title="Editar" type="button" value="Editar" ng-click="grid.appScope.edit(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Editar' },
        { field: 'image_url', cellTemplate: '<input class="eliminarGrid" title="Eliminar" type="button" value="Eliminar" ng-click="grid.appScope.open(row.entity)">', sortable: false, cellClass: 'column-button', displayName: 'Eliminar' }
        ],
        paginationPageSizes: [10, 20, 30, 40, 50],
        paginationPageSize: 20,
        enableColumnResizing: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
    };

    //Función para el manejo de mensajes de validación al eliminar un indicador y abrir la ventana modal
    $scope.open = function (entidad) {
        $scope.newIndicador.Id = entidad.Id;
        $scope.newIndicador.NombreIndicador = entidad.NombreIndicador;
        $scope.newIndicador.SimboloActual = entidad.SimboloActual;
        $scope.newIndicador.SimboloHistorico = entidad.SimboloHistorico;
        $scope.newIndicador.Activo = entidad.Activo;
        var modalInstance = $uibModal.open({
            animation: false,
            templateUrl: 'modalEliminar.html',
            scope: $scope,
            controller: 'ModalEliminarController',
            size: 'sm',
            resolve:
                {
                    Entity: function () {
                        return entidad;
                    }
                }
        });     
    };

    //Función para el establecer el tipo de mensaje y la descripción del mensaje el cual se va a mostrar en la alerta. 
    $scope.setAlert = function (typemMsg, nameMsg) {
        $scope.showMsg = true;
        $scope.alerts.splice(0, 1);
        $scope.alerts.push({ type: typemMsg, msg: nameMsg });
    };

    //Función para cerrar la alerta. El index debe ser cero para cerrarla
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    //Función para cancelar la insercción o la edición.
    $scope.cancel = function () {
        $scope.formMode = false;
        $scope.addIndicador.submitted = false;
        //limpiamos los campos del formulario
        $scope.newIndicador.Id = '';
        $scope.newIndicador.NombreIndicador = '';
        $scope.newIndicador.SimboloActual = '';
        $scope.newIndicador.SimboloHistorico = '';
        $scope.newIndicador.Activo = true;
        $scope.closeAlert(0);
    };

    //Función para desplegar el formulario para crear un nuevo indicador
    $scope.create = function () {
        $scope.formMode = true;
        $scope.editMode = false;
        $scope.addMode = true;
        //limpiamos los campos del formulario
        $scope.newIndicador.Id = '';
        $scope.newIndicador.NombreIndicador = '';
        $scope.newIndicador.SimboloActual = '';
        $scope.newIndicador.SimboloHistorico = '';
        $scope.newIndicador.Activo = true;
        $scope.closeAlert(0);
    }

    //Función para insertar un indicador en la base de datos.
    $scope.add = function () {
        if ($scope.addIndicador.$valid) {
            IndicadorFactory.crearIndicador(this.newIndicador).then(function (response) {
                if (response.data.mensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'El indicador ' + $scope.newIndicador.NombreIndicador + ' ya existe, por favor validar');
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.gridOpts.paginationCurrentPage = Math.ceil($scope.gridOpts.totalItems / $scope.gridOpts.paginationPageSize);
                    $scope.addIndicador.submitted = false;
                    $scope.setAlert('success', 'El indicador ' + $scope.newIndicador.NombreIndicador + ' ha sido creado exitosamente');
                }
            });
        } else {
            $scope.addIndicador.submitted = true;
        }
    };

    //Función para actualizar un indicador en la base de datos.
    $scope.update = function () {
        if ($scope.addIndicador.$valid) {
            IndicadorFactory.actualizarIndicador(this.newIndicador).then(function (response) {
                if (response.data.MensajeError === 'OperacionErroneaDuplicada') {
                    $scope.setAlert('danger', 'El indicador ' + $scope.newIndicador.NombreIndicador + ' ya existe, por favor validar');
                    $scope.newIndicador.NombreIndicador = '';
                }
                else {
                    $scope.formMode = false;
                    $scope.getData();
                    $scope.addIndicador.submitted = false;
                    $scope.setAlert('success', 'El indicador ' + $scope.newIndicador.NombreIndicador + ' ha sido guardado exitosamente');
                }
            })
        } else {
            $scope.addIndicador.submitted = true;
        }
    }

    //Función para activar o inactivar un indicador
    $scope.active = function (entidad) {
        $scope.closeAlert(0);
        entidad.Activo = !entidad.Activo;
        IndicadorFactory.inactivarIndicador(entidad).then(function (response) {
            $scope.getData();     
        })
    }

    //Función para eliminar un tipo indicador en la base de datos.
    $scope.delete = function (entidad) {
        $scope.closeAlert(0);
        IndicadorFactory.eliminarIndicador(entidad.Id).then(function (response) {
            $scope.getData();
            $scope.setAlert('success', 'El indicador ' + entidad.NombreIndicador + ' ha sido eliminado');           
        })
    }

    //Función para mostrar el formulario en modo de edición.
    $scope.toggleEdit = function () {
        $scope.formMode = true;
        $scope.editMode = true;
        $scope.addMode = false;
        $scope.isEdit = true;
    }

    //Función para mostrar el formulario en modo de creación.
    $scope.toggleCreate = function () {
        $scope.closeAlert(0);
        $scope.newIndicador.NombreIndicador = '';
        $scope.formMode = true;
        $scope.addMode = true;
        $scope.editMode = false;
        $scope.isEdit = false;
    }

    //Función para establer los datos que se desean editar.
    $scope.edit = function (entidad) {
        $scope.closeAlert(0);
        $scope.toggleEdit();
        $scope.newIndicador.Id = entidad.Id;
        $scope.newIndicador.NombreIndicador = entidad.NombreIndicador;
        $scope.newIndicador.SimboloActual = entidad.SimboloActual;
        $scope.newIndicador.SimboloHistorico = entidad.SimboloHistorico;
        $scope.newIndicador.Activo = entidad.Activo;
        $scope.editMode = true;
    }

    //Función para validar que el nombre del indicador sea único en la tabla al momento de insertar o actualizar.
    $scope.UniqueName = function () {
        IndicadorFactory.validarIndicadorUnico(this.newIndicador).then(function (response) {
            if (response != null) {             
                $scope.setAlert('danger', 'El indicador  ' + $scope.newIndicador.NombreIndicador + ' ya existe, por favor validar');
                $scope.newIndicador.NombreIndicador = '';
                return false;
            }
            else {
                $scope.closeAlert(0);
                return true;
            }
        });
    };

    //Función para validar que el simbolo ingresado para el indicador sea válido
    $scope.verificarIndicador = function () {
        IndicadorFactory.verificarIndicadorExiste(this.newIndicador.SimboloActual).then(function (response) {
            var resp = response;
            if (resp == false) {
                $scope.setAlert('danger', 'El Simbolo Actual ' + $scope.newIndicador.SimboloActual + ' no existe, por favor validar');
                $scope.newIndicador.SimboloActual = '';
                $scope.newIndicador.SimboloHistorico = '';
                return false;
            }
            else {
                $scope.closeAlert(0);
                return true;
            }
        });
    };


    //Función para generar la fecha inicial que se cuenta n días atrás a partir de la fecha actual 
    $scope.generarFechaInicial = function (dias) {
        var milisegundos = parseInt(35 * 24 * 60 * 60 * 1000);
        var fecha = new Date();
        var day = fecha.getDate();
        // el mes es devuelto entre 0 y 11
        var month = fecha.getMonth() + 1;
        var year = fecha.getFullYear();
        //Obtenemos los milisegundos desde media noche del 1/1/1970
        var tiempo = fecha.getTime();
        //Calculamos los milisegundos sobre la fecha que hay que sumar o restar
        milisegundos = parseInt((dias * -1) * 24 * 60 * 60 * 1000);
        //Modificamos la fecha actual
        var total = fecha.setTime(tiempo + milisegundos);
        day = fecha.getDate();
        month = fecha.getMonth() + 1;
        year = fecha.getFullYear();
        if (month < 10) {
            month = "0" + month;
        }
        if (day < 10) {
            day = "0" + day;
        }
        //creamos el string de fecha en el formato requerido
        var fechaInicial = year + "-" + month + "-" + day;
        return fechaInicial;
    }

    
    //Función para generar la fecha inicial que se cuenta n días atrás a partir de la fecha actual 
    $scope.generarFechaFinal = function () {
        var milisegundos = parseInt(35 * 24 * 60 * 60 * 1000);
        var fecha = new Date();
        var day = fecha.getDate();
        // el mes es devuelto entre 0 y 11
        var month = fecha.getMonth() + 1;
        var year = fecha.getFullYear();
        //Obtenemos los milisegundos desde media noche del 1/1/1970
        var tiempo = fecha.getTime();
        if (month < 10) {
            month = "0" + month;
        }
        if (day < 10) {
            day = "0" + day;
        }
        //creamos el string de fecha en el formato requerido
        var fechaFinal = year + "-" + month + "-" + day;
        return fechaFinal;
    }

    //Función para validar que el simbolo ingresado para el histórico sea válido
    $scope.verificarIndicadorHistorico = function () {
        $scope.filtros.Simbolo = this.newIndicador.SimboloHistorico;
        $scope.filtros.FechaInicial = $scope.generarFechaInicial(15);
        $scope.filtros.FechaFinal = $scope.generarFechaFinal();
        IndicadorFactory.verificarIndicadorHistoricoExiste($scope.filtros).then(function (response) {
            var resp = response;
            if (resp == false) {
                $scope.setAlert('danger', 'El Símbolo Histórico ' + $scope.newIndicador.SimboloHistorico + ' no existe, por favor validar o dejar el campo sin diligenciar.');
                $scope.newIndicador.SimboloHistorico = '';
                return false;
            }
            else {
                $scope.closeAlert(0);
                return true;
            }
        });
    };

    $scope.getData();
});