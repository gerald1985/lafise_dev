﻿/*==========================================================================
Archivo:            IndicadorFactory
Descripción:        Objeto creado para implementar el llamado de los servicios CRUD
                    indicadores
Autor:              arley.lopez                        
Fecha de creación:  17/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A                                            
==========================================================================*/
angular.module("IndicadorModule").factory("IndicadorFactory", function ($http, $q) {
    var appContext = "/DesktopModules/Servicios/API";

    //Función encargada de consumir el servicio para la consulta todos los indicadores. 
    var obtenerIndicadores = function () {
        var result = $http({
            method: "get",
            url: appContext + "/Indicador/VerTodos"
        });
        return result.then(handleSuccess, handleError);
    }

    
    //Función encargada de crear un indicador 
    //Parametro entidad: recibe la entidad de indicador.
    var crearIndicador = function (entidad) {

        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Indicador/CrearIndicador"
        });
        return sendRequest(result);
    }

    //Función encargada de editar un indicador 
    //Parametro entidad: recibe la entidad de indicador.
    var actualizarIndicador = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Indicador/EditarIndicador'
        });
        return sendRequest(result);
    }

    //Función encargada de inactivar un indicador 
    //Parametro entidad: recibe la entidad de indicador.
    var inactivarIndicador = function (entidad) {
        var result = $http({
            method: 'POST',
            data: entidad,
            url: appContext + '/Indicador/InactivarIndicador'
        });
        return sendRequest(result);
    }

    //Función encargada de eliminar un indicador 
    //Parametro entidad: recibe la entidad de indicador.
    var eliminarIndicador = function (id) {

        var result = $http({
            method: 'get',
            url: appContext + '/Indicador/EliminarIndicador/' + id
        });

        return sendRequest(result);
    }

    //Función encargada de consultar un indicador por su nombre y así validar su existencia
    //Parametro entidad: recibe el nombre y el id del indicador.  
    var validarIndicadorUnico = function (entidad) {
        var result = $http({
            method: "POST",
            data: entidad,
            url: appContext + "/Indicador/ValidarIndicadorUnico"
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de validar si el símbolo para 
    //datos actuales si es correcto en Yahoo Finance
    var verificarIndicadorExiste = function (simboloActual) {
        var result = $http({
            method: "get",
            url: appContext + "/Indicador/VerificarIndicador?simbolo=" + simboloActual
        });
        return result.then(handleSuccess, handleError);
    }

    //Función encargada de validar si el símbolo para 
    //datos históricos si es correcto en Yahoo Finance
    var verificarIndicadorHistoricoExiste = function (entidad) {
        var result = $http({
            method: "POST",
            data : entidad,
            url: appContext + "/Indicador/VerificarIndicadorHistorico"
        });
        return result.then(handleSuccess, handleError);
    }


    //Retorna cada una de las funciones del factory
    return ({
        obtenerIndicadores: obtenerIndicadores,
        crearIndicador: crearIndicador,
        actualizarIndicador: actualizarIndicador,
        inactivarIndicador: inactivarIndicador,
        eliminarIndicador: eliminarIndicador,
        validarIndicadorUnico: validarIndicadorUnico,
        verificarIndicadorExiste: verificarIndicadorExiste,
        verificarIndicadorHistoricoExiste: verificarIndicadorHistoricoExiste
    });

    //Función encargada de resolver una operación exitosa en el consumo del servicio
    function handleSuccess(response) {
        return response.data;
    }

    //Función encargada de resolver una operación con error en el consumo del servicio
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return $q.reject("An unknown error occurred.");
        }
        return $q.reject(response.data.message);
    }


    //Función encargada de resolver el resultado del consumo de un servicio
    function sendRequest(config) {
        var deferred = $q.defer();
        config.then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

});