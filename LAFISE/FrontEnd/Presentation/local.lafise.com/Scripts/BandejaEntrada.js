﻿// A simple templating method for replacing placeholders enclosed in curly braces.
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$(function () {

    var ticker = $.connection.bandejaEntrada;

    $MarcaAuto = $('#MarcaAuto');
    $PrestamoPersonal = $('#PrestamoPersonal');
    $PrestamoHipotecario = $('#PrestamoHipotecario');
    $TarjetaCredito = $('#TarjetaCredito');
    $TarjetaDebito = $('#TarjetaDebito');
    $PrestamoEducativo = $('#PrestamoEducativo');
    $AperturaCuenta = $('#AperturaCuenta');
    $SeguroVidaAccidente = $('#SeguroVidaAccidente');
    $SeguroVehicular = $('#SeguroVehicular');
    $Reclamos = $('#Reclamos');
    
    $TableMarcaAuto = $MarcaAuto.find('tbody');
    $TablePrestamoPersonal = $PrestamoPersonal.find('tbody');
    $TablePrestamoHipotecario = $PrestamoHipotecario.find('tbody');
    $TableTarjetaCredito = $TarjetaCredito.find('tbody');
    $TableTarjetaDebito = $TarjetaDebito.find('tbody');
    $TablePrestamoEducativo = $PrestamoEducativo.find('tbody');
    $TableAperturaCuenta = $AperturaCuenta.find('tbody');
    $TableSeguroVidaAccidente = $SeguroVidaAccidente.find('tbody');
    $TableSeguroVehicular = $SeguroVehicular.find('tbody');
    $TableReclamos = $Reclamos.find('tbody');
    
    rowTemplate = '<tr><td>{IdTipoSolicitud}</td><td>{NumeroSolicitud}</td><td>{NombreCompleto}</td><td>{FechaCreacion}</td></tr>';

    function formatmessage(message) {
        return $.extend(message, { Id: message.Id, IdTipoSolicitud: message.IdTipoSolicitud, NumeroSolicitud: message.NumeroSolicitud, NombreCompleto: message.NombreCompleto, FechaCreacion: message.FechaCreacion  });
    }

    function init() {

        console.log('Now connected, connection ID =' + $.connection.hub.id);

        //Bandeja Entrada - Marca Auto
        ticker.server.getBandejaEntradaMarcaAuto().done(function (messages) {
            $TableMarcaAuto.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TableMarcaAuto.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Prestamo Personal
        ticker.server.getBandejaEntradaPrestamoPersonal().done(function (messages) {
            $TablePrestamoPersonal.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TablePrestamoPersonal.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Prestamo Hipotecario
        ticker.server.getBandejaEntradaPrestamoHipotecario().done(function (messages) {
            $TablePrestamoHipotecario.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TablePrestamoHipotecario.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Tarjeta Crédito
        ticker.server.getBandejaEntradaTarjetaCredito().done(function (messages) {
            $TableTarjetaCredito.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TableTarjetaCredito.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Tarjeta Débito
        ticker.server.getBandejaEntradaTarjetaDebito().done(function (messages) {
            $TableTarjetaDebito.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TableTarjetaDebito.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Prestamo Educativo
        ticker.server.getBandejaEntradaPrestamoEducativo().done(function (messages) {
            $TablePrestamoEducativo.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TablePrestamoEducativo.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Apertura Cuenta
        ticker.server.getBandejaEntradaAperturaCuenta().done(function (messages) {
            $TableAperturaCuenta.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TableAperturaCuenta.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Seguro de Vida y Accidente
        ticker.server.getBandejaEntradaSeguroVidaAccidente().done(function (messages) {
            $TableSeguroVidaAccidente.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TableSeguroVidaAccidente.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Seguro Vehicular
        ticker.server.getBandejaEntradaSeguroVehicular().done(function (messages) {
            $TableSeguroVehicular.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TableSeguroVehicular.append(rowTemplate.supplant(message));
            });
        });

        //Bandeja Entrada - Reclamos
        ticker.server.getBandejaEntradaReclamos().done(function (messages) {
            $TableReclamos.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $TableReclamos.append(rowTemplate.supplant(message));
            });
        });
    }

    //********************************************************************************//
    // Add a client-side hub method that the server will call
    //********************************************************************************//

    //Cambio en Bandeja Entrada - Marca Auto
    ticker.client.pushGestorBandejaEntradaMarcaAuto = function (messages) {
        $TableMarcaAuto.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TableMarcaAuto.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Prestamo Personal
    ticker.client.pushGestorBandejaEntradaPrestamoPersonal = function (messages) {
        $TablePrestamoPersonal.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TablePrestamoPersonal.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Prestamo Hipotecario
    ticker.client.pushGestorBandejaEntradaPrestamoHipotecario = function (messages) {
        $TablePrestamoHipotecario.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TablePrestamoHipotecario.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Tarjeta Crédito
    ticker.client.pushGestorBandejaEntradaTarjetaCredito = function (messages) {
        $TableTarjetaCredito.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TableTarjetaCredito.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Tarjeta Débito
    ticker.client.pushGestorBandejaEntradaTarjetaDebito = function (messages) {
        $TableTarjetaDebito.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TableTarjetaDebito.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Prestamo Educativo
    ticker.client.pushGestorBandejaEntradaPrestamoEducativo = function (messages) {
        $TablePrestamoEducativo.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TablePrestamoEducativo.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Apertura Cuenta
    ticker.client.pushGestorBandejaEntradaAperturaCuenta = function (messages) {
        $TableAperturaCuenta.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TableAperturaCuenta.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Seguro de Vida y Accidente
    ticker.client.pushGestorBandejaEntradaSeguroVidaAccidente = function (messages) {
        $TableSeguroVidaAccidente.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TableSeguroVidaAccidente.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Seguro vehicular
    ticker.client.pushGestorBandejaEntradaSeguroVehicular = function (messages) {
        $TableSeguroVehicular.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TableSeguroVehicular.append(rowTemplate.supplant(message));
        });
    }

    //Cambio en Bandeja Entrada - Seguro vehicular
    ticker.client.pushGestorBandejaEntradaReclamos = function (messages) {
        $TableReclamos.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $TableReclamos.append(rowTemplate.supplant(message));
        });
    }

    // Start the connection
    $.connection.hub.start().done(init);

});