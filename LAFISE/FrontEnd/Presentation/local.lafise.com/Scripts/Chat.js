﻿// A simple templating method for replacing placeholders enclosed in curly braces.
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$(function () {

    var ticker = $.connection.Chat,
        $messageTable = $('#messageTable'),
        $messageTableBody = $messageTable.find('tbody'),
        rowTemplate = '<tr><td>{User}</td><td>{Message}</td></tr>';

    function formatmessage(message) {
        return $.extend(message, { User: message.User, Message: message.Message });
    }

    function init() {
        ticker.server.getAllMessage().done(function (messages) {
            $messageTableBody.empty();
            $.each(messages, function () {
                var message = formatmessage(this);
                $messageTableBody.append(rowTemplate.supplant(message));
            });
        });
    }

    // Add a client-side hub method that the server will call
    ticker.client.addMessage = function (messages) {
        $messageTableBody.empty();
        $.each(messages, function () {
            var message = formatmessage(this);
            $messageTableBody.append(rowTemplate.supplant(message));
        });
    }

    // Start the connection
    $.connection.hub.start().done(init);

});