﻿/*==========================================================================
Archivo:            BandejaEntradaDto
Descripción:        Clase en la que se definen los metodos que se comunicarán con SignalR
Autor:              steven.echavarria                          
Fecha de creación:  08/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A
==========================================================================*/

#region Referencias
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using SignalR.BandejaEntrada.Dtos;
using System.Web.Http; 
#endregion

[AllowAnonymous]
public class GestorBandejaEntradaHub : Hub
{
    /// <summary>
    /// Metodo encargado de enviar las solicitudes registradas en cada formulario a los gestores
    /// </summary>
    /// <param name="_dto">objeto de dominio que será enviado a los gestores</param>
    /// <returns></returns>
    public Task Sendsolicitud(BandejaEntradaDto _dto)
    {
        return Clients.All.broadcastSolicitud(_dto);
    }
}
