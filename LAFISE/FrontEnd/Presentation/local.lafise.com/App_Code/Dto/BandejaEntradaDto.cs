﻿/*==========================================================================
Archivo:            BandejaEntradaDto
Descripción:        Dto que se tendra en cuenta para la comunicación SignalR en los gestores
Autor:              steven.echavarria                          
Fecha de creación:  08/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2016 Arkix S.A
==========================================================================*/

using System;

namespace SignalR.BandejaEntrada.Dtos
{
    public class BandejaEntradaDto
    {
        public int IdSolicitud { get; set; }
        public int IdTipoSolicitud { get; set; }
        public string NumeroSolicitud { get; set; }
        public string NombreCompleto { get; set; }
        public string FechaCreacion { get; set; }
        public long IdPais { get; set; }
    }
}