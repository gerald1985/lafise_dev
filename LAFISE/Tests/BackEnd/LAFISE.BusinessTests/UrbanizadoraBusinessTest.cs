﻿/*==========================================================================
Archivo:            UrbanizadoraDataTest
Descripción:        (Descripción de UrbanizadoraDataTest..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  29/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Business;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
#endregion

namespace LAFISE.BusinessTests
{
    [TestClass]
    public class UrbanizadoraBusinessTest
    {
        private IUrbanizadoraBusiness targetU;

        [TestInitialize()]
        public void Carga()
        {
            targetU = new UrbanizadoraBusiness();
        }

        [TestMethod]
        public async Task VerTodos()
        {
            UrbanizadoraTipoPropiedadDto _urbanizadoraTipoPropiedad = new UrbanizadoraTipoPropiedadDto { Pais = "1,2", Urbanizadora = "ar", TipoPropiedad = "1,2" };

            var result = await targetU.VerUrbanizadorasTiposPropiedades(_urbanizadoraTipoPropiedad);
            Assert.IsTrue(result.Result.Count > 0);
        }

        [TestMethod]
        public async Task VerPorId()
        {
            var result = await targetU.VerUrbanizadoraPorId(2);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CrearUrbanizadora()
        {
            List<TipoPropiedadDto> _listaDetalle = new List<TipoPropiedadDto>();
            _listaDetalle.Add(new TipoPropiedadDto { Id = 3 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 5 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 7 });
            UrbanizadoraDto _urbanizadora = new UrbanizadoraDto { Nombre = "Arkix", IdPais = 1, Activo = true,  ListaPropiedades = _listaDetalle};

            var result = await targetU.CrearUrbanizadora(_urbanizadora);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EditarUrbanizadora()
        {
            List<TipoPropiedadDto> _listaDetalle = new List<TipoPropiedadDto>();
            _listaDetalle.Add(new TipoPropiedadDto { Id = 4 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 2 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 1 });
            UrbanizadoraDto _urbanizadora = new UrbanizadoraDto { Id = 8, Nombre = "Harquis", IdPais = 2, Activo = true, ListaPropiedades = _listaDetalle };

            var result = await targetU.EditarUrbanizadora(_urbanizadora);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarUrbanizadora()
        {
            var result = await targetU.EliminarUrbanizadoraPorId(4,"host");
            Assert.IsTrue(result.SuccessfulOperation);
        }

        [TestMethod]
        public async Task InactivarUrbanizadora()
        {
            UrbanizadoraDto _urbanizadora = new UrbanizadoraDto { Id = 2, Activo = true };
            var result = await targetU.InactivarUrbanizadora(_urbanizadora);
            Assert.IsTrue(result.SuccessfulOperation);
        }
    }
}
