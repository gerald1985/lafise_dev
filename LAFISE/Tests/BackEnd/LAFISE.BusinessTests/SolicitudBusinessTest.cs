﻿/*==========================================================================
Archivo:            SolicitudBusinessTest
Descripción:        (Descripción de TarjetaBusinessTest..........)                      
Autor:              paola.munoz                        
Fecha de creación:  18/12/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Business;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic; 
#endregion

namespace LAFISE.BusinessTests
{
    [TestClass]
    public class SolicitudBusinessTest
    {
        private ISolicitudBusiness solicitud;

        [TestInitialize()]
        public void Carga()
        {
            solicitud = new SolicitudBusiness();
        }

        [TestMethod]
        public async Task EditarSolicitudBussiness()
        {
            List<HistoricoComentarioDto> _historicoComentario = new List<HistoricoComentarioDto>();
            _historicoComentario.Add(new HistoricoComentarioDto { Comentario = "prueba", IdEstado = 2, FechaCreacion = DateTime.Now });
            SolicitudDto _solicitud = new SolicitudDto { IdEstado = 2, Id = 1, IdTipoContacto = 1, PathUrl = "http://local.lafise.com/gestores", HistoricoComentario = _historicoComentario };

            var result = await solicitud.EditarSolicitud(_solicitud,"tester");
            Assert.IsTrue(result != null);
        }

      
    }
}
