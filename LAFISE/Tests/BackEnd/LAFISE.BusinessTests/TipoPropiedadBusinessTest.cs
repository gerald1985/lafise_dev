﻿/*==========================================================================
Archivo:            TipoPropiedadBusinessTest
Descripción:        (Descripción de TipoPropiedadBusinessTest..........)                      
Autor:              paola.munoz                        
Fecha de creación:  01/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Business;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic; 
#endregion

namespace LAFISE.BusinessTests
{
    [TestClass]
    public class TipoPropiedadBusinessTest
    {
        private ITipoPropiedadBusiness tipoPropiedad;

        [TestInitialize()]
        public void Carga()
        {
            tipoPropiedad = new TipoPropiedadBusiness();
        }

        [TestMethod]
        public async Task CrearTipoPropiedadBusinnes()
        {
            TipoPropiedadDto _tipoPropiedad = new TipoPropiedadDto { Tipo="pams pams", Activo=false};
            var result = tipoPropiedad.Crear(_tipoPropiedad);

            Assert.IsTrue(result.Result.SuccessfulOperation);
        }

        [TestMethod]
        public async Task VerTodosTipoPropiedadBusinnes()
        {
            var result = await tipoPropiedad.VerTodosGrid();
            Assert.IsTrue(result.Result.Count > 0);
        }

        [TestMethod]
        public async Task VerTipoPropiedadIdBusinnes()
        {
            var result = await tipoPropiedad.VerPorId(1);
            Assert.IsTrue(result != null);
        }



        [TestMethod]
        public async Task EditarTipoPropiedadBusiness()
        {
            TipoPropiedadDto _tipoPropiedad = new TipoPropiedadDto { Id = 1, Tipo = "pams pams", Activo = true };
            var result = await tipoPropiedad.Editar(_tipoPropiedad);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarTipoPropiedadBusiness()
        {
            await tipoPropiedad.Eliminar(1,"host");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TipoPropiedadBusiness()
        {
            string tipo = "1,2";
            var result = await tipoPropiedad.TipoPropiedadFiltro(tipo);
            Assert.IsTrue(result.Result.Count > 0);
        }

        [TestMethod]
        public async Task InactivarTipoPropiedadBusiness()
        {
            TipoPropiedadDto _tipoPropiedad = new TipoPropiedadDto { Id = 5, Tipo = "prueba guardar 2", Activo = false };
            var result = await tipoPropiedad.Inactivar(_tipoPropiedad);

            Assert.IsTrue(result.SuccessfulOperation);
        }
    }
}
