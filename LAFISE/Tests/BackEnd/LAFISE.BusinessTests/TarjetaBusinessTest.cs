﻿/*==========================================================================
Archivo:            TarjetaBusinessTest
Descripción:        (Descripción de TarjetaBusinessTest..........)                      
Autor:              paola.munoz                        
Fecha de creación:  01/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Business;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic; 
#endregion

namespace LAFISE.BusinessTests
{
    [TestClass]
    public class TarjetaBusinessTest
    {
        private ITarjetaBusiness tarjeta;

        [TestInitialize()]
        public void Carga()
        {
            tarjeta = new TarjetaBusiness();
        }

        [TestMethod]
        public async Task CrearTarjetaBusinnes()
        {
            TarjetaDto _tarjeta = new TarjetaDto { Imagen="prueba pams", TarjetaCredito="Visa",Activo=false};
            var result = tarjeta.Crear(_tarjeta);

            Assert.IsTrue(result.Result.SuccessfulOperation);
        }

        [TestMethod]
        public async Task VerTodosTarjetaBusinnes()
        {
            var result = await tarjeta.VerTodosGrid();
            Assert.IsTrue(result.Result.Count > 0);
        }

        [TestMethod]
        public async Task VerTarjetaIdBusinnes()
        {
            var result = await tarjeta.VerPorId(1);
            Assert.IsTrue(result!=null);
        }



        [TestMethod]
        public async Task EditarTarjetaBusiness()
        {
            TarjetaDto _tarjeta = new TarjetaDto { Id=1, TarjetaCredito="pruebannn",Imagen="si",Activo=false};
            var result = await tarjeta.Editar(_tarjeta);
           
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarTarjetaBusiness()
        {
            await tarjeta.Eliminar(1,"host");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TipoTarjetaBusiness()
        {
            string tarj= "1,2";
            var result = await tarjeta.TarjetaFiltro(tarj);
            Assert.IsTrue(result.Result.Count > 0);
        }

        [TestMethod]
        public async Task InactivarTarjetaBusiness()
        {
            TarjetaDto _tarjeta = new TarjetaDto { Id = 1, TarjetaCredito = "pruebannn", Imagen = "si", Activo = true };
            var result = await tarjeta.Inactivar(_tarjeta);

            Assert.IsTrue(result.SuccessfulOperation);
        }
    }
}
