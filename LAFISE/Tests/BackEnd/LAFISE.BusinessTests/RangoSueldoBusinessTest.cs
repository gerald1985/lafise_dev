﻿/*==========================================================================
Archivo:            RangoSueldoBusinessTest
Descripción:        (Descripción de RangoSueldoBusinessTest..........)                      
Autor:              paola.munoz                        
Fecha de creación:  01/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Business;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic; 
#endregion

namespace LAFISE.BusinessTests
{
    [TestClass]
    public class RangoSueldoBusinessTest
    {
        private IRangoSueldoBusiness rangoSueldo;
       
        [TestInitialize()]
        public void Carga()
        {
            rangoSueldo = new RangoSueldoBusiness();
        }

        [TestMethod]
        [TestCategory("RangoSueldoBusiness")]
        public async Task VerTodos()
        {
            var result = await rangoSueldo.VerTodos();
            Assert.IsTrue(result.Result.Count > 0);
        }

        [TestMethod]
        [TestCategory("RangoSueldoBusiness")]
        public async Task VerRangoSueldoFiltro()
        {
            string pais = "1,2";
            var result = await rangoSueldo.VerRangoSueldoFiltro(pais);
            Assert.IsTrue(result.Result.Count > 0);
        }

        [TestMethod]
        [TestCategory("RangoSueldoBusiness")]
        public async Task VerporId()
        {
            var result = await rangoSueldo.VerPorId(2);
            Assert.IsTrue(result.SuccessfulOperation);
        }

       
    }
}
