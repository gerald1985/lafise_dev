﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public class SucursalTest
    {
        private ISucursalData sucursalData = new SucursalData();

        [TestMethod]
        public async Task VerTodosSucursal()
        {
            var result = await sucursalData.VerTodosGrid();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearSucursal()
        {
            SucursalDto _sucursal = new SucursalDto {  Activo = true };
            var result = await sucursalData.Crear(_sucursal);
            
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        //[ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearSucursalDtoCompleto()
        {
            SucursalDto _sucursal = new SucursalDto {IdCiudad=2,IdDepartamento=1,IdPais=1,Nombre="prueba pams", IdTipoSucursal=1, Activo = true };
            var result = await sucursalData.Crear(_sucursal);

            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task EditarSucursal()
        {
            SucursalDto _sucursal = new SucursalDto { Id = 1,  Activo = false };
            var result = await sucursalData.Editar(_sucursal);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarSucursal()
        {
            await sucursalData.Eliminar(2);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task SucursalFiltro()
        {
            SucursalFiltroDto sucursal = new SucursalFiltroDto();

            var result = await sucursalData.SucursalFiltro(sucursal);
            Assert.IsTrue(result.Count > 0);
        }

    }
}
