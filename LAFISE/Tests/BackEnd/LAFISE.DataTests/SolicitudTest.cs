﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;


namespace LAFISE.DataTests
{

    [TestClass]
    public class SolicitudTest
    {

        private ISolicitudData solicitudData;

        [TestInitialize()]
        public void Carga()
        {
            solicitudData = new SolicitudData();
        }
        [TestMethod]
        public async Task VerTodosSolicitud()
        {

            var result = await solicitudData.VerTodos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerFormularioGeneralPorIdSolicitud()
        {
            var result = await solicitudData.VerFormularioGeneralPorIdSolicitud(0);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task VerFormularioConReferenciasPorIdSolicitud()
        {
            var result = await solicitudData.VerFormularioConReferenciasPorIdSolicitud(0);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task VerFormularioSeguroVidaPorIdSolicitud()
        {
            var result = await solicitudData.VerFormularioSeguroVidaPorIdSolicitud(0);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task VerFormularioReclamosPorIdSolicitud()
        {
            var result = await solicitudData.VerFormularioReclamosPorIdSolicitud(0);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task VerGestorSolicitud()
        {
            //var result = await solicitudData.VerGestorSolicitud("1",244);
            //Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarMarcaAuto()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 1;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = "1,3";
            var result = await solicitudData.CargarExportarMarcaAuto(solicitudFiltro);
            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task CargarExportarPrestamoPersonal()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 2;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarPrestamoPersonal(solicitudFiltro);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarPrestamoHipotecario()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 3;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarPrestamoHipotecario(solicitudFiltro);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarTarjetaCredito()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 4;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarTarjetaCreditoDebito(solicitudFiltro);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarTarjetaDebito()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 5;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarTarjetaCreditoDebito(solicitudFiltro);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarPrestamoEducativo()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 6;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarPrestamoEducativo(solicitudFiltro);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarAperturaCuenta()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 7;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarAperturaCuenta(solicitudFiltro);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarSeguroVidaAccidente()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 8;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarSeguroVidaAccidente(solicitudFiltro);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CargarExportarSeguroVehicular()
        {
            SolicitudCondicionesDto solicitudFiltro = new SolicitudCondicionesDto();
            solicitudFiltro.IdTipoSolicitud = 8;
            solicitudFiltro.IdPais = 244;
            solicitudFiltro.Estado = null;
            var result = await solicitudData.CargarExportarSeguroVehicular(solicitudFiltro);
            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task CrearSolicitud()
        {

            SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = 1, IdPais = 2, FechaCreacion = DateTime.Now };

            var result = await solicitudData.CrearSolicitud(_solicitud);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CrearSolicitudDatosPersonales()
        {
            int ts = 1;
            for (int i = 0; i < 100; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Paola" + i, Apellidos = "Muñoz", Telefono = "2343424", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Camila" + i, Apellidos = "Muñoz", Telefono = "2343424", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero", EsCodeudor = true });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }

                Assert.IsTrue(result != null);
            }


        }

        [TestMethod]
        public async Task CrearSolicitudDatosFinancieros()
        {
            int ts = 1;
            for (int i = 0; i < 100; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Mariana" + i, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = null });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }

                Assert.IsTrue(result != null);
            }
        }

        [TestMethod]
        public async Task CrearSolicitudDatosFinancierosPromedio()
        {
            int ts = 1;
            for (int i = 0; i < 100; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Luisa" + i, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = "10" });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }

                Assert.IsTrue(result != null);
            }
        }

        [TestMethod]
        public async Task CrearSolicitudDatosFinancierosActividad()
        {
            int ts = 1;
            for (int i = 0; i < 1000; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Luisa" + i, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                List<ActividadEconomicaDto> _actividadEconomica = new List<ActividadEconomicaDto>();
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 1 });
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 2 });
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = "", ActividadEconomica = _actividadEconomica });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }

                Assert.IsTrue(result != null);
            }
        }

        [TestMethod]
        public async Task CrearSolicitudDatosFinancierosSoporte()
        {
            int ts = 1;
            for (int i = 0; i < 1000; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Luisa" + 1, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                List<ActividadEconomicaDto> _actividadEconomica = new List<ActividadEconomicaDto>();
                List<SoporteDto> _soporte = new List<SoporteDto>();
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 1 });
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 2 });
                _soporte.Add(new SoporteDto { IdTipoSoporte = 2, Nombre = "Negocio propio", Ruta = "ruta" });
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = "", ActividadEconomica = _actividadEconomica });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros, Soportes = _soporte };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }
                Assert.IsTrue(result != null);
            }
        }

        [TestMethod]
        public async Task CrearSolicitudPlanFinancieros()
        {
            int ts = 1;
            for (int i = 0; i < 1000; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Luisa" + i, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                List<ActividadEconomicaDto> _actividadEconomica = new List<ActividadEconomicaDto>();
                List<PlanFinanciacionDto> _planFinanciacion = new List<PlanFinanciacionDto>();
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 1 });
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 2 });
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = "", ActividadEconomica = _actividadEconomica });
                _planFinanciacion.Add(new PlanFinanciacionDto { ValorBien = 2222, MontoFinanciar = 3242, PrimaAnticipo = 3534543, Plazo = 12, AutoUsado = false, IdCategoria = 1, IdMarcaAuto = 1, IdModelo = 1, IdCasaComercial = 1 });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros, PlanFinanciacion = _planFinanciacion };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }
                Assert.IsTrue(result != null);
            }
        }

        [TestMethod]
        public async Task CrearSolicitudPlanFinancieros2()
        {
            int ts = 1;
            for (int i = 0; i < 1000; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Pams" + i, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                List<ActividadEconomicaDto> _actividadEconomica = new List<ActividadEconomicaDto>();
                List<PlanFinanciacionDto> _planFinanciacion = new List<PlanFinanciacionDto>();
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 1 });
                _actividadEconomica.Add(new ActividadEconomicaDto { IdTipoActividad = 2 });
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = "", ActividadEconomica = _actividadEconomica });
                _planFinanciacion.Add(new PlanFinanciacionDto { MontoFinanciar = 3242, IdGarantia = 2, Plazo = 12, IdCategoria = null });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros, PlanFinanciacion = _planFinanciacion };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }
                Assert.IsTrue(result != null);
            }
        }

        [TestMethod]
        public async Task CrearSolicitudReferncia()
        {
            int ts = 1;
            for (int i = 0; i < 1000; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Pams" + i, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero", EsCodeudor = false });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = "" });
                List<ReferenciaDto> _referencia = new List<ReferenciaDto>();
                _referencia.Add(new ReferenciaDto { Nombre = "prueba", Apellidos = "prueba2", Telefono = "3434343" });
                _referencia.Add(new ReferenciaDto { Nombre = "prueba22", Apellidos = "prueba322", Telefono = "24234234" });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = ts, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros, Referencias = _referencia };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }
                Assert.IsTrue(result != null);
            }
        }


        [TestMethod]
        public async Task CrearSolicitudDatoAuto()
        {
            int ts = 1;
            for (int i = 0; i < 1000; i++)
            {
                List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
                _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Pams"+i, Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
                List<DatosFinancieroDto> _datosFinancieros = new List<DatosFinancieroDto>();
                _datosFinancieros.Add(new DatosFinancieroDto { EsCliente = false, PoseeNomina = false, NegocioPropio = true, Asalariado = false, PromedioIngresoMensualNombre = "" });
                SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = 1, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, DatosFinancieros = _datosFinancieros };
                ts++;
                var result = await solicitudData.CrearSolicitud(_solicitud);
                if (ts == 10)
                {
                    ts = 1;
                }
                Assert.IsTrue(result != null);
            }
        }

        [TestMethod]
        public async Task CrearSolicitudSiniestro()
        {
            List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
            _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Sofia", Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
            List<SiniestroDto> _siniestro = new List<SiniestroDto>();
            _siniestro.Add(new SiniestroDto { NumeroPoliza = "er333", Detalle = "prueba", Fecha = DateTime.Now });
            SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = 1, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, Siniestro = _siniestro };

            var result = await solicitudData.CrearSolicitud(_solicitud);

            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task CrearSolicitudSiniestroCiudad()
        {
            List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
            _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Sofia", Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
            List<SiniestroCiudadDto> _siniestroCiudad = new List<SiniestroCiudadDto>();
            _siniestroCiudad.Add(new SiniestroCiudadDto { IdCiudad = 2 });
            List<SiniestroDto> _siniestro = new List<SiniestroDto>();
            _siniestro.Add(new SiniestroDto { NumeroPoliza = "er333", Detalle = "prueba", Fecha = DateTime.Now, SiniestroCiudad = _siniestroCiudad });
            SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = 1, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, Siniestro = _siniestro };

            var result = await solicitudData.CrearSolicitud(_solicitud);

            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task CrearSolicitudSiniestroPais()
        {
            List<DatosPersonalesDto> _datosPersonales = new List<DatosPersonalesDto>();
            _datosPersonales.Add(new DatosPersonalesDto { Nombre = "Sofia", Apellidos = "Muñoz", Telefono = "523521", Email = "prueba", Masculino = false, Nacionalidad = "Colombiana", CiudadResidencia = "prueba", IdTipoIdentificacion = 1, NumeroIdentificacion = "22", Profesion = "Ingeniero" });
            List<SiniestroPaisDto> _siniestroPais = new List<SiniestroPaisDto>();
            _siniestroPais.Add(new SiniestroPaisDto { IdPais = 2 });
            List<SiniestroDto> _siniestro = new List<SiniestroDto>();
            _siniestro.Add(new SiniestroDto { NumeroPoliza = "er333", Detalle = "prueba", Fecha = DateTime.Now, SiniestroPais = _siniestroPais });
            SolicitudDto _solicitud = new SolicitudDto { IdEstado = null, IdTipoSolicitud = 1, IdPais = 2, FechaCreacion = DateTime.Now, DatosPersonales = _datosPersonales, Siniestro = _siniestro };

            var result = await solicitudData.CrearSolicitud(_solicitud);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EditarSolicitud()
        {
            List<HistoricoComentarioDto> _historicoComentario = new List<HistoricoComentarioDto>();
            _historicoComentario.Add(new HistoricoComentarioDto { Comentario = "prueba", IdEstado = 2, FechaCreacion = DateTime.Now });
            SolicitudDto _solicitud = new SolicitudDto { IdEstado = 2, Id = 0, IdTipoContacto = 1, PathUrl = "http://local.lafise.com/gestores", HistoricoComentario = _historicoComentario };

            var result = await solicitudData.EditarSolicitud(_solicitud,"tester");
            Assert.IsTrue(result != null);
        }

    }
}
