﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public  class TasaCambioTest
    {
        private ITasaCambioData tasaCambioData = new TasaCambioData();

        [TestMethod]
        public async Task VerPorPaisActivo()
        {
            Int64 idPais = 161;
            var result = await tasaCambioData.VerPorPaisActivo(idPais);
            Assert.IsTrue(result.Count > 0);
        }
    }
}
