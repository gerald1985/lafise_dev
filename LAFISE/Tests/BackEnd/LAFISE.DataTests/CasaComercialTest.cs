﻿/*==========================================================================
Archivo:            CasaComercialTest
Descripción:        (Descripción de CasaComercialTest..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  29/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System;
using System.Collections.Generic;
using System.Collections;
#endregion

namespace LAFISE.DataTests
{
    [TestClass]
    public class CasaComercialTest
    {
        private ICasaComercialData targetCC;
        private ICasaComercialModeloAutoData targetCCMA;

        [TestInitialize()]
        public void Carga()
        {
            targetCC = new CasaComercialData();
            targetCCMA = new CasaComercialModeloAutoData();
        }

        [TestMethod]
        public async Task VerTodos()
        {
            var result = await targetCC.ConsultarTodosGrid();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerPorId()
        {
            var result = await targetCC.VerCasaComercialPorId(1);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CrearCasaComercial()
        {

            List<long> listaModelos = new List<long>();
            listaModelos.Add(3);
            listaModelos.Add(4);
            CasaComercialDto _casaComercial = new CasaComercialDto { Nombre = "PRUEBA UNITARIA CASA COMERCIAL", Nuevo = true, Usado = false, FechaCreacion = DateTime.Now, IdPais = 1 };
            CasaComercialDto result = await targetCC.CrearCasaComercial(_casaComercial);
            if (result != null)
            {
                foreach (var modelo in listaModelos)
                {
                    CasaComercialModeloAutoDto _CasaComercialModeloAutoDto = new CasaComercialModeloAutoDto { IdCasaComercial = result.Id, IdModeloAuto = modelo, FechaCreacion = DateTime.Now };
                    var resultDetalle = await targetCCMA.Crear(_CasaComercialModeloAutoDto);
                }

            }
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EditarCasaComercial()
        {
            CasaComercialDto _casaComercial = new CasaComercialDto { Id = 7, IdPais = 1, Nombre = "PRUEBA UNITARIA EDITAR4 CASA COMERCIAL", Nuevo = false, Usado = false };
            var result = await targetCC.EditarCasaComercial(_casaComercial);
            List<long> listaModelos = new List<long>();
            listaModelos.Add(3);
            if (result != null)
            {
                foreach (var modelo in listaModelos)
                {
                    CasaComercialModeloAutoDto _CasaComercialModeloAutoDto = new CasaComercialModeloAutoDto { IdCasaComercial = result.Id, IdModeloAuto = modelo, FechaCreacion = DateTime.Now };
                    var resultDetalle = await targetCCMA.Editar(_CasaComercialModeloAutoDto);
                }
            }
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarCasaComercial()
        {
            var result = await targetCC.EliminarDetalleCasaComercial(7);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task ConsultarDetalleCasaComercial()
        {
            var result = await targetCC.ConsultarDetalleCasaComercial(1);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public void ConsultarCasaComercialModeloMarcaFiltros()
        {
            var result = targetCC.ConsultarCasaComercialModeloMarcaFiltros("1,2,3");
            Assert.IsTrue(result != null);
        }


    }
}
