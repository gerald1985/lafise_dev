﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public class TipoPropiedadTest
    {
        private ITipoPropiedadData tipoPropiedadData = new TipoPropiedadData();

        [TestMethod]
        public async Task VerTodosTipoPropiedad()
        {
            var result = await tipoPropiedadData.VerTodosGrid();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task CrearTipoPropiedad()
        {
            TipoPropiedadDto _tipoPropiedad = new TipoPropiedadDto { Tipo="Es una prueba",Activo=true };
            var result = await tipoPropiedadData.Crear(_tipoPropiedad);
            
            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task EditarTipoPropiedad()
        {
            TipoPropiedadDto _tipoPropiedad = new TipoPropiedadDto { Id = 1, Tipo="Cassa",Activo=false };
            var result = await tipoPropiedadData.Editar(_tipoPropiedad);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarTipoPropiedad()
        {
            await tipoPropiedadData.Eliminar(9);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TipoPropiedadFiltro()
        {
            string tipo = "a";
            var result = await tipoPropiedadData.TipoPropiedadFiltro(tipo);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task ValidarTipoPropiedad()
        {
            Int64 tipo =13;
            var result = await tipoPropiedadData.ValidarTipoPropiedad(tipo);
            Assert.IsTrue(result==null);
        }
    }
}
