﻿/*==========================================================================
Archivo:            DepartamentoTest
Descripción:        (Descripción de DepartamentoTest..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;

#endregion

namespace LAFISE.DataTests
{
    [TestClass]
    public class DepartamentoTest
    {
        private IDepartamentoData targetD;

        [TestInitialize()]
        public void Carga()
        {
            targetD = new DepartamentoData();
        }

        [TestMethod]
        public async Task VerTodos()
        {
            var result = await targetD.VerTodos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerDepartamentoPorId()
        {
            var result = await targetD.VerDepartamentoPorId(1);
            Assert.IsTrue(result.Id != 0);
        }

        [TestMethod]
        public async Task VerDepartamentosPorPais()
        {
            var result = await targetD.VerDepartamentosPorPais(1);
            Assert.IsTrue(result.Count > 0);
        }
    }
}
