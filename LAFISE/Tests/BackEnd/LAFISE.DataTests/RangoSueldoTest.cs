﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public class RangoSueldoTest
    {
        private IRangoSueldoData rangoSueldoData = new RangoSueldoData();
        [TestMethod]
        public async Task VerTodosRangoSueldo()
        {
            var result = await rangoSueldoData.VerTodos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerporIdRangoSueldo()
        {
            var result = await rangoSueldoData.VerPorId(1);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearRangoSueldo()
        {
            RangoSueldoDto _rangoSueldo = new RangoSueldoDto { IdPais = 2, IdMoneda = 1 };
            var result = await rangoSueldoData.Crear(_rangoSueldo);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearRangoSueldoNoExistePais()
        {
            RangoSueldoDto _rangoSueldo = new RangoSueldoDto { IdPais = 5, IdMoneda = 1 };
            var result = await rangoSueldoData.Crear(_rangoSueldo);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearRangoSueldoNoExisteMoneda()
        {
            RangoSueldoDto _rangoSueldo = new RangoSueldoDto { IdPais = 2, IdMoneda = 3 };
            var result = await rangoSueldoData.Crear(_rangoSueldo);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EditarRangoSueldo()
        {
            List<RangoSueldoDetalleDto> _listaDetalle = new List<RangoSueldoDetalleDto>();
            _listaDetalle.Add(new RangoSueldoDetalleDto { MinSueldo = 1000, MaxSueldo = 2000 });
            _listaDetalle.Add(new RangoSueldoDetalleDto { MinSueldo = 2001, MaxSueldo = 3000 });
            _listaDetalle.Add(new RangoSueldoDetalleDto { MinSueldo = 3001, MaxSueldo = 4000 });
            _listaDetalle.Add(new RangoSueldoDetalleDto { MinSueldo = 4001, MaxSueldo = 5000 });
            RangoSueldoDto _rangoSueldo = new RangoSueldoDto { Id = 22, IdPais = 2, IdMoneda = 2, RangoSueldoDetalle = _listaDetalle };

            var result = await rangoSueldoData.Editar(_rangoSueldo);
            Assert.IsTrue(result != null);
        }

        //[TestMethod]
        //public EliminarRangoSueldo()
        //{
        //    //await rangoSueltoDetalleData.DeleteRango(1);
        //    rangoSueldoData.Eliminar(1);
        //    Assert.IsTrue(true);
        //}

        [TestMethod]
        public async Task VerRangoSueldoFiltro()
        {
            string pais = "2";
            var result = await rangoSueldoData.VerRangoSueldoFiltro(pais);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerRangoSueldoPorId()
        {
            Int64 id = 2;
            var result = await rangoSueldoData.VerPorId(id);
            Assert.IsTrue(result.Id == id);
        }

        [TestMethod]
        public async Task VerRangoSueldoDetallePorId()
        {
            Int64 id = 2;
            var result = await rangoSueldoData.VerPorId(id);
            Assert.IsTrue(result.RangoSueldoDetalle.Count > 0);
        }
    }
}
