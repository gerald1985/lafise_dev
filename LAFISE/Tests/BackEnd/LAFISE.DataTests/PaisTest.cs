﻿/*==========================================================================
Archivo:            PaisTest
Descripción:        (Descripción de PaisTest..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.DataTests
{
    [TestClass]
    public class PaisTest
    {
        private IPaisData targetP;

        [TestInitialize()]
        public void Carga()
        {
            targetP = new PaisData();
        }

        [TestMethod]
        public async Task VerTodos()
        {
            var result = await targetP.VerTodos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerPaisPorId()
        {
            var result = await targetP.VerPaisPorId(1);
            Assert.IsTrue(result.Id != 0);
        }
    }
}
