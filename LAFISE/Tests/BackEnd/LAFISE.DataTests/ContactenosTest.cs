﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public class ContactenosTest
    {

        private IContactenosData contactenosData = new ContactenosData();

        [TestMethod]
        public async Task VerTodosTarjeta()
        {
            var result = await contactenosData.VerTodosGrid();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerTodosContactenosporId()
        {
            var result = await contactenosData.VerPorId(1);
            Assert.IsTrue(result != null);
        }

       
        [TestMethod]
        public async Task CrearContactenos()
        {
            ContactenosDto _contactenos = new ContactenosDto {NombreCompleto = "Es una prueba", Telefono="11111",Mensaje="Es una prueba", IdPais=1,FechaCreacion=DateTime.Now,EsCliente=false,Email="prueba@prueba.com",Asunto="prueba" };
            var result = await contactenosData.Crear(_contactenos);

            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task EditarContactenos()
        {
            ContactenosDto _contactenos = new ContactenosDto { Id = 1, NombreCompleto = "Es una prueba", Telefono = "11111", Mensaje = "Es una prueba", IdPais = 1, FechaCreacion = DateTime.Now, EsCliente = false, Email = "prueba@prueba.com", Asunto = "prueba" };
            var result = await contactenosData.Editar(_contactenos);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarContactenos()
        {
            await contactenosData.Eliminar(1);
            Assert.IsTrue(true);
        }

    }
}
