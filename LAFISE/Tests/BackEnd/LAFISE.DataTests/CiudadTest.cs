﻿/*==========================================================================
Archivo:            CiudadTest
Descripción:        (Descripción de CiudadTest..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  08/10/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks; 
#endregion

namespace LAFISE.DataTests
{
    [TestClass]
    public class CiudadTest
    {
        private ICiudadData targetC;

        [TestInitialize()]
        public void Carga()
        {
            targetC = new CiudadData();
        }

        [TestMethod]
        public async Task VerTodos()
        {
            var result = await targetC.VerTodos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerCiudadPorId()
        {
            var result = await targetC.VerCiudadPorId(3);
            Assert.IsTrue(result.Id != 0);
        }

        [TestMethod]
        public async Task VerCiudadesPorDepartamento()
        {
            var result = await targetC.VerCiudadesPorDepartamento(1);
            Assert.IsTrue(result.Count > 0);
        }
    }
}
