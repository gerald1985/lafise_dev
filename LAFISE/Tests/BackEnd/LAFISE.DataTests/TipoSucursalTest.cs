﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public class TipoSucursalTest
    {
        private ITipoSucursalData tipoSucursalData = new TipoSucursalData();

        [TestMethod]
        public async Task VerTodosTipoSucursal()
        {
            var result = await tipoSucursalData.VerTodosGrid();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearTipoSucursal()
        {
            TipoSucursalDto _tipoSucursal = new TipoSucursalDto { TipoSucursal1 = "Es una prueba", Activo = true };
            var result = await tipoSucursalData.Crear(_tipoSucursal);
            
            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task EditarTipoSucursal()
        {
            TipoSucursalDto _tipoSucursal = new TipoSucursalDto { Id = 1, TipoSucursal1 = "Cassa", Activo = false };
            var result = await tipoSucursalData.Editar(_tipoSucursal);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarTipoSucursal()
        {
            await tipoSucursalData.Eliminar(2);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TipoSucursalFiltro()
        {
            string tipo = "a";
            var result = await tipoSucursalData.TipoSucursalFiltro(tipo);
            Assert.IsTrue(result.Count > 0);
        }

    }
}
