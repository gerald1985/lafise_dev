﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public class TarjetaTest
    {

        private ITarjetaData tarjetaData = new TarjetaData();

        [TestMethod]
        public async Task VerTodosTarjeta()
        {
            var result = await tarjetaData.VerTodosGrid();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerTodosTarjetaporId()
        {
            var result = await tarjetaData.VerPorId(1);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task InactivarTarjeta()
        {
            TarjetaDto _tarjeta = new TarjetaDto { Id=1, TarjetaCredito = "Es una prueba", Activo = false, Imagen = "prueba" };
            var result = await tarjetaData.Inactivar(_tarjeta);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CrearTarjeta()
        {
            TarjetaDto _tarjeta = new TarjetaDto {TarjetaCredito = "Es una prueba", Activo = true,Imagen="prueba" };
            var result = await tarjetaData.Crear(_tarjeta);

            Assert.IsTrue(result != null);
        }


        [TestMethod]
        public async Task EditarTarjeta()
        {
            TarjetaDto _tarjeta = new TarjetaDto { Id = 1, TarjetaCredito = "mastercard", Activo = false, Imagen = "prueba11" };
            var result = await tarjetaData.Editar(_tarjeta);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarTarjeta()
        {
            await tarjetaData.Eliminar(1);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TipoTarjeta()
        {
            string tarjeta = "a";
            var result = await tarjetaData.TarjetaFiltro(tarjeta);
            Assert.IsTrue(result.Count > 0);
        }
    }
}
