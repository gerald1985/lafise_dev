﻿/*==========================================================================
Archivo:            IndicadorTest
Descripción:        (Pruebas para indicadores)                      
Autor:              arley.lopez                          
Fecha de creación:  17/02/2016
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;

namespace LAFISE.DataTests
{
    [TestClass]
    public class IndicadorTest
    {
        private IIndicadorData indicadorData = new IndicadorData();

        [TestMethod]
        public void VerIndicadoresActivos()
        {
            var result = indicadorData.VerTodosActivos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void VerTodos()
        {
            var result = indicadorData.VerTodos();
            Assert.IsTrue(result != null);
        }
        
        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearIndicador()
        {
            IndicadorDto _indicador = new IndicadorDto { Activo = true };
            var result = await indicadorData.Crear(_indicador);

            Assert.IsTrue(result != null);
        }


        [TestMethod]
        //[ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        public async Task CrearIndicadorDtoCompleto()
        {
            IndicadorDto _indicador = new IndicadorDto { NombreIndicador = "USD/COP", SimboloActual = "USDCOP=X", SimboloHistorico = "COP=X", Activo = true };
            var result = await indicadorData.Crear(_indicador);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EditarIndicador()
        {
            IndicadorDto _indicador = new IndicadorDto { Id = 8, NombreIndicador = "USD/COP", SimboloActual = "USDCOP=X", SimboloHistorico = "COP=X", Activo = false };
            var result = await indicadorData.Editar(_indicador);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task InactivarIndicador()
        {
            IndicadorDto _indicador = new IndicadorDto { Id = 9, Activo = false };
            var result = await indicadorData.Inactivar(_indicador);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task EliminarIndicador()
        {
            await indicadorData.Eliminar(8);
            Assert.IsTrue(true);
        }
    }
}
