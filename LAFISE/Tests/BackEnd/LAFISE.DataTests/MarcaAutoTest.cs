﻿/*==========================================================================
Archivo:            MarcaAutoDataTest
Descripción:        (Descripción de MarcaAutoDataTest..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  29/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections.Generic;
#endregion

namespace LAFISE.DataTests
{
    [TestClass]
    public class MarcaAutoTest
    {
        private IMarcaAutoData targetMA;

        [TestInitialize()]
        public void Carga()
        {
            targetMA = new MarcaAutoData();
        }

        [TestMethod]
        public async Task VerMarcasModelos()
        {
            MarcaAutoDto _marcaAuto = new MarcaAutoDto { Marca = "f", Modelo = "l" };
            var result = await targetMA.VerMarcasModelos(_marcaAuto);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerTodos()
        {
            var result = await targetMA.VerTodos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerPorId()
        {
            var result = await targetMA.VerMarcaAutoPorId(2);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CrearMarcaAuto()
        {
            MarcaAutoDto _marcaAuto = new MarcaAutoDto { Marca = "BMW", Activo = true, FechaCreacion = DateTime.Now, listaModelos = new List<ModeloAutoDto>() };
            _marcaAuto.listaModelos.Add(new ModeloAutoDto { Modelo = "Serie 3", Imagen = "ImagenSerie3", FechaCreacion = DateTime.Now, Activo = true });
            _marcaAuto.listaModelos.Add(new ModeloAutoDto { Modelo = "X3", Imagen = "ImagenX3", FechaCreacion = DateTime.Now, Activo = true });
            _marcaAuto.listaModelos.Add(new ModeloAutoDto { Modelo = "X5", Imagen = "ImagenX5", FechaCreacion = DateTime.Now, Activo = true });
            _marcaAuto.listaModelos.Add(new ModeloAutoDto { Modelo = "Serie M", Imagen = "Serie M", FechaCreacion = DateTime.Now, Activo = true });
            var result = await targetMA.CrearMarcaAuto(_marcaAuto);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EditarMarcaAuto()
        {
            MarcaAutoDto _marcaAuto = new MarcaAutoDto { Id = 17, Marca = "BMW", FechaCreacion = DateTime.Now, Activo = true, listaModelos = new List<ModeloAutoDto>() };
            _marcaAuto.listaModelos.Add(new ModeloAutoDto { Modelo = "Serie 1", Imagen = "Serie1", FechaCreacion = DateTime.Now, Activo = true });
            _marcaAuto.listaModelos.Add(new ModeloAutoDto { Modelo = "Serie 5", Imagen = "Serie5", FechaCreacion = DateTime.Now, Activo = true });
            var result = await targetMA.EditarMarcaAuto(_marcaAuto);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarMarcaAuto()
        {
            var result = await targetMA.EliminarMarcaAutoPorId(13);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task InactivarMarcaAuto()
        {
            MarcaAutoDto _marcaAuto = new MarcaAutoDto { Id = 2, Activo = false };
            var result = await targetMA.InactivarMarcaAuto(_marcaAuto);
            Assert.IsTrue(result);
        }
    }
}
