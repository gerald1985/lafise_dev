﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;

namespace LAFISE.DataTests
{
    [TestClass]
    public class SuscripcionBoletinTest
    {

        private ISuscripcionBoletinData targetSB;

        [TestInitialize()]
        public void Carga()
        {
            targetSB = new SuscripcionBoletinData();
        }

        /// <summary>
        /// Prueba crear suscripción a boletin
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CrearSuscripcionBoletin()
        {
            SuscripcionBoletinDto _suscripcion = new SuscripcionBoletinDto { Nombre = "PRUEBA", Email = "steven.echavarria@arkix.com", FechaCreacion = DateTime.Now };
            SuscripcionBoletinDto result = await targetSB.Crear(_suscripcion);

            Assert.IsTrue(result != null);
        }
    }
}
