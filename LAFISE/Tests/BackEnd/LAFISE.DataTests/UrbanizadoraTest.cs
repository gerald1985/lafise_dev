﻿/*==========================================================================
Archivo:            UrbanizadoraDataTest
Descripción:        (Descripción de UrbanizadoraDataTest..........)                      
Autor:              steven.echavarria                          
Fecha de creación:  29/09/2015
Versión:            1.0                                                       
Derechos Reservados (c), 2015 Arkix S.A                                  
==========================================================================*/

#region Referencias
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LAFISE.Data;
using System.Threading.Tasks;
using LAFISE.Entities.Dtos;
using System.Collections;
using System.Collections.Generic;
#endregion

namespace LAFISE.DataTests
{
    [TestClass]
    public class UrbanizadoraTest
    {
        private IUrbanizadoraData targetU;

        [TestInitialize()]
        public void Carga()
        {
            targetU = new UrbanizadoraData();
        }

        [TestMethod]
        public async Task VerUrbanizadorasTiposPropiedades()
        {
            UrbanizadoraTipoPropiedadDto _urbanizadoraTipoPropiedad = new UrbanizadoraTipoPropiedadDto { Pais = null, Urbanizadora = null, TipoPropiedad = null };

            var result = await targetU.VerUrbanizadorasTiposPropiedades(_urbanizadoraTipoPropiedad);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerTodos()
        {
            var result = await targetU.VerTodosActivos();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task VerPorId()
        {
            var result = await targetU.VerUrbanizadoraPorId(2);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task CrearUrbanizadora()
        {

            List<TipoPropiedadDto> _listaDetalle = new List<TipoPropiedadDto>();
            _listaDetalle.Add(new TipoPropiedadDto { Id = 2 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 3 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 1 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 8 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 5 });
            UrbanizadoraDto _urbanizadora = new UrbanizadoraDto { Nombre = "Globant", IdPais = 2, Activo = true, ListaPropiedades = _listaDetalle };

            var result = await targetU.CrearUrbanizadora(_urbanizadora);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EditarUrbanizadora()
        {
            List<TipoPropiedadDto> _listaDetalle = new List<TipoPropiedadDto>();
            _listaDetalle.Add(new TipoPropiedadDto { Id = 2 });
            _listaDetalle.Add(new TipoPropiedadDto { Id = 3 });
            UrbanizadoraDto _urbanizadora = new UrbanizadoraDto { Id = 12, Nombre = "Chefo", IdPais = 1, Activo = false, ListaPropiedades = _listaDetalle };
            var result = await targetU.EditarUrbanizadora(_urbanizadora);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task EliminarUrbanizadora()
        {
            var result = await targetU.EliminarUrbanizadoraPorId(4);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task InactivarUrbanizadora()
        {
            UrbanizadoraDto _urbanizadora = new UrbanizadoraDto { Id = 2, Activo = true };
            var result = await targetU.InactivarUrbanizadora(_urbanizadora);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public async Task ConsultarDetalleUrbanizadora()
        {
            var result = await targetU.ConsultarDetalleUrbanizadora(2);
        }

        [TestMethod]
        public async Task EliminarUrbanizadoraTipo()
        {
            var result = await targetU.EliminarDetalleUrbanizadora(2);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task ExistePlanFinanciacionPorUrbanizadora()
        {
            var result = await targetU.ExistePlanFinanciacionPorUrbanizadora(22);
            Assert.IsTrue(result);
        }

    }
}
